<?php
class Avalara_tax_rates {

	//codeigniter object
	protected $CI;

	//urls & keys
	protected $url = 'https://taxrates.api.avalara.com:443/';
	private $apiKey;

	//all actions should call this function
	function __construct(array $config = array())
	{
		//load codeigniter into this
		$this->CI =& get_instance();

		//set api key
		$this->CI->config->load('api_keys');
		$avalaraConfig = $this->CI->config->item('avalara');
		$this->apiKey = $avalaraConfig['key'];
	}

	public function calc_tax(array $address = array())
	{
		//load CURL library
		$this->CI->load->library('curl');

		//build data
		$sendData = array(
			'street' => (isset($address['address1']))? $address['address1'] : '',
			'city' => (isset($address['city']))? $address['city'] : '',
			'state' => (isset($address['state']))? $address['state'] : '',
			'postal' => (isset($address['zip']))? $address['zip'] : '',
			'country' => 'usa',
			'apikey' => $this->apiKey
		);

		//determine url
		if (isset($address['address1']) && $address['address1'] != '')
		{
			$url = $this->url . 'address';
		} else {
			$url = $this->url . 'postal';
		}

		//make call
		$response = json_decode($this->CI->curl->simple_get($url, $sendData), TRUE);

		if (isset($response['totalRate']))
		{
			return $response['totalRate'];
		} else {
			return false;
		}
	}
}