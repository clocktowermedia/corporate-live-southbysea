<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation {

	protected $CI;

	/**
	 * Constructor
	 */
	public function __construct($rules = array())
	{
		parent::__construct($rules);
	}

	// get CI instance object 
    public function __get ($object)
    {
		$this->CI =& get_instance();
		return $this->CI->$object;
    }

	/**
	* Extending form helper to add ReCaptcha
	*/
	public function valid_recaptcha($str) 
	{
		//load library
		$this->CI->load->library(array('recaptcha'));

		//check if result is valid
		$result = $this->CI->recaptcha->recaptcha_check_answer();
		
		if ($result->is_valid) {
			return TRUE;
		} else {
			$this->CI->form_validation->recaptcha_error = $result->error;
			$this->CI->form_validation->set_message('valid_recaptcha', 'The %s is incorrect. Please try again.');
			return FALSE;
		}
	}
	
	/**
	* Extending form helper to a valid url check
	*/
	public function valid_url($str)
	{
		$pattern = "/^(http|https|ftp):\/\/([A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i";
		
		if (!preg_match($pattern, $str))
		{
			$this->CI->form_validation->set_message('valid_url', 'The %s is not a valid URL. Please try again.');
			RETURN FALSE;
		}
		
		return TRUE;
	}

	/**
	 * SEO friendly URL
	 *
	 * @access	public
	 * @param	string
	 * @return	bool
	 */
	public function seo_url($str)
	{
		$valid = (!preg_match("/^[a-z0-9-]+$/", $str))? FALSE : TRUE;

		if ($valid == FALSE)
		{
			$this->CI->form_validation->set_message('seo_url', 'The %s does not adhere to the advised format. Please try again.');
		}

		return $valid;
	}

	/**
	 * Password validator - 1 uppercase, 1 lowercase, 1 number, 8-40 character length
	 *
	 * @access	public
	 * @param	string
	 * @return	bool
	 */
	public function strong_pw($str)
	{
		$valid = (!preg_match("/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,40}$/", $str))? FALSE : TRUE;

		if ($valid == FALSE)
		{
			$this->CI->form_validation->set_message('strong_pw', 'The %s is not strong enough. Please try again.');
		}

		return $valid;
	}
}
