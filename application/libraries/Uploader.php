<?php
class Uploader {

	//codeigniter object
	protected $CI;

	//image upload variables
	public $uploadPaths = array(
		'uploadFolder' => '',
		'imgUploadFolder' => '',
		'imgThumbUploadFolder' => ''
	);

	private $fieldName = 'userfile';

	//model
	private $modelName = 'images_model';

	//thumbnail sizing
	private $thumbConfig = array(
		'width' => 100,
		'height' => 100,
		'maintain_ratio' => FALSE
	);

	//all actions should call this function
	function __construct(array $config = array())
	{
		//load codeigniter into this
		$this->CI =& get_instance();

		//default upload paths
		$this->uploadPaths = array(
			'uploadFolder' => '/uploads/',
			'imgUploadFolder' => '/images/',
			'imgThumbUploadFolder' => '/images/thumbnails/'
		);

		//set upload paths from config, else set default ones
		if (count($config) > 0)
		{
			//set upload paths
			if (isset($config['uploadPaths']) && is_array($config['uploadPaths']))
			{
				$this->set_upload_paths($config['uploadPaths']);
			}

			//set thumbnail sizing
			if (isset($config['thumbConfig']) && is_array($config['thumbConfig']))
			{
				$this->set_thumbnail_config($config['thumbConfig']);
			}

			//set model
			if (isset($config['model']))
			{
				$this->set_model($config['model']);
			}

			//set fieldname
			if (isset($config['fieldName']))
			{
				$this->set_fieldname($config['fieldName']);
			}
		}
	}

	public function set_upload_paths(array $uploadPaths = array())
	{
		if (!empty($uploadPaths) && is_array($uploadPaths))
		{
			$this->uploadPaths = $uploadPaths;
		}
	}

	public function set_thumbnail_config(array $thumbConfig = array())
	{
		if (!empty($thumbConfig) && is_array($thumbConfig))
		{
			$this->thumbConfig = $thumbConfig;
		}
	}

	public function adjust_thumbnail_size($width = 0, $height = 0)
	{
		if ($width > 0)
		{
			$this->thumbConfig['width'] = $width;
		}

		if ($height > 0)
		{
			$this->thumbConfig['height'] = $height;
		}
	}

	public function set_model($modelName = '')
	{
		if ($modelName != '')
		{
			$this->modelName = $modelName;
		}
	}

	public function set_fieldname($fieldName = '')
	{
		if ($fieldName != '')
		{
			$this->fieldName = $fieldName;
		}
	}

	public function upload(array $formData = array())
	{
		//get model we want to save this to & load it
		$this->CI->load->model($this->modelName, 'file_model');

		//retrieve file names & replace yucky characters
		$name = $_FILES[$this->fieldName]['name'];
		$name = strtr($name, 'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
		$name = preg_replace('/([^.a-z0-9]+)/i', '_', $name);

		//get file extension
		$extension = strrchr($name, '.');

		//Your upload directory and some config settings
		$config['upload_path'] = (in_array($extension, $imageTypes))? $_SERVER['DOCUMENT_ROOT'] . $this->uploadPaths['imgUploadFolder'] : $_SERVER['DOCUMENT_ROOT'] . $this->uploadPaths['uploadFolder'];
		$config['max_size'] = '10000';
		$config['file_name'] = $name;
		$config['allowed_types'] = '*';

	   	//Load the upload library
		$this->CI->load->library('upload', $config);

		//create folder if doesn't exist
		if (!file_exists($config['upload_path'])) {
			mkdir('/' . $config['upload_path']);
		}

		//attempt upload
	   	if ($this->CI->upload->do_upload($this->fieldName))
		{
			//get upload data
			$data = $this->CI->upload->data();

			//use image function if necessary
			if ($data['is_image'] == true)
			{
				$this->upload_image();
			}

			//regular file data
			$fileData = array(
				'filename' => $data['file_name'],
				'filepath' => $this->uploadPaths['uploadFolder'],
				'fullpath' => $this->uploadPaths['uploadFolder'] . $data['file_name'],
				'extension' => $data['file_ext'],
				'mimeType' => $data['file_type'],
				'fileSize' => $data['file_size']
			);

			//merge file data, and form data together
			$fileData = array_merge($fileData, $formData);

			//insert in db
			$fileID = $this->CI->file_model->insert($fileData);

			//return file id
			return $fileID;

		} else {

			//return errors
			return $this->CI->upload->display_errors();
		}
	}

	/**
	* Function for uploading a file
	*/
	public function upload_image(array $formData = array())
	{
		//get model we want to save this to & load it
		$this->CI->load->model($this->modelName, 'image_model');

		//retrieve file names & replace yucky characters
		$name = $_FILES[$this->fieldName]['name'];
		$name = strtr($name, 'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
		$name = preg_replace('/([^.a-z0-9]+)/i', '_', $name);

		//get file extension
		$extension = strrchr($name, '.');

		//define image types
		$imageTypes = array('.gif', '.jpg', '.jpeg', '.png');

		//Your upload directory and some config settings
		$config['upload_path'] = (in_array($extension, $imageTypes))? $_SERVER['DOCUMENT_ROOT'] . $this->uploadPaths['imgUploadFolder'] : $_SERVER['DOCUMENT_ROOT'] . $this->uploadPaths['uploadFolder'];
		$config['max_size'] = '10000';
		$config['file_name'] = $name;
		$config['allowed_types'] = 'gif|jpg|png|JPG|GIF|PNG';

	   	//Load the upload library
		$this->CI->load->library('upload', $config);

		//create folder if doesn't exist
		if (!file_exists($config['upload_path'])) {
			mkdir('/' . $config['upload_path']);
		}

		//attempt upload
	   	if ($this->CI->upload->do_upload($this->fieldName))
		{
			//get upload data
			$data = $this->CI->upload->data();

			//if the file is an image...
			if ($data['is_image'] == true)
			{
				//Config for thumbnail creation
				$thumbConfig = $this->thumbConfig;
				$thumbConfig['new_image'] = $_SERVER['DOCUMENT_ROOT'] . $this->uploadPaths['imgThumbUploadFolder'];
				$thumbConfig['image_library'] = 'gd2';
				$thumbConfig['source_image'] = $_SERVER['DOCUMENT_ROOT'] . $this->uploadPaths['imgUploadFolder'] . $data['file_name'];
				$thumbConfig['create_thumb'] = TRUE; //this adds _thumb to the end of the file name

				//create folder if doesn't exist
				if (!file_exists($thumbConfig['new_image'])) {
					mkdir('/' . $thumbConfig['new_image']);
				}

				//create thumbnails
				$this->CI->load->library('image_lib', $thumbConfig);
				$this->CI->image_lib->initialize($thumbConfig);
				if (!$this->CI->image_lib->resize())
				{
					return $this->CI->image_lib->display_errors();
				}

				//grab thumbnail info
				$thumbName = str_replace($extension, '', $data['file_name']) . '_thumb' . $extension;
				$thumbPath = $_SERVER['DOCUMENT_ROOT'] . $this->uploadPaths['imgThumbUploadFolder'] . $thumbName;
				$thumbFilesize = round ((filesize($thumbPath) / 1024 ), 2);
				$thumbInfo = getimagesize($thumbPath);

				//set image data
				$imageData = array(
					'width' => $data['image_width'],
					'height' => $data['image_height'],
					'widthHeightString' => $data['image_size_str'],
					'thumbFilename' => $thumbName,
					'thumbFilepath' => $this->uploadPaths['imgThumbUploadFolder'],
					'thumbFullPath' => $this->uploadPaths['imgThumbUploadFolder'] . $thumbName,
					'thumbExtension' => $extension,
					'thumbMimeType' => $thumbInfo['mime'],
					'thumbFileSize' => $thumbFilesize,
					'thumbWidth' => $thumbInfo[0],
					'thumbHeight' => $thumbInfo[1],
					'thumbWidthHeightString' => $thumbInfo[3]
				);

			} else {
				return 'This file is not an image.';
			}

			//regular file data
			$fileData = array(
				'filename' => $data['file_name'],
				'filepath' => (in_array($extension, $imageTypes))? $this->uploadPaths['imgUploadFolder'] : $this->uploadPaths['uploadFolder'],
				'fullpath' => (in_array($extension, $imageTypes))? $this->uploadPaths['imgUploadFolder'] . $data['file_name'] : $this->uploadPaths['uploadFolder'] . $data['file_name'],
				'extension' => $data['file_ext'],
				'mimeType' => $data['file_type'],
				'fileSize' => $data['file_size']
			);

			//merge image, file data, and form data together
			$fileData = array_merge($fileData, $imageData, $formData);

			//insert in db
			$imageID = $this->CI->image_model->insert($fileData);

			//return file id
			return $imageID;

		} else {

			//return errors
			return $this->CI->upload->display_errors();
		}
	}
}