<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Library to build uniform form elements with bootstrap v3 styling
 * @author Marissa Eder
 * TODO: File, button/submit, date, time, range, week, color, datetime, datetime-local, image, month, search, reset, color
 */
class Form_builder {

	//settings
	private $styling = true;
	private $showLabels = true;

	//defaults
	private $formClass = 'form-horizontal';
	private $fieldClasses;
	private $addons;

	//classes
	private $inputClass;
	private $inputGroupClass;
	private $groupClass;
	private $labelClass;
	private $labelHiddenClass;

	//constructor
	public function __construct($formClass = 'form-horizontal', $styling = true, $showLabels = true) {

		//set defaults
		$this->formClass = $formClass;
		$this->styling = $styling;
		$this->showLabels = $showLabels;
		$this->set_defaults();
		$this->set_input_classes();
	}

	/************************************************************************************************************************************
	/
	/ Settings
	/
	/***********************************************************************************************************************************/

	/**
	 * Change the styling variable
	 * @param [boolean] $value [new value to set the styling variable to]
	 */
	public function set_styling($value = true)
	{
		$this->styling = $value;
		$this->set_input_classes();
	}

	/**
	 * Change the show labels variable
	 * @param [boolean] $value [new value to set the styling variable to]
	 */
	public function set_labels($value = true)
	{
		$this->showLabels = $value;
	}

	/**
	 * Change the form class
	 * @param [string] $value [new form class]
	 */
	public function set_form_class($value)
	{
		$this->formClass = $value;
	}

	/**
	 * Resets default classes or  addons to empty
	 * @param [string] $type [what to reset]
	 */
	private function set_defaults($type = 'all')
	{
		//set empty values as default
		$fieldClasses = array(
			'labelClass' => '',
			'containerClass' => '',
			'inputClass' => '',
			'groupClass' => ''
		);

		$addons['prepend'] = array(
			'type' => 'text',
			'content' => ''
		);
		$addons['append'] = array(
			'type' => 'text',
			'content' => ''
		);

		//reset specified variable
		if ($type == 'classes')
		{
			$this->fieldClasses = $fieldClasses;

		} else if ($type == 'addons') {

			$this->addons = $addons;

		} else {
			$this->fieldClasses = $fieldClasses;
			$this->addons = $addons;
		}
	}

	public function set_container_classes($classes = array())
	{
		$this->set_defaults('classes');

		if (is_array($classes) && count($classes) > 0)
		{
			foreach ($classes as $key => $value)
			{
				$this->fieldClasses[$key] = $value;
			}
		}
	}

	private function set_input_classes()
	{
		$this->inputClass = ($this->styling == true)? 'form-control ' : '';
		$this->inputGroupClass = ($this->styling == true)? 'input-group ' : '';
		$this->groupClass = ($this->styling == true)? 'form-group ' : '';
		$this->labelClass = ($this->styling == true)? 'control-label ' : '';
		$this->labelHiddenClass = ($this->styling == true)? 'sr-only ' : '';
	}

	public function set_class($className, $value)
	{
		$this->{$className} = $value;
	}

	public function set_addons($addons = array())
	{
		if (is_array($addons) && count($addons) > 0)
		{
			foreach ($addons as $type => $index)
			{
				foreach ($index as $key => $value)
				{
					$this->addons[$type][$key] = $value;
				}
			}
		} else {
			$this->set_defaults('addons');
		}
	}

	/************************************************************************************************************************************
	/
	/ Element Builder Functions
	/
	/***********************************************************************************************************************************/

	/**
	 * Builds a text input field
	 */
	public function text($name, $label, $value, $classes = array(), $placeholder, $helpblock, $addons, $attributes)
	{
		$this->build_text_field('text', $name, $label, $value, $classes, $placeholder, $helpblock, $addons, $attributes);
	}

	/**
	 * Builds a number input field
	 */
	public function number($name, $label, $value, $classes = array(), $placeholder, $helpblock, $addons, $attributes)
	{
		$this->build_text_field('number', $name, $label, $value, $classes, $placeholder, $helpblock, $addons, $attributes);
	}

	/**
	 * Builds a email input field
	 */
	public function email($name, $label, $value, $classes = array(), $placeholder, $helpblock, $addons, $attributes)
	{
		$this->build_text_field('email', $name, $label, $value, $classes, $placeholder, $helpblock, $addons, $attributes);
	}

	/**
	 * Builds a tel input field
	 */
	public function tel($name, $label, $value, $classes = array(), $placeholder, $helpblock, $addons, $attributes)
	{
		$this->build_text_field('tel', $name, $label, $value, $classes, $placeholder, $helpblock, $addons, $attributes);
	}

	/**
	 * Builds a url input field
	 */
	public function url($name, $label, $value, $classes = array(), $placeholder, $helpblock, $addons, $attributes)
	{
		$this->build_text_field('url', $name, $label, $value, $classes, $placeholder, $helpblock, $addons, $attributes);
	}

	/**
	 * Builds a file input field
	 */
	public function file($name, $label, $value, $classes = array(), $placeholder, $helpblock, $addons, $attributes)
	{
		$this->build_text_field('file', $name, $label, $value, $classes, $placeholder, $helpblock, $addons, $attributes);
	}

	/**
	 * Builds a password input field
	 */
	public function password($name, $label, $value, $classes = array(), $placeholder, $helpblock, $addons, $attributes)
	{
		$this->build_text_field('password', $name, $label, $value, $classes, $placeholder, $helpblock, $addons, $attributes);
	}

	/**
	 * Builds a textarea input field
	 */
	public function textarea($name, $label, $value, $classes, $rows = 5, $placeholder, $helpblock, $addons, $attributes)
	{
		//set classes & addons
		$this->set_container_classes($classes);
		$this->set_addons($addons);

		//start main container
		$this->build_container_start($name, $label, $this->fieldClasses['labelClass'], $this->fieldClasses['containerClass'], $this->fieldClasses['groupClass'], $this->addons['prepend'], $this->addons['append']);

		//build the element itself
		$attributes = $this->build_attributes($attributes);
		echo '<textarea rows="' . $rows . '" name="' . $name . '" class="'. $this->inputClass . ' ' . $this->fieldClasses['inputClass'] . '" placeholder="' . $placeholder . '"' .  $attributes . '>' . $value . '</textarea>';

		//start the end container
		$this->build_container_end($this->fieldClasses['containerClass'], $helpblock, $this->addons['prepend'], $this->addons['append']);
	}

	/**
	 * Builds a dropdown
	 */
	public function select($name, $label, $options = array(), $values, $classes = array(), $helpblock, $addons, $attributes, $isOptgroup = null)
	{
		//set classes & addons
		$this->set_container_classes($classes);
		$this->set_addons($addons);

		//start main container
		$this->build_container_start($name, $label, $this->fieldClasses['labelClass'], $this->fieldClasses['containerClass'], $this->fieldClasses['groupClass'], $this->addons['prepend'], $this->addons['append']);

		//build the element itself
		$attributes = $this->build_attributes($attributes);
		echo '<select name="' . $name . '" class="'. $this->inputClass . ' ' . $this->fieldClasses['inputClass'] . '" ' .  $attributes . '>';

		//add options
		$this->build_options($options, $values, $isOptgroup);

		//close element
		echo '</select>';

		//start the end container
		$this->build_container_end($this->fieldClasses['containerClass'], $helpblock, $this->addons['prepend'], $this->addons['append']);
	}

	/**
	 * Builds a hidden field
	 */
	public function hidden($name, $value, $attributes = array())
	{
		$attributes = $this->build_attributes($attributes);
		echo '<input type="hidden" name="' . $name . '" value="' . $value . '" ' . $attributes . '/>';
	}

	public function psuedo_hidden($name, $label, $value, $classes = array(), $placeholder, $helpblock, $addons, $attributes)
	{
		$this->build_text_field('hidden', $name, $label, $value, $classes, $placeholder, $helpblock, $addons, $attributes);
	}

	/**
	 * Builds a singular radio box
	 */
	public function radio($name, $label, $uncheckedValue, $checkedValue, $value, $classes = array(), $prefix, $helpblock, $attributes)
	{
		//create readio
		$this->build_checkbox_radio('radio', $name, $label, $uncheckedValue, $checkedValue, $value, $classes, $prefix, $helpblock, $attributes);
	}

	/**
	 * Builds a radio box group
	 */
	public function radios($name, $label, $options, $values, $classes = array(), $helpblock, $attributes)
	{
		//create radio group
		$this->build_checkbox_radio_group('radio', $name, $label, $options, $values, $classes, $helpblock, $attributes);
	}

	/**
	 * Builds a singular checkbox
	 */
	public function checkbox($name, $label, $uncheckedValue, $checkedValue, $value, $classes = array(), $prefix, $helpblock, $attributes)
	{
		//create checkbox
		$this->build_checkbox_radio('checkbox', $name, $label, $uncheckedValue, $checkedValue, $value, $classes, $prefix, $helpblock, $attributes);
	}

	/**
	 * Builds a checkbox box group
	 */
	public function checkboxes($name, $label, $options, $values, $classes = array(), $helpblock, $attributes)
	{
		//create checkbox group
		$this->build_checkbox_radio_group('checkbox', $name, $label, $options, $values, $classes, $helpblock, $attributes);
	}

	public function button($type = 'submit', $value = 'submit', $classes, $icon, $attributes)
	{
		//set classes
		$this->set_container_classes($classes);

		//start container
		$this->build_check_radio_container_start($this->fieldClasses['containerClass']);

		//build element itself
		$attributes = $this->build_attributes($attributes);
		$icon = ($icon != '')? '<i class="' . $icon . '"></i> ' : '';
		echo '<button type="' . $type . '" class="' . $this->fieldClasses['inputClass'] . '" ' . $attributes . '>' . $icon . $value . '</button>';

		//end container
		$this->build_check_radio_container_end($this->fieldClasses['containerClass'], '');
	}

	/************************************************************************************************************************************
	/
	/ Building input markup
	/
	/***********************************************************************************************************************************/

	/**
	 * Builds any field that is basically similar to text
	 * Text, Number, Tel, Email, Url, Password, etc.
	 */
	public function build_text_field($type = 'text', $name, $label, $value, $classes, $placeholder, $helpblock, $addons, $attributes)
	{
		//set classes
		$this->set_container_classes($classes);
		$this->set_addons($addons);

		//start main container
		$this->build_container_start($name, $label, $this->fieldClasses['labelClass'], $this->fieldClasses['containerClass'], $this->fieldClasses['groupClass'], $this->addons['prepend'], $this->addons['append']);

		//build element itself
		$attributes = $this->build_attributes($attributes);
		echo '<input type="' . $type .'" name="' . $name . '" value="' . $value . '" class="'. $this->inputClass . ' ' . $this->fieldClasses['inputClass'] . '" placeholder="' . $placeholder . '"' .  $attributes . ' />';

		//start the end container
		$this->build_container_end($this->fieldClasses['containerClass'], $helpblock, $this->addons['prepend'], $this->addons['append']);
	}

	/**
	 * Builds a singular checkbox or radio button
	 */
	public function build_checkbox_radio($type = 'checkbox', $name, $label, $uncheckedValue, $checkedValue, $value, $classes, $prefix, $helpblock, $attributes)
	{
		//set defaults
		$this->set_container_classes($classes);

		//start container
		$this->build_check_radio_container_start($this->fieldClasses['containerClass'], $prefix, $this->fieldClasses['labelClass']);

		//add hidden input
		if ($type == 'checkbox')
		{
			$this->hidden($name, $uncheckedValue);
		}

		//determine if element is checked
		$checked = ($value == $checkedValue)? ' checked="checked"' : '';

		//build typeclass
		$typeClass = ($this->styling == true)? $type : '';
		$attributes = $this->build_attributes($attributes);

		//output element
		echo '<div class="' . $typeClass . '">' .
			'<label>' .
				'<input type="' . $type . '" name="' . $name  . '" value="' . $checkedValue . '" class="' . $this->fieldClasses['inputClass'] . '" ' . $attributes . $checked . '>' .
				'<span class="label-container">' . $label . '</span>' .
			'</label>' .
		'</div>';

		//end container
		$this->build_check_radio_container_end($this->fieldClasses['containerClass'], $helpblock);
	}

	/**
	 * Builds a group of checkboxes or radio options
	 */
	public function build_checkbox_radio_group($type = 'checkbox', $name, $label, $options, $values, $classes, $helpblock, $attributes)
	{
		//set defaults
		$this->set_container_classes($classes);

		//start container
		$this->build_check_radio_container_start($this->fieldClasses['containerClass'], $label, $this->fieldClasses['labelClass']);

		//build checkboxes
		if (is_array($options) && count($options) > 0)
		{
			foreach ($options as $value => $text)
			{
				//see if field is checked
				$checked = (is_array($values))? in_array($value, $values) : ($values == $value);
				$checkedMarkup = ($checked == true)? ' checked="checked"' : '';

				//build typeclass
				$typeClass = ($this->styling == true)? $type : '';

				//build elements
				$attributes = $this->build_attributes($attributes);

				//inline field markup
				$markup = '<label class="' . $typeClass . '-inline">' .
					'<input type="' . $type . '" name="' . $name  . '" value="' . $value . '" class="' . $this->fieldClasses['inputClass'] . '" ' . $attributes . $checkedMarkup . '>' .
					'<span class="label-container">' . $text . '</span>' .
				'</label>';

				//stacked field markup
				$markup2 = '<div class="' . $typeClass . '">' .
					'<label>' .
						'<input type="' . $type . '" name="' . $name  . '" value="' . $value . '" class="' . $this->fieldClasses['inputClass'] . '" ' . $attributes . $checkedMarkup . '>' .
						'<span class="label-container">' . $text . '</span>' .
					'</label>' .
				'</div>';

				//determine if we are displaying these inline or not
				if ($this->fieldClasses['groupClass'] == 'inline')
				{
					echo $markup;
				} else {
					echo $markup2;
				}
			}
		}

		//end container
		$this->build_check_radio_container_end($this->fieldClasses['containerClass'], $helpblock);
	}

	/**
	 * Builds the options for a select field
	 */
	public function build_options($options = array(), $values = array(), $isOptgroup = false)
	{
		if (is_array($options) && count($options) > 0)
		{
			foreach ($options as $value => $text)
			{
				//determine if we have option groups or not
				if (is_array($text) && !empty($text) && isset($text['text']) && (isset($text['children']) || $isOptgroup == true))
				{
					if ($isOptgroup == true)
					{
						//display category & children
						echo '<optgroup label="' . $text['text'] . '">';
							if (count($text['children']) > 0)
							{
								foreach ($text['children'] as $optValue => $optText)
								{
									$selected = (is_array($values))? in_array($optValue, $values) : ($values == $optValue);
									$selectedMarkup = ($selected == true)? 'selected="selected"' : '';
									echo '<option value="'. $optValue .'" ' . $selectedMarkup . '>' . $optText .'</option>';
								}
							}
						echo '</optgroup>';

					} else {
						//display category
						$selected = (is_array($values))? in_array($value, $values) : ($values == $value);
						$selectedMarkup = ($selected == true)? 'selected="selected"' : '';
						echo '<option value="'. $value .'" ' . $selectedMarkup . '>' . $text['text'] .'</option>';

						//display children
						if (count($text['children']) > 0)
						{
							foreach ($text['children'] as $optValue => $optText)
							{
								$selected = (is_array($values))? in_array($optValue, $values) : ($values == $optValue);
								$selectedMarkup = ($selected == true)? 'selected="selected"' : '';
								echo '<option value="'. $optValue .'" ' . $selectedMarkup . '>&nbsp;&nbsp;&nbsp;' . $optText .'</option>';
							}
						}
					}
				} else if (is_array($text) && !empty($text) && isset($text['text'])) {

					//build attributes
					$attributes = (isset($text['attributes']))? $this->build_attributes($text['attributes']) : '';
					$selected = (is_array($values))? in_array($value, $values) : ($values == $value);
					$selectedMarkup = ($selected == true)? 'selected="selected"' : '';
					echo '<option value="'. $value .'" ' . $selectedMarkup . $attributes . '>' . $text['text'] .'</option>';

				} else {
					$selected = (is_array($values))? in_array($value, $values) : ($values == $value);
					$selectedMarkup = ($selected == true)? 'selected="selected"' : '';
					echo '<option value="'. $value .'" ' . $selectedMarkup . '>' . $text .'</option>';
				}
			}
		}
	}

	/**
	 * Displays a label for an input field
	 */
	public function build_label($name, $text = '', $classes = '')
	{
		//determine what bootstrap classes we need
		if ($this->formClass == 'form-inline') {
			$classes = $this->labelHiddenClass . $classes;
		} else if ($this->formClass == 'form-horizontal' || $this->formClass == 'form-vertical') {
			$classes = $this->labelClass . $classes;
		}

		//output labels if we need them
		if ($this->showLabels == true)
		{
			echo '<label for="' . $name . '" class="' . $classes . '">' . $text  . '</label>';
		}
	}

	/************************************************************************************************************************************
	/
	/ Container Markup
	/
	/***********************************************************************************************************************************/

	/**
	 * Opening of generic field container
	 */
	public function build_container_start($name, $label, $labelClasses = '', $containerClass = '', $groupClass = '', $prepend = array(), $append = array())
	{
		//build container
		$this->start_container();

		//add label
		$this->build_label($name, $label, $labelClasses);

		//add input styling container styling if applicable
		if ($containerClass != '') { echo '<div class="'. $containerClass . '">'; }

		//add prepend if applicable
		if ($prepend['content'] != '' || $append['content'] != '') { $this->start_group($groupClass); }
		$this->display_prepend_append($prepend);
	}

	/**
	 * Closing of generic field container
	 */
	public function build_container_end($containerClass = '', $helpblock = '', $prepend = array(), $append = array())
	{
		//add append if applicable
		$this->display_prepend_append($append);

		//add help block if applicable
		$this->display_help_block($helpblock);

		//add group
		if ($prepend['content'] != '' || $append['content'] != '') { $this->end_group(); }

		//add end for input styling container
		if ($containerClass != '') { $this->end_container(); }

		//add closing elements
		$this->end_container();
	}

	/**
	 * First half of checkbox/radio field container
	 */
	public function build_check_radio_container_start($containerClass = '', $label = '', $labelClass = '')
	{
		//build container
		$this->start_container();

		//add prefix label
		$this->build_label('', $label, $labelClass);

		//add input styling container styling if applicable
		if ($containerClass != '') { echo '<div class="'. $containerClass . '">'; }
	}

	/**
	 * Second half of checkbox/radio field container
	 */
	public function build_check_radio_container_end($containerClass = '', $helpblock = '')
	{
		//add help block if applicable
		$this->display_help_block($helpblock);

		//add end for input styling container
		if ($containerClass != '') { $this->end_container(); }

		//add closing elements
		$this->end_container();
	}

	/**
	 * Start form group container
	 */
	public function start_container()
	{
		echo '<div class="' . $this->groupClass . '">';
	}

	/**
	 * End form group container
	 */
	public function end_container()
	{
		echo '</div>';

		//reset input classes
		$this->set_input_classes();
	}

	/**
	 * Start a form control group
	 * @param  [type] $classes [description]
	 */
	public function start_group($classes = '')
	{
		echo '<div class="'. $this->inputGroupClass . ' ' . $classes  . '">';
	}

	/**
	 * End a form control group
	 */
	public function end_group()
	{
		$this->end_container();
	}

	/************************************************************************************************************************************
	/
	/ Additional Elements
	/
	/***********************************************************************************************************************************/

	/**
	 * Adds a help block
	 * @param  [type] $text [description]
	 */
	public function display_help_block($text)
	{
		if ($text != '')
		{
			echo '<span class="help-block">' . $text . '</span>';
		}
	}

	/**
	 * Adds prepend/append block
	 * @param  [type] $text [description]
	 */
	public function display_prepend_append($addon = array())
	{
		//make sure proper values were set
		if (isset($addon['type']) && isset($addon['content']) && $addon['content'] != '')
		{
			switch ($addon['type'])
			{
				case 'text':
					echo '<span class="input-group-addon">' . $addon['content'] . '</span>';
					break;
				case 'button':
					$class = (isset($addon['class']))? $addon['class'] : 'btn btn-default';
					echo '<span class="input-group-btn">' .
						'<button class="' . $class . '" type="button">' . $addon['content'] . '</button>' .
					'</span>';
					break;
				case 'custom':
					echo $addon['content'];
					break;
			}
		}
	}

	/**
	 * Creates a string that will be appended to the input field with secondary info from the array
	 * @param  array  		   $options
	 * @return string          string with all options from the array
	 */
	private function build_attributes($attributes = array())
	{
		$formAttributes = '';

		if (is_array($attributes) && count($attributes) > 0)
		{
			foreach ($attributes as $key => $value)
			{
				$formAttributes .= ' ' . $key . '="' . $value . '"' . ' ';
			}
		}

		return $formAttributes;
	}
}

/* End of file form_builder.php */
/* Location: ./application/models/form_builder.php */