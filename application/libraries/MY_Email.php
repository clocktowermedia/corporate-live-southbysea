<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Email extends CI_Email {

	public $_orig_subject;
	public $_orig_reply_to;
	public $_orig_from;

	/**
	 * Constructor
	 */
	public function __construct(array $config = array())
	{
		parent::__construct($config);
	}

	public function clear_debugger_messages()
	{
		$this->_debug_msg = array();
	}

	public function get_debugger_messages()
	{
		return $this->_debug_msg;
	}

	/**
	 * Set FROM
	 *
	 * @param	string	$from
	 * @param	string	$name
	 * @param	string	$return_path = NULL	Return-Path
	 * @return	CI_Email
	 */
	public function from($from, $name = '', $return_path = NULL)
	{
		if (preg_match('/\<(.*)\>/', $from, $match))
		{
			$from = $match[1];
		}

		if ($this->validate)
		{
			$this->validate_email($this->_str_to_array($from));
			if ($return_path)
			{
				$this->validate_email($this->_str_to_array($return_path));
			}
		}

		$this->_orig_from['email'] = $from;

		// prepare the display name
		if ($name !== '')
		{
			$this->_orig_from['name'] = $name;

			// only use Q encoding if there are characters that would require it
			if ( ! preg_match('/[\200-\377]/', $name))
			{
				// add slashes for non-printing characters, slashes, and double quotes, and surround it in double quotes
				$name = '"'.addcslashes($name, "\0..\37\177'\"\\").'"';
			}
			else
			{
				$name = $this->_prep_q_encoding($name);
			}
		} else {
			$this->_orig_from['name'] = '';
		}

		$this->set_header('From', $name.' <'.$from.'>');

		isset($return_path) OR $return_path = $from;
		$this->set_header('Return-Path', '<'.$return_path.'>');

		return $this;
	}

	/**
	 * Set Reply-to
	 *
	 * @param	string
	 * @param	string
	 * @return	CI_Email
	 */
	public function reply_to($replyto, $name = '')
	{
		if (preg_match('/\<(.*)\>/', $replyto, $match))
		{
			$replyto = $match[1];
		}

		if ($this->validate)
		{
			$this->validate_email($this->_str_to_array($replyto));
		}

		$this->_orig_reply_to['email'] = $replyto;

		if ($name !== '')
		{
			$this->_orig_reply_to['name'] = $name;

			// only use Q encoding if there are characters that would require it
			if ( ! preg_match('/[\200-\377]/', $name))
			{
				// add slashes for non-printing characters, slashes, and double quotes, and surround it in double quotes
				$name = '"'.addcslashes($name, "\0..\37\177'\"\\").'"';
			}
			else
			{
				$name = $this->_prep_q_encoding($name);
			}
		} else {
			$this->_orig_reply_to['name'] = '';
		}

		$this->set_header('Reply-To', $name.' <'.$replyto.'>');
		$this->_replyto_flag = TRUE;

		return $this;
	}

	/**
	 * Set Email Subject
	 *
	 * @param	string
	 * @return	CI_Email
	 */
	public function subject($subject)
	{
		$this->_orig_subject = $subject;
		$subject = $this->_prep_q_encoding($subject);
		$this->set_header('Subject', $subject);
		return $this;
	}

	/**
	 * Set Body
	 *
	 * @param	string
	 * @return	CI_Email
	 */
	public function message($body)
	{
		$content = wordwrap($body, 75, $this->crlf);
		$content = rtrim(str_replace("\r", '', $body));
		$this->_body = $content;

		/* strip slashes only if magic quotes is ON
		   if we do it with magic quotes OFF, it strips real, user-inputted chars.

		   NOTE: In PHP 5.4 get_magic_quotes_gpc() will always return 0 and
			 it will probably not exist in future versions at all.
		*/
		if ( ! is_php('5.4') && get_magic_quotes_gpc())
		{
			$this->_body = stripslashes($this->_body);
		}

		return $this;
	}

	public function sbs_send()
	{
		//attempt sending with first set of credentials, do not clear email
		$sent = $this->send();

		//attempt sending with second set of credentials
		if (!$sent)
		{
			$messages = $this->get_debugger_messages();
			$message = (count($messages) > 0)? implode("\n", $messages) : $messages;
			log_message('error', $this->_orig_subject . ' - ' . strip_tags($message));

			/*
			//build new config settings
			$CI =& get_instance();
			$temp_config = array();
			$items = array('smtp_host', 'smtp_user', 'smtp_pass', 'smtp_port', 'crlf', 'newline');
			foreach ($items AS $item)
			{
				if (!is_null($CI->config->item($item . '_2')))
				{
					$temp_config[$item] = $CI->config->item($item . '_2');
				}
			}
			$second_config = array_merge($CI->config->config, $temp_config);

			//rebuild new email
			$second_email = new self();
			$second_email->initialize($second_config, FALSE);
			$second_email->from($this->_orig_from['email'], $this->_orig_from['name']);
			if ($this->_replyto_flag === TRUE && isset($this->_headers['Reply-To']) && $this->_headers['Reply-To'] != '')
			{
				$second_email->reply_to($this->_orig_reply_to['email'], $this->_orig_reply_to['name']);
			} else {
				$second_email->reply_to($this->_orig_from['email'], $this->_orig_from['name']);
			}
			$second_email->to($this->_recipients);
			if (count($this->_cc_array) > 0)
			{
				$second_email->cc($this->_cc_array);
			}
			if (count($this->_bcc_array) > 0)
			{
				$second_email->bcc($this->_bcc_array);
			}
			if (count($this->_attachments) > 0)
			{
				foreach ($this->_attachments AS $attachment)
				{
					$second_email->attach($attachment['name'][0]);
				}
			}
			$second_email->subject($this->_orig_subject);
			$second_email->message($this->_body);
			#$second_email->set_alt_message();
			$sent2 = $second_email->send();

			if (!$sent2)
			{
				$messages = $second_email->get_debugger_messages();
				$message = (count($messages) > 0)? implode("\n", $messages) : $messages;
				log_message('error', $this->_orig_subject . ' - ' . strip_tags($message));
			}

			//return new response
			return $sent2;
			*/
		}

		return $sent;
	}

	/**
	 * Word Wrap
	 *
	 * @param	string
	 * @param	int	line-length limit
	 * @return	string
	 */
	public function word_wrap($str, $charlim = NULL)
	{
		// Set the character limit, if not already present
		if (empty($charlim))
		{
			$charlim = empty($this->wrapchars) ? 76 : $this->wrapchars;
		}

		// Standardize newlines
		if (strpos($str, "\r") !== FALSE)
		{
			$str = str_replace(array("\r\n", "\r"), "\n", $str);
		}

		// Reduce multiple spaces at end of line
		$str = preg_replace('| +\n|', "\n", $str);

		// If the current word is surrounded by {unwrap} tags we'll
		// strip the entire chunk and replace it with a marker.
		$unwrap = array();
		if (preg_match_all('|\{unwrap\}(.+?)\{/unwrap\}|s', $str, $matches))
		{
			for ($i = 0, $c = count($matches[0]); $i < $c; $i++)
			{
				$unwrap[] = $matches[1][$i];
				$str = str_replace($matches[0][$i], '{{unwrapped'.$i.'}}', $str);
			}
		}

		// Use PHP's native function to do the initial wordwrap.
		// We set the cut flag to FALSE so that any individual words that are
		// too long get left alone. In the next step we'll deal with them.
		#$str = wordwrap($str, $charlim, "\n", FALSE);
		$str = chunk_split($str, $charlim, "\n");

		// Split the string into individual lines of text and cycle through them
		$output = '';
		foreach (explode("\n", $str) as $line)
		{
			// Is the line within the allowed character count?
			// If so we'll join it to the output and continue
			if (mb_strlen($line) <= $charlim)
			{
				$output .= $line.$this->newline;
				continue;
			}

			$temp = '';
			do
			{
				// If the over-length word is a URL we won't wrap it
				if (preg_match('!\[url.+\]|://|www\.!', $line))
				{
					break;
				}

				// Trim the word down
				$temp .= mb_substr($line, 0, $charlim - 1);
				$line = mb_substr($line, $charlim - 1);
			}
			while (mb_strlen($line) > $charlim);

			// If $temp contains data it means we had to split up an over-length
			// word into smaller chunks so we'll add it back to our current line
			if ($temp !== '')
			{
				$output .= $temp.$this->newline;
			}

			$output .= $line.$this->newline;
		}

		// Put our markers back
		if (count($unwrap) > 0)
		{
			foreach ($unwrap as $key => $val)
			{
				$output = str_replace('{{unwrapped'.$key.'}}', $val, $output);
			}
		}

		return $output;
	}
}