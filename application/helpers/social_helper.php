<?php

	function extractTwitterUN($url)
	{
		$result = preg_match("|https?://(www\.)?twitter\.com/(#!/)?@?([^/]*)|", $url, $matches);
		return ($result == 1)? $matches[3] : false;
	}

	function extractFacebookUN($url)
	{
		$result = preg_match("|https?://(www\.)?facebook\.com/(#!/)?@?([^/]*)|", $url, $matches);
		return ($result == 1)? $matches[3] : false;
	}

	function extractPinterestUN($url)
	{
		$result = preg_match("|https?://(www\.)?pinterest\.com/(#!/)?@?([^/]*)|", $url, $matches);
		return ($result == 1)? $matches[3] : false;
	}

	function extractInstagramUN($url)
	{
		$result = preg_match("|https?://(www\.)?instagram\.com/(#!/)?@?([^/]*)|", $url, $matches);
		return ($result == 1)? $matches[3] : false;
	}

