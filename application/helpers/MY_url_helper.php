<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Extending form helper to add ReCaptcha
 */
 if ( ! function_exists('full_site_url'))
{
	function full_site_url() {

		$url = 'http';
		$url .= (($_SERVER['SERVER_PORT'] == 443 || (!empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on')))? 's://' . $_SERVER['HTTP_HOST'] : '://' . $_SERVER['HTTP_HOST'];
		return $url;
	}
}