<?php
/**
* Returns city/state, lat/lon for the visitors ip
*/
function getDefaultLocation($apiKey = null)
{
	$ipaddress = get_ip_address();
	
	$query = 'http://geoip.maxmind.com/b?l=' . $apiKey . '&i=' . $ipaddress;
	$url = parse_url($query);
	$host = $url['host'];
	$path = $url['path'] . '?' . $url['query'];
	$timeout = 1;
	$fp = fsockopen ($host, 80, $errno, $errstr, $timeout) or die('Can not open connection to server.');
	$buf = '';
	
	if ($fp) 
	{
		fputs ($fp, "GET $path HTTP/1.0\nHost: " . $host . "\n\n");
		
		while (!feof($fp)) 
		{
			$buf .= fgets($fp, 128);
		}
		$lines = explode("\n", $buf);
		$data = $lines[count($lines)-1];
		fclose($fp);
	} else {
	  # enter error handing code here
	}
	
	$geo = explode(',',$data);
	
	if ($geo[0] != '' && $geo[1] != '' && $geo[2] != '')
	{
		$location = array('country' => $geo[0],
						  'state' => $geo[1],
						  'city' => $geo[2],
						  'lat' => $geo[3],
						  'lon' => $geo[4]
						);					 
	} else {
		$location = false;
	}
	
	return $location;
}

function get_ip_address() {
	foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key) {
		if (array_key_exists($key, $_SERVER) === true) {
			foreach (explode(',', $_SERVER[$key]) as $ip) {
				if (filter_var($ip, FILTER_VALIDATE_IP) !== false) {
					return $ip;
				}
			}
		}
	}
}