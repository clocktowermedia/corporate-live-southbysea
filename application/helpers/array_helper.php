<?php
	function format_key($key = '')
	{
		$replace = array('_', '-');
		$key = ucwords($key);
		$key = preg_replace("/(([a-z])([A-Z])|([A-Z])([A-Z][a-z]))/","\\2\\4 \\3\\5", $key);
		$key = str_replace($replace, '', $key);
		return $key;
	}

	/**
	 * Removes a specified value from an array
	 * @param  [type] $array [description]
	 * @param  [type] $val   [description]
	 * @return [type]		[description]
	 */
	function recursiveRemoval(&$array, $val)
	{
		if (is_array($array)) 
		{
			foreach ($array as $key => &$arrayElement)
			{
				if (is_array($arrayElement))
				{
					recursiveRemoval($arrayElement, $val);
				} else {

					if ($arrayElement == $val)
					{
						unset($array[$key]);
					}
				}
			}
		}
	}

	/**
	 * Same as array search...but runs recursively
	 * @param  [type] $needle   [description]
	 * @param  [type] $haystack [description]
	 * @return [type]		   [description]
	 */
	function recursive_array_search($needle, $haystack) {
		foreach ($haystack as $key => $value) {
			$current_key = $key;
			if ($needle === $value || (is_array($value) && recursive_array_search($needle,$value) !== false)) 
			{
				return $current_key;
			}
		}
		return false;
	}

	/**
	 * Remove specified keys from array
	 * @param  array  $input [original array]
	 * @param  [array or string] $keys  [keys to unset]
	 * @return array
	 */
	function array_unset_keys(array $input, $keys) {
		if (!is_array($keys))
		{
			$keys = array($keys => 0);
		} else {
			$keys = array_flip($keys);
		}

		return array_diff_key($input, $keys);
	}

	/**
	* This function uses one array for keys, and one array for values - and creates a merged array
	*/
	function array_combine_special($arr1, $arr2) {
		$out = array();

		foreach($arr1 as $key1 => $value1) {
			if (isset($arr2[$key1]))
			{
				$out[(string)$value1] = $arr2[$key1];
			} else {
				$out[(string)$value1] = '';
			}
		}

		return $out;
	}

	/**
	* Basically flattens an array for a specified key
	*/
	function array_pluck($arr, $toPluck) {
	    return array_map(function ($item) use ($toPluck) {
	        return $item[$toPluck];
	    }, $arr);
	}

	/**
	 * Insert item after specified key
	 */
	function array_splice_after_key($array, $key, $array_to_insert)
	{
	    $key_pos = array_search($key, array_keys($array));
	    if($key_pos !== false){
	        $key_pos++;
	        $second_array = array_splice($array, $key_pos);
	        $array = array_merge($array, $array_to_insert, $second_array);
	    }
	    return $array;
	}