<?php
class Admin extends Admin_controller {

	//layout variables
	public $defaultLayout;

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//set layout variable(s)
		$this->defaultLayout = $this->get_admin_layout();

		//load all models required
		$this->load->model('potentials/potential_model');
	}

	/**
	 * View for listing all potentials
	 */
	function index()
	{
		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Potentials';

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}

	/**
	 * View and action for creating a potential, just title for now
	 */
	function create()
	{
		//tell it what view file to use
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Create Potential';

		//load form validation library
		$this->load->library('form_validation');

		//get validation rules from model, set extra options as neccessary
		$validationRules = $this->potential_model->validate;

		//set form validation rules
		$this->form_validation->set_rules($validationRules);

		//if the form was submitted, update database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the design and show form errors
				$data['potential'] = $this->input->post(NULL, TRUE);
				$this->load->view($this->defaultLayout, $data);

			} else {

				//insert potential
				$potentialID = $this->potential_model->skip_validation()->insert($this->input->post(NULL, TRUE));

				//display success message & redirect
				//$this->session->set_flashdata('success', 'Potential successfully created. You may continue editing your potential below.');
				redirect('/admin/potentials/edit/' . $potentialID);
			}
		} else {
			// tell it what layout to use and load the design
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	 * View and action for editing a potential, TEMPORARY
	 * @param  integer $potentialID
	 */
	function edit($potentialID = 0)
	{
		//make sure potential is valid
		$potentialID = (int) $potentialID;
		$this->_potential_is_valid($potentialID);

		//get potential information
		$data['potential'] = $this->potential_model->get($potentialID);

		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Potentials';

		//get list of designers for dropdown
		$this->load->model('admins/admin_model');
		$data['dropdowns']['designers'] = $this->admin_model->getRoleDropdown('Designer');

		//send to view
		$this->load->model('forms/form_option_model');
		$data['formOptions'] = $this->form_option_model->getByFormName('potential');

		//load form validation library & set rules
		$this->load->library('form_validation');
		$validationRules = $this->potential_model->validate;
		$this->form_validation->set_rules($validationRules);

		//if the form was submitted, edit in DB
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the page and show form errors
				$this->load->view($this->defaultLayout, $data);
			} else {

				//update user information in db
				$this->potential_model->update($potentialID, $this->input->post(NULL, TRUE));

				//redirect and show success message
				$this->session->set_flashdata('success', "Potential information was updated.");
				redirect('/admin/potentials/');
			}
		} else {
			// tell it what layout to use and load the page
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

	/**
	* Checks to see that potential ID is valid and that the potential exists
	*/
	public function _potential_is_valid($potentialID = 0)
	{
		//make sure potential id is an integer
		$potentialID = (int) $potentialID;

		//make sure potential id is greater than zero
		if ($potentialID > 0)
		{
			//check to see if the potential exists
			$exists = $this->potential_model->get($potentialID);

			if ($exists == false)
			{
				$this->session->set_flashdata('error', 'Invalid potential id.');
				redirect('/admin/orders');
			}

		} else {
			$this->session->set_flashdata('error', 'Invalid potential id.');
			redirect('/admin/orders');
		}
	}
}