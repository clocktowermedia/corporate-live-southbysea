<?php
class Admin_json extends Admin_controller {

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		$this->load->helper('inflector');
		$this->load->model('potentials/potential_model');
	}

	function add($type = 'contact', $potentialID = 0)
	{
		try
		{
			//make sure we are posting
			if (!$this->is_post())
			{
				throw new Exception('Must post data.');
			}

			//set potential id to int
			$potentialID = ($potentialID > 0)? $potentialID : (int) $this->input->post('potentialID', TRUE);

			//make sure potential is found
			modules::run('potentials/admin/_potential_is_valid', $potentialID);

			//load appropriate model, make sure it exists
			$modelName = $this->_check_type($type);

			//run validation
			$this->load->library('form_validation');
			$validationRules = $this->{$modelName}->validate;
			unset($validationRules['potentialID']);
			$this->form_validation->set_rules($validationRules);
			if ($this->form_validation->run() == FALSE)
			{
				throw new Exception(validation_errors());
			}

			//try to insert item
			$itemID = $this->{$modelName}->insert($this->input->post(NULL, TRUE) + array('createdBy' => $this->adminID, 'potentialID' => $potentialID));

			if (is_int($itemID) && $itemID > 0)
			{
				//if item addition was successful, return the id
				$data['status'] = 'ok';
				$data['itemID'] = $itemID;
				$data['message'] = 'Successfully added ' . $type . '.';
			} else {
				throw new Exception($itemID);
			}

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = strip_tags($e->getMessage());
		}

		$this->render_json($data);
	}

	function edit($type = 'contact', $itemID = 0)
	{
		try
		{
			//make sure we are posting
			if (!$this->is_post())
			{
				throw new Exception('Must post data.');
			}

			//load model
			$modelName = $this->_check_type($type);

			//make sure item is found
			$item = $this->{$modelName}->get((int) $itemID);
			if ($item == false || empty($item))
			{
				throw new Exception('Could not find that ' . $type . '.');
			}

			//make sure only the owner can edit it, if applicable
			if ($this->{$modelName}->owner_only == true && $item['createdBy'] != (int) $this->session->userdata('adminID'))
			{
				throw new Exception('You do not have permission to do this.');
			}

			//run validation
			$this->load->library('form_validation');
			$validationRules = $this->{$modelName}->validate;
			unset($validationRules['potentialID']);
			$this->form_validation->set_rules($validationRules);
			if ($this->form_validation->run() == FALSE)
			{
				throw new Exception(validation_errors());
			}

			//try to update item
			$this->{$modelName}->skip_validation()->update((int) $itemID, $this->input->post(NULL, TRUE));

			//if successful, return success message
			$data['status'] = 'ok';
			$data['message'] = 'Successfully updated ' . $type . '.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = strip_tags($e->getMessage());
		}

		$this->render_json($data);
	}

	function delete($type = 'contact', $itemID = 0)
	{
		try
		{
			//make sure we are getting
			if (!$this->is_get())
			{
				throw new Exception('Must pass data.');
			}

			//load model
			$modelName = $this->_check_type($type);

			//make sure item is found
			$item = $this->{$modelName}->get((int) $itemID);
			if ($item == false || empty($item))
			{
				throw new Exception('Could not find that ' . $type . '.');
			}

			//make sure only the owner can delete it, if applicable
			if ($this->{$modelName}->owner_only == true && $item['createdBy'] != (int) $this->session->userdata('adminID'))
			{
				throw new Exception('You do not have permission to do this.');
			}

			//try to delete item
			$this->{$modelName}->skip_validation()->delete((int) $itemID);

			//if successful, return success message
			$data['status'] = 'ok';
			$data['message'] = 'Successfully deleted ' . $type . '.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = strip_tags($e->getMessage());
		}

		$this->render_json($data);
	}

	function update_status($potentialID = 0)
	{
		try
		{
			//make sure we are posting
			if (!$this->is_post())
			{
				throw new Exception('Must post data.');
			}

			//make sure we have all the vields we need
			if ($this->input->post('status', TRUE) == '' || $this->input->post('type', TRUE) == '' || $this->input->post('items', TRUE) == '')
			{
				throw new Exception('Invalid request.');
			}

			//set potential id to int
			$potentialID = ($potentialID > 0)? $potentialID : (int) $this->input->post('potentialID', TRUE);

			//make sure potential is found
			modules::run('potentials/admin/_potential_is_valid', $potentialID);

			//load appropriate model, make sure it exists
			$type = $this->input->post('type', TRUE);
			$modelName = $this->_check_type($type);

			//get items & status
			$items = $this->input->post('items', TRUE);
			$status = $this->input->post('status', TRUE);

			//determine what needs to be deleted or updated
			if (is_array($items))
			{
				if (count($items) <= 0)
				{
					throw new Exception('Must select items.');
				}

				foreach ($items AS $itemID)
				{
					if ($status == 'delete')
					{
						$this->{$modelName}->delete($itemID);
					} else {
						$this->{$modelName}->skip_validation()->update($itemID, array('status' => $status));
					}
				}
			} else {

				//delete all
				if ($status == 'delete')
				{
					$this->{$modelName}->delete_by('potentialID', $potentialID);
				} else {
					$this->{$modelName}->update_by('potentialID', $potentialID, array('status' => $status));
				}
			}

			//return status
			$data['status'] = 'ok';
			$data['message'] = 'Successfully updated items.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}

	function data($type = 'contacts', $potentialID = 0)
	{
		try
		{
			//set potential id to int
			$potentialID = ($potentialID > 0)? $potentialID : (int) $this->input->get('potentialID', TRUE);

			//make sure potential is found
			modules::run('potentials/admin/_potential_is_valid', $potentialID);

			//load model
			$type = singular($type);
			$modelName = $this->_check_type($type);

			//set order by
			$orderBy['name'] = ($this->input->get('sort-field', TRUE) == '')? 'dateCreated' : $this->input->get('sort-field', TRUE);
			$orderBy['order'] = ($this->input->get('sort', TRUE) == '')? 'ASC' :  $this->input->get('sort', TRUE);

			//get data from model
			$items = $this->{$modelName}->append('user')->append('attachments')->order_by($orderBy['name'], $orderBy['order'])->get_many_by('potentialID', $potentialID);
			if ($items != false && count($items) > 0)
			{
				$data['items'] = $items;
			}

			//return status
			$data['status'] = 'ok';
			$data['message'] = 'Successfully retreived items.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}

	function upload($type = 'attachment', $potentialID = 0)
	{
		try
		{
			//make sure we are posting
			if (!$this->is_post())
			{
				throw new Exception('Must post data.');
			}

			//load parent model, check to make sure relation exists and parent is found
			$modelName = $this->_check_type($type);
			$parentModel = $this->_check_parent($modelName);

			//get link field name
			$parentLink = (isset($this->{$parentModel}->has_many['attachments']['join_key']))? $this->{$parentModel}->has_many['attachments']['join_key'] : false;
			if ($parentLink == false)
			{
				throw new Exception('Invalid attachment type.');
			}

			//make sure parent id was passed
			$parentID = (int) $this->input->post($parentLink, TRUE);
			if ($parentID <= 0)
			{
				throw new Exception('Parent ID must be passed.');
			}

			//make sure parent exists
			$parent = $this->{$parentModel}->get($parentID);
			if (empty($parent) || $parent == false)
			{
				throw new Exception('Parent not found.');
			}

			//try to upload the file
			$this->load->library('uploader');
			$this->uploader->set_model('potentials/' . $modelName);
			$this->uploader->set_upload_paths(array('uploadFolder' => '/assets/uploads/potentials/' . plural($type) . '/'));

			//upload image
			if (isset($_FILES['userfile']['name']) && $_FILES['userfile']['name'] != '')
			{
				$fileID = $this->uploader->upload($this->input->post(NULL, TRUE) + array('createdBy' => $this->adminID));

				if (is_int($fileID) && $fileID > 0)
				{
					//if file upload was successful, return the file id
					$data['status'] = 'ok';
					$data['fileID'] = $fileID;
					$data['message'] = 'Successfully uploaded file.';
				} else {
					throw new Exception($fileID);
				}
			}

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = strip_tags($e->getMessage());
		}

		$this->render_json($data);
	}

	function get_expense_total($potentialID = 0)
	{
		//set potential id to int
		$potentialID = ($potentialID > 0)? $potentialID : (int) $this->input->post('potentialID', TRUE);

		//make sure potential is found
		modules::run('potentials/admin/_potential_is_valid', $potentialID);

		//get model we need and grab total
		$this->load->model('potentials/potential_expense_model');
		$data['total'] = $this->potential_expense_model->getSum($potentialID);
		$data['message'] = 'Successfully retreived total.';
		$data['status'] = 'ok';

		$this->render_json($data);
	}

	function edit_field($type = '', $itemID = 0)
	{
		try
		{
			//make sure we are getting
			if (!$this->is_post())
			{
				throw new Exception('Must post data.');
			}

			//make sure an id was passed
			$itemID = ((int) $itemID <= 0)? (int) $this->input->post('id', TRUE) : (int) $itemID;
			if ($itemID <= 0)
			{
				throw new Exception('Invalid item id.');
			}

			//load model
			if ($type == '')
			{
				$modelName = 'potential_model';
			} else {
				$modelName = $this->_check_type($type);
			}
			$this->load->model($modelName);

			//get item and make sure it's found
			$item = $this->{$modelName}->get($itemID);
			if (empty($item) || $item == false)
			{
				throw new Exception('Item not found.');
			}

			//update item
			if ($this->input->post('value', TRUE) != $item[(string) $this->input->post('name', TRUE)])
			{
				$updateData[(string) $this->input->post('name', TRUE)] = $this->input->post('value', TRUE);
				$this->{$modelName}->skip_validation()->update($itemID, $updateData);
			}

			//return status
			$data['status'] = 'ok';
			$data['message'] = 'Successfully updated.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}

	public function report_fields($reportID = 0, $format = 'tree')
	{
		if ($format == 'tree')
		{
			//load model
			$this->load->model('potentials/reporting/potential_section_model');

			if ($reportID > 0)
			{
				$this->load->helper('array');
				$this->load->model('potentials/reporting/potential_report_field_model');
				$selected = $this->potential_report_field_model->where('potentialReportTypeID', $reportID)->dropdown('potentialFieldID', 'potentialFieldID');
			} else {
				$selected = array();
			}

			//get sections and render to view
			$data = $this->potential_section_model->get_all_section_fields(0, $selected);
		} else {

			if ($reportID > 0)
			{
				$this->load->model('potentials/reporting/potential_report_field_model');
				$data['fields'] = $this->potential_report_field_model->with('field')->order_by('displayOrder', 'asc')->get_many_by('potentialReportTypeID', $reportID);
			} else {
				$data = array();
			}
		}

		$this->render_json($data);
	}

	public function report_fields_by_id()
	{
		$this->load->model('potentials/reporting/potential_field_model');

		if (count($this->input->get('ids')) > 0)
		{
			$data['fields'] = $this->potential_field_model->get_many($this->input->get('ids', TRUE));
		} else {
			$data['fields'] = $this->potential_field_model->get_all();
		}

		$this->render_json($data);
	}

	public function save_report_fields($reportID = 0)
	{
		try {

			//make sure we have a valid request type
			if (!$this->is_post())
			{
				throw new Exception('Invalid request type.');
			}

			//make sure some fields were actually passed
			if (!is_array($this->input->post('fields')) || count($this->input->post('fields')) <= 0)
			{
				throw new Exception('Must select fields to display in report.');
			}

			//load the models we need
			$this->load->model('potentials/reporting/potential_report_type_model');
			$this->load->model('potentials/reporting/potential_report_field_model');

			//try to add a new report, else update existing report
			$reportID = (int) $reportID;
			if ($reportID <= 0)
			{
				//create report
				$reportID = $this->potential_report_type_model->insert(array('title' => $this->input->post('title', TRUE)));

				//make sure report creation worked
				if ((int) $reportID <= 0)
				{
					throw new Exception('An error occurred, please try again.');
				}

				//add fields to report
				foreach ($this->input->post('fields', TRUE) AS $i => $value)
				{
					$this->potential_report_field_model->skip_validation()->insert(array('potentialFieldID' => $value, 'potentialReportTypeID' => $reportID, 'displayOrder' => $i));
				}

				//success msg
				$data['status'] = 'ok';
				$data['message'] = 'Successfully created report.';
				$data['redirect'] = '/admin/potentials/reporting';
				$this->session->set_flashdata('success', $data['message']);
			} else {
				//make sure report is valid and exists
				$reportExists = $this->potential_report_type_model->count_by('potentialReportTypeID', $reportID);
				if ($reportExists <= 0)
				{
					throw new Exception('Invalid report id.');
				}

				//update report settings
				$this->potential_report_type_model->update($reportID, array('title' => $this->input->post('title', TRUE)));

				//delete all old fields, repopulate with selected ones
				$this->potential_report_field_model->delete_by('potentialReportTypeID', $reportID);
				foreach ($this->input->post('fields', TRUE) AS $i => $value)
				{
					$this->potential_report_field_model->skip_validation()->insert(array('potentialFieldID' => $value, 'potentialReportTypeID' => $reportID, 'displayOrder' => $i));				}

				//success message
				$data['status'] = 'ok';
				$data['message'] = 'Successfully updated report.';
				//$data['redirect'] = '/admin/potentials/reporting';
				$this->session->set_flashdata('success', $data['message']);
			}

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/
	public function _check_type($type = '')
	{
		try {
			//set model name & attempt loading
			$type = str_replace('-', '_', $type);
			$modelName = 'potential_' . $type . '_model';
			$this->load->model('potentials/' . $modelName);

			//if no error occurred, return
			return $modelName;

		} catch (Exception $e) {
			die('Invalid item type.');
		}
	}

	public function _check_parent($modelName = '', $return = false)
	{
		try {
			if (isset($this->{$modelName}->belongs_to) && count($this->{$modelName}->belongs_to) > 0)
			{
				$relationship = key($this->{$modelName}->belongs_to);
				$parentModel = $this->{$modelName}->belongs_to[$relationship]['model'];
				$this->load->model($parentModel);
				return basename($parentModel);
			} else {
				throw new Exception();
			}

		} catch (Exception $e) {
			if ($return == true)
			{
				return false;
			} else {
				die('Invalid parent type.');
			}
		}
	}
}