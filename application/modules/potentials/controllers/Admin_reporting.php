<?php
class Admin_reporting extends Admin_controller {

	//layout variables
	public $defaultLayout;

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//set layout variable(s)
		$this->defaultLayout = $this->get_admin_layout();

		//load some models
		$this->load->model('potentials/reporting/potential_report_type_model');
		$this->load->model('potentials/reporting/potential_report_field_model');

		//load helper
		$this->load->helper('string');
	}

	/**
	* Shows the form for generating a report
	*/
	function index()
	{
		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->_get_custom_view();
		$data['title'] = 'Generate Report';

		//get list of reports
		$data['reports'] = $this->potential_report_type_model->dropdown('url_slug', 'title');

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}

	/**
	* View for creating a new report type
	*/
	function create()
	{
		//send data to view
		$data['title'] = 'Create Report';
		$data['view'] = $this->_get_custom_view();

		// tell it what layout to use and load the page
		$this->load->view($this->defaultLayout, $data);
	}

	/**
	* View for updating a report type
	*/
	function edit($reportID = 0)
	{
		//make sure report exists
		$this->_report_is_valid($reportID);

		//send data to view
		$data['title'] = 'Create Report';
		$data['view'] = $this->_get_custom_view();
		$data['report'] = $this->potential_report_type_model->get($reportID);

		// tell it what layout to use and load the page
		$this->load->view($this->defaultLayout, $data);
	}

	/**
	 * Takes post data and generates a report
	 */
	function generate($output = null)
	{
		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->_get_custom_view();
		$data['title'] = 'Generate Report';

		//get list of reports
		$data['reports'] = $this->potential_report_type_model->dropdown('url_slug', 'title');

		//make sure we have passed the proper fields
		$this->load->library('form_validation');
		$this->form_validation->set_rules('reportType', 'Report', 'required');

		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//throw back invalid status
				$data['status'] = 'fail';
				$data['message'] = 'Report type not passed';
			} else {

				//send information from post to view
				$data['postData'] = $this->input->post(NULL, TRUE);
				$data['generateUrl'] = '/admin/potentials/reporting/generate';

				//get some of the other values passed in the form
				$reportType = $data['postData']['reportType'] = $this->input->post('reportType', TRUE);

				//get report title
				$data['reportTitle'] = $this->_get_report_title($data['postData']);

				//get report
				$data = $this->_get_report_data($data);

				//determine if we are displaying a page or outputing to a pdf/csv
				if ($output == 'csv')
				{
					$this->_csv($data);

				} else if ($output == 'pdf') {

					$this->_pdf($data);

				} else {

					// tell it what layout to use and load
					$this->load->view($this->defaultLayout, $data);
				}
			}
		} else {
			// tell it what layout to use and load
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	 * Gets the report data from model
	 * @param  array $data [data posted from form on what kind of report we want]
	 * @return array
	 */
	function _get_report_data($data = null)
	{
		//get the headings for the report
		$reportType = $this->potential_report_type_model->append('fields')->get_by(array('url_slug' => $data['postData']['reportType']));

		//build headings
		if (count($reportType['fields']) > 0)
		{
			$this->load->model('potentials/reporting/potential_field_model');
			foreach ($reportType['fields'] AS $field)
			{
				$data['headings'][] = $this->potential_field_model->select('text, value, fieldType')->get($field['potentialFieldID']);
			}
		}

		//where parameters
		$params = $data['postData'];
		unset($params['reportType']);

		/*
		//unset headers we dont need & move appropriate header to front
		$data['headings'] = $this->_unset_headings($data['headings'], $params);
		if (isset($params['groupBy']))
		{
			//if 'all' was selected for group type that means there is no grouping..but needed a value there for front end form validation
			if ($params['groupBy'] == 'all') { $params['groupBy'] = ''; }

			$data['headings'] = $this->_add_group_by($data['headings'], $params['groupBy']);

			//if we already are filtering by the same thing we are grouping by, remove it
			$groupByPlain = remove_alias($params['groupBy']);
			if (isset($data['postData'][$groupByPlain]) && $data['postData'][$groupByPlain] != '')
			{
				unset($data['headings'][0]);
			}
		}
		*/

		//assume the model/report type for now
		$modelName = 'potentials/potential_model';
		$functionName = 'listingReport';

		//load the appropriate model & get data
		$this->load->model($modelName, 'report_generated_model');
		$data['reportData'] = $this->report_generated_model->call_custom_function($functionName, $params);
		$data['reportView'] = 'layouts/data';

		//return data
		return $data;
	}

	public function _csv(array $data = array())
	{
		//load the csv library
		$this->load->library('array_to_csv');
		$this->load->helper('array');

		//get headings in a one dimensional array
		$headingValues = array_pluck($data['headings'], 'value');
		$headingTitles = array_pluck($data['headings'], 'text');
		$csvHeadings = array_combine_special($headingValues, $headingTitles);

		//set the headings
		$this->array_to_csv->setHeading($csvHeadings);

		//get the data in a format that is good for csv
		$csvArray = array();

		//loop through all the headings, get the matching data set, and add it to the csv
		if (!empty($data['reportData']) && $data['reportData'] != false)
		{
			foreach ($data['reportData'] as $key => $form)
			{
				foreach ($csvHeadings as $headingKey => $value)
				{
					$encoding = mb_detect_encoding($form[$headingKey]);
					$csvArray[$key][$headingKey] = iconv($encoding, "ISO-8859-1//TRANSLIT", $form[$headingKey]);
				}
			}
		}

		//add data to csv
		if (count($csvArray) > 1)
		{
			foreach ($csvArray as $lineData)
			{
				$this->array_to_csv->addLine($lineData);
			}
		} else if (count($csvArray) > 0) {
			$this->array_to_csv->addLine($csvArray[0]);
		}

		// output the csv
		$this->array_to_csv->output('D', url_title($data['reportTitle'], '_') . '.csv');

		// clear the buffer
		$this->array_to_csv->clear();
		exit;
	}

	public function _pdf(array $data = array())
	{
		//grab the html from the partial
		$data['pdfOutput'] = true;
		$html = $this->load->view('admin/reporting/' . $data['reportView'], $data, true) . '</body></html>';

		//load pdf class
		require_once('application/libraries/mpdf/mpdf.php');
		$mpdf = new mPDF();
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->WriteHTML($html);

		//set the filename and output
		$mpdf->Output(url_title($data['reportTitle'], '_') . '.pdf', 'D');

		/*
		//spit out a pdf, we will worry about styling later...
		require_once('application/libraries/dompdf/dompdf_config.inc.php');
		spl_autoload_register('DOMPDF_autoload');
		$domPDF = new DOMPDF();
		$domPDF->load_html($html);
		$domPDF->render();

		//set the file name
		$domPDF->stream(url_title($data['reportTitle'], '_') . '.pdf');
		*/

		exit;
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

	public function _get_custom_view()
	{
		return '/admin/reporting/' . $this->get_view();
	}

	public function _report_is_valid($reportID = 0)
	{
		//make sure report id is an integer
		$reportID = (int) $reportID;

		//make sure report id is greater than zero
		if ($reportID > 0)
		{
			//check to see if the report exists
			$exists = $this->potential_report_type_model->get($reportID);

			if ($exists == false)
			{
				$this->session->set_flashdata('error', 'Invalid report id.');
				redirect('/admin/potentials/reporting');
			}

		} else {
			$this->session->set_flashdata('error', 'Invalid report id.');
			redirect('/admin/potentials/reporting');
		}
	}

	//if they filtered by an option, they don't need to show it anymore
	//(ie: if someone chose hawaii, they dont need a column telling them that)
	private function _unset_headings(array $headings = array(), array $filters = array()) {
		if (count($filters) > 0 && count($headings) > 0)
		{
			//load array helper
			$this->load->helper('array');

			foreach ($filters as $filterName => $filterValue)
			{
				if ($filterName != 'groupBy' && ((is_int($filterValue) && $filterValue > 0) || $filterValue != '' ))
				{
					//remove if it exists
					$key = recursive_array_search($filterName, $headings);
					$key2 = recursive_array_search(array_search($filterName, $this->relatedFilters), $headings);
					$key3 = recursive_array_search(array_search($filterName, $this->relatedFiltersPlain), $headings);
					if ($key !== false) { unset($headings[$key]); }
					if ($key2 !== false) { unset($headings[$key2]); }
					if ($key3 !== false) { unset($headings[$key3]); }
				}
			}
		}
		return $headings;
	}

	//move group by selection to be the first column
	private function _add_group_by(array $headings = array(), $groupBy = null)
	{
		if (count($headings) > 0 && $groupBy != '')
		{
			//determine if field already exists or if we have to add it
			$key = recursive_array_search($groupBy, $headings);
			$key2 = recursive_array_search(array_search($groupBy, $this->relatedFilters), $headings);
			$key3 = recursive_array_search(array_search($groupBy, $this->relatedFiltersPlain), $headings);
			if ($key !== false) { $item = $headings[$key]; unset($headings[$key]); }
			if ($key2 !== false){ $item = $headings[$key2]; unset($headings[$key2]); }
			if ($key3 !== false){ $item = $headings[$key3]; unset($headings[$key3]); }

			//if item does not exist, create it
			if (!isset($item))
			{
				//get all custom group options, so that we can determine what the label should be
				$dropdowns = modules::run('reporting/admin/_get_dropdowns');

				//get field value
				if (isset($this->relatedFilters[$groupBy]))
				{
					$fieldValue = $this->relatedFilters[$groupBy];
				} else if (isset($this->relatedFiltersPlain[$groupBy])) {
					$fieldValue = $this->relatedFiltersPlain[$groupBy];
				} else {
					$fieldValue = $groupBy;
				}

				$item = array(
					'value' => $fieldValue,
					'text' => str_replace('By ', '', $dropdowns['allGroupByOpts'][$groupBy])
				);
			}

			//add heading to front
			$item['value'] = remove_alias($item['value']); //alias is used for grouping items only, can remove it now
			array_unshift($headings, $item);
		}
		return $headings;
	}

	//get report title
	function _get_report_title($postData = null)
	{
		//get some of the other values passed in the form
		$reportType = $postData['reportType'] = $this->input->post('reportType', TRUE);

		//get the report title(s)
		$reportType = $this->potential_report_type_model->get_by(array('url_slug' => $reportType));
		if ($reportType['parentID'] > 0)
		{
			//get parent
			$parentReport = $this->potential_report_type_model->get($reportType['parentID']);
			$typeTitle = ($parentReport != false && !empty($parentReport))? $parentReport['text'] . ' - ' : '';
		} else {
			$typeTitle = '';
		}

		return ucwords($typeTitle . ' ' . $reportType['title']) . ' Report';
	}
}