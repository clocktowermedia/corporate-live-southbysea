<?php
class Admin_modals extends Admin_controller {

	//all actions should call this function
	function __construct()
	{
		parent::__construct();
	}

	/**
	* Add view
	*/
	function add($type = 'contact', $itemID = 0)
	{
		//make sure potential id is valid and type exists
		$modelName = modules::run('potentials/admin_json/_check_type', $type);
		$parentModel = modules::run('potentials/admin_json/_check_parent', $modelName, true);
		if ($parentModel == false)
		{
			modules::run('potentials/admin/_potential_is_valid', $itemID);
		}

		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->_get_custom_view($type);
		$data['action'] = 'add';
		$data['itemID'] = (int) $itemID;

		//send to view
		$this->load->model('forms/form_option_model');
		$data['formOptions'] = $this->form_option_model->getByFormName('potential');

		// tell it what layout to use and load
		$this->load->view($data['view'], $data);
	}

	/**
	 * Edit View
	 */
	function edit($type = 'contact', $itemID = 0)
	{
		//get item information, start by making sure type exists
		$modelName = modules::run('potentials/admin_json/_check_type', $type);
		$data['item'] = $this->{$modelName}->get((int) $itemID);

		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->_get_custom_view($type);
		$data['action'] = 'edit';
		$data['itemID'] = (int) $itemID;

		//throw exception if item not found
		if ($data['item'] == false || empty($data['item']))
		{
			throw new Exception('Could not find that ' . $type . '.');
		}

		//send to view
		$this->load->model('forms/form_option_model');
		$data['formOptions'] = $this->form_option_model->getByFormName('potential');

		// tell it what layout to use and load
		$this->load->view($data['view'], $data);
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/
	private function _get_custom_view($type = '')
	{
		return '/admin/modals/' . $type;
	}
}