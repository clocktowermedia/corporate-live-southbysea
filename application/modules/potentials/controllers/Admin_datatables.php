<?php
class Admin_datatables extends Admin_controller {

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		$this->load->library('datatables');
	}

	/**
	* uses datatables library to get list of potentials
	*/
	function data($return = false)
	{
		//grab data from the users table
		$output = $this->datatables->select('p.potentialID, p.title, p.priority, p.stage')
			->add_column('actions',  '<div class="btn-group">
					<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-list-alt"></i> Potential <span class="caret"></span>
					</button>
					<ul class="dropdown-menu">
						<!--<li><a href="/admin/potentials/view/$1"><i class="fa fa-eye"></i> View</a></li>-->
						<li><a href="/admin/potentials/edit/$1"><i class="fa fa-edit"></i> Edit</a></li>
						<li><a data-confirm="Are you sure you want to delete this?" href="/admin/potentials/delete/$1"><i class="fa fa-trash-o"></i> Delete</a></li>
					</ul>
				</div>', 'p.potentialID')
			->from('Potentials p')
			#->join('Institutions i', 'u.institutionID = i.institutionID', 'left outer')
			#->where('status !=', 'deleted')
			->generate();

		//return or echo the data
		if ($return == true)
		{
			return $output;
		}

		echo $output;
	}

	function reports()
	{
		//grab data from the users table
		$output = $this->datatables->select('r.potentialReportTypeID, r.title')
			->add_column('actions',  '<div class="btn-group">
					<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-bar-chart-o"></i> Report <span class="caret"></span>
					</button>
					<ul class="dropdown-menu">
						<li><a href="/admin/potentials/reporting/edit/$1"><i class="fa fa-edit"></i> Edit</a></li>
						<!--<li><a data-confirm="Are you sure you want to delete this?" href="/admin/potentials/reporting/delete/$1"><i class="fa fa-trash-o"></i> Delete</a></li>-->
					</ul>
				</div>', 'r.potentialReportTypeID')
			->unset_column('r.potentialReportTypeID')
			->from('PotentialReportTypes r')
			->generate();

		echo $output;
	}

	/**
	* uses datatables library to get list of potential contacts
	*/
	function contact($potentialID = 0, $return = false)
	{
		$this->datatables->select('pc.potContactID')
			->select('"" AS checkbox', false)
			->edit_column('checkbox', '<input type="checkbox" class="contact-checkbox row-checkbox" value="$1" name="items[]" data-form="contact">', 'pc.potContactID')
			->select('pc.firstName, pc.lastName, pc.email, pc.phone, pc.role')
			->unset_column('pc.potContactID')
			->from('PotentialContacts pc');

		if ($potentialID > 0)
		{
			$this->datatables->where('pc.potentialID', $potentialID);
		}

		$output = $this->datatables->generate();

		//return or echo the data
		if ($return == true)
		{
			return $output;
		}

		echo $output;
	}

	/**
	* uses datatables library to get list of potential expenses
	*/
	function expense($potentialID = 0, $return = false)
	{
		$this->datatables->select('"" AS checkbox', false)
			->edit_column('checkbox', '<input type="checkbox" class="contact-checkbox row-checkbox" value="$1" name="items[]" data-form="expense">', 'pe.potExpenseID')
			->select('pe.title, pe.label, pe.value')
			->select('pe.potExpenseID')
			//->unset_column('pe.potExpenseID')
			->from('PotentialExpenses pe');

		if ($potentialID > 0)
		{
			$this->datatables->where('pe.potentialID', $potentialID);
		}

		$output = $this->datatables->generate();

		//return or echo the data
		if ($return == true)
		{
			return $output;
		}

		echo $output;
	}

	/**
	* uses datatables library to get list of potential items
	*/
	function item($potentialID = 0, $return = false)
	{
		$this->datatables->select('"" AS checkbox', false)
			->edit_column('checkbox', '<input type="checkbox" class="contact-checkbox row-checkbox" value="$1" name="items[]" data-form="item">', 'pi.potItemID')
			->select('pi.product, pi.colors, pi.quantity, pi.orderedFrom, pi.trackingNum')
			->select('pi.potItemID')
			//->unset_column('pi.potItemID')
			->from('PotentialItems pi');

		if ($potentialID > 0)
		{
			$this->datatables->where('pi.potentialID', $potentialID);
		}

		$output = $this->datatables->generate();

		//return or echo the data
		if ($return == true)
		{
			return $output;
		}

		echo $output;
	}

	/**
	* uses datatables library to get list of potential attachments
	*/
	function attachment($potentialID = 0, $return = false)
	{
		$this->datatables->select('pa.potAttachmentID')
			->select('"" AS checkbox', false)
			->edit_column('checkbox', '<input type="checkbox" class="attachment-checkbox row-checkbox" value="$1" name="items[]" data-form="attachment">', 'pa.potAttachmentID')
			->select('pa.filename, pa.fullpath')
			->edit_column('pa.filename', '<a href="$2" target="_blank">$1</a>', 'pa.filename, pa.fullpath')
			->select('CONCAT(a.firstName, " ", a.lastName) AS name', false)
			->select('pa.dateCreated')
			->select('CONCAT(pa.fileSize, " kb")', false)
			->unset_column('pa.potAttachmentID')
			->unset_column('pa.fullpath')
			->from('PotentialAttachments pa')
			->join('Admins a', 'a.adminID = pa.createdBy', 'left outer');

		if ($potentialID > 0)
		{
			$this->datatables->where('pa.potentialID', $potentialID);
		}

		$output = $this->datatables->generate();

		//return or echo the data
		if ($return == true)
		{
			return $output;
		}

		echo $output;
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

}