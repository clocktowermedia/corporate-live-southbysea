<div class="col-sm-12">
	<a href="/admin/potentials/modals/add/note/<?php echo $potential['potentialID'];?>" data-toggle="ajax-modal" class="btn btn-info"><i class="fa fa-comment"></i> Add Note</a>
	<a class="btn btn-info sort-listing" data-section="notes" data-order="desc" data-order-by="dateUpdated" data-desc-msg="Newest to Oldest" data-asc-msg="Oldest to Newest"><i class="fa fa-caret-down"></i> <span>Newest to Oldest</span></a>
</div>

<div class="col-sm-12">
	<script id="notes-listing" type="x-handlebars-template">
		{{#if items}}
			{{#each items}}
				<div class="panel panel-white post panel-shadow">
					<div class="post-heading">
						<div class="pull-left meta">
							<div class="title h5">
								<a href="#"><b>{{user.firstName}} {{user.lastName}}</b></a>
							</div>
							<h6 class="text-muted time">{{dateFormat dateUpdated format="MMM D, YYYY"}} @ {{dateFormat dateUpdated format="h:mm A"}}</h6>
						</div>
						<div class="pull-right">
							<div class="stats">
								<div class="upload-container no-style" data-parent="potNoteID" data-id="{{potNoteID}}" data-url="/admin/potentials/json/upload/note-attachment/{{potNoteID}}" data-section="notes">
									<a class="btn btn-default upload-btn" title="Add Attachment to Note"><i class="fa fa-paperclip"></i></a>
									<input class="upload-trigger hide" type="file" name="userfile[]" multiple="multiple" title="Click to add Files">
								</div>
								<a data-toggle="ajax-modal" href="/admin/potentials/modals/edit/note/{{potNoteID}}" class="btn btn-default" title="Edit Note"><i class="fa fa-pencil"></i></a>
								{{#compare user.adminID "==" <?php echo $_SESSION['adminID']; ?>}}
									<a data-confirm="Are you sure you want to delete this?" data-section="notes" href="/admin/potentials/json/delete/note/{{potNoteID}}" class="btn btn-default"><i class="fa fa-trash"></i></a>
								{{/compare}}
							</div>
						</div>
					</div>
					<div class="post-description">
						<span class="note-title">{{title}}</span>
						<span class="note-content">{{comments}}</span>

						{{#if attachments.length}}
							<span class="note-attachments">Attachments:</span>
							<div class="row">
								{{#each attachments}}
									{{#compare mimeType "~=" 'image'}}
										<div class="item col-md-3 col-xs-4 text-center">
											<div class="thumbnail">
												<img class="group list-group-image img-responsive" src="{{fullpath}}" alt="{{filename}}" />
												<div class="caption">
													<h4 class="group inner list-group-item-heading">{{filename}}</h4>
													<a data-confirm="Are you sure you want to delete this?" data-section="notes" href="/admin/potentials/json/delete/note-attachment/{{potNoteAttachmentID}}" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</a>
													<a href="{{fullpath}}" target="_blank" class="btn btn-default" title="{{filename}}"><i class="fa fa-file-o"></i> View File</a>
												</div>
											</div>
										</div>
									{{/compare}}
									{{#compare mimeType "!~=" 'image'}}
										<div class="item col-md-3 col-xs-4 text-center">
											<div class="thumbnail">
												<div class="caption">
													<h4 class="group inner list-group-item-heading">{{filename}}</h4>
													<a data-confirm="Are you sure you want to delete this?" data-section="notes" href="/admin/potentials/json/delete/note-attachment/{{potNoteAttachmentID}}" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</a>
													<a href="{{fullpath}}" target="_blank" class="btn btn-default" title="{{filename}}"><i class="fa fa-file-o"></i> View File</a>
												</div>
											</div>
										</div>
									{{/compare}}
								{{/each}}
							</div>
						{{/if}}
					</div>
				</div>
			{{/each}}
		{{else}}
			<p>No notes yet.</p>
		{{/if}}
	</script>

	<div id="notes-container" class="handlebars-listing" data-url="/admin/potentials/json/data/notes/<?php echo $potential['potentialID']; ?>/?sort=desc&sort-by=dateCreated">
	</div>
</div>