<div class="col-sm-6">
	<?php
		$this->form_builder->text('title', 'Potential Name', (isset($potential['title']))? $potential['title'] : '', '', '', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));
	?>
</div>
<div class="col-sm-6">
	<?php
		//Potential ID = ????? auto generated from what?
		$this->form_builder->text('potentialID', 'Potential ID', (isset($potential['potentialID']))? $potential['potentialID'] : '???', '', '', '', '', array('disabled' => 'disabled', 'readonly' => 'readonly'));
	?>
</div>
<div class="clearfix"></div>

<div class="col-sm-6">
	<?php
		$this->form_builder->select('priority', 'Priority', $formOptions['priority']['options'], (isset($potential['priority']))? $potential['priority'] : '', array('inputClass' => 'populated-options'), '', '', array('required' => 'required', 'data-category-id' => $formOptions['origPrintStatus']['category']['formOptCategoryID']));
	?>
</div>
<div class="col-md-1 col-sm-2">
	<?php
		$this->form_builder->checkbox('bigOrder', 'Yes', 0, 1, (isset($potential['bigOrder']))? $potential['bigOrder'] : '', '', 'Big Order?', '', array());
	?>
</div>
<div class="col-md-5 col-sm-4">
	<?php
		$this->form_builder->select('stage', 'Stage', $formOptions['stage']['options'], (isset($potential['stage']))? $potential['stage'] : '', array('inputClass' => 'populated-options'), '', '', array('required' => 'required', 'data-category-id' => $formOptions['stage']['category']['formOptCategoryID']));
	?>
</div>
<div class="clearfix"></div>

<div class="col-sm-6">
	<?php
		$this->form_builder->select('leadSource', 'Lead Source', $formOptions['leadSources']['options'], (isset($potential['leadSource']))? $potential['leadSource'] : '', array('inputClass' => 'populated-options'), '', '', array('required' => 'required', 'data-category-id' => $formOptions['leadSources']['category']['formOptCategoryID']));
	?>
</div>
<div class="col-sm-6">
	<?php
		$this->form_builder->select('newCustomer', 'New Customer?', $formOptions['yes-no']['options'], (isset($potential['newCustomer']))? $potential['newCustomer'] : '', array('inputClass' => 'populated-options'), '', '', array('required' => 'required', 'data-category-id' => $formOptions['yes-no']['category']['formOptCategoryID']));
	?>
</div>
<div class="clearfix"></div>

<div class="col-sm-6">
	<?php
		$this->form_builder->select('licensingType', 'Licensing Type', $formOptions['licenseType']['options'], (isset($potential['licensingType']))? $potential['licensingType'] : '', array('inputClass' => 'populated-options'), '', '', array('required' => 'required', 'data-category-id' => $formOptions['licenseType']['category']['formOptCategoryID']));
	?>
</div>
<div class="col-sm-6">
	<?php
		$this->form_builder->text('productionRecord', 'Production Record', (isset($potential['productionRecord']))? $potential['productionRecord'] : '???', '', '', '', '', array('required' => 'required'));
	?>
</div>
<div class="clearfix"></div>


<div class="col-sm-6">
	<?php
		$this->form_builder->select('school', 'School', $formOptions['school']['options'], (isset($potential['school']))? $potential['school'] : '', array('inputClass' => 'populated-options select2'), '', '', array('required' => 'required', 'data-category-id' => $formOptions['school']['category']['formOptCategoryID']));
	?>
</div>
<div class="col-sm-6">
	<?php
		$this->form_builder->select('chapter', 'Organization/Chapter', $formOptions['chapter']['options'], (isset($potential['chapter']))? $potential['chapter'] : '', array('inputClass' => 'populated-options select2'), '', '', array('required' => 'required', 'data-category-id' => $formOptions['chapter']['category']['formOptCategoryID']));
	?>
</div>
<div class="clearfix"></div>

<div class="col-sm-6">
	<div class="row">
		<div class="col-sm-6">
			<?php
				$this->form_builder->text('acctMgrFirstName', 'Account Manager First Name', (isset($potential['acctMgrFirstName']))? $potential['acctMgrFirstName'] : '', '', 'Account Manager First Name', '', '', array('required' => 'required'));
			?>
		</div>
		<div class="col-sm-6">
			<?php
				$this->form_builder->text('acctMgrLastName', 'Account Manager Last Name', (isset($potential['acctMgrLastName']))? $potential['acctMgrLastName'] : '', '', 'Account Manager Last Name', '', '', array('required' => 'required'));
			?>
		</div>
	</div>
</div>
<div class="col-sm-6">
	<div class="row">
		<div class="col-sm-6">
			<?php
				$this->form_builder->text('customerFirstName', 'Customer First Name', (isset($potential['customerFirstName']))? $potential['customerFirstName'] : '', '', 'Customer First Name', '', '', array('required' => 'required'));
			?>
		</div>
		<div class="col-sm-6">
			<?php
				$this->form_builder->text('customerLastName', 'Customer Last Name', (isset($potential['customerLastName']))? $potential['customerLastName'] : '', '', 'Customer Last Name', '', '', array('required' => 'required'));
			?>
		</div>
	</div>
</div>
<div class="clearfix"></div>

<div class="col-sm-12">
	<span><strong>Last Modified By:</strong></span> Goes Here
</div>