<div class="col-sm-12">
	<legend>Greek</legend>
</div>

<div class="col-sm-6">
	<?php
		$this->form_builder->select('greekLicStatus', 'Greek Status', $formOptions['greekStatus']['options'], (isset($potential['greekLicStatus']))? $potential['greekLicStatus'] : '', array('inputClass' => 'populated-options'), '', '', array('required' => 'required', 'data-category-id' => $formOptions['greekStatus']['category']['formOptCategoryID']));
	?>
</div>
<div class="col-sm-6">
	<?php
		$this->form_builder->select('greekLicStage', 'Greek Stage', $formOptions['greekCollegiateStages']['options'], (isset($potential['greekLicStage']))? $potential['greekLicStage'] : '', array('inputClass' => 'populated-options'), '', '', array('required' => 'required', 'data-category-id' => $formOptions['greekCollegiateStages']['category']['formOptCategoryID']));
	?>
</div>
<div class="clearfix"></div>

<div class="col-sm-12">
	<?php
		$this->form_builder->textarea('greekLicNotes', 'G.L. Approval/Rejection Notes', (isset($potential['greekLicNotes']))? $potential['greekLicNotes'] : '', array('inputClass' => ''), 3, '', '', '', array());
	?>
</div>

<div class="col-sm-12">
	<legend class="mt-30">Collegiate</legend>
</div>

<div class="col-sm-6">
	<?php
		$this->form_builder->select('collegLicStatus', 'Collegiate Status', $formOptions['collegiateStatus']['options'], (isset($potential['collegLicStatus']))? $potential['collegLicStatus'] : '', array('inputClass' => 'populated-options'), '', '', array('required' => 'required', 'data-category-id' => $formOptions['collegiateStatus']['category']['formOptCategoryID']));
	?>
</div>
<div class="col-sm-6">
	<?php
		$this->form_builder->select('collegLicStage', 'Collegiate Stage', $formOptions['greekCollegiateStages']['options'], (isset($potential['collegLicStage']))? $potential['collegLicStage'] : '', array('inputClass' => 'populated-options'), '', '', array('required' => 'required', 'data-category-id' => $formOptions['greekCollegiateStages']['category']['formOptCategoryID']));
	?>
</div>
<div class="clearfix"></div>

<div class="col-sm-6">
	<?php
		$this->form_builder->select('collegLicGroup', 'Collegiate Group', $formOptions['collegiateGroups']['options'], (isset($potential['collegLicGroup']))? $potential['collegLicGroup'] : '', array('inputClass' => 'populated-options'), '', '', array('required' => 'required', 'data-category-id' => $formOptions['collegiateGroups']['category']['formOptCategoryID']));
	?>
</div>
<div class="col-sm-6">
	<?php
		$this->form_builder->text('collegLicRoyalty', 'Collegiate Royalty %', (isset($potential['collegLicRoyalty']))? $potential['collegLicRoyalty'] : '', '', 'Collegiate Royalty %', '', '', array());
	?>
</div>
<div class="clearfix"></div>

<div class="col-sm-12">
	<?php
		$this->form_builder->textarea('collegLicNotes', 'C.L. Approval/Rejection Notes', (isset($potential['collegLicNotes']))? $potential['collegLicNotes'] : '', array('inputClass' => ''), 3, '', '', '', array());
	?>
</div>