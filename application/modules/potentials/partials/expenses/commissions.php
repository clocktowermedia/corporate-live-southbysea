<?php $monetarySign = array('prepend' => array('type' => 'button', 'content' => '<i class="fa fa-usd"></i>', 'class' => 'btn btn-default form-control')); ?>

<div class="col-sm-6">
	<div class="row">
		<div class="col-sm-6">
			<?php
				$this->form_builder->text('campusManagerFirstName', 'Campus Manager First Name', (isset($potential['campusManagerFirstName']))? $potential['campusManagerFirstName'] : '', '', 'Campus Manager First Name', '', '', array());
			?>
		</div>
		<div class="col-sm-6">
			<?php
				$this->form_builder->text('campusManagerLastName', 'Campus Manager Last Name', (isset($potential['campusManagerLastName']))? $potential['campusManagerLastName'] : '', '', 'Campus Manager Last Name', '', '', array());
			?>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<?php
				$this->form_builder->text('cmOwed', 'CM Amount Owed', (isset($potential['cmOwed']))? $potential['cmOwed'] : '', array('inputClass' => 'decimal expense'), 'CM Amount Owed', '', $monetarySign, array());
			?>
		</div>
		<div class="col-sm-6">
			<?php
				$this->form_builder->text('cmPayDate', 'CM Pay Date', (isset($potential['cmPayDate']))? $potential['cmPayDate'] : '', array('inputClass' => 'date'), 'CM Pay Date', '', '', array());
			?>
		</div>
	</div>
</div>
<div class="col-sm-6">
	<div class="col-sm-6">
		<?php
			$this->form_builder->text('chapterRepFirstName', 'Chapter Rep First Name', (isset($potential['chapterRepFirstName']))? $potential['chapterRepFirstName'] : '', '', 'Chapter Rep First Name', '', '', array());
		?>
	</div>
	<div class="col-sm-6">
		<?php
			$this->form_builder->text('chapterRepLastName', 'Chapter Rep Last Name', (isset($potential['chapterRepLastName']))? $potential['chapterRepLastName'] : '', '', 'Chapter Rep Last Name', '', '', array());
		?>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<?php
				$this->form_builder->text('crOwed', 'CR Amount Owed', (isset($potential['crOwed']))? $potential['crOwed'] : '', array('inputClass' => 'decimal expense'), 'CR Amount Owed', '', $monetarySign, array());
			?>
		</div>
		<div class="col-sm-6">
			<?php
				$this->form_builder->text('crPayDate', 'CR Pay Date', (isset($potential['crPayDate']))? $potential['crPayDate'] : '', array('inputClass' => 'date'), 'CM Pay Date', '', '', array());
			?>
		</div>
	</div>
</div>
<div class="clearfix"></div>

<div class="col-sm-12">
	<?php
		$this->form_builder->textarea('commissionNotes', 'Commission Notes', (isset($potential['commissionNotes']))? $potential['commissionNotes'] : '', array('inputClass' => ''), 3, '', '', '', array());
	?>
</div>