<?php $monetarySign = array('prepend' => array('type' => 'button', 'content' => '<i class="fa fa-usd"></i>', 'class' => 'btn btn-default form-control')); ?>

<div class="col-sm-6">
	<?php
		$this->form_builder->textarea('reprintReason', 'Reprint Reason', (isset($potential['reprintReason']))? $potential['reprintReason'] : '', array('inputClass' => ''), 3, '', '', '', array());
	?>
	<div class="row">
		<div class="col-xs-6">
			<?php
				$this->form_builder->text('reprintDecoCost', 'Reprint Decoration Cost', (isset($potential['reprintDecoCost']))? $potential['reprintDecoCost'] : '', array('inputClass' => 'decimal expense reprint'), '', '', $monetarySign, array());
			?>
		</div>
		<div class="col-xs-6">
			<?php
				$this->form_builder->text('reprintGarmentCost', 'Reprint Garment Cost', (isset($potential['reprintGarmentCost']))? $potential['reprintGarmentCost'] : '', array('inputClass' => 'decimal expense reprint'), '', '', $monetarySign, array());
			?>
		</div>
	</div>
</div>
<div class="col-sm-6">
	<?php
		$this->form_builder->select('originalPrintStatus', 'Original Print Status', $formOptions['origPrintStatus']['options'], (isset($potential['originalPrintStatus']))? $potential['originalPrintStatus'] : '', array('inputClass' => 'populated-options'), '', '', array('data-category-id' => $formOptions['origPrintStatus']['category']['formOptCategoryID']));
		$this->form_builder->textarea('originalPrintNote', 'Original Print Notes', (isset($potential['originalPrintNote']))? $potential['originalPrintNote'] : '', array('inputClass' => ''), 3, '', '', '', array());
	?>
</div>