<?php $monetarySign = array('prepend' => array('type' => 'button', 'content' => '<i class="fa fa-usd"></i>', 'class' => 'btn btn-default form-control')); ?>

<div class="col-sm-6">
	<div class="row">
		<div class="col-sm-6">
			<?php
				$this->form_builder->text('glOwed', 'Greek Royalties Owed', (isset($potential['glOwed']))? $potential['glOwed'] : '', array('inputClass' => 'decimal expense'), 'Greek Royalties Owed', '', $monetarySign, array());
			?>
		</div>
		<div class="col-sm-6">
			<?php
				$this->form_builder->checkbox('glPaid', 'Yes', 0, 1, (isset($potential['glPaid']))? $potential['glPaid'] : '', '', 'Greek Royalties Paid?', '', array());
			?>
		</div>
	</div>
	<?php
		$this->form_builder->text('glQuarterPaid', 'GR Quarter', (isset($potential['glQuarterPaid']))? $potential['glQuarterPaid'] :  '', 'GR Quarter', '', '', '', array());
	?>
</div>
<div class="col-sm-6">
	<div class="row">
		<div class="col-sm-6">
			<?php
				$this->form_builder->text('clOwed', 'Collegiate Royalties Owed', (isset($potential['clOwed']))? $potential['clOwed'] : '', array('inputClass' => 'decimal expense'), 'Collegiate Royalties Owed', '', $monetarySign, array());
			?>
		</div>
		<div class="col-sm-6">
			<?php
				$this->form_builder->checkbox('clPaid', 'Yes', 0, 1, (isset($potential['clPaid']))? $potential['clPaid'] : '', '', 'Collegiate Royalties Paid?', '', array());
			?>
		</div>
	</div>
	<?php
		$this->form_builder->text('clQuarterPaid', 'CR Quarter', (isset($potential['clQuarterPaid']))? $potential['clQuarterPaid'] : '', '', 'CR Quarter', '', '', array());
	?>
</div>