<?php $monetarySign = array('prepend' => array('type' => 'button', 'content' => '<i class="fa fa-usd"></i>', 'class' => 'btn btn-default form-control')); ?>

<div class="col-xs-12">
	<legend>Information</legend>
</div>

<div class="col-sm-6">
	<div class="row">
		<div class="col-sm-6">
			<?php
				$this->form_builder->text('nameToInvoiceFirstName', 'Name of Person to Invoice First Name', (isset($potential['nameToInvoiceFirstName']))? $potential['nameToInvoiceFirstName'] : '', '', 'Name of Person to Invoice', '', '', array('required' => 'required', 'data-validation-minlength' => '4'));
			?>
		</div>
		<div class="col-sm-6">
			<?php
				$this->form_builder->text('nameToInvoiceLastName', 'Name of Person to Invoice Last Name', (isset($potential['nameToInvoiceLastName']))? $potential['nameToInvoiceLastName'] : '', '', 'Name of Person to Invoice', '', '', array('required' => 'required', 'data-validation-minlength' => '4'));
			?>
		</div>
	</div>
</div>
<div class="col-sm-6">
	<?php
		$this->form_builder->email('invoiceContactEmail', 'Invoice Contact Email', (isset($potential['invoiceContactEmail']))? $potential['invoiceContactEmail'] : '', '', 'Invoice Contact Email', '', '', array('required' => 'required'));
	?>
</div>
<div class="clearfix"></div>

<div class="col-sm-6">
	<div class="row">
		<div class="col-sm-6">
			<?php
				$this->form_builder->text('poNumber', 'PO Number', (isset($potential['poNumber']))? $potential['poNumber'] : '', '', 'PO Number', '', '', array('required' => 'required', 'data-validation-minlength' => '4'));
			?>
		</div>
		<div class="col-sm-6">
			<?php
				$this->form_builder->select('foSubmission', 'Final Order Submission (From Website)?', $formOptions['yes-no']['options'], (isset($potential['foSubmission']))? $potential['foSubmission'] : '', array('inputClass' => 'populated-options'), '', '', array('required' =>'required', 'data-category-id' => $formOptions['yes-no']['category']['formOptCategoryID']));
			?>
		</div>
	</div>
</div>
<div class="col-sm-6">
	<div class="row">
		<div class="col-xs-4">
			<?php
				$this->form_builder->checkbox('rushOrder', 'Yes', 0, 1, (isset($potential['rushOrder']))? $potential['rushOrder'] : '', '', 'Rush?', '', array());
			?>
		</div>
		<div class="col-xs-8">
			<?php
				$this->form_builder->text('rushReason', 'Rush Reason', (isset($potential['rushReason']))? $potential['rushReason'] : '', '', 'Rush Reason', '', '', array());
			?>
		</div>
	</div>
</div>
<div class="clearfix"></div>

<div class="col-sm-6">
	<?php
		$this->form_builder->text('finalPricing', 'Final Pricing', (isset($potential['finalPricing']))? $potential['finalPricing'] : '', array('inputClass' => 'decimal'), '', '', $monetarySign, array());
	?>
</div>
<div class="col-sm-6">
	<?php
		$this->form_builder->text('finalMargin', 'Final Margin', (isset($potential['finalMargin']))? $potential['finalMargin'] : '', array('inputClass' => 'decimal'), '', '', '', array());
	?>
</div>
<div class="clearfix"></div>

<div class="col-sm-12">
<?php
	$this->form_builder->text('finalMockup', 'Final Mockup Version', (isset($potential['finalMockup']))? $potential['finalMockup'] : '', '', 'Final Mockup Version', '', '', array());
	$this->form_builder->textarea('finalMockupAttachments', 'Final Mockup Attachments', (isset($potential['finalMockupAttachments']))? $potential['finalMockupAttachments'] : '', array('inputClass' => ''), 3, 'Final Mockup Attachments', '', '', array());
?>
</div>
<div class="clearfix"></div>

<div class="col-sm-12">
	<?php
		$this->form_builder->textarea('otherDetails', 'Other Details', (isset($potential['otherDetails']))? $potential['otherDetails'] : '', array('inputClass' => ''), 3, 'Other Details', '', '', array());
	?>
</div>
<div class="clearfix"></div>

<div class="col-xs-12">
	<legend class="mt-30">Online Buy Link</legend>
</div>
<div class="col-sm-4">
	<?php $this->form_builder->text('ecomm_store_id', 'Find Store', (isset($potential['ecomm_store_id']))? $potential['ecomm_store_id'] : '', array('inputClass' => 'form-control autosuggest'), 'Start typing to find an existing store', '', '', array('data-url' => '/admin/ecommerce/json/stores')); ?>
</div>
<div class="col-sm-4">
	<?php $this->form_builder->url('storeLink', 'Store Link', (isset($potential['storeLink']))? $potential['storeLink'] : '', array('inputClass' => 'form-control'), 'If store not found, enter in store link here', '', '', array('data-autosuggest-link' => 'ecomm_store_id', 'data-autosuggest-disable' => true)); ?>
</div>
<div class="col-sm-4">
	<?php
		$this->form_builder->text('storeCloseDate', 'Store Close Date', (isset($potential['storeCloseDate']))? $potential['storeCloseDate'] : '', array('inputClass' => 'date data-autosuggest-linked'), 'Store Close Date', '', '', array('data-autosuggest-link' => 'ecomm_store_id', 'data-autosuggest-val' => 'end_date', 'data-autosuggest-disable' => true));
	?>
</div>
<div class="clearfix"></div>

<div class="col-xs-12">
	<legend class="mt-30">Statuses</legend>
</div>
<div class="col-sm-3 col-xs-4">
<?php
	$this->form_builder->set_labels(false);
	$this->form_builder->checkbox('workOrderSent', 'Work Order Sent?', 0, 1, (isset($potential['workOrderSent']))? $potential['workOrderSent'] : '', '', '', '', array());
?>
</div>
<div class="col-sm-3 col-xs-4">
<?php
	$this->form_builder->checkbox('artworkSent', 'Artwork Sent?', 0, 1, (isset($potential['artworkSent']))? $potential['artworkSent'] : '', '', '', '', array());
?>
</div>
<div class="col-sm-3 col-xs-4">
<?php
	$this->form_builder->checkbox('garmentSent', 'Garments Ordered?', 0, 1, (isset($potential['garmentSent']))? $potential['garmentSent'] : '', '', '', '', array());
?>
</div>
<?php
	$this->form_builder->checkbox('productionSent', 'Sent to Production?', 0, 1, (isset($potential['productionSent']))? $potential['productionSent'] : '', '', '', '', array());
?>
<?php $this->form_builder->set_labels(true); ?>
<div class="clearfix"></div>


<div class="col-xs-12">
	<legend class="mt-30">Items Ordered From</legend>
</div>
<div class="col-xs-12">
	<?php $this->load->view('../../potentials/partials/items'); ?>
</div>


<div class="col-xs-12">
	<legend class="mt-30">Payment</legend>
</div>
<div class="col-sm-6">
	<?php
		$this->form_builder->select('paymentMethod', 'Payment Method', $formOptions['paymentMethod']['options'], (isset($potential['paymentMethod']))? $potential['paymentMethod'] : '', array('inputClass' => 'populated-options'), '', '', array('required' =>'required', 'data-category-id' => $formOptions['paymentMethod']['category']['formOptCategoryID']));
	?>
</div>
<div class="col-sm-6">
	<?php
		$this->form_builder->text('invoiceNum', 'Invoice #', (isset($potential['invoiceNum']))? $potential['invoiceNum'] : '', '', 'Invoice #', '', '', array());
	?>
</div>
<div class="clearfix"></div>

<div class="col-sm-6">
	<div class="row">
		<div class="col-md-4 col-sm-6">
			<?php
				$this->form_builder->checkbox('paymentReceived', 'Yes', 0, 1, (isset($potential['paymentReceived']))? $potential['paymentReceived'] : '', '', 'Payment Received?', '', array());
			?>
		</div>
		<div class="col-md-8 col-sm-6">
			<?php
				$this->form_builder->text('paymentDate', 'Payment Received Date', (isset($potential['paymentDate']))? $potential['paymentDate'] : '', array('inputClass' => 'date'), 'Payment Received Date', '', '', array());
			?>
		</div>
	</div>
</div>

<div class="col-xs-12">
	<legend class="mt-30">Shipments/Delivery</legend>
</div>
<div class="col-sm-12">
	<?php
		$this->form_builder->text('address1', 'Delivery Address', (isset($potential['address1']))? $potential['address1'] : '', '', 'Delivery Address', '', '', array('required' =>'required', 'data-validation-minlength' => '2'));
		$this->form_builder->text('address2', 'Delivery Address Line 2', (isset($potential['address2']))? $potential['address2'] : '', '', 'Delivery Address Line 2', '', '', array());
	?>
</div>
<div class="col-sm-6">
	<?php
		$this->form_builder->text('city', 'Delivery City',  (isset($potential['city']))? $potential['city'] : '', '', 'Delivery City', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));
	?>
</div>
<div class="col-sm-6">
	<div class="row">
		<div class="col-xs-6">
			<?php
				$this->load->helper('dropdown');
				$this->form_builder->select('state', 'Delivery State', array('' => 'Delivery State') + usStates(), (isset($potential['state']))? $potential['state'] : '', '', '', '', array('required' => 'required', 'data-validation-minlength' => '2', 'data-validation-maxlength' => '2'));
			?>
		</div>
		<div class="col-xs-6">
			<?php
				$this->form_builder->text('zip', 'Delivery Zip Code', (isset($potential['zip']))? $potential['zip'] : '', '', 'Delivery Zip Code', '', '', array('required' => 'required', 'data-validation-minlength' => '5', 'data-validation-maxlength' => '10'));
			?>
		</div>
	</div>
</div>
<div class="clearfix"></div>

<div class="col-sm-6">
	<div class="row">
		<div class="col-sm-6">
			<?php
				$this->form_builder->text('shipDate', 'Shipping Date', (isset($potential['shipDate']))? $potential['shipDate'] : '', array('inputClass' => 'date'), 'Shipping Date', '', '', array());
			?>
		</div>
		<div class="col-sm-6">
			<?php
				$this->form_builder->text('deliveryDate', 'Delivery Date', (isset($potential['deliveryDate']))? $potential['deliveryDate'] : '', array('inputClass' => 'date'), 'Delivery Date', '', '', array());
			?>
		</div>
	</div>
</div>
<div class="col-sm-6">
	<?php
		$this->form_builder->text('trackingNum', 'Tracking #', (isset($potential['trackingNum']))? $potential['trackingNum'] : '', '', 'Tracking #', '', '', array());
	?>
</div>
<div class="clearfix"></div>

<div class="col-sm-12">
	<?php
		$this->form_builder->textarea('lostReason', 'Closed Lost Reason?', (isset($potential['lostReason']))? $potential['lostReason'] : '', array('inputClass' => ''), 3, 'Closed Lost Reason?', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));
	?>
</div>