<?php $monetarySign = array('prepend' => array('type' => 'button', 'content' => '<i class="fa fa-usd"></i>', 'class' => 'btn btn-default form-control')); ?>

<div class="col-sm-6">
	<?php
		$this->form_builder->text('totalGross', 'Total Gross', (isset($potential['totalGross']))? $potential['totalGross'] : '', array('inputClass' => 'decimal'), '', '', $monetarySign, array());
	?>
</div>
<div class="col-sm-6">
	<?php
		$this->form_builder->text('preTax', 'Total Pre-Tax', (isset($potential['preTax']))? $potential['preTax'] : '', array('inputClass' => 'decimal'), '', '', $monetarySign, array());
	?>
</div>
<div class="clearfix"></div>

<div class="col-sm-6">
	<?php
		$this->form_builder->text('profit', 'Profit', (isset($potential['profit']))? $potential['profit'] : '', array('inputClass' => 'decimal'), '', '', $monetarySign, array());
	?>
</div>
<div class="col-sm-6">
	<?php
		$this->form_builder->text('margin', 'Actual Margin', (isset($potential['margin']))? $potential['margin'] : '', array('inputClass' => 'decimal'), '', '', '', array());
	?>
</div>
<div class="clearfix"></div>

<div class="col-sm-6">
	<?php
		$this->form_builder->text('reprintProfit', 'Reprint Adjusted Profit', (isset($potential['reprintProfit']))? $potential['reprintProfit'] : '', array('inputClass' => 'decimal'), '', '', $monetarySign, array());
	?>
</div>
<div class="col-sm-6">
	<?php
		$this->form_builder->text('reprintMargin', 'Reprint Adjusted Actual Margin', (isset($potential['reprintMargin']))? $potential['reprintMargin'] : '', array('inputClass' => 'decimal'), '', '', '', array());
	?>
</div>
<div class="clearfix"></div>

<div class="col-sm-12">
	<hr/>
</div>

<div class="col-sm-12">
	<?php
		$this->form_builder->select('auditStatus', 'Audit', $formOptions['auditStatus']['options'], (isset($potential['auditStatus']))? $potential['auditStatus'] : '', array('inputClass' => 'populated-options'), '', '', array('data-category-id' => $formOptions['auditStatus']['category']['formOptCategoryID']));
		$this->form_builder->textarea('auditNotes', 'Audit Notes', (isset($potential['auditNotes']))? $potential['auditNotes'] : '', array('inputClass' => ''), 3, '', '', '', array());
	?>
</div>