<div class="col-sm-6">
	<?php
		$this->form_builder->select('designer', 'Designer', $dropdowns['designers'], (isset($potential['designer']))? $potential['designer'] : '', '', '', '', array('required' => 'required'));
	?>
</div>
<div class="col-sm-6">
	<?php
		$this->form_builder->select('designStatus', 'Design Status', $formOptions['designStatus']['options'], (isset($potential['designStatus']))? $potential['designStatus'] : '', array('inputClass' => 'populated-options'), '', '', array('required' => 'required', 'data-category-id' => $formOptions['designStatus']['category']['formOptCategoryID']));
	?>
</div>
<div class="clearfix"></div>

<div class="col-sm-12">
	<?php
		$this->form_builder->textarea('designQuickFix', 'Quick Fix', (isset($potential['designQuickFix']))? $potential['designQuickFix'] : '', array('inputClass' => ''), 3, '', '', '', array());
	?>
</div>