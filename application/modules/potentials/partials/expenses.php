<div class="potential-items" id="potentials-expenses">

	<div class="row">
		<div class="col-xs-6">

			<div class="pull-left">
				<label class="check-all-label">
					<input type="checkbox" name="check-all" class="check-all"/>
					<span class="sr-only">Check All</span>
				</label>
			</div>

			<div class="pull-left">
				<div class="btn-group">
					<button type="button" class="btn btn-default btn-sm btn-flat dropdown-toggle" data-toggle="dropdown">
						Action <span class="caret"></span>
					</button>
					<ul class="dropdown-menu datatables-actions" role="menu">
						<li><a href="/admin/potentials/modals/add/expense/<?php echo $potential['potentialID'];?>" data-toggle="ajax-modal"><i class="fa fa-usd"></i> Add Expense</a></li>
						<li role="separator" class="divider"></li>
						<li><a data-hijack="delete" data-type="expense"><i class="fa fa-trash"></i> Delete</a></li>
					</ul>
				</div>
			</div>

			<div class="pull-left">
				<form class="form-horizontal">
					<div class="show-dropdown">
						<label>Show:</label>
						<select name="show" class="form-control input-sm">
							<option value="5">5</option>
							<option value="10">10</option>
							<option value="20">20</option>
							<option value="-1">All</option>
						</select>
					</div>
				</form>
			</div>
		</div>

		<div class="col-xs-6">
			<form action="" class="text-right datatables-search" method="post">
				<div class="input-group">
					<input type="text" name="query" class="form-control input-sm full-width-search" placeholder="Search">
					<div class="input-group-btn">
						<button type="submit" name="q" class="btn btn-sm btn-info"><i class="fa fa-search"></i></button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="table-responsive">
				<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered datatable potentials-table custom-table editable" width="100%" data-url="/admin/potentials/datatables/expense/<?php echo $potential['potentialID'];?>">
					<thead>
						<tr>
							<th></th>
							<th data-class="expand">Title</th>
							<th data-hide="phone">Label</th>
							<th>Value</th>
						</tr>
					</thead>

					<tbody>
					</tbody>
				</table>
			</div>
		</div>
	</div>

</div>

<div class="row mt-15">
	<div class="col-xs-12">
	<?php
		$this->form_builder->textarea('expenseNotes', 'Expense Notes', (isset($potential['expenseNotes']))? $potential['expenseNotes'] : '', array('inputClass' => ''), 3, '', '', '', array());
	?>
	</div>
</div>