<?php
	$this->form_builder->text('title', 'Title:', (isset($report['title']))? $report['title'] : '', '', 'Report Title', '', '', array('required' => 'required'));
?>

<div class="row">
	<div class="col-md-6">
		<h5><strong>Please select the fields you would like in your report from the list below.</strong></h5>
		<div id="field-list">
		</div>
	</div>

	<div class="col-md-6">
		<h5><strong>Drag and drop the fields below to change the order in which they are displayed.</strong></h5>
		<ul class="nav nav-pills nav-stacked" id="field-container">
		</ul>
		<script id="field-listing" type="x-handlebars-template">
			{{#if fields}}
				{{#each fields}}
					<li id="fields_{{potentialFieldID}}"><a>{{text}}</a></li>
				{{/each}}
				</ul>
			{{else}}
				<p>No fields selected yet.</p>
			{{/if}}
		</script>
	</div>
</div>