<p>Please start by entering a potential name. Once you have entered a name, you will be able to edit all other values and fields for the potential.</p>

<?php echo form_open('admin/potentials/create'); ?>
	<?php
		$this->form_builder->set_labels(false);
		$this->form_builder->text('title', 'Potential Name:', (isset($potential['title']))? $potential['title'] : '', '', 'Potential Name', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));
		$this->form_builder->button('submit', 'Submit', array('inputClass' => 'btn btn-info'), '', '');
	?>
</form>