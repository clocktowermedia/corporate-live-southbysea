<?php echo form_open_multipart('/admin/potentials/json/upload/note-attachment/' . $itemID, array('class' => 'form-horizontal ajax-submit', 'data-section' => 'notes')); ?>
	<div class="modal-body">
		<div class="form-group" id="note-attachment-upload">
			<label class="control-label col-sm-1" for="userfile">File:</label>
			<div class="col-sm-11">
				<input type="file" name="userfile" id="userfile" size="20" class="form-control"/>
			</div>
		</div>
		<input type="hidden" name="potNoteID" value="<?php echo $itemID; ?>">
	</div>
	<div class="modal-footer">
		<button class="btn btn-info" type="submit">Submit</button>
		<a data-dismiss="modal" class="btn btn-default">Close</a>
	</div>
</form>