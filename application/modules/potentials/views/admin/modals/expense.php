<?php echo form_open('/admin/potentials/json/' . $action . '/expense/' . $itemID, array('class' => 'form-horizontal ajax-submit', 'data-table' => 'expenses')); ?>
	<div class="modal-body">
		<?php
			//create array for classes
			$containerArray = array('labelClass' => 'col-sm-2', 'containerClass' => 'col-sm-10');

			$this->form_builder->select('title', 'Title:', $formOptions['expenseTypes']['options'], (isset($item['title']))? $item['title'] : '', $containerArray + array('inputClass' => 'populated-options'), '', '', array('required' => 'required', 'data-category-id' => $formOptions['expenseTypes']['category']['formOptCategoryID']));
			$this->form_builder->text('Label', 'Label:', (isset($item['label']))? $item['label'] : '', $containerArray, '', '', '', array('required' => 'required'));
			$this->form_builder->text('value', 'Value:', (isset($item['value']))? $item['value'] : '', $containerArray + array('inputClass' => 'decimal'), '', '', '', array('required' => 'required'));
		?>
	</div>
	<div class="modal-footer">
		<button class="btn btn-info" type="submit">Submit</button>
		<a data-dismiss="modal" class="btn btn-default">Close</a>
	</div>
</form>