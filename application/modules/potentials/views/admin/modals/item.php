<?php echo form_open('/admin/potentials/json/' . $action . '/item/' . $itemID, array('class' => 'form-horizontal ajax-submit', 'data-table' => 'items')); ?>
	<div class="modal-body">
		<?php
			//create array for classes
			$containerArray = array('labelClass' => 'col-sm-4', 'containerClass' => 'col-sm-8');

			$this->form_builder->text('product', 'Product(s):', (isset($item['product']))? $item['product'] : '', $containerArray, 'Brand/Num/Description', '', '', array('required' => 'required'));
			$this->form_builder->text('colors', 'Product Color(s):', (isset($item['colors']))? $item['colors'] : '', $containerArray, 'Product Color(s)', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));
			$this->form_builder->number('quantity', 'Total Quantity:', (isset($item['quantity']))? $item['quantity'] : '', $containerArray, 'Total Quantity', '', '', array('required' => 'required', 'min' => 12));
			$this->form_builder->textarea('sizeBreakdown', 'Size Breakdown:', (isset($item['sizeBreakdown']))? $item['sizeBreakdown'] : '', $containerArray + array('inputClass' => ''), 3, 'Size Breakdown', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));
			$this->form_builder->text('orderedFrom', 'Garment Ordered From:', (isset($item['orderedFrom']))? $item['orderedFrom'] : '', $containerArray, 'Garment Ordered From', '', '', array());
			$this->form_builder->text('trackingNum', 'Garment Tracking #:', (isset($item['trackingNum']))? $item['trackingNum'] : '', $containerArray, 'Garment Tracking #', '', '', array());
		?>
	</div>
	<div class="modal-footer">
		<button class="btn btn-info" type="submit">Submit</button>
		<a data-dismiss="modal" class="btn btn-default">Close</a>
	</div>
</form>