<?php echo form_open('/admin/potentials/json/' . $action . '/note/' . $itemID, array('class' => 'form-horizontal ajax-submit', 'data-section' => 'notes')); ?>
	<div class="modal-body">
		<?php
			//create array for classes
			$containerArray = array('labelClass' => 'col-sm-2', 'containerClass' => 'col-sm-10');

			$this->form_builder->text('title', 'Title:', (isset($item['title']))? $item['title'] : '', $containerArray, '', '', '', array());
			$this->form_builder->textarea('comments', 'Comments:', (isset($item['comments']))? $item['comments'] : '', $containerArray + array('inputClass' => ''), 3, '', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));
		?>
	</div>
	<div class="modal-footer">
		<button class="btn btn-info" type="submit">Submit</button>
		<a data-dismiss="modal" class="btn btn-default">Close</a>
	</div>
</form>