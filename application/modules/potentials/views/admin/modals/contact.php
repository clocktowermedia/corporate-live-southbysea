<?php echo form_open('/admin/potentials/json/' . $action . '/contact/' . $itemID, array('class' => 'form-horizontal ajax-submit', 'data-table' => 'contacts')); ?>
	<div class="modal-body">
		<?php
			//create array for classes
			$containerArray = array('labelClass' => 'col-sm-3', 'containerClass' => 'col-sm-9');

			$this->form_builder->text('firstName', 'First Name:', (isset($item['firstName']))? $item['firstName'] : '', $containerArray, 'First Name', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));
			$this->form_builder->text('lastName', 'Last Name:', (isset($item['lastName']))? $item['lastName'] : '', $containerArray, 'Last Name', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));
			$this->form_builder->text('email', 'Email:', (isset($item['email']))? $item['email'] : '', $containerArray, 'Email', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));
			$this->form_builder->tel('phone', 'Phone:', (isset($item['phone']))? $item['phone'] : '', $containerArray, 'Phone', '', '', array());
			$this->form_builder->select('role', 'Role:', $formOptions['contactRoles'], (isset($item['role']))? $item['role'] : '', $containerArray, '', '', array('required' => 'required'));
		?>
	</div>
	<div class="modal-footer">
		<button class="btn btn-info" type="submit">Submit</button>
		<a data-dismiss="modal" class="btn btn-default">Close</a>
	</div>
</form>