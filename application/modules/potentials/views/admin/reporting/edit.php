<div class="row">
	<div class="col-xs-12">

		<?php echo form_open('admin/potentials/json/save-report-fields/' . $report['potentialReportTypeID'], array('data-id' => $report['potentialReportTypeID'])); ?>

			<?php $this->load->view('../../potentials/partials/forms/report'); ?>

			<?php
				$this->form_builder->button('submit', 'Update Report', array('inputClass' => 'btn btn-info btn-block'), '', '');
			?>

		</form>
	</div>
</div>