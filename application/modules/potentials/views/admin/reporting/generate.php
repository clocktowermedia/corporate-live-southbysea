
<?php echo form_open('admin/potentials/reporting/generate'); ?>
	<?php
		$this->form_builder->select('reportType', 'Report:', $reports, (isset($postData['reportType']))? $postData['reportType'] : '', '', '', '', array('required' => 'required'));
		$this->form_builder->button('submit', 'Generate Report', array('inputClass' => 'btn btn-info btn-block'), '', '');
	?>
</form>

<?php
	if (isset($reportView))
	{
		echo '<hr/>';
		$this->load->view($reportView);
	}
?>