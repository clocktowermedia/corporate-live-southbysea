<?php if (isset($pdfOutput) && $pdfOutput == true):?>
	<?php $this->load->view('pdf-style'); ?>
<?php endif;?>

<?php $btnColor = ($this->uri->segment(1) == 'admin')? 'info' : 'warning'; ?>

<h4 class="pull-left"><?php echo $reportTitle;?></h4>

<?php if ($reportData != false && count($reportData) > 0):?>

	<?php if (!isset($pdfOutput) || $pdfOutput == false):?>
		<div class="pull-right">

			<form class="pull-left form-inline" id="reporting-table-search-form">
				<label>Search:<input type="search" class="form-control" placeholder="" id="reporting-table-search"></label>
			</form>

			<form method="post" action="<?php echo $generateUrl;?>/csv" class="pull-left">
				<?php foreach ($postData as $key => $value):?>
				<input type="hidden" name="<?php echo $key;?>" value="<?php echo $value;?>">
				<?php endforeach;?>
				<button type="submit" class="btn btn-<?php echo $btnColor;?>"><i class="icon-list-alt"></i> Download CSV</button>
			</form>

			<form method="post" action="<?php echo $generateUrl;?>/pdf" class="pull-right">
				<?php foreach ($postData as $key => $value):?>
				<input type="hidden" name="<?php echo $key;?>" value="<?php echo $value;?>">
				<?php endforeach;?>
				&nbsp;<button type="submit" class="btn btn-<?php echo $btnColor;?>"><i class="icon-download-alt"></i> Download PDF</button>
			</form>
		</div>
	<?php endif;?>

	<div class="clearfix"></div>

	<div class="table-responsive">
		<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered datatable no-pagination">
			<thead>
				<tr>
					<?php foreach ($headings as $heading):?>
						<th><?php echo $heading['text'];?></th>
					<?php endforeach;?>
				</tr>
			</thead>

			<tbody>
				<?php foreach ($reportData as $form):?>
					<tr>
						<?php foreach ($headings as $heading):?>
							<td><?php echo $form[$heading['value']];?></td>
						<?php endforeach;?>
					</tr>
				<?php endforeach;?>
			</tbody>
		</table>
	</div>

<?php else:?>
	<div class="clearfix"></div>
	<div class="alert alert-danger alert-dismissable">
		<button class="close" data-dismiss="alert">x</button>
		<p><strong>No matches found</strong></p>
	</div>

<?php endif;?>