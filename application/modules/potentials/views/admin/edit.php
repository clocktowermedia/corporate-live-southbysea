<!--<h1><?php echo $title; ?></h1>-->

<div id="order-form-container" data-id="<?php echo $potential['potentialID']; ?>">

	<?php
		$monetarySign = array('prepend' => array('type' => 'button', 'content' => '<i class="fa fa-usd"></i>', 'class' => 'btn btn-default form-control'));
		$percentSign = array('append' => array('type' => 'button', 'content' => '<i class="fa fa-percent"></i>', 'class' => 'btn btn-default form-control'));
	?>

	<a class="accordian-toggle" data-toggle="collapse" data-target="#information" aria-expanded="true" aria-controls="information">
		<h4>
			Potential Information
			<div class="pull-right"><i class="glyphicon glyphicon-minus"></i></div>
		</h4>
	</a>
	<div class="collapse in accordian-result" id="information" aria-expanded="false">
		<div class="row">
			<?php $this->load->view('../../potentials/partials/info'); ?>
		</div>
	</div>

	<a class="accordian-toggle" data-toggle="collapse" data-target="#final-order" aria-expanded="true" aria-controls="final-order">
		<h4>
			Designer Info
			<div class="pull-right"><i class="glyphicon glyphicon-plus"></i></div>
		</h4>
	</a>
	<div class="collapse accordian-result" id="final-order" aria-expanded="false">
		<div class="row">
			<?php $this->load->view('../../potentials/partials/designer'); ?>
		</div>
	</div>


	<a class="accordian-toggle" data-toggle="collapse" data-target="#designer" aria-expanded="true" aria-controls="designer">
		<h4>
			Final Order
			<div class="pull-right"><i class="glyphicon glyphicon-plus"></i></div>
		</h4>
	</a>
	<div class="collapse accordian-result" id="designer" aria-expanded="false">
		<div class="row">
			<?php $this->load->view('../../potentials/partials/final-order'); ?>
		</div>
	</div>


	<a class="accordian-toggle" data-toggle="collapse" data-target="#licensing" aria-expanded="true" aria-controls="licensing">
		<h4>
			Licensing
			<div class="pull-right"><i class="glyphicon glyphicon-plus"></i></div>
		</h4>
	</a>
	<div class="collapse accordian-result" id="licensing" aria-expanded="false">
		<div class="row">
			<?php $this->load->view('../../potentials/partials/licensing'); ?>
		</div>
	</div>


	<a class="accordian-toggle" data-toggle="collapse" data-target="#expenses" aria-expanded="true" aria-controls="expenses">
		<h4>
			Expenses & Profit - <small>(Commissions, Royalties, Reprints, etc.)</small>
			<div class="pull-right"><i class="glyphicon glyphicon-plus"></i></div>
		</h4>
	</a>
	<div class="collapse accordian-result" id="expenses" aria-expanded="false">

		<div class="row">
			<div class="col-sm-12">
				<legend>Commissions</legend>
			</div>
			<?php $this->load->view('../../potentials/partials/expenses/commissions'); ?>
			<div class="clearfix"></div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<legend class="mt-30">Royalties</legend>
			</div>
			<?php $this->load->view('../../potentials/partials/expenses/royalties'); ?>
			<div class="clearfix"></div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<legend class="mt-30">Expenses</legend>
			</div>
			<div class="col-sm-12">
				<?php $this->load->view('../../potentials/partials/expenses'); ?>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<legend class="mt-30">Reprints</legend>
			</div>
			<?php $this->load->view('../../potentials/partials/expenses/reprint'); ?>
			<div class="clearfix"></div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<legend class="mt-30">Profit</legend>
			</div>
			<?php $this->load->view('../../potentials/partials/profit'); ?>
			<div class="clearfix"></div>
		</div>

	</div>


	<a class="accordian-toggle" data-toggle="collapse" data-target="#request" aria-expanded="true" aria-controls="request">
		<h4>
			Original Request - <small>(Artwork, Product Instructions)</small>
			<div class="pull-right"><i class="glyphicon glyphicon-plus"></i></div>
		</h4>
	</a>
	<div class="collapse accordian-result" id="request" aria-expanded="false">
		<div class="row">
			<div class="col-sm-12">
				<?php
					$this->form_builder->set_labels(false);
					$this->form_builder->textarea('originalSubmission', '', (isset($submission['originalSubmission']))? $submission['originalSubmission'] : '', array('inputClass' => ''), 3, 'Quote Request', '', '', array('disabled' => 'disabled', 'readonly' => 'readonly'));
					$this->form_builder->textarea('originalFinalOrder', '', (isset($submission['originalFinalOrder']))? $submission['originalFinalOrder'] : '', array('inputClass' => ''), 3, 'Final Order', '', '', array('disabled' => 'disabled', 'readonly' => 'readonly'));
					$this->form_builder->set_labels(true);
				?>
			</div>
		</div>
	</div>

	<a class="accordian-toggle" data-toggle="collapse" data-target="#notes" aria-expanded="true" aria-controls="notes">
		<h4>
			Notes/Comments
			<div class="pull-right"><i class="glyphicon glyphicon-plus"></i></div>
		</h4>
	</a>
	<div class="collapse accordian-result" id="notes" aria-expanded="false">
		<div class="row">
			<?php $this->load->view('../../potentials/partials/notes'); ?>
		</div>
	</div>


	<a class="accordian-toggle" data-toggle="collapse" data-target="#attachments" aria-expanded="true" aria-controls="attachments">
		<h4>
			Attachments
			<div class="pull-right"><i class="glyphicon glyphicon-plus"></i></div>
		</h4>
	</a>
	<div class="collapse accordian-result" id="attachments" aria-expanded="false">
		<div class="row">
			<div class="col-sm-12">

				<div id="potential-attachments-upload-container" class="text-center upload-container mb-30" data-parent="potentialID" data-id="<?php echo $potential['potentialID']; ?>" data-url="/admin/potentials/json/upload/attachment" data-table="attachments">
					<h5>Drag &amp; Drop Files Here</h5>
					<h5>-or-</h5>
					<label>
						<div class="form-group">
							<button type="button" class="upload-btn" class="btn btn-info">Click to Add Files</button>
						</div>
						<input class="upload-trigger hide" type="file" name="userfile[]" multiple="multiple" title="Click to add Files">
					</label>
				</div>

			</div>
		</div>

		<?php $this->load->view('../../potentials/partials/attachments'); ?>
	</div>


	<a class="accordian-toggle" data-toggle="collapse" data-target="#contacts" aria-expanded="true" aria-controls="contacts">
		<h4>
			Contacts
			<div class="pull-right"><i class="glyphicon glyphicon-plus"></i></div>
		</h4>
	</a>
	<div class="collapse accordian-result" id="contacts" aria-expanded="false">
		<?php $this->load->view('../../potentials/partials/contacts'); ?>
	</div>


	<a class="accordian-toggle" data-toggle="collapse" data-target="#emails" aria-expanded="true" aria-controls="emails">
		<h4>
			Emails
			<div class="pull-right"><i class="glyphicon glyphicon-plus"></i></div>
		</h4>
	</a>
	<div class="collapse accordian-result" id="emails" aria-expanded="false">
		<?php //$this->load->view('../../potentials/partials/emails'); ?>
	</div>

</div>
