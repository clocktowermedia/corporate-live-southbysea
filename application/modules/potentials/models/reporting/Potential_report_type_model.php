<?php
/********************************************************************************************************
/
/ Potential "report" types
/
********************************************************************************************************/
class Potential_report_type_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'PotentialReportTypes';

	//primary key
	public $primary_key = 'potentialReportTypeID';

	//relationships
	public $has_many = array(
		'fields' => array('model' => 'potentials/reporting/potential_report_field_model', 'primary_key' => 'potentialReportFieldID', 'join_key' => 'potentialReportTypeID'),
	);

	//callbacks/observers (use function names)
	public $before_create = array('_set_url_slug');
	public $before_update = array('_set_url_slug');

	//validation
	public $validate = array(
		'title' => array(
			'field' => 'title',
			'label' => 'Title',
			'rules' => 'required'
		),
		'url_slug' => array(
			'field' => 'url_slug',
			'label' => 'Value',
			'rules' => 'seo_url|is_unique[PotentialReportTypes.url_slug]'
		)
	);

	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Validity checker functions & helper functions, may contain redirects
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
	public function _set_url_slug($data)
	{
		if (isset($data['title']) && $data['title'] != '')
		{
			$this->load->helper('url');
			$data['url_slug'] = url_title($data['title'], '-', TRUE);
		}
		return $data;
	}
}