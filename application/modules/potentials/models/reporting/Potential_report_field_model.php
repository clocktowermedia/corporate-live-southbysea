<?php
/********************************************************************************************************
/
/ Potential "report" fields
/
********************************************************************************************************/
class Potential_report_field_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'PotentialReportFields';

	//primary key
	public $primary_key = 'potentialReportFieldID';

	//relationships
	public $has_single = array(
		'field' => array('model' => 'potentials/reporting/potential_field_model', 'primary_key' => 'potentialFieldID', 'join_key' => 'potentialFieldID'),
		'report' => array('model' => 'potentials/reporting/potential_report_type_model', 'primary_key' => 'potentialReportTypeID', 'join_key' => 'potentialReportTypeID')
	);

	//callbacks/observers (use function names)

	//validation
	public $validate = array(
		'potentialFieldID' => array(
			'field' => 'potentialFieldID',
			'label' => 'Field Name',
			'rules' => 'required|is_natural_no_zero'
		),
		'potentialReportTypeID' => array(
			'field' => 'potentialReportTypeID',
			'label' => 'Report',
			'rules' => 'required|is_natural_no_zero'
		)
	);

	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Validity checker functions & helper functions, may contain redirects
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/

}