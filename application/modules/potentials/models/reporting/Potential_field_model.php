<?php
/********************************************************************************************************
/
/ Potential "fields"
/
********************************************************************************************************/
class Potential_field_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'PotentialFields';

	//primary key
	public $primary_key = 'potentialFieldID';

	//relationships
	public $has_single = array(
		'section' => array('model' => 'potentials/reporting/potential_section_model', 'primary_key' => 'potentialFieldSectionID', 'join_key' => 'potentialFieldSectionID'),
	);

	//callbacks/observers (use function names)

	//validation
	public $validate = array(
		'text' => array(
			'field' => 'text',
			'label' => 'Text',
			'rules' => 'required'
		),
		'value' => array(
			'field' => 'value',
			'label' => 'Field Name',
			'rules' => 'required'
		),
		'potentialFieldSectionID' => array(
			'field' => 'potentialFieldSectionID',
			'label' => 'Section',
			'rules' => 'required|is_natural_no_zero'
		)
	);

	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Validity checker functions & helper functions, may contain redirects
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/

}