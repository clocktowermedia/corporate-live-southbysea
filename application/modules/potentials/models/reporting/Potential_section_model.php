<?php
/********************************************************************************************************
/
/ Sections in the "Potential" form
/
********************************************************************************************************/
class Potential_section_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'PotentialSections';

	//primary key
	public $primary_key = 'potentialFieldSectionID';

	//relationships
	public $has_many = array(
		'fields' => array('model' => 'potentials/reporting/potential_field_model', 'primary_key' => 'potentialFieldID', 'join_key' => 'potentialFieldSectionID'),
		'child_sections' => array('model' => 'potentials/reporting/potential_section_model', 'primary_key' => 'potentialFieldSectionID', 'join_key' => 'parentSectionID'),
	);

	//callbacks/observers (use function names)

	//validation
	public $validate = array(
		'text' => array(
			'field' => 'text',
			'label' => 'Text',
			'rules' => 'required'
		),
		'displayOrder' => array(
			'field' => 'displayOrder',
			'label' => 'Display Order',
			'rules' => 'is_natural'
		)
	);

	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/
	public function get_all_section_fields($parentID = 0, array $selected = array())
	{
		//get all top level sections
		$sections = $this->append('fields')->order_by('displayOrder', 'ASC')->get_many_by('parentSectionID', $parentID);

		//loop through sections, get fields
		if (count($sections) > 0)
		{
			foreach ($sections AS $section)
			{
				//get fields for this section
				if (count($section['fields']))
				{
					foreach ($section['fields'] AS $field)
					{
						$children[] = array(
							'text' => $field['text'],
							'id' => $field['potentialFieldID'],
							'icon' => 'fa fa-file',
							'state' => array(
									'selected' => (in_array($field['potentialFieldID'], $selected))? true : false,
								)
						);
					}

				} else {

					//check to see if there are any sub sections
					$child_sections = $this->count_by('parentSectionID', $section[$this->primary_key]);
					if ($child_sections > 0)
					{
						$children = $this->get_all_section_fields($section[$this->primary_key], $reportID);
					} else {
						$children = array();
					}
				}

				$returnSections[] = array(
					'id' => '',
					'text' => $section['text'],
					'icon' => 'fa fa-folder',
					'children' => $children,
					'state' => 'disabled'
				);

				unset($children);
			}

			return $returnSections;
		}
	}


	/**********************************************************************************************
	*
	* Validity checker functions & helper functions, may contain redirects
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/

}