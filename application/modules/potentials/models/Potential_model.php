<?php
/********************************************************************************************************
/
/ Potentials
/ Explanation: Potentials are basically "pending" orders...
/
********************************************************************************************************/
class Potential_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'Potentials';

	//primary key
	public $primary_key = 'potentialID';

	//set this as protected, meaning only the owner can delete it
	public $owner_only = false;

	//relationships
	public $has_many = array(
		'attachments' => array('model' => 'potentials/potential_attachment_model', 'primary_key' => 'potAttachmentID', 'join_key' => 'potentialID'),
		'contacts' => array('model' => 'potentials/potential_contact_model', 'primary_key' => 'potContactID', 'join_key' => 'potentialID'),
		'notes' => array('model' => 'potentials/potential_note_model', 'primary_key' => 'potNoteID', 'join_key' => 'potentialID'),
		'items' => array('model' => 'potentials/potential_item_model', 'primary_key' => 'potItemID', 'join_key' => 'potentialID'),
		'expenses' => array('model' => 'potentials/potential_expense_model', 'primary_key' => 'potExpenseID', 'join_key' => 'potentialID'),
	);

	//callbacks/observers (use function names)
	public $before_create = array('_create_timestamp');
	public $before_update = array('_create_update_timestamp');

	//validation
	public $validate = array(
		'title' => array(
			'field' => 'title',
			'label' => 'Title',
			'rules' => 'required'
		)
	);

	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/

	/**
	* Soft delete of entry from db by id - change status to deleted
	*/
	function soft_delete($potentialID = 0)
	{
		//make sure product id is valid
		if ($potentialID > 0)
		{
			$this->db->where($this->primary_key, $potentialID)->set('status', 'deleted')->update($this->_table);
		}

		return false;
	}

	/**
	 * Get all fields from a potential TEMPORARY
	 */
	function listingReport()
	{
		//grab everything for now...
		foreach ($this->has_many AS $relationship => $relationshipArray)
		{
			$this->append($relationship);
		}

		return $this->get_all();
	}

	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
	protected function _create_timestamp($data)
	{
		$data['dateCreated'] = $data['dateUpdated'] = date('Y-m-d H:i:s');
		return $data;
	}

	protected function _create_update_timestamp($data)
	{
		$data['dateUpdated'] = date('Y-m-d H:i:s');
		return $data;
	}
}