<?php
/********************************************************************************************************
/
/ Potential Expenses
/
********************************************************************************************************/
class Potential_expense_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'PotentialExpenses';

	//primary key
	public $primary_key = 'potExpenseID';

	//set this as protected, meaning only the owner can delete it
	public $owner_only = false;

	//relationships
	public $belongs_to = array(
		'potential' => array('model' => 'potentials/potential_model', 'primary_key' => 'potentialID')
	);
	public $has_single = array(
		'user' => array('model' => 'admins/admin_model', 'primary_key' => 'adminID', 'join_key' => 'createdBy')
	);

	//callbacks/observers (use function names)
	public $before_create = array('_create_timestamp');

	//validation
	public $validate = array(
		'potentialID' => array(
			'field' => 'potentialID',
			'label' => 'Associated Potential',
			'rules' => 'required|is_natural_no_zero'
		),
		'title' => array(
			'field' => 'title',
			'label' => 'Title',
			'rules' => 'required'
		),
		'value' => array(
			'field' => 'value',
			'label' => 'Value in $$',
			'rules' => 'required|decimal'
		)
	);

	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/
	public function getSum($potentialID = 0)
	{
		if ($potentialID > 0)
		{
			return $this->db->select_sum('value', 'total')
				->where('potentialID', $potentialID)
				->from($this->_table)
				->get()
				->row()
				->total;
		}

		return false;
	}

	/**********************************************************************************************
	*
	* Validity checker functions & helper functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
	protected function _create_timestamp($data)
	{
		$data['dateCreated'] = $data['dateUpdated'] = date('Y-m-d H:i:s');
		return $data;
	}

	protected function _create_update_timestamp($data)
	{
		$data['dateUpdated'] = date('Y-m-d H:i:s');
		return $data;
	}
}