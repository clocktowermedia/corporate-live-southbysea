<?php
/********************************************************************************************************
/
/ Potential Items Ordered
/
********************************************************************************************************/
class Potential_item_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'PotentialItems';

	//primary key
	public $primary_key = 'potItemID';

	//set this as protected, meaning only the owner can delete it
	public $owner_only = false;

	//relationships
	public $belongs_to = array(
		'potential' => array('model' => 'potentials/potential_model', 'primary_key' => 'potentialID')
	);
	public $has_single = array(
		'user' => array('model' => 'admins/admin_model', 'primary_key' => 'adminID', 'join_key' => 'createdBy')
	);

	//callbacks/observers (use function names)
	public $before_create = array('_create_timestamp');

	//validation
	public $validate = array(
		'potentialID' => array(
			'field' => 'potentialID',
			'label' => 'Associated Potential',
			'rules' => 'required|is_natural_no_zero'
		),
		'product' => array(
			'field' => 'product',
			'label' => 'Product(s)',
			'rules' => 'required'
		),
		'colors' => array(
			'field' => 'colors',
			'label' => 'Color(s)',
			'rules' => 'required'
		),
		'quantity' => array(
			'field' => 'quantity',
			'label' => 'Quantity',
			'rules' => 'required|is_natural_no_zero'
		)
	);

	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Validity checker functions & helper functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
	protected function _create_timestamp($data)
	{
		$data['dateCreated'] = $data['dateUpdated'] = date('Y-m-d H:i:s');
		return $data;
	}

	protected function _create_update_timestamp($data)
	{
		$data['dateUpdated'] = date('Y-m-d H:i:s');
		return $data;
	}
}