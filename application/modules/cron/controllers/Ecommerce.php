<?php
class Ecommerce extends Cron_controller {

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//load relevant models
		$this->load->model('ecommerce/store_model');
		$this->load->model('ecommerce/product_model');
	}

	/**
	 * Get all stores that are closing within the next two days...send email notification
	 */
	function index()
	{
		//get all stores closing soon (2 days)
		$stores = $this->store_model->append('acct_mgr')->get_many_by(array(
			'end_date >' => date('Y-m-d H:i:s'),
			'end_date <=' => date('Y-m-d H:i:s', strtotime('+48 hours')),
			'status' => 'enabled',
			'processed' => 0
		));

		//loop through stores, create email contents
		if (count($stores) > 0)
		{
			foreach ($stores AS $store)
			{
				$emailContent[$store['acct_mgr']['email']]['stores'][] = $store;
			}

			//loop through account manager emails and create email to send
			foreach ($emailContent AS $emailAddress => $data)
			{
				$thisContent = '<p>The following stores are closing soon. Please log into the <a href="/admin/ecommerce/closing">admin area</a> to review these stores and extend the store open dates or process orders.</p>';

				//loop through stores
				if (count($data['stores']))
				{
					$thisContent .= '<ul>';

					foreach ($data['stores'] AS $store)
					{
						$thisContent .= '<li>' . $store['title'] . ' (/' . $store['url_slug'] . ')</li>';
					}

					$thisContent .= '</ul>';

					//send email
					$emailData['subject'] = $this->config->item('site_name') . ' Preorder Stores Closing Soon';

					//format content into table
					$emailData['content'] = $this->convert_text_to_html_email_table($emailData['subject'], $thisContent);
					$this->load->library('css_to_inline_styles');
					$this->css_to_inline_styles->setHtml($emailData['content']);
					$this->css_to_inline_styles->setCSS(file_get_contents(APPPATH . '../assets/css/email.css'));
					$emailData['html'] = $this->css_to_inline_styles->convert();

					//load email & parser library
					$this->load->library('email');
					$this->config->load('email');

					//set reply and from info
					$this->email->from($this->config->item('site_email'), $this->config->item('site_email_name'));
					$this->email->to($emailAddress);
					#$this->email->bcc('forms@southbysea.com', 'South by Sea [Ecommerce Email]');

					//add content to email
					$this->email->subject($emailData['subject']);
					$this->email->message($emailData['html']);

					//send email
					$this->email->sbs_send();
				}
			}
		}

		die('Done sending emails.');
	}

	/**
	 * Email "subscribers" that the store is closing within 24 hours
	 */
	function subscribers()
	{
		//get all stores closing within 24 hours
		$stores = $this->store_model->append('subscribers')->get_many_by(array(
			'end_date >' => date('Y-m-d H:i:s'),
			'end_date <=' => date('Y-m-d H:i:s', strtotime('+48 hours')),
			'status' => 'enabled',
			'processed' => 0
		));

		//loop through stores, create email contents
		if (count($stores) > 0)
		{
			foreach ($stores AS $store)
			{
				if (count($store['subscribers']) > 0)
				{
					foreach ($store['subscribers'] AS $subscriber)
					{
						//subject
						$emailData['subject'] = $this->config->item('site_name') . ' ' . $store['title'] . ' Preorder Store Closing Soon';

						//build content
						$link = 'http://' . $_SERVER['HTTP_HOST'] . '/preorder/' . $store['url_slug'];
						$thisContent = "<p>The " . $store['title'] . " store is closing soon. Please visit the store by clicking <a href='" . $link . "'>here</a>, or the link below to place your order before it's too late.</p><br/>";
						$thisContent .= '<p>' . $link . '</p>';

						//format content into table
						$emailData['content'] = $this->convert_text_to_html_email_table($emailData['subject'], $thisContent);
						$this->load->library('css_to_inline_styles');
						$this->css_to_inline_styles->setHtml($emailData['content']);
						$this->css_to_inline_styles->setCSS(file_get_contents(APPPATH . '../assets/css/email.css'));
						$emailData['html'] = $this->css_to_inline_styles->convert();

						//load email & parser library
						$this->load->library('email');
						$this->config->load('email');

						//set reply and from info
						$this->email->from($this->config->item('site_email'), $this->config->item('site_email_name'));
						$this->email->to($subscriber['email']);
						#$this->email->bcc('forms@southbysea.com', 'South by Sea [Form Email]');

						//add content to email
						$this->email->subject($emailData['subject']);
						$this->email->message($emailData['html']);

						//send email
						$this->email->sbs_send();
					}
				}
			}
		}

		die('Done sending emails.');
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

}