<?php

class Admin extends Admin_controller {

	//layout variables
	public $defaultLayout;
	public $emailLayout;

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//set layout variable(s)
		$this->defaultLayout = $this->get_admin_layout();
		$this->emailLayout = $this->get_email_layout();
	}

	/**
	* admin dashboard
	*/
	function index()
	{
		//load relevant models
		$this->load->model('admins/admin_model');
		$this->load->model('products/product_model');
		$this->load->model('products/product_category_model');
		$this->load->model('designs/design_model');
		$this->load->model('designs/design_category_model');
		$this->load->model('pages/page_model');
		$this->load->model('forms/form_model');
		$this->load->model('forms/form_submission_model');

		//get forms by url
		$proofForm = $this->form_model->get_by('formUrl', 'quote-request');
		$finalSubForm = $this->form_model->get_by('formUrl', 'order-form');
		$contactForm = $this->form_model->get_by('formUrl', 'contact');
		$airstreamForm = $this->form_model->get_by('formUrl', 'school-suggestion');

		$data['unpublishedPageCount'] = $this->page_model->countUnpublished();
		$data['adminCount'] = $this->admin_model->count_by(array('status' => 'enabled'));
		$data['productCount'] = $this->product_model->count_by(array('status' => 'enabled'));
		$data['productCategoryCount'] = $this->product_category_model->count_by(array('status' => 'enabled'));
		$data['designCount'] = $this->design_model->count_by(array('status' => 'enabled'));
		$data['designCategoryCount'] = $this->design_category_model->count_by(array('status' => 'enabled'));
		$data['proofCount'] = $this->form_submission_model->count_by(array('status !=' => 'deleted', 'formID' => $proofForm['formID']));
		$data['unreadProofCount'] = $this->form_submission_model->count_by(array('status' => 'unread', 'formID' => $proofForm['formID']));
		$data['finalSubCount'] = $this->form_submission_model->count_by(array('status !=' => 'deleted', 'formID' => $finalSubForm['formID']));
		$data['unreadFinalSubCount'] = $this->form_submission_model->count_by(array('status' => 'unread', 'formID' => $finalSubForm['formID']));
		$data['contactCount'] = $this->form_submission_model->count_by(array('status !=' => 'deleted', 'formID' => $contactForm['formID']));
		$data['unreadContactCount'] = $this->form_submission_model->count_by(array('status' => 'unread', 'formID' => $contactForm['formID']));
		$data['airstreamCount'] = $this->form_submission_model->count_by(array('status !=' => 'deleted', 'formID' => $airstreamForm['formID']));
		$data['airstreamContactCount'] = $this->form_submission_model->count_by(array('status' => 'unread', 'formID' => $airstreamForm['formID']));


		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Dashboard';

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}

	/**
	* View & action for a logged in user to change their password
	*/
	function change_password()
	{
		//get user ID from session
		$adminID = (int) $this->session->userdata('adminID');

		//make sure admin user is valid/enabled
		modules::run('admins/admin/_admin_is_valid', $adminID);

		//get user info
		$data['admin'] = $this->admin_model->findByID($adminID);

		//tell it what view file to use
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Change Password';

		//set form validation rules
		$this->load->library('form_validation');
		$this->form_validation->set_rules('oldPassword', 'Old Password', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required|strong_pw');
		$this->form_validation->set_rules('password2', 'Password Confirmation', 'required|strong_pw|matches[password]');

		//if the form was submitted
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the page and show form errors
				$this->load->view($this->defaultLayout, $data);
			} else {

				//make sure old password is right
				$checkOldPassword = $this->admin_model->_checkPassword($adminID, $this->input->post('oldPassword', TRUE));

				if ($checkOldPassword == false)
				{
					$this->session->set_flashdata('error', 'Your current password is incorrect, please try again.');
					redirect('/admin/change-password');
				}

				//update password in db
				$changed = $this->admin_model->resetPassword($adminID, $this->input->post('password', TRUE));

				if ($changed == false)
				{
					//display error message
					$this->session->set_flashdata('error', 'There was problem changing your password, please try again.');
					redirect('/admin/change-password');
				} else {
					//display success message
					$this->session->set_flashdata('success', 'Your password has been updated.');
					redirect('/admin');
				}
			}
		} else {
			// tell it what layout to use and load the page
			$this->load->view($this->defaultLayout, $data);
		}
	}

	function _get_dashboard_counts()
	{


		return $counts;
	}
}
