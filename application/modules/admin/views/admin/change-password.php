<div class="row">
	<div class="col-xs-12">
		<div class="form-horizontal">
			<?php echo form_open('admin/change-password'); ?>
			<fieldset>
				<?php
					//create array for classes
					$data['containerArray'] = array('labelClass' => 'col-sm-2', 'containerClass' => 'col-sm-10');

					//old password field
					$this->form_builder->password('oldPassword', 'Current Password:', '', $data['containerArray'], '', '', '', array('required' => 'required'));

					//load password form
					$this->load->view('../modules/admin_auth/partials/password-form', $data);
					
					//submit button
					$this->form_builder->button('submit', 'Save', array('containerClass' => 'col-sm-offset-2 col-sm-10' , 'inputClass' => 'btn btn-info'), '', '');
				?>
			</fieldset>
			</form>
		</div>
	</div>
</div>