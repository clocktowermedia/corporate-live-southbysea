<div class="row">

	<div class="col-lg-3 col-xs-6">
		<div class="small-box bg-green">
			<div class="inner">
				<h3><?php echo $unreadFinalSubCount; ?></h3>
				<p>Unread Order Forms</p>
			</div>
			<div class="icon">
				<i class="ion ion-ios7-download"></i>
			</div>
			<a href="/admin/forms" class="small-box-footer">
				More info <i class="fa fa-arrow-circle-right"></i>
			</a>
		</div>
	</div>

	<div class="col-lg-3 col-xs-6">
		<div class="small-box bg-yellow">
			<div class="inner">
				<h3><?php echo $finalSubCount;?></h3>
				<p>Order Forms</p>
			</div>
			<div class="icon">
				<i class="ion ion-ios7-download-outline"></i>
			</div>
			<a href="/admin/forms" class="small-box-footer">
				More info <i class="fa fa-arrow-circle-right"></i>
			</a>
		</div>
	</div>

	<div class="col-lg-3 col-xs-6">
		<div class="small-box bg-aqua">
			<div class="inner">
				<h3><?php echo $unreadProofCount;?></h3>
				<p>Unread Quote Requests</p>
			</div>
			<div class="icon">
				<i class="ion ion-android-chat"></i>
			</div>
			<a href="/admin/forms" class="small-box-footer">
				More info <i class="fa fa-arrow-circle-right"></i>
			</a>
		</div>
	</div>

	<div class="col-lg-3 col-xs-6">
		<div class="small-box bg-red">
			<div class="inner">
				<h3><?php echo $proofCount;?></h3>
				<p>Quote Requests</p>
			</div>
			<div class="icon">
				<i class="ion ion-quote"></i>
			</div>
			<a href="/admin/forms" class="small-box-footer">
				More info <i class="fa fa-arrow-circle-right"></i>
			</a>
		</div>
	</div>

	<div class="col-lg-3 col-xs-6">
		<div class="small-box bg-orange">
			<div class="inner">
				<h3><?php echo $unreadContactCount;?></h3>
				<p>Unread Contact Forms</p>
			</div>
			<div class="icon">
				<i class="ion ion-ios7-email"></i>
			</div>
			<a href="/admin/forms" class="small-box-footer">
				More info <i class="fa fa-arrow-circle-right"></i>
			</a>
		</div>
	</div>

	<div class="col-lg-3 col-xs-6">
		<div class="small-box bg-olive">
			<div class="inner">
				<h3><?php echo $contactCount;?></h3>
				<p>Contact Forms</p>
			</div>
			<div class="icon">
				<i class="ion ion-ios7-email-outline"></i>
			</div>
			<a href="/admin/forms" class="small-box-footer">
				More info <i class="fa fa-arrow-circle-right"></i>
			</a>
		</div>
	</div>

	<div class="col-lg-3 col-xs-6">
		<div class="small-box bg-fuchsia">
			<div class="inner">
				<h3><?php echo $adminCount;?></h3>
				<p>Administators</p>
			</div>
			<div class="icon">
				<i class="ion ion-person-add"></i>
			</div>
			<a href="/admin/admins" class="small-box-footer">
				More info <i class="fa fa-arrow-circle-right"></i>
			</a>
		</div>
	</div>

</div>
