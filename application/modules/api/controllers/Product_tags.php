<?php
class Product_tags extends REST_controller {

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//load the models we need
		$this->load->model('products/product_tag_model');
	}

	/**
	 * Get product by id, url, or all products
	 */
	public function index_get()
	{

		//get tags by category id, or all get unique tags
		if ($this->input->get('category_id')) {

			$tags = $this->product_tag_model->get_unique_category_tags((int) $this->input->get('category_id'), 'text');
			$this->response($tags, 200);

		} else {

			//get all top level categories
			$tags = $this->product_category_model->get_unique_tags('text');
			$this->response($tags, 200);
		}
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

}