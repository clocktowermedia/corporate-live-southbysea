<?php
class Product_categories extends REST_controller {

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//load the models we need
		$this->load->model('products/product_category_model');
	}

	/**
	 * Get product by id, url, or all products
	 */
	public function index_get()
	{

		if ($this->input->get('id')) {

			$category = $this->product_category_model->get((int) $this->input->get('id'));

		} else if ($this->input->get('url')) {

			$category = $this->product_category_model->get_by('url', $this->input->get('url'));
		}

		if (isset($category))
		{
			//return bad status if category not found
			if ($category == false)
			{
				$this->response('Product category not found.', 400);
			}

			//see if there are any sub categories
			$categories = $this->product_category_model->append('image')
				->order_by('displayOrder')
				->get_many_by(array(
					'status' => 'enabled',
					'parentID' => $category['productCategoryID']
				));

			//if there are no subcategories, get products
			if ($categories == false || count($categories) <= 0)
			{
				$this->load->model('products/product_model');
				$products = $this->product_model->append('image')->order_by('displayOrder')
					->get_many_by(array(
						'status' => 'enabled',
						'productCategoryID' => $category['productCategoryID']
					));

				$category['products'] = $products;

			} else {
				$category['children'] = $categories;
			}

			//return category
			$this->response($category, 200);

		} else {

			//get all top level categories
			$categories = $this->product_category_model->append('image')->append('children')->order_by('displayOrder')
				->get_many_by(array(
					'status' => 'enabled',
					'parentID' => 0
				));

			//return bad status if none found
			if ($categories == false)
			{
				$this->response('No product categories found', 400);
			}

			//send response
			$this->response($categories, 200);
		}
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

}