<?php
class Pages extends REST_controller {

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//load the models we need
		$this->load->model('pages/page_model');
	}

	/**
	 * Get page by path, or all pages
	 */
	public function index_get()
	{
		//special check for images only
		if ($this->input->get('request') == 'images')
		{
			if ((int) $this->input->get('id') >= 0)
			{
				//load model
				$this->load->model('pages/page_image_model');

				//get images and send
				$images = $this->page_image_model->get_many_by('pageID', (int) $this->input->get('id'));
				$this->response($images, 200);

			} else {
				$this->response('Invalid request.', 400);
			}

		} else {

			//get page
			if ($this->input->get('url')) {

				//get page
				$page = $this->page_model->where('status', 'enabled')->append('page_images')->get_by('url', $this->input->get('url'));


			} else if ($this->input->get('url_path')) {

				//get page
				$page = $this->page_model->where('status', 'enabled')->append('page_images')->get_by('urlPath', $this->input->get('url_path'));


			} else if ($this->input->get('id')) {

				//get page
				$page = $this->page_model->where('status', 'enabled')->append('page_images')->get((int) $this->input->get('id'));

			}

			//send page back if applicable
			if (isset($page))
			{
				//return bad status if page not found
				if ($page == false)
				{
					$this->response('Page not found.', 400);
				}

				//send response
				$this->response($page, 200);

			} else {

				//get all pages
				$pages = $this->page_model->append('page_images')->get_many_by('status', 'enabled');

				//make sure pages were found
				if ($pages == false)
				{
					$this->response('No pages found.', 400);
				}

				//send response
				$this->response($pages, 200);
			}
		}
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

}