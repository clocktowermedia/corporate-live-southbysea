<?php
class Products extends REST_controller {

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//load the models we need
		$this->load->model('products/product_model');
		$this->load->model('products/product_color_model');
	}

	/**
	 * Get product by id, url, or category ID
	 */
	public function index_get()
	{

		if ($this->input->get('id')) {

			//get product by id
			$product = $this->product_model->where('status', 'enabled')->append('image')->get((int) $this->input->get('id'));

			//return bad status if product not found
			if ($product == false)
			{
				$this->response('Product not found.', 400);
			}

			//append colors
			$product['colors'] = $this->product_color_model->append('product_image')
				->append_select('fullpath, thumbFullpath')
				->get_many_by('productID', $product['productID']);

			//send response
			$this->response($product, 200);

		} else if ($this->input->get('url')) {

			//get product by url
			$product = $this->product_model->where('status', 'enabled')->append('image')->get_by('url', $this->input->get('url'));

			//send bad status if product not found
			if ($product == false)
			{
				$this->response('Product not found.', 400);
			}

			//append colors
			$product['colors'] = $this->product_color_model->append('product_image')
				->append_select('fullpath, thumbFullpath')
				->get_many_by('productID', $product['productID']);

			//send response
			$this->response($product, 200);

		} else if ($this->input->get('category_id')) {

			//get products by category id
			$products = $this->propuct_model->append('image')->get_many_by(array(
				'status' => 'enabled',
				'productCategoryID' => (int) $this->input->get('category_id')
			));

			//return bad status if no products found
			if ($products == false)
			{
				$this->response('No products found.', 400);
			}

			//send response
			$this->response($products, 200);

		} else {

			$this->response('Invalid request', 400);
		}
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

}