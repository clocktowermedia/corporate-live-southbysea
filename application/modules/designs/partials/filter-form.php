<form id="design-filter-form" method="get" role="form">
	<div class="form-group">
		<select id="design-tags" name="filters" class="form-control" size="2">
			<?php foreach ($tags as $tag): ?>
				<?php $selected = ($this->input->get('filters', TRUE) == $tag['id'])? ' selected="selected"' : ''; ?>
				<option value="<?php echo $tag['id']; ?>" <?php echo $selected;?>><?php echo $tag['text']; ?></option>
			<?php endforeach; ?>
		</select>
	</div>
</form>

<div class="clearfix"></div>