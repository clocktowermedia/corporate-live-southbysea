<?php
	//title
	$this->form_builder->text('title', 'Title', (isset($design['title']))? $design['title'] : '', '', '', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));
	//url
	$urlReadOnly = ($this->uri->segment(3) == 'edit')? array() : array();
	$this->form_builder->text('url', 'URL', (isset($design['url']))? $design['url'] : '', '', '', 'Alphanumeric characters only. For SEO purposes, use lowercase letters and dashes to indicate spaces.', '', array('pattern' => '^[a-z0-9-]+$', 'required' => 'required') + $urlReadOnly);
	//category
	$this->form_builder->select('designCategoryID', 'Category', $categories, (isset($design['designCategoryID']))? $design['designCategoryID'] : '', '', '', '', array());
    $this->form_builder->select('type', 'Type', array('S'=>'Sorority','F'=>'Fraternity','U'=>'Both'), (isset($design['type']))? $design['type'] : '', '', '', '', array('required' => 'required'), true);

?>


<?php
	$this->form_builder->select('status', 'Status', array('disabled' => 'disabled', 'disabled' => 'disabled'), (isset($product['status']))? $product['status'] : '', '', '', '', array('required' => 'required'), true);
	//short description
	$this->form_builder->textarea('shortDesc', 'Short Description', (isset($design['shortDesc']))? $design['shortDesc'] : '', array('inputClass' => 'wysiwyg short'), 10, '', '', '', array());
	//long description
	//$this->form_builder->textarea('longDesc', 'Long Description', (isset($design['longDesc']))? $design['longDesc'] : '', array('inputClass' => 'wysiwyg'), 10, '', '', '', array('required' => 'required'));
?>

	<?php if (isset($design['designImageID']) && $design['designImageID'] > 0): ?>
		<div class="form-group">
			<label class="control-label" for="userfile">Current Image:</label><br/>
			<a target="_blank" href="<?php echo $design['design_image']['fullpath']; ?>"><img src="<?php echo $design['design_image']['thumbFullpath']; ?>" class="img-thumbnail"></a>
		</div>
	<?php endif; ?>

	<div class="form-group" id="category-image-upload">
		<label class="control-label" for="userfile">Image</label>
		<input type="file" name="userfile" id="userfile" size="20" class="form-control"/>
		<?php if (isset($design['designImageID']) && $design['designImageID'] > 0): ?>
			<p class="help-block">If you upload a new image, the image above will be replaced.</p>
		<?php endif; ?>
	</div>

<?php
	//tags
	$this->form_builder->psuedo_hidden('tags', "Tags (Separate tags using commas or by pressing the 'Enter' key)", (isset($design['tags']))? $design['tags'] : '', '', '', '', '', array('id' => 'tags'));

	//featured design
	$this->form_builder->checkbox('featuredInCategory', 'Yes', 0, 1, (isset($design['featuredInCategory']))? $design['featuredInCategory'] : '', '', 'Featured on Category Page?', '', array());

	//status
	$this->form_builder->select('status', 'Status', array('enabled' => 'enabled', 'disabled' => 'disabled'), (isset($design['status']))? $design['status'] : '', '', '', '', array('required' => 'required'), true);
?>
