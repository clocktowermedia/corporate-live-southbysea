<?php
	//title
	$this->form_builder->text('title', 'Title', (isset($category['title']))? $category['title'] : '', '', '', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));

	//url
	$urlReadOnly = (isset($category) && $category['url'] == 'new')? array('readonly' => 'readonly') : array();
	$this->form_builder->text('url', 'URL', (isset($category['url']))? $category['url'] : '', '', '', 'Alphanumeric characters only. For SEO purposes, use lowercase letters and dashes to indicate spaces.', '', array('pattern' => '^[a-z0-9-]+$', 'required' => 'required') + $urlReadOnly);

	//date range picker
	if (isset($category) && $category['url'] == 'new')
	{
		$this->form_builder->select('dateRange', 'Show designs within', array('' => 'Select a Number of Days', '30' => '30 days', '60' => '60 days', '90' => '90 days'), (isset($category['dateRange']))? $category['dateRange'] : '', '', '', '', array('required' => 'required'));
	}

	//description
	$this->form_builder->textarea('shortDesc', 'Short/Meta Description', (isset($category['shortDesc']))? $category['shortDesc'] : '', '', 3, '', '', '', array());

	//meta keywords
	$this->form_builder->textarea('metaKeywords', 'Meta Keywords', (isset($category['metaKeywords']))? $category['metaKeywords'] : '', '', 3, '', 'Design tags will be used instead if there is no entry here', '', array());
?>

	<?php if (isset($category['designCategoryImageID']) && $category['designCategoryImageID'] > 0): ?>
		<div class="form-group">
			<label class="control-label" for="userfile">Current Image</label><br/>
			<img src="<?php echo $category['design_category_image']['thumbFullpath']; ?>" class="img-thumbnail">
		</div>
	<?php endif; ?>

	<div class="form-group" id="category-image-upload">
		<label class="control-label" for="userfile">Image</label>
		<input type="file" name="userfile" id="userfile" size="20" class="form-control"/>
		<?php if (isset($category['designCategoryImageID']) && $category['designCategoryImageID'] > 0): ?>
			<p class="help-block">If you upload a new image, the image above will be replaced.</p>
		<?php endif; ?>
	</div>