<?php
class Admin extends Admin_controller {

	//layout variables
	public $defaultLayout;

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//set layout variable(s)
		$this->defaultLayout = $this->get_admin_layout();

		//load relevant models
		$this->load->model('designs/design_model');
		$this->load->model('designs/design_category_model');
		$this->load->model('designs/design_tag_model');
	}

	/**
	* Shows all designs in a datatable
	*/
	function index()
	{
		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Designs';

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}

	/**
	* view and action for adding a design
	*/
	function create()
	{
		//tell it what view file to use
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Add Design';

		//grab categories for dropdown
		$data['categories'] = $this->design_category_model->getDropdown();

		//load form validation library
		$this->load->library('form_validation');

		//get validation rules from model, set extra options as neccessary
		$validationRules = $this->design_model->validate;
		$validationRules['url']['rules'] .= '|is_unique[' . $this->design_model->_table . '.url]';

		//set form validation rules
		$this->form_validation->set_rules($validationRules);

		//if the form was submitted, update database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the design and show form errors
				$data['design'] = $this->input->post(NULL, TRUE);
				$this->load->view($this->defaultLayout, $data);

			} else {

				//get insert data
				$insertData = $this->input->post(NULL, TRUE);

				//upload image if applicable
				if (isset($_FILES) && $_FILES['userfile']['name'] != '')
				{
					$imageID = modules::run('designs/admin_image_handler/_upload_image', array(), 'design');
					if (is_int($imageID) && $imageID > 0)
					{
						$insertData['designImageID'] = $imageID;
					} else {
						$imageID = (gettype($imageID) != 'string' && gettype($imageID) != 'boolean')? 'Image could not be uploaded.' : $imageID;
						$this->session->set_flashdata('error', $imageID);
						redirect('/admin/designs/create');
					}
				}

				//insert design
				$designID = $this->design_model->insert($insertData);

				//update tags if applicable
				if ($this->input->post('tags', TRUE) != '')
				{
					$tags = explode(',', $this->input->post('tags', TRUE));
					foreach ($tags as $tag)
					{
						$this->design_tag_model->insert(array(
							'designID' => $designID,
							'tag' => $tag
						));
					}
				}

				//display success message & redirect
				$this->session->set_flashdata('success', 'Design was successfully created.');
				redirect('/admin/designs');
			}
		} else {
			// tell it what layout to use and load the design
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	* view and action for editing a design
	*/
	function edit($designID = 0)
	{
		//make sure design exists
		$this->_design_is_valid($designID);

		//grab design info for view
		$data['design'] = $this->design_model->append('design_image')->get($designID);

		//tell it what view file to use
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Edit ' . $data['design']['title'];

		//grab categories for dropdown
		$data['categories'] = $this->design_category_model->getDropdown();

		//load form validation library
		$this->load->library('form_validation');

		//get validation rules from model, set extra options as neccessary
		$validationRules = $this->design_model->validate;

		$this->form_validation->set_rules($validationRules);

		//if the form was submitted, update database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the design and show form errors
				$this->load->view($this->defaultLayout, $data);
			} else {
				//double check that the url was not edited
				$exists = $this->design_model->get_by(array(
					'designID !=' => $designID,
					'url' => $this->input->post('url', TRUE)
				));
				if (!empty($exists) || isset($exists['designID']))
				{
					$this->session->set_flashdata('error', 'Design was not updated, an error occurred.');
					redirect('/admin/designs/edit/' . $designID);
				}

				//get insert data
				$updateData = $this->input->post(NULL, TRUE);

				//upload image if applicable
				if (isset($_FILES) && $_FILES['userfile']['name'] != '')
				{
					$imageID = modules::run('designs/admin_image_handler/_upload_image', array(), 'design');
					if (is_int($imageID) && $imageID > 0)
					{
						$updateData['designImageID'] = $imageID;
					} else {
						$imageID = (gettype($imageID) != 'string' && gettype($imageID) != 'boolean')? 'Image could not be uploaded.' : $imageID;
						$this->session->set_flashdata('error', $imageID);
						redirect('/admin/designs/edit/' . $designID);
					}
				}

				//update design
				$this->design_model->update($designID, $updateData);

				//update tags if applicable
				if ($this->input->post('tags', TRUE) != '')
				{
					$tags = explode(',', $this->input->post('tags', TRUE));
					$this->design_tag_model->delete_by('designID', $designID);
					foreach ($tags as $tag)
					{
						$this->design_tag_model->insert(array(
							'designID' => $designID,
							'tag' => $tag
						));
					}
				}

				//display success message & redirect
				$this->session->set_flashdata('success', 'Design was successfully updated.');
				redirect('/admin/designs');
			}
		} else {
			// tell it what layout to use and load the design
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	* Changes design status to deleted
	*/
	function delete($designID = 0)
	{
		//make sure design exists
		$this->_design_is_valid($designID);

		//set status to deleted
		$this->design_model->soft_delete($designID);

		//redirect upon success
		$this->session->set_flashdata('success', 'Design was deleted.');
		redirect('/admin/designs/');
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

	/**
	* Checks to see that design ID is valid and that the design exists
	*/
	public function _design_is_valid($designID = 0)
	{
		//make sure design id is an integer
		$designID = (int) $designID;

		//make sure design id is greater than zero
		if ($designID > 0)
		{
			//check to see if the design exists
			$exists = $this->design_model->get($designID);

			if ($exists == false)
			{
				$this->session->set_flashdata('error', 'Invalid design id.');
				redirect('/admin/designs');
			}

		} else {
			$this->session->set_flashdata('error', 'Invalid design id.');
			redirect('/admin/designs');
		}
	}
}