<?php
class Designs_json extends MY_Controller {

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//load relevant models
		$this->load->model('designs/design_model');
	}

	function results()
	{
		try
		{
			//make sure we have a valid request
			if (!$this->is_post())
			{
				throw new Exception('Invalid request.');
			}

			//set defaults for limit/offset
			$limit = ($this->input->post('show', TRUE) == '')? 12 : (int) $this->input->post('show', TRUE);
			$limit = ($limit > 100)? 100 : $limit; //make sure limit is not over 100
			$offset = ($this->input->post('p', TRUE) == '')? 0 : (int) $this->input->post('p', TRUE) * $limit;

			//get results
			$results = $this->design_model->getByTags((int) $this->input->post('catID', TRUE), $this->input->post('tags', TRUE), $limit + 1, $offset);

			//if matches were found, see if there are more
			if (!empty($results) && count($results) > 0)
			{
				//see if we should show more or not
				if (count($results) > $limit)
				{
					$data['showMore'] = true;
					unset($results[$limit]);
				}
			}

			$data['results'] = $results;
			$data['page'] = (int) $this->input->post('p', TRUE) + 1;
			$data['search'] = true;
			$data['status'] = 'ok';
			$data['message'] = ($results == false || empty($results))? 'No results found' : 'Found matches.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/
}