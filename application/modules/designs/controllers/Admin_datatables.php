<?php
class Admin_datatables extends Admin_controller {

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//load model
		$this->load->model('designs/design_model');
		$this->load->model('designs/design_category_model');
	}

	/**
	* uses datatables library to get list of pages in a json format for use in admin index
	*/
	function data()
	{
		//load library
		$this->load->library('datatables');

		//grab data from admins table in db
		$output = $this->datatables->select('d.designID, d.title, d.url')
			->select('dc.title AS category', false)
			->select('d.status')
			->select('d.dateUpdated')
			->add_column('actions',  '<div class="btn-group">
					<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-picture-o"></i> Design <span class="caret"></span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li><a href="/admin/designs/edit/$1"><i class="fa fa-edit"></i> Edit</a></li>
						<li><a data-confirm="Are you sure you want to delete this?" href="/admin/designs/delete/$1"><i class="fa fa-trash-o"></i> Delete</a></li>
					</ul>
				</div>', 'd.designID')
			->unset_column('d.designID')
			->from($this->design_model->_table . ' d')
			->join($this->design_category_model->_table . ' dc', 'd.designCategoryID = dc.designCategoryID', 'left outer')
			->where('d.status !=', 'deleted')
			->generate();

		echo $output;
	}

	/**
	* uses datatables library to get list of categories in a json format for use in admin index
	*/
	function categories()
	{
		//load library
		$this->load->library('datatables');

		//grab data from admins table in db
		$output = $this->datatables->select('c.designCategoryID, c.title, c.dateUpdated')
			->select('CONCAT("<li><a href=\"/admin/designs/categories/reorder-designs/", c.designCategoryID, "\"><i class=\"fa fa-arrows\"></i> Reorder Designs</a></li>") AS reorderButton', false)
			->add_column('actions',  '<div class="btn-group">
					<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-outdent"></i> Category <span class="caret"></span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li><a href="/admin/designs/categories/edit/$1"><i class="fa fa-edit"></i> Edit</a></li>
						$2
						<li><a data-confirm="Are you sure you want to delete this?" href="/admin/designs/categories/delete/$1"><i class="fa fa-trash-o"></i> Delete</a></li>
					</ul>
				</div>', 'c.designCategoryID, reorderButton')
			->unset_column('c.designCategoryID')
			->unset_column('reorderButton')
			->from($this->design_category_model->_table . ' AS c')
			->where('c.status !=', 'deleted')
			->generate();

		echo $output;
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

}