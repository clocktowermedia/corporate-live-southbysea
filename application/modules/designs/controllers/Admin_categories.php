<?php
class Admin_categories extends Admin_controller {

	//layout variables
	public $defaultLayout;

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//set layout variable(s)
		$this->defaultLayout = $this->get_admin_layout();

		//load relevant models
		$this->load->model('designs/design_category_model');
	}

	/**
	* Shows all designs in a datatable
	*/
	function index()
	{
		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->_get_custom_view();
		$data['title'] = 'Design Categories';

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}

	/**
	* view and action for adding a category
	*/
	function create()
	{
		//tell it what view file to use
		$data['view'] = $this->_get_custom_view();
		$data['title'] = 'Add Design Category';

		//load form validation library
		$this->load->library('form_validation');

		//get validation rules from model, set extra options as neccessary
		$validationRules = $this->design_category_model->validate;
		$validationRules['url']['rules'] .= '|is_unique[' . $this->design_category_model->_table . '.url]';

		//set form validation rules
		$this->form_validation->set_rules($validationRules);

		//if the form was submitted, update database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the category and show form errors
				$data['category'] = $this->input->post(NULL, TRUE);
				$this->load->view($this->defaultLayout, $data);
			} else {

				$insertData = $this->input->post(NULL, TRUE);

				//upload image if applicable
				if (isset($_FILES) && $_FILES['userfile']['name'] != '')
				{
					$imageID = modules::run('designs/admin_image_handler/_upload_image', array(), 'category');
					if (is_int($imageID) && $imageID > 0)
					{
						$insertData['designCategoryImageID'] = $imageID;
					} else {
						$imageID = (gettype($imageID) != 'string' && gettype($imageID) != 'boolean')? 'Image could not be uploaded.' : $imageID;
						$this->session->set_flashdata('error', $imageID);
						redirect('/admin/designs/categories/edit/' . $categoryID);
					}
				}

				// insert record to db & redirect
				$categoryID = $this->design_category_model->insert($insertData);

				//display success message & redirect
				$this->session->set_flashdata('success', 'Design category successfully created.');
				redirect('/admin/designs/categories');
			}
		} else {
			// tell it what layout to use and load the category
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	* view and action for editing a category
	*/
	function edit($categoryID = 0)
	{
		//make sure category exists
		$this->_category_is_valid($categoryID);

		//grab category info for view
		$data['category'] = $this->design_category_model->append('design_category_image')->get($categoryID);

		//tell it what view file to use
		$data['view'] = $this->_get_custom_view();
		$data['title'] = 'Edit ' . $data['category']['title'];

		//load form validation library
		$this->load->library('form_validation');

		//get validation rules from model, set extra options as neccessary
		$validationRules = $this->design_category_model->validate;
		$this->form_validation->set_rules($validationRules);

		//if the form was submitted, update database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the category and show form errors
				$this->load->view($this->defaultLayout, $data);
			} else {
				//double check that the url was not edited
				$exists = $this->design_category_model->get_by(array(
					'url' => $this->input->post('url', TRUE),
					'designCategoryID !=' => $categoryID
				));
				if (!empty($exists) || isset($exists['designCategoryID']))
				{
					$this->session->set_flashdata('error', 'A category with this url alredy exists.');
					redirect('/admin/designs/categories/edit/' . $categoryID);
				}

				$updateData = $this->input->post(NULL, TRUE);

				//upload image if applicable
				if (isset($_FILES) && $_FILES['userfile']['name'] != '')
				{
					$imageID = modules::run('designs/admin_image_handler/_upload_image', array(), 'category');
					if (is_int($imageID) && $imageID > 0)
					{
						$updateData['designCategoryImageID'] = $imageID;
					} else {
						$imageID = (gettype($imageID) != 'string' && gettype($imageID) != 'boolean')? 'Image could not be uploaded.' : $imageID;
						$this->session->set_flashdata('error', $imageID);
						redirect('/admin/designs/categories/edit/' . $categoryID);
					}
				}

				// update record to db & redirect
				$this->design_category_model->update($categoryID, $updateData);

				//display success message & redirect
				$this->session->set_flashdata('success', 'Design category was successfully updated.');
				redirect('/admin/designs/categories');
			}
		} else {
			// tell it what layout to use and load the category
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	* Changes category status to deleted
	*/
	function delete($categoryID = 0)
	{
		//make sure category exists
		$this->_category_is_valid($categoryID);

		//set status to deleted
		$this->design_category_model->soft_delete($categoryID);

		//redirect upon success
		$this->session->set_flashdata('success', 'Design category was deleted.');
		redirect('/admin/designs/categories');
	}

	/**
	* View for re-ordering categories
	*/
	function reorder()
	{
		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->_get_custom_view();
		$data['title'] = 'Reorder Categories';

		//get a list of items
		$data['categories'] = $this->design_category_model->order_by('displayOrder')->get_many_by('status != ', 'deleted');

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}

	/**
	* View for re-ordering designs (featured and regular) w/n a category
	*/
	function reorder_designs($categoryID = 0)
	{
		//make sure category exists
		$this->_category_is_valid($categoryID);

		//get category info
		$data['category'] = $this->design_category_model->get($categoryID);

		//get design info
		$this->load->model('designs/design_model');
		$data['featuredDesigns'] = $this->design_model->getByCategory($categoryID, true);
		$data['designs'] = $this->design_model->getByCategory($categoryID);

		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->_get_custom_view();
		$data['title'] = 'Reorder ' . $data['category']['title'] . ' Designs';

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

	private function _get_custom_view()
	{
		return 'admin/categories/' . $this->get_view();
	}

	/**
	* Checks to see that category ID is valid and that the category exists
	*/
	public function _category_is_valid($categoryID = 0)
	{
		//make sure category id is an integer
		$categoryID = (int) $categoryID;

		//make sure category id is greater than zero
		if ($categoryID > 0)
		{
			//check to see if the category exists
			$exists = $this->design_category_model->get($categoryID);

			if ($exists == false)
			{
				$this->session->set_flashdata('error', 'Invalid design category id.');
				redirect('/admin/designs/categories');
			}

		} else {
			$this->session->set_flashdata('error', 'Invalid design category id.');
			redirect('/admin/designs/categories');
		}
	}
}