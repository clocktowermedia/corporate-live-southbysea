<?php
class Admin_image_handler extends Admin_controller {

	//image upload variables
	protected $uploadFolder;
	protected $imgUploadFolder;
	protected $imgThumbUploadFolder;

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		$this->uploadFolder = 'assets/uploads/';
	}

	public function _set_type($imageType = 'design')
	{
		switch ($imageType)
		{
			case 'design':
				$this->imgUploadFolder = 'assets/uploads/images/designs/';
				$this->imgThumbUploadFolder = 'assets/uploads/images/designs/thumbnails/';
				$modelName = 'designs/design_image_model';
				$thumbConfig['width'] = 209;
				$thumbConfig['height'] = 284;
				break;
			case 'category':
				$this->imgUploadFolder = 'assets/uploads/images/designs/categories/';
				$this->imgThumbUploadFolder = 'assets/uploads/images/designs/categories/thumbnails/';
				$modelName = 'designs/design_category_image_model';
				$thumbConfig['width'] = 209;
				$thumbConfig['height'] = 284;
				break;
			case 'color':
				$this->imgUploadFolder = 'assets/uploads/images/designs/colors/';
				$this->imgThumbUploadFolder = 'assets/uploads/images/designs/colors/thumbnails/';
				$modelName = 'designs/design_color_image_model';
				$thumbConfig['width'] = 60;
				$thumbConfig['height'] = 60;
				break;
		}

		return array('modelName' => $modelName, 'thumbConfig' => $thumbConfig);
	}

	/**
	* Function for uploading a file
	*/
	public function _upload_image(array $formData = array(), $imageType = 'design')
	{
		//get model we want to save this to & load it
		$typeInfo = $this->_set_type($imageType);
		$this->load->model($typeInfo['modelName'], 'image_model');

		//retrieve file names & replace yucky characters
		$name = $_FILES['userfile']['name'];
		$name = strtr($name, 'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
		$name = preg_replace('/([^.a-z0-9]+)/i', '_', $name);

		//get file extension
		$extension = strrchr($name, '.');

		//define image types
		$imageTypes = array('.gif', '.jpg', '.jpeg', '.png');

		//Your upload directory and some config settings
		$config['upload_path'] = (in_array($extension, $imageTypes))? $this->imgUploadFolder : $this->uploadFolder;
		$config['max_size'] = '10000';
		$config['file_name'] = $name;
		$config['allowed_types'] = 'gif|jpg|png|JPG|GIF|PNG';

	   	//Load the upload library
		$this->load->library('upload', $config);

		//attempt upload
	   	if ($this->upload->do_upload())
		{
			//get upload data
			$data = $this->upload->data();

			//if the file is an image...
			if ($data['is_image'] == true)
			{
				//Config for thumbnail creation
				$thumbConfig = $typeInfo['thumbConfig'];
				$thumbConfig['new_image'] = $this->imgThumbUploadFolder;
				$thumbConfig['image_library'] = 'gd2';
				$thumbConfig['source_image'] = $config['upload_path'] . $data['file_name'];
				$thumbConfig['create_thumb'] = TRUE; //this adds _thumb to the end of the file name
				$thumbConfig['maintain_ratio'] = TRUE;

				//create thumbnails
				$this->load->library('image_lib', $thumbConfig);
				$this->image_lib->initialize($thumbConfig);
				if (!$this->image_lib->resize())
				{
					return $this->image_lib->display_errors();
				}

				//grab thumbnail info
				$thumbName = str_replace($extension, '', $data['file_name']) . '_thumb' . $extension;
				$thumbPath = $this->imgThumbUploadFolder . $thumbName;
				$thumbFilesize = round((filesize($thumbPath) / 1024 ), 2);
				$thumbInfo = getimagesize($thumbPath);

				//set image data
				$imageData = array(
					'width' => $data['image_width'],
					'height' => $data['image_height'],
					'widthHeightString' => $data['image_size_str'],
					'thumbFilename' => $thumbName,
					'thumbFilepath' => '/' . $this->imgThumbUploadFolder,
					'thumbFullPath' => '/' . $this->imgThumbUploadFolder . $thumbName,
					'thumbExtension' => $extension,
					'thumbMimeType' => $thumbInfo['mime'],
					'thumbFileSize' => $thumbFilesize,
					'thumbWidth' => $thumbInfo[0],
					'thumbHeight' => $thumbInfo[1],
					'thumbWidthHeightString' => $thumbInfo[3]
				);

			} else {
				//set image data to empty
				$imageData = array();
			}

			//regular file data
			$fileData = array(
				'filename' => $data['file_name'],
				'filepath' => '/' . $config['upload_path'],
				'fullpath' => '/' . $config['upload_path'] . $data['file_name'],
				'extension' => $data['file_ext'],
				'mimeType' => $data['file_type'],
				'fileSize' => $data['file_size']
			);

			//merge image, file data, and form data together
			$fileData = array_merge($fileData, $imageData, $formData);

			//insert in db
			$imageID = $this->image_model->insert($fileData);

			//return file id
			return $imageID;

		} else {

			//return errors
			return $this->upload->display_errors();
		}
	}
}