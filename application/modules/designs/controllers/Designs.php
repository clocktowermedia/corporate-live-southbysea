<?php
class Designs extends MY_Controller {

	//layout variables
	public $defaultLayout;
	public $perPage = 18;

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//set layout variable(s)
		$this->defaultLayout = $this->get_default_layout();

		//load relevant models
		$this->load->model('designs/design_model');
		$this->load->model('designs/design_tag_model');
		$this->load->model('designs/design_category_model');
	}

	/**
	* Shows design categories
	*/
	function index()
	{
		if (strpos($_SERVER[REQUEST_URI], 'fraternity')) {

				//tell it what view to use, this is always called the view variable
				$data['view'] = $this->get_view();
				$data['title'] = 'Designs';

				//get design page content for seo purposes
				$data['page'] = modules::run('pages/get', 'designs');
				$data['metaKeywords'] = ($data['page']['metaKeywords'] != '')? $data['page']['metaKeywords'] : $this->design_tag_model->get_unique_tags('text');

				//get designs & result count
				$offset = ($this->input->get('page', TRUE) != '')? (((int) $this->input->get('page', TRUE) - 1) * $this->perPage) : 0;
				if ($this->input->get('filters', TRUE) != '')
				{
					$data['designs'] = $this->design_model->where('type !=','S')->getByTags(0, $this->input->get('filters', TRUE), $this->perPage, $offset);
					$data['totalDesigns'] = $this->design_model->countByTags(0, $this->input->get('filters', TRUE));
				} else {
					$data['designs'] = $this->design_model->where('type !=','S')->getByTags(0, '', $this->perPage, $offset);
					$data['totalDesigns'] = $this->design_model->countByTags(0);
				}

				//get pagination
				$data['pagination'] = $this->_pagination($data['totalDesigns']);

				//get list of tags
				$data['tags'] = $this->design_tag_model->get_unique_tags();

				//get list of design categories
				$data['categories'] = $this->design_category_model->append('design_category_image')->get_many_by('status', 'enabled');

				// tell it what layout to use and load
				$this->load->view($this->defaultLayout, $data);
			}else{
				//tell it what view to use, this is always called the view variable
				$data['view'] = $this->get_view();
				$data['title'] = 'Designs';

				//get design page content for seo purposes
				$data['page'] = modules::run('pages/get', 'designs');
				$data['metaKeywords'] = ($data['page']['metaKeywords'] != '')? $data['page']['metaKeywords'] : $this->design_tag_model->get_unique_tags('text');

				//get designs & result count
				$offset = ($this->input->get('page', TRUE) != '')? (((int) $this->input->get('page', TRUE) - 1) * $this->perPage) : 0;
				if ($this->input->get('filters', TRUE) != '')
				{
					$data['designs'] = $this->design_model->where('type !=','F')->getByTags(0, $this->input->get('filters', TRUE), $this->perPage, $offset);
					$data['totalDesigns'] = $this->design_model->countByTags(0, $this->input->get('filters', TRUE));
				} else {
					$data['designs'] = $this->design_model->where('type !=','F')->getByTags(0, '', $this->perPage, $offset);
					$data['totalDesigns'] = $this->design_model->countByTags(0);
				}

				//get pagination
				$data['pagination'] = $this->_pagination($data['totalDesigns']);

				//get list of tags
				$data['tags'] = $this->design_tag_model->get_unique_tags();

				//get list of design categories
				$data['categories'] = $this->design_category_model->append('design_category_image')->where('type', 'S')->get_many_by('status', 'enabled');

				// tell it what layout to use and load
				$this->load->view($this->defaultLayout, $data);
			}
	}

	function new_designs()
	{
		//get category info
		$data['category'] = $this->design_category_model->get_by('url', 'new');

		//tell it what view to use, this is always called the view variable
		$data['view'] = 'category';
		$data['title'] = $data['category']['title'];

		//seo stuff
		$data['metaDesc'] = $data['category']['shortDesc'];
		$data['metaKeywords'] = ($data['category']['metaKeywords'] != '')? $data['category']['metaKeywords'] : $this->design_tag_model->get_unique_category_tags($data['category']['designCategoryID'], 'text');

		//get settings for timeframe
		$timeframe = '- ' . $data['category']['dateRange'] .' days';
		$filters['dateCreated >'] = date('Y-m-d g:i:s',strtotime($timeframe));

		if (strpos($_SERVER[REQUEST_URI], 'fraternity')) {
			//get all designs within that time frame
			$offset = ($this->input->get('page', TRUE) != '')? (((int) $this->input->get('page', TRUE) - 1) * $this->perPage) : 0;
			if ($this->input->get('filters', TRUE) != '')
			{
				$data['designs'] = $this->design_model->where('type !=','S')->getByTags(0, $this->input->get('filters', TRUE), $this->perPage, $offset, 'results', $filters);
				$data['totalDesigns'] = $this->design_model->countByTags(0, $this->input->get('filters', TRUE), $filters);
			} else {
				$data['designs'] = $this->design_model->where('type !=','S')->getByTags(0, '', $this->perPage, $offset, 'results', $filters);
				$data['totalDesigns'] = $this->design_model->countByTags(0, array(), $filters);
			}
		}else{
			//get all designs within that time frame
			$offset = ($this->input->get('page', TRUE) != '')? (((int) $this->input->get('page', TRUE) - 1) * $this->perPage) : 0;
			if ($this->input->get('filters', TRUE) != '')
			{
				$data['designs'] = $this->design_model->where('type !=','F')->getByTags(0, $this->input->get('filters', TRUE), $this->perPage, $offset, 'results', $filters);
				$data['totalDesigns'] = $this->design_model->countByTags(0, $this->input->get('filters', TRUE), $filters);
			} else {
				$data['designs'] = $this->design_model->where('type !=','F')->getByTags(0, '', $this->perPage, $offset, 'results', $filters);
				$data['totalDesigns'] = $this->design_model->countByTags(0, array(), $filters);
			}
		}

		//get pagination
		$data['pagination'] = $this->_pagination($data['totalDesigns']);

		//get tags
		$data['tags'] = $this->design_tag_model->get_unique_tags();

		//get other categories
		$data['categories'] = $this->design_category_model->get_many_by('status', 'enabled');

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}

	function category($categoryUrl = '')
	{

		//make sure category is valid
		$categoryUrl = $this->_category_is_valid($categoryUrl);

		//get category info
		$data['category'] = $this->design_category_model->get_by(array(
			'status' => 'enabled',
			'url' => $categoryUrl
		));

		if (strpos($_SERVER[REQUEST_URI], 'fraternity')) {
			//get designs
			$offset = ($this->input->get('page', TRUE) != '')? (((int) $this->input->get('page', TRUE) - 1) * $this->perPage) : 0;
			if ($this->input->get('filters', TRUE) != '')
			{
				$data['designs'] = $this->design_model->where('type !=','S')->getByTags($data['category']['designCategoryID'], $this->input->get('filters', TRUE), $this->perPage, $offset);
				$data['totalDesigns'] = $this->design_model->countByTags($data['category']['designCategoryID'], $this->input->get('filters', TRUE));
			} else {
				$data['designs'] = $this->design_model->where('type !=','S')->append('design_image')->order_by('displayOrder')->limit($this->perPage, $offset)->get_many_by(array(
					'status' => 'enabled',
					'designCategoryID' => $data['category']['designCategoryID']
				));
				$data['totalDesigns'] = $this->design_model->count_by(array(
					'status' => 'enabled',
					'designCategoryID' => $data['category']['designCategoryID']
				));
			}
		}else{
			//get designs
			$offset = ($this->input->get('page', TRUE) != '')? (((int) $this->input->get('page', TRUE) - 1) * $this->perPage) : 0;
			if ($this->input->get('filters', TRUE) != '')
			{
				$data['designs'] = $this->design_model->where('type !=','F')->getByTags($data['category']['designCategoryID'], $this->input->get('filters', TRUE), $this->perPage, $offset);
				$data['totalDesigns'] = $this->design_model->countByTags($data['category']['designCategoryID'], $this->input->get('filters', TRUE));
			} else {
				$data['designs'] = $this->design_model->where('type !=','F')->append('design_image')->order_by('displayOrder')->limit($this->perPage, $offset)->get_many_by(array(
					'status' => 'enabled',
					'designCategoryID' => $data['category']['designCategoryID']
				));
				$data['totalDesigns'] = $this->design_model->count_by(array(
					'status' => 'enabled',
					'designCategoryID' => $data['category']['designCategoryID']
				));
			}
		}

		//get pagination
		$data['pagination'] = $this->_pagination($data['totalDesigns']);

		//get a list of tags
		$data['tags'] = $this->design_tag_model->get_unique_category_tags($data['category']['designCategoryID']);

		//get list of design categories
		$data['categories'] = $this->design_category_model->get_many_by('status', 'enabled');

		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->get_view();
		$data['title'] = $data['category']['title'];

		//seo stuff
		$data['metaDesc'] = $data['category']['shortDesc'];
		$data['metaKeywords'] = ($data['category']['metaKeywords'] != '')? $data['category']['metaKeywords'] : $this->design_tag_model->get_unique_category_tags($data['category']['designCategoryID'], 'text');

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}

	function design($categoryUrl = '', $designUrl = '')
	{
		//make sure all are valid
		$designUrl = $this->_design_is_valid($designUrl);

		//get design info
		$data['design'] = $this->design_model->append('design_image')->get_by(array(
			'status' => 'enabled',
			'url' => $designUrl
		));

		if ($categoryUrl != 'new')
		{
			$categoryUrl = $this->_category_is_valid($categoryUrl);

			//get category info
			$data['category'] = $this->design_category_model->append('parent')->get_by(array(
				'status' => 'enabled',
				'designCategoryID' => $data['design']['designCategoryID']
			));
		} else {
			$data['category'] = array('url' => 'new', 'title' => 'New');
		}

		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->get_view();
		$data['title'] = $data['design']['title'];

		//seo stuff
		$data['metaDesc'] = $data['design']['shortDesc'];
		$data['metaKeywords'] = $data['design']['tags'];

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

	/**
	* Checks to see that design is valid and that the design exists
	*/
	public function _design_is_valid($designUrl = '')
	{
		//make sure design url is valid
		if (!is_null($designUrl) && $designUrl != '')
		{
			//check to see if the design exists
			$exists = $this->design_model->select('title')->get_by(array(
				'url' => $designUrl,
				'status' => 'enabled'
			));
			$exists2 = $this->design_model->select('title')->get_by(array(
				'url' => str_replace('_', '-', $designUrl),
				'status' => 'enabled'
			));

			//redirect if not found
			if (($exists == false || empty($exists)) && ($exists2 == false || empty($exists2)))
			{
				$this->session->set_flashdata('error', 'Design not found.');
				redirect('/designs');
			}

			//send back proper version of url
			$designUrl = (!empty($exists2))? str_replace('_', '-', $designUrl) : $designUrl;
			return $designUrl;

		} else {
			$this->session->set_flashdata('error', 'Design not found.');
			redirect('/designs');
		}
	}

	/**
	* Checks to see that category is valid and that it  exists
	*/
	public function _category_is_valid($categoryUrl = '')
	{
		//make sure category url is valid
		if (!is_null($categoryUrl) && $categoryUrl != '')
		{
			//check to see if the design category exists
			$exists = $this->design_category_model->select('title')->get_by(array(
				'url' => $categoryUrl,
				'status' => 'enabled'
			));
			$exists2 = $this->design_category_model->select('title')->get_by(array(
				'url' => str_replace('_', '-', $categoryUrl),
				'status' => 'enabled'
			));

			//rediret if not found
			if (($exists == false || empty($exists)) && ($exists2 == false || empty($exists2)))
			{
				$data['view'] = $this->get_view();
				$data['title'] = 'Designs';

				//get design page content for seo purposes
				$data['page'] = modules::run('pages/get', 'designs');
				$data['metaKeywords'] = ($data['page']['metaKeywords'] != '')? $data['page']['metaKeywords'] : $this->design_tag_model->get_unique_tags('text');

				//get designs & result count
				$offset = ($this->input->get('page', TRUE) != '')? (((int) $this->input->get('page', TRUE) - 1) * $this->perPage) : 0;

					$data['designs'] = $this->design_model->getByTags(0, $categoryUrl, $this->perPage, $offset);
					$data['totalDesigns'] = $this->design_model->countByTags(0, $this->input->get('filters', TRUE));

				//get pagination
				$data['pagination'] = $this->_pagination($data['totalDesigns']);

				//get list of tags
				$data['tags'] = $this->design_tag_model->get_unique_tags();

				//get list of design categories
				$data['categories'] = $this->design_category_model->append('design_category_image')->get_many_by('status', 'enabled');

				// tell it what layout to use and load
				$this->load->view($this->defaultLayout, $data);

			}

			//send back proper version of url
			$categoryUrl = (!empty($exists2))? str_replace('_', '-', $categoryUrl) : $categoryUrl;
			return $categoryUrl;

		} else {



			$this->session->set_flashdata('error', 'Category not found.');
			redirect('/designs');
		}
	}

	public function _pagination($totalResults = 0)
	{
		//pagination
		$this->load->library('pagination');

		//remove offset from querystring if its already there
		parse_str($this->input->server('QUERY_STRING', TRUE), $querystrings);
		unset($querystrings['page']);

		//basic pagincation config
		$config['base_url'] = '?'. http_build_query($querystrings);
		$config['total_rows'] = $totalResults;
		$config['per_page'] = $this->perPage;
		$config['page_query_string'] = TRUE;
		$config['use_page_numbers'] = TRUE;
		$config['query_string_segment'] = 'page';
		$config['num_links'] = 4;

		//pagination styling config
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['prev_link'] = false;
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';

		$config['next_link'] = false;
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';

		$config['first_link'] = '<span class="arrow-dbl-right"></span>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_link'] = '<span class="arrow-dbl-left"></span>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		//initiate pagination
		$this->pagination->initialize($config);
		return $this->pagination->create_links();
	}
}
