<?php
class Admin_json extends Admin_controller {

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//load relevant models
		$this->load->model('designs/design_model');
		$this->load->model('designs/design_tag_model');
		//$this->load->model('designs/design_image_model');
	}

	function reorder_designs($categoryID = 0, $featured = false)
	{
		try
		{
			//get the category
			$categoryID = (int) $categoryID;
			$this->load->model('designs/design_category_model');
			$category = $this->design_category_model->get($categoryID);

			//throw exception if category not found
			if (empty($category) || $category == false)
			{
				throw new Exception('Category not found.');
			}

			//make sure we variable we need from post
			if ($this->input->post('designs', TRUE) == '')
			{
				throw new Exception('Must reorder designs.');
			}

			//try to reorder the designs
			$featured = ($featured == 'featured')? true : false;
			$this->design_model->reorder($this->input->post('designs', TRUE), $featured);

			//set status message
			$data['status'] = 'ok';
			$data['message'] = 'Successfully saved design order.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}

	function tags()
	{
		//load model we need
		$this->load->model('designs/design_tag_model');

		//get all tags
		$tags = $this->design_tag_model->get_unique_tags();

		//output tags
		$this->render_json($tags);
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/
}