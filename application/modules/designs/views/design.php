<?php if (strpos($_SERVER[REQUEST_URI], 'fraternity')): ?>
		<div class="col-md-6">
			<?php if (isset($design['design_image']['fullpath']) && $design['design_image']['fullpath'] != ''): ?>
		        <img src="<?php echo $design['design_image']['fullpath']; ?>" alt="<?php echo $design['title']; ?>" title="<?php echo $design['title']; ?>" class="main-product-img">
		    <?php else: ?>
		        <img src="/assets/images/prod-img-lg.jpg" alt="<?php echo $design['title']; ?>" title="<?php echo $design['title']; ?>" class="main-product-img">
		    <?php endif; ?>
		</div><!-- /.col-md-6 -->
		<div class="col-md-6">
		    <h3 class="item-title"><?php echo $design['title']; ?></h3>
		    <?php echo $design['longDesc']; ?>
		    <h4 class="bnr-ttl5-f-cm">Share this design</h4>
		    <div class="social-share text-center">
		        <ul>
		            <?php
		                $shareUrl = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		                $shareText = 'Look at this cool design I found at South By Sea!';
		            ?>
		            <li class="st_facebook_custom" st_url="<?php echo $shareUrl; ?>" st_title="<?php echo $shareText; ?>" displayText="Twitter"></li>
		            <li class="st_pinterest_custom" st_url="<?php echo $shareUrl; ?>" st_title="<?php echo $shareText; ?>" displayText="Pinterest"></li>
		            <li class="st_twitter_custom" st_url="<?php echo $shareUrl; ?>" st_title="<?php echo $shareText; ?>" displayText="Twitter"></li>
		            <li class="st_instagram_custom" st_url="<?php echo $shareUrl; ?>" st_title="<?php echo $shareText; ?>" displayText="Instagram"></li>
		        </ul>
		    </div>
		    <h4 class="bnr-ttl5-f-cm"> get a quote with this design</h4>
		    <?php echo form_open_multipart('/forms/quote-request', array('id' => 'proof-request-form', 'class' => 'persist', 'accept-charset' => 'UTF-8')); ?>
		    <fieldset>
		        <?php
		            //add the form
		            $this->load->view('../../forms/partials/forms/quote-request');

		            //submit button
		            $this->form_builder->button('submit', ' ', array('inputClass' => 'frat-special btn btn-info'), '', '');
		        ?>
		    </fieldset>
		    </form>
		</div><!-- /.col-md-6 -->


<?php else: ?>


	<div class="col-md-6">
		<?php if (isset($design['design_image']['fullpath']) && $design['design_image']['fullpath'] != ''): ?>
					<img src="<?php echo $design['design_image']['fullpath']; ?>" alt="<?php echo $design['title']; ?>" title="<?php echo $design['title']; ?>" class="main-product-img">
			<?php else: ?>
					<img src="/assets/images/prod-img-lg.jpg" alt="<?php echo $design['title']; ?>" title="<?php echo $design['title']; ?>" class="main-product-img">
			<?php endif; ?>
	</div><!-- /.col-md-6 -->
	<div class="col-lg-6 col-md-6">
			<h3 class="item-title"><?php echo $design['title']; ?></h3>
			<?php echo $design['longDesc']; ?>
			<h4 class="bnr-ttl5-s-cm" style="font-size:50px; padding:10px 30px;">Share this design</h4>
			<div class="social-share text-center">
					<ul>
							<?php
									$shareUrl = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
									$shareText = 'Look at this cool design I found at South By Sea!';
							?>
							<li class="st_facebook_custom" st_url="<?php echo $shareUrl; ?>" st_title="<?php echo $shareText; ?>" displayText="Twitter"></li>
							<li class="st_pinterest_custom" st_url="<?php echo $shareUrl; ?>" st_title="<?php echo $shareText; ?>" displayText="Pinterest"></li>
							<li class="st_twitter_custom" st_url="<?php echo $shareUrl; ?>" st_title="<?php echo $shareText; ?>" displayText="Twitter"></li>
							<li class="st_instagram_custom" st_url="<?php echo $shareUrl; ?>" st_title="<?php echo $shareText; ?>" displayText="Instagram"></li>
					</ul>
			</div>
			<h4 class="bnr-ttl5-s-cm" style="font-size:50px; padding:20px 30px;">get a quote with this design</h4>
			<?php echo form_open_multipart('/forms/quote-request', array('id' => 'proof-request-form', 'class' => 'persist', 'accept-charset' => 'UTF-8')); ?>
			<fieldset>
					<?php
							//add the form
							$this->load->view('../../forms/partials/forms/quote-request');

							//submit button
							$this->form_builder->button('submit', '', array('inputClass' => 'btn btn-info'), '', '');
					?>
			</fieldset>
			</form>
	</div><!-- /.col-md-6 -->
<?php endif ?>
