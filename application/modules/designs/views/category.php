<?php if (strpos($_SERVER[REQUEST_URI], 'fraternity')): ?>
	<div  class="col-lg-12 col-md-12 col-sm-12">
		<div class="col-lg-3 col-md-3 col-sm-3">
		<!-- <div class="sidebar">
			<div class= "pull-right">
				<form action="/designs" method="get" class="design_search">
						<label for="filters"></label>
						<input type="text" name="filters" value="<?php echo $this->input->post('filters', TRUE); ?>" placeholder="search designs &#xF002;" style=" opacity: 0.55;outline: none; border-color: transparent; border: none; background: url(/assets/images/search-icon.png) 195px 2px no-repeat;
	; font-family:Helvetica Neue, FontAwesome" class="text-right">
				</form>
			</div>
					<h3 class="wgt-title"><i class="arrow-triple-left"></i> Designs <i class="arrow-triple-right"></i></h3>
					<?php if (isset($categories) && count($categories) > 0): ?>
							<ul class="categories">
									<li><a href="/designs">All Designs</a>
									<li <?php if ($category['url'] == 'new') {echo 'class="active"'; } ?>>
											<a href="/designs/new">new</a>
											<?php if ($category['url'] == 'new'): ?>
													&nbsp;<i class="arrow-single-left"></i>
											<?php endif; ?>
									</li>
									<?php foreach ($categories as $subCategory): ?>
											<?php $liClass = ($category['url'] == $subCategory['url'])? 'active' : ''; ?>
											<li class="<?php echo $liClass; ?>">
													<a href="/designs/<?php echo $subCategory['url'];?>" title="<?php echo $subCategory['title']; ?>">
															<?php echo $subCategory['title']; ?>
															<?php if ($category['url'] == $subCategory['url']) :?>
																	&nbsp;<i class="arrow-single-left"></i>
															<?php endif; ?>
													</a>
											</li>
									<?php endforeach; ?>
							</ul>
					<?php endif; ?>
			</div> -->
			<div class="sidebar">
				<div class="clearfix"></div>
						<h3 class="" style = "font-size:calc(10px + 3vw);margin-left:-65px;font-family:NORTHWEST Bold; color: #879FAA;"> Designs</h3>
						<?php if (isset($categories) && count($categories) > 0): ?>
								<ul class="categories" style = "font-family:NORTHWEST Bold; font-size:calc(8px + .95vw); text-transform:uppercase; margin-top:-10px;">
									<li class="active"><a href="/fraternity/designs"><small>All Designs</small></a><br><br>
									<li><a href="/fraternity/designs/new"><small>New</small></a><br><br>
									<?php foreach ($categories as $category): ?>
										<li>
												<a href="/fraternity/designs/<?php echo $category['url'];?>" title="<?php echo $category['title']; ?>"><small>
														<?php echo $category['title']; ?>
												</small></a>
										</li><br>
									<?php endforeach; ?>
								</ul>
						<?php endif; ?>
						<!-- <div class= "" style="margin-left:-15px;">
							<form action="/fraternity/designs" method="get" class="design_search">
									<label for="filters"></label>
									<input type="text" name="filters" value="<?php echo $this->input->post('filters', TRUE); ?>" placeholder="search" style=" direction: rtl; text-align: left; font-size: 1em; padding-right:16px; opacity: 0.55;outline: none; border-color: transparent; border: none;
				; font-family:Helvetica Neue, FontAwesome" class="text-right">
							</form>
						</div> -->
				</div>
	</div><!-- /.col-md-3 -->

	<div class="col-lg-9 col-md-9 col-sm-9" id="results-container">

		<div class="col-lg-12 col-md-12 col-sm-12 hidden-xs" style = "padding-right:0px;padding-left:0px;">
			<div class="col-lg-7 col-md-7 col-sm-7" style = "padding-right:0px;padding-left:0px;">
				<img class =" fraternity-slideshow" src="/assets/images/NewFSlideshow1-2.jpg" style = "max-height:260px;">
			</div>
			<div class="col-lg-5 col-md-5 col-sm-5 image-wrapper" style = "padding-right:0px;padding-left:0px;">
				<!-- <img class =" fraternity-slideshow" src="/assets/images/NewFSlideshow1-1.jpg" style = " "> -->
				<img class =" fraternity-slideshow" src="/assets/images/NewFBackground.jpg" style = "max-height:260px;">
				<div style="letter-spacing:5px; font-size:calc(8px + 1.0vw); font-family:NORTHWEST Bold; text-transfrom: uppercase;" class="top-right pull-right hidden-sm">DOn't See What</br></br><span style="">yOu need?</span></div>
				<div style="letter-spacing:5px; font-size:calc(4px + .7vw); margin-top: -20px; margin-right:-10px;  font-family:NORTHWEST Bold; text-transfrom: uppercase;"class="top-right pull-right hidden-lg hidden-md">Don't See What</br><span style="">you need?</span></div>
				<div style="font-family:Campground Regular; font-size:calc(18px + 1.25vw);top:60%;" class="centered">Email Us</div>
			</div>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12" style = "padding-right:0px;padding-left:0px;">
			<br style="height:10px;">
		</div>

		<?php if (isset($designs) && count($designs) > 0): ?>
					<?php $count = 0; ?>
					<?php foreach ($designs as $design): ?>
							<?php $count++; ?>

							<?php if ($count % 3 == 1): ?>
									<div class="row mt-0">
							<?php endif; ?>
											<div class="col-sm-4">
													<div class="product">
															<h4>
																	<a style="font-family:serif;" href="/fraternity/designs/<?php echo $category['url'];?>/<?php echo $design['url'];?>" title="<?php echo $design['title']; ?>"><?php echo $design['title']; ?></a>
																	<i class="arrow-dbl-left"></i>
															</h4>
															<?php if (isset($design['design_image']['thumbFullpath'])): ?>
																	<a style="font-family:serif;" href="/fraternity/designs/<?php echo $category['url'];?>/<?php echo $design['url'];?>" title="<?php echo $design['title']; ?>">
																			<img src="<?php echo $design['design_image']['thumbFullpath']; ?>" alt="<?php echo $design['title']; ?>" title="<?php echo $design['title']; ?>">
																	</a>
															<?php else: ?>
																	<a style="font-family:serif;" href="/fraternity/designs/<?php echo $category['url'];?>/<?php echo $design['url'];?>" title="<?php echo $design['title']; ?>">
																			<img src="/assets/images/sm-pl-hld.jpg" alt="<?php echo $design['title']; ?>" title="<?php echo $design['title']; ?>">
																	</a>
															<?php endif; ?>
													</div>
											</div><!-- /.col-sm-4 -->
							<?php if ($count % 3 == 0): ?>
									</div>
							<?php endif; ?>
					<?php endforeach; ?>

					<div class="clearfix"></div>

					<div class="sbs-paging">
							<div class="pull-right"><?php echo $pagination; ?></div>
					</div>
			<?php else: ?>
					<h3 class="mt-0">Sorry! There are no designs in this category yet. Please be sure to check back later!</h3>
		<?php endif; ?>
	</div><!-- /.col-md-9 -->
</div>
<?php else: ?>
	<div class="col-lg-3 col-md-3 col-sm-3">
		<!-- <div class="sidebar">
			<div class= "pull-right">
				<form action="/designs" method="get" class="design_search">
						<label for="filters"></label>
						<input type="text" name="filters" value="<?php echo $this->input->post('filters', TRUE); ?>" placeholder="search designs &#xF002;" style=" opacity: 0.55;outline: none; border-color: transparent; border: none; background: url(/assets/images/search-icon.png) 195px 2px no-repeat;
	; font-family:Helvetica Neue, FontAwesome" class="text-right">
				</form>
			</div>
					<h3 class="wgt-title"><i class="arrow-triple-left"></i> Designs <i class="arrow-triple-right"></i></h3>
					<?php if (isset($categories) && count($categories) > 0): ?>
							<ul class="categories">
									<li><a href="/designs">All Designs</a>
									<li <?php if ($category['url'] == 'new') {echo 'class="active"'; } ?>>
											<a href="/designs/new">new</a>
											<?php if ($category['url'] == 'new'): ?>
													&nbsp;<i class="arrow-single-left"></i>
											<?php endif; ?>
									</li>
									<?php foreach ($categories as $subCategory): ?>
											<?php $liClass = ($category['url'] == $subCategory['url'])? 'active' : ''; ?>
											<li class="<?php echo $liClass; ?>">
													<a href="/designs/<?php echo $subCategory['url'];?>" title="<?php echo $subCategory['title']; ?>">
															<?php echo $subCategory['title']; ?>
															<?php if ($category['url'] == $subCategory['url']) :?>
																	&nbsp;<i class="arrow-single-left"></i>
															<?php endif; ?>
													</a>
											</li>
									<?php endforeach; ?>
							</ul>
					<?php endif; ?>
			</div> -->
			<div class="sidebar">
				<div class="clearfix"></div>
						<h3 class="wgt-title" style = "font-size:100px;margin-left:-140px;"> Designs </h3>
						<?php if (isset($categories) && count($categories) > 0): ?>
								<ul class="categories" style = "font-size:calc(15px + .5vw); ">
									<li class="active"><a href="/designs"><small>All Designs</small></a> <i class="arrow-single-left"></i><br><br>
									<li><a href="/designs/new"><small>New</small></a><br><br>
									<?php foreach ($categories as $category): ?>
										<li>
												<a href="/designs/<?php echo $category['url'];?>" title="<?php echo $category['title']; ?>"><small>
														<?php echo $category['title']; ?>
												</small></a>
										</li><br>
									<?php endforeach; ?>
								</ul>
						<?php endif; ?>
						<div class= "pull-right">
							<form action="/designs" method="get" class="design_search">
									<label for="filters"></label>
									<input type="text" name="filters" value="<?php echo $this->input->post('filters', TRUE); ?>" placeholder="&#xF002; search" style="font-size: 1em; padding-right:16px; opacity: 0.55;outline: none; border-color: transparent; border: none; background: url(/assets/images/search-icon.png) 195px 2px no-repeat;
				; font-family:Helvetica Neue, FontAwesome" class="text-right">
							</form>
						</div>
				</div>
	</div><!-- /.col-md-3 -->

	<div class="col-lg-9 col-md-9 col-sm-9" id="results-container">

		<div class="col-lg-12 col-md-12 col-sm-12 hidden-xs" style = "padding-right:0px;padding-left:0px;">
			<div class="col-lg-7 col-md-7 col-sm-7" style = "padding-right:0px;padding-left:0px;">
				<img class =" fraternity-slideshow" src="/assets/slideshow/2 - Medium Top Right.jpg" style = "max-height:260px;">
			</div>
			<div class="col-lg-5 col-md-5 col-sm-5 image-wrapper" style = "padding-right:0px;padding-left:0px;">
				<!-- <img class =" fraternity-slideshow" src="/assets/images/NewFSlideshow1-1.jpg" style = " "> -->
				<img class =" fraternity-slideshow" src="/assets/images/NewSBackground.jpg" style = "max-height:260px;">
				<div class="top-right pull-right hidden-md hidden-sm" style="font-family:Hello Stockholm Alt; letter-spacing:0px; font-size:calc(62px + .90vw);right:10px;">don't see what you need</div>
				<div class="top-right pull-right hidden-lg  hidden-sm" style="font-family:Hello Stockholm Alt; letter-spacing:0px; font-size:calc(44px + 1.55vw);right:10px;">don't see what you need</div>
				<div class="top-right pull-right hidden-lg  hidden-md" style="font-family:Hello Stockholm Alt; letter-spacing:0px; font-size:calc(34px + 1.55vw);right:10px;top:20%;">don't see what you need</div>
				<div style="font-size:calc(5px + 2vw);top:45%;left:40%;" class="centered">Email Us</div>
			</div>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12" style = "padding-right:0px;padding-left:0px;">
			<br style="height:10px;">
		</div>		<?php if (isset($designs) && count($designs) > 0): ?>
					<?php $count = 0; ?>
					<?php foreach ($designs as $design): ?>
							<?php $count++; ?>

							<?php if ($count % 3 == 1): ?>
									<div class="row mt-0">
							<?php endif; ?>
											<div class="col-sm-4">
													<div class="product">
															<h4>
																	<a href="/designs/<?php echo $category['url'];?>/<?php echo $design['url'];?>" title="<?php echo $design['title']; ?>"><?php echo $design['title']; ?></a>
																	<i class="arrow-dbl-left"></i>
															</h4>
															<?php if (isset($design['design_image']['thumbFullpath'])): ?>
																	<a href="/designs/<?php echo $category['url'];?>/<?php echo $design['url'];?>" title="<?php echo $design['title']; ?>">
																			<img src="<?php echo $design['design_image']['thumbFullpath']; ?>" alt="<?php echo $design['title']; ?>" title="<?php echo $design['title']; ?>">
																	</a>
															<?php else: ?>
																	<a href="/designs/<?php echo $category['url'];?>/<?php echo $design['url'];?>" title="<?php echo $design['title']; ?>">
																			<img src="/assets/images/sm-pl-hld.jpg" alt="<?php echo $design['title']; ?>" title="<?php echo $design['title']; ?>">
																	</a>
															<?php endif; ?>
													</div>
											</div><!-- /.col-sm-4 -->
							<?php if ($count % 3 == 0): ?>
									</div>
							<?php endif; ?>
					<?php endforeach; ?>

					<div class="clearfix"></div>

					<div class="sbs-paging">
							<div class="pull-right"><?php echo $pagination; ?></div>
					</div>
			<?php else: ?>
					<h3 class="mt-0">Sorry! There are no designs in this category yet. Please be sure to check back later!</h3>
		<?php endif; ?>
	</div><!-- /.col-md-9 -->
<?php endif; ?>
