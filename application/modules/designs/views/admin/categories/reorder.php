<div class="row">
	<div class="col-xs-12">

		<p>Drag and drop categories to re-arrange them.</p>

		<?php if (!empty($categories) && count($categories) > 0): ?>
			<ol class="sortable">
				<?php foreach ($categories as $category): ?>
					<li id="categories_<?php echo $category['designCategoryID']; ?>">
						<div class="sortable-container">
							<div class="reorder-title pull-left">
								<i class="fa fa-arrows"></i> <?php echo $category['title']; ?>
							</div>
						</div>
					</li>
				<?php endforeach; ?>
			</ol>
		<?php endif; ?>

	</div>
</div>