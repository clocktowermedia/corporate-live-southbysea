<div class="row">
	<div class="col-xs-12">

		<?php echo form_open_multipart('admin/designs/categories/create'); ?>
		<fieldset>
			<?php
				//add the form
				$this->load->view('../partials/admin/category-form');

				//submit button
				$this->form_builder->button('submit', 'Create Design Category', array('inputClass' => 'btn btn-info'), '', '');
			?>
		</fieldset>
		</form>

	</div>
</div>