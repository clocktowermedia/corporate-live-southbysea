<div class="row">
	<div class="col-xs-12">
		<div class="box box-primary">
			<div class="box-header" data-toggle="tooltip" title="" data-original-title="Actions">
				<h3 class="box-title">Reorder Featured Designs</h3>
				<div class="box-tools pull-right">
					<button class="btn btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
					<button class="btn btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
				</div>
			</div>

			<div class="box-body no-pad-top">
				<hr class="no-pad-top">

				<div id="featured-design-images">
					<?php if ($featuredDesigns != false && count($featuredDesigns) > 0): ?>
						<div class="row">
							<?php foreach ($featuredDesigns as $design): ?>
								<div class="col-md-2 col-sm-4 col-xs-6 thumbnail-img" id="designs-<?php echo $design['designID'];?>">
									<?php if ($design['thumbFullpath'] != ''): ?>
										<img src="<?php echo $design['thumbFullpath']; ?>" class="img-responsive">
									<?php else: ?>
										<?php echo $design['title']; ?>
									<?php endif; ?>
								</div>
							<?php endforeach; ?>
						</div>
					<?php else: ?>
						<p>No featured designs in this category yet.</p>
					<?php endif; ?>
				</div>

			</div>
		</div>

		<div class="box box-primary">
			<div class="box-header" data-toggle="tooltip" title="" data-original-title="Actions">
				<h3 class="box-title">Reorder Designs</h3>
				<div class="box-tools pull-right">
					<button class="btn btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
					<button class="btn btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
				</div>
			</div>

			<div class="box-body no-pad-top">
				<hr class="no-pad-top">

				<div id="design-images">
					<?php if ($designs != false && count($designs) > 0): ?>
						<div class="row">
							<?php foreach ($designs as $design): ?>
								<div class="col-md-2 col-sm-4 col-xs-6 thumbnail-img" id="designs-<?php echo $design['designID'];?>">
									<?php if ($design['thumbFullpath'] != ''): ?>
										<img src="<?php echo $design['thumbFullpath']; ?>" class="img-responsive">
									<?php else: ?>
										<?php echo $design['title']; ?>
									<?php endif; ?>
								</div>
							<?php endforeach; ?>
						</div>
					<?php else: ?>
						<p>No designs in this category yet.</p>
					<?php endif; ?>
				</div>

			</div>
		</div>
		
	</div>
</div>