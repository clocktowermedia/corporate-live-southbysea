<div class="row">
	<div class="col-xs-12">

		<?php echo form_open_multipart('admin/designs/create'); ?>
		<fieldset>
			<?php
				//add the form
				$this->load->view('../partials/admin/form');

				//submit button
				$this->form_builder->button('submit', 'Create Design', array('inputClass' => 'btn btn-info'), '', '');
			?>
		</fieldset>
		</form>

	</div>
</div>