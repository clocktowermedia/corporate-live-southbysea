<?php
/********************************************************************************************************
/
/ Product Categories
/
********************************************************************************************************/
class Design_category_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'DesignCategories';

	//primary key
	public $primary_key = 'designCategoryID';

	//relationships
	public $has_single = array(
		'design_category_image' => array('model' => 'designs/design_category_image_model', 'primary_key' => 'designCategoryImageID', 'join_key' => 'designCategoryImageID')
	);

	public $has_many = array(
		'designs' => array('model' => 'designs/design_model', 'primary_key' => 'type')
	);

	//callbacks/observers (use function names)

	//validation
	public $validate = array(
		'title' => array(
			'field' => 'title', 
			'label' => 'Title',
			'rules' => 'required|min_length[2]'
		),
		'url' => array(
			'field' => 'url', 
			'label' => 'URL',
			'rules' => 'required|seo_url'
		),
		'designCategoryImageID' => array(
			'field' => 'designCategoryImageID', 
			'label' => 'Image',
			'rules' => 'is_natural'
		)
	);

	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/

	/**
	* Soft delete of entry from db by id - change status to deleted
	*/
	function soft_delete($categoryID = 0)
	{
		//make sure product id is valid
		if ($categoryID > 0)
		{
			$this->db->where($this->primary_key, $categoryID)->set('status', 'deleted')->update($this->_table);
		}

		return false;
	}

	/**
	* Used to change the order in which categories are displayed
	*/
	public function reorder($categories = null)
	{
		//make sure there are pages to loop through
		if (!is_null($categories))
		{
			//loop through each gallery and update the order in the db
			$count = 0;
			foreach ($categories as $key => $value)
			{
				//extract the values that we need
				list($parentID, $depth) = explode(',', $value);

				//increase count
				$count++;

				//call db to update
				$this->db->where($this->primary_key, $key)
					->set('displayOrder', $count)
					//->set('depth', $depth)
					//->set('parentID', $parentID)
					->update($this->_table);
			}

			return true;
		}

		return false;
	}

	/**
	 * Get designs in a format we can use for a select dropdown in a form
	 * @return array
	 */
	public function getDropdown()
	{
		//start query
		$results = $this->db->where('status', 'enabled')
			->from($this->_table)
			->get()
			->result_array();

		foreach ($results as $result)
		{
			$options[$result[$this->primary_key]] = $result['title'];
		}

		return $options;
	}

	/**
	 * Get a list of categories that have designs with a specified tag(s)
	 * This is no longer used since we no longer have category pages for designs
	 * @param  array  $tags [array of tag keywords]
	 * @return array        [matching categories]
	 */
	public function getByTags($tags = array())
	{
		//load other models that we need
		$this->load->model($this->has_many['designs']['model'], 'design_model');
		$this->load->model($this->has_single['design_category_image']['model'], 'image_model');
		$this->load->model($this->design_model->has_many['design_tags']['model'], 'tag_model');

		//start defining query
		$this->db->select('c.*')->from($this->_table . ' AS c');

		//build like portion of query
		if (count($tags) > 0 && is_array($tags))
		{
			foreach ($tags as $tag)
			{
				$likes[] = 'd.tags LIKE "%' . $tag . '%"';
				$likes[] = 't.tag LIKE "%' . $tag . '%"';
			}
			$likeString = implode(' || ', $likes);
			$this->db->where("(" . $likeString . ")");
		} else if ($tags != '') {
			$this->db->where('(d.tags LIKE "%' . $tags . '%" || t.tag LIKE "%' . $tags . '%")');
		}

		$results = $this->db->where('c.status', 'enabled')
			->join($this->design_model->_table . ' AS d', "d.$this->primary_key = c.$this->primary_key AND d.status = 'enabled'")
			->join($this->tag_model->_table . ' AS t', "t.designID = d.designID", 'left outer')
			->group_by('c.' . $this->primary_key)
			->order_by('c.displayOrder')
			->get()
			->result_array();

		//append image if applicable
		if (count($results) > 0)
		{
			foreach ($results as $key => $result)
			{
				if ($result['designCategoryImageID'] > 0)
				{
					$results[$key]['design_category_image'] = $this->image_model->get($result['designCategoryImageID']);
				}
			}
		}

		return $results;
	}

	/**
	 * Only grab information relevant for the sitemap xml
	 * @return array
	 */
	function getSitemapXmlInfo()
	{
		//define query
		return $this->db->select('dateUpdated')
			->select('CONCAT("/designs/", url) AS link', false)
			->where('status', 'enabled')
			->from($this->_table)
			->get()
			->result_array();
	}

	/**********************************************************************************************
	*
	* Validity checker functions & helper functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
}