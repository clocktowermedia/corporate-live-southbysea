<?php
/********************************************************************************************************
/
/ Designs
/ Explanation: Designs
/
********************************************************************************************************/
class Design_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'Designs';

	//primary key
	public $primary_key = 'designID';

	//relationships
	public $has_single = array(
		'design_category' => array('model' => 'designs/design_category_model', 'primary_key' => 'designCategoryID'),
		'design_image' => array('model' => 'designs/design_image_model', 'primary_key' => 'designImageID', 'join_key' => 'designImageID')
	);

	public $has_many = array(
		//'images' => array('model' => 'designs/design_image_model', 'primary_key' => 'designImageID'),
		'design_tags' => array('model' => 'designs/design_tag_model', 'primary_key' => 'designTagID')
	);

	//callbacks/observers (use function names)
	public $before_create = array('_create_timestamp', '_uppercase_tags');
	public $before_update = array('_create_update_timestamp', '_uppercase_tags');

	//validation
	public $validate = array(
		'title' => array(
			'field' => 'title',
			'label' => 'Title',
			'rules' => 'required|min_length[2]'
		),
		'url' => array(
			'field' => 'url',
			'label' => 'URL',
			'rules' => 'required|seo_url'
		),
		'designCategoryID' => array(
			'field' => 'designCategoryID',
			'label' => 'Category',
			'rules' => 'is_natural_no_zero'
		),
		'designImageID' => array(
			'field' => 'designImageID',
			'label' => 'Image',
			'rules' => 'is_natural'
		),
		'status' => array(
			'field' => 'status',
			'label' => 'Status',
			'rules' => 'required'
		)
	);


	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/

	/**
	* Soft delete of entry from db by id - change status to deleted
	*/
	function soft_delete($designID = 0)
	{
		if ($designID > 0)
		{
			$this->db->where($this->primary_key, $designID)->set('status', 'deleted')->update($this->_table);
		}

		return false;
	}

	/**
	 * Get designs by category id
	 * @param  integer $categoryID
	 * @param  boolean $featured   [return featured productsonly]
	 * @return array
	 */
	function getByCategory($categoryID = 0, $featured = false)
	{
		//make sure category id is valid
		if ($categoryID > 0)
		{
			//load model we need
			$this->load->model('designs/design_image_model');

			//determine if we are reordering featured or not
			$reorderField = ($featured == true)? 'd.displayOrderFeatured' : 'd.displayOrder';

			//start defining query
			$this->db->select('d.*')
				->select('di.fullpath')
				->select('di.thumbFullpath')
				->from($this->_table . ' AS d')
				->join($this->design_image_model->_table . ' as di', 'di.designImageID = d.designImageID', 'left outer')
				->where('d.designCategoryID', $categoryID)
				->where('d.status', 'enabled');

			//only select featured categories if applicable
			if ($featured == true)
			{
				$this->db->where('d.featuredInCategory', (int) $featured);
			}

			$results = $this->db->order_by($reorderField)
				->group_by('d.designID')
				->get()
				->result_array();

			return $results;
		}

		return false;
	}

	/**
	* Used to change the order in which designs are displayed based on drag/drop re-arranging done by user
	*/
	function reorder(array $designs = null, $featured = false)
	{
		//mke sure data passed is valid
		if (!is_null($designs) && count($designs) > 0)
		{
			//determine the total number of designs
			$totalProducts = count($designs);

			//determine if we are reordering featured or not
			$reorderField = ($featured == true)? 'displayOrderFeatured' : 'displayOrder';

			//loop through each gallery and update the order in the db
			for ($i = 0; $i < $totalProducts; $i++)
			{
				$this->db->where($this->primary_key, $designs[$i])
					->set($reorderField, $i)
					->update($this->_table);
			}

			return true;
		}

		return false;
	}

	/**
	 * Returns designs that match what the user is typing
	 * @param  string  $query  [search term]
	 * @param  integer $offset
	 * @param  integer $limit
	 * @return array
	 */
	function search($query = '', $limit = 20, $offset = 0)
	{


	  //make sure we have a valid search term
		if (!is_null($query) && $query != '')
		{

			//load model to get tablename(s) that we will need
			$this->load->model($this->has_single['design_category']['model']);
			$this->load->model($this->has_many['design_tags']['model'], 'tag_model');
			$this->load->model($this->has_single['design_image']['model'], 'image_model');

			//start query
			$this->db->select('d.*')
				->select('c.url AS categoryUrl')
				->select('i.thumbFullpath AS image');

			//if there are spaces in the search query, loop through each term
			if (strrpos($query, ',') !== true)
			{
				$terms = explode(',', $query);
				foreach ($terms as $term)
				{
					if (strlen($term) >= 3)
					{
						$termArray[] = '(d.tags LIKE "%' . $term . '%" || t.tag LIKE "%' . $term . '%")';
					}
				}
				$termString = implode(' OR ', $termArray);
			}

			$this->db->where('(' . $termString . ')')
				->from($this->_table . ' AS d')
				->join($this->design_category_model->_table . ' AS c', 'c.designCategoryID = d.designCategoryID')
				->join($this->tag_model->_table . ' AS t', "t.$this->primary_key = d.$this->primary_key", 'left outer')
				->join($this->image_model->_table . ' AS i', 'i.designImageID = d.designImageID', 'left outer');

			//add limit/offset
			if ($limit > 0)
			{
				$this->db->limit($limit, $offset);
			}

			//get/return results
			$results1 = $this->db->where('d.status', 'enabled')
				->group_by('d.' . $this->primary_key)->order_by('dateUpdated','desc')->get()->result_array();

			//load model to get tablename(s) that we will need
			$this->load->model($this->has_single['design_category']['model']);
			$this->load->model($this->has_many['design_tags']['model'], 'tag_model');
			$this->load->model($this->has_single['design_image']['model'], 'image_model');

			//start query
			$this->db->select('d.*')
				->select('c.url AS categoryUrl')
				->select('i.thumbFullpath AS image');

			//if there are spaces in the search query, loop through each term
			if (strrpos($query, ',') !== false)
			{
				$terms = explode(',', $query);
				foreach ($terms as $term)
				{
					if (strlen($term) >= 3)
					{
						$termArray[] = '(d.tags LIKE "%' . $term . '%" || t.tag LIKE "%' . $term . '%")';
					}
				}
				$termString = implode(' OR ', $termArray);

			} else {

				if (strrpos($query, ' ') !== false)
				{
				$terms = explode(' ', $query);
				foreach ($terms as $term)
				{
					if (strlen($term) >= 3)
					{
						$termArray[] = '(d.tags LIKE "%' . $term . '%" || t.tag LIKE "%' . $term . '%")';
					}
				}
				$termString = implode(' OR ', $termArray);
				} else {
					$termString = '(d.tags LIKE "%' . $query . '%" || t.tag LIKE "%' . $query . '%")';
				}
			}

			$this->db->where('(' . $termString . ')')
				->from($this->_table . ' AS d')
				->join($this->design_category_model->_table . ' AS c', 'c.designCategoryID = d.designCategoryID')
				->join($this->tag_model->_table . ' AS t', "t.$this->primary_key = d.$this->primary_key", 'left outer')
				->join($this->image_model->_table . ' AS i', 'i.designImageID = d.designImageID', 'left outer');

			//add limit/offset
			if ($limit > 0)
			{
				$this->db->limit($limit, $offset);
			}

			//get/return results
			$results2 = $this->db->where('d.status', 'enabled')
				->group_by('d.' . $this->primary_key)->order_by('dateUpdated','desc')->get()->result_array();

			//load model to get tablename(s) that we will need
			$this->load->model($this->has_single['design_category']['model']);
			$this->load->model($this->has_many['design_tags']['model'], 'tag_model');
			$this->load->model($this->has_single['design_image']['model'], 'image_model');

			//start query
			$this->db->select('d.*')
				->select('c.url AS categoryUrl')
				->select('i.thumbFullpath AS image');

			//if there are spaces in the search query, loop through each term
			if (strrpos($query, ',') !== false)
			{
				$terms = explode(',', $query);
				foreach ($terms as $term)
				{
					if (strlen($term) >= 3)
					{
						$termArray[] = '(c.title LIKE "%' . $term . '%" || d.title LIKE "%' . $term . '%")';
					}
				}
				$termString = implode(' OR ', $termArray);

			} else {


				$terms = explode(' ', $query);
				foreach ($terms as $term)
				{
					if (strlen($term) >= 3)
					{
						$termArray[] = '(c.title LIKE "%' . $term . '%" || d.title LIKE "%' . $term . '%")';
					}
				}
				$termString = implode(' OR ', $termArray);
			}

			$this->db->where('(' . $termString . ')')
				->from($this->_table . ' AS d')
				->join($this->design_category_model->_table . ' AS c', 'c.designCategoryID = d.designCategoryID')
				->join($this->tag_model->_table . ' AS t', "t.$this->primary_key = d.$this->primary_key", 'left outer')
				->join($this->image_model->_table . ' AS i', 'i.designImageID = d.designImageID', 'left outer');

			//add limit/offset
			if ($limit > 0)
			{
				$this->db->limit($limit, $offset);
			}

			//get/return results
			$results3 = $this->db->where('d.status', 'enabled')
				->group_by('d.' . $this->primary_key)->order_by('dateUpdated','desc')->get()->result_array();

			$final_array = array();
			$results4 = array();

			foreach($results1 as $results){
				$object = new StdClass();
				$object = json_decode(json_encode($results),FALSE);
				if(!isset($final_array[$object->url])){
					$final_array[$object->url]=$object;
					array_push($results4,$object);
				}
			}
			foreach($results2 as $results){
				$object = new StdClass();
				$object = json_decode(json_encode($results),FALSE);
				if(!isset($final_array[$object->url])){
					$final_array[$object->url]=$object;
					array_push($results4,$object);
				}
			}
			foreach($results3 as $results){
				$object = new StdClass();
				$object = json_decode(json_encode($results),FALSE);
				if(!isset($final_array[$object->url])){
					$final_array[$object->url]=$object;
					array_push($results4,$object);
				}
			}

			return $results4;
		}

		return false;
	}

	/**
	 * Get a list of designs in a specific category with a specified tag(s)
	 * @param  integer  $designCategoryID
	 * @param  array  $tags [array of tag keywords]
	 * @param  integer  $limit
	 * @param  integer  $offset
	 * @return array        [matching designs]
	 */
	public function getByTags($designCategoryID = 0, $tags = array(), $limit = 0, $offset = 0, $resultType = 'results', $filters = array())
	{
		//load other models that we need
		$this->load->model($this->has_single['design_image']['model'], 'image_model');
		$this->load->model($this->has_single['design_category']['model']);
		$this->load->model($this->has_many['design_tags']['model'], 'tag_model');

		//start defining query
		$this->db->select('d.*')->from($this->_table . ' AS d');

		//build like portion of query
		if (count($tags) > 0 && is_array($tags))
		{
			foreach ($tags as $tag)
			{
				$likes[] = 'd.tags LIKE "%' . $tag . '%"';
				$likes[] = 't.tag LIKE "%' . $tag . '%"';
			}
			$likeString = implode(' || ', $likes);
			$this->db->where("(" . $likeString . ")");
		} else if (!is_array($tags) && $tags != '') {
			$this->db->where('(d.tags LIKE "%' . $tags . '%" || t.tag LIKE "%' . $tags . '%")');
		}

		if ($designCategoryID > 0)
		{
			$this->db->where('d.designCategoryID', $designCategoryID);
		}

		//if there is a limit, pass it
		if ($limit > 0)
		{
			$this->db->limit($limit, $offset);
		}

		$this->db->where('d.status', 'enabled')
			->join($this->tag_model->_table . ' AS t', "t.$this->primary_key = d.$this->primary_key", 'left outer');

		//if any filters, loop through them
		if (count($filters) > 0)
		{
			foreach ($filters as $key => $value)
			{
				$this->db->where('d.'. $key, $value);
			}
		}

		if ($resultType == 'count')
		{
			$count = $this->db->select('COUNT(DISTINCT d.designID) AS count')->get()->row('count');
			return $count;
		}

		$results = $this->db->group_by('d.' . $this->primary_key)
			->order_by('d.displayOrder')
			->order_by('d.dateCreated', 'DESC')
			->get()
			->result_array();

		//append image/category if applicable
		if (count($results) > 0)
		{
			foreach ($results as $key => $result)
			{
				if ($result['designImageID'] > 0)
				{
					$results[$key]['design_image'] = $this->image_model->get($result['designImageID']);
				}

				$results[$key]['design_category'] = $this->design_category_model->get($result['designCategoryID']);
			}
		}

		return $results;
	}

	public function countByTags($designCategoryID = 0, $tags = array(), $filters = array())
	{
		return $this->getByTags($designCategoryID, $tags, 0, 0, 'count', $filters);
	}

	/**
	 * Only grab information relevant for the sitemap xml
	 * @return array
	 */
	function getSitemapXmlInfo()
	{
		//load category model
		$this->load->model($this->has_single['design_category']['model']);
		$categoryTable = (string) $this->design_category_model->_table;
		$joinKey = $this->has_single['design_category']['primary_key'];

		//define query
		return $this->db->select('d.dateUpdated')
			->select('CONCAT("/designs/", c.url, "/", d.url) AS link', false)
			->where('d.status', 'enabled')
			->from($this->_table . ' AS d')
			->join($categoryTable . ' AS c', "c.$joinKey = d.$joinKey")
			->get()
			->result_array();
	}

	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
	protected function _create_timestamp($data)
	{
		$data['dateCreated'] = $data['dateUpdated'] = date('Y-m-d H:i:s');
		return $data;
	}

	protected function _create_update_timestamp($data)
	{
		$data['dateUpdated'] = date('Y-m-d H:i:s');
		return $data;
	}

	protected function _uppercase_tags($data)
	{
		if (isset($data['tags']) && $data['tags'])
		{
			$data['tags'] = implode(',', array_map('ucwords', explode(',', $data['tags'])));
		}
		return $data;
	}
}
