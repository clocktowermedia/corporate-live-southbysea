<?php
/********************************************************************************************************
/
/ Design Category Images
/
********************************************************************************************************/
class Design_category_image_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'DesignCategoryImages';

	//primary key
	public $primary_key = 'designCategoryImageID';

	//relationships
	public $has_single = array(
		'design_category' => array('model' => 'designs/design_category_model', 'primary_key' => 'designCategoryID')
	);

	//callbacks/observers (use function names)

	//validation
	public $validate = array(
		'filename' => array(
			'field' => 'filename', 
			'label' => 'Filename',
			'rules' => 'required'
		),
		'filepath' => array(
			'field' => 'filepath', 
			'label' => 'Filepath',
			'rules' => 'required'
		),
		'fullpath' => array(
			'field' => 'fullpath', 
			'label' => 'Fullpath',
			'rules' => 'required'
		),
	);

	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/
	

	/**********************************************************************************************
	*
	* Validity checker functions & helper functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
}