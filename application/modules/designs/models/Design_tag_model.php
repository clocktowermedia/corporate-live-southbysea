<?php
/********************************************************************************************************
/
/ Design Tags
/
********************************************************************************************************/
class Design_tag_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'DesignTags';

	//primary key
	public $primary_key = 'designTagID';

	//relationships
	public $has_single = array(
		'design' => array('model' => 'designs/design_model', 'primary_key' => 'designID', 'join_key' => 'designID')
	);

	//callbacks/observers (use function names)
	public $before_create = array('_create_timestamp', '_uppercase_tag');
	public $before_update = array('_uppercase_tag');

	//validation
	public $validate = array(
		'tag' => array(
			'field' => 'tag',
			'label' => 'Tag',
			'rules' => 'required|min_length[2]'
		),
		'designID' => array(
			'field' => 'designID',
			'label' => 'Design',
			'rules' => 'required|is_natural_no_zero'
		)
	);

	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/

	/**
	 * Get a list of tags, make sure there are no duplicates
	 * @param  string $format [can be array or text for comma separated string]
	 */
	public function get_unique_tags($format = 'array')
	{
		$results = $this->db->select('tag AS id', false)
			->select('tag AS text', false)
			->from($this->_table)
			->where('status !=', 'deleted')
			->group_by('tag')
			->get()
			->result_array();

		if ($format == 'text')
		{
			if (!empty($results) && $results != false && count($results) > 0)
			{
				foreach ($results as $result)
				{
					$tempArray[] = $result['text'];
				}

				$results = implode(',', $tempArray);
			} else {
				$results = '';
			}
		}

		return $results;
	}

	/**
	 * Get a list of tags in a specific category, make sure there are no duplicates
	 * @param  integer $categoryID
	 * @param  string $format [can be array or text for comma separated string]
	 */
	public function get_unique_category_tags($categoryID = 0, $format = 'array')
	{
		if ($categoryID > 0)
		{
			//get design tablename
			$this->load->model($this->has_single['design']['model']);

			//start query
			$results = $this->db->select('t.tag AS id', false)
				->select('t.tag AS text', false)
				->from($this->_table . ' AS t')
				->join($this->design_model->_table . ' AS d', 'd.designID = t.designID AND d.status != "deleted" && d.designCategoryID = ' . $categoryID)
				->where('d.designCategoryID', $categoryID)
				->where('t.status !=', 'deleted')
				->group_by('t.tag')
				->get()
				->result_array();

			if ($format == 'text')
			{
				if (!empty($results) && $results != false && count($results) > 0)
				{
					foreach ($results as $result)
					{
						$tempArray[] = $result['text'];
					}

					$results = implode(',', $tempArray);
				} else {
					$results = '';
				}
			}

			return $results;
		}

		return false;
	}

	/**
	 * Returns tags that match what the user is typing
	 * @param  string  $query [search term]
	 * @param  integer $limit
	 * @return array
	 */
	public function search($query = '', $limit = 5, $exclude = array())
	{
		if (!is_null($query) && $query != '')
		{
			$this->db->select('tag AS id', false)
				->select('tag AS text', false)
				->from($this->_table);

			if ($limit > 0)
			{
				$this->db->limit($limit);
			}

			//exclude
			if (count($exclude) > 0)
			{
				$this->db->where_not_in('tag', $exclude);
			}

			$results = $this->db->where('status !=', 'deleted')
				->like('tag', $query)
				->group_by('tag')
				->get()
				->result_array();

			return $results;
		}
	}

	/**
	* Soft delete of entry from db by id - change status to deleted
	*/
	public function soft_delete($designID = 0)
	{
		if ($designID > 0)
		{
			$this->db->where('designID', $designID)->set('status', 'deleted')->update($this->_table);
			return true;
		}

		return false;
	}

	/**********************************************************************************************
	*
	* Validity checker functions & helper functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
	protected function _create_timestamp($data)
	{
		$data['dateCreated'] = date('Y-m-d H:i:s');
		return $data;
	}

	protected function _uppercase_tag($data)
	{
		if (isset($data['tag']) && $data['tag'] != '')
		{
			$data['tag'] = ucwords($data['tag']);
		}
		return $data;
	}
}