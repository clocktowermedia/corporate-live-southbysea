<?php
/********************************************************************************************************
/
/ User Accounts
/
/ Front end site users
/
********************************************************************************************************/
class User_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'Users';

	//primary key
	public $primary_key = 'userID';

	//relationships
	public $has_many = array(
		'validation_links' => array('model' => 'accounts/user_validation_model', 'primary_key' => 'validationID')
	);

	//callbacks/observers (use function names)
	public $before_create = array('_create_timestamp', '_create_password');

	//validation
	public $validate = array(
		'firstName' => array(
			'field' => 'firstName',
			'label' => 'First Name',
			'rules' => 'required|min_length[2]'
		),
		'lastName' => array(
			'field' => 'lastName',
			'label' => 'Last Name',
			'rules' => 'required|min_length[2]'
		),
		'email' => array(
			'field' => 'email',
			'label' => 'Email',
			'rules' => 'required|valid_email'
		)
	);

	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/

	/**
	* Soft delete of user entry from db by id - change status to deleted
	*/
	public function soft_delete($userID = 0)
	{
		//make sure a valid user id was passed
		if ($userID > 0)
		{
			//set the status of the record in the db to "deleted"
			$this->db->where($this->primary_key, $userID)
				->set('status', 'deleted')
				->update($this->_table);

			return true;
		}

		return false;
	}

	/**
	* ban of user entry from db by id - change status to banned
	*/
	public function ban($userID = 0)
	{
		//make sure the user id is valid
		if ($userID > 0)
		{
			//set the status of the record in the db to "banned"
			$this->db->where($this->primary_key, $userID)
				->set('status', 'banned')
				->update($this->_table);

			return true;
		}

		return false;
	}


	/**
	* ban of user entry from db by id - change status to enabled
	*/
	public function enable($userID = 0)
	{
		//make sure the user id is valid
		if ($userID > 0)
		{
			//set the status of the record in the db to "enabled"
			$this->db->where($this->primary_key, $userID)
				->set('status', 'enabled')
				->update($this->_table);

			return true;
		}

		return false;
	}


	/**
	* pending of user entry from db by id - change status to pending
	*/
	public function pending($userID = 0)
	{
		//make sure the user id is valid
		if ($userID > 0)
		{
			//set the status of the record in the db to "pending"
			$this->db->where($this->primary_key, $userID)
				->set('status', 'pending')
				->update($this->_table);

			return true;
		}

		return false;
	}

	/**
	* Update user password by user id
	*/
	public function resetPassword($userID = 0, $newPassword = null)
	{
		//make sure a valid user id and password were passed
		if ($userID > 0 && !is_null($newPassword))
		{
			//get encrypted password & salt
			$salt = $this->_createSalt();

			//define query and update record in db
			$this->db->where($this->primary_key, $userID)
				->set('password', $this->_encrypt_password($newPassword, $salt))
				->set('passwordSalt', $salt)
				->update($this->_table);

			return true;
		}

		return false;
	}

	/**
	* Checks to see if the user has a password
	*/
	public function hasPassword($userID = 0)
	{
		//make sure a valid user id was passed
		if ($userID > 0)
		{
			$query = $this->db->from($this->_table)
				->where($this->primary_key, $userID)
				->where('password !=', '')
				->get();

			//return true if match is found
			if ($query->num_rows() > 0)
			{
				return true;
			}

			return false;
		}

		return false;
	}

	/**
	* Grabs information for the user by id
	*/
	public function findByID($userID = 0)
	{
		//make sure a valid user id was passed
		if ($userID > 0)
		{
			//define query
			$query = $this->db->select('u.userID, u.firstName, u.lastName, u.email, u.dateCreated, u.lastLogin, u.status')
				->from($this->_table . ' u')
				->where($this->primary_key, $userID)
				->get();

			//return false if no match was found
			if ($query->num_rows() <= 0)
			{
				return false;
			}

			//grab matching value and format as an array
			return $query->row_array();
		}

		return false;
	}

	/**
	* Grabs information for the user by email
	*/
	public function findByEmail($email = null, $userID = 0, $status = null)
	{
		//make sure a valid email was passed
		if (!is_null($email))
		{
			//start defining query
			$this->db->select('u.userID, u.firstName, u.lastName, u.email, u.dateCreated, u.lastLogin, u.status')
				->from($this->_table . ' u')
				->where('email', $email);

			//if user id was passed, add it query (exlcude it because we are checking that the email is used by someone besides the user)
			if ($userID > 0)
			{
				$this->db->where('userID !=', $userID);
			}

			//if status was passed, add it to the query
			if (!is_null($status))
			{
				$this->db->where('status', $status);
			}

			$query = $this->db->get();

			//return false if no matches were found
			if ($query->num_rows() <= 0)
			{
				return false;
			}

			//return the results formatted as an array
			return $query->row_array();
		}

		return false;
	}

	/**
	* Authenticates/logs a user into the site
	*/
	public function authenticate($data)
	{
		//make sure valid data (from form) was passed
		if (!is_null($data))
		{
			//try to find the user with that email or screen name
			$user = $this->findByEmail($data['email']);

			//return false if user was not found
			if ($user == false)
			{
				return false;
			}

			//check to see if the password passed in post matches the users password
			$check = $this->_checkPassword($user[$this->primary_key], $data['password']);

			//return the user id if the password matches
			if ($check == true)
			{
				return $user[$this->primary_key];
			}

			return false;
		}

		return false;
	}

	/**********************************************************************************************
	*
	* Validity checker functions & helper functions, may contain redirects
	*
	***********************************************************************************************/

	/**
	* Makes sure the account exists and is enabled
	*/
	public function _isValid($userID = 0)
	{
		//check to see if the user exists
		$exists = $this->_exists($userID);

		if ($exists == true)
		{
			//define query
			$query = $this->db->from($this->_table)
				->where($this->primary_key, $userID)
				->where('status', 'enabled')
				->get();

			//return false if no match was found
			if ($query->num_rows() <= 0)
			{
				return false;
			}

			return true;

		} else {
			return false;
		}
	}

	/**
	* determines whether the account is pending or not
	*/
	public function _isPending($userID = 0)
	{
		//make sure a valid user id was passed
		if ($userID > 0)
		{
			//define query
			$query = $this->db->from($this->_table)
				->where($this->primary_key, $userID)
				->where('status', 'pending')
				->get();

			//return true if a match was found
			if ($query->num_rows() > 0)
			{
				return true;
			}
		}

		return false;
	}

	/**
	* Checks if password is correct
	*/
	public function _checkPassword($userID = 0, $password = null)
	{
		//make sure a valid user id and password were passed
		if ($userID > 0 && !is_null($password))
		{
			//define query
			$query = $this->db->from($this->_table)
				->where($this->primary_key, $userID)
				->get();

			//make sure the current password is correct
			if (sha1($password.$query->row('passwordSalt')) == $query->row('password'))
			{
				return true;
			}

			return false;
		}

		return false;
	}

	/**
	* Creates a salt hash to be used in authentication
	*/
	protected function _createSalt()
	{
		$this->load->helper('string');
		return sha1(random_string('alnum', 32));
	}

	/**
	* encrypts the password with the salt
	*/
	protected function _encrypt_password($password = '', $salt = '')
	{
		return sha1($password . $salt);
	}

	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
	protected function _create_timestamp($data)
	{
		$data['dateCreated'] = date('Y-m-d H:i:s');
		return $data;
	}

	protected function _create_password($data)
	{
		//if password was passed, generate salt & encrypt password
		if (isset($data['password']))
		{
			$salt = $this->_createSalt();

			//add password and salt to data
			$data['password'] = $this->_encrypt_password($data['password'], $salt);
			$data['passwordSalt'] = $salt;
		}

		return $data;
	}
}