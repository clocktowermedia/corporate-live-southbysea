<?php
/********************************************************************************************************
/
/ User Reset
/
/ Used for front end site users reseting their account password or confirming their account
/
********************************************************************************************************/
class User_validation_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'UserValidations';

	//primary key
	public $primary_key = 'validationID';

	//relationships
	public $belongs_to = array(
		'user' => array('model' => 'accounts/user_model', 'primary_key' => 'userID', 'join_key' => 'userID')
	);

	//callbacks/observers (use function names)
	public $before_create = array('_create_timestamp', '_create_link', '_set_expiration');

	//validation
	public $validate = array(
		'userID' => array(
			'field' => 'userID', 
			'label' => 'User ID',
			'rules' => 'required|is_natural_no_zero'
		)
	);


	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/
	
	/**
	* Grabs information for the user reset by user id and code
	*/
	public function getConfirmationLink($userID = 0)
	{
		//make sure a valid user id was passed
		if ($userID > 0)
		{
			//define query
			$query = $this->db->from($this->_table)
				->where('userID', $userID)
				->where('dateExpires', '0000-00-00 00:00:00')
				->get();
				
			//return false if no match found
			if ($query->num_rows() <= 0)
			{
				return false;
			} 
			
			//grab matching value and format as an array	
			return $query->row_array();
		}
		
		return false;
	}
	
	/**
	* Grabs information for the user reset by user id and code
	*/
	public function getResetLink($userID = 0, $code = null)
	{
		//make sure a valid user id and code were passed
		if ($userID > 0 && !is_null($code))
		{
			//define query
			$query = $this->db->from($this->_table)
				->where('userID', $userID)
				->where('code', $code)
				->get();
				
			//return false if no match found
			if ($query->num_rows() <= 0)
			{
				return false;
			} 
			
			//grab matching value and format as an array
			return $query->row_array();
		}
		
		return false;
	}
	
	/**
	* Deletes user entry from db by user ID and code
	*/
	public function deleteLink($userID = 0, $code = null)
	{
		//make sure a valid user id and code were passed
		if ($userID > 0 && !is_null($code))
		{
			//delete record from db
			$this->db->from($this->_table)
				->where('userID', $userID)
				->where('code', $code)
				->delete();
				
			return true;
		}
		
		return false;
	}
	
	/**********************************************************************************************
	*
	* Validity checker functions
	*
	***********************************************************************************************/
	
	/**
	* Checks to see that the reset link is valid, checks expiration date
	*/
	public function _resetLinkIsValid($userID = 0, $code = null)
	{
		//make sure a valid user id and code were passed
		if ($userID > 0 && !is_null($code))
		{
			//define query
			$query = $this->db->from($this->_table)
				->where('userID', $userID)
				->where('code', $code)
				->where('dateExpires >', date('Y-m-d H:i:s'))
				->get();
			
			//return false if no match was found
			if ($query->num_rows() <= 0)
			{
				return false;
			}
			
			return true;
		}
		
		return false;
	}
	
	/**
	* Checks to see that the reset link is valid, does not check expiration date
	*/
	public function _linkIsValid($userID = 0, $code = null)
	{
		//make sure a valid user id and code were passed
		if ($userID > 0 && !is_null($code))
		{
			//define query
			$query = $this->db->from($this->_table)
				->where('userID', $userID)
				->where('code', $code)
				->get();
			
			//return false if no match was found
			if ($query->num_rows() <= 0)
			{
				return false;
			}
			
			return true;
		}
		
		return false;
	}

	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
	protected function _create_timestamp($data)
	{
		$data['dateCreated'] = date('Y-m-d H:i:s');
		return $data;
	}

	protected function _create_link($data)
	{
		$data['code'] = substr(md5(uniqid()), 0, 8);
		return $data;
	}

	protected function _set_expiration($data)
	{
		if (isset($data['expires']) && $data['expires'] == true)
		{
			$data['dateExpires'] = date('Y-m-d H:i:s', strtotime('+2 hours'));
			unset($data['expires']);
		}

		return $data;
	}
}