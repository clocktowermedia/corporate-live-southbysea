<?php
class Accounts extends MY_Controller {

	//layout variables
	public $defaultLayout;

	//set user id
	private $userID;

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//set layout variable(s)
		$this->defaultLayout = $this->get_default_layout();

		//set user id
		$this->userID = (int) $this->session->userdata('userID');

		//load relevant models
		$this->load->model('user_model');
	}

	/**
	* page does not exist, redirect to dashboard or login page
	*/
	public function index()
	{
		//redirect to dashboard if they are logged in
		$this->_ifLoggedIn();

		//redirect to login page if not logged in, set to false to not show the error message
		$this->_requireLoggedIn(false);
	}

	/**
	* Dashboard view page, contains user information
	*/
	public function dashboard()
	{
		//redirect if the user is not logged in
		$this->_requireLoggedIn();

		//grab user info
		$data['user'] = $this->user_model->findByID($this->userID);

		//tell it what view file to use
		$data['view'] = $this->get_view();
		$data['title'] = 'Account Dashboard';

		// tell it what layout to use and load the page
		$this->load->view($this->defaultLayout, $data);
	}


	/**
	* View & action for updating user information
	*/
	public function edit()
	{
		//redirect if the user is not logged in
		$this->_requireLoggedIn();

		//grab user info
		$data['user'] = $this->user_model->findByID($this->userID);

		//tell it what view file to use
		$data['view'] = $this->get_view();
		$data['title'] = 'Update Account Information';

		//load form validation library
		$this->load->library('form_validation');

		//get validation rules from model, set extra options as neccessary
		$validationRules = $this->user_model->validate;

		//set form validation rules
		$this->form_validation->set_rules($validationRules);

		//if the form was submitted, add to database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the page and show form errors
				$this->load->view($this->defaultLayout, $data);
			} else {
				//make sure email is unique, cannot rely on form validation rules for this because it does not take into account when you don't edit these fields
				$emailUsed = $this->user_model->findByEmail($this->input->post('email', TRUE), $this->userID);

				if ($emailUsed == false)
				{
					//set school
					$this->_setSchool('/accounts/edit');

					//update user information in db
					$this->user_model->update($this->userID, $this->input->post(NULL, TRUE));

					//redirect and show success message
					$this->session->set_flashdata('success', "Your account information was updated.");
					redirect('/accounts/edit');

				} else {
					//redirect and show error message
					$this->session->set_flashdata('error', "This email is already in use.");
					redirect('/accounts/edit');
				}
			}
		} else {
			// tell it what layout to use and load the page
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	* View & action for a logged in user to change their password
	*/
	public function change_password()
	{
		//redirect if the user is not logged in
		$this->_requireLoggedIn();

		//get user info
		$data['user'] = $this->user_model->findByID($this->userID);

		//tell it what view file to use
		$data['view'] = $this->get_view();
		$data['title'] = 'Change Password';

		//set form validation rules
		$this->load->library('form_validation');
		$this->form_validation->set_rules('oldPassword', 'Old Password', 'required|min_length[6]');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|matches[password2]');
		$this->form_validation->set_rules('password2', 'Password Confirmation', 'required|min_length[6]');

		//if the form was submitted
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the page and show form errors
				$this->load->view($this->defaultLayout, $data);
			} else {

				//make sure old password is right
				$checkOldPassword = $this->user_model->_checkPassword($this->userID, $this->input->post('oldPassword', TRUE));

				if ($checkOldPassword == false)
				{
					//show error message and redirect
					$this->session->set_flashdata('error', 'Your current password is incorrect, please try again.');
					redirect('/accounts/change-password');
				}

				//update password in db
				$changed = $this->user_model->resetPassword($this->userID, $this->input->post('password', TRUE));

				//display success message
				$this->session->set_flashdata('success', 'Your password has been updated.');
				redirect('/accounts/dashboard');
			}
		} else {
			// tell it what layout to use and load the page
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	* Login action and view
	*/
	function login()
	{
		//redirect users who are already logged in
		$this->_ifLoggedIn();

		//tell it what view file to use
		$data['view'] = $this->get_view();
		$data['title'] = 'Log In';

		//load the config
		$this->load->config('config');

		//set form validation rules
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'required');

		//if the form was submitted, add to database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the page and show form errors
				$this->load->view($this->defaultLayout, $data);
			} else {
				//grab user information
				$user = $this->user_model->findByEmail($this->input->post('email', TRUE));

				//do not even bother trying to authenticate pending/deleted/banned users
				if ($user != false)
				{
					//if status is pending...
					if ($user['status'] == 'pending')
					{
						if ($this->config->item('confirmation') === true && $this->user_model->hasPassword($user['userID']) == true)
						{
							$this->session->set_flashdata('error', 'Your account still requires confirmation. Please check your email inbox. <a href="/accounts/resend-confirmation">Resend Confirmation Email?</a>');
						} else {
							$this->session->set_flashdata('error', 'Your password has not been set. Please check your email inbox. <a href="/accounts/resend-password/?email=' . $this->input->post('email', TRUE) . '">Resend Set Password Email?</a>');
						}

						//redirect
						redirect('/accounts/login');
					} else if ($user['status'] == 'deleted' || $user['status'] == 'banned') {
						//if user is banned/deleted
						$this->session->set_flashdata('error', 'Your account has been deleted or banned due to inactivity or improper behavior. If you believe this to be a mistake, please <a href="/contact">contact us</a>.');
						redirect('/accounts/login');
					}
				} else {
					//if no account is found
					$this->session->set_flashdata('error', 'There is no account associated with that email address.');
					redirect('/accounts/register');
				}

				// try to log the user in
				$userID = $this->user_model->authenticate($this->input->post(NULL, TRUE));

				//redirect if login failed
				if ((int) $userID <= 0)
				{
					//show error message and redirect
					$this->session->set_flashdata('error', 'Login Failed. Please try again.');
					redirect('/accounts/login');
				}

				//make sure account is valid
				$this->_checkAccount($userID);

				//add userID to session
				$this->session->set_userdata('userID', $userID);

				// redirect to dashboard
				redirect('/accounts/dashboard');
			}
		} else {
			// tell it what layout to use and load the page
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	* Logs the user out and redirects them to the homepage
	*/
	function logout()
	{
		$this->session->sess_destroy();
		redirect('/');
	}

	/**
	* Registration page
	*/
	function register()
	{
		//load relevant config file
		$this->load->config('config');

		//redirect users who are already logged in
		$this->_ifLoggedIn();

		//tell it what view file to use
		$data['view'] = $this->get_view();
		$data['title'] = 'Register';

		//load form validation library
		$this->load->library('form_validation');

		//get validation rules from model, set extra options as neccessary
		$validationRules = $this->user_model->validate;
		$validationRules['email']['rules'] .= '|is_unique[' . $this->user_model->_table . '.email]';
		$validationRules['password'] = array('field' => 'password', 'label' => 'Password', 'rules' => 'required|strong_pw');
		$validationRules['password2'] = array('field' => 'password2', 'label' => 'Password', 'rules' => 'required|strong_pw|matches[password]');

		//set form validation rules
		$this->form_validation->set_rules($validationRules);

		//if the form was submitted, add to database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the page and show form errors
				unset($_POST['password'], $_POST['password2']);
				$data['user'] = $this->input->post(NULL, TRUE);
				$this->load->view($this->defaultLayout, $data);
			} else {
				//remove this as it is not used in the db insert
				unset($_POST['password2']);

				//if email confirmation is required, set status to pending
				if ($this->config->item('confirmation') === true)
				{
					$_POST['status'] = 'pending';
				}

				//set school
				$this->_setSchool('/accounts/register');

				// add record to db & redirect
				$userID = $this->user_model->insert($this->input->post(NULL, TRUE));

				//redirect if account creation failed
				if ((int) $userID <= 0)
				{
					//show error and redirect
					$this->session->set_flashdata('error', 'Registration failed. Please try again.');
					redirect('/accounts/register');
				}

				//if email confirmation required, send confirmation email
				if ($this->config->item('confirmation') === true)
				{
					//add user id to post for entry in validation model
					$_POST['userID'] = $userID;

					//generate validation in db
					$this->load->model('user_validation_model');
					$setID = $this->user_validation_model->insert(array('userID' => $userID));
					$set = $this->user_validation_model->get($setID);

					//send validation email
					modules::run('accounts/account_emails/_send_confirmation_email', $this->input->post(NULL, TRUE), $set);

					//display success message & redirect
					$this->session->set_flashdata('success', 'You should recieve an email with a link to confirmation your account shortly. If you do not recieve the email within 30 minutes, please check your spam folder and <a href="/contact">contact us</a>.');
					redirect('/accounts/login');

				} else if ($this->config->item('confirmation') === false) {

					//add userID to session
					$this->session->set_userdata('userID', $userID);

					//display success message & redirect
					$this->session->set_flashdata('success', 'Your account was successfully created.');
					redirect('/accounts/dashboard');
				}
			}
		} else {
			// tell it what layout to use and load the page
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	* View & Action for confirming an account after registration (if confirmation was required)
	*/
	public function confirm($userID = 0, $code = null)
	{
		//load relevant model
		$this->load->model('user_validation_model');

		//redirect if user is logged in
		$this->_ifLoggedIn();

		//set userID to int
		$userID = (int) $userID;

		//make sure that the link is valid
		$set = $this->user_validation_model->_linkIsValid($userID, $code);

		if ($set == false)
		{
			//redirect and show error
			$this->session->set_flashdata('error', 'Invalid confirmation link.');
			redirect('/accounts/login');
		}

		//tell it what view file to use
		$data['view'] = $this->get_view();
		$data['title'] = 'Confirm Your Account';

		//get user info
		$data['user'] = $this->user_model->findByID($userID);
		$data['code'] = $code;

		//set the account status to enabled
		$this->user_model->enable($userID);

		//remove entry from validation table
		$this->user_validation_model->deleteLink($userID, $code);

		//display success message & redirect
		$this->session->set_flashdata('success', 'Your account has been confirmed. You may now log in using the form below.');
		redirect('/accounts/login');
	}

	/**
	* View & action for re-sending confirmation email
	*/
	public function resend_confirmation()
	{
		//load relevant model
		$this->load->model('user_validation_model');

		//tell it what view file to use
		$data['view'] = $this->get_view();
		$data['title'] = 'Resend Account Confirmation Email';

		//set form validation rules
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');

		//if the form was submitted, add to database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the page and show form errors
				$this->load->view($this->defaultLayout, $data);
			} else {
				//grab user info
				$user = $this->user_model->findByEmail($this->input->post('email', TRUE));

				//redirect if user is not found
				if ($user == false)
				{
					//show error message and redirect
					$this->session->set_flashdata('error', 'There is no account associated with that email address.');
					redirect('/accounts/register');
				}

				//redirect if status is not pending
				if ($user['status'] != 'pending')
				{
					//show error message and redirect
					$this->session->set_flashdata('error', 'Your account has already been confirmed.');
					redirect('/accounts/login');
				}

				//find original link
				$link = $this->user_validation_model->getConfirmationLink($user['userID']);

				//resend confirmation email
				modules::run('accounts/account_emails/_send_confirmation_email', $user, $link);

				//redirect and show success message
				$this->session->set_flashdata('success', 'You should recieve an email with a link to confirmation your account shortly. If you do not recieve the email within 30 minutes, please check your spam folder and <a href="/contact">contact us</a>.');
				redirect('/accounts/login');
			}
		} else {
			// tell it what layout to use and load the page
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	* Action for resending account password email
	*/
	public function resend_password()
	{
		//redirect if no email is set
		if ($this->input->get('email', TRUE) == '')
		{
			redirect('/');
		}

		//load model
		$this->load->model('user_validation_model');

		//send an email to the user for them to set their password
		$user = $this->user_model->findByEmail($this->input->get('email', TRUE));
		$setID = $this->user_validation_model->insert(array('userID' => $user['userID']));
		$set = $this->user_validation_model->get($setID);
		modules::run('accounts/account_emails/_send_set_pw_email', $user, $set);

		//display success message & redirect
		$this->session->set_flashdata('success', 'Please check your email inbox for a link to re-set your password. <a href="accounts/resend-password/?email=' . $this->input->get('email', TRUE) . '">Resend Set Password Email?</a>');
		redirect('/accounts/login');
	}

	/**
	* View & Action for setting a password based on the link a user recieved in email after an account was created for them
	*/
	public function set_password($userID = 0, $code = null)
	{
		//load relevant model
		$this->load->model('user_validation_model');

		//load relevant config file
		$this->load->config('config');

		//redirect if user is logged in
		$this->_ifLoggedIn();

		//set userID to int
		$userID = (int) $userID;

		//make sure that the link is valid, redirect if link not found
		$set = $this->user_validation_model->_linkIsValid($userID, $code);

		if ($set == false)
		{
			//show error message and redirect
			$this->session->set_flashdata('error', 'Invalid set password link.');
			redirect('/');
		}

		//tell it what view file to use
		$data['view'] = $this->get_view();
		$data['title'] = 'Set Password';

		//get info and send to view
		$data['user'] = $this->user_model->findByID($userID);
		$data['code'] = $code;

		//set form validation rules
		$this->load->library('form_validation');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|matches[password2]');
		$this->form_validation->set_rules('password2', 'Password Confirmation', 'required|min_length[6]');

		//if the form was submitted
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the page and show form errors
				$this->load->view($this->defaultLayout, $data);

			} else {

				//update password in db & set status to enabled
				$this->user_model->resetPassword($userID, $this->input->post('password', TRUE));
				$this->user_model->enable($userID);

				//remove entry from reset table
				$this->user_validation_model->deleteLink($userID, $code);

				//display success message
				$this->session->set_flashdata('success', 'Your password has been set. You may now login using the form below.');
				redirect('/accounts/login');
			}
		} else {
			// tell it what layout to use and load the page
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	* View & Action for forgot password
	*/
	public function forgot_password()
	{
		//load relevant model
		$this->load->model('user_validation_model');

		//redirect users who are already logged in
		$this->_ifLoggedIn();

		//tell it what view file to use
		$data['view'] = $this->get_view();
		$data['title'] = 'Forgot Password?';

		//set form validation rules
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');

		//if the form was submitted, add to database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the page and show form errors
				$this->load->view($this->defaultLayout, $data);

			} else {

				//find user account, redirect if doesnt exist
				$user = $this->user_model->findByEmail($this->input->post('email', TRUE));

				if ($user == false)
				{
					//show error message and redirect
					$this->session->set_flashdata('error', 'There is no account associated with that email address.');
					redirect('/accounts/forgot-password');
				}

				//if account does exist, add to reset db
				$resetID = $this->user_validation_model->insert(array('userID' => $user['userID'], 'expires' => true));

				//get reset information from db
				$reset = $this->user_validation_model->get($resetID);

				if ($reset != false)
				{
					//send reset email
					modules::run('accounts/account_emails/_send_reset_email', $user, $reset);

					//display success message
					$this->session->set_flashdata('success', 'You should recieve an email shortly with instructions on how to reset your password.');
					redirect('/accounts/forgot-password');
				} else {
					//display error message
					$this->session->set_flashdata('error', 'An error occurred with your request, please try again.');
					redirect('/accounts/forgot-password');
				}
			}
		} else {
			// tell it what layout to use and load the page
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	* View & Action for reseting a password based on the link a user recieved in email after clicking forgot password
	*/
	function reset_password($userID = 0, $code = null)
	{
		//load relevant model
		$this->load->model('user_validation_model');

		//redirect if user is logged in
		$this->_ifLoggedIn();

		//set userID to int
		$userID = (int) $userID;

		//make sure that the link is valid
		$reset = $this->user_validation_model->_resetLinkIsValid($userID, $code);

		if ($reset == false)
		{
			//show error message and redirect
			$this->session->set_flashdata('error', 'Invalid reset link.');
			redirect('/accounts/forgot-password');
		}

		//tell it what view file to use
		$data['view'] = $this->get_view();
		$data['title'] = 'Reset Password';

		//get user info
		$data['user'] = $this->user_model->findByID($userID);

		//set form validation rules
		$this->load->library('form_validation');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|matches[password2]');
		$this->form_validation->set_rules('password2', 'Password Confirmation', 'required|min_length[6]');

		//if the form was submitted
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the page and show form errors
				$this->load->view($this->defaultLayout, $data);
			} else {
				//set status to enabled if neccessary
				if ($data['user']['status'] == 'pending' && $this->config->item('confirmation') === false)
				{
					$this->user_model->enable($userID);
				}

				//update password in db
				$this->user_model->resetPassword($userID, $this->input->post('password', TRUE));

				//remove entry from reset table
				$this->user_validation_model->deleteLink($userID, $code);

				//display success message
				$this->session->set_flashdata('success', 'Your password has been reset. You may now login using the form below.');
				redirect('/accounts/login');
			}
		} else {
			// tell it what layout to use and load the page
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

	/**
	* make sure account is enabled and exists
	*/
	public function _checkAccount($userID = 0)
	{
		//checks to see if the account is enabled and exists
		$account = $this->user_model->select('userID, status')->get_by(array(
			'userID' => $userID,
			'status' => 'enabled'
		));

		//redirect if account not found or account is not enabled
		if ($account == false)
		{
			//remove session data
			$this->session->unset_userdata('userID');

			//show error message and redirect
			$this->session->set_flashdata('error', 'This account does not exist, or has been blocked or deleted. Please contact a site administrator if you believe this to be an error.');
			redirect('/accounts/login');
		}
	}

	/**
	* Returns true if a user is logged in and their account is enabled
	*/
	public function _isLoggedIn()
	{
		//make sure the user id is valid
		if ($this->userID > 0)
		{
			//try to find the user
			$user = $this->user_model->findByID($this->userID);

			//if user was not found or status is not enabled, return false
			if ($user == false || $user['status'] != 'enabled')
			{
				return false;
			}

			return true;
		}

		return false;
	}

	/**
	* Redirects a user to the account dashboard if they are already logged in, used to prevent viewing register page if already logged in, etc.
	*/
	public function _ifLoggedIn()
	{
		//check to see if they are logged in
		$loggedIn = $this->_isLoggedIn();

		//if they are logged in...
		if ($loggedIn == true)
		{
			redirect('/accounts/dashboard');
		}
	}

	/**
	* Require a user to be logged in, redirect them if they are not
	* @parameter $message, show error message if true
	*/
	public function _requireLoggedIn($message = true)
	{
		//check to see if they are logged in
		$loggedIn = $this->_isLoggedIn();

		//if they are not logged in...
		if ($loggedIn == false)
		{
			if ($message == true)
			{
				//add an error message if we want to show it
				$this->session->set_flashdata('error', 'You need to log in to view this page or perform that action.');
			}

			//remove user id from session in case they were already logged in and are now banned/deleted
			$this->session->unset_userdata('userID');

			//redirect
			redirect('/accounts/login');
		}
	}
}