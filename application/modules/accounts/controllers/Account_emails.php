<?php
class Account_emails extends MY_Controller {

	//layout variables
	public $emailLayout;

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//set layout variable(s)
		$this->emailLayout = $this->get_email_layout();
	}

	public function _send_reset_email($user = null, $reset = null)
	{
		//load email & parser library
		$this->load->library('email');
		$this->load->library('parser');
		$this->config->load('email');

		//get sending options from config
		$this->email->from($this->config->item('site_email'), $this->config->item('site_email_name'));

		//send to user
		$this->email->to($user['email'], $user['firstName'] . ' ' . $user['lastName']);

		//email content
		$data['subject'] = $this->config->item('site_name') . ' Account Password Reset';

		//build content
		$url = 'http://' . $_SERVER['HTTP_HOST'] . '/accounts/reset-password/' . $reset['userID'] . '/' . $reset['code'];
		$text = '<p>Visit the URL below in your browser to reset your password:</p>';
		$text .= '<p>Link: <a href="'. $url .'">'. $url . '</a></p>';
		$data['text'] = $text;

		//format content into table
		$data['content'] = $this->convert_text_to_html_email_table($data['subject'], $data['text']);

		//$params = array('html' => $data['content'], 'css' => '/assets/email.css');
		$this->load->library('css_to_inline_styles');
		$this->css_to_inline_styles->setHtml($data['content']);
		$this->css_to_inline_styles->setCSS(file_get_contents(APPPATH . '../assets/css/email.css'));
		$data['html'] = $this->css_to_inline_styles->convert();

		//tell it what view to use
		$data['view'] = $this->get_email_view();

		//get content into layout/view
		$content = $this->parser->parse($this->emailLayout, $data, true);

		//set subject and message
		$this->email->subject($data['subject']);
		$this->email->message($content);

		//send email
		$this->email->sbs_send();
	}

	/**
	* Sends an email to the user to set their password when their account is first created
	*/
	public function _send_set_pw_email($user = null, $set = null)
	{
		//load email & parser library
		$this->load->library('email');
		$this->load->library('parser');
		$this->config->load('email');

		//get sending options from config
		$this->email->from($this->config->item('site_email'), $this->config->item('site_email_name'));

		//send to user
		$this->email->to($user['email'], $user['firstName'] . ' ' . $user['lastName']);

		//email content
		$data['subject'] = $this->config->item('site_name') . ' User Account - Set Password';

		//build content
		$url = 'http://' . $_SERVER['HTTP_HOST'] . '/accounts/set-password/' . $set['userID'] . '/' . $set['code'];
		$text = '<p>Visit the URL below in your browser to set your password:</p>';
		$text .= '<p>Link: <a href="'. $url .'">'. $url . '</a></p>';
		$data['text'] = $text;

		//format content into table
		$data['content'] = $this->convert_text_to_html_email_table($data['subject'], $data['text']);

		$this->load->library('css_to_inline_styles');
		$this->css_to_inline_styles->setHtml($data['content']);
		$this->css_to_inline_styles->setCSS(file_get_contents(APPPATH . '../assets/css/email.css'));
		$data['html'] = $this->css_to_inline_styles->convert();

		//tell it what view to use
		$data['view'] = $this->get_email_view();

		//get content into layout/view
		$content = $this->parser->parse($this->emailLayout, $data, true);

		//set subject & message
		$this->email->subject($data['subject']);
		$this->email->message($content);

		//send email
		$this->email->sbs_send();
	}

	public function _send_confirmation_email($user = null, $set = null)
	{
		//load email & parser library
		$this->load->library('email');
		$this->load->library('parser');
		$this->config->load('email');

		//get sending options from config
		$this->email->from($this->config->item('site_email'), $this->config->item('site_email_name'));

		//send to user
		$this->email->to($user['email'], $user['firstName'] . ' ' . $user['lastName']);

		//email content
		$data['subject'] = $this->config->item('site_name') . ' Account Confirmation Required';

		//build content
		$url = 'http://' . $_SERVER['HTTP_HOST'] . '/accounts/confirm/' . $set['userID'] . '/' . $set['code'];
		$text = '<p>Visit the URL below in your browser to confirm your account:</p>';
		$text .= '<p>Link: <a href="'. $url .'">'. $url . '</a></p>';
		$data['text'] = $text;

		//format content into table
		$data['content'] = $this->convert_text_to_html_email_table($data['subject'], $data['text']);

		$this->load->library('css_to_inline_styles');
		$this->css_to_inline_styles->setHtml($data['content']);
		$this->css_to_inline_styles->setCSS(file_get_contents(APPPATH . '../assets/css/email.css'));
		$data['html'] = $this->css_to_inline_styles->convert();

		//tell it what view to use
		$data['view'] = $this->get_email_view();

		//get content into layout/view
		$content = $this->parser->parse($this->emailLayout, $data, true);

		//set subject and message
		$this->email->subject($data['subject']);
		$this->email->message($content);

		//send email
		$this->email->sbs_send();
	}
}