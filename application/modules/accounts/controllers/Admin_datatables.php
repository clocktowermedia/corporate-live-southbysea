<?php
class Admin_datatables extends Admin_controller {

	//all actions should call this function
	function __construct()
	{
		parent::__construct();
	}

	/**
	* uses datatables library to get accounts in a json format for use in admin index
	*/
	function data()
	{
		//load the appropriate library
		$this->load->library('datatables');

		//grab data from the users table
		$output = $this->datatables->select('u.userID, u.firstName, u.lastName, u.email, u.userType')
			->select('i.title AS institution', false)
			->select('u.department')
			->select('CASE WHEN u.contentPublisher = 1 THEN "Y" ELSE "N" END AS contentPublisher', false)
			->select('u.status')
			->select('IF (u.status = "banned", "unban", "ban") AS banType', false)
			->select('IF (u.status = "banned", "<i class=\"fa fa-undo\"></i> Un-Ban", "<i class=\"fa fa-ban\"></i> Ban") AS banIcon', false)
			->add_column('actions',  '<div class="btn-group">
					<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-user"></i> User <span class="caret"></span>
					</button>
					<ul class="dropdown-menu">
						<li><a href="/admin/accounts/edit/$1"><i class="fa fa-edit"></i> Edit</a></li>
						<li><a href="/admin/accounts/$2/$1">$3</a></li>
						<li><a data-confirm="Are you sure you want to delete this?" href="/admin/accounts/delete/$1"><i class="fa fa-trash-o"></i> Delete</a></li>
					</ul>
				</div>', 'userID, banType, banIcon')
			->unset_column('u.banType')
			->unset_column('u.banIcon')
			->unset_column('u.userID')
			->from('Users u')
			->join('Institutions i', 'u.institutionID = i.institutionID', 'left outer')
			->where('status !=', 'deleted')
			->generate();

		echo $output;
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

}