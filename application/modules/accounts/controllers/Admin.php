<?php
class Admin extends Admin_controller {

	//layout variables
	public $defaultLayout;
	public $emailLayout;

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//set layout variable(s)
		$this->defaultLayout = $this->get_admin_layout();
		$this->emailLayout = $this->get_email_layout();

		//load relevant models
		$this->load->model('user_model');
	}

	/**
	* Shows all accounts in a datatable
	*/
	function index()
	{
		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'User Accounts';

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}

	/**
	* View and action for creating a user account
	*/
	function create()
	{
		//tell it what view file to use
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Create User Account';

		//load form validation library
		$this->load->library('form_validation');

		//get validation rules from model, set extra options as neccessary
		$validationRules = $this->user_model->validate;
		$validationRules['email']['rules'] .= '|is_unique[' . $this->user_model->_table . '.email]';

		//set form validation rules
		$this->form_validation->set_rules($validationRules);

		//if the form was submitted, add to database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the page and show form errors
				$data['user'] = $this->input->post(NULL, TRUE);
				$this->load->view($this->defaultLayout, $data);
			} else {

				// add record to db
				$userID = $this->user_model->insert($this->input->post(NULL, TRUE) + array('status' => 'pending'));

				//redirect if account creation failed
				if ((int) $userID <= 0)
				{
					$this->session->set_flashdata('error', 'Account creation failed.');
					redirect('/admin/accounts/create');
				}

				//load model we need
				$this->load->model('user_validation_model');

				//get user information, use it to create a validation link
				$user = $this->user_model->findByID($userID);
				$setID = $this->user_validation_model->insert(array('userID' => $userID));
				$set = $this->user_validation_model->get($setID);

				//send email link to set password
				$this->_send_new_account_email($user, $set);

				//display success message & redirect
				$this->session->set_flashdata('success', 'Account was successfully created. The user will receive an email with a link to set their password.');
				redirect('/admin/accounts');
			}
		} else {
			// tell it what layout to use and load the page
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	* View & action for updating user information
	*/
	function edit($userID = 0)
	{
		//grab user info
		$data['user'] = $this->user_model->get($userID);

		//make sure account is valid & enabled
		$this->_checkAccount($data['user']);

		//tell it what view file to use
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Update User Account Information';

		//load form validation library
		$this->load->library('form_validation');

		//get validation rules from model, set extra options as neccessary
		$validationRules = $this->user_model->validate;

		//set form validation rules
		$this->form_validation->set_rules($validationRules);

		//if the form was submitted, add to database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the page and show form errors
				$this->load->view($this->defaultLayout, $data);
			} else {
				//make sure email is unique, cannot rely on form validation rules for this because it does not take into account when you don't edit these fields
				$emailUsed = $this->user_model->findByEmail($this->input->post('email', TRUE), $userID);

				if ($emailUsed == false)
				{

					//update user information in db
					$this->user_model->update($userID, $this->input->post(NULL, TRUE));

					//redirect and show success message
					$this->session->set_flashdata('success', "User information was updated.");
					redirect('/admin/accounts/');

				} else {
					//redirect and show error message
					$this->session->set_flashdata('error', "This email is already in use.");
					redirect('/admin/accounts/edit/' . $userID);
				}
			}
		} else {
			// tell it what layout to use and load the page
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	* action for soft deleting an account
	*/
	public function delete($userID = 0)
	{
		//grab user info
		$user = $this->user_model->get($userID);

		//make sure account is valid & enabled
		$this->_checkAccount($user);

		//set status to deleted
		$this->user_model->soft_delete($userID);

		//redirect upon success
		$this->session->set_flashdata('success', "User was deleted.");
		redirect('/admin/accounts/');
	}

	/**
	* action for banning an account
	*/
	public function ban($userID = 0)
	{
		//grab user info
		$user = $this->user_model->get($userID);

		//make sure account is valid & enabled
		$this->_checkAccount($user);

		//set status to banned
		$this->user_model->ban($userID);

		//redirect upon success
		$this->session->set_flashdata('success', "User was banned.");
		redirect('/admin/accounts/');
	}

	/**
	* action for unbanning an account
	*/
	public function unban($userID = 0)
	{
		//grab user info
		$user = $this->user_model->get($userID);

		//make sure account exists
		$this->_checkAccount($user);

		//set status to enabled
		$this->user_model->enable($userID);

		//redirect upon success
		$this->session->set_flashdata('success', "User was un-banned.");
		redirect('/admin/accounts/');
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

	/**
	* make sure the account exists
	*/
	public function _checkAccount($user)
	{
		if ($user == false)
		{
			//redirect and show error message
			$this->session->set_flashdata('error', "This user account does not exist.");
			redirect('/admin/accounts');
		}
	}

	/**
	* Sends an email to the user to set their password when their account is first created
	*/
	public function _send_new_account_email($user = null, $set = null)
	{
		//load email & parser library
		$this->load->library('email');
		$this->load->library('parser');
		$this->config->load('email');

		//get sending options from config
		$this->email->from($this->config->item('site_email'), $this->config->item('site_email_name'));

		//send to admin
		$this->email->to($user['email'], $user['firstName'] . ' ' . $user['lastName']);

		//email content
		$data['subject'] = $this->config->item('site_name') . ' User Account Created';

		//build content
		$url = 'http://' . $_SERVER['HTTP_HOST'] . '/accounts/set-password/' . $set['userID'] . '/' . $set['code'];
		$text = '<p>Visit the URL below in your browser to set your password:</p>';
		$text .= '<p>Link: <a href="'. $url .'">'. $url . '</a></p>';
		$data['text'] = $text;

		//format content into table
		$data['content'] = $this->convert_text_to_html_email_table($data['subject'], $data['text']);

		$this->load->library('css_to_inline_styles');
		$this->css_to_inline_styles->setHtml($data['content']);
		$this->css_to_inline_styles->setCSS(file_get_contents(APPPATH . '../assets/css/email.css'));
		$data['html'] = $this->css_to_inline_styles->convert();

		//tell it what view to use
		$data['view'] = $this->get_email_view();

		//get content into layout/view
		$content = $this->parser->parse($this->emailLayout, $data, true);

		//set subject & message
		$this->email->subject($data['subject']);
		$this->email->message($content);

		//send email
		$this->email->sbs_send();
	}

	/**
	* Sends an email to the admin notifying them if their account password was changed
	*/
	public function _send_email_changed_email($user = null, $input = null)
	{
		//load email & parser library
		$this->load->library('email');
		$this->load->library('parser');
		$this->config->load('email');

		//get sending options from config
		$this->email->from($this->config->item('site_email'), $this->config->item('site_email_name'));

		//send to admin
		$this->email->to($user['email'], $user['firstName'] . ' ' . $user['lastName']);

		//email content
		$data['subject'] = $this->config->item('site_name') . ' User Account Email Changed';

		//build content
		$text = '<p>This is a notification that the email you use for logging into ' . $this->config->item('site_name') . ' has been changed to ' . $input['email']  . '. You will no longer be able to log in with this current email address (' . $user['email'] . ').</p><p>If you did not change your email address or request for your email address to be changed, please contact a site administrator.</p>';

		//format content into table
		$data['content'] = $this->convert_text_to_html_email_table($data['subject'], $text);

		$this->load->library('css_to_inline_styles');
		$this->css_to_inline_styles->setHtml($data['content']);
		$this->css_to_inline_styles->setCSS(file_get_contents(APPPATH . '../assets/css/email.css'));
		$data['html'] = $this->css_to_inline_styles->convert();

		//tell it what view to use
		$data['view'] = $this->get_email_view();

		//get content into layout/view
		$content = $this->parser->parse($this->emailLayout, $data, true);

		//set subject and message
		$this->email->subject($data['subject']);
		$this->email->message($content);

		//send email
		$this->email->sbs_send();
	}
}