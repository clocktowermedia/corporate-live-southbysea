<?php if ($user['status'] != 'enabled'):?>
<div class="alert alert-info">
	<button class="close" data-dismiss="alert">x</button>
	This account is currently <?php echo $user['status'];?> and cannot be seen by non-admin users.
</div>
<?php endif;?>


<div class="row">
	<div class="col-xs-12">
		<div class="form-horizontal">
			<?php echo form_open('admin/accounts/edit/' . $user['userID']); ?>
			<fieldset>
				<?php $this->load->view('../modules/'. $this->router->fetch_module() .'/partials/admin/form');?>
				<?php
					//submit button
					$this->form_builder->button('submit', 'Update User Account', array('containerClass' => 'col-sm-offset-1 col-sm-11', 'inputClass' => 'btn btn-info'), '', '');
				?>
			</fieldset>
			</form>
		</div>
	</div>
</div>