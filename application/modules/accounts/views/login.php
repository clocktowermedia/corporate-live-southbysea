<div class="form-horizontal">
	<?php echo form_open('accounts/login'); ?>
	<fieldset>
		<div class="body bg-gray">
			<?php
				//create array for classes
				$containerArray = array('labelClass' => 'col-sm-1', 'containerClass' => 'col-sm-11');

				//email field
				$this->form_builder->email('email', 'Email:', $this->input->post('email', TRUE), $containerArray, '', '', '', array('required' => 'required'));

				// password field
				$this->form_builder->password('password', 'Password:', '', $containerArray, '', '', '', array('required' => 'required', 'id' => 'password'));
			?>
		</div>

		<div class="footer">
			<button type="submit" class="btn btn-default btn-block">Log Me In</button>
			<p><a href="/accounts/forgot-password">Forgot Password?</a></p>
		</div>

	</fieldset>
	</form>
</div>