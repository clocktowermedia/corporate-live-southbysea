<div class="col-xs-12">

	<h1><?php echo $title;?></h1>
	<div class="">
		<?php echo form_open('accounts/register'); ?>
		<fieldset>

			<?php
				//create array for classes
				$data['containerArray'] = array('labelClass' => 'col-sm-2', 'containerClass' => 'col-sm-10');
				$data['containerArray'] = array();

				//load form
				$this->load->view('../modules/'. $this->router->fetch_module() .'/partials/form', $data);

				//load password form
				$this->load->view('../modules/'. $this->router->fetch_module() .'/partials/password-form', $data);

				//submit button
				$this->form_builder->button('submit', 'Register', array('containerClass' => '' , 'inputClass' => 'btn btn-primary'), '', '');
			?>
		</fieldset>
		</form>
	</div>
</div>