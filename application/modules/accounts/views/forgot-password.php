<div class="form-horizontal">
	<?php echo form_open('accounts/forgot-password'); ?>
	<fieldset>
		<div class="body bg-gray">
			<?php
				//create array for classes
				$containerArray = array('labelClass' => 'col-sm-2', 'containerClass' => 'col-sm-10');

				//email field
				$this->form_builder->email('email', 'Email:', (isset($user['email']))? $user['email'] : '', $containerArray, '', '', '', array('required' => 'required'));
			?>
		</div>

		<div class="footer">
			<button type="submit" class="btn btn-default btn-block">Submit</button>
		</div>

	</fieldset>
	</form>
</div>