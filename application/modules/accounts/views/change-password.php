<div class="col-xs-12">

	<h1><?php echo $title;?></h1>

	<div class="form-horizontal">
		<?php echo form_open('accounts/change-password'); ?>
		<fieldset>
			<?php
				//create array for classes
				$data['containerArray'] = array('labelClass' => 'col-sm-2', 'containerClass' => 'col-sm-10');

				//old password field
				$this->form_builder->password('oldPassword', 'Old Password:', '', $data['containerArray'], '', '', '', array('required' => 'required'));

				//load form
				$this->load->view('../modules/'. $this->router->fetch_module() .'/partials/password-form', $data);

				//submit button
				$this->form_builder->button('submit', 'Submit', array('containerClass' => 'col-sm-offset-2 col-sm-10', 'inputClass' => 'btn btn-primary'), '', '');
			?>
		</fieldset>
		</form>
	</div>

</div>
