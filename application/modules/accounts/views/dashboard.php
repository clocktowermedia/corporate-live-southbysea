<div class="col-xs-12">

	<h1><?php echo $title;?></h1>
	<div class="clearfix"></div>

	<div class="row">

		<div class="col-md-8">

			<h4 class="bnr-ttl3">— Your Orders —</h4>

					<table cellpadding="0" cellspacing="0" border="0" class="table datatable" width="100%">
						<thead>
							<tr>
								<th data-class="expand">Confirmation #</th>
								<th>Submitted</th>
								<th>Product/Design</th>
								<th data-hide="phone">Qty</th>
								<th data-hide="phone">Budget</th>
								<th>Needed By</th>
								<th>Status</th>
							</tr>
						</thead>

						<tbody>
							<tr>
								<td>NW790mTjw30pXvuU</td>
								<td>6/08/2016</td>
								<td><a href="/products/apparel/vnecks/2456-american-apparel-fine-jersey-vneck" target="_blank">2456 american apparel fine jersey v-neck</td>
								<td>40</td>
								<td>$200</td>
								<td>7/30/2016</td>
								<td>Quote Needed</td>
							</tr>
						</tbody>
					</table>


		</div>

		<div class="col-md-4">
			<div class="account-sidebar">
				<h3 class="wgt-title"><i class="arrow-triple-left"></i> Your Account <i class="arrow-triple-right"></i></h3>
				<p><a class="btn sbs-btn" href="/accounts/edit"><i class="fa fa-edit"></i> Edit Account Info</a></p>
				<p><a class="btn sbs-btn" href="/accounts/change-password"><i class="fa fa-key"></i> Change Password</a></p>
				<p><a class="btn sbs-btn" href="/accounts/logout"><i class="fa fa-sign-out"></i> Logout</a></p>

				<h3 class="wgt-title"><i class="arrow-triple-left"></i> Your Favorites <i class="arrow-triple-right"></i></h3>
				<p>No favorites yet.<br/><br/>Start browsing <a href="/products">products</a> or <a href="/designs">designs</a> to add them to your favorites.</p>
			</div>
		</div>

	</div>
</div>