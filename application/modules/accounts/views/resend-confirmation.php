<div class="form-horizontal">
	<?php echo form_open('accounts/resend-confirmation'); ?>
	<fieldset>
		<div class="body bg-gray">
			<?php
				//create array for classes
				$containerArray = array('labelClass' => 'col-sm-1', 'containerClass' => 'col-sm-11');

				//email field
				$this->form_builder->email('email', 'Email:', $this->input->post('email', TRUE), $containerArray, '', '', '', array('required' => 'required'));
			?>
		</div>

		<div class="footer">
			<button type="submit" class="btn btn-warning btn-block">Submit</button>
		</div>

	</fieldset>
	</form>
</div>
