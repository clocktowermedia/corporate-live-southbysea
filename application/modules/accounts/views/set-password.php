<div class="form-horizontal">
	<?php echo form_open('accounts/set-password/' . $user['userID'] . '/' . $code); ?>
	<fieldset>
		<div class="body bg-gray">
			<?php
				//set container array
				$data['containerArray'] = array('labelClass' => 'col-sm-5', 'containerClass' => 'col-sm-7');

				//load form
				$this->load->view('../modules/accounts/partials/password-form', $data);
			?>
		</div>

		<div class="footer">
			<button type="submit" class="btn btn-warning btn-block">Submit</button>
		</div>

	</fieldset>
	</form>
</div>