=== Accounts Module ===
Contributors: Marissa Eder
Requires at least: Codeigniter 2.1, HMVC by wiredesignz, core files (application/core) from CTM-Skeleton OR CTM CI-Skeleton

The accounts module handles user login, registration, and admin management of user accounts.

== Description ==

The accounts module includes several features related to accounts. Including:
User Registration with optional required email confirmation & resend confirmation email
User Login 
Forgot Password/Reset Password
Edit Account/Change Password (when logged in)
Admin editing of user account
Admin banning of user account
Admin deleting of user account
Admin browsing of user accounts

== Installation ==

1. Use the sql file in the dbschema folder and execute it on the website's associated database
2. Copy this folder "application/modules/accounts" into "application/modules" of your site files
2. If the folder exists, copy the modue assets folder "assets/modules/accounts" into "assets/modules" of your site files

== Settings ==
Email Confirmation: To require email confirmation after user registration, open up the config file in the config folder, and set $config['confirmation'] to TRUE, or FALSE if you do not want users to verify their email
* Please note that the email settings are inherited from the "application/config/email.php" file

