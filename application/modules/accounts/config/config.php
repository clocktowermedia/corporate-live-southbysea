<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Require users to confirm their email account first?
|--------------------------------------------------------------------------
|
|
*/
$config['confirmation']	= true;


/* End of file config.php */
/* Location: ./application/modules/accounts/config/config.php */
