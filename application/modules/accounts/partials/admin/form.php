<?php
	//create array for classes
	$containerArray = array('labelClass' => 'col-sm-1', 'containerClass' => 'col-sm-11');

	//first name field
	$this->form_builder->text('firstName', 'First Name:', (isset($user['firstName']))? $user['firstName'] : '', $containerArray, '', '', '', array('required' => 'required'));

	//last name field
	$this->form_builder->text('lastName', 'Last Name:', (isset($user['lastName']))? $user['lastName'] : '', $containerArray, '', '', '', array('required' => 'required'));

	//email field
	$this->form_builder->email('email', 'Email:', (isset($user['email']))? $user['email'] : '', $containerArray, '', '', '', array('required' => 'required'));
?>