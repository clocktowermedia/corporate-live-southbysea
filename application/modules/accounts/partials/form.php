<div class="row mt-0 mb-0">
	<div class="col-sm-12">
		<?php
			//first name field
			$this->form_builder->text('firstName', 'First Name:', (isset($user['firstName']))? $user['firstName'] : '', $containerArray, '', '', '', array('required' => 'required'));
		?>
	</div>
	<div class="col-sm-12">
		<?php
			//last name field
			$this->form_builder->text('lastName', 'Last Name:', (isset($user['lastName']))? $user['lastName'] : '', $containerArray, '', '', '', array('required' => 'required'));
		?>
	</div>
</div>

<div class="row mt-0 mb-0">
	<div class="col-sm-12">
		<?php
			//email field
			$this->form_builder->email('email', 'Email:', (isset($user['email']))? $user['email'] : '', $containerArray, '', '', '', array('required' => 'required'));
		?>
	</div>
	<div class="col-sm-12">
	</div>
</div>