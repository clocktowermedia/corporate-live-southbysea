<?php

	//module header
	$this->load->view('../../ecommerce/partials/header');

	//load containers
	$this->load->view('partials/container');

	//global partials
	$this->load->view('partials/winks');

	//load the main content from the controller
	$this->load->view($view);

	//add the modal
	$this->load->view('partials/modal');

	//end containers
	$this->load->view('partials/container-end');

	//site footer
	$this->load->view('../../ecommerce/partials/footer');

	exit;