<?php
class Admin_orders extends Admin_controller {

	//layout variables
	public $defaultLayout;

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//set layout variable(s)
		$this->defaultLayout = $this->get_admin_layout();

		//load relevant models
		$this->load->model('ecommerce/store_model');
		$this->load->model('ecommerce/order_model');
	}

	/**
	* Shows all orders for a store
	*/
	function index($storeID = 0)
	{
		//make sure store is valid
		modules::run('ecommerce/admin/_store_is_valid', $storeID);

		//get store info
		$data['store'] = $this->store_model->get($storeID);

		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->_get_custom_view();
		$data['title'] = 'Orders for ' . $data['store']['title'];

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}

	/**
	* View for an order
	*/
	function view($orderID = 0)
	{
		//make sure order exists
		$this->_order_is_valid($orderID);

		//grab order info for view
		$data['order'] = $this->order_model->getCart($orderID);
		$data['order']['details'] = $this->order_model->append('store')->get($orderID);

		//tell it what view file to use
		$data['view'] = $this->_get_custom_view();
		$data['title'] = $data['order']['details']['cust_first_name'] . ' ' . $data['order']['details']['cust_last_name'] . "'s Order";

		// tell it what layout to use and load the view
		$this->load->view($this->defaultLayout, $data);
	}

	/**
	 * Action for cancelling an order
	 */
	function cancel($orderID = 0)
	{
		//make sure order exists
		$this->_order_is_valid($orderID);

		//get order
		$order = $this->order_model->get($orderID);

		//attempt to cancel order
		$captureData = $this->_cancel_order($orderID);

		//show messages and redirect
		$type = ($captureData['success'] == false)? 'error' : 'success';
		$this->session->set_flashdata($type, $captureData['message']);
		redirect('/admin/ecommerce/orders/' . $order['ecomm_store_id']);
	}

	/**
	 * View and action for doing a partial refund of an order
	 */
	function partial_refund($orderID = 0)
	{
		//make sure order exists
		$this->_order_is_valid($orderID);

		//grab order info for view
		$order = $this->order_model->append('store')->get($orderID);

		//determine if refund can be done
		$amount_left = number_format($order['total'] - $order['refund_amt'], '2', '.', '');
		if ($amount_left < '0.50' || $order['status'] != 'completed')
		{
			$this->session->set_flashdata('error', 'This order may not be partially refunded.');
			redirect('/admin/ecommerce/orders/view/' . $orderID);
		}

		//set form validation rules
		$this->load->library('form_validation');
		$this->form_validation->set_rules('refundAmt', 'Refund Amount', 'required|decimal');

		//if the form was submitted, update database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the design and show form errors
				$data['submission'] = $this->input->post(NULL, TRUE);
				$this->load->view($this->defaultLayout, $data);

			} else {

				//attempt to refund
				if ($order['payment_method'] == 'credit')
				{
					$captureData = modules::run('ecommerce/ecommerce_gateways/_stripe_refund', $order['stripe_cust_id'], $this->input->post('refundAmt', TRUE));
				} else if ($order['payment_method'] == 'paypal') {
					$captureData = modules::run('ecommerce/ecommerce_gateways/_paypal_refund', $order, $this->input->post('refundAmt', TRUE));
				}

				//set status to refunded
				if ($captureData['success'] == true)
				{
					//update refund amount & set to refunded if entire order is refunded
					$updateData['refund_amt'] = number_format($this->input->post('refundAmt', TRUE) + $order['refund_amt'], 2, '.', '');
					if ($updateData['refund_amt'] >= $order['total'])
					{
						$updateData['status'] = 'refunded';
					}

					$this->order_model->skip_validation()->update($orderID, $updateData);
				}

				//show messages and redirect
				$type = ($captureData['success'] == false)? 'error' : 'success';
				$this->session->set_flashdata($type, $captureData['message']);
				redirect('/admin/ecommerce/orders/' . $order['ecomm_store_id']);
			}
		} else {
			redirect('/admin/ecommerce/orders/' . $orderID);
		}
	}

	/**
	 * Action for processing a single order
	 */
	function process($orderID = 0)
	{
		//make sure order exists
		$this->_order_is_valid($orderID);

		//make sure order has not already been processed
		$order = $this->order_model->append('store')->get($orderID);
		if ($order['status'] != 'authorized')
		{
			$this->session->set_flashdata('error', 'This order is not ready to be processed.');
			redirect('/admin/ecommerce');
		}

		//attempt to process order
		if ($order['payment_method'] == 'credit')
		{
			$captureData = modules::run('ecommerce/ecommerce_gateways/_stripe_charge', $order['stripe_cust_id']);

		} else if ($order['payment_method'] == 'paypal') {

			$captureData = modules::run('ecommerce/ecommerce_gateways/_paypal_capture', $order);
		}

		//send email to user
		if ($captureData['success'] == true)
		{
			$emailData['order'] = $this->order_model->getCart($orderID);
			$emailData['order']['details'] = $order;
			$emailData['store'] = $order['store'];
			modules::run('ecommerce/ecommerce_emails/_send_order_processed_email', $emailData);
		}

		//show messages and redirect
		$type = ($captureData['success'] == false)? 'error' : 'success';
		$this->session->set_flashdata($type, $captureData['message']);

		//redirect to store
		redirect('/admin/ecommerce/orders/' . $order['ecomm_store_id']);
	}

	/**
	 * Action for exporting orders, we will need more info from client on format
	 * @return csv download
	 */
	public function export($storeID = 0)
	{
		//if store id is set, make sure it's valid
		if ((int) $storeID > 0)
		{
			modules::run('ecommerce/admin/_store_is_valid', $storeID);
			$filters['ecomm_store_id'] = $storeID;
		}
		$filters['status !='] = 'pending';
		$orders = $this->order_model->append('store')->get_many_by($filters);

		//headings
		$csvData[] = array(
			'ecomm_order_id' => 'Order #',
			'confirmationNumber' => 'Confirmation #',
			'status' => 'Order Status',
			'store_name' => 'Store Name',
			'store_url' => 'Store URL',
			'cust_first_name' => 'Customer First Name',
			'cust_last_name' => 'Customer Last Name',
			'cust_email' => 'Customer Email',
			'cust_phone' => 'Customer Phone',
			'subtotal' => 'Order Subtotal',
			'tax' => 'Order Tax',
			'tax_rate' => 'Order Tax Rate',
			'total' => 'Order Total',
			'payment_method' => 'Payment Method',
			'order_items' => 'Ordered Item',
			'total_qty' => 'Total Qty Ordered',
			'sizes' => 'Sizes Ordered',
			'comments' => 'Order Notes',
			'datePlaced' => 'Date Placed',
			'dateProcessed' => 'Date Processed'
		);

		//loop through orders
		foreach ($orders AS $order)
		{
			//get cart info
			$cart = $this->order_model->getCart($order['ecomm_order_id']);
			$cartCsvItems = $cart['cart'];
			unset($cartCsvItems[0]);

			$csvData[] = array(
				'ecomm_order_id' => $order['ecomm_order_id'],
				'confirmationNumber' => $order['confirmationNumber'],
				'status' => ucwords($order['status']),
				'store_name' => $order['store']['title'],
				'store_url' => '/' . $order['store']['url_slug'],
				'cust_first_name' => $order['cust_first_name'],
				'cust_last_name' => $order['cust_last_name'],
				'cust_email' => $order['cust_email'],
				'cust_phone' => $order['cust_phone'],
				'subtotal' => '$' . $order['subtotal'],
				'tax' => '$' . $order['tax'],
				'tax_rate' => $order['tax_rate'] . '%',
				'total' => '$' . $order['total'],
				'payment_method' => $order['payment_method'],
				'order_items' => $cart['cart'][0]['product']['title'],
				'total_qty' => $cart['cart'][0]['qty'],
				'sizes' => $cart['cart'][0]['size']['size'] . '(' . $cart['cart'][0]['qty'] . ')',
				'comments' => $order['comments'],
				'dateCreated' => $order['dateCreated'],
				'dateProcessed' => $order['dateProcessed']
			);

			if (count($cartCsvItems) > 0)
			{
				foreach ($cartCsvItems AS $item)
				{
					$csvData[] = array(
						'ecomm_order_id' => $order['ecomm_order_id'],
						'confirmationNumber' => $order['confirmationNumber'],
						'status' => ucwords($order['status']),
						'store_name' => $order['store']['title'],
						'store_url' => '/' . $order['store']['url_slug'],
						'cust_first_name' => $order['cust_first_name'],
						'cust_last_name' => $order['cust_last_name'],
						'cust_email' => $order['cust_email'],
						'cust_phone' => $order['cust_phone'],
						'subtotal' => '$' . $order['subtotal'],
						'tax' => '$' . $order['tax'],
						'tax_rate' => $order['tax_rate'] . '%',
						'total' => '$' . $order['total'],
						'payment_method' => $order['payment_method'],
						'order_items' => $item['product']['title'],
						'total_qty' => $item['qty'],
						'sizes' => $item['size']['size'] . '(' . $item['qty'] . ')',
						'comments' => $order['comments'],
						'dateCreated' => $order['dateCreated'],
						'dateProcessed' => $order['dateProcessed']
					);
				}
			}
		}

		$this->load->library('array_to_csv');
		$this->array_to_csv->addData($csvData);
		$this->array_to_csv->output('D');
		exit;
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

	private function _get_custom_view()
	{
		return 'admin/orders/' . $this->get_view();
	}

	/**
	* Checks to see that order ID is valid and that the order exists
	*/
	public function _order_is_valid($orderID = 0)
	{
		//make sure order id is an integer
		$orderID = (int) $orderID;

		//make sure order id is greater than zero
		if ($orderID > 0)
		{
			//check to see if the order exists
			$exists = $this->order_model->get($orderID);

			if ($exists == false)
			{
				$this->session->set_flashdata('error', 'Invalid order id.');
				redirect('/admin/ecommerce');
			}

		} else {
			$this->session->set_flashdata('error', 'Invalid order id.');
			redirect('/admin/ecommerce');
		}
	}

	public function _cancel_order($orderID = 0)
	{
		//make sure order has not already been processed
		$data['order'] = $this->order_model->getCart($orderID);
		$data['order']['details'] = $this->order_model->append('store')->get($orderID);

		//redirect if order has already been refunded
		if ($data['order']['details']['status'] == 'refunded')
		{
			$this->session->set_flashdata('error', 'This order has already been refunded.');
			redirect('/admin/ecommerce/orders/' . $data['order']['details']['ecomm_store_id']);
		}

		//we only need to process a refund if the order was completed
		if ($data['order']['details']['status'] == 'completed')
		{
			//attempt to process order
			if ($data['order']['details']['payment_method'] == 'credit')
			{
				$captureData = modules::run('ecommerce/ecommerce_gateways/_stripe_refund', $data['order']['details']['stripe_cust_id']);

			} else if ($data['order']['details']['payment_method'] == 'paypal') {

				$captureData = modules::run('ecommerce/ecommerce_gateways/_paypal_refund', $data['order']['details']);
			}

		} else {

			//void the authorization
			if ($data['order']['details']['payment_method'] == 'paypal')
			{
				$captureData = modules::run('ecommerce/ecommerce_gateways/_paypal_void', $data['order']['details']);

			} else {
				//don't need to do anything, set fake status
				$captureData['success'] = true;
				$captureData['message'] = 'Successfully cancelled order.';
			}
		}

		//set status to refunded and send email
		if ($captureData['success'] == true)
		{
			$this->order_model->skip_validation()->update($orderID, array('status' => 'refunded'));

			//email user
			modules::run('ecommerce/ecommerce_emails/_send_order_cancelled_email', $data);
		}

		//return statuses
		return $captureData;
	}
}
