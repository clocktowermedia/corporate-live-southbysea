<?php
class Cart_json extends MY_Controller {

	protected $orderID;
	protected $storeID;

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//load relevant models
		$this->load->model('ecommerce/order_model');
		$this->load->model('ecommerce/order_item_model');

		//get some stuff from session
		$this->storeID = (int) $this->session->userdata('storeID');
		$this->orderID = $this->_gen_order_id();

		//make sure store is still valid
		$this->_get_store();
	}

	/**
	 * Add item to cart
	 * @return json
	 */
	function add_item()
	{
		try {

			//make sure we are posting
			if (!$this->is_post())
			{
				throw new Exception('Must post data.');
			}

			if ($this->input->post('size', TRUE) == '' || $this->input->post('product_id', TRUE) == '')
			{
				throw new Exception('Invalid post data.');
			}

			//make sure item still exists
			$productIsValid = $this->_product_is_valid((int) $this->input->post('product_id', TRUE));

			//make sure product belongs to store..
			$this->load->model('ecommerce/product_model');
			$product = $this->product_model->get((int) $this->input->post('product_id', TRUE));
			if ($this->storeID != $product['ecomm_store_id'])
			{
				throw new Exception('This product does not belong to the store you are currently viewing.');
			}

			//if item already in cart, increase quantity, else add
			$itemInCart = $this->order_item_model->get_by(array(
				'ecomm_order_id' => $this->orderID,
				'ecomm_product_id' => (int) $this->input->post('product_id', TRUE),
				'ecomm_product_size_id' => (int) $this->input->post('size', TRUE)
			));
			if ($itemInCart != false && !empty($itemInCart))
			{
				//if product no longer valid, remove from cart
				if ($productIsValid == false)
				{
					$itemsInCart = $this->order_item_model->get_many_by(array(
						'ecomm_order_id' => $this->orderID,
						'ecomm_product_id' => (int) $this->input->post('product_id', TRUE)
					));
					foreach ($itemsInCart AS $item)
					{
						$this->order_item_model->delete($item['ecomm_cart_id']);
					}

					throw new Exception('This product is no longer valid. This item was removed from your cart.');
				}

				//else, increase quantity
				$this->order_item_model->skip_validation()->update($itemInCart['ecomm_cart_id'], array('qty' => $itemInCart['qty'] + 1));

			} else {

				//if product no longer valid, do not add to cart
				if ($productIsValid == false)
				{
					throw new Exception('This product is no longer valid.');
				}

				//add to cart
				$itemID = $this->order_item_model->insert(array(
					'ecomm_order_id' => $this->orderID,
					'ecomm_product_id' => (int) $this->input->post('product_id', TRUE),
					'ecomm_product_size_id' => (int) $this->input->post('size', TRUE),
					'qty' => 1
				));

				//display error or success message
				if ((int) $itemID <= 0)
				{
					throw new Exception('An error occurred, please try again.');
				}
			}

			//get store info
			$store = $this->_get_store();

			//send back data
			$data['status'] = 'ok';
			$data['message'] = 'Successfully added item to cart.';
			$data['redirect'] = '/preorder/' . $store['url_slug'] . '/cart';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();

			if ($this->input->post('product_id'))
			{
				$this->load->model('ecommerce/product_model');
				$product = $this->product_model->append('store')->get((int) $this->input->post('product_id', TRUE));

				$this->session->set_flashdata('error', $e->getMessage());
				$data['redirect'] = '/preorder/' . $product['store']['url_slug'];
			}
		}

		$this->render_json($data);
	}

	/**
	 * Remove item from cart
	 * @return json
	 */
	function remove_item()
	{
		try {
			//make sure we are posting
			if (!$this->is_post())
			{
				throw new Exception('Must post data.');
			}

			//make sure item was posted
			if ((int) $this->input->post('itemID') <= 0)
			{
				throw new Exception('Invalid item ID.');
			}

			//make sure item is found and belongs to user
			$itemInCart = $this->order_item_model->get_by(array(
				'ecomm_order_id' => $this->orderID,
				'ecomm_cart_id' => (int) $this->input->post('itemID')
			));
			if ($itemInCart == false || empty($itemInCart))
			{
				throw new Exception('Invalid item ID.');
			}

			//remove item from order
			$this->order_item_model->delete((int) $this->input->post('itemID'));

			//if no items left in order, redirect
			$order = $this->order_model->get($this->orderID);
			if ($order['total'] <= 0)
			{
				$data['redirect'] = true;
			}

			//send back data
			$data['status'] = 'ok';
			$data['message'] = 'Successfully removed item from cart.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}

	/**
	 * Update quantity of passed item
	 * @return json
	 */
	function update_quantity()
	{
		try {
			//make sure we are posting
			if (!$this->is_post())
			{
				throw new Exception('Must post data.');
			}

			//make sure item was posted
			if ((int) $this->input->post('itemID') <= 0)
			{
				throw new Exception('Invalid item ID.');
			}

			//make sure item is found and belongs to user
			$itemInCart = $this->order_item_model->get_by(array(
				'ecomm_order_id' => $this->orderID,
				'ecomm_cart_id' => (int) $this->input->post('itemID')
			));
			if ($itemInCart == false || empty($itemInCart))
			{
				throw new Exception('Invalid item ID.');
			}

			//if no quantity passed, increase by one, else use quantity passed
			if ((int) $this->input->post('qty') > 0)
			{
				$this->order_item_model->skip_validation()->update((int) $this->input->post('itemID'), array('qty' => (int) $this->input->post('qty')));
			} else {
				$this->order_item_model->skip_validation()->update((int) $this->input->post('itemID'), array('qty' => $itemInCart['qty'] + 1));
			}

			$data['status'] = 'ok';
			$data['message'] = 'Successfully updated item quantity.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}

	/**
	 * Get cart info
	 * @return json
	 */
	function get_cart_info()
	{
		try {
			//make sure we are getting
			if (!$this->is_get())
			{
				throw new Exception('Must get data.');
			}

			//get store info
			$store = $this->_get_store();

			//get cart info (items and total)
			$data['order'] = $this->order_model->getCart($this->orderID);
			$data['order']['store']['url_slug'] = $store['url_slug'];
			$data['status'] = 'ok';
			$data['message'] = 'Successfully retreived cart information.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}

	function stripe_key() {
		try {
			//make sure we are getting
			if (!$this->is_get())
			{
				throw new Exception('Must get data.');
			}

			//get api config
			$this->load->config('api_keys');
			$stripeConfig = $this->config->item('stripe');

			//get cart info (items and total)
			$data['stripe'] = $stripeConfig['publishable'];
			$data['status'] = 'ok';
			$data['message'] = 'Successfully retreived information.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/
	function _get_store()
	{
		if ($this->storeID > 0)
		{
			$this->load->model('ecommerce/store_model');
			$store = $this->store_model->get($this->storeID);
			modules::run('ecommerce/_store_is_valid', $store['url_slug']);
			return $store;
		} else {
			return false;
		}
	}

	function _gen_new_order_id($storeID = 0)
	{
		if ($storeID > 0)
		{
			//create order
			$orderID = $this->order_model->skip_validation()->insert(array(
				'session_id' => session_id(),
				'ecomm_store_id' => $storeID
			));

			//set variables
			$this->session->set_userdata('orderID', $orderID);
			$this->orderID = $orderID;
			$this->storeID = $storeID;
		}
	}

	function _gen_order_id()
	{
		try {
			if ((int) $this->session->userdata('orderID') > 0)
			{
				return (int) $this->session->userdata('orderID');
			} else {

				if ($this->storeID <= 0)
				{
					throw new Exception('Invalid store id.');
				} else {

					//create order
					$orderID = $this->order_model->skip_validation()->insert(array(
						'session_id' => session_id(),
						'ecomm_store_id' => $this->storeID
					));

					//save in session if applicable
					if ((int) $orderID > 0)
					{
						$this->session->set_userdata('orderID', $orderID);
						return $orderID;
					} else {
						throw new Exception('Failed to initiate cart.');
					}
				}
			}
		} catch (Exception $e) {
			redirect('/');
		}
	}

	function _product_is_valid($product_id = 0)
	{
		$this->load->model('ecommerce/product_model');
		$product = $this->product_model->select('ecomm_product_id')->get_by(array(
			'ecomm_product_id' => $product_id,
			'status' => 'enabled'
		));
		return (empty($product) || $product == false)? false : true;
	}
}