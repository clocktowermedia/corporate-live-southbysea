<?php
class Ecommerce_gateways extends MY_Controller {

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//get api values
		$this->load->config('api_keys');

		//load error model
		$this->load->model('ecommerce/error_model');
	}

	/************************************************************************************************************************************
	/
	/
	/ Stripe functions
	/
	/
	/***********************************************************************************************************************************/

	/**
	 * Create a stripe customer and store their credit card
	 * @return integer - customer id
	 */
	public function _stripe_payment($token = null, array $data = array())
	{
		//try to create a customer
		try {

			//make sure we have a token and certain values
			if ($token == '' || !isset($data['order']) || !isset($data['store']))
			{
				throw new Exception('Invalid request.');
			}

			//load stripe helper
			$this->load->helper('stripe');
			$stripeConfig = $this->config->item('stripe');
			\Stripe\Stripe::setApiKey($stripeConfig['secret']);

			//create the customer
			$customer = \Stripe\Customer::create(array(
				'email' => $this->input->post('cust_email', TRUE),
				'source'  => $token, //credit card info, tokenized
				'metadata' => array(
					'first_name' => $this->input->post('cust_first_name', TRUE),
					'last_name' => $this->input->post('cust_last_name', TRUE),
					'phone' => $this->input->post('cust_phone', TRUE),
					'order_id' => $data['order']['details']['ecomm_order_id'],
					'confirmation_number' => $data['order']['details']['confirmationNumber']
				)
			));

			//return customer id
			return $customer->id;

		} catch(\Stripe\Error\Card $e) {

			//set message
			$message = 'An error occurred while processing your credit card.' . $e->getMessage();

			//insert for logging purposes
			$this->error_model->insert(array(
				'ecomm_order_id' => (int) $this->session->userdata('orderID'),
				'payment_method' => 'credit',
				'error_msg' => $message
			));

			//set error message and redirect, issue with card
			$this->session->set_flashdata('error', $message);
			redirect('/preorder/' . $data['store']['url_slug'] . '/contact');

		} catch (\Stripe\Error\RateLimit $e) {

			// Too many requests made to the API too quickly, set message
			$message = 'The server is really busy right now. Please try again in a few moments.' . $e->getMessage();

			//insert for logging purposes
			$this->error_model->insert(array(
				'ecomm_order_id' => (int) $this->session->userdata('orderID'),
				'payment_method' => 'credit',
				'error_msg' => $message
			));

			//set error message and redirect
			$this->session->set_flashdata('error', $message);
			redirect('/preorder/' . $data['store']['url_slug'] . '/contact');

		} catch (\Stripe\Error\InvalidRequest $e) {

			// Invalid parameters were supplied to Stripe's API
			$message = "Invalid parameters were supplied to Stripe's API:" . $e->getMessage();

			//insert for logging purposes
			$this->error_model->insert(array(
				'ecomm_order_id' => (int) $this->session->userdata('orderID'),
				'payment_method' => 'credit',
				'error_msg' => $message
			));

			//set error message and redirect
			$this->session->set_flashdata('error', 'Please resubmit your payment information and try again. If the error continues, please <a href="/preorder/' . $data['store']['url_slug'] . '/contact">contact</a> your order coordinator and inform them of the error.');
			redirect('/preorder/' . $data['store']['url_slug'] . '/checkout');

		} catch (\Stripe\Error\Authentication $e) {

			// Authentication with Stripe's API failed (maybe you changed API keys recently)
			$message = "Authentication with Stripe's API failed:" . $e->getMessage();

			//insert for logging purposes
			$this->error_model->insert(array(
				'ecomm_order_id' => (int) $this->session->userdata('orderID'),
				'payment_method' => 'credit',
				'error_msg' => $message
			));

			//set error message and redirect
			$this->session->set_flashdata('error', 'Invalid request made. Please contact your order coordinator and inform them that you received this error. ' . $e->getMessage());
			redirect('/preorder/' . $data['store']['url_slug'] . '/contact');

		} catch (\Stripe\Error\ApiConnection $e) {

			// Network communication with Stripe failed
			$message = 'Network communication with payment processor failed:' . $e->getMessage();

			//insert for logging purposes
			$this->error_model->insert(array(
				'ecomm_order_id' => (int) $this->session->userdata('orderID'),
				'payment_method' => 'credit',
				'error_msg' => $message
			));

			//set error message and redirect
			$this->session->set_flashdata('error', 'Invalid request made. Please contact your order coordinator and inform them that you received this error. ' . $e->getMessage());
			redirect('/preorder/' . $data['store']['url_slug'] . '/contact');

		}  catch (Exception $e) {

			//insert for logging purposes
			$this->error_model->insert(array(
				'ecomm_order_id' => (int) $this->session->userdata('orderID'),
				'payment_method' => 'credit',
				'error_msg' => $e->getMessage()
			));

			// Something else happened, completely unrelated to Stripe
			redirect('/preorder/' . $data['store']['url_slug'] . '/contact');
		}
	}

	/**
	 * Attempt to charge a customer for their order
	 * @param  [string] $customerId [stripe customer id]
	 * @return array with status information
	 */
	public function _stripe_charge($customerId = null)
	{
		try {

			//make sure we have a valid customer id
			if (is_null($customerId) || $customerId == '')
			{
				throw new Exception('Invalid stripe customer id.');
			}

			//make sure order is found
			$this->load->model('ecommerce/order_model');
			$order = $this->order_model->append('store')->get_by('stripe_cust_id', $customerId);
			if (empty($order) || $order == false)
			{
				throw new Exception('Order not found.');
			}

			//make sure we didn't already capture this order
			if ($order['status'] != 'authorized')
			{
				throw new Exception('This order is not ready to be charged.');
			}

			//set order id to use in other functions if neccessary
			$this->session->set_userdata('adminProcessingOrderID');

			//load up stripe
			$this->load->helper('stripe');
			$stripeConfig = $this->config->item('stripe');
			\Stripe\Stripe::setApiKey($stripeConfig['secret']);

			//make sure customer exists
			$customer = \Stripe\Customer::retrieve($customerId);

			//check tax rate again
			$taxRate = modules::run('ecommerce/_check_tax_rate', $order['ecomm_store_id']);

			//get cart info
			$cart = $this->order_model->getCart($order['ecomm_order_id']);

			//try to charge the customer
			$charge = \Stripe\Charge::create(array(
				'amount' => $cart['total'] * 100, // amount in cents
				'currency' => 'usd',
				'customer' => $customerId,
				'capture' => true,
				'description' => $order['store']['title'] . ' Preorder',
				'metadata' => array(
					'order_id' => $order['ecomm_order_id'],
					'confirmation_number' => $order['confirmationNumber']
				)
			));

			//if capture was successful, update order status to completed
			$this->order_model->skip_validation()->update($order['ecomm_order_id'], array(
				'status' => 'completed',
				'stripe_chrg_id' => $charge->id,
				'dateProcessed' => date('Y-m-d H:i:s'),
				'subtotal' => $cart['subtotal'],
				'tax' => $cart['tax'],
				'tax_rate' => $taxRate,
				'total' => $cart['total']
			));

			//return successful status
			return array('success' => true, 'message' => $order['cust_first_name'] . ' ' . $order['cust_last_name'] . ' was successfully charged.');

		} catch(\Stripe\Error\Card $e) {

			//set message
			$message = 'An error occurred while processing the credit card.' . $e->getMessage();

			//insert for logging purposes
			$this->error_model->insert(array(
				'ecomm_order_id' => (int) $this->session->userdata('adminProcessingOrderID'),
				'payment_method' => 'credit',
				'error_msg' => $message
			));

			//return error info
			return array('success' => false, 'message' => $message, 'type' => 'user');

		} catch (\Stripe\Error\RateLimit $e) {

			// Too many requests made to the API too quickly, set message
			$message = 'The server is really busy right now. Please try again in a few moments.' . $e->getMessage();

			//insert for logging purposes
			$this->error_model->insert(array(
				'ecomm_order_id' => (int) $this->session->userdata('adminProcessingOrderID'),
				'payment_method' => 'credit',
				'error_msg' => $message
			));

			//return error info
			return array('success' => false, 'message' => $message, 'type' => 'stripe');

		} catch (\Stripe\Error\InvalidRequest $e) {

			// Invalid parameters were supplied to Stripe's API
			$message = "Invalid parameters were supplied to Stripe's API:" . $e->getMessage();

			//insert for logging purposes
			$this->error_model->insert(array(
				'ecomm_order_id' => (int) $this->session->userdata('adminProcessingOrderID'),
				'payment_method' => 'credit',
				'error_msg' => $message
			));

			//return error info
			return array('success' => false, 'message' => $message, 'type' => 'stripe');

		} catch (\Stripe\Error\Authentication $e) {

			// Authentication with Stripe's API failed (maybe you changed API keys recently)
			$message = "Authentication with Stripe's API failed:" . $e->getMessage();

			//insert for logging purposes
			$this->error_model->insert(array(
				'ecomm_order_id' => (int) $this->session->userdata('adminProcessingOrderID'),
				'payment_method' => 'credit',
				'error_msg' => $message
			));

			//return error info
			return array('success' => false, 'message' => $message, 'type' => 'stripe');

		} catch (\Stripe\Error\ApiConnection $e) {

			// Network communication with Stripe failed
			$message = 'Network communication with Stripe failed:' . $e->getMessage();

			//insert for logging purposes
			$this->error_model->insert(array(
				'ecomm_order_id' => (int) $this->session->userdata('adminProcessingOrderID'),
				'payment_method' => 'credit',
				'error_msg' => $message
			));

			//return error info
			return array('success' => false, 'message' => $message, 'type' => 'stripe');

		}  catch (Exception $e) {

			//insert for logging purposes
			$this->error_model->insert(array(
				'ecomm_order_id' => (int) $this->session->userdata('adminProcessingOrderID'),
				'payment_method' => 'credit',
				'error_msg' => $e->getMessage()
			));

			//return error info
			return array('success' => false, 'message' => $e->getMessage(), 'type' => 'stripe');
		}
	}


	/**
	 * Refund a customers order
	 * @param  [string] $customerId [stripe customer id]
	 * @return array with status information
	 */
	public function _stripe_refund($customerId = null, $refundAmt = 0)
	{
		try {

			//make sure we have a valid customer id
			if (is_null($customerId) || $customerId == '')
			{
				throw new Exception('Invalid stripe customer id.');
			}

			//make sure order is found
			$this->load->model('ecommerce/order_model');
			$order = $this->order_model->append('store')->get_by('stripe_cust_id', $customerId);
			if (empty($order) || $order == false)
			{
				throw new Exception('Order not found.');
			}

			//make sure there is something to actually refund
			if ($order['status'] != 'completed')
			{
				throw new Exception('There is nothing to refund for this order.');
			}

			//make sure a charge id is set
			if ($order['stripe_chrg_id'] == '')
			{
				throw new Exception('Invalid charge id');
			}

			//set order id to use in other functions if neccessary
			$this->session->set_userdata('adminProcessingOrderID');

			//load up stripe
			$this->load->helper('stripe');
			$stripeConfig = $this->config->item('stripe');
			\Stripe\Stripe::setApiKey($stripeConfig['secret']);

			//set data to pass
			$refundData['charge'] = $order['stripe_chrg_id']; //charge id
			if ($refundAmt > 0)
			{
				$refundData['amount'] = number_format($refundAmt * 100, 0, '.', '');
			}

			//try to charge the customer
			$refund = \Stripe\Refund::create($refundData);

			//return successful status
			return array('success' => true, 'message' => $order['cust_first_name'] . ' ' . $order['cust_last_name'] . ' was successfully refunded.');

		} catch(\Stripe\Error\Card $e) {

			//set message
			$message = 'An error occurred while processing the credit card.' . $e->getMessage();

			//insert for logging purposes
			$this->error_model->insert(array(
				'ecomm_order_id' => (int) $this->session->userdata('adminProcessingOrderID'),
				'payment_method' => 'credit',
				'error_msg' => $message
			));

			//return error info
			return array('success' => false, 'message' => $message, 'type' => 'user');

		} catch (\Stripe\Error\RateLimit $e) {

			// Too many requests made to the API too quickly, set message
			$message = 'The server is really busy right now. Please try again in a few moments.' . $e->getMessage();

			//insert for logging purposes
			$this->error_model->insert(array(
				'ecomm_order_id' => (int) $this->session->userdata('adminProcessingOrderID'),
				'payment_method' => 'credit',
				'error_msg' => $message
			));

			//return error info
			return array('success' => false, 'message' => $message, 'type' => 'stripe');

		} catch (\Stripe\Error\InvalidRequest $e) {

			// Invalid parameters were supplied to Stripe's API
			$message = "Invalid parameters were supplied to Stripe's API:" . $e->getMessage();

			//insert for logging purposes
			$this->error_model->insert(array(
				'ecomm_order_id' => (int) $this->session->userdata('adminProcessingOrderID'),
				'payment_method' => 'credit',
				'error_msg' => $message
			));

			//return error info
			return array('success' => false, 'message' => $message, 'type' => 'stripe');

		} catch (\Stripe\Error\Authentication $e) {

			// Authentication with Stripe's API failed (maybe you changed API keys recently)
			$message = "Authentication with Stripe's API failed:" . $e->getMessage();

			//insert for logging purposes
			$this->error_model->insert(array(
				'ecomm_order_id' => (int) $this->session->userdata('adminProcessingOrderID'),
				'payment_method' => 'credit',
				'error_msg' => $message
			));

			//return error info
			return array('success' => false, 'message' => $message, 'type' => 'stripe');

		} catch (\Stripe\Error\ApiConnection $e) {

			// Network communication with Stripe failed
			$message = 'Network communication with Stripe failed:' . $e->getMessage();

			//insert for logging purposes
			$this->error_model->insert(array(
				'ecomm_order_id' => (int) $this->session->userdata('adminProcessingOrderID'),
				'payment_method' => 'credit',
				'error_msg' => $message
			));

			//return error info
			return array('success' => false, 'message' => $message, 'type' => 'stripe');

		}  catch (Exception $e) {

			//insert for logging purposes
			$this->error_model->insert(array(
				'ecomm_order_id' => (int) $this->session->userdata('adminProcessingOrderID'),
				'payment_method' => 'credit',
				'error_msg' => $e->getMessage()
			));

			//return error info
			return array('success' => false, 'message' => $e->getMessage(), 'type' => 'stripe');
		}
	}

	/************************************************************************************************************************************
	/
	/
	/ Paypal functions
	/
	/
	/***********************************************************************************************************************************/

	/**
	 * Action for starting the authentication process - redirects to paypal
	 * @param  array  $data [order and store info]
	 */
	public function _paypal_init_auth(array $data = array())
	{
		//try to submit payment
		try {

			//make sure we have the values we need first
			if (!isset($data['order']) || !isset($data['store']))
			{
				throw new Exception('Invalid request.');
			}

			//connect to paypal
			$apiContext = $this->_paypal_connect('/preorder/' . $data['store']['url_slug'] . '/contact');

			//set paypal as source
			$payer = new \PayPal\Api\Payer();
			$payer->setPaymentMethod('paypal');

			//set the amount(s)
			$details = new \PayPal\Api\Details();
			$details->setTax($data['order']['tax'])
				->setSubtotal($data['order']['subtotal']);
			$amount = new \PayPal\Api\Amount();
			$amount->setCurrency('USD')
				->setTotal($data['order']['total']);

			//start the transaction
			$transaction = new \PayPal\Api\Transaction();
			$transaction->setAmount($amount)
				#->setItemList($itemList)
				->setDescription($this->config->item('site_name') . ' ' . $data['store']['title'] . ' Preorder')
				->setInvoiceNumber($order['details']['confirmationNumber']);

			//Set redirect URLs
			$baseUrl = 'http';
			$baseUrl .= (($_SERVER['SERVER_PORT'] == 443 || (!empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on')))? 's://' . $_SERVER['HTTP_HOST'] : '://' . $_SERVER['HTTP_HOST'];
			$baseUrl .= '/preorder/' . $data['store']['url_slug'];
			$redirectUrls = new \PayPal\Api\RedirectUrls();
			$redirectUrls->setReturnUrl("$baseUrl/paypal?success=true")
				->setCancelUrl("$baseUrl/paypal?success=false");

			//Payment, set to authorize
			$payment = new \PayPal\Api\Payment();
			$payment->setIntent('authorize')
				->setPayer($payer)
				->setRedirectUrls($redirectUrls)
				->setTransactions(array($transaction))
				->create($apiContext);

			//redirect user to approval page upon success
			$approvalUrl = $payment->getApprovalLink();
			header('Location: ' . $approvalUrl);

		} catch (Exception $e) {

			//insert for logging purposes
			$this->error_model->insert(array(
				'ecomm_order_id' => (int) $this->session->userdata('orderID'),
				'payment_method' => 'paypal',
				'error_msg' => $e->getMessage()
			));

			// Display a very generic error to the user
			$this->session->set_flashdata('error', 'Invalid request made. Please contact your order coordinator and inform them that you received this error. ' . $e->getMessage());
			redirect('/preorder/' . $data['store']['url_slug'] . '/contact');
		}
	}

	/**
	 * Action for handling the confirm or denial of the payment, authorizes payment if approved
	 * @return array (paypal payment id, paypal payer id, paypal auth id)
	 */
	public function _paypal_confirm_auth()
	{
		try {

			//make sure this is a get request
			if (!$this->is_get())
			{
				throw new Exception('Invalid request.');
			}

			//make sure we have the expected values from paypal
			if (is_null($this->input->get('paymentId')) || is_null($this->input->get('token')) || is_null($this->input->get('PayerID')))
			{
				throw new Exception('Invalid request.');
			}

			//connect to paypal
			$apiContext = $this->_paypal_connect('/preorder/' . $data['store']['url_slug'] . '/contact');

			//get payment id and token, execute payment
			$paymentID = $this->input->get('paymentId', TRUE);
			$token = $this->input->get('token', TRUE);
			$payerID = $this->input->get('PayerID', TRUE);

			//get items we need for paypal
			$this->load->config('api_keys');
			$this->load->helper('paypal');
			$paypalConfig = $this->config->item('paypal');

			// try to connect to paypal
			$apiContext = new \PayPal\Rest\ApiContext(
				new \PayPal\Auth\OAuthTokenCredential(
					$paypalConfig['client_id'], // ClientID
					$paypalConfig['secret'] // ClientSecret
				)
			);

			//try to execute payment
			$execution = new \PayPal\Api\PaymentExecution();
			$execution->setPayerId($payerID);
			$payment = \PayPal\Api\Payment::get($paymentID, $apiContext);
			$payment->execute($execution, $apiContext);

			//get payment and auth after execution
			$payment = \PayPal\Api\Payment::get($paymentID, $apiContext);
			$transactions = $payment->getTransactions();
			$relatedResources = $transactions[0]->getRelatedResources();
			$authorization = $relatedResources[0]->getAuthorization();

			//format data to return
			$returnData = array(
				'paypal_auth_id' => $authorization->id,
				'paypal_pay_id' => $paymentID,
				'paypal_payer_id' => $payerID
			);
			return $returnData;

		} catch (Exception $e) {

			//insert for logging purposes
			$this->error_model->insert(array(
				'ecomm_order_id' => (int) $this->session->userdata('orderID'),
				'payment_method' => 'paypal',
				'error_msg' => $e->getMessage()
			));

			// Display a very generic error to the user
			$this->session->set_flashdata('error', 'Invalid request made. Please contact your order coordinator and inform them that you received this error. ' . $e->getMessage());
			redirect('/preorder/' . $data['store']['url_slug'] . '/contact');
		}
	}

	/**
	 * Attempt to capture a previously authorized charge
	 * @param  array  $paypalInfo [array containing auth id, and potentially payer id if needed later]
	 * @return array with status information
	 */
	public function _paypal_capture(array $paypalInfo = array())
	{
		try {

			//make sure we have a authorization id
			if (!isset($paypalInfo['paypal_auth_id']) || $paypalInfo['paypal_auth_id'] == '')
			{
				throw new Exception('');
			}

			//make sure order is found
			$this->load->model('ecommerce/order_model');
			$order = $this->order_model->append('store')->get_by('paypal_auth_id', $paypalInfo['paypal_auth_id']);
			if (empty($order) || $order == false)
			{
				throw new Exception('');
			}

			//make sure we didn't already capture this order
			if ($order['status'] != 'authorized')
			{
				throw new Exception('');
			}

			//connect to paypal, purposely leave the parameter blank because we don't want redirects right now
			$apiContext = $this->_paypal_connect('');

			//get authorization
			$authorization = \PayPal\Api\Authorization::get($paypalInfo['paypal_auth_id'], $apiContext);

			//check tax rate again
			$taxRate = modules::run('ecommerce/_check_tax_rate', $order['ecomm_store_id']);

			//get cart info
			$cart = $this->order_model->getCart($order['ecomm_order_id']);

			//set amount
			$amt = new \PayPal\Api\Amount();
			$amt->setCurrency('USD')
				->setTotal($cart['total']);

			//attempt capture
			$capture = new \PayPal\Api\Capture();
			$capture->setAmount($amt);
			$getCapture = $authorization->capture($capture, $apiContext);

			//if capture was successful, update order status to completed
			$this->order_model->skip_validation()->update($order['ecomm_order_id'], array(
				'status' => 'completed',
				'dateProcessed' => date('Y-m-d H:i:s'),
				'paypal_capt_id' => $getCapture->id,
				'subtotal' => $cart['subtotal'],
				'tax' => $cart['tax'],
				'tax_rate' => $taxRate,
				'total' => $cart['total']
			));

			//return successful status
			return array('success' => true, 'message' => $order['cust_first_name'] . ' ' . $order['cust_last_name'] . ' was successfully charged.');

		} catch (Exception $e) {

			//return unsuccessful status
			return array('success' => false, 'message' => $e->getMessage(), 'type' => 'user');
		}
	}

	/**
	 * Refund a previous capture
	 * @param  array  $paypalInfo [array containing auth id, and potentially payer id if needed later]
	 * @return array with status information
	 */
	public function _paypal_refund(array $paypalInfo = array(), $refundAmt = 0)
	{
		try {

			//make sure we have a authorization id
			if (!isset($paypalInfo['paypal_auth_id']) || $paypalInfo['paypal_auth_id'] == '')
			{
				throw new Exception('');
			}

			//make sure order is found
			$this->load->model('ecommerce/order_model');
			$order = $this->order_model->get_by('paypal_auth_id', $paypalInfo['paypal_auth_id']);
			if (empty($order) || $order == false)
			{
				throw new Exception('');
			}

			//make sure the user has been charged
			if ($order['status'] != 'completed')
			{
				throw new Exception('');
			}

			//connect to paypal, purposely leave the parameter blank because we don't want redirects right now
			$apiContext = $this->_paypal_connect('');

			//get cart info
			$cart = $this->order_model->getCart($order['ecomm_order_id']);
			$refundAmt = ($refundAmt > 0)? $refundAmt : $cart['total'];

			//set refund amount
			$amt = new \PayPal\Api\Amount();
			$amt->setCurrency('USD')
				->setTotal(number_format($refundAmt, 2, '.', ''));
			$refund = new \PayPal\Api\Refund();
			$refund->setAmount($amt);

			//try to refund it
			$capture = \PayPal\Api\Capture::get($order['paypal_capt_id'], $apiContext);
			$refundData = $capture->refund($refund, $apiContext);

			//return successful status
			return array('success' => true, 'message' => $order['cust_first_name'] . ' ' . $order['cust_last_name'] . " was successfully refunded.");

		} catch (Exception $e) {

			//return unsuccessful status
			return array('success' => false, 'message' => $e->getMessage(), 'type' => 'paypal');
		}
	}

	/**
	 * Void an authorization
	 * @param  array  $paypalInfo [array containing auth id, and potentially payer id if needed later]
	 * @return array with status information
	 */
	public function _paypal_void(array $paypalInfo = array())
	{
		try {

			//make sure we have a authorization id
			if (!isset($paypalInfo['paypal_auth_id']) || $paypalInfo['paypal_auth_id'] == '')
			{
				throw new Exception('');
			}

			//make sure order is found
			$this->load->model('ecommerce/order_model');
			$order = $this->order_model->get_by('paypal_auth_id', $paypalInfo['paypal_auth_id']);
			if (empty($order) || $order == false)
			{
				throw new Exception('');
			}

			//make sure we didn't already capture or cancel this order
			if ($order['status'] != 'authorized')
			{
				throw new Exception('');
			}

			//connect to paypal, purposely leave the parameter blank because we don't want redirects right now
			$apiContext = $this->_paypal_connect('');

			//get authorization
			$authorization = \PayPal\Api\Authorization::get($paypalInfo['paypal_auth_id'], $apiContext);
			$getVoid = $authorization->void($apiContext);

			//return successful status
			return array('success' => true, 'message' => $order['cust_first_name'] . ' ' . $order['cust_last_name'] . "'s' order authorization was successfully voided.");

		} catch (Exception $e) {

			//return unsuccessful status
			return array('success' => false, 'message' => $e->getMessage(), 'type' => 'paypal');
		}
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/
	private function _paypal_connect($redirectUrl = '')
	{
		//load paypal helper
		$this->load->helper('paypal');
		$paypalConfig = $this->config->item('paypal');

		try {

			// try to connect to paypal
			$apiContext = new \PayPal\Rest\ApiContext(
				new \PayPal\Auth\OAuthTokenCredential(
					$paypalConfig['client_id'], // ClientID
					$paypalConfig['secret'] // ClientSecret
				)
			);

			$apiContext->setConfig(array(
				'mode' => (ENVIRONMENT === 'production')? 'live' : 'sandbox',
				#'http.ConnectionTimeOut' => 30,
				'log.LogEnabled' => true,
				'log.FileName' => '../PayPal.log',
				'log.LogLevel' => 'DEBUG',
				#'validation.level' => 'log',
				#'cache.enabled' => true,
			));

			return $apiContext;

		} catch (\PayPal\Exception\PayPalConfigurationException $e) {

			//set message
			$message = 'Invalid Paypal configuration. ' . $e->getMessage();

			//insert for logging purposes
			$this->error_model->insert(array(
				'ecomm_order_id' => (int) $this->session->userdata('orderID'),
				'payment_method' => 'paypal',
				'error_msg' => $message
			));

			//redirect and show or return error message
			$this->session->set_flashdata('error', 'There was a problem connecting with Paypal. Please contact your order coordinator and inform them that you received this error.');
			if ($redirectUrl != '')
			{
				redirect($redirectUrl);
			} else {
				return $message;
			}

		} catch (\PayPal\Exception\PayPalConnectionException $e) {

			//set message
			$message = 'Paypal connection failed. ' . $e->getMessage();

			//insert for logging purposes
			$this->error_model->insert(array(
				'ecomm_order_id' => (int) $this->session->userdata('orderID'),
				'payment_method' => 'paypal',
				'error_msg' => $message
			));

			//redirect and show error message
			$this->session->set_flashdata('error', 'There was a problem connecting with Paypal. Please contact your order coordinator and inform them that you received this error.');
			if ($redirectUrl != '')
			{
				redirect($redirectUrl);
			} else {
				return $message;
			}

		} catch (\PayPal\Exception\PayPalInvalidCredentialException $e) {

			//set message
			$message = 'Invalid Paypal credentials. ' . $e->getMessage();

			//insert for logging purposes
			$this->error_model->insert(array(
				'ecomm_order_id' => (int) $this->session->userdata('orderID'),
				'payment_method' => 'paypal',
				'error_msg' => $message
			));

			//redirect and show error message
			$this->session->set_flashdata('error', 'Invalid Paypal credentials. ' . $e->getMessage());
			if ($redirectUrl != '')
			{
				redirect($redirectUrl);
			} else {
				return $message;
			}

		} catch (\PayPal\Exception\PayPalMissingCredentialException $e) {

			//set message
			$message = 'Missing Paypal credentials. ' . $e->getMessage();

			//insert for logging purposes
			$this->error_model->insert(array(
				'ecomm_order_id' => (int) $this->session->userdata('orderID'),
				'payment_method' => 'paypal',
				'error_msg' => $message
			));

			//redirect and show error message
			$this->session->set_flashdata('error', 'There was a problem connecting with Paypal. Please contact your order coordinator and inform them that you received this error.');
			if ($redirectUrl != '')
			{
				redirect($redirectUrl);
			} else {
				return $message;
			}

		} catch (Exception $e) {

			//insert for logging purposes
			$this->error_model->insert(array(
				'ecomm_order_id' => (int) $this->session->userdata('orderID'),
				'payment_method' => 'paypal',
				'error_msg' => $e->getMessage()
			));

			// Display a very generic error to the user
			$this->session->set_flashdata('error', 'There was a problem connecting with Paypal. Please contact your order coordinator and inform them that you received this error.');
			if ($redirectUrl != '')
			{
				redirect($redirectUrl);
			} else {
				return $e->getMessage();
			}
		}
	}
}