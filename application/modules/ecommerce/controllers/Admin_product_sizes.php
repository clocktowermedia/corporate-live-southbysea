<?php
class Admin_product_sizes extends Admin_controller {

	//layout variables
	public $defaultLayout;

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//set layout variable(s)
		$this->defaultLayout = $this->get_admin_layout();

		//load relevant models
		$this->load->model('ecommerce/product_model');
		$this->load->model('ecommerce/product_size_model');
	}

	/**
	* Shows all sizes for a product
	*/
	function index($productID = 0)
	{
		//make sure store is valid
		modules::run('ecommerce/admin_products/_product_is_valid', $productID);

		//get product & size info
		$data['product'] = $this->product_model->get($productID);

		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->_get_custom_view();
		$data['title'] = 'Sizes for ' . $data['product']['title'];

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}

	/**
	* view and action for adding a size
	*/
	function create($productID = 0)
	{
		//make sure store is valid
		modules::run('ecommerce/admin_products/_product_is_valid', $productID);

		//get product info
		$data['product'] = $this->product_model->get($productID);

		//tell it what view file to use
		$data['view'] = $this->_get_custom_view();
		$data['title'] = 'Add Size to ' . $data['product']['title'];

		//load form validation library
		$this->load->library('form_validation');
		$validationRules = $this->product_size_model->validate;
		unset($validationRules['ecomm_product_id']);
		$this->form_validation->set_rules($validationRules);

		//if the form was submitted, update database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the view and show form errors
				$data['size'] = $this->input->post(NULL, TRUE);
				$this->load->view($this->defaultLayout, $data);
			} else {

				// insert record to db & redirect
				$sizeID = $this->product_size_model->insert($this->input->post(NULL, TRUE) + array('ecomm_product_id' => $productID));

				//show error or success message
				if ((int) $sizeID <= 0)
				{
					//display success message & redirect
					$this->session->set_flashdata('error', 'An error occurred, please try again.');
					redirect('/admin/ecommerce/products/sizes/create/' . $productID);
				} else {

					//display success message & redirect
					$this->session->set_flashdata('success', 'Product size successfully created.');
					redirect('/admin/ecommerce/products/sizes/' . $productID);
				}
			}
		} else {
			// tell it what layout to use and load the view
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	* view and action for editing a size
	*/
	function edit($sizeID = 0)
	{
		//make sure size exists
		$this->_size_is_valid($sizeID);

		//grab size info for view
		$data['size'] = $this->product_size_model->append('product')->get($sizeID);

		//tell it what view file to use
		$data['view'] = $this->_get_custom_view();
		$data['title'] = 'Edit ' . $data['size']['title'];

		//load form validation library
		$this->load->library('form_validation');
		$validationRules = $this->product_size_model->validate;
		unset($validationRules['ecomm_product_id']);
		$this->form_validation->set_rules($validationRules);

		//is modal?
		$isModal = ($this->input->get('modal'))? true : false;

		//if the form was submitted, update database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the view and show form errors
				if ($isModal)
				{
					$this->load->view($data['view'], $data);
				} else {
					$this->load->view($this->defaultLayout, $data);
				}
			} else {

				if ($data['size']['price'] != $this->input->post('price'))
				{
					//determine if any orders were placed
					$this->load->model('ecommerce/order_model');
					$ordersPlaced = $this->order_model->countBySize($data['size']['ecomm_product_size_id']);

					if ($ordersPlaced > 0)
					{
						$this->session->set_flashdata('error', 'Orders have already been placed for this product size. You must cancel/refund all orders where this product size was purchased first.');
						redirect('/admin/ecommerce/products/sizes/' . $data['size']['ecomm_product_id']);
					}
				}

				// update record to db & redirect
				$this->product_size_model->skip_validation()->update($sizeID, $this->input->post(NULL, TRUE));

				//display success message & redirect
				$this->session->set_flashdata('success', 'Product size was successfully updated.');
				redirect('/admin/ecommerce/products/sizes/' . $data['size']['ecomm_product_id']);
			}
		} else {
			// tell it what layout to use and load the view
			if ($isModal)
			{
				$this->load->view($data['view'], $data);
			} else {
				$this->load->view($this->defaultLayout, $data);
			}
		}
	}

	/**
	* Changes size status to deleted
	*/
	function delete($sizeID = 0)
	{
		//make sure size exists
		$this->_size_is_valid($sizeID);

		//get size info
		$size = $this->product_size_model->get($sizeID);

		//determine if any orders were placed
		$this->load->model('ecommerce/order_model');
		$ordersPlaced = $this->order_model->countBySize($size['ecomm_product_size_id']);

		if ($ordersPlaced > 0)
		{
			//show error message
			$this->session->set_flashdata('error', 'Orders have already been placed for this product size. You must cancel/refund all orders where this product size was purchased first.');
			redirect('/admin/ecommerce/products/sizes/' . $size['ecomm_product_id']);
		} else {
			//delete size
			$this->product_size_model->delete($sizeID);
		}

		//redirect upon success
		$this->session->set_flashdata('success', 'Product size was deleted.');
		redirect('/admin/ecommerce/products/sizes/' . $size['ecomm_product_id']);
	}

	/**
	* View for re-ordering sizes
	*/
	/*
	function reorder()
	{
		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->_get_custom_view();
		$data['title'] = 'Reorder Products';

		//get a list of items
		$data['list_html'] = $this->product_model->getReorderHtml();

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}
	*/

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

	private function _get_custom_view()
	{
		return 'admin/products/sizes/' . $this->get_view();
	}

	/**
	* Checks to see that size ID is valid and that the size exists
	*/
	public function _size_is_valid($sizeID = 0)
	{
		//make sure size id is an integer
		$sizeID = (int) $sizeID;

		//make sure size id is greater than zero
		if ($sizeID > 0)
		{
			//check to see if the size exists
			$exists = $this->product_size_model->get($sizeID);

			if ($exists == false)
			{
				$this->session->set_flashdata('error', 'Invalid size id.');
				redirect('/admin/ecommerce');
			}

		} else {
			$this->session->set_flashdata('error', 'Invalid size id.');
			redirect('/admin/ecommerce');
		}
	}
}