<?php

class Ecommerce_emails extends MY_Controller {

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//load relevant models
		$this->load->model('ecommerce/store_model');
		$this->load->model('ecommerce/product_model');
	}

	public function _send_order_confirmation_email(array $data = array())
	{
		//email subject
		$emailData['subject'] = $this->config->item('site_name') . ' ' . $data['store']['title'] . ' Preorder Store Order Confirmation';

		//build order email content
		$content = '<p>Thank you for your order. You may find a summary of your order below. Your order confirmation number is <strong>#' . $data['order']['details']['confirmationNumber'] . '</strong>.</p><br/>';
		$content .= '<p>Please note that your order will not be shipped to you individually and will not go into production until store closes on ' . date('n/d/Y', strtotime($data['store']['end_date'])) . '.</p><br/>';
		$content .= '<table class="inner-table">' .
		'<tr>' .
			'<th></th>' .
			'<th>Item</th>' .
			'<th>Size</th>' .
			'<th>Quantity</th>' .
			'<th>Unit Price</th>' .
			'<th>Subtotal</th>' .
		'</tr>';
		foreach ($data['order']['cart'] AS $item)
		{
			if ($item['product']['image']['thumbFullpath'] != '')
			{
				$image = '<img class="product-img" src="https://' . $_SERVER['HTTP_HOST'] . $item['product']['image']['thumbFullpath'] .'" title="' . $item['product']['title'] .'">';
			} else {
				$image = '<img class="product-img" src="http://dummyimage.com/100x100/fff/57656e&text=No+Image" title="' . $item['product']['title'] .'">';
			}

			$content .= '<tr>' .
				'<td>' . $image . '</td>' .
				'<td>' . $item['product']['title'] . '</td>' .
				'<td>' . $item['size']['size'] . '</td>' .
				'<td>' . $item['qty'] . '</td>' .
				'<td>$' . $item['size']['price'] . '</td>' .
				'<td>$' . number_format($item['qty'] * $item['size']['price'], 2) . '</td>' .
			'</tr>';
		}
		$content .= '<tr><td colspan="6">&nbsp;</td></tr>';
		$content .= '<tr>
			<td colspan="4"></td>
			<td>Subtotal:</td>
			<td>$' . $data['order']['subtotal'] . '</td>
		</tr>
		<tr>
			<td colspan="4">&nbsp;</td>
			<td>Tax:</td>
			<td>$' . $data['order']['tax'] . '</td>
		</tr>
		<tr>
			<td colspan="4">&nbsp;</td>
			<td>Total:</td>
			<td>$' . $data['order']['total'] . '</td>
		</tr>
		</table>';

		$content .= ($data['order']['details']['comments'] != '')? '<br/><p><strong>Order Notes:</strong> ' . $data['order']['details']['comments'] . '</p>' : '';

		//style email
		$emailData['content'] = $this->convert_text_to_html_email_table($emailData['subject'], $content);
		$this->load->library('css_to_inline_styles');
		$this->css_to_inline_styles->setHtml($emailData['content']);
		$this->css_to_inline_styles->setCSS(file_get_contents(APPPATH . '../assets/css/email.css'));
		$emailData['html'] = $this->css_to_inline_styles->convert();

		//tell it what view to use & get content
		$emailData['view'] = 'emails/checkout';
		$this->load->library('parser');
		$content = $this->parser->parse($this->get_email_layout(), $emailData, TRUE);

		//load email & parser library
		$this->load->library('email');
		$this->config->load('email');

		//set from and to info
		$this->email->from($this->config->item('site_email'), $this->config->item('site_email_name'));
		$this->email->to($data['order']['details']['cust_email']);
		$this->email->bcc('forms@southbysea.com', 'South by Sea [Ecommerce Email]');

		//add content to email
		$this->email->subject($emailData['subject']);
		$this->email->message($content);

		//send email
		$this->email->sbs_send();
	}

	public function _send_order_confirmation_email_pdf(array $data = array())
	{
		//email subject
		$emailData['subject'] = $this->config->item('site_name') . ' ' . $data['store']['title'] . ' Preorder Store Order Confirmation';

		//build order email content
		$content = '<p>Thank you for your order. You will find a summary of your order in the attached PDF. Your order confirmation number is <strong>#' . $data['order']['details']['confirmationNumber'] . '</strong>.</p><br/>';
		$content .= '<p>Please note that your order will not be shipped to you individually and will not go into production until store closes on ' . date('n/d/Y', strtotime($data['store']['end_date'])) . '.</p><br/>';

		//style email
		$emailData['content'] = $this->convert_text_to_html_email_table($emailData['subject'], $content);
		$this->load->library('css_to_inline_styles');
		$this->css_to_inline_styles->setHtml($emailData['content']);
		$this->css_to_inline_styles->setCSS(file_get_contents(APPPATH . '../assets/css/email.css'));
		$emailData['html'] = $this->css_to_inline_styles->convert();

		//tell it what view to use & get content
		$emailData['view'] = 'emails/checkout';
		$this->load->library('parser');
		$content = $this->parser->parse($this->get_email_layout(), $emailData, TRUE);

		//load email & parser library
		$this->load->library('email');
		$this->config->load('email');

		$pdf = modules::run('ecommerce/generate_pdf',  $data['order']['details']['confirmationNumber'], 'string');
		$this->email->attach($pdf['pdf'], 'attachment', $pdf['fileName'], 'application/pdf');

		//set from and to info
		$this->email->from($this->config->item('site_email'), $this->config->item('site_email_name'));
		$this->email->to($data['order']['details']['cust_email']);
		$this->email->bcc('forms@southbysea.com', 'South by Sea [Ecommerce Email]');

		//add content to email
		$this->email->subject($emailData['subject']);
		$this->email->message($content);

		//send email
		$this->email->sbs_send();
	}

	public function _send_order_processed_email(array $data = array())
	{
		//email subject
		$emailData['subject'] = $this->config->item('site_name') . ' ' . $data['store']['title'] . ' Preorder Store Order Update';

		//build order email content
		$content = '<p>Your order with confirmation number <strong>#' . $data['order']['details']['confirmationNumber'] . '</strong> has been processed and will be shipped to you shortly.</p><br/>';
		$content .= '<table class="inner-table">' .
		'<tr>' .
			'<th></th>' .
			'<th>Item</th>' .
			'<th>Size</th>' .
			'<th>Quantity</th>' .
			'<th>Unit Price</th>' .
			'<th>Subtotal</th>' .
		'</tr>';
		foreach ($data['order']['cart'] AS $item)
		{
			if ($item['product']['image']['thumbFullpath'] != '')
			{
				$image = '<img class="product-img" src="https://' . $_SERVER['HTTP_HOST'] . $item['product']['image']['thumbFullpath'] .'" title="' . $item['product']['title'] .'">';
			} else {
				$image = '<img class="product-img" src="http://dummyimage.com/100x100/fff/57656e&text=No+Image" title="' . $item['product']['title'] .'">';
			}

			$content .= '<tr>' .
				'<td>' . $image . '</td>' .
				'<td>' . $item['product']['title'] . '</td>' .
				'<td>' . $item['size']['size'] . '</td>' .
				'<td>' . $item['qty'] . '</td>' .
				'<td>$' . $item['size']['price'] . '</td>' .
				'<td>$' . number_format($item['qty'] * $item['size']['price'], 2) . '</td>' .
			'</tr>';
		}
		$content .= '<tr><td colspan="6">&nbsp;</td></tr>';
		$content .= '<tr>
			<td colspan="4"></td>
			<td>Subtotal:</td>
			<td>$' . $data['order']['subtotal'] . '</td>
		</tr>
		<tr>
			<td colspan="4">&nbsp;</td>
			<td>Tax:</td>
			<td>$' . $data['order']['tax'] . '</td>
		</tr>
		<tr>
			<td colspan="4">&nbsp;</td>
			<td>Total:</td>
			<td>$' . $data['order']['total'] . '</td>
		</tr>
		</table>';

		$content .= ($data['order']['details']['comments'] != '')? '<br/><p><strong>Order Notes:</strong> ' . $data['order']['details']['comments'] . '</p>' : '';

		//style email
		$emailData['content'] = $this->convert_text_to_html_email_table($emailData['subject'], $content);
		$this->load->library('css_to_inline_styles');
		$this->css_to_inline_styles->setHtml($emailData['content']);
		$this->css_to_inline_styles->setCSS(file_get_contents(APPPATH . '../assets/css/email.css'));
		$emailData['html'] = $this->css_to_inline_styles->convert();

		//tell it what view to use & get content
		$emailData['view'] = 'emails/checkout';
		$this->load->library('parser');
		$content = $this->parser->parse($this->get_email_layout(), $emailData, TRUE);

		//load email & parser library
		$this->load->library('email');
		$this->config->load('email');

		//set from and to info
		$this->email->from($this->config->item('site_email'), $this->config->item('site_email_name'));
		$this->email->to($data['order']['details']['cust_email']);
		$this->email->bcc('forms@southbysea.com', 'South by Sea [Ecommerce Email]');

		//add content to email
		$this->email->subject($emailData['subject']);
		$this->email->message($content);

		//send email
		$this->email->sbs_send();
	}

	public function _send_order_cancelled_email(array $data = array())
	{
		//email subject
		$emailData['subject'] = $this->config->item('site_name') . ' ' . $data['order']['details']['store']['title'] . ' Preorder Store Order Cancellation';

		//build order email content
		$content = '<p>We apologize for the inconvenience, but the transaction for your order with confirmation number <strong>#' . $data['order']['details']['confirmationNumber'] . '</strong> did not process successfully.</p><br/>';
		// if ($data['order']['status'] == 'completed')
		// {
		// 	if ($data['order']['payment_method'] == 'credit')
		// 	{
		// 		$content .= '<p>Your order has been refunded. Your charge account will be credited with the amount in 3-5 business days.</p>';
		// 	} else {
		// 		$content .= '<p>Your order has been refunded. The refunded amount should show up in your Paypal account in 3-5 business days.</p>';
		// 	}
		// } else {
			$content .= '<p>The transaction associated with this order has been voided and you will be sent an invoice shortly. With that invoice you can pay by card within 24 hours to ensure your order is still included! If the invoice is not paid, your order will be canceled.</p><br/>';
		// }
		$content .= '<p>For reference, please find a summary of your cancelled order below.</p><br/>';
		$content .= '<table class="inner-table">' .
		'<tr>' .
			'<th></th>' .
			'<th>Item</th>' .
			'<th>Size</th>' .
			'<th>Quantity</th>' .
			'<th>Unit Price</th>' .
			'<th>Subtotal</th>' .
		'</tr>';
		foreach ($data['order']['cart'] AS $item)
		{
			if ($item['product']['image']['thumbFullpath'] != '')
			{
				$image = '<img class="product-img" src="https://' . $_SERVER['HTTP_HOST'] . $item['product']['image']['thumbFullpath'] .'" title="' . $item['product']['title'] .'">';
			} else {
				$image = '<img class="product-img" src="http://dummyimage.com/100x100/fff/57656e&text=No+Image" title="' . $item['product']['title'] .'">';
			}

			$content .= '<tr>' .
				'<td>' . $image . '</td>' .
				'<td>' . $item['product']['title'] . '</td>' .
				'<td>' . $item['size']['size'] . '</td>' .
				'<td>' . $item['qty'] . '</td>' .
				'<td>$' . $item['size']['price'] . '</td>' .
				'<td>$' . number_format($item['qty'] * $item['size']['price'], 2) . '</td>' .
			'</tr>';
		}
		$content .= '<tr><td colspan="6">&nbsp;</td></tr>';
		$content .= '<tr>
			<td colspan="4"></td>
			<td>Subtotal:</td>
			<td>$' . $data['order']['subtotal'] . '</td>
		</tr>
		<tr>
			<td colspan="4">&nbsp;</td>
			<td>Tax:</td>
			<td>$' . $data['order']['tax'] . '</td>
		</tr>
		<tr>
			<td colspan="4">&nbsp;</td>
			<td>Total:</td>
			<td>$' . $data['order']['total'] . '</td>
		</tr>
		</table>';

		$content .= ($data['order']['details']['comments'] != '')? '<br/><p><strong>Order Notes:</strong> ' . $data['order']['details']['comments'] . '</p>' : '';

		//style email
		$emailData['content'] = $this->convert_text_to_html_email_table($emailData['subject'], $content);
		$this->load->library('css_to_inline_styles');
		$this->css_to_inline_styles->setHtml($emailData['content']);
		$this->css_to_inline_styles->setCSS(file_get_contents(APPPATH . '../assets/css/email.css'));
		$emailData['html'] = $this->css_to_inline_styles->convert();

		//tell it what view to use & get content
		$emailData['view'] = 'emails/checkout';
		$this->load->library('parser');
		$content = $this->parser->parse($this->get_email_layout(), $emailData, TRUE);

		//load email & parser library
		$this->load->library('email');
		$this->config->load('email');

		//set from and to info
		$this->email->from($this->config->item('site_email'), $this->config->item('site_email_name'));
		$this->email->to($data['order']['details']['cust_email']);
		$this->email->bcc('forms@southbysea.com', 'South by Sea [Ecommerce Email]');

		//add content to email
		$this->email->subject($emailData['subject']);
		$this->email->message($content);

		//send email
		$this->email->sbs_send();
	}

	public function _send_declined_transaction_email(array $data = array(), $error = '')
	{
		//email subject
		$emailData['subject'] = $this->config->item('site_name') . ' ' . $data['order']['details']['store']['title'] . ' Preorder Store Order - Payment Declined';

		//build order email content
		$content = '<p>Payment for your order with confirmation number <strong>#' . $data['order']['details']['confirmationNumber'] . '</strong> has been declined.</p><br/>';
		$content .= '<p>The following message was returned:<br/>"' . $error . '" </p><br/>';
		$content .= '<p>For reference, please find a summary of your order below. Please <a href="https://' . $_SERVER['HTTP_HOST'] . '/preorder/' . $data['order']['details']['store']['url_slug'] . '/contact">contact your order coordinator</a> to resolve the issue.</p><br/>';
		$content .= '<table class="inner-table">' .
		'<tr>' .
			'<th></th>' .
			'<th>Item</th>' .
			'<th>Size</th>' .
			'<th>Quantity</th>' .
			'<th>Unit Price</th>' .
			'<th>Subtotal</th>' .
		'</tr>';
		foreach ($data['order']['cart'] AS $item)
		{
			if ($item['product']['image']['thumbFullpath'] != '')
			{
				$image = '<img class="product-img" src="https://' . $_SERVER['HTTP_HOST'] . $item['product']['image']['thumbFullpath'] .'" title="' . $item['product']['title'] .'">';
			} else {
				$image = '<img class="product-img" src="http://dummyimage.com/100x100/fff/57656e&text=No+Image" title="' . $item['product']['title'] .'">';
			}

			$content .= '<tr>' .
				'<td>' . $image . '</td>' .
				'<td>' . $item['product']['title'] . '</td>' .
				'<td>' . $item['size']['size'] . '</td>' .
				'<td>' . $item['qty'] . '</td>' .
				'<td>$' . $item['size']['price'] . '</td>' .
				'<td>$' . number_format($item['qty'] * $item['size']['price'], 2) . '</td>' .
			'</tr>';
		}
		$content .= '<tr><td colspan="6">&nbsp;</td></tr>';
		$content .= '<tr>
			<td colspan="4"></td>
			<td>Subtotal:</td>
			<td>$' . $data['order']['subtotal'] . '</td>
		</tr>
		<tr>
			<td colspan="4">&nbsp;</td>
			<td>Tax:</td>
			<td>$' . $data['order']['tax'] . '</td>
		</tr>
		<tr>
			<td colspan="4">&nbsp;</td>
			<td>Total:</td>
			<td>$' . $data['order']['total'] . '</td>
		</tr>
		</table>';

		$content .= ($data['order']['details']['comments'] != '')? '<br/><p><strong>Order Notes:</strong> ' . $data['order']['details']['comments'] . '</p>' : '';

		//style email
		$emailData['content'] = $this->convert_text_to_html_email_table($emailData['subject'], $content);
		$this->load->library('css_to_inline_styles');
		$this->css_to_inline_styles->setHtml($emailData['content']);
		$this->css_to_inline_styles->setCSS(file_get_contents(APPPATH . '../assets/css/email.css'));
		$emailData['html'] = $this->css_to_inline_styles->convert();

		//tell it what view to use & get content
		$emailData['view'] = 'emails/checkout';
		$this->load->library('parser');
		$content = $this->parser->parse($this->get_email_layout(), $emailData, TRUE);

		//load email & parser library
		$this->load->library('email');
		$this->config->load('email');

		//set from and to info
		$this->email->from($this->config->item('site_email'), $this->config->item('site_email_name'));
		$this->email->to($data['order']['details']['cust_email']);
		$this->email->bcc('forms@southbysea.com', 'South by Sea [Ecommerce Email]');

		//add content to email
		$this->email->subject($emailData['subject']);
		$this->email->message($content);

		//send email
		$this->email->sbs_send();
	}

	public function _send_product_cancelled_email()
	{

	}

	/**
	 * Sends an email to the order coordinator
	 * @param  array  $data [data we need to populate values int he email]
	 */
	public function _send_contact_email(array $data = array())
	{
		//send email to order coordinator
		$_POST = modules::run('forms/_format_post', $this->input->post(NULL, TRUE));

		//email subject
		$emailData['subject'] = $this->config->item('site_name') . ' ' . $data['store']['title'] . ' Preorder Store Contact Form';

		//build additional content
		$additionalContent = '<br/><p>This contact form was submitted from <a href="http://' . $_SERVER['HTTP_HOST'] . '/preorder/' . $data['store']['url_slug'] . '">/preorder/' . $data['store']['url_slug'] . '</a>.</p>';

		//format content into table
		$emailData['content'] = $this->convert_1D_array_to_html_email_table($emailData['subject'], $this->input->post(NULL, TRUE), $additionalContent);
		$emailData['content'] = modules::run('forms/_htmlentitiesExcludeHTMLTags', $emailData['content'], ENT_QUOTES);
		$this->load->library('css_to_inline_styles');
		$this->css_to_inline_styles->setHtml($emailData['content']);
		$this->css_to_inline_styles->setCSS(file_get_contents(APPPATH . '../assets/css/email.css'));
		$emailData['html'] = $this->css_to_inline_styles->convert();

		//tell it what view to use & get content
		$emailData['view'] = $this->get_email_view();
		$this->load->library('parser');
		$content = $this->parser->parse($this->get_email_layout(), $emailData, TRUE);

		//load email & parser library
		$this->load->library('email');
		$this->config->load('email');

		//set reply and from info
		$this->email->from($this->input->post('email', TRUE), $this->input->post('name', TRUE));
		$this->email->reply_to($this->input->post('email', TRUE), $this->input->post('name', TRUE));
		$this->email->to($data['store']['coord_email']);
		$this->email->bcc('forms@southbysea.com', 'South by Sea [Ecommerce Email]');

		//add content to email
		$this->email->subject($emailData['subject']);
		$this->email->message($content);

		//send email
		$this->email->sbs_send();
	}
}
