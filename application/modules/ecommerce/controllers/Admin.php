<?php
class Admin extends Admin_controller {

	//layout variables
	public $defaultLayout;

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//set layout variable(s)
		$this->defaultLayout = $this->get_admin_layout();

		//load relevant models
		$this->load->model('ecommerce/store_model');
	}

	function fix_store_counts()
	{
		$stores = $this->store_model->get_all();
		$this->load->model('ecommerce/order_model');
		foreach ($stores AS $store)
		{
			$this->order_model->updateUnprocessedCount($store['ecomm_store_id']);
		}

		die('done');
	}

	/**
	* Shows all active stores in a datatable
	*/
	function index()
	{
		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Ecommerce Stores';

		//get list of managers
		$this->load->model('admins/admin_model');
		$data['managers'] = $this->admin_model->getRoleDropdown('Account Manager', true);

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}

	/**
	* view and action for adding a store
	*/
	function create()
	{
		//tell it what view file to use
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Add Ecommerce Store';

		//get list of managers
		$this->load->model('admins/admin_model');
		$data['managers'] = $this->admin_model->getRoleDropdown('Account Manager', true);

		//load form validation library
		$this->load->library('form_validation');

		//get validation rules from model, set extra options as neccessary
		$validationRules = $this->store_model->validate;
		$validationRules['url_slug']['rules'] .= '|is_unique[' . $this->store_model->_table . '.url_slug]';
		unset($validationRules['created_by'], $validationRules['tax_rate']);
		$this->form_validation->set_rules($validationRules);

		//if the form was submitted, update database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the view and show form errors
				$data['store'] = $this->input->post(NULL, TRUE);
				$this->load->view($this->defaultLayout, $data);
			} else {

				//insert store
				$storeID = $this->store_model->skip_validation()->insert($this->input->post(NULL, TRUE));

				//make sure store creation worked
				if ((int) $storeID <= 0)
				{
					//display error message & redirect
					$this->session->set_flashdata('error', 'An error occurred, please try again.');
					redirect('/admin/ecommerce/create/');
				} else {
					//display success message & redirect
					$this->session->set_flashdata('success', 'Store was successfully created. You should add some products to your store now.');
					redirect('/admin/ecommerce/products/' . $storeID);
				}
			}
		} else {
			// tell it what layout to use and load the view
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	* view and action for editing a store
	*/
	function edit($storeID = 0)
	{
		//make sure store exists
		$this->_store_is_valid($storeID);

		//grab store info for view
		$this->load->library('encryption');
		$data['store'] = $this->store_model->append('acct_mgr')->get($storeID);
		$data['store']['storeP'] = $this->encryption->decrypt($data['store']['storeP']);


		//tell it what view file to use
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Edit ' . $data['store']['title'];

		//get list of managers
		$this->load->model('admins/admin_model');
		$data['managers'] = $this->admin_model->getRoleDropdown('Account Manager', true);

		//load form validation library
		$this->load->library('form_validation');
		$validationRules = $this->store_model->validate;
		unset($validationRules['created_by'], $validationRules['tax_rate']);
		$this->form_validation->set_rules($validationRules);

		//if the form was submitted, update database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the view and show form errors
				$this->load->view($this->defaultLayout, $data);
			} else {
				//double check that the url does not already exist
				$exists = $this->store_model->get_by(array(
					'url_slug' => $this->input->post('url_slug', TRUE),
					'ecomm_store_id !=' => $storeID
				));
				if (!empty($exists) || isset($exists['ecomm_store_id']))
				{
					$this->session->set_flashdata('error', 'Store was not updated, a store with that URL already exists.');
					redirect('/admin/ecommerce/edit/' . $storeID);
				}

				//update store
				$this->store_model->skip_validation()->update($storeID, $this->input->post(NULL, TRUE));

				//display success message & redirect
				$this->session->set_flashdata('success', 'Store was successfully updated.');
				redirect('/admin/ecommerce');
			}
		} else {
			// tell it what layout to use and load the view
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	* Changes store status to enabled
	*/
	function enable($storeID = 0)
	{
		//make sure store exists
		$this->_store_is_valid($storeID);

		//set status to deleted
		$this->store_model->enable($storeID);

		//redirect upon success
		$this->session->set_flashdata('success', 'Store was successfully enabled.');
		redirect('/admin/ecommerce/');
	}

	/**
	* Changes store status to deleted
	*/
	function disable($storeID = 0)
	{
		//make sure store exists
		$this->_store_is_valid($storeID);

		//set status to deleted
		$this->store_model->disable($storeID);

		//redirect upon success
		$this->session->set_flashdata('success', 'Store was successfully disabled.');
		redirect('/admin/ecommerce/');
	}

	/**
	* Changes store status to deleted
	*/
	function delete($storeID = 0)
	{
		//make sure store exists
		$this->_store_is_valid($storeID);

		//set status to deleted
		$this->store_model->soft_delete($storeID);

		//redirect upon success
		$this->session->set_flashdata('success', 'Store was deleted.');
		redirect('/admin/ecommerce/');
	}

	/**
	* Action for cancelling a store and all its orders
	*/
	function cancel($storeID = 0)
	{
		//make sure store exists
		$this->_store_is_valid($storeID);

		//get all completed orders
		$this->load->model('ecommerce/order_model');
		$orders = $this->order_model->get_many_by(array(
			'ecomm_store_id' => $storeID,
			'status !=' => 'pending',
			'status !=' => 'refunded'
		));

		//loop through orders and attempt to cancel
		if (count($orders) > 0)
		{
			foreach ($orders As $order)
			{
				$cancelData = modules::run('ecommerce/admin_orders/_cancel_order', $order['ecomm_order_id']);
				if ($cancelData['success'] == false)
				{
					$errors[] = $cancelData['message'];
				}
			}
		}

		//set status to disabled
		$this->store_model->disable($storeID);

		//redirect upon success
		$messageType = (isset($errors) && count($errors) > 0)? 'error' : 'success';
		$message = (isset($errors) && count($errors) > 0)? 'Store has been disabled and all orders have been attempted to be refunded. However, some errors occurred during the refund process. Please check the <a href="/admin/ecommerce/orders/' . $storeID . '">orders</a> page to see which orders still need to be refunded.' : 'Store has been disabled and all its orders have been cancelled.';
		$this->session->set_flashdata($messageType, $message);
		redirect('/admin/ecommerce/');
	}

	/**
	 * Clones a store (store, products, product sizes)
	 */
	function clone_store($storeID = 0)
	{
		//make sure store exists
		$this->_store_is_valid($storeID);

		try {
			//get store info
			$store = $this->store_model->get($storeID);
			unset($store['ecomm_store_id'], $store['dateCreated'], $store['dateUpdated']);

			//format url and dates
			$store['url_slug'] = $store['url_slug'] . '-' . time();
			$store['start_time'] = date('H:i:s', strtotime($store['start_date']));
			$store['end_time'] = date('H:i:s', strtotime($store['end_date']));


			//make new store
			$newStoreID = $this->store_model->skip_validation()->insert($store);

			//if store creation successful
			if ((int) $newStoreID > 0)
			{
				//get products
				$this->load->model('ecommerce/product_model');
				$products = $this->product_model->append('sizes')->get_many_by('ecomm_store_id', $storeID);

				//copy products
				if (count($products) > 0)
				{
					foreach ($products AS $product)
					{
						//save sizes for later
						$sizes = $product['sizes'];

						//create new product
						unset($product['ecomm_product_id'], $product['sizes'], $product['dateCreated'], $product['dateUpdated']);
						$product['ecomm_store_id'] = $newStoreID;
						$newProductID = $this->product_model->skip_validation()->insert($product);

						if ((int) $newProductID <= 0)
						{
							throw new Exception('Store information was successfully cloned, but not all products could be copied.');
						}

						//copy sizes
						if (count($sizes) > 0)
						{
							$this->load->model('ecommerce/product_size_model');
							foreach ($sizes AS $size)
							{
								unset($size['ecomm_product_size_id'], $size['dateCreated'], $size['dateUpdated']);
								$size['ecomm_product_id'] = $newProductID;
								$this->product_size_model->skip_validation()->insert($size);
							}
						}
					}
				}

				//show success message and redirect
				$this->session->set_flashdata('success', 'Store successfully copied. Please adjust the Store URL.');
				redirect('/admin/ecommerce/edit/' . $newStoreID);

			} else {
				throw new Exception('Store cloning failed. Please try again.');
			}
		} catch (Exception $e) {
			$this->session->set_flashdata('error', $e->getMessage());
			redirect('/admin/ecommerce');
		}
	}

	/**
	 * Action for processing all orders in a store
	 * @param  integer $storeID
	 */
	function process_orders($storeID = 0, $redirect = true)
	{
		//make sure store exists
		$this->_store_is_valid($storeID);

		//get store info
		$store = $this->store_model->get($storeID);

		//get all authorized orders for that store, group by type
		$this->load->model('ecommerce/order_model');

		//get all paypal orders
		$ppOrders = $this->order_model->get_many_by(array(
			'ecomm_store_id' => $storeID,
			'payment_method' => 'paypal',
			'status' => 'authorized'
		));

		//get all stripe orders
		$stripeOrders = $this->order_model->get_many_by(array(
			'ecomm_store_id' => $storeID,
			'payment_method' => 'credit',
			'status' => 'authorized'
		));

		//string to hold any messages
		$messages = array();

		//loop through paypal orders and try to process them
		if (count($ppOrders) > 0)
		{
			$paypalError = false;

			//attempt to connect first
			$paypalConnection = modules::run('ecommerce/ecommerce_gateways/_paypal_connect', '');

			//connection failed?
			if (is_string($paypalConnection))
			{
				$paypalError = true;
				$messages['errors'][] = 'Remaining paypal transaction processing for <a href="/admin/ecommerce/orders/' . $store['ecomm_store_id'] . '" target="_blank">' . $store['title'] . '</a> has been halted. ' . $captureData['message'];

			} else {

				//loop through orders
				foreach ($ppOrders AS $order)
				{
					//attempt to capture
					$captureData = modules::run('ecommerce/ecommerce_gateways/_paypal_capture', $order);

					//if error occurred, add to notifications
					if ($captureData['success'] == false && $captureData['type'] == 'paypal')
					{
						$paypalError = true;
						$messages['errors'][] = 'Remaining paypal transaction processing for <a href="/admin/ecommerce/orders/' . $store['ecomm_store_id'] . '" target="_blank">' . $store['title'] . '</a> has been halted for <a href="/admin/ecommerce/orders/view/' . $order['ecomm_order_id'] . '" target="_blank">order confirmation #' . $order['confirmationNumber'] . '</a>. ' . $captureData['message'];
						break;
					}

					//if success was false, add to notification message list
					if ($captureData['success'] == false)
					{
						$messages['notifications'][] = 'Error processing <a href="/admin/ecommerce/orders/view/' . $order['ecomm_order_id'] . '" target="_blank">order confirmation #' . $order['confirmationNumber'] . '</a> for <a href="/admin/ecommerce/orders/' . $store['ecomm_store_id'] . '" target="_blank">' . $store['title'] . '</a>. ' . $captureData['message'];

						//get more details we will need for the email
						if ($captureData['type'] == 'user')
						{
							$orderData = $this->order_model->getCart($order['ecomm_order_id']);
							$orderData['details'] = $this->order_model->append('store')->get($order['ecomm_order_id']);
							modules::run('ecommerce/ecommerce_emails/_send_declined_transaction_email', array('order' => $orderData), $captureData['message']);
						}

					} else {
						//send confirmation email
						$emailData['order'] = $this->order_model->getCart($order['ecomm_order_id']);
						$emailData['order']['details'] = $order;
						$emailData['store'] = $store;
						modules::run('ecommerce/ecommerce_emails/_send_order_processed_email', $emailData);
					}
				}

				//set success message if applicable
				if ($paypalError == false)
				{
					$messages['success'][] = 'Paypal orders successfully processed for ' . $store['title'] . '.';
				}
			}
		} else {
			$messages['success'][] = 'No existing paypal transactions needed to be processed for ' . $store['title'] . '.';
		}

		//loop through credit card/stripe orders and try to process
		if (count($stripeOrders) > 0)
		{
			$stripeError = false;

			//loop through orders
			foreach ($stripeOrders AS $order)
			{
				//attempt to capture
				$captureData = modules::run('ecommerce/ecommerce_gateways/_stripe_charge', $order['stripe_cust_id']);

				//if there was a stripe error, do not continue
				if ($captureData['success'] == false && $captureData['type'] == 'stripe')
				{
					$stripeError = true;
					$messages['errors'][] = 'Remaining credit card transaction processing for <a href="/admin/ecommerce/orders/view/' . $order['ecomm_order_id'] . '" target="_blank">' . $store['title'] . '</a> has been halted for <a href="/admin/ecommerce/orders/view/' . $order['ecomm_order_id'] . '" target="_blank">order confirmation # ' . $order['confirmationNumber'] . '</a>. ' . $captureData['message'];
					break;
				}

				//if success was false, add to notification message list
				if ($captureData['success'] == false)
				{
					$messages['notifications'][] = 'Error processing order confirmation #' . $order['confirmationNumber'] . ' for ' . $store['title'] . '. ' . $captureData['message'];

					//get more details we will need for the email
					if ($captureData['type'] == 'user')
					{
						$orderData = $this->order_model->getCart($order['ecomm_order_id']);
						$orderData['details'] = $this->order_model->append('store')->get($order['ecomm_order_id']);
						modules::run('ecommerce/ecommerce_emails/_send_declined_transaction_email', array('order' => $orderData), $captureData['message']);
					}
				} else {
					//send confirmation email
					$emailData['order'] = $this->order_model->getCart($order['ecomm_order_id']);
					$emailData['order']['details'] = $order;
					modules::run('ecommerce/ecommerce_emails/_send_order_processed_email', $emailData);
				}
			}

			//set success message if applicable
			if ($stripeError == false)
			{
				$messages['success'][] = 'Credit card orders successfully processed for ' . $store['title'] . '.';
			}

		} else {
			$messages['success'][] = 'No existing credit card transactions needed to be processed for ' . $store['title'] . '.';
		}

		//set "processed" to true
		if (empty($messages['errors']))
		{
			$this->store_model->skip_validation()->update($storeID, array(
				'processed' => 1,
				'processed_by' => (int) $this->session->userdata('adminID')
			));
		}

		//show or return messages, redirect if applicable
		if ($redirect == false)
		{
			return $messages;
		} else {
			if (isset($messages['errors']) && count($messages['errors']) > 0)
			{
				$this->session->set_flashdata('error', implode('<br/>', $messages['errors']));
			}
			if (isset($messages['notifications']) && count($messages['notifications']) > 0)
			{
				$this->session->set_flashdata('info', implode('<br/>', $messages['notifications']));
			}
			if (isset($messages['success']) && count($messages['success']) > 0)
			{
				$this->session->set_flashdata('success', implode('<br/>', $messages['success']));
			}
			redirect('/admin/ecommerce/closing');
		}
	}

	/**
	 * View for closing soon/closed stores
	 */
	public function closing($acct_mgr_id = 0)
	{
		//show stores by selected or logged in account manager
		if (is_numeric($acct_mgr_id) && $acct_mgr_id > 0) {

			//if account manager was set, make sure that account is active, then filter by store
			modules::run('admins/admin/_admin_is_valid', $acct_mgr_id);
			$filterData['acct_mgr_id'] = $acct_mgr_id;

			//get admin data
			$data['acct_mgr'] = $this->admin_model->get($acct_mgr_id);

		} else {

			//if logged in user is an account manager and they have not specifically selected to view all stores, default to their stores
			$account = $this->admin_model->append('role')->get($this->adminID);
			if ($account['role']['title'] == 'Account Manager')
			{
				redirect('/admin/ecommerce/closing/' . $this->adminID);
			}
		}

		//get list of account managers
		$this->load->model('admins/admin_model');
		$data['managers'] = $this->admin_model->getRoleDropdown('Account Manager', false);

		//add to filters
		$filterData['processed'] = 0;
		$filterData['status'] = 'enabled';

		//get all stores closing within
		$data['stores']['soon'] = $this->store_model->get_stores_with_minimum(array_merge($filterData, array(
			'end_date >' => date('Y-m-d H:i:s'),
			'end_date <=' => date('Y-m-d H:i:s', strtotime('+48 hours'))
		)));

		//get all stores closing within
		$data['stores']['closed'] = $this->store_model->get_stores_with_minimum(array_merge($filterData, array(
			'end_date <' => date('Y-m-d H:i:s'),
		)));

		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Ecommerce Stores Closing Soon';
		$data['title'] .= ($acct_mgr_id > 0)? ' - ' . $data['acct_mgr']['firstName'] . ' ' . $data['acct_mgr']['lastName'] : '';

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}

	/**
	 * Action for processing a list of stores
	 */
	public function process()
	{
		try {
			//make sure we are posting
			if (!$this->is_post())
			{
				throw new Exception('Invalid request.');
			}

			$stores = $this->input->post('stores', TRUE);

			//make sure we have values being passed
			if (is_null($stores) || !is_array($stores) || empty($stores))
			{
				throw new Exception('Must select stores');
			}

			//create empty array for messages
			$messages = array(
				'errors' => array(),
				'notifications' => array(),
				'success' => array()
			);

			//start looping through stores
			foreach ($stores AS $storeID)
			{
				$processData = $this->process_orders($storeID, false);

				//merge message data
				if (isset($processData['errors']))
				{
					$messages['errors'] = array_merge($messages['errors'], (array) $processData['errors']);
				}
				if (isset($processData['notifications']))
				{
					$messages['notifications'] = array_merge($messages['notifications'], (array) $processData['notifications']);
				}
				if (isset($processData['success']))
				{
					$messages['success'] = array_merge($messages['success'], (array) $processData['success']);
				}
			}

			//set messages and redirect
			if (count($messages['errors']) > 0)
			{
				$this->session->set_flashdata('error', implode('<br/>', $messages['errors']));
			}
			if (count($messages['notifications']) > 0)
			{
				$this->session->set_flashdata('info', implode('<br/>', $messages['notifications']));
			}
			if (count($messages['success']) > 0)
			{
				$this->session->set_flashdata('success', implode('<br/>', $messages['success']));
			}
			redirect('/admin/ecommerce/closing');

		} catch (Exception $e) {

			//show error message and redirect
			$this->session->set_flashdata('error', $e->getMessage());
			redirect('/admin/ecommerce/closing');
		}
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

	/**
	* Checks to see that store ID is valid and that the store exists
	*/
	public function _store_is_valid($storeID = 0)
	{
		//make sure store id is an integer
		$storeID = (int) $storeID;

		//make sure store id is greater than zero
		if ($storeID > 0)
		{
			//check to see if the store exists
			$exists = $this->store_model->get($storeID);

			if ($exists == false)
			{
				$this->session->set_flashdata('error', 'Invalid store id.');
				redirect('/admin/ecommerce');
			}

		} else {
			$this->session->set_flashdata('error', 'Invalid store id.');
			redirect('/admin/ecommerce');
		}
	}
}