<?php
class Admin_datatables extends Admin_controller {

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//load model
		$this->load->model('ecommerce/store_model');

		//load library
		$this->load->library('datatables');
	}

	/**
	* uses datatables library to get list of stores in a json format for use in admin index
	*/
	function data()
	{
		//grab data from stores table in db
		$output = $this->datatables->select('s.ecomm_store_id')
			->select('CONCAT("<a target=\"_blank\" href=\"/preorder/", s.url_slug, "\">", s.title ,"</a>") AS storeLink', false)
			->select('s.url_slug')
			->select('DATE_FORMAT(s.start_date, "%Y-%m-%e") AS startDate', false)
			->select('DATE_FORMAT(s.end_date, "%Y-%m-%e") AS endDate', false)
			->select('CONCAT(a.firstName, " ", a.lastName) AS createdBy', false)
			->select('CONCAT(am.firstName, " ", am.lastName) AS acctMgr', false)
			->select('s.unprocessedCount')
			#->select('COUNT(o.ecomm_order_id) AS unprocessedCount', false)
			->select('CASE WHEN s.status = "disabled" THEN CONCAT("<li><a data-confirm=\"Are you sure you want to enable this?\" href=\"/admin/ecommerce/enable/", s.ecomm_store_id, "\"><i class=\"fa fa-check\"></i> Enable</a></li>") ELSE "" END AS enableButton', false)
			->select('CASE WHEN s.status = "enabled" THEN CONCAT("<li><a data-confirm=\"Are you sure you want to disable this?\" href=\"/admin/ecommerce/disable/", s.ecomm_store_id, "\"><i class=\"fa fa-ban\"></i> Disable</a></li>") ELSE "" END AS disableButton', false)
			->add_column('actions',  '<div class="btn-group">
					<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-shopping-cart"></i> Store <span class="caret"></span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li><a href="/admin/ecommerce/products/$1"><i class="fa fa-tag"></i> Products</a></li>
						<li><a href="/admin/ecommerce/orders/$1"><i class="fa fa-usd"></i> Orders</a></li>
						<li><a href="/admin/ecommerce/process-orders/$1" data-confirm="Are you sure you want to process this?"><i class="fa fa-check"></i> Process Orders</a></li>
						<li><a href="/admin/ecommerce/orders/export/$1" data-confirm="Are you sure you want to export this?"><i class="fa fa-download"></i> Export Orders</a></li>
						<li><a href="/admin/ecommerce/cancel/$1" data-confirm="Are you sure you want to cancel this?"><i class="fa fa-ban"></i> Cancel Store</a></li>
						<li><a href="/admin/ecommerce/edit/$1"><i class="fa fa-edit"></i> Edit</a></li>
						<li><a href="/admin/ecommerce/clone-store/$1" data-confirm="Are you sure you want to clone this?"><i class="fa fa-files-o"></i> Clone Store</a></li>
						$2
						$3
						<li><a data-confirm="Are you sure you want to delete this?" href="/admin/ecommerce/delete/$1"><i class="fa fa-trash-o"></i> Delete</a></li>
					</ul>
				</div>', 's.ecomm_store_id, enableButton, disableButton')
			->unset_column('s.ecomm_store_id')
			->unset_column('enableButton')
			->unset_column('disableButton')
			->from($this->store_model->_table . ' s')
			->join('Admins a', 'a.adminID = s.created_by', 'left outer')
			->join('Admins am', 'am.adminID = s.acct_mgr_id', 'left outer')
			#->join('EcommOrders o', 'o.ecomm_store_id = s.ecomm_store_id AND o.status = "authorized"', 'left outer')
			->where('s.status !=', 'deleted')
			->where('s.status !=', 'disabled')
			->where('s.end_date >=', date('Y-m-d H:i:s'))
			->group_by('s.ecomm_store_id')
			->generate();

		echo $output;
	}

	/**
	* uses datatables library to get list of stores in a json format for use in admin index
	*/
	function data_disabled()
	{
		//grab data from stores table in db
		$output = $this->datatables->select('s.ecomm_store_id')
			->select('CONCAT("<a target=\"_blank\" href=\"/preorder/", s.url_slug, "\">", s.title ,"</a>") AS storeLink', false)
			->select('s.url_slug')
			->select('DATE_FORMAT(s.start_date, "%Y-%m-%e") AS startDate', false)
			->select('DATE_FORMAT(s.end_date, "%Y-%m-%e") AS endDate', false)
			->select('CONCAT(a.firstName, " ", a.lastName) AS createdBy', false)
			->select('CONCAT(am.firstName, " ", am.lastName) AS acctMgr', false)
			->select('s.unprocessedCount')
			#->select('COUNT(o.ecomm_order_id) AS unprocessedCount', false)
			->select('CASE WHEN s.status = "disabled" THEN CONCAT("<li><a data-confirm=\"Are you sure you want to enable this?\" href=\"/admin/ecommerce/enable/", s.ecomm_store_id, "\"><i class=\"fa fa-check\"></i> Enable</a></li>") ELSE "" END AS enableButton', false)
			->select('CASE WHEN s.status = "enabled" THEN CONCAT("<li><a data-confirm=\"Are you sure you want to disable this?\" href=\"/admin/ecommerce/disable/", s.ecomm_store_id, "\"><i class=\"fa fa-ban\"></i> Disable</a></li>") ELSE "" END AS disableButton', false)
			->add_column('actions',  '<div class="btn-group">
					<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-shopping-cart"></i> Store <span class="caret"></span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li><a href="/admin/ecommerce/products/$1"><i class="fa fa-tag"></i> Products</a></li>
						<li><a href="/admin/ecommerce/orders/$1"><i class="fa fa-usd"></i> Orders</a></li>
						<li><a href="/admin/ecommerce/orders/export/$1"><i class="fa fa-download"></i> Export Orders</a></li>
						<li><a href="/admin/ecommerce/edit/$1"><i class="fa fa-edit"></i> Edit</a></li>
						<li><a href="/admin/ecommerce/clone-store/$1"><i class="fa fa-files-o"></i> Clone Store</a></li>
						$2
						$3
						<li><a data-confirm="Are you sure you want to delete this?" href="/admin/ecommerce/delete/$1"><i class="fa fa-trash-o"></i> Delete</a></li>
					</ul>
				</div>', 's.ecomm_store_id, enableButton, disableButton')
			->unset_column('s.ecomm_store_id')
			->unset_column('enableButton')
			->unset_column('disableButton')
			->from($this->store_model->_table . ' s')
			->join('Admins a', 'a.adminID = s.created_by', 'left outer')
			->join('Admins am', 'am.adminID = s.acct_mgr_id', 'left outer')
			#->join('EcommOrders o', 'o.ecomm_store_id = s.ecomm_store_id  AND o.status = "authorized"', 'left outer')
			->where('s.status', 'disabled')
			->group_by('s.ecomm_store_id')
			->generate();

		echo $output;
	}

	/**
	* uses datatables library to get list of stores in a json format for use in admin index
	*/
	function data_inactive()
	{
		//grab data from stores table in db
		$output = $this->datatables->select('s.ecomm_store_id')
			->select('CONCAT("<a target=\"_blank\" href=\"/preorder/", s.url_slug, "\">", s.title ,"</a>") AS storeLink', false)
			->select('s.url_slug')
			->select('DATE_FORMAT(s.start_date, "%Y-%m-%e") AS startDate', false)
			->select('DATE_FORMAT(s.end_date, "%Y-%m-%e") AS endDate', false)
			->select('CONCAT(a.firstName, " ", a.lastName) AS createdBy', false)
			->select('CONCAT(am.firstName, " ", am.lastName) AS acctMgr', false)
			->select('s.unprocessedCount')
			#->select('COUNT(o.ecomm_order_id) AS unprocessedCount', false)
			->select('CASE WHEN s.status = "disabled" THEN CONCAT("<li><a data-confirm=\"Are you sure you want to enable this?\" href=\"/admin/ecommerce/enable/", s.ecomm_store_id, "\"><i class=\"fa fa-check\"></i> Enable</a></li>") ELSE "" END AS enableButton', false)
			->select('CASE WHEN s.status = "enabled" THEN CONCAT("<li><a data-confirm=\"Are you sure you want to disable this?\" href=\"/admin/ecommerce/disable/", s.ecomm_store_id, "\"><i class=\"fa fa-ban\"></i> Disable</a></li>") ELSE "" END AS disableButton', false)
			->add_column('actions',  '<div class="btn-group">
					<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-shopping-cart"></i> Store <span class="caret"></span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li><a href="/admin/ecommerce/products/$1"><i class="fa fa-tag"></i> Products</a></li>
						<li><a href="/admin/ecommerce/orders/$1"><i class="fa fa-usd"></i> Orders</a></li>
						<li><a href="/admin/ecommerce/process-orders/$1"><i class="fa fa-check"></i> Process Orders</a></li>
						<li><a href="/admin/ecommerce/orders/export/$1"><i class="fa fa-download"></i> Export Orders</a></li>
						<li><a href="/admin/ecommerce/edit/$1"><i class="fa fa-edit"></i> Edit</a></li>
						<li><a href="/admin/ecommerce/clone-store/$1"><i class="fa fa-files-o"></i> Clone Store</a></li>
						<li><a data-confirm="Are you sure you want to do this?" href="/admin/ecommerce/cancel/$1"><i class="fa fa-ban"></i> Refund Orders</a></li>
						$2
						$3
						<li><a data-confirm="Are you sure you want to delete this?" href="/admin/ecommerce/delete/$1"><i class="fa fa-trash-o"></i> Delete</a></li>
					</ul>
				</div>', 's.ecomm_store_id, enableButton, disableButton')
			->unset_column('s.ecomm_store_id')
			->unset_column('enableButton')
			->unset_column('disableButton')
			->from($this->store_model->_table . ' s')
			->join('Admins a', 'a.adminID = s.created_by', 'left outer')
			->join('Admins am', 'am.adminID = s.acct_mgr_id', 'left outer')
			#->join('EcommOrders o', 'o.ecomm_store_id = s.ecomm_store_id AND o.status = "authorized"', 'left outer')
			->where('s.status !=', 'deleted')
			->where('s.status !=', 'disabled')
			->where('s.end_date <', date('Y-m-d H:i:s'))
			->group_by('s.ecomm_store_id')
			->generate();

		echo $output;
	}

	function orders($storeID = 0)
	{
		if ($storeID > 0)
		{
			//make sure store is valid
			modules::run('ecommerce/admin/_store_is_valid', $storeID);
		}

		//load model
		$this->load->model('ecommerce/order_model');

		//grab data from table
		$this->datatables->select('o.ecomm_order_id, o.dateUpdated, o.cust_first_name, o.cust_last_name, o.cust_email, o.confirmationNumber, o.payment_method, o.status')
			->select('CASE WHEN o.status = "authorized" THEN CONCAT("<li><a href=\"/admin/ecommerce/orders/process/", o.ecomm_order_id, "\"><i class=\"fa fa-check\"></i> Process</a></li>") ELSE "" END AS processButton', false)
			->select('CASE WHEN o.status = "completed" THEN CONCAT("<li><a href=\"/admin/ecommerce/orders/view/", o.ecomm_order_id, "#partial-refund\"><i class=\"fa fa-dollar\"></i> Partial Refund</a></li>") ELSE "" END AS partialRefundButton', false)
			->add_column('actions',  '<div class="btn-group">
					<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-shopping-cart"></i> Order <span class="caret"></span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li><a href="/admin/ecommerce/orders/view/$1"><i class="fa fa-eye"></i> View</a></li>
						$2
						<li><a href="/admin/ecommerce/orders/cancel/$1"><i class="fa fa-undo"></i> Cancel/Refund</a></li>
						$3
						<!--<li><a data-confirm="Are you sure you want to delete this?" href="/admin/ecommerce/products/delete/$1"><i class="fa fa-trash-o"></i> Delete</a></li>-->
					</ul>
				</div>', 'o.ecomm_order_id, processButton, partialRefundButton')
			->unset_column('o.ecomm_order_id')
			->unset_column('processButton')
			->unset_column('partialRefundButton')
			->from($this->order_model->_table . ' o')
			->where('o.status !=', 'pending');

		if ($storeID > 0)
		{
			//limit by store id
			$this->datatables->where('o.ecomm_store_id', $storeID);
		}

		$output = $this->datatables->generate();
		echo $output;
	}

	function products($storeID = 0)
	{
		if ($storeID > 0)
		{
			//make sure store is valid
			modules::run('ecommerce/admin/_store_is_valid', $storeID);
		}

		//load model
		$this->load->model('ecommerce/product_model');
		$this->load->model('ecommerce/product_size_model');

		//grab data from products table in db
		$this->datatables->select('p.ecomm_product_id, p.title')
			->select('GROUP_CONCAT(s.size SEPARATOR ",") AS sizes', false)
			->select('p.price')
			->add_column('actions',  '<div class="btn-group">
					<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-tag"></i> Product <span class="caret"></span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li><a href="/admin/ecommerce/products/edit/$1"><i class="fa fa-edit"></i> Edit</a></li>
						<li><a href="/admin/ecommerce/products/sizes/$1"><i class="fa fa-sort-amount-asc"></i> Sizes</a></li>
						<li><a data-confirm="Are you sure you want to delete this?" href="/admin/ecommerce/products/delete/$1"><i class="fa fa-trash-o"></i> Delete</a></li>
					</ul>
				</div>', 'p.ecomm_product_id')
			->unset_column('p.ecomm_product_id')
			->from($this->product_model->_table . ' p')
			->join($this->product_size_model->_table . ' s', 's.ecomm_product_id = p.ecomm_product_id', 'left outer')
			->group_by('p.ecomm_product_id')
			->where('p.status !=', 'deleted');

		if ($storeID > 0)
		{
			//limit by store id
			$this->datatables->where('p.ecomm_store_id', $storeID);
		}

		$output = $this->datatables->generate();
		echo $output;
	}

	function sizes($productID = 0)
	{
		if ($productID > 0)
		{
			//make sure product is valid
			modules::run('ecommerce/admin_products/_product_is_valid', $productID);
		}

		//load model
		$this->load->model('ecommerce/product_size_model');

		//grab data from sizes table in db
		$this->datatables->select('s.ecomm_product_size_id, s.size, s.price')
			->add_column('actions',  '<div class="btn-group">
					<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-sort-amount-asc"></i> Size <span class="caret"></span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<!--<li><a href="/admin/ecommerce/products/sizes/edit/$1"><i class="fa fa-edit"></i> Edit</a></li>-->
						<li><a data-confirm="Are you sure you want to delete this?" href="/admin/ecommerce/products/sizes/$1"><i class="fa fa-trash-o"></i> Delete</a></li>
					</ul>
				</div>', 's.ecomm_product_size_id')
			->unset_column('s.ecomm_product_size_id')
			->from($this->product_size_model->_table . ' s');

		if ($productID > 0)
		{
			//limit by product id
			$this->datatables->where('s.ecomm_product_id', $productID);
		}

		$output = $this->datatables->generate();
		echo $output;
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

}
