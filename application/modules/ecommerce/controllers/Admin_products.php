<?php
class Admin_products extends Admin_controller {

	//layout variables
	public $defaultLayout;

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//set layout variable(s)
		$this->defaultLayout = $this->get_admin_layout();

		//load relevant models
		$this->load->model('ecommerce/store_model');
		$this->load->model('ecommerce/product_model');
	}

	/**
	* Shows all products for a store
	*/
	function index($storeID = 0)
	{
		//make sure store is valid
		modules::run('ecommerce/admin/_store_is_valid', $storeID);

		//get store info
		$data['store'] = $this->store_model->get($storeID);

		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->_get_custom_view();
		$data['title'] = 'Products for ' . $data['store']['title'];

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}

	/**
	* view and action for adding a product
	*/
	function create($storeID = 0)
	{
		//make sure store is valid
		modules::run('ecommerce/admin/_store_is_valid', $storeID);

		//get store info
		$data['store'] = $this->store_model->get($storeID);

		//tell it what view file to use
		$data['view'] = $this->_get_custom_view();
		$data['title'] = 'Add Product to ' . $data['store']['title'];

		//load form validation library
		$this->load->library('form_validation');
		$validationRules = $this->product_model->validate;
		unset($validationRules['ecomm_store_id']);
		$this->form_validation->set_rules($validationRules);

		//if the form was submitted, update database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the view and show form errors
				$data['product'] = $this->input->post(NULL, TRUE);
				$this->load->view($this->defaultLayout, $data);

			} else {

				//make sure url not already in use for this store
				$urlExists = $this->product_model->get_by(array(
					'ecomm_store_id' => $storeID,
					'url_slug' => $this->input->post('url_slug')
				));
				if (!empty($exists) || isset($exists['ecomm_product_id']))
				{
					$this->session->set_flashdata('error', 'A product with this URL is already in use for this store.');
					redirect('/admin/ecommerce/products/create/' . $storeID);
				}

				$insertData = $this->input->post(NULL, TRUE);

				// insert record to db & redirect
				$productID = $this->product_model->insert($insertData + array('ecomm_store_id' => $storeID));

				//show error or success message
				if ((int) $productID <= 0)
				{
					//display success message & redirect
					$this->session->set_flashdata('error', 'An error occurred, please try again.');
					redirect('/admin/ecommerce/products/create/' . $storeID);
				} else {

					//upload image if applicable
					if (isset($_FILES) && $_FILES['userfile']['name'] != '')
					{
						//try to upload the file
						$this->load->library('uploader');
						$this->uploader->set_model('ecommerce/product_image_model');
						$this->uploader->set_upload_paths(array(
							'imgUploadFolder' => '/assets/uploads/ecommerce/' . $storeID . '/',
							'imgThumbUploadFolder' => '/assets/uploads/ecommerce/' . $storeID . '/thumbnails/'
						));
						$this->uploader->set_thumbnail_config(array(
							'width' => '400',
							'height' => '400',
							'maintain_ratio' => TRUE
						));

						$imageID = $this->uploader->upload_image(array('ecomm_product_id' => $productID, 'defaultImg' => 1));
						if (!is_int($imageID) || (int) $imageID <= 0)
						{
							$this->session->set_flashdata('error', 'Product successfully created. However - ' . $imageID);
							redirect('/admin/ecommerce/products/' . $storeID);
						}
					}

					//display success message & redirect
					$this->session->set_flashdata('success', 'Product successfully created.');
					redirect('/admin/ecommerce/products/' . $storeID);
				}
			}
		} else {
			// tell it what layout to use and load the view
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	* view and action for editing a product
	*/
	function edit($productID = 0)
	{
		//make sure product exists
		$this->_product_is_valid($productID);

		//grab product info for view
		$data['product'] = $this->product_model->append('store')->append('image')->get($productID);

		//tell it what view file to use
		$data['view'] = $this->_get_custom_view();
		$data['title'] = 'Edit ' . $data['product']['title'];

		//load form validation library
		$this->load->library('form_validation');
		$validationRules = $this->product_model->validate;
		unset($validationRules['ecomm_store_id']);
		$this->form_validation->set_rules($validationRules);

		//if the form was submitted, update database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the view and show form errors
				$this->load->view($this->defaultLayout, $data);
			} else {

				//double check that the url does not already exist
				//make sure url not already in use for this store
				$urlExists = $this->product_model->get_by(array(
					'ecomm_store_id' => $data['product']['ecomm_store_id'],
					'url_slug' => $this->input->post('url_slug'),
					'ecomm_product_id !=' => $productID
				));
				if (!empty($exists) || isset($exists['ecomm_product_id']))
				{
					$this->session->set_flashdata('error', 'A product with this URL is already in use for this store.');
					redirect('/admin/ecommerce/products/edit/' . $productID);
				}

				$updateData = $this->input->post(NULL, TRUE);

				//upload image if applicable
				if (isset($_FILES) && $_FILES['userfile']['name'] != '')
				{
					//try to upload the file
					$this->load->library('uploader');
					$this->uploader->set_model('ecommerce/product_image_model');
					$this->uploader->set_upload_paths(array(
						'imgUploadFolder' => '/assets/uploads/ecommerce/' . $data['product']['store']['ecomm_store_id'] . '/',
						'imgThumbUploadFolder' => '/assets/uploads/ecommerce/' . $data['product']['store']['ecomm_store_id'] . '/thumbnails/'
					));
					$this->uploader->set_thumbnail_config(array(
						'width' => '400',
						'height' => '400',
						'maintain_ratio' => TRUE
					));

					$imageID = $this->uploader->upload_image(array('ecomm_product_id' => $productID));
					if (!is_int($imageID) || (int) $imageID <= 0)
					{
						$this->session->set_flashdata('error', $imageID);
						redirect('/admin/ecommerce/products/edit/' . $productID);
					} else {
						//unset previous default image for product
						$this->load->model('ecommerce/product_image_model');
						$this->product_image_model->skip_validation()->update_by('ecomm_product_id', $productID, array('defaultImg' => 0));
						$this->product_image_model->skip_validation()->update($imageID, array('defaultImg' => 1));
					}
				}

				// update record to db & redirect
				$this->product_model->skip_validation()->update($productID, $updateData);

				//display success message & redirect
				$this->session->set_flashdata('success', 'Product was successfully updated.');
				redirect('/admin/ecommerce/products/' . $data['product']['ecomm_store_id']);
			}
		} else {
			// tell it what layout to use and load the view
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	* Changes product status to deleted
	*/
	function delete($productID = 0)
	{
		//make sure product exists
		$this->_product_is_valid($productID);

		//get product info
		$product = $this->product_model->get($productID);

		//determine if any orders were placed
		$this->load->model('ecommerce/order_model');
		$ordersPlaced = $this->order_model->countByProduct($product['ecomm_product_id']);

		if ($ordersPlaced > 0)
		{
			//show error message
			$this->session->set_flashdata('error', 'Orders have already been placed for this product. You must cancel all orders where this product was purchased first.');
			redirect('/admin/ecommerce/products/' . $product['ecomm_store_id']);
		} else {
			//set status to deleted
			$this->product_model->soft_delete($productID);
		}

		//redirect upon success
		$this->session->set_flashdata('success', 'Product was deleted.');
		redirect('/admin/ecommerce/products/' . $product['ecomm_store_id']);
	}

	/**
	* View for re-ordering products
	*/
	function reorder($storeID = 0)
	{
		//make sure store is valid
		modules::run('ecommerce/admin/_store_is_valid', $storeID);

		//get store info
		$data['store'] = $this->store_model->get($storeID);

		//tell it what view file to use
		$data['view'] = $this->_get_custom_view();
		$data['title'] = 'Re-arrange Products for ' . $data['store']['title'];

		//get a list of items
		$data['products'] = $this->product_model->append('image')->order_by('display_order', 'asc')->get_many_by(array(
			'ecomm_store_id' => $storeID,
			'status' => 'enabled'
		));

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

	private function _get_custom_view()
	{
		return 'admin/products/' . $this->get_view();
	}

	/**
	* Checks to see that product ID is valid and that the product exists
	*/
	public function _product_is_valid($productID = 0)
	{
		//make sure product id is an integer
		$productID = (int) $productID;

		//make sure product id is greater than zero
		if ($productID > 0)
		{
			//check to see if the product exists
			$exists = $this->product_model->get($productID);

			if ($exists == false)
			{
				$this->session->set_flashdata('error', 'Invalid product id.');
				redirect('/admin/ecommerce');
			}

		} else {
			$this->session->set_flashdata('error', 'Invalid product id.');
			redirect('/admin/ecommerce');
		}
	}
}