<?php
class Admin_json extends Admin_controller {

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//load models that are commonly used
		$this->load->model('ecommerce/product_model');
	}

	/**
	 * Reorder products based on the drag/drop by the admin user
	 * @param  integer $storeID
	 * @return json [status messages]
	 */
	function reorder_products($storeID = 0)
	{
		try
		{
			//make sure store is valid
			if ((int) $storeID <= 0)
			{
				throw new Exception('Invalid store id.');
			} else {
				$this->load->model('ecommerce/store_model');
				$store = $this->store_model->select('ecomm_store_id')->get($storeID);
				if (empty($store) || $store == false)
				{
					throw new Exception('Invalid store id.');
				}
			}

			//make sure we variable we need from post
			if ($this->input->post('products', TRUE) == '')
			{
				throw new Exception('Must reorder products.');
			}

			//try to reorder the products
			$this->product_model->reorder($this->input->post('products', TRUE));

			//set status message
			$data['status'] = 'ok';
			$data['message'] = 'Successfully saved product order.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}

	/**
	 * Retrieve product sizes based on product id
	 * @param  integer $productID
	 * @return json [status messages and data]
	 */
	function sizes($productID = 0)
	{
		try
		{
			//make sure productID is valid
			if ((int) $productID <= 0)
			{
				throw new Exception('Invalid product id.');
			} else {
				$product = $this->product_model->select('ecomm_product_id')->get($productID);
				if (empty($product) || $product == false)
				{
					throw new Exception('Invalid product id.');
				}
			}

			//try to reorder the sizes
			$this->load->model('ecommerce/product_size_model');
			$data['sizes'] = $this->product_size_model->order_by('display_order', 'asc')->get_many_by('ecomm_product_id', $productID);

			//set status message
			$data['status'] = 'ok';
			$data['message'] = 'Successfully retreived product sizes.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}

	/**
	 * Reorder product sizes based on the drag/drop by the admin user
	 * @param  integer $productID
	 * @return json [status messages]
	 */
	function reorder_sizes($productID = 0)
	{
		try
		{
			//make sure productID is valid
			if ((int) $productID <= 0)
			{
				throw new Exception('Invalid product id.');
			} else {
				$product = $this->product_model->select('ecomm_product_id')->get($productID);
				if (empty($product) || $product == false)
				{
					throw new Exception('Invalid product id.');
				}
			}

			//make sure we variable we need from post
			if ($this->input->post('sizes', TRUE) == '')
			{
				throw new Exception('Must reorder product sizes.');
			}

			//try to reorder the sizes
			$this->load->model('ecommerce/product_size_model');
			$this->product_size_model->reorder($this->input->post('sizes', TRUE));

			//set status message
			$data['status'] = 'ok';
			$data['message'] = 'Successfully saved product size order.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}

	function add_sizes()
	{
		try
		{
			//make sure we are posting
			if (!$this->is_post())
			{
				throw new Exception('Must post data.');
			}

			//make sure a valid quantity was passed
			$qty = (int) $this->input->post('qty', TRUE);
			if ($qty <= 0)
			{
				throw new Exception('Invalid product size quantity.');
			}

			//set product id to int
			$productID = (int) $this->input->post('ecomm_product_id', TRUE);

			//make sure product is found
			$product = $this->product_model->select('ecomm_product_id')->get($productID);
			if (empty($product) || $product == false)
			{
				throw new Exception('Product not found.');
			}

			//loop through sizes and save
			$this->load->model('ecommerce/product_size_model');
			for ($i = 1; $i <= $qty; $i++)
			{
				$this->product_size_model->insert(array(
					'size' => $this->input->post('size' . $i, TRUE),
					'price' => $this->input->post('price' . $i, TRUE),
					'ecomm_product_id' => $productID
				));
			}

			//send back success message
			$data['status'] = 'ok';
			$data['message'] = 'Successfully added product sizes.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}

	function stores()
	{
		try
		{
			//make sure we variable we need from post
			if ($this->input->get('query', TRUE) == '')
			{
				throw new Exception('Must enter a search term.');
			}

			if (strlen($this->input->get('query', TRUE)) < 3)
			{
				throw new Exception('Search term not long enough.');
			}

			//try to find matching stores
			$this->load->model('ecommerce/store_model');
			$stores = $this->store_model->autosuggest($this->input->get('query', TRUE));

			//set response
			$data['results'] = $stores;
			$data['status'] = 'ok';
			$data['message'] = 'Successfully retrieved stores';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/


}