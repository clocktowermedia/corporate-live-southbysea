<?php
class Ecommerce extends MY_Controller {

	//layout variables
	public $defaultLayout;

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//set layout variable(s)
		$this->defaultLayout = $this->get_default_module_layout();

		//load relevant models
		$this->load->model('ecommerce/store_model');
		$this->load->model('ecommerce/product_model');
	}

	/**
	 * Preorder landing page, view
	 * @param  string $storeUrl
	 */
	function index($storeUrl = '')
	{
		//make sure site URL is valid, else redirect
		$storeUrl = $this->_store_is_valid($storeUrl);

		//get store info
		$data['store'] = $this->store_model->get_by('url_slug', $storeUrl);
		$data['store']['products'] = $this->product_model->append('image')->order_by('display_order', 'asc')->get_many_by(array(
			'ecomm_store_id' => $data['store']['ecomm_store_id'],
			'status' => 'enabled'
		));

		//tell it what view to use, this is always called the view variable
		$data['title'] = 'SeaPort Stores';
		$data['view'] = $this->get_view();
		$data['passwordEntered'] = $this->_store_password_entered($storeUrl, FALSE);

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}

	function login($storeUrl = '')
	{
		//make sure site URL is valid, else redirect
		$storeUrl = $this->_store_is_valid($storeUrl);

		if (!$this->is_post() || !$this->input->post('password'))
		{
			redirect('/preorder/' . $storeUrl);
		}

		$store = $this->store_model->get_by('url_slug', $storeUrl);
		$authenticated = $this->store_model->authenticate($store['ecomm_store_id'], $this->input->post('password'));
		if ($authenticated === TRUE)
		{
			$this->session->set_userdata($store['ecomm_store_id'] . '-authorized', TRUE);
		} else {
			$this->session->set_flashdata('error', 'Invalid login.');
		}

		redirect('/preorder/' . $storeUrl);
	}

	/**
	 * Product landing page, view
	 * @param  string $storeUrl
	 * @param  string $productUrl
	 */
	function product($storeUrl = '', $productUrl = '')
	{
		//make sure all Urls are valid
		$storeUrl = $this->_store_is_valid($storeUrl);
		$this->_store_is_active($storeUrl);
		$this->_store_password_entered($storeUrl, TRUE);

		$productUrl = $this->_product_is_valid($productUrl, $storeUrl);

		//get product info
		$data['store'] =  $this->store_model->get_by('url_slug', $storeUrl);

		$this->load->model('ecommerce/order_item_model');
		$data['store']['num_ordered'] = $this->order_item_model->getTotalOrdered($data['store']['ecomm_store_id']);
		$data['store']['num_to_sell'] = ((int) $data['store']['num_to_sell'] <= 0)? $this->product_model->getMinimum($data['store']['ecomm_store_id']) : $data['store']['num_to_sell'];

		$data['product'] = $this->product_model->append('image')->get_by(array(
			'url_slug' => $productUrl,
			'ecomm_store_id' => $data['store']['ecomm_store_id']
		));

		//get product sizes
		$this->load->model('ecommerce/product_size_model');
		$data['size_options'] = $this->product_size_model->order_by('display_order', 'asc')->get_many_by('ecomm_product_id', $data['product']['ecomm_product_id']);
		$data['prices'] = $this->product_size_model->where('ecomm_product_id', $data['product']['ecomm_product_id'])->dropdown('price');

		//get number of times product was ordered
		$this->load->model('ecommerce/order_item_model');
		$data['product']['num_ordered'] = $this->order_item_model->getProductNumberOrdered($data['product']['ecomm_product_id']);

		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->get_view();
		$data['title'] = $data['product']['title'];

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}

	/**
	 * View for cart page
	 * @param  string $storeUrl
	 */
	function cart($storeUrl = '')
	{
		//make sure site URL is valid, else redirect
		$storeUrl = $this->_store_is_valid($storeUrl);
		$this->_store_is_active($storeUrl);
		$this->_store_password_entered($storeUrl, TRUE);

		//get store info
		$data['store'] = $this->store_model->get_by('url_slug', $storeUrl);

		//cart display
		$orderID = (int) $this->session->userdata('orderID');
		if ((int) $orderID > 0)
		{
			$this->load->model('ecommerce/order_model');
			$data['order'] = $this->order_model->getCart($orderID);
		} else {
			$data['order'] = array();
		}

		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->get_view();
		$data['title'] = 'Cart';

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}

	/**
	 * View and action for checkout page
	 * @param  string $storeUrl
	 */
	function checkout($storeUrl = '')
	{
		//make sure site URL is valid, else redirect
		$storeUrl = $this->_store_is_valid($storeUrl);
		$this->_store_is_active($storeUrl);
		$this->_store_password_entered($storeUrl, TRUE);

		//get store info
		$data['store'] = $this->store_model->get_by('url_slug', $storeUrl);

		//get order info to display totals and such
		$orderID = (int) $this->session->userdata('orderID');
		if ((int) $orderID > 0)
		{
			$this->load->model('ecommerce/order_model');
			$data['order'] = $this->order_model->getCart($orderID);
			$data['order']['details'] = $this->order_model->get($orderID);
		} else {
			redirect('/preorder/' . $storeUrl . '/cart');
		}

		//if no items, redirect
		if ($data['order']['total'] <= 0)
		{
			redirect('/preorder/' . $storeUrl . '/cart');
		}

		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->get_view();
		$data['title'] = 'Checkout';

		//load form validation library and set rules
		$this->load->library('form_validation');
		$this->form_validation->set_rules('cust_first_name', 'First Name', 'required');
		$this->form_validation->set_rules('cust_last_name', 'Last Name', 'required');
		$this->form_validation->set_rules('cust_phone', 'Phone', 'required|min_length[10]');
		$this->form_validation->set_rules('cust_email', 'Email', 'required|valid_email');

		//get items we need for paypal
		$this->load->config('api_keys');
		$paypalConfig = $this->config->item('paypal');
		$data['paypal_enabled'] = $paypalConfig['enabled'];

		//if the form was submitted, add to database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the page and show form errors
				$data['submission'] = $this->input->post(NULL, TRUE);
				$this->load->view($this->defaultLayout, $data);
			} else {
				//format post
				$_POST = $this->_format_post();

				//get api values
				$this->load->config('api_keys');

				//proceed to whatever payment method they selected
				if ($this->input->post('payment_method', TRUE) == 'credit')
				{
					//try to save other info
					$token = $this->input->post('stripeToken', TRUE);
					unset($_POST['stripeToken']);
					$this->order_model->skip_validation()->update($orderID, $this->input->post(NULL, TRUE));

					//try to submit payment to stripe
					$stripe_cust_id = modules::run('ecommerce/ecommerce_gateways/_stripe_payment', $token, $data);

					//save customer id
					$this->order_model->skip_validation()->update($orderID, array('stripe_cust_id' => $stripe_cust_id, 'status' => 'authorized'));

					//send order email & redirect to thank you page
					$sent = modules::run('ecommerce/ecommerce_emails/_send_order_confirmation_email', $data);
					if (!$sent)
					{
						modules::run('ecommerce/ecommerce_emails/_send_order_confirmation_email_pdf', $data);
					}
					redirect('/preorder/' . $storeUrl . '/thank-you');

				} else {

					//try to save order info
					$this->order_model->skip_validation()->update($orderID, $this->input->post(NULL, TRUE));

					//submit to paypal for confirmation from user
					modules::run('ecommerce/ecommerce_gateways/_paypal_init_auth', $data);
				}
			}
		} else {

			// tell it what layout to use and load
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	 * Action page for handling a paypal request once it has been confirmed or cancelled
	 * @param  string $storeUrl
	 */
	function paypal($storeUrl = '')
	{
		//make sure site URL is valid, else redirect
		$storeUrl = $this->_store_is_valid($storeUrl);
		$this->_store_is_active($storeUrl);
		$this->_store_password_entered($storeUrl, TRUE);

		//get store info
		$data['store'] = $this->store_model->get_by('url_slug', $storeUrl);

		try {

			if (!$this->is_get())
			{
				throw new Exception('Invalid request.');
			}

			if (is_null($this->input->get('success', TRUE)))
			{
				throw new Exception('Invalid request.');
			}

			if ($this->input->get('success', TRUE) == 'false')
			{
				throw new Exception('There was an error authorizing funds from your paypal account.');
			}

			//get order id
			$orderID = (int) $this->session->userdata('orderID');
			if ($orderID <= 0)
			{
				throw new Exception('Invalid order id.');
			}

			//confirm payment authorization
			$paypalData = modules::run('ecommerce/ecommerce_gateways/_paypal_confirm_auth', $data);

			//save order details
			$this->load->model('ecommerce/order_model');
			$updateData = array_merge($paypalData, array('status' => 'authorized'));
			$this->order_model->skip_validation()->update($orderID, $updateData);

			//get order details
			$data['order'] = $this->order_model->getCart($orderID);
			$data['order']['details'] = $this->order_model->get($orderID);

			//send order email & redirect to thank you page
			$sent = modules::run('ecommerce/ecommerce_emails/_send_order_confirmation_email', $data);
			if (!$sent)
			{
				modules::run('ecommerce/ecommerce_emails/_send_order_confirmation_email_pdf', $data);
			}
			redirect('/preorder/' . $storeUrl . '/thank-you');

		} catch (Exception $e) {
			//redirect and show error message
			$this->session->set_flashdata('error', $e->getMessage());
			redirect('/preorder/' . $storeUrl . '/checkout');
		}
	}

	/**
	 * View for thank you/order confirmation page
	 * @param  string $storeUrl
	 */
	function thank_you($storeUrl = '')
	{
		//make sure site URL is valid, else redirect
		$storeUrl = $this->_store_is_valid($storeUrl);
		$this->_store_is_active($storeUrl);
		$this->_store_password_entered($storeUrl, TRUE);

		//get store info
		$data['store'] = $this->store_model->get_by('url_slug', $storeUrl);

		//get order info
		$orderID = (int) $this->session->userdata('orderID');
		if ((int) $orderID > 0)
		{
			$this->load->model('ecommerce/order_model');
			$data['order'] = $this->order_model->getCart($orderID);
			$data['order']['details'] = $this->order_model->get($orderID);
		} else {
			redirect('/preorder/' . $storeUrl . '/cart');
		}

		//empty cart
		$this->session->sess_destroy();

		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->get_view();
		$data['title'] = 'Thank you for your Order';

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}

	function generate_pdf($confirmationNumber = '', $output = 'download')
	{
		//get order
		$this->load->model('ecommerce/order_model');
		$order = $this->order_model->append('store')->get_by('confirmationNumber', $confirmationNumber);
		$data['order'] = $this->order_model->getCart($order['ecomm_order_id']);
		$data['order']['details'] = $order;
		$data['store'] = $data['order']['details']['store'];

		//build order email content
		$content =  '<h1 class="center-align">' . $this->config->item('site_name') . ' ' . $data['store']['title'] . ' Preorder Store Order Confirmation</h1><br/>';
		$content .= '<p>Thank you for your order. You may find a summary of your order below. Your order confirmation number is <strong>#' . $data['order']['details']['confirmationNumber'] . '</strong>.</p><br/>';
		$content .= '<p>Please note that your order will not be shipped to you individually and will not go into production until store closes on ' . date('n/d/Y', strtotime($data['store']['end_date'])) . '.</p><br/>';
		$content .= '<table class="inner-table" cellpadding="10" cellspacing="10">' .
		'<tr>' .
			'<th></th>' .
			'<th>Item</th>' .
			'<th>Size</th>' .
			'<th>Quantity</th>' .
			'<th>Unit Price</th>' .
			'<th>Subtotal</th>' .
		'</tr>';
		foreach ($data['order']['cart'] AS $item)
		{
			if ($item['product']['image']['thumbFullpath'] != '')
			{
				$image = '<img class="product-img" src="https://' . $_SERVER['HTTP_HOST'] . $item['product']['image']['thumbFullpath'] .'" title="' . $item['product']['title'] .'">';
			} else {
				$image = '<img class="product-img" src="http://dummyimage.com/100x100/fff/57656e&text=No+Image" title="' . $item['product']['title'] .'">';
			}

			$content .= '<tr>' .
				'<td>' . $image . '</td>' .
				'<td>' . $item['product']['title'] . '</td>' .
				'<td>' . $item['size']['size'] . '</td>' .
				'<td>' . $item['qty'] . '</td>' .
				'<td>$' . $item['size']['price'] . '</td>' .
				'<td>$' . number_format($item['qty'] * $item['size']['price'], 2) . '</td>' .
			'</tr>';
		}
		$content .= '<tr><td colspan="6">&nbsp;</td></tr>';
		$content .= '<tr>
			<td colspan="4"></td>
			<td>Subtotal:</td>
			<td>$' . $data['order']['subtotal'] . '</td>
		</tr>
		<tr>
			<td colspan="4">&nbsp;</td>
			<td>Tax:</td>
			<td>$' . $data['order']['tax'] . '</td>
		</tr>
		<tr>
			<td colspan="4">&nbsp;</td>
			<td>Total:</td>
			<td>$' . $data['order']['total'] . '</td>
		</tr>
		</table>';
		$content .= ($data['order']['details']['comments'] != '')? '<br/><p><strong>Order Notes:</strong> ' . $data['order']['details']['comments'] . '</p>' : '';

		//style table
		$emailData['content'] = $this->convert_text_to_html_pdf_table($emailData['subject'], $content);
		$this->load->library('css_to_inline_styles');
		$this->css_to_inline_styles->setHtml($emailData['content']);
		$this->css_to_inline_styles->setCSS(file_get_contents(APPPATH . '../assets/css/email.css'));
		$emailData['html'] = $this->css_to_inline_styles->convert();

		//tell it what view to use & get content
		$emailData['view'] = 'emails/checkout';
		$this->load->library('parser');
		$content = $this->parser->parse($this->get_email_layout(), $emailData, TRUE);
		$fileName = 'South_by_Sea_Order-' . url_title($data['store']['title']) . '_' . $data['order']['details']['confirmationNumber'] . '.pdf';

		//output pdf to page
		require_once('application/libraries/mpdf/mpdf.php');
		$mpdf = new mPDF();
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->WriteHTML($content);

		//set the filename and output
		if ($output === 'download')
		{
			$mpdf->Output($fileName, 'D');
			exit;
		} else {
			$returnData['pdf'] = $mpdf->Output($fileName, 'S');
			$returnData['fileName'] = $fileName;
			return $returnData;
		}
	}

	/**
	 * View and action for contacting order coordinator
	 * @param  string $storeUrl
	 */
	function contact($storeUrl = '')
	{
		//make sure site URL is valid, else redirect
		$storeUrl = $this->_store_is_valid($storeUrl);
		$this->_store_is_active($storeUrl);
		$this->_store_password_entered($storeUrl, TRUE);

		//get store info
		$data['store'] = $this->store_model->get_by('url_slug', $storeUrl);

		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->get_view();
		$data['title'] = 'Contact Order Coordinator';

		//set form validation rules
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('subject', 'Subject', 'required');
		$this->form_validation->set_rules('comments', 'Comments', 'required');

		//if the form was submitted, add to database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the page and show form errors
				$data['submission'] = $this->input->post(NULL, TRUE);
				$this->load->view($this->defaultLayout, $data);
			} else {
				//format post
				$_POST = $this->_format_post();

				//send email to order coordinator
				modules::run('ecommerce/ecommerce_emails/_send_contact_email', $data);

				//redirect and show direct message
				$this->session->set_flashdata('success', 'Your message was sent successfully.');
				redirect('/preorder/' . $data['store']['url_slug'] . '/contact');
			}
		} else {
			// tell it what layout to use and load
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	 * View (modal) and action for subscribing to store
	 * @param  string $storeUrl
	 */
	function subscribe($storeUrl = '')
	{
		//make sure site URL is valid, else redirect
		$storeUrl = $this->_store_is_valid($storeUrl);
		$this->_store_is_active($storeUrl);
		$this->_store_password_entered($storeUrl, TRUE);

		//get store info
		$data['store'] = $this->store_model->get_by('url_slug', $storeUrl);

		//tell it what view to use, this is always called the view variable
		$data['title'] = 'Subscribe to Store';

		//if the form was submitted, add to database
		if ($this->is_post())
		{
			$this->_handle_subscribe();
		} else {
			// tell it what layout to use and load
			$this->load->view($this->get_view(), $data);
		}
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

	/**
	* Checks to see that store is valid and that the store exists
	*/
	public function _store_is_valid($storeUrl = '')
	{
		$storeUrl = ($storeUrl == '')? $this->storeUrl : $storeUrl;

		//make sure store url is valid
		if (!is_null($storeUrl) && $storeUrl != '')
		{
			//check to see if the store exists
			$exists = $this->store_model->select('ecomm_store_id')->get_by(array(
				'url_slug' => $storeUrl,
				'start_date <=' => date('Y-m-d H:i:s'),
				'status' => 'enabled'
			));
			$exists2 = $this->store_model->select('ecomm_store_id')->get_by(array(
				'url_slug' => str_replace('_', '-', $storeUrl),
				'start_date <=' => date('Y-m-d H:i:s'),
				'status' => 'enabled'
			));

			//redirect if not found
			if (($exists == false || empty($exists)) && ($exists2 == false || empty($exists2)))
			{
				#$this->session->set_flashdata('error', 'This store is currently inactive or the store link is invalid.');
				redirect('/');
			}

			//send back proper version of url and save in session just in case
			$storeUrl = (!empty($exists2))? str_replace('_', '-', $storeUrl) : $storeUrl;
			$storeID = (!empty($exists2))? $exists2['ecomm_store_id'] : $exists['ecomm_store_id'];
			$this->session->set_userdata('storeUrl', $storeUrl);
			$this->session->set_userdata('storeID', $storeID);

			$orderID = (int) $this->session->userdata('orderID');
			if ($orderID > 0)
			{
				//make sure order id matches store, else unset
				$this->load->model('ecommerce/order_model');
				$order = $this->order_model->get($orderID);
				if ($order['ecomm_store_id'] != $storeID)
				{
					$this->session->unset_userdata('orderID');
				}
			}

			//check tax rates
			$this->_check_tax_rate($storeID);

			//return valid url
			return $storeUrl;

		} else {
			#$this->session->set_flashdata('error', 'This store is currently inactive or the store link is invalid.');
			redirect('/');
		}
	}

	public function _store_is_active($storeUrl = '')
	{
		$storeUrl = $this->_store_is_valid($storeUrl);
		$store = $this->store_model->get_by('url_slug', $storeUrl);
		if (date('Y-m-d H:i:s') > $store['end_date'])
		{
			redirect('/preorder/' . $storeUrl);
		}
	}

	public function _store_has_password($storeUrl = '')
	{
		$store = $this->store_model->get_by('url_slug', $storeUrl);
		if ($store['passwordEnabled'] == TRUE && $store['storeP'] != '')
		{
			return true;
		}

		return false;
	}

	public function _store_password_entered($storeUrl = '', $redirect = false)
	{
		$storeUrl = $this->_store_is_valid($storeUrl);
		$store = $this->store_model->get_by('url_slug', $storeUrl);
		if ($store['passwordEnabled'] == TRUE && $store['storeP'] != '' && !$this->session->userdata($store['ecomm_store_id'] . '-authorized'))
		{
			if ($redirect == TRUE)
			{
				redirect('/preorder/' . $storeUrl);
			} else {
				return false;
			}
		}

		return true;
	}

	/**
	 * Checks to see that the product is valid and that the product exists
	 * @param  string $productUrl
	 */
	public function _product_is_valid($productUrl = '', $storeUrl = '')
	{
		$storeUrl = ($storeUrl != '')? $storeUrl : (string) $this->session->userdata('storeUrl');
		$store = $this->store_model->get_by('url_slug', $storeUrl);

		//make sure store url is valid
		if (!is_null($productUrl) && $productUrl != '')
		{
			//check to see if the store exists
			$exists = $this->product_model->select('title')->get_by(array(
				'ecomm_store_id' => $store['ecomm_store_id'],
				'url_slug' => $productUrl,
				'status' => 'enabled'
			));
			$exists2 = $this->product_model->select('title')->get_by(array(
				'ecomm_store_id' => $store['ecomm_store_id'],
				'url_slug' => str_replace('_', '-', $productUrl),
				'status' => 'enabled'
			));

			//redirect if not found
			if (($exists == false || empty($exists)) && ($exists2 == false || empty($exists2)))
			{
				$this->session->set_flashdata('error', 'Product not found.');
				if ($storeUrl != '')
				{
					redirect('/preorder/' . $storeUrl);
				} else {
					redirect('/');
				}
			}

			//send back proper version of url
			$productUrl = (!empty($exists2))? str_replace('_', '-', $productUrl) : $productUrl;
			return $productUrl;

		} else {
			$this->session->set_flashdata('error', 'Product not found.');

			if ($storeUrl != '')
			{
				redirect('/preorder/' . $storeUrl);
			} else {
				redirect('/');
			}
		}
	}

	public function _handle_subscribe()
	{
		try {

			//make sure we are posting
			if (!$this->is_post())
			{
				throw new Exception('Must post data.');
			}

			//load the model we need
			$this->load->model('ecommerce/subscriber_model');

			//set form validation rules
			$this->load->library('form_validation');
			$validationRules = $this->subscriber_model->validate;
			unset($validationRules['ecomm_store_id']);
			$validationRules['email']['rules'] .= '|is_unique[' . $this->subscriber_model->_table . '.email]';
			$this->form_validation->set_rules($validationRules);

			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				throw new Exception(strip_tags(validation_errors()));
			}

			//try to insert into subscriber model
			$subscriberID = $this->subscriber_model->insert($this->input->post(NULL, TRUE));

			//make sure insert was successful
			if ((int) $subscriberID <= 0)
			{
				throw new Exception('An error occurred. Please try again.');
			}

			//send success message
			$data['status'] = 'ok';
			$data['message'] = 'Successfully registered to receive store updates.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}

	public function _check_tax_rate($storeID = 0)
	{
		if ($storeID > 0)
		{
			//get store info
			$store = $this->store_model->get($storeID);

			//temporarily disable this until client is ready to use avalara, config setting
			$this->config->load('api_keys');
			$avalara = $this->config->item('avalara');
			if ($avalara['calc_tax'] == true)
			{
				//if tax rate hasn't been updated within the past 24 hours, try to grab a new one
				if (date('Y-m-d', strtotime($store['tax_rate_updated'])) <= date('Y-m-d'))
				{
					//try to calculate tax rates again
					$this->load->library('avalara_tax_rates');
					$taxRate = (int) $this->avalara_tax_rates->calc_tax(array(
						'address1' => $store['coord_address1'],
						'city' => $store['coord_city'],
						'state' => $store['coord_state'],
						'zip' => $store['coord_zip']
					));

					//update tax rate if avalara behaved
					if (!is_null($taxRate) && $taxRate > 0)
					{
						$data['tax_rate'] = number_format($taxRate, 2, '.', '');
						$data['tax_rate_updated'] = date('Y-m-d H:i:s');
						$this->store_model->skip_validation()->update($storeID, $data);
						return $data['tax_rate'];
					}
				}
			}

			return number_format($store['tax_rate'], 2, '.', '');
		}
	}

	function _format_post()
	{
		//format textareas
		if ($this->input->post('textarea_list', TRUE) != '')
		{
			$textareas = explode(',', $this->input->post('textarea_list', TRUE));
			foreach ($textareas as $textarea)
			{
				$_POST[$textarea] = str_replace("\n",'<br/>', $this->input->post($textarea, TRUE));
			}
		}
		unset($_POST['textarea_list']);
		return $this->input->post(NULL, TRUE);
	}
}