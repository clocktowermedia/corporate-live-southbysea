<?php
	//create array for classes
	$containerArray = array('labelClass' => 'col-sm-1', 'containerClass' => 'col-sm-11');

	//title
	$this->form_builder->text('title', 'Title:', (isset($product['title']))? $product['title'] : '', $containerArray, '', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));

	//url field
	$this->form_builder->text('url_slug', 'URL:', (isset($product['url_slug']))? $product['url_slug'] : '', $containerArray, '', 'This will become /preorder/store-url/product-url', '', array('required' => 'required', 'data-validation-minlength' => '2'));

	//description field
	$this->form_builder->textarea('desc', 'Description', (isset($product['desc']))? $product['desc'] : '', $containerArray + array('inputClass' => 'wysiwyg short'), 10, '', '', '', array('id' => 'product-desc'));

	//base price field
	$this->form_builder->text('price', 'Base Price:', (isset($product['price']))? $product['price'] : '', $containerArray + array('inputClass' => 'decimal'), '', 'This is the price that will be used by default for any size, if one is not set specifically for that size', '', array('required' => 'required'));

	//minimum required to sell
	$this->form_builder->number('num_to_sell', 'Minimum # for Production:', (isset($product['num_to_sell']))? $product['num_to_sell'] : 0, $containerArray, '', 'This is the number of items required to sell to meet production requirements. Leave as 0 if no such requirement exists.', '', array('required' => 'required'));
?>

	<?php if (isset($product['image']['thumbFullpath']) && $product['image']['thumbFullpath'] != ''): ?>
		<div class="form-group">
			<label class="control-label <?php echo $containerArray['labelClass'];?>" for="userfile">Current Image:</label>
			<div class="<?php echo $containerArray['containerClass'];?>">
				<a target="_blank" href="<?php echo $product['image']['fullpath']; ?>"><img src="<?php echo $product['image']['thumbFullpath']; ?>" class="img-thumbnail"></a>
			</div>
		</div>
	<?php endif; ?>

	<div class="form-group" id="product-image-upload">
		<label class="control-label <?php echo $containerArray['labelClass'];?>" for="userfile">Image:</label>
		<div class="<?php echo $containerArray['containerClass'];?>">
			<input type="file" name="userfile" id="userfile" size="20" class="form-control"/>
			<?php if (isset($product['image']['thumbFullpath']) && $product['image']['thumbFullpath'] != ''): ?>
				<p class="help-block">If you upload a new image, the image above will be replaced.</p>
			<?php endif; ?>
		</div>
	</div>