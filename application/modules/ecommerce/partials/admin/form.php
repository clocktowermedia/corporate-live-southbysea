
<span><legend>General Settings</legend></span>
<?php
	//name field
	$this->form_builder->text('title', 'Store Name:', (isset($store['title']))? $store['title'] : '', '', '', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));

	//url field
	$this->form_builder->text('url_slug', 'Store URL:', (isset($store['url_slug']))? $store['url_slug'] : '', '', '', 'This will become /preorder/store-url', '', array('required' => 'required', 'data-validation-minlength' => '2'));
?>
<div class="row">
	<div class="col-xs-6">
		<?php
			//date start
			$this->form_builder->text('start_date', 'Store Open Date:', (isset($store['start_date']))? date('Y-m-d', strtotime($store['start_date'])) : '', array('inputClass' => 'datepicker start'), '', '', '', array('required' => 'required'));
		?>
	</div>
	<div class="col-xs-6">
		<?php
			//time start
			$this->form_builder->text('start_time', 'Store Open Time:', (isset($store['start_date']))? date('h:i A', strtotime($store['start_date'])) : '00:00:00', array('inputClass' => 'timepicker'), '', '', '', array('required' => 'required'));
		?>
	</div>
</div>
<div class="clearfix"></div>

<div class="row">
	<div class="col-xs-6">
		<?php
			//date end
			$this->form_builder->text('end_date', 'Store Close Date:', (isset($store['end_date']))? date('Y-m-d', strtotime($store['end_date'])) : '', array('inputClass' => 'datepicker end'), '', '', '', array('required' => 'required'));
		?>
	</div>
	<div class="col-xs-6">
		<?php
			//time end
			$this->form_builder->text('end_time', 'Store Close Time:', (isset($store['end_date']))? date('h:i A', strtotime($store['end_date'])) : '23:59:59', array('inputClass' => 'timepicker'), '', '', '', array('required' => 'required'));
		?>
	</div>
</div>
<div class="clearfix"></div>

<div class="row">
	<div class="col-xs-6">
		<?php
			//tax rate field
			$this->form_builder->text('tax_rate', 'Tax Rate:', (isset($store['tax_rate']))? $store['tax_rate'] : '0.00', array('inputClass' => 'decimal'), '', '', '', array('required' => 'required', 'max' => '99.99', 'data-validation-error-message' => 'This value is required and cannot be greater than 100.'));

		?>
	</div>
	<div class="col-xs-6">
		<?php
			//time end
			$this->form_builder->text('num_to_sell', 'Order Minimum:', (isset($store['num_to_sell']))? $store['num_to_sell'] : 0, '', '', 'This is the number of items that need to be sold to meet production requirements. Leave as 0 if no such requirement exists.', '', array());
		?>
	</div>
</div>
<div class="clearfix"></div>

<?php
	//account manager field
	$this->form_builder->select('acct_mgr_id', 'Account Manager:', $managers, (isset($store['acct_mgr_id']))? $store['acct_mgr_id'] : '', '', '', '', array('required' => 'required'));

	//status field
	//$this->form_builder->select('status', 'Status:', array('' => 'Select an option') + $statuses, (isset($store['status']))? $store['status'] : '', '', '', '', array());
?>

<span><legend>Order Coordinator Information</legend></span>
<div class="row">
	<div class="col-xs-6">
		<?php
			//first name field
			$this->form_builder->text('coord_first_name', 'First Name:', (isset($store['coord_first_name']))? $store['coord_first_name'] : '', '', '', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));
		?>
	</div>
	<div class="col-xs-6">
		<?php
			//last name field
			$this->form_builder->text('coord_last_name', 'Last Name:', (isset($store['coord_last_name']))? $store['coord_last_name'] : '', '', '', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));
		?>
	</div>
</div>
<?php
	//email field
	$this->form_builder->email('coord_email', 'Email:', (isset($store['coord_email']))? $store['coord_email'] : '', '', '', '', '', array('required' => 'required'));
?>
<?php
	//address
	$this->form_builder->text('coord_address1', 'Address', (isset($store['coord_address1']))? $store['coord_address1'] : '', '', 'Address', '', '', array('required' =>'required', 'data-validation-minlength' => '2'));

	//address
	$this->form_builder->text('coord_address2', 'Address Line 2', (isset($store['coord_address2']))? $store['coord_address2'] : '', '', 'Address Line 2', '', '', array());
?>

<div class="row">
	<div class="col-sm-6">
		<?php
			//city
			$this->form_builder->text('coord_city', 'City',  (isset($store['coord_city']))? $store['coord_city'] : '', '', 'City', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));
		?>
	</div>
	<div class="col-sm-6">
		<div class="row">
			<div class="col-xs-6">
				<?php
					//state
					$this->load->helper('dropdown');
					$this->form_builder->select('coord_state', 'State', array('' => 'State') + usStates(), (isset($store['coord_state']))? $store['coord_state'] : '', '', '', '', array('required' => 'required', 'data-validation-minlength' => '2', 'data-validation-maxlength' => '2'));
				?>
			</div>
			<div class="col-xs-6">
				<?php
					//zip
					$this->form_builder->text('coord_zip', 'Zip Code', (isset($store['coord_zip']))? $store['coord_zip'] : '', '', 'Zip Code', '', '', array('required' => 'required', 'data-validation-minlength' => '5', 'data-validation-maxlength' => '10'));
				?>
			</div>
		</div>
	</div>
</div>

<span><legend>Password Protection</legend></span>
<div class="row">
	<div class="col-xs-3">
		<?php
			$this->form_builder->checkbox('passwordEnabled', 'Yes', 0, 1, (isset($store['passwordEnabled']))? $store['passwordEnabled'] : '', '', 'Enable Password?', '', array());
		?>
	</div>
	<div class="col-xs-9">
		<?php
			//last name field
		$this->form_builder->password('storeP', 'Password:', (isset($store['storeP']))? $store['storeP'] : '', '', '', '<a href="#" class="toggle-password">Show Password</a>', '', array('data-validation-minlength' => '4'));
		?>
	</div>
</div>