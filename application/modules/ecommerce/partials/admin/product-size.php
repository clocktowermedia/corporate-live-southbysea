<?php
	//create array for classes
	$containerArray = array('labelClass' => 'col-sm-1', 'containerClass' => 'col-sm-11');

	//size
	$this->form_builder->text('size', 'Size:', (isset($size['size']))? $size['size'] : '', $containerArray, '', '', '', array('required' => 'required'));

	//price field
	$this->form_builder->text('price', 'Price:', (isset($size['price']))? $size['price'] : $product['price'], $containerArray + array('inputClass' => 'decimal'), '', '', '', array('required' => 'required'));
?>