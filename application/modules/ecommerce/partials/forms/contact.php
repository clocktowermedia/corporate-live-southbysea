<?php
	//remove labels
	if ($this->uri->segment(1) != 'admin')
	{
		$this->form_builder->set_labels(false);
	}

	//create array for classes
	$containerArray = array('labelClass' => 'col-sm-1', 'containerClass' => 'col-sm-11');
	$this->form_builder->hidden('textarea_list', '', array('id' => 'textarea-list', 'data-storage' => 'false'));

	//Name Field
	$this->form_builder->text('name', 'Name', (isset($submission['name']))? $submission['name'] : '', '', 'Name', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));

	//email field
	$this->form_builder->email('email', 'Email', (isset($submission['email']))? $submission['email'] : '', '', 'Email', '', '', array('required' => 'required'));

	//Subject Field
	$this->form_builder->text('subject', 'Subject', (isset($submission['subject']))? $submission['subject'] : '', '', 'Subject', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));

	//Comments Field
	$this->form_builder->textarea('comments', 'Comments', (isset($submission['comments']))? $submission['comments'] : '', array('inputClass' => 'cust-textarea'), 10, 'Comments', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));
?>