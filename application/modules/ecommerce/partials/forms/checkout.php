<?php
	$this->form_builder->set_labels(false);
	$this->form_builder->hidden('textarea_list', '', array('id' => 'textarea-list', 'data-storage' => 'false'));
?>

<div class="row">
	<div class="col-xs-6">
		<?php
			//First name fields
			$this->form_builder->text('cust_first_name', 'First Name', (isset($submission['cust_first_name']))? $submission['cust_first_name'] : '', '', 'First Name', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));
		?>
	</div>
	<div class="col-xs-6">
		<?php
			//last name field
			$this->form_builder->text('cust_last_name', 'Last Name', (isset($submission['cust_last_name']))? $submission['cust_last_name'] : '', '', 'Last Name', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));
		?>
	</div>
</div>
<div class="clearfix"></div>

<?php
	//email field
	$this->form_builder->email('cust_email', 'Email', (isset($submission['cust_email']))? $submission['cust_email'] : '', '', 'Email', '', '', array('required' => 'required'));

	//phone field
	$this->form_builder->tel('cust_phone', 'Phone', (isset($submission['cust_phone']))? $submission['cust_phone'] : '', '', 'Phone', '', '', array('required' => 'required', 'data-validation-minlength' => '10'));

	//payment method field
	if ($paypal_enabled == true)
	{
		$this->form_builder->select('payment_method', 'Payment Method', array('' => 'Payment Method') + array('credit' => 'Credit Card', 'paypal' => 'Paypal'), (isset($submission['payment_method']))? $submission['payment_method'] : '', array('inputClass' => 'hider-shower'), '', '', array('id' => 'payment-method'));
	} else {
		$this->form_builder->hidden('payment_method', 'credit', '');
	}
?>
<div class="clearfix"></div>

<?php
	//order commments/notes
	$this->form_builder->textarea('comments', 'Notes', (isset($submission['comments']))? $submission['comments'] : '', '', 3, 'Order Notes', '', '', array());
?>
<div class="clearfix"></div>

<!-- Credit Card Form -->
<div class="panel panel-default text-left <?php echo ($paypal_enabled == true)? 'hide' : '' ?>" id="credit" data-parent="payment-method">
	<div class="panel-heading">
		<h3 class="panel-title"><img class="pull-right" src="http://i76.imgup.net/accepted_c22e0.png" alt="Accepted Payment Methods" title="South by Sea's Accepted Payment Methods">Payment Details</h3>
	</div>
	<div class="panel-body">
		<div id="payment-errors">
		</div>

		<div class="row">
			<div class="col-xs-7 col-md-7">
				<div class="form-group">
					<label for="cardNumber">Card Number</label>
					<div class="input-group">
						<input type="text" class="form-control credit-card" placeholder="Valid Card Number" required data-stripe="number" data-validation-minlength="15" maxlength="16" autocomplete="cc-number"/>
						<span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
					</div>
				</div>
			</div>
			<div class="col-xs-5 col-md-5 pull-right">
				<div class="form-group">
					<label for="cvCode">Billing Zipcode</label>
					<input type="text" class="form-control zip" placeholder="Zipcode" required data-stripe="address_zip" data-validation-minlength="5" maxlength="10"/>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-7 col-md-7">
				<div class="form-group">
					<label for="expMonth">Expiration Date</label>
					<div class="row">
						<div class="col-xs-6 col-lg-6">
							<input type="text" class="form-control months" placeholder="MM" required data-stripe="exp_month" data-validation-minlength="2" maxlength="2" max="12"/>
						</div>
						<div class="col-xs-6 col-lg-6">
							<input type="text" class="form-control years" placeholder="YY" required data-stripe="exp_year" data-validation-minlength="2" maxlength="2" min="<?php echo date('y');?>" />
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-5 col-md-5 pull-right">
				<div class="form-group">
					<label for="cvCode">CV Code</label>
					<input type="password" class="form-control cvc" placeholder="CV" required data-stripe="cvc" data-validation-minlength="3" autocomplete="cc-csc" maxlength="4"/>
				</div>
			</div>
		</div>
		<div class="row" style="display:none;">
			<div class="col-xs-12">
				<p class="payment-errors"></p>
			</div>
		</div>
	</div>
</div>

<div id="paypal" class="hide" data-parent="payment-method">
	<p class="mt-0 mb-10">You will be directed to the Paypal website to complete your purchase.</p>
</div>

<div class="form-group custom-checkbox">
	<input type="hidden" name="agreeToTerms" value="0">
	<input type="checkbox" id="agreeToTerms" name="agreeToTerms" value="1" required="required">
	<label for="agreeToTerms">
		<span class="text-uppercase">
			I agree to the terms and conditions outlined <a href="/faq" target="_blank">here</a> and <a href="/privacy-policy" target="_blank">here</a>
		</span>
	</label>
</div>

<?php
	$this->form_builder->button('submit', 'Submit', array('containerClass' => '' , 'inputClass' => 'btn btn-info btn-lg'), '', '');
?>