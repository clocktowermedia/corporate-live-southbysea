<?php $this->load->view('../../ecommerce/partials/meta-header'); ?>
	<div class="container">
		<header>
			<div class="page-header">
				<div class="row">
					<div class="col-lg-12 on-top">
						<div class="pull-left">
                            <a href="/preorder/<?php echo $store['url_slug'];?>/subscribe" data-toggle="ajax-modal">Subscribe to Store</a>
						</div>
						<div class="pull-right">
			            	<div class="dropdown">
								<a id="cartSummary" data-toggle="dropdown">
									<i class="fa fa-shopping-cart"></i> Cart <span class="caret"></span>
								</a>
								<ul class="pull-right dropdown-menu dropdown-cart" aria-labelledby="cartSummary">
								<div id="cart-summary-container">
								</div>
								<script id="cart-summary-markup" type="x-handlebars-template">
									{{#each cart}}
										<li>
											<span class="item">
												<span class="item-left">
													{{#if product.image.thumbFullpath}}
														<img src="{{product.image.thumbFullpath}}" title="{{product.title}}" alt="{{product.title}}">
													{{else}}
														<img src="http://dummyimage.com/50x50/eee/57656e&text=No+Image" title="{{product.title}}" alt="{{product.title}}">
													{{/if}}
													<span class="item-info">
														<span>{{product.title}} x {{qty}}</span>
														<span>${{size.price}} ea.</span>
													</span>
												</span>
											</span>
										</li>
									{{else}}
									<li>
										<span class="item text-center">
											<div class="alert alert-info mb-0">Your cart is empty.</div>
										</span>
									</li>
									{{/each}}
								</script>
								<li class="divider"></li>
								<li><a class="text-center" href="/preorder/<?php echo $store['url_slug'];?>/cart">View Cart</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-12 text-center mt-20">
						<a href="/" title="Start your own store at southbysea.com" target="_blank">
							<h1 id="logo"><?php echo $this->config->item('site_name');?> <small><?php echo $this->config->item('site_tagline');?></small></h1>
						</a>
						<h2 class="uppercase"><?php echo $store['title']; ?> PREORDER</h2>
						<div class="countdown_timer" id="store_closing">
							<span id="clock" data-start="<?php echo $store['end_date']; ?>"></span>
							<span id="days-remaining">store closes on <strong><?php echo date('n/d/Y', strtotime($store['end_date'])); ?></strong></span>
						</div>
					</div>
				</div>
			</div>
		</header>
		<nav class="navbar" role="navigation">
        	<div class="navbar-wrpr">
				<ul id="main-nav" class="nav nav-pills">
					<li><a href="/preorder/<?php echo $store['url_slug'];?>">Store Home</a></li>
					<li><a href="/preorder/<?php echo $store['url_slug'];?>">Products</a></li>
					<li><a href="/preorder/<?php echo $store['url_slug'];?>/cart">Cart</a></li>
					<li><a href="/preorder/<?php echo $store['url_slug'];?>/contact">Contact</a></li>
					<li><a href="/quote-request" target="_blank">start your store</a></li>
				</ul>
            </div>
		</nav>
