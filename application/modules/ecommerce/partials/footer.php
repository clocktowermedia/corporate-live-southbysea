		<div class="push clearfix"></div>
	</div> <!-- end #container -->

	<footer id="mainFooter">
		<div class="container">
			<div class="row">
				<div class="col-lg-7 col-md-7 col-sm-7">
					<ul id="footer-nav" class="pull-left">
						<li><a href="/preorder/<?php echo $store['url_slug'];?>">Products</a></li>
						<li><a href="/preorder/<?php echo $store['url_slug'];?>/cart">Cart</a></li>
						<li><a href="/preorder/<?php echo $store['url_slug'];?>/contact">Contact</a></li>
					</ul>
					<p id="copyright">&#169; <?php echo date('Y');?> <?php echo $this->config->item('site_name');?> | <a href="http://www.clocktowermedia.com" title="Seattle Web Design">Seattle Web Design</a></p>
				</div>

				<div class="col-lg-5 col-md-5 col-sm-5">
                    <div class="foot-info pull-right">
                    	<img src="/assets/images/collegiate.png" alt="Collegiate Licensed Product" class="clp-seal">
                    	<img src="/assets/images/olpsxs.png" alt="Official Licensed Product" class="olp-seal">
                        <ul class="social-nav">
                            <li><a href="https://www.facebook.com/southbyseacollege" target="_blank"><img src="/assets/images/fb.png" alt="Facebook" title="South by Sea's Facebook"></a></li>
                            <li><a href="http://www.pinterest.com/southbysea" target="_blank"><img src="/assets/images/pntr.png" alt="Pinterest" title="South by Sea's Pinterest"></a></li>
                            <li><a href="http://instagram.com/southbyseacollege" target="_blank"><img src="/assets/images/inst.png" alt="Instagram" title="South by Sea's Instagram"></a></li>
                        </ul>
                     </div>
				</div>
			</div>
		</div>
	</footer><!-- end footer -->

	<div class="device-xs visible-xs"></div>
	<div class="device-sm visible-sm"></div>
	<div class="device-md visible-md"></div>
	<div class="device-lg visible-lg"></div>

	<!-- JavaScript -->
	<script src="/assets/js/site.js"></script>

	<?php
	if (file_exists(APPPATH . 'modules/'. $this->uri->segment(1) .'/partials/js.php'))
	{
		$this->load->view('../modules/'. $this->uri->segment(1) .'/partials/js');
	}
	?>

	<?php if (ENVIRONMENT == 'production'): ?>
	<?php endif; ?>

<?php $this->load->view('partials/meta-footer'); ?>
