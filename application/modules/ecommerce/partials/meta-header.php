<!DOCTYPE html>
<!--[if lt IE 7]>	<html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>		<html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>		<html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]>	<!--> <html class="no-js" lang="en"> <!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>
		<?php
			$baseTitle = $store['title']  . ' Preorder | ' . $this->config->item('site_name');

			if (isset($page))
			{
				echo $page['title'] . ' | ' . $baseTitle;
			} else if (isset($pageTitle)) {
				echo $pageTitle  . ' | ' . $baseTitle;
			} else if (isset($title)) {
				echo $title  . ' | ' . $baseTitle;
			} else {
				echo $baseTitle;
			}
		?>
	</title>

	<!-- Meta Tags -->
	<meta name="author" content="<?php echo $this->config->item('site_name'); ?> | College" />
	<meta name="description" content="<?php if (isset($page)) {echo $page['metaDesc'];} ?> <?php if (isset($metaDesc)) {echo $metaDesc;} ?>" />
	<meta name="keywords" content="<?php if (isset($page)) {echo $page['metaKeywords'];} ?> <?php if (isset($metaKeywords)) {echo $metaKeywords;} ?>" />

	<!-- Modernizr & Script Loader -->
	<script src="/assets/js/modernizr-2.5.3.js"></script>
	<script src="/assets/js/head.load.min.js"></script>

	<!-- Common/Shared CSS -->
	<link href="//fonts.googleapis.com/css?family=Raleway" rel="stylesheet" title="Google Fonts" type="text/css">
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="/assets/css/plugins.css">
	<link rel="stylesheet" href="/assets/css/style.css">

	<!-- Site Specific CSS -->
	<link rel="stylesheet" href="/assets/css/site.css">
	<link rel="stylesheet" href="/assets/css/ecomm.css">

	<!-- Module CSS -->
	<?php
		if (file_exists(APPPATH . 'modules/'. $this->router->fetch_module() .'/partials/css.php'))
		{
			$this->load->view('../modules/'. $this->router->fetch_module() .'/partials/css');
		}
	?>

	<!-- Favicon -->
	<link rel="icon" type="image/x-icon" sizes="16x16" href="/favicon.ico">
</head>

<body class="<?php echo $this->uri->segment(1) . ' ' . $this->router->fetch_module() . ' ' . $this->router->fetch_class() . ' ' . $this->router->fetch_method(); if(isset($bodyClass)) { echo ' ' . $bodyClass; } if(isset($page)) { echo ' page-' . $page['pageID']; } ?>">
