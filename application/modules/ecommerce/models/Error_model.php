<?php
/********************************************************************************************************
/
/ Ecomm Errors (from payment gateways)
/
********************************************************************************************************/
class Error_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'EcommErrors';

	//primary key
	public $primary_key = 'ecomm_error_id';

	//relationships
	public $belongs_to = array(
		'order' => array('model' => 'ecommerce/order_model', 'primary_key' => 'ecomm_order_id', 'join_key' => 'ecomm_order_id')
	);

	//callbacks/observers (use function names)

	//validation

	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Validity checker functions & helper functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
}