<?php
/********************************************************************************************************
/
/ Subscribers
/ Explanation: Site visitors can "subscribe" to a store and receive email updates
/
********************************************************************************************************/
class Subscriber_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'EcommSubscribers';

	//primary key
	public $primary_key = 'ecomm_sub_id';

	//relationships
	public $belongs_to = array(
		'store' => array('model' => 'ecommerce/store_model', 'primary_key' => 'ecomm_store_id', 'join_key' => 'ecomm_store_id')
	);

	//callbacks/observers (use function names)
	public $before_create = array('_create_timestamp');
	public $before_update = array('_create_update_timestamp');

	//validation
	public $validate = array(
		'ecomm_store_id' => array(
			'field' => 'ecomm_store_id',
			'label' => 'Store Name',
			'rules' => 'required|is_natural_no_zero'
		),
		'name' => array(
			'field' => 'name',
			'label' => 'Name',
			'rules' => 'required|min_length[2]'
		),
		'email' => array(
			'field' => 'email',
			'label' => 'Email',
			'rules' => 'required|valid_email'
		)
	);


	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
	protected function _create_timestamp($data)
	{
		$data['dateCreated'] = $data['dateUpdated'] = date('Y-m-d H:i:s');
		return $data;
	}

	protected function _create_update_timestamp($data)
	{
		$data['dateUpdated'] = date('Y-m-d H:i:s');
		return $data;
	}
}