<?php
/********************************************************************************************************
/
/ Product Sizes
/
********************************************************************************************************/
class Product_size_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'EcommProductSizes';

	//primary key
	public $primary_key = 'ecomm_product_size_id';

	//relationships
	public $belongs_to = array(
		'product' => array('model' => 'ecommerce/product_model', 'primary_key' => 'ecomm_product_id', 'join_key' => 'ecomm_product_id')
	);

	//callbacks/observers (use function names)

	//validation
	public $validate = array(
		'ecomm_product_id' => array(
			'field' => 'ecomm_product_id',
			'label' => 'Associated Product',
			'rules' => 'required|is_natural_no_zero'
		),
		'size' => array(
			'field' => 'size',
			'label' => 'Size',
			'rules' => 'required'
		),
		'price' => array(
			'field' => 'price',
			'label' => 'Price',
			'rules' => 'required|decimal'
		)
	);

	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/
	/**
	* Used to change the order in which sizes are displayed based on drag/drop re-arranging done by user
	*/
	function reorder(array $sizes = array())
	{
		//mke sure data passed is valid
		if (!is_null($sizes) && count($sizes) > 0)
		{
			//determine the total number of sizes
			$totalSizes = count($sizes);

			//loop through each gallery and update the order in the db
			for ($i = 0; $i < $totalSizes; $i++)
			{
				$this->db->where($this->primary_key, $sizes[$i])
					->set('display_order', $i)
					->update($this->_table);
			}

			return true;
		}

		return false;
	}

	/**********************************************************************************************
	*
	* Validity checker functions & helper functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
}