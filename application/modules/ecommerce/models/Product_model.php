<?php
/********************************************************************************************************
/
/ Stores
/ Explanation: Ecommerce store/site urls
/
********************************************************************************************************/
class Product_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'EcommProducts';

	//primary key
	public $primary_key = 'ecomm_product_id';

	//relationships
	public $belongs_to = array(
		'store' => array('model' => 'ecommerce/store_model', 'primary_key' => 'ecomm_store_id', 'join_key' => 'ecomm_store_id')
	);
	public $has_single = array(
		'image' => array('model' => 'ecommerce/product_image_model', 'primary_key' => 'ecomm_product_id', 'join_key' => 'ecomm_product_id', 'where' => array('defaultImg' => '1'))
	);
	public $has_many = array(
		'images' => array('model' => 'ecommerce/product_image_model', 'primary_key' => 'ecomm_product_id', 'join_key' => 'ecomm_product_id'),
		'sizes' => array('model' => 'ecommerce/product_size_model', 'primary_key' => 'ecomm_product_id', 'join_key' => 'ecomm_product_id'),
		'ordered' => array('model' => 'ecommerce/order_item_model', 'primary_key' => 'ecomm_cart_id', 'join_key' => 'ecomm_product_id')
	);

	//callbacks/observers (use function names)
	public $before_create = array('_create_timestamp');
	public $before_update = array('_create_update_timestamp');

	//validation
	public $validate = array(
		'ecomm_store_id' => array(
			'field' => 'ecomm_store_id',
			'label' => 'Store Name',
			'rules' => 'required|is_natural_no_zero'
		),
		'title' => array(
			'field' => 'title',
			'label' => 'Title',
			'rules' => 'required'
		),
		'url_slug' => array(
			'field' => 'url_slug',
			'label' => 'URL',
			'rules' => 'required|seo_url'
		),
		'price' => array(
			'field' => 'price',
			'label' => 'Price',
			'rules' => 'required|decimal'
		),
		'num_to_sell' => array(
			'field' => 'num_to_sell',
			'label' => 'Minimum # for Production',
			'rules' => 'required|is_natural'
		),
		'display_order' => array(
			'field' => 'display_order',
			'label' => 'Display Order',
			'rules' => 'is_natural'
		)
	);


	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/
	function getMinimum($storeID = 0)
	{
		//make sure valid store id was passed
		if ($storeID > 0)
		{
			//load models we need
			$this->load->model($this->belongs_to['store']['model'], 'store_model');
			$joinKey = $this->belongs_to['store']['join_key'];

			//start query
			$result = $this->db->select_sum('p.num_to_sell')
				->from($this->_table . ' p')
				->join($this->store_model->_table . ' s', "s.$joinKey = p.$joinKey AND s.ecomm_store_id = $storeID")
				->get()
				->row();

			return (int) $result->num_to_sell;
		}

		return false;
	}

	/**
	* Used to change the order in which products are displayed based on drag/drop re-arranging done by user
	*/
	function reorder(array $products = array())
	{
		//mke sure data passed is valid
		if (!is_null($products) && count($products) > 0)
		{
			//determine the total number of products
			$totalProducts = count($products);

			//loop through each gallery and update the order in the db
			for ($i = 0; $i < $totalProducts; $i++)
			{
				$this->db->where($this->primary_key, $products[$i])
					->set('display_order', $i)
					->update($this->_table);
			}

			return true;
		}

		return false;
	}

	/**
	* Soft delete of entry from db by id - change status to deleted
	*/
	function soft_delete($productID = 0)
	{
		//make sure product id is valid
		if ($productID > 0)
		{
			$this->db->where($this->primary_key, $productID)->set('status', 'deleted')->update($this->_table);
		}

		return false;
	}

	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
	protected function _create_timestamp($data)
	{
		$data['dateCreated'] = $data['dateUpdated'] = date('Y-m-d H:i:s');
		return $data;
	}

	protected function _create_update_timestamp($data)
	{
		$data['dateUpdated'] = date('Y-m-d H:i:s');
		return $data;
	}
}