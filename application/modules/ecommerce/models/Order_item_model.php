<?php
/********************************************************************************************************
/
/ Ecomm Order Items/Cart
/
********************************************************************************************************/
class Order_item_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'EcommCart';

	//primary key
	public $primary_key = 'ecomm_cart_id';

	//relationships
	public $belongs_to = array(
		'order' => array('model' => 'ecommerce/order_model', 'primary_key' => 'ecomm_order_id', 'join_key' => 'ecomm_order_id')
	);
	public $has_single = array(
		'product' => array('model' => 'ecommerce/product_model', 'primary_key' => 'ecomm_product_id', 'join_key' => 'ecomm_product_id'),
		'size' => array('model' => 'ecommerce/product_size_model', 'primary_key' => 'ecomm_product_size_id', 'join_key' => 'ecomm_product_size_id'),
	);

	//callbacks/observers (use function names)

	//validation

	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/

	public function getTotalAuthorized($storeID = 0)
	{
		//make sure valid store id was passed
		if ($storeID > 0)
		{
			//load models we need
			$this->load->model($this->belongs_to['order']['model'], 'order_model');
			$joinKey = $this->belongs_to['order']['join_key'];

			//start query
			$result = $this->db->select_sum('i.qty')
				->from($this->_table . ' i')
				->join($this->order_model->_table . ' o', "o.$joinKey = i.$joinKey AND o.status = 'authorized' AND o.ecomm_store_id = $storeID")
				->get()
				->row();

			return (int) $result->qty;
		}
	}

	public function getTotalOrdered($storeID = 0)
	{
		//make sure valid store id was passed
		if ($storeID > 0)
		{
			//load models we need
			$this->load->model($this->belongs_to['order']['model'], 'order_model');
			$joinKey = $this->belongs_to['order']['join_key'];

			//start query
			$result = $this->db->select_sum('i.qty')
				->from($this->_table . ' i')
				->join($this->order_model->_table . ' o', "o.$joinKey = i.$joinKey AND (o.status = 'authorized' OR o.status = 'completed') AND o.ecomm_store_id = $storeID")
				->get()
				->row();

			return (int) $result->qty;
		}
	}

	/**
	 * Get cart total from order id
	 * @param  integer $orderID
	 * @return decimal
	 */
	public function getCartTotal($orderID = 0)
	{
		//make sure valid order id was passed
		if ($orderID > 0)
		{
			//load models for sizes
			$this->load->model($this->has_single['size']['model'], 'product_size_model');
			$joinKey = (string) $this->has_single['size']['join_key'];

			//start query
			$query = $this->db->select('SUM(i.qty * s.price) AS cartTotal')
				->from($this->_table . ' AS i')
				->join($this->product_size_model->_table . ' AS s', "i.$joinKey = s.$joinKey")
				->where('i.ecomm_order_id', $orderID)
				->get();

			return number_format($query->row('cartTotal'), 2, '.', '');
		}
		return 0;
	}

	/**
	 * [getProductNumberOrdered description]
	 * @param  integer $productID
	 * @return integer
	 */
	public function getProductNumberOrdered($productID = 0)
	{
		//make sure valid product id was passed
		if ($productID > 0)
		{
			//load models for orders
			$this->load->model($this->belongs_to['order']['model'], 'order_model');
			$joinKey = (string) $this->belongs_to['order']['join_key'];

			//start query
			$query = $this->db->select('SUM(oi.qty) AS itemTotal')
				->from($this->_table . ' oi')
				->join($this->order_model->_table . ' AS o', "o.$joinKey = oi.$joinKey AND oi.ecomm_product_id = $productID")
				->where('o.status', 'authorized')
				->or_where('o.status', 'completed')
				->get();

			return number_format($query->row('itemTotal'), 0, '.', '');
		}

		return 0;
	}

	/**********************************************************************************************
	*
	* Validity checker functions & helper functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
}