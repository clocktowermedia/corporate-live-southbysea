<?php
/********************************************************************************************************
/
/ Ecomm Orders
/
********************************************************************************************************/
class Order_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'EcommOrders';

	//primary key
	public $primary_key = 'ecomm_order_id';

	//relationships
	public $belongs_to = array(
		'store' => array('model' => 'ecommerce/store_model', 'primary_key' => 'ecomm_store_id', 'join_key' => 'ecomm_store_id')
	);
	public $has_many = array(
		'items' => array('model' => 'ecommerce/order_item_model', 'primary_key' => 'ecomm_cart_id', 'join_key' => 'ecomm_order_id')
	);

	//callbacks/observers (use function names)
	public $before_create = array('_create_timestamp', '_set_confirmation_id');
	public $before_update = array('_create_update_timestamp');
	public $after_create = array('_update_unprocessed_count');
	public $after_update = array('_update_unprocessed_count');

	//validation

	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/
	/**
	 * Count number of orders by product ID
	 * @param  integer $productID
	 * @return integer
	 */
	public function countByProduct($productID = 0)
	{
		if ($productID > 0)
		{
			$this->load->model($this->has_many['items']['model'], 'order_item_model');
			$joinKey = (string) $this->has_many['items']['join_key'];

			$this->db->from($this->_table . ' o')
				->join($this->order_item_model->_table . ' oi', "oi.$joinKey = o.$joinKey")
				->where('o.status !=', 'pending')
				->where('oi.ecomm_product_id', $productID)
				->group_by('o.' . $this->primary_key);

			return $this->db->count_all_results();
		}

		return 0;
	}

	/**
	 * Count number of orders by size ID
	 * @param  integer $sizeID
	 * @return integer
	 */
	public function countBySize($sizeID = 0)
	{
		if ($sizeID > 0)
		{
			$this->load->model($this->has_many['items']['model'], 'order_item_model');
			$joinKey = (string) $this->has_many['items']['join_key'];

			$row = $this->db->select('COUNT(oi.' . $this->primary_key .') AS orderCount', false)
				->from($this->_table . ' o')
				->join($this->order_item_model->_table . ' oi', "oi.$joinKey = o.$joinKey")
				//->where('o.status !=', 'pending')
				->where('oi.ecomm_product_size_id', $sizeID)
				->where('o.status !=', 'pending')
				->where('o.status !=', 'refunded')
				->get()
				->row_array();

			return $row['orderCount'];
		}

		return 0;
	}

	public function getCart($orderID = 0)
	{
		if ($orderID > 0)
		{
			//load models
			$this->load->model($this->has_many['items']['model'], 'order_item_model');
			$this->load->model($this->order_item_model->has_single['product']['model'], 'product_model');

			//get order
			$order = $this->select('ecomm_order_id, ecomm_store_id')->append('store')->get($orderID);

			//get items
			$cart = $this->order_item_model->append_selection('product', 'url_slug, title')
				->append('product')
				->append_selection('size', 'size, price')
				->append('size')
				->get_many_by('ecomm_order_id', $orderID);

			//define subtotal
			$data['subtotal'] = 0;

			//get product images, and start calculating subtotal
			if (count($cart) > 0)
			{
				foreach ($cart AS $key => $item)
				{
					$product = $this->product_model->select('ecomm_product_id')
						->append_selection('image', 'fullpath, thumbFullpath')
						->append('image')
						->get($item['ecomm_product_id']);
					$cart[$key]['product']['image'] = $product['image'];

					//add to subtotal
					$data['subtotal'] += ($item['qty'] * $item['size']['price']);
					unset($cart[$key]['ecomm_order_id']);
				}
			}
			$data['cart'] = $cart;

			//get tax rate
			$taxRate = $order['store']['tax_rate'];

			//get totals
			$data['subtotal'] = number_format($data['subtotal'], 2, '.', '');
			$data['tax'] = number_format(($taxRate / 100) * $data['subtotal'], 2, '.', '');
			$data['total'] = number_format($data['tax'] + $data['subtotal'], 2, '.', '');

			//return data
			return $data;
		}

		return false;
	}

	public function updateUnprocessedCount($storeID = 0)
	{
		if ($storeID > 0)
		{
			$unprocessed = $this->select($this->primary_key)->where('ecomm_store_id', $storeID)->where('status', 'authorized')->count_all_results();

			$this->load->model($this->belongs_to['store']['model']);
			$modelName = basename($this->belongs_to['store']['model']);
			$this->{$modelName}->skip_validation()->update($storeID, array(
				'unprocessedCount' => $unprocessed
			));
		}
	}

	/**********************************************************************************************
	*
	* Validity checker functions & helper functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
	protected function _create_timestamp($data)
	{
		$data['dateCreated'] = $data['dateUpdated'] = date('Y-m-d H:i:s');
		return $data;
	}

	protected function _create_update_timestamp($data)
	{
		$data['dateUpdated'] = date('Y-m-d H:i:s');
		return $data;
	}

	protected function _set_confirmation_id($data)
	{
		$data['confirmationNumber'] = $this->_generate_confirmation_id();
		return $data;
	}

	protected function _generate_confirmation_id()
	{
		//set confirmation #
		$this->load->helper('string');
		$string = random_string('alnum', 16);

		//make sure it doesnt already exist
		$exists = $this->get_by('confirmationNumber', $string);
		if (empty($exists) || $exists == false)
		{
			return $string;
		} else {
			$this->_generate_confirmation_id();
		}
	}

	protected function _update_unprocessed_count($id)
	{
		$id = (is_array($id))? $id[0] : $id;
		if (is_numeric($id) && $id > 0)
		{
			$data = $this->select('ecomm_store_id')->get($id);
			$this->updateUnprocessedCount($data['ecomm_store_id']);
		}
	}
}