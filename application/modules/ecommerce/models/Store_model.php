<?php
/********************************************************************************************************
/
/ Stores
/ Explanation: Ecommerce store/site urls
/
********************************************************************************************************/
class Store_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'EcommStores';

	//primary key
	public $primary_key = 'ecomm_store_id';

	//relationships
	public $has_single = array(
		'creator' => array('model' => 'admins/admin_model', 'primary_key' => 'adminID', 'join_key' => 'created_by'),
		'account_mgr' => array('model' => 'admins/admin_model', 'primary_key' => 'adminID', 'join_key' => 'acct_mgr_id')
	);

	public $has_many = array(
		'orders' => array('model' => 'ecommerce/order_model', 'primary_key' => 'ecomm_order_id', 'join_key' => 'ecomm_store_id'),
		'products' => array('model' => 'ecommerce/product_model', 'primary_key' => 'ecomm_product_id', 'join_key' => 'ecomm_store_id'),
		'sellable_products' => array('model' => 'ecommerce/product_model', 'primary_key' => 'ecomm_product_id', 'join_key' => 'ecomm_store_id', 'where' => array('status' => 'enabled')),
		'subscribers' => array('model' => 'ecommerce/subscriber_model', 'primary_key' => 'ecomm_sub_id', 'join_key' => 'ecomm_store_id')
	);

	//callbacks/observers (use function names)
	public $before_create = array('_create_timestamp', '_set_date_times', '_set_created_by', '_account_mgr', '_tax_rate', '_set_password');
	public $before_update = array('_create_update_timestamp', '_set_date_times', '_account_mgr', '_tax_rate', '_set_password');

	//validation
	public $validate = array(
		'title' => array(
			'field' => 'title',
			'label' => 'Store Name',
			'rules' => 'required|min_length[2]'
		),
		'url_slug' => array(
			'field' => 'url_slug',
			'label' => 'URL',
			'rules' => 'required|seo_url'
		),
		'start_date' => array(
			'field' => 'start_date',
			'label' => 'Store Open Date',
			'rules' => 'required'
		),
		'end_date' => array(
			'field' => 'end_date',
			'label' => 'Store Close Date',
			'rules' => 'required'
		),
		'num_to_sell' => array(
			'field' => 'num_to_sell',
			'label' => 'Order Minimum',
			'rules' => 'is_natural'
		),
		'acct_mgr_id' => array(
			'field' => 'acct_mgr_id',
			'label' => 'Account Manager',
			'rules' => 'required|is_natural_no_zero'
		),
		'coord_first_name' => array(
			'field' => 'coord_first_name',
			'label' => 'Order Corodinator First Name',
			'rules' => 'required|min_length[2]'
		),
		'coord_last_name' => array(
			'field' => 'coord_last_name',
			'label' => 'Order Corodinator Last Name',
			'rules' => 'required|min_length[2]'
		),
		'coord_address1' => array(
			'field' => 'coord_address1',
			'label' => 'Order Corodinator Address',
			'rules' => 'required'
		),
		'coord_city' => array(
			'field' => 'coord_city',
			'label' => 'Order Corodinator City',
			'rules' => 'required'
		),
		'coord_state' => array(
			'field' => 'coord_state',
			'label' => 'Order Corodinator State',
			'rules' => 'required|min_length[2]|max_length[2]'
		),
		'coord_zip' => array(
			'field' => 'coord_zip',
			'label' => 'Order Corodinator Zip',
			'rules' => 'required|min_length[5]'
		),
		'created_by' => array(
			'field' => 'created_by',
			'label' => 'Created By',
			'rules' => 'required|is_natural_no_zero'
		),
		'tax_rate' => array(
			'field' => 'tax_rate',
			'label' => 'Tax Rate',
			'rules' => 'required|decimal'
		)
		/*
		'status' => array(
			'field' => 'status',
			'label' => 'Status',
			'rules' => 'required'
		)
		*/
	);


	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/
	function get_stores_with_minimum(array $filters = array())
	{
		//load models
		$this->load->model($this->has_many['products']['model'], 'product_model');

		//get stores
		$stores = $this->append('account_mgr')->get_many_by($filters);

		//calculate number of orders
		if (count($stores) > 0)
		{
			$this->load->model('ecommerce/order_item_model');

			foreach ($stores AS $key => $store)
			{
				$stores[$key]['minimum'] = $this->product_model->getMinimum($store['ecomm_store_id']);
				$stores[$key]['total'] = $this->order_item_model->getTotalAuthorized($store['ecomm_store_id']);

				if ($stores[$key]['total'] >= $stores[$key]['minimum'])
				{
					$storeArray['met_requirement'][] = $stores[$key];
				} else {
					$storeArray['behind_requirement'][] = $stores[$key];
				}
			}

			return $storeArray;
		}

		return $stores;
	}

	/**
	* Soft delete of entry from db by id - change status to deleted
	*/
	function soft_delete($storeID = 0)
	{
		//make sure product id is valid
		if ($storeID > 0)
		{
			$this->db->where($this->primary_key, $storeID)->set('status', 'deleted')->update($this->_table);
		}

		return false;
	}

	/**
	* Disable of entry from db by id - change status to disabled
	*/
	function disable($storeID = 0)
	{
		//make sure product id is valid
		if ($storeID > 0)
		{
			$this->db->where($this->primary_key, $storeID)->set('status', 'disabled')->update($this->_table);
		}

		return false;
	}

	/**
	* Enable of entry from db by id - change status to enabled
	*/
	function enable($storeID = 0)
	{
		//make sure product id is valid
		if ($storeID > 0)
		{
			$this->db->where($this->primary_key, $storeID)->set('status', 'enabled')->update($this->_table);
		}

		return false;
	}

	/**
	 * Provides a list of matching store names based on what the user is typing
	 * @param  string  $query [the string the user is typing]
	 * @param  integer $limit [limit the number of matches returned]
	 * @return array
	 */
	function autosuggest($query = '', $limit = 20)
	{
		//make sure we have a valid search term
		if (!is_null($query) && $query != '')
		{
			//start query
			$this->db->select($this->primary_key . ' AS id', false)
				->select('CONCAT(title, " (/preorder/", url_slug, ")") AS text', false)
				->select('DATE_FORMAT(end_date, "%Y-%m-%d") AS end_date', false)
				->like('title', $query)
				->from($this->_table . ' AS s')
				->where('status !=', 'deleted')
				->where('status !=', 'disabled');

			//add limit/offset
			if ($limit > 0)
			{
				$this->db->limit($limit);
			}

			//get/return results
			$results = $this->db->get()->result_array();
			return $results;
		}

		return false;
	}

	function authenticate($storeID = 0, $password = '')
	{
		if ($storeID > 0 && $password != '')
		{
			$store = $this->get($storeID);
			if ($store['passwordEnabled'] == TRUE)
			{
				$this->load->library('encryption');
				$storePw = $this->encryption->decrypt($store['storeP']);

				if ($storePw == $password)
				{
					return true;
				} else {
					return false;
				}
			}

			return true;
		}

		return false;
	}

	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
	protected function _set_date_times($data)
	{
		if (isset($data['start_date']))
		{
			if (isset($data['start_time']) && $data['start_time'] != '')
			{
				$data['start_date'] = date('Y-m-d', strtotime($data['start_date'])) . ' ' . date('H:i:s', strtotime($data['start_time']));
				unset($data['start_time']);
			} else {
				$data['start_date'] = date('Y-m-d', strtotime($data['start_time'])) . ' 00:00:00';
			}
		}

		if (isset($data['end_date']))
		{
			if (isset($data['end_time']) && $data['end_time'] != '')
			{
				$data['end_date'] = date('Y-m-d', strtotime($data['end_date'])) . ' ' . date('H:i:s', strtotime($data['end_time']));
				unset($data['end_time']);
			} else {
				$data['end_date'] = date('Y-m-d', strtotime($data['end_time'])) . ' 23:59:59';
			}
		}

		return $data;
	}

	protected function _create_timestamp($data)
	{
		$data['dateCreated'] = $data['dateUpdated'] = date('Y-m-d H:i:s');
		return $data;
	}

	protected function _create_update_timestamp($data)
	{
		$data['dateUpdated'] = date('Y-m-d H:i:s');
		return $data;
	}

	protected function _set_created_by($data)
	{
		$userID = (int) $this->session->userdata('adminID');
		if ($userID > 0)
		{
			$data['created_by'] = $userID;
		}

		return $data;
	}

	protected function _account_mgr($data)
	{
		if (isset($data['acct_mgr_id']))
		{
			$data['acct_mgr_id'] = (int) $data['acct_mgr_id'];
		}
		return $data;
	}

	protected function _tax_rate($data)
	{
		//temporarily disable until client is ready to use avalara
		$this->config->load('api_keys');
		$avalara = $this->config->item('avalara');

		if ($avalara['calc_tax'] == true && isset($data['coord_zip']) && $data['coord_zip'] != '')
		{
			//get tax rate
			$this->load->library('avalara_tax_rates');
			$taxRate = (int) $this->avalara_tax_rates->calc_tax(array(
				'address1' => (isset($data['coord_address1']))? $data['coord_address1'] : '',
				'city' => (isset($data['coord_city']))? $data['coord_city'] : '',
				'state' => (isset($data['coord_state']))? $data['coord_state'] : '',
				'zip' => $data['coord_zip']
			));

			//return tax rate & date updated
			$originalTax = (isset($data['tax_rate']))? $data['tax_rate'] : 0;
			$data['tax_rate'] = (!is_null($taxRate))? number_format($taxRate, 2, '.', '') : $originalTax;
			$data['tax_rate_updated'] = date('Y-m-d H:i:s');

		} else if (isset($data['tax_rate'])) {

			$data['tax_rate_updated'] = date('Y-m-d H:i:s');
		}

		return $data;
	}

	protected function _set_password($data)
	{
		if (isset($data['passwordEnabled']) && $data['passwordEnabled'] == TRUE && isset($data['storeP']) && $data['storeP'] != '')
		{
			$this->load->library('encryption');
			$data['storeP'] = $this->encryption->encrypt($data['storeP']);
		}

		return $data;
	}
}