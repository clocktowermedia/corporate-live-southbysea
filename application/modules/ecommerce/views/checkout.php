<div class="text-center">

	<h1 class="ecomm-title"><?php echo $title; ?></h1>
	<span class="ecomm-dash"></span>

	<div class="col-sm-8 col-xs-12 no-pad-inner-rows">
		<p class="no-margin mb-20 text-left">
			Your order will not be shipped to you individually and will not go into production until store closes on <?php echo date('n/d/Y', strtotime($store['end_date'])); ?>.
		</p>

		<?php echo form_open('/preorder/' . $store['url_slug'] . '/checkout', array('id' => 'checkout-form')); ?>
		<fieldset>
	        <?php
				//add the form
				$this->load->view('../partials/forms/checkout');
	        ?>
    	</fieldset>
		</form>

		<p>Please do not click "submit" more than once or refresh the page after you have clicked "submit".</p>
	</div>

	<div class="col-sm-4 col-xs-12 text-center">
		<div class="sidebar">
			<h3 class="wgt-title"><i class="arrow-triple-left"></i> Summary <i class="arrow-triple-right"></i></h3>

			<div class="hb-container" id="pricing-container">
				<table class="table table-condensed">
					<tr>
						<td class="text-left">Subtotal:</td>
						<td class="text-right">$<?php echo $order['subtotal'];?></td>
					</tr>
					<tr>
						<td class="text-left">Tax:</td>
						<td class="text-right">$<?php echo $order['tax'];?></td>
					</tr>
					<tr>
						<td class="text-left">Total:</td>
						<td class="text-right"><span class="total">$<?php echo $order['total'];?></span></td>
					</tr>
				</table>
			</div>
		</div>
	</div>

</div>