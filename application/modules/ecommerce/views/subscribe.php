<div class="modal-body">
	<div id="subscribe-form-messages"></div>

	<span>Enter your name and email in the form below to receive news about this store.</span>

	<?php echo form_open('', array('id' => 'subscribe-form', 'class' => 'form-horizontal')); ?>
		<?php
			$containerArray = array('labelClass' => 'col-sm-1', 'containerClass' => 'col-sm-11');

			//Name Field
			$this->form_builder->text('name', 'Name:', (isset($submission['name']))? $submission['name'] : '', $containerArray, 'Your Name', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));

			//email field
			$this->form_builder->email('email', 'Email:', (isset($submission['email']))? $submission['email'] : '', $containerArray, 'Your Email', '', '', array('required' => 'required'));

			//hidden field
			$this->form_builder->hidden('ecomm_store_id', $store['ecomm_store_id'], '');

			//submit button
			$this->form_builder->button('submit', 'Subscribe', array('containerClass' => 'pull-right', 'inputClass' => 'btn btn-info btn-lg'), '', '');
		?>
	</form>
</div>