<?php
	//share this settings
	$shareUrl = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	$shareText = 'Look at this cool product I found at South By Sea!';
	$productDisclaimer = '<p>This is a presale order that will be shipped in bulk to the order coordinator! Please click <a href="/preorder/' . $store['url_slug'] . '/contact">here</a> if you have any questions and need to contact the order coordinator.</p>';
?>

<div class="hidden-xs text-center">

	<div class="col-sm-4">
		<h1 class="ecomm-title"><?php echo $product['title']; ?></h1>
		<span class="ecomm-dash"></span>
		<div class="ecomm-price">$
			<span class="attached-to" data-watch="select[name='size']" data-watch-attr="data-price">
				<?php echo min($prices); ?>
				<?php if (min($prices) != max($prices)): ?>
					- $<?php echo max($prices);?>
				<?php endif; ?>
			</span>
		</div>
		<?php if ($product['desc'] != ''): ?>
			<p><?php echo $product['desc']; ?></p>
		<?php endif; ?>
		<?php echo $productDisclaimer;?>

		<?php echo form_open('/preorder/' . $store['url_slug'] . '/cart/json/add-item', array('id' => 'add_to_cart', 'class' => 'form-horizontal hijack-submit')); ?>
			<?php
				$containerArray = array('labelClass' => 'col-xs-2', 'containerClass' => 'col-xs-10');
			?>
			<div class="form-group">
				<label for="size" class="control-label <?php echo $containerArray['labelClass'];?>">Size:</label>
				<div class="<?php echo $containerArray['containerClass'];?>">
					<select name="size" class="form-control" required="required">
						<?php if (count($size_options) > 0): ?>
							<option value="" disabled selected>Select a Size</option>
							<?php foreach ($size_options AS $option): ?>
								<option value="<?php echo $option['ecomm_product_size_id'];?>" data-price="<?php echo $option['price'];?>"><?php echo $option['size'];?> - $<?php echo $option['price'];?></option>
							<?php endforeach; ?>
						<?php endif; ?>
					</select>
				</div>
			</div>
			<?php
				$this->form_builder->hidden('product_id', $product['ecomm_product_id'], '');
				$this->form_builder->button('submit', 'Add to Cart', array('containerClass' => '' , 'inputClass' => 'btn btn-info btn-lg'), '', '');
			?>
		</form>


		<?php if ($store['num_to_sell'] > 0): ?>
			<div id="num-sold">
				<?php if ($store['num_to_sell'] - $store['num_ordered'] > 0): ?>
					<?php echo $store['num_ordered']; ?> sold, <?php echo $store['num_to_sell'] - $store['num_ordered']; ?> more orders needed <i class="fa fa-exclamation-circle"></i>
				<?php else: ?>
					Order Minimum Met
				<?php endif; ?>
			</div>
		<?php endif; ?>

	</div>

	<div class="col-sm-8">
		<div class="ecomm-product-image">
			<?php if (isset($product['image']['fullpath']) && $product['image']['fullpath'] != ''): ?>
				<img class="img-full" src="<?php echo $product['image']['fullpath']; ?>" title="<?php echo $product['title'];?>" alt="<?php echo $product['title'];?>">
			<?php else: ?>
				<img class="img-full" src="http://dummyimage.com/400x400/eee/57656e&text=No+Image+Available"  title="<?php echo $product['title'];?>" alt="<?php echo $product['title'];?>">
			<?php endif; ?>
		</div>
	</div>

</div>

<div class="visible-xs text-center">
	<h1 class="ecomm-title">Preorder <?php echo $product['title']; ?></h1>
	<span class="ecomm-dash"></span>
	<span class="ecomm-price">$<?php echo $product['price']; ?></span>
	<?php echo $productDisclaimer;?>

	<div class="ecomm-product-image">
		<?php if (isset($product['image']['fullpath']) && $product['image']['fullpath'] != ''): ?>
			<img class="img-full" src="<?php echo $product['image']['fullpath']; ?>" title="<?php echo $product['title'];?>" alt="<?php echo $product['title'];?>">
		<?php else: ?>
			<img class="img-full" src="http://dummyimage.com/400x400/eee/57656e&text=No+Image+Available"  title="<?php echo $product['title'];?>" alt="<?php echo $product['title'];?>">
		<?php endif; ?>
	</div>

	<?php echo form_open('/preorder/' . $store['url_slug'] . '/cart/json/add-item', array('id' => 'add_to_cart', 'class' => 'form-horizontal hijack-submit')); ?>
		<div class="form-group">
			<label for="size" class="control-label <?php echo $containerArray['labelClass'];?>">Size:</label>
			<div class="<?php echo $containerArray['containerClass'];?>">
				<select name="size" class="form-control" required="required">
					<?php if (count($size_options) > 0): ?>
						<option value="" disabled selected>Select a Size</option>
						<?php foreach ($size_options AS $option): ?>
							<option value="<?php echo $option['ecomm_product_size_id'];?>" data-price="<?php echo $option['price'];?>"><?php echo $option['size'];?> - $<?php echo $option['price'];?></option>
						<?php endforeach; ?>
					<?php endif; ?>
				</select>
			</div>
		</div>
		<?php
			$this->form_builder->hidden('product_id', $product['ecomm_product_id'], '');
			$this->form_builder->button('submit', 'Add to Cart', array('containerClass' => '' , 'inputClass' => 'btn btn-info btn-lg'), '', '');
		?>
	</form>

	<?php if ($store['num_to_sell'] > 0): ?>
		<div id="num-sold">
			<?php if ($store['num_to_sell'] - $store['num_ordered'] > 0): ?>
				<?php echo $store['num_ordered']; ?> sold, <?php echo $store['num_to_sell'] - $store['num_ordered']; ?> more orders needed <i class="fa fa-exclamation-circle"></i>
			<?php else: ?>
				Order Minimum Met
			<?php endif; ?>
		</div>
	<?php endif; ?>
</div>