<div class="text-center">

	<h1 class="ecomm-title"><?php echo $title; ?></h1>
	<span class="ecomm-dash"></span>

	<?php if (!empty($order['cart'])): ?>
		<div class="col-sm-8 col-xs-12">
			<div class="row hb-container" id="cart-container">
				<table>
					<?php foreach ($order['cart'] as $item): ?>
						<tr class="cart-item">
							<td class="col-sm-1 col-xs-2">
								<div class="remove">
									<a class="remove-item" data-id="<?php echo $item['ecomm_cart_id'];?>">
										<i class="fa fa-times-circle fa-3"></i>
									</a>
								</div>
							</td>

							<td class="col-sm-2 hidden-xs text-center">
								<div class="item-img">
									<a href="/preorder/<?php echo $store['url_slug'];?>/<?php echo $item['product']['url_slug'];?>">
										<?php if ($item['product']['image']['thumbFullpath'] != ''): ?>
											<img src="<?php echo $item['product']['image']['thumbFullpath'];?>" title="<?php echo $item['product']['title'];?>" alt="<?php echo $item['product']['title'];?>">
										<?php else: ?>
											<img src="http://dummyimage.com/400x400/eee/57656e&text=No+Image+Available" title="<?php echo $item['product']['title'];?>" alt="<?php echo $item['product']['title'];?>">
										<?php endif; ?>
									</a>
								</div>
							</td>

							<td class="col-sm-4 col-xs-5">
								<p>
									<a href="/preorder/<?php echo $store['url_slug'];?>/<?php echo $item['product']['url_slug'];?>">
										<?php echo $item['product']['title']; ?>
									</a><br/>
									<?php echo $item['size']['size']; ?>
								</p>
							</td>

							<td class="col-sm-2 col-xs-2">
								<span class="item-price">$<?php echo $item['size']['price']; ?></span>
							</td>
							<td class="col-sm-3 col-xs-3">
								<input class="form-control pull-right" type="number" name="qty" value="<?php echo $item['qty']; ?>" data-id="<?php echo $item['ecomm_cart_id'];?>" min="1" required data-validation-required-message="Required" data-validation-min-message="1 or more">
							</td>
						</tr>
					<?php endforeach; ?>
				</table>
			</div>

			<div class="row">
				<div class="col-xs-12">
					<a id="update-cart-btn" class="btn sbs-btn btn-lg">Update Cart</a>
				</div>
			</div>
		</div>

		<div class="col-sm-4 col-xs-12 text-center">
			<div class="sidebar">
				<h3 class="wgt-title"><i class="arrow-triple-left"></i> Summary <i class="arrow-triple-right"></i></h3>

				<div class="hb-container" id="pricing-container">
					<table class="table table-condensed">
						<tr>
							<td class="text-left">Subtotal:</td>
							<td class="text-right">$<?php echo $order['subtotal'];?></td>
						</tr>
						<tr>
							<td class="text-left">Tax:</td>
							<td class="text-right">$<?php echo $order['tax'];?></td>
						</tr>
						<tr>
							<td class="text-left">Total:</td>
							<td class="text-right"><span class="total">$<?php echo $order['total'];?></span></td>
						</tr>
					</table>
				</div>

				<a id="checkout-btn" href="/preorder/<?php echo $store['url_slug'];?>/checkout/" class="btn sbs-btn">Checkout&nbsp;&nbsp;<i class="arrow-dbl-left"></i></a>

			</div>
		</div>

		<script id="cart-markup" type="x-handlebars-template">
			<table>
				{{#each cart}}
					<tr class="cart-item">
						<td class="col-sm-1 col-xs-2">
							<div class="remove">
								<a class="remove-item" data-id="{{ecomm_cart_id}}">
									<i class="fa fa-times-circle fa-3"></i>
								</a>
							</div>
						</td>

						<td class="col-sm-2 hidden-xs text-center">
							<div class="item-img">
								<a href="/preorder/{{../store.url_slug}}/{{product.url_slug}}">
									{{#if product.image.thumbFullpath}}
										<img src="{{product.image.thumbFullpath}}" title="{{product.title}}" alt="{{product.title}}">
									{{else}}
										<img src="http://dummyimage.com/400x400/eee/57656e&text=No+Image+Available" title="{{product.title}}" alt="{{product.title}}">
									{{/if}}
								</a>
							</div>
						</td>

						<td class="col-sm-4 col-xs-5">
							<p>
								<a href="/preorder/{{../store.url_slug}}/{{product.url_slug}}">
									{{product.title}}
								</a><br/>
								{{size.size}}
							</p>
						</td>

						<td class="col-sm-2 col-xs-2">
							<span class="item-price">${{size.price}}</span>
						</td>
						<td class="col-sm-3 col-xs-3">
							<input class="form-control pull-right" type="number" name="qty" value="{{qty}}" data-id="{{ecomm_cart_id}}" min="1">
						</td>
					</tr>
				{{/each}}
			</table>
		</script>

		<script id="pricing-markup" type="x-handlebars-template">
			<table class="table table-condensed">
				<tr>
					<td class="text-left">Subtotal:</td>
					<td class="text-right">${{subtotal}}</td>
				</tr>
				<tr>
					<td class="text-left">Tax:</td>
					<td class="text-right">${{tax}}</td>
				</tr>
				<tr>
					<td class="text-left">Total:</td>
					<td class="text-right"><span class="total">${{total}}</span></td>
				</tr>
			</table>
		</script>

	<?php else: ?>
		<p>Your cart is empty! Sounds like a good time to <a href="/preorder/<?php echo $store['url_slug'];?>">start shopping</a>.</p>
	<?php endif; ?>
</div>