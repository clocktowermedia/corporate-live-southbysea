<div class="text-center">

	<h1 class="ecomm-title"><?php echo $title; ?></h1>
	<span class="ecomm-dash"></span>

	<p>Thank you for your order totalling $<?php echo $order['total'];?>. Your confirmation number is <strong>#<?php echo $order['details']['confirmationNumber'];?></strong>, please keep this number for your records. You should receive an email confirmation with your order details shortly, please check your spam folder as well. Please note that your order will not be shipped to you individually and will not go into production until store closes on <?php echo date('n/d/Y', strtotime($store['end_date'])); ?>.</p>
	<?php if ($order['details']['payment_method'] == 'credit'): ?>
		<p>Your card will be charged at a later date when the order is shipped to your order coordinator.</p>
	<?php endif;?>

	<?php if ($order['details']['payment_method'] == 'paypal'): ?>
		<p>Your account balance will be deducted at a later date when the order is shipped to your order coordinator.</p>
	<?php endif; ?>

	<p>Click the button below to download a PDF of your order for record-keeping purposes.</p>
	<a target="_blank" href="/preorder/<?php echo $store['url_slug'];?>/generate-pdf/<?php echo $order['details']['confirmationNumber'];?>" id="download-pdf" class="btn sbs-btn btn-lg">Download PDF</a>
</div>