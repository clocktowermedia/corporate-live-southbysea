<div class="text-center">

	<h1 class="ecomm-title"><?php echo $title; ?></h1>
	<span class="ecomm-dash"></span>

	<?php echo form_open('/preorder/' . $store['url_slug'] . '/contact', array('accept-charset' => 'UTF-8')); ?>
	<fieldset>
		<?php
			//add the form
			$this->load->view('../../ecommerce/partials/forms/contact');

			//submit button
			$this->form_builder->button('submit', 'Send', array('inputClass' => 'btn btn-info'), '', '');
		?>
	</fieldset>
	</form>

</div>