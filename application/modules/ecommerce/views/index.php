<div class="row" style ="margin-top:-50px;">

	<?php if (date('Y-m-d H:i:s') < $store['end_date']): ?>

		<?php if ($passwordEntered == FALSE): ?>
			<div class="col-xs-6">
				<h4>Enter store password in the field below:</h4>
				<?php echo form_open('preorder/' . $store['url_slug'] . '/login'); ?>
					<?php
						$this->form_builder->set_labels(false);
						$this->form_builder->password('password', '', '', '', '******', '', '', array('required' => 'required'));
						$this->form_builder->button('submit', 'Login', array('', 'inputClass' => 'btn btn-info'), '', '');
					?>
				</form>
			</div>
		<?php else: ?>

			<?php if (count($store['products']) > 0): ?>
				<?php foreach ($store['products'] AS $product): ?>
						<div class="col-md-4 col-sm-6 col-xs-12 hp-ecomm-product">
							<div class="hp-ecomm-product-display">
								<h4>
									<a href="/preorder/<?php echo $store['url_slug'];?>/<?php echo $product['url_slug'];?>">
										<?php echo $product['title']; ?>
									</a>
									<i class="arrow-dbl-left"></i>
								</h4>

								<a href="/preorder/<?php echo $store['url_slug'];?>/<?php echo $product['url_slug'];?>">
									<?php if (isset($product['image']['fullpath']) && $product['image']['fullpath'] != ''): ?>
										<img class="img-full" src="<?php echo $product['image']['fullpath']; ?>" title="<?php echo $product['title'];?>" alt="<?php echo $product['title'];?>">
									<?php else: ?>
										<img class="img-full" src="http://dummyimage.com/400x400/eee/57656e&text=No+Image+Available"  title="<?php echo $product['title'];?>" alt="<?php echo $product['title'];?>">
									<?php endif; ?>
								</a>
							</div>
							<div class="hp-ecomm-product-info">
								<h2><?php echo $product['title']; ?></h2>
								<span class="ecomm-dash"></span>
								<span class="hp-ecomm-price">$<?php echo $product['price']; ?></span>
							</div>
						</div>
				<?php endforeach; ?>
			<?php endif; ?>

		<?php endif; ?>

	<?php else: ?>

		<h1>This campaign has ended.</h1>
		<h3>Check in with your order coordinator to see when the next campaign starts.</h3>

	<?php endif; ?>
	<div class="clearfix"></div><br><br>


</div>
