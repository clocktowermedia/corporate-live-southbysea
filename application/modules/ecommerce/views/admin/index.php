<div class="row">
	<div class="col-xs-12">

		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#active" data-toggle="tab">Active</a></li>
				<li><a href="#disabled" data-toggle="tab">Disabled</a></li>
				<li><a href="#inactive" data-toggle="tab">Inactive</a></li>
				<li class="pull-right"><a href="/admin/ecommerce/create" class="btn"><i class="fa fa-plus"></i> Create Store</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="active">

					<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered datatable custom-table" width="100%" data-url="/admin/ecommerce/datatables/data" id="active-stores">
						<thead>
							<tr>
								<th data-class="expand">Title</th>
								<th data-hide="phone, tablet">URL</th>
								<th data-hide="phone">Start Date</th>
								<th data-hide="phone">End Date</th>
								<th data-hide="phone, tablet">Created By</th>
								<th data-hide="phone" class="acct-mgr">Account Manager</th>
								<th class="no-search">Unprocessed Orders</th>
								<th class="no-search">Actions</th>
							</tr>
						</thead>

						<tbody>
						</tbody>
					</table>

				</div>

				<div class="tab-pane" id="disabled">

					<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered datatable custom-table" width="100%" data-url="/admin/ecommerce/datatables/data-disabled" id="disabled-stores">
						<thead>
							<tr>
								<th data-class="expand">Title</th>
								<th data-hide="phone, tablet">URL</th>
								<th data-hide="phone">Start Date</th>
								<th data-hide="phone">End Date</th>
								<th data-hide="phone, tablet">Created By</th>
								<th data-hide="phone" class="acct-mgr">Account Manager</th>
								<th class="no-search">Unprocessed Orders</th>
								<th class="no-search">Actions</th>
							</tr>
						</thead>

						<tbody>
						</tbody>
					</table>

				</div>

				<div class="tab-pane" id="inactive">

					<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered datatable custom-table" width="100%" data-url="/admin/ecommerce/datatables/data-inactive" id="inactive-stores">
						<thead>
							<tr>
								<th data-class="expand">Title</th>
								<th data-hide="phone, tablet">URL</th>
								<th data-hide="phone">Start Date</th>
								<th data-hide="phone">End Date</th>
								<th data-hide="phone, tablet">Created By</th>
								<th data-hide="phone" class="acct-mgr">Account Manager</th>
								<th class="no-search">Unprocessed Orders</th>
								<th class="no-search">Actions</th>
							</tr>
						</thead>

						<tbody>
						</tbody>
					</table>

				</div>
			</div>
		</div>

	</div>
</div>

<div class="hide" id="acct_mgr_dropdown">
	<select name="acct_mgr">
		<?php if (count($managers) > 0): ?>
			<?php foreach ($managers AS $id => $name): ?>
				<?php $selected = ($_SESSION['adminID'] == $id)? 'selected="selected"' : ''; ?>
				<?php if ($id == ''): ?>
					<option value="<?php echo $id;?>" <?php echo $selected;?>>All</option>
				<?php else: ?>
					<option value="<?php echo $name;?>" <?php echo $selected;?>><?php echo $name; ?></option>
				<?php endif; ?>
			<?php endforeach; ?>
		<?php else: ?>
			<option value="">None added to system yet</option>
		<?php endif; ?>
	</select>
</div>