<div class="row">
	<div class="col-xs-12">

		<div class="ecommerce-order">

			<div class="row">

				<div class="col-xs-12">
					<h3 class="mt-0"><?php echo $order['details']['store']['title']; ?> Preorder - #<?php echo $order['details']['confirmationNumber']; ?></h3><br/>
				</div>

				<div class="col-xs-6 col-sm-4">
					<strong>First Name:</strong> <?php echo $order['details']['cust_first_name'];?><br/>
					<strong>Last Name:</strong> <?php echo $order['details']['cust_last_name'];?><br/>
					<strong>Email:</strong> <?php echo $order['details']['cust_email'];?><br/>
					<strong>Phone:</strong> <?php echo $order['details']['cust_phone'];?><br/>
				</div>

				<div class="col-xs-6 col-sm-4">
					<strong>Order Coordinator:</strong><br/>
					<strong>Date:</strong> <?php echo $order['details']['dateUpdated'];?><br/>
					<strong>Paid With:</strong> <?php echo ucwords($order['details']['payment_method']);?><br/>
				</div>

				<div class="col-xs-6 col-sm-4">
					<strong>Order Number:</strong> <?php echo $order['details']['ecomm_order_id'];?><br/>
					<strong>Confirmation Number:</strong> <?php echo $order['details']['confirmationNumber'];?><br/>
					<?php if ($order['details']['payment_method'] == 'credit'): ?>
						<strong>Stripe Customer ID:</strong> <?php echo $order['details']['stripe_cust_id'];?>
					<?php elseif ($order['details']['payment_method'] == 'paypal'): ?>
						<strong>Paypal Authorization ID:</strong> <?php echo $order['details']['paypal_auth_id'];?><br/>
						<strong>Paypal Payment ID:</strong> <?php echo $order['details']['paypal_pay_id'];?><br/>
						<strong>Paypal Payer ID:</strong> <?php echo $order['details']['paypal_payer_id'];?>
					<?php endif; ?>
				</div>

				<?php if ($order['comments'] != ''): ?>
					<div class="col-xs-12 mt-15">
						<strong>Order Notes:</strong>
						<div class="well">
							<?php echo $order['comments']; ?>
						</div>
					</div>
				<?php endif; ?>
			</div>

			<table class="table mt-30">
				<tr>
					<th>Item</th>
					<th>Size</th>
					<th>Quantity</th>
					<th>Unit Price</th>
					<th>Subtotal</th>
				</tr>
				<?php foreach ($order['cart'] AS $item): ?>
					<tr>
						<td><?php echo $item['product']['title']; ?></td>
						<td><?php echo $item['size']['size']; ?></td>
						<td><?php echo $item['qty']; ?></td>
						<td>$<?php echo $item['size']['price']; ?></td>
						<td>$<?php echo number_format($item['qty'] * $item['size']['price'], 2); ?></td>
					</tr>
				<?php endforeach; ?>
			</table>

			<div class="well">
				<table>
					<tr>
						<td style="width: 100px;"><strong>Subtotal:</strong></td>
						<td> $<?php echo $order['subtotal']; ?></td>
					</tr>
					<tr>
						<td><strong>Tax:</strong></td>
						<td>$<?php echo $order['tax']; ?></td>
					</tr>
					<tr>
						<td><strong>Total:</strong></td>
						<td>$<?php echo $order['total']; ?></td>
					</tr>
					<?php if ($order['details']['refund_amt'] > 0): ?>
					<tr>
						<td class="pt-15"><strong>Refunded:</strong></td>
						<td class="pt-15">($<?php echo $order['details']['refund_amt']; ?>)</td>
					</tr>
					<?php endif; ?>
				</table>
			</div>

			<a href="/admin/ecommerce/orders/<?php echo $order['details']['ecomm_store_id'];?>" class="btn btn-info"><i class="fa fa-arrow-left"></i> Back to Orders</a>
			<?php if ($order['details']['status'] != 'refunded'): ?>
				<a href="/admin/ecommerce/orders/cancel/<?php echo $order['details']['ecomm_order_id']; ?>" class="btn btn-danger"><i class="fa fa-undo"></i> Cancel/Refund</a>
				<?php if ($order['details']['status'] == 'completed' && number_format($order['total'] - $order['details']['refund_amt'], '2', '.', '') > '.50'): ?>
					<a href="#partial-refund" data-toggle="collapse" class="btn btn-warning"><i class="fa fa-dollar"></i> Partial Refund</a>
					<div class="collapse mt-20" id="partial-refund">
						<hr>
						<h4>Partial Refund</h4>
						<?php echo form_open('admin/ecommerce/orders/partial-refund/' . $order['details']['ecomm_order_id']); ?>
						<fieldset>
							<div class="form-group">
								<label class="sr-only" for="refundAmt">Amount (in dollars)</label>
								<div class="input-group">
									<div class="input-group-addon">$</div>
									<input type="text" class="form-control decimal" name="refundAmt" placeholder="Amount (in dollars)">
									<span class="input-group-btn">
										<button type="submit" class="btn btn-success" type="button">Submit</button>
									</span>
								</div>
							</div>
							<span class="help-block">Value must be less than or equal to $<?php echo ($order['details']['refund_amt'] > 0)? number_format($order['total'] - $order['details']['refund_amt'], '2', '.', '') : $order['total'];?> but more than $.50.</span>
						</fieldset>
						</form>
					</div>
				<?php endif; ?>
			<?php endif; ?>
			<?php if ($order['details']['status'] == 'authorized'): ?>
				<a href="/admin/ecommerce/orders/process/<?php echo $order['details']['ecomm_order_id']; ?>" class="btn btn-success"><i class="fa fa-check"></i> Process</a>
			<?php endif; ?>

		</div>
	</div>
</div>