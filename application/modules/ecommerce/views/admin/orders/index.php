<div class="row">
	<div class="col-xs-12">

		<div class="table-responsive">
			<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered datatable" width="100%" data-url="/admin/ecommerce/datatables/orders/<?php echo $store['ecomm_store_id'];?>">
				<thead>
					<tr>
						<th data-class="expand">Date</th>
						<th>First Name</th>
						<th>Last Name</th>
						<th data-hide="phone, tablet">Email</th>
						<th data-hide="phone">Confirmation #</th>
						<th data-hide="phone">Payment Method</th>
						<th>Status</th>
						<th>Actions</th>
					</tr>
				</thead>

				<tbody>
				</tbody>
			</table>
		</div>

	</div>
</div>