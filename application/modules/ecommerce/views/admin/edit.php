<div class="row">
	<div class="col-xs-12">
		<?php echo form_open('/admin/ecommerce/edit/' . $store['ecomm_store_id']); ?>
		<fieldset>
			<?php $this->load->view('../modules/ecommerce/partials/admin/form');?>
			<?php
				//submit button
				$this->form_builder->button('submit', 'Edit Store', array('inputClass' => 'btn btn-info'), '', '');
			?>
		</fieldset>
		</form>
	</div>
</div>