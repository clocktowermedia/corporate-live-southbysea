<div class="row">
	<div class="col-xs-12">
		<div class="box box-primary">
			<div class="box-header" data-toggle="tooltip" title="" data-original-title="Actions">
				<h3 class="box-title">Shortcuts:</h3>
				<div class="box-tools pull-right">
					<a href="/admin/ecommerce/products/create/<?php echo $store['ecomm_store_id'];?>"><button class="btn btn-info btn-sm"><i class="fa fa-plus"></i> Add Product</button></a>
					<a href="/admin/ecommerce/products/reorder/<?php echo $store['ecomm_store_id'];?>"><button class="btn btn-info btn-sm"><i class="fa fa-arrows"></i> Re-arrange Products</button></a>
					<a href="/preorder/<?php echo $store['url_slug'];?>" target="_blank"><button class="btn btn-info btn-sm"><i class="fa fa-globe"></i> Visit Store</button></a>
				</div>
			</div>

			<div class="box-body table-responsive no-pad-top">
				<hr class="no-pad-top">
				<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered datatable" width="100%" data-url="/admin/ecommerce/datatables/products/<?php echo $store['ecomm_store_id'];?>">
					<thead>
						<tr>
							<th data-class="expand">Title</th>
							<th data-hide="phone">Sizes</th>
							<th data-hide="phone">Base Price</th>
							<th>Actions</th>
						</tr>
					</thead>

					<tbody>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>