<div class="row">
	<div class="col-xs-12">

		<h4>Drag and drop items below to re-arrange them. Click <a href="/admin/ecommerce/products/<?php echo $store['ecomm_store_id'];?>">here</a> to go back to the product list.</h4>
		<br/>

		<div id="product-images" data-id="<?php echo $store['ecomm_store_id'];?>">
			<?php if ($products != false && count($products) > 0): ?>
				<div class="row">
					<?php foreach ($products as $product): ?>
						<div class="col-md-2 col-sm-4 col-xs-6 thumbnail-img draggable" id="products-<?php echo $product['ecomm_product_id'];?>">
							<div class="thumbnail text-center">
								<?php if (isset($product['image']['thumbFullpath']) && $product['image']['thumbFullpath'] != ''): ?>
									<img src="<?php echo $product['image']['thumbFullpath']; ?>" class="img-responsive">
								<?php else: ?>
									<?php echo $product['title']; ?>
								<?php endif; ?>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			<?php else: ?>
				<p>No products for this store yet.</p>
			<?php endif; ?>
		</div>

	</div>
</div>