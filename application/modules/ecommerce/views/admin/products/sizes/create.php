<div class="row">
	<div class="col-xs-12">
		<div class="form-horizontal">
			<?php echo form_open_multipart('/admin/ecommerce/products/sizes/create/' . $product['ecomm_product_id']); ?>
			<fieldset>
				<?php $this->load->view('../modules/ecommerce/partials/admin/product-size');?>
				<?php
					//submit button
					$this->form_builder->button('submit', 'Create Product Size', array('containerClass' => 'col-sm-offset-1 col-sm-11', 'inputClass' => 'btn btn-info'), '', '');
				?>
			</fieldset>
			</form>
		</div>
	</div>
</div>