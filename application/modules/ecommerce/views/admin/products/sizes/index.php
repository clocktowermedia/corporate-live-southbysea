<div class="row">
	<div class="col-xs-12">

		<!--
		<div class="box box-primary">
			<div class="box-header" data-toggle="tooltip" title="" data-original-title="Actions">
				<h3 class="box-title">Shortcuts:</h3>
				<div class="box-tools pull-right">
					<a href="/admin/ecommerce/products/sizes/create/<?php //echo $product['ecomm_product_id'];?>"><button class="btn btn-info btn-sm"><i class="fa fa-plus"></i> Add Size</button></a>
				</div>
			</div>

			<div class="box-body table-responsive no-pad-top">
				<hr class="no-pad-top">
				<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered datatable" width="100%" data-url="/admin/ecommerce/datatables/sizes/<?php echo $product['ecomm_product_id'];?>">
					<thead>
						<tr>
							<th data-class="expand">Size</th>
							<th>Price</th>
							<th>Actions</th>
						</tr>
					</thead>

					<tbody>
					</tbody>
				</table>
			</div>
		</div>
		-->

		<h4>Drag and drop sizes below to re-arrange them. Click <a href="/admin/ecommerce/products/<?php echo $product['ecomm_store_id'];?>">here</a> to go back to the product list.</h4>
		<br/>

		<div id="product-sizes-container" data-id="<?php echo $product['ecomm_product_id'];?>">
		</div>

		<script id="product-sizes-listing" type="x-handlebars-template">
			{{#if sizes}}
				<div class="row">
					{{#each sizes}}
						<div class="col-md-2 col-sm-4 col-xs-6 thumbnail-img draggable" id="sizes-{{ecomm_product_size_id}}">
							<div class="thumbnail text-center">
								{{size}}<br/>
								${{price}}<br/>
								<p class="mt-15">
									<a data-toggle="ajax-modal" title="Edit Product Size" href="/admin/ecommerce/products/sizes/edit/{{ecomm_product_size_id}}?modal=y" class="btn btn-default"><i class="fa fa-pencil"></i> Edit</a>
									<a data-confirm="Are you sure you want to delete this?" href="/admin/ecommerce/products/sizes/delete/{{ecomm_product_size_id}}" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</a>
								</p>
							</div>
						</div>
					{{/each}}
				</div>
			{{else}}
				<p>No sizes for this product yet.</p>
			{{/if}}
		</script>

		<hr/>

		<div id="add-product-sizes">
			<h4>Add Sizes</h4>

			<?php echo form_open('', array('class' => 'form-horizontal', 'id' => 'add-sizes-form', 'data-default-price' => $product['price'])); ?>
				<div id="add-size-container">
				</div>

				<?php
					$this->form_builder->hidden('qty', 0, '');
					$this->form_builder->hidden('ecomm_product_id', $product['ecomm_product_id'], '');
					$this->form_builder->button('submit', 'Add All Sizes', array('containerClass' => 'col-sm-12', 'inputClass' => 'btn btn-info btn-block'), '', '');
				?>
			</form>

			<script id="add-size-template" type="x-handlebars-template">
				<div class="row product-size-row">
					<div class="col-xs-5">
						<div class="form-group">
							<label for="size{{number}}" class="col-xs-1 control-label">Size: </label>
							<div class="col-xs-11">
								<input type="text" name="size{{number}}" class="form-control">
							</div>
						</div>
					</div>

					<div class="col-xs-5">
						<div class="form-group">
							<label for="price{{number}}" class="col-xs-1 control-label">Price: </label>
							<div class="col-xs-11">
								<input type="text" name="price{{number}}" class="form-control monetary" value="{{price}}">
							</div>
						</div>
					</div>

					<div class="col-xs-2 text-right">
						<a class="add-product-size btn btn-info"><i class="fa fa-plus"></i> Add Another Size</a>
					</div>
				</div>
			</script>

		</div>

	</div>
</div>