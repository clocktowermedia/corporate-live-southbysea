<div class="row">
	<div class="col-xs-12">

		<?php if (isset($acct_mgr)):?>
			<?php if ($acct_mgr['adminID'] == $_SESSION['adminID']): ?>
				<?php $name = 'yourself'; ?>
			<?php else: ?>
				<?php $name = $acct_mgr['firstName'] . ' ' . $acct_mgr['lastName']; ?>
			<?php endif; ?>
			<h4>You are viewing stores assigned to <?php echo $name; ?>. If you'd like to view another's account manager's stores, please select their name in the list below:</h4>
		<?php else: ?>
			<h4>You are currently viewing all stores closing soon. If you'd like to filter this list, please select an account manager from the list below:</h4>
		<?php endif; ?>

		<?php echo form_open('admin/ecommerce/closing', array('id' => 'store-closing-form', 'class' => 'mb-30'));?>
			<?php
				$this->form_builder->set_labels(false);
				$this->form_builder->select('acct_mgr_id', '', array('' => 'Any') + $managers, (isset($acct_mgr['adminID']))? $acct_mgr['adminID'] : '', '', '', '', array('required' => 'required'));
			?>
		</form>

		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#soon" data-toggle="tab">Closing Soon</a></li>
				<li><a href="#closed" data-toggle="tab">Already Closed</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="soon">

					<?php if (
						(isset($stores['soon']['met_requirement']) && count($stores['soon']['met_requirement']) > 0) ||
						(isset($stores['soon']['behind_requirement']) && count($stores['soon']['behind_requirement']) > 0)
					):?>
						<p>Select the checkbox next to each store whose orders you would like to process.</p>

						<?php echo form_open('admin/ecommerce/process', array('class' => 'ignore-validation')); ?>
							<div class="row">

								<?php if (isset($stores['soon']['met_requirement']) && count($stores['soon']['met_requirement']) > 0): ?>
									<div class="col-sm-6">
										<div class="panel panel-success">
											<div class="panel-heading"><h4>Stores that met production requirements</h4></div>
											<div class="panel-body">
												<?php foreach ($stores['soon']['met_requirement'] AS $store): ?>
													<div class="checkbox">
														<label>
															<input name="stores[]" type="checkbox" value="<?php echo $store['ecomm_store_id']; ?>"> <?php echo $store['title'];?> (Minimum: <?php echo $store['minimum']; ?>, Sold: <?php echo $store['total']; ?>)
														</label>
														<a class="pull-right" href="/admin/ecommerce/edit/<?php echo $store['ecomm_store_id'];?>">Edit Store</a>
													</div>
												<?php endforeach; ?>
											</div>
										</div>
									</div>
								<?php endif; ?>

								<?php if (isset($stores['soon']['behind_requirement']) && count($stores['soon']['behind_requirement']) > 0): ?>
									<div class="col-sm-6">
										<div class="panel panel-danger">
											<div class="panel-heading"><h4>Stores that did not meet production requirements</h4></div>
											<div class="panel-body">
												<?php foreach ($stores['soon']['behind_requirement'] AS $store): ?>
													<div class="checkbox">
														<label>
															<input name="stores[]" type="checkbox" value="<?php echo $store['ecomm_store_id']; ?>"> <?php echo $store['title'];?> (Minimum: <?php echo $store['minimum']; ?>, Sold: <?php echo $store['total']; ?>)
														</label>
														<a class="pull-right" href="/admin/ecommerce/edit/<?php echo $store['ecomm_store_id'];?>">Edit Store</a>
													</div>
												<?php endforeach; ?>
											</div>
										</div>
									</div>
								<?php endif; ?>

							</div>

							<button type="submit" class="btn btn-default btn-block"><i class="fa fa-check"></i> Process Orders</button>
						</form>

					<?php else: ?>
						<p>No stores closing soon, or all stores closing soon have already had their orders processed.</p>
					<?php endif; ?>

				</div>

				<div class="tab-pane" id="closed">

					<?php if (
						(isset($stores['closed']['met_requirement']) && count($stores['closed']['met_requirement']) > 0) ||
						(isset($stores['closed']['behind_requirement']) && count($stores['closed']['behind_requirement']) > 0)
					):?>
						<p>Select the checkbox next to each store whose orders you would like to process.</p>

						<?php echo form_open('admin/ecommerce/process', array('class' => 'ignore-validation')); ?>
							<div class="row">

								<?php if (isset($stores['closed']['met_requirement']) && count($stores['closed']['met_requirement']) > 0): ?>
									<div class="col-sm-6">
										<div class="panel panel-success">
											<div class="panel-heading"><h4>Stores that met production requirements</h4></div>
											<div class="panel-body">
												<?php foreach ($stores['closed']['met_requirement'] AS $store): ?>
													<div class="checkbox">
														<label>
															<input name="stores[]" type="checkbox" value="<?php echo $store['ecomm_store_id']; ?>"> <?php echo $store['title'];?> (Minimum: <?php echo $store['minimum']; ?>, Sold: <?php echo $store['total']; ?>)
														</label>
														<a class="pull-right" href="/admin/ecommerce/edit/<?php echo $store['ecomm_store_id'];?>">Edit Store</a>
													</div>
												<?php endforeach; ?>
											</div>
										</div>
									</div>
								<?php endif; ?>

								<?php if (isset($stores['closed']['behind_requirement']) && count($stores['closed']['behind_requirement']) > 0): ?>
									<div class="col-sm-6">
										<div class="panel panel-danger">
											<div class="panel-heading"><h4>Stores that did not meet production requirements</h4></div>
											<div class="panel-body">
												<?php foreach ($stores['closed']['behind_requirement'] AS $store): ?>
													<div class="checkbox">
														<label>
															<input name="stores[]" type="checkbox" value="<?php echo $store['ecomm_store_id']; ?>"> <?php echo $store['title'];?> (Minimum: <?php echo $store['minimum']; ?>, Sold: <?php echo $store['total']; ?>)
														</label>
														<a class="pull-right" href="/admin/ecommerce/edit/<?php echo $store['ecomm_store_id'];?>">Edit Store</a>
													</div>
												<?php endforeach; ?>
											</div>
										</div>
									</div>
								<?php endif; ?>

							</div>

							<button type="submit" class="btn btn-default btn-block"><i class="fa fa-check"></i> Process Orders</button>
						</form>

					<?php else: ?>
						<p>No closed stores awaiting order processing.</p>
					<?php endif; ?>

				</div>

			</div>
		</div>

	</div>
</div>