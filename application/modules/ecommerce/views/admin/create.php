<div class="row">
	<div class="col-xs-12">
		<?php echo form_open('/admin/ecommerce/create'); ?>
		<fieldset>
			<?php $this->load->view('../modules/ecommerce/partials/admin/form');?>
			<?php
				//submit button
				$this->form_builder->button('submit', 'Create Store', array('inputClass' => 'btn btn-info'), '', '');
			?>
		</fieldset>
		</form>
	</div>
</div>