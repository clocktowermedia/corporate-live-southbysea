<div class="text-center">
    <?php if (isset($categories) && count($categories) > 0): ?>
        <?php foreach ($categories as $category): ?>
            <div class="col-sm-6 category">
                <?php if (isset($category['image']['fullpath']) && $category['image']['fullpath'] != ''): ?>
                    <img src="<?php echo $category['image']['fullpath'];?>" alt="<?php echo $category['title']; ?>">
                <?php else: ?>
                    <img src="/assets/uploads/images/tumblr_lcckvcUM2f1qake9so1_500.jpg" alt="<?php echo $category['title']; ?>">
                <?php endif;?>
                <h4 class="category-title">
                	<a href="/products/<?php echo $category['url'];?>" title="<?php echo $category['title']; ?>"><?php echo $category['title']; ?></a>
                </h4>
            </div>
        <?php endforeach; ?>
    <?php else: ?>
        <h2 class="mt-0">Sorry! No categories have been set up yet. Please be sure to check back later!</h2>
    <?php endif; ?>
</div>