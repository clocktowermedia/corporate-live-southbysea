<div class="col-md-6 text-center">

	<?php if (strpos($_SERVER[REQUEST_URI], 'fraternity')): ?>


		<?php if (isset($product['image']['fullpath']) && $product['image']['fullpath'] != ''): ?>
	        <img id="main-img" src="<?php echo $product['image']['fullpath']; ?>" alt="<?php echo $product['title']; ?>" title="<?php echo $product['title']; ?>" class="main-product-img">
	        <div id="default-img" data-img="<?php echo $product['image']['fullpath']; ?>" data-alt="<?php echo $product['title']; ?>"></div>
	    <?php else: ?>
	        <img id="main-img" src="/assets/images/prod-img-lg.jpg" alt="<?php echo $product['title']; ?>" title="<?php echo $product['title']; ?>" class="main-product-img">
	        <div id="default-img" data-img="/assets/images/prod-img-lg.jpg" data-alt="<?php echo $product['title']; ?>"></div>
	    <?php endif; ?>
	    <?php if (isset($product['model_image']['fullpath']) && $product['model_image']['fullpath'] != ''): ?>
	        <a href="#model-image" data-toggle="modal" title="View Model Image">View Model Image</a>
	        <!--
	        <div class="form-group">
	            <a href="#model-image" title="View Model Image" data-toggle="modal"><button type="button" class="btn btn-info pull-none">Model Image</button></a>
	        </div>
	        -->
	        <div class="modal fade" id="model-image" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	            <div class="modal-dialog">
	                <div class="modal-content">
	                    <div class="modal-header">
	                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                        <h3 class="modal-title" id="myModalLabel">Model Image</h3>
	                    </div>
	                    <div class="modal-body text-center">
	                        <img src="<?php echo $product['model_image']['fullpath']; ?>" alt="<?php echo $product['title']; ?> - Model Image" title="<?php echo $product['title']; ?> - Model Image">
	                    </div>
	                    <div class="modal-footer">
	                        <div class="form-group">
	                            <a href="" title="Close" data-dismiss="modal"><button type="button" class="btn btn-info pull-none">Close</button></a>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    <?php endif; ?>
	</div><!-- /.col-md-6 -->

	<div class="col-md-6" style="font-family: Times;">
	    <h3 class="item-title" style="font-size:27px; font-family: Northwest Bold;"><?php echo $product['title']; ?></h3></br></br>
	    <?php if ($product['longDesc'] != ''): ?>
				<h4 style="font-size:25px; opacity: 0.75;"> product specs</h4>
	        <small><?php echo $product['longDesc']; ?></small>
	    <?php endif; ?>
		</br><h4 style="font-size:25px; opacity: 0.75;"> colors</h4>
	    <?php if (!empty($colors) && $colors != false && count($colors) > 0): ?>

	        <?php if (isset($defaultColor)): ?>
	            <small><span id="color-label">color selected: <span id="color-selected"><?php echo $defaultColor['title']; ?></span></span></small>
	        <?php else: ?>
	            <small><span id="color-label" class="hide">color selected: <span id="color-selected"></span></span></small>
	        <?php endif; ?>

	        <ul id="colors">
	            <?php foreach ($colors as $color): ?>
	                <?php $fullImg = (isset($color['product_image']['fullpath']) && $color['product_image']['fullpath'] != '')? $color['product_image']['fullpath'] : ''; ?>
	                <?php $active = (isset($defaultColor) && $defaultColor['productColorID'] == $color['productColorID'])? 'active' : ''; ?>

	                <?php if ($color['hexCode'] != ''): ?>
	                    <li class=" <?php echo $active; ?>" data-img="<?php echo $fullImg; ?>" data-alt="<?php echo $color['title'];?>" data-toggle="tooltip" data-placement="top" title="<?php echo $color['title'];?>"><span style="background: <?php echo $color['hexCode']; ?>" class="color-swatch"></span></li>
	                <?php elseif ($color['thumbFullpath'] != ''): ?>
	                    <li class="<?php echo $active; ?>" data-img="<?php echo $fullImg; ?>" data-alt="<?php echo $color['title'];?>" data-toggle="tooltip" data-placement="top" title="<?php echo $color['title'];?>">
	                        <img src="<?php echo $color['thumbFullpath']; ?>" alt="<?php echo $color['title'];?>" title="<?php echo $color['title'];?>" class="color-swatch-image">
	                    </li>
	                <?php endif; ?>
	            <?php endforeach; ?>
	        </ul>
	    <?php endif; ?>
		</br><h4 style="font-size:20px; opacity: 0.75;"> Get a quote with this design</h4>
	    <?php echo form_open_multipart('/forms/quote-request', array('id' => 'proof-request-form', 'class' => 'persist', 'accept-charset' => 'UTF-8')); ?>
	    <fieldset>
	        <?php
	            //add the form
	            $this->load->view('../../forms/partials/forms/quote-request');

	            //submit button
	            $this->form_builder->button('submit', '', array('inputClass' => 'frat-special btn btn-info'), '', '');
	        ?>
	    </fieldset>
	    </form>



	<?php else: ?>



		<?php if (isset($product['image']['fullpath']) && $product['image']['fullpath'] != ''): ?>
	        <img id="main-img" src="<?php echo $product['image']['fullpath']; ?>" alt="<?php echo $product['title']; ?>" title="<?php echo $product['title']; ?>" class="main-product-img">
	        <div id="default-img" data-img="<?php echo $product['image']['fullpath']; ?>" data-alt="<?php echo $product['title']; ?>"></div>
	    <?php else: ?>
	        <img id="main-img" src="/assets/images/prod-img-lg.jpg" alt="<?php echo $product['title']; ?>" title="<?php echo $product['title']; ?>" class="main-product-img">
	        <div id="default-img" data-img="/assets/images/prod-img-lg.jpg" data-alt="<?php echo $product['title']; ?>"></div>
	    <?php endif; ?>
	    <?php if (isset($product['model_image']['fullpath']) && $product['model_image']['fullpath'] != ''): ?>
	        <a href="#model-image" data-toggle="modal" title="View Model Image">View Model Image</a>
	        <!--
	        <div class="form-group">
	            <a href="#model-image" title="View Model Image" data-toggle="modal"><button type="button" class="btn btn-info pull-none">Model Image</button></a>
	        </div>
	        -->
	        <div class="modal fade" id="model-image" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	            <div class="modal-dialog">
	                <div class="modal-content">
	                    <div class="modal-header">
	                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                        <h3 class="modal-title" id="myModalLabel">Model Image</h3>
	                    </div>
	                    <div class="modal-body text-center">
	                        <img src="<?php echo $product['model_image']['fullpath']; ?>" alt="<?php echo $product['title']; ?> - Model Image" title="<?php echo $product['title']; ?> - Model Image">
	                    </div>
	                    <div class="modal-footer">
	                        <div class="form-group">
	                            <a href="" title="Close" data-dismiss="modal"><button type="button" class="btn btn-info pull-none">Close</button></a>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    <?php endif; ?>
	</div><!-- /.col-md-6 -->

	<div class="col-md-6">
	    <h3 class="item-title" style="font-size:30px;"><?php echo $product['title']; ?></h3>
		    <?php if ($product['longDesc'] != ''): ?>
				</br>
		        <h4 style="font-size:22px; opacity: 0.75;"> product specs</h4>
		        <span><small><?php echo $product['longDesc']; ?></small></span>
		    <?php endif; ?>
			</br>
	    <h4 style="font-size:22px; opacity: 0.75;"> colors</h4>
	    <?php if (!empty($colors) && $colors != false && count($colors) > 0): ?>

	        <?php if (isset($defaultColor)): ?>
	            <small><span id="color-label">color selected: <span id="color-selected"><?php echo $defaultColor['title']; ?></span></span></small>
	        <?php else: ?>
	            <small><span id="color-label" class="hide">color selected: <span id="color-selected"></span></span></small>
	        <?php endif; ?>

	        <ul id="colors">
	            <?php foreach ($colors as $color): ?>
	                <?php $fullImg = (isset($color['product_image']['fullpath']) && $color['product_image']['fullpath'] != '')? $color['product_image']['fullpath'] : ''; ?>
	                <?php $active = (isset($defaultColor) && $defaultColor['productColorID'] == $color['productColorID'])? 'active' : ''; ?>

	                <?php if ($color['hexCode'] != ''): ?>
	                    <li class=" <?php echo $active; ?>" data-img="<?php echo $fullImg; ?>" data-alt="<?php echo $color['title'];?>" data-toggle="tooltip" data-placement="top" title="<?php echo $color['title'];?>"><span style="background: <?php echo $color['hexCode']; ?>" class="color-swatch"></span></li>
	                <?php elseif ($color['thumbFullpath'] != ''): ?>
	                    <li class="<?php echo $active; ?>" data-img="<?php echo $fullImg; ?>" data-alt="<?php echo $color['title'];?>" data-toggle="tooltip" data-placement="top" title="<?php echo $color['title'];?>">
	                        <img src="<?php echo $color['thumbFullpath']; ?>" alt="<?php echo $color['title'];?>" title="<?php echo $color['title'];?>" class="color-swatch-image">
	                    </li>
	                <?php endif; ?>
	            <?php endforeach; ?>
	        </ul>
	    <?php endif; ?>
		</br>
	    <h4 style="font-size:20px; opacity: 0.75;">Get a quote with this design</h4>
	    <?php echo form_open_multipart('/forms/quote-request', array('id' => 'proof-request-form', 'class' => 'persist', 'accept-charset' => 'UTF-8')); ?>
	    <fieldset>
	        <?php
	            //add the form
	            $this->load->view('../../forms/partials/forms/quote-request');

	            //submit button
	            $this->form_builder->button('submit', ' ', array('inputClass' => 'btn btn-info'), '', '');
	        ?>
	    </fieldset>
	    </form>
	<?php endif; ?>
</div>
