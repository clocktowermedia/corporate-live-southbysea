<div class="row">
	<div class="col-xs-12">

		<p>Please ensure that all data in the "Product" tab is valid before attempting to add images/colors. Clicking on the "Images" or "Colors" tab will automatically submit anything in the "Product" tab.</p>

		<?php
			//add the form
			$this->load->view('../partials/admin/form');
		?>

		<a class="btn btn-info" id="submit-product-form-btn">Create Product</a>
	</div>
</div>