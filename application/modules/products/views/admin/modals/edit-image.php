<div class="modal-body">
	<?php if (!isset($image) || $image == false || empty($image)): ?>
		<div class="alert alert-danger">
			Invalid image selected. Please close this popup window and try again.
		</div>
	<?php else: ?>
		<?php echo form_open('admin/products/edit-image/' . $image['productImageID'], array('id' => 'image-modal-form', 'class' => 'form-horizontal', 'data-id' => $image['productImageID'])); ?>
		<fieldset>
			<?php
				$containerArray = array('labelClass' => 'col-sm-1', 'containerClass' => 'col-sm-11');

				//color
				$this->form_builder->select('productColorID', 'Color: ', $colors, (isset($image['productColorID']))? $image['productColorID'] : '', $containerArray, '', '', array('required' => 'required'));

				//submit button
				$this->form_builder->button('submit', 'Update Image', array('containerClass' => 'col-sm-offset-1 col-sm-11', 'inputClass' => 'btn btn-info'), '', '');
			?>
		</fieldset>
		</form>
	<?php endif; ?>
</div>