<div class="modal-body">
	<?php if (!isset($color) || $color == false || empty($color)): ?>
		<div class="alert alert-danger">
			Invalid color selected. Please close this popup window and try again.
		</div>
	<?php else: ?>
		<?php echo form_open('admin/products/edit-color/' . $color['productColorID'], array('id' => 'color-modal-form', 'class' => 'form-horizontal', 'data-id' => $color['productColorID'])); ?>
		<fieldset>
			<?php
				$containerArray = array('labelClass' => 'col-sm-1', 'containerClass' => 'col-sm-11');

				//title
				$this->form_builder->text('title', 'Title:', (isset($color['title']))? $color['title'] : '', $containerArray, '', '', '', array('required' => 'required'));

				//submit button
				$this->form_builder->button('submit', 'Update Color', array('containerClass' => 'col-sm-offset-1 col-sm-11', 'inputClass' => 'btn btn-info'), '', '');
			?>
		</fieldset>
		</form>
	<?php endif; ?>
</div>