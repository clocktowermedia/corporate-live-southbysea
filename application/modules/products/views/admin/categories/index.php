<div class="row">
	<div class="col-xs-12">

		<div class="table-responsive">
			<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered datatable" width="100%">
				<thead>
					<tr>
						<th data-class="expand">Title</th>
						<th data-hide="phone">Parent Category</th>
						<th data-hide="phone, tablet">Date Updated</th>
						<th>Actions</th>
					</tr>
				</thead>

				<tbody>
				</tbody>
			</table>
		</div>

	</div>
</div>