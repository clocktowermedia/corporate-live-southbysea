<div class="row">
	<div class="col-xs-12">
		<div class="box box-primary">
			<div class="box-header" data-toggle="tooltip" title="" data-original-title="Actions">
				<h3 class="box-title">Reorder Featured Products</h3>
				<div class="box-tools pull-right">
					<button class="btn btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
					<button class="btn btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
				</div>
			</div>

			<div class="box-body no-pad-top">
				<hr class="no-pad-top">

				<div id="featured-product-images">
					<?php if ($featuredProducts != false && count($featuredProducts) > 0): ?>
						<div class="row">
							<?php foreach ($featuredProducts as $product): ?>
								<div class="col-md-2 col-sm-4 col-xs-6 thumbnail-img" id="products-<?php echo $product['productID'];?>">
									<?php if ($product['thumbFullpath'] != ''): ?>
										<img src="<?php echo $product['thumbFullpath']; ?>" class="img-responsive">
									<?php else: ?>
										<?php echo $product['title']; ?>
									<?php endif; ?>
								</div>
							<?php endforeach; ?>
						</div>
					<?php else: ?>
						<p>No featured products in this category yet.</p>
					<?php endif; ?>
				</div>

			</div>
		</div>

		<div class="box box-primary">
			<div class="box-header" data-toggle="tooltip" title="" data-original-title="Actions">
				<h3 class="box-title">Reorder Products</h3>
				<div class="box-tools pull-right">
					<button class="btn btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
					<button class="btn btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
				</div>
			</div>

			<div class="box-body no-pad-top">
				<hr class="no-pad-top">

				<div id="product-images">
					<?php if ($products != false && count($products) > 0): ?>
						<div class="row">
							<?php foreach ($products as $product): ?>
								<div class="col-md-2 col-sm-4 col-xs-6 thumbnail-img" id="products-<?php echo $product['productID'];?>">
									<?php if ($product['thumbFullpath'] != ''): ?>
										<img src="<?php echo $product['thumbFullpath']; ?>" class="img-responsive">
									<?php else: ?>
										<?php echo $product['title']; ?>
									<?php endif; ?>
								</div>
							<?php endforeach; ?>
						</div>
					<?php else: ?>
						<p>No products in this category yet.</p>
					<?php endif; ?>
				</div>

			</div>
		</div>
		
	</div>
</div>