<div class="row">
	<div class="col-xs-12">

		<?php echo form_open_multipart('admin/products/categories/edit/' . $category['productCategoryID']); ?>
		<fieldset>
			<?php
				//add the form
				$this->load->view('../partials/admin/category-form');

				//submit button
				$this->form_builder->button('submit', 'Edit Product Category', array('inputClass' => 'btn btn-info'), '', '');
			?>
		</fieldset>
		</form>

	</div>
</div>