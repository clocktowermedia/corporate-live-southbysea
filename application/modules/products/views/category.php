<?php if (strpos($_SERVER[REQUEST_URI], 'fraternity')): ?>
	<div class="col-md-3 col-lg-3 col-sm-3">
		<div class="sidebar">
			<div class="pull-right">
				<!-- <?php if (strpos($_SERVER[REQUEST_URI], 'fraternity')): ?>
				<?php else: ?>
				<?php endif; ?> -->
				<!-- <form action="/products/<?php echo $category['url']?>" method="get" id="search-form" class="header_search">
						<label for="filters"></label>
						<input type="text" name="filters" value="<?php echo $this->input->post('q', TRUE); ?>" placeholder="filter &#xF002;" style=" opacity: 0.55;outline: none; border-color: transparent; border: none; background: url(/assets/images/search-icon.png) 195px 2px no-repeat;
	; font-family:Helvetica Neue, FontAwesome" class="text-right">
				</form> -->
			</div>
			<div class="clear-fix"></div>
			<h3 class="" style = "font-size:calc(15px + 1.5vw);margin-left:-70px;font-family:NORTHWEST Bold; color: #879FAA;text-transform:uppercase;"><?php echo $category['title'];?></h3>
			<?php if (isset($categories) && count($categories) > 0): ?>
				<ul class="categories" style = "font-family:NORTHWEST Bold; font-size:calc(5px + .65vw); text-transform:uppercase;">
					<?php foreach ($categories as $subCategory): ?>
						<?php if (strpos($_SERVER[REQUEST_URI], 'fraternity')): ?>
							<li>
								<a href="/fraternity/products/<?php echo $category['url'];?>/<?php echo $subCategory['url'];?>" title="<?php echo $subCategory['title']; ?>">
									<?php echo $subCategory['title']; ?>
								</a>
							</li>
						<?php else: ?>
							<li>
								<a href="/products/<?php echo $category['url'];?>/<?php echo $subCategory['url'];?>" title="<?php echo $subCategory['title']; ?>">
									<?php echo $subCategory['title']; ?>
								</a>
							</li>
						<?php endif; ?>
					<?php endforeach; ?>
				</ul>
			<?php endif; ?>
		</div>
	</div><!-- /.col-md-3 -->

	<div class="col-md-9 col-lg-9 col-sm-9">

		<div class="col-lg-12 col-md-12 col-sm-12 hidden-xs" style = "padding-right:0px;padding-left:0px;">
			<div class="col-lg-7 col-md-7 col-sm-7" style = "padding-right:0px;padding-left:0px;">
				<img class =" fraternity-slideshow" src="/assets/images/NewFSlideshow1-2.jpg" style = "max-height:260px;">
			</div>
			<div class="col-lg-5 col-md-5 col-sm-5 image-wrapper" style = "padding-right:0px;padding-left:0px;">
				<!-- <img class =" fraternity-slideshow" src="/assets/images/NewFSlideshow1-1.jpg" style = " "> -->
				<img class =" fraternity-slideshow" src="/assets/images/NewFBackground.jpg" style = "max-height:260px;">
				<div style="letter-spacing:5px; font-size:calc(8px + 1.0vw); font-family:NORTHWEST Bold; text-transfrom: uppercase;" class="top-right pull-right hidden-sm">DOn't See What</br></br><span style="">yOu need?</span></div>
				<div style="letter-spacing:5px; font-size:calc(4px + .7vw); margin-top: -20px; margin-right:-10px;  font-family:NORTHWEST Bold; text-transfrom: uppercase;"class="top-right pull-right hidden-lg hidden-md">Don't See What</br><span style="">you need?</span></div>
				<div style="font-family:Campground Regular; font-size:calc(18px + 1.25vw);top:60%;" class="centered">Email Us</div>
			</div>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12" style = "padding-right:0px;padding-left:0px;">
			<br style="height:10px;">
		</div>

		<?php if (isset($categories) && count($categories) > 0): ?>
			<?php $count = 0; ?>
			<?php foreach ($categories as $subCategory): ?>
				<?php $count++; ?>

				<?php if ($count % 3 == 1): ?>
					<div class="row mt-0">
				<?php endif; ?>
				<?php if (strpos($_SERVER[REQUEST_URI], 'fraternity')): ?>
					<div class="col-sm-4">
						<div class="product">
							<h4>
								<a style="font-family:Northwest Bold; text-transform:uppercase;" href="/fraternity/products/<?php echo $category['url'];?>/<?php echo $subCategory['url'];?>" title="<?php echo $subCategory['title']; ?>">
									<?php echo $subCategory['title']; ?>
								</a>
								<i class="arrow-dbl-left"></i>
							</h4>
							<?php if (isset($subCategory['image']['fullpath'])): ?>
								<a style="font-family:Northwest Bold; text-transform:uppercase;"  href="/fraternity/products/<?php echo $category['url'];?>/<?php echo $subCategory['url'];?>" title="<?php echo $subCategory['title']; ?>">
									<img src="<?php echo $subCategory['image']['fullpath']; ?>" alt="<?php echo $subCategory['title']; ?>" title="<?php echo $subCategory['title']; ?>">
								</a>
							<?php else: ?>
								<a style="font-family:Northwest Bold; text-transform:uppercase;"  href="/fraternity/products/<?php echo $category['url'];?>/<?php echo $subCategory['url'];?>" title="<?php echo $subCategory['title']; ?>">
									<img src="/assets/images/sm-pl-hld.jpg" alt="<?php echo $subCategory['title']; ?>" title="<?php echo $subCategory['title']; ?>">
								</a>
							<?php endif; ?>
						</div>
					</div>
				<?php else: ?>
					<div class="col-sm-4">
						<div class="product">
							<h4>
								<a href="/products/<?php echo $category['url'];?>/<?php echo $subCategory['url'];?>" title="<?php echo $subCategory['title']; ?>">
									<?php echo $subCategory['title']; ?>
								</a>
								<i class="arrow-dbl-left"></i>
							</h4>
							<?php if (isset($subCategory['image']['fullpath'])): ?>
								<a href="/products/<?php echo $category['url'];?>/<?php echo $subCategory['url'];?>" title="<?php echo $subCategory['title']; ?>">
									<img src="<?php echo $subCategory['image']['fullpath']; ?>" alt="<?php echo $subCategory['title']; ?>" title="<?php echo $subCategory['title']; ?>">
								</a>
							<?php else: ?>
								<a href="/products/<?php echo $category['url'];?>/<?php echo $subCategory['url'];?>" title="<?php echo $subCategory['title']; ?>">
									<img src="/assets/images/sm-pl-hld.jpg" alt="<?php echo $subCategory['title']; ?>" title="<?php echo $subCategory['title']; ?>">
								</a>
							<?php endif; ?>
						</div>
					</div>
				<?php endif; ?>

				<?php if ($count % 3 == 0): ?>
					</div>
				<?php endif; ?>
			<?php endforeach; ?>
		<?php else: ?>
			<h3 class="mt-0">Sorry! There are no categories for this section yet. Please be sure to check back later!</h3>
		<?php endif; ?>
	</div><!-- /.col-md-9 -->
	</div><!-- yes this is supposed to be here -->



<?php else: ?>



	<div class="col-lg-3 col-md-3 col-sm-3">
		<div class="sidebar">
			<div class="pull-right">
				<!-- <?php if (strpos($_SERVER[REQUEST_URI], 'fraternity')): ?>
				<?php else: ?>
				<?php endif; ?> -->
				<!-- <form action="/products/<?php echo $category['url']?>" method="get" id="search-form" class="header_search">
						<label for="filters"></label>
						<input type="text" name="filters" value="<?php echo $this->input->post('q', TRUE); ?>" placeholder="filter &#xF002;" style=" opacity: 0.55;outline: none; border-color: transparent; border: none; background: url(/assets/images/search-icon.png) 195px 2px no-repeat;
	; font-family:Helvetica Neue, FontAwesome" class="text-right">
				</form> -->
			</div>
			<div class="clear-fix"></div>
			<h3 class="wgt-title" style = "font-size:100px;margin-left:-140px;"><?php echo $category['title'];?></h3>
			<?php if (isset($categories) && count($categories) > 0): ?>
				<ul class="categories" style = "font-size:calc(10px + .5vw); margin-top:-22px;">
					<?php foreach ($categories as $subCategory): ?>
						<?php if (strpos($_SERVER[REQUEST_URI], 'fraternity')): ?>
							<li>
								<a href="/fraternity/products/<?php echo $category['url'];?>/<?php echo $subCategory['url'];?>" title="<?php echo $subCategory['title']; ?>">
									<?php echo $subCategory['title']; ?>
								</a>
							</li>
						<?php else: ?>
							<li>
								<a href="/products/<?php echo $category['url'];?>/<?php echo $subCategory['url'];?>" title="<?php echo $subCategory['title']; ?>">
									<?php echo $subCategory['title']; ?>
								</a>
							</li>
						<?php endif; ?>
					<?php endforeach; ?>
				</ul>
			<?php endif; ?>
		</div>
	</div><!-- /.col-md-3 -->

	<div class="col-lg-9 col-md-9 col-sm-9" id="results-container">

		<div class="col-lg-12 col-md-12 col-sm-12 hidden-xs" style = "padding-right:0px;padding-left:0px;">
			<div class="col-lg-7 col-md-7 col-sm-7" style = "padding-right:0px;padding-left:0px;">
				<img class =" fraternity-slideshow" src="/assets/slideshow/2 - Medium Top Right.jpg" style = "max-height:260px;">
			</div>
			<div class="col-lg-5 col-md-5 col-sm-5 image-wrapper" style = "padding-right:0px;padding-left:0px;">
				<!-- <img class =" fraternity-slideshow" src="/assets/images/NewFSlideshow1-1.jpg" style = " "> -->
				<img class =" fraternity-slideshow" src="/assets/images/NewSBackground.jpg" style = "max-height:260px;">
				<div class="top-right pull-right hidden-md hidden-sm" style="font-family:Hello Stockholm Alt; letter-spacing:0px; font-size:calc(62px + .90vw);right:10px;">don't see what you need</div>
				<div class="top-right pull-right hidden-lg  hidden-sm" style="font-family:Hello Stockholm Alt; letter-spacing:0px; font-size:calc(44px + 1.55vw);right:10px;">don't see what you need</div>
				<div class="top-right pull-right hidden-lg  hidden-md" style="font-family:Hello Stockholm Alt; letter-spacing:0px; font-size:calc(34px + 1.55vw);right:10px;top:20%;">don't see what you need</div>
				<div style="font-size:calc(5px + 2vw);top:45%;left:40%;" class="centered">Email Us</div>
			</div>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12" style = "padding-right:0px;padding-left:0px;">
			<br style="height:10px;">
		</div>		<?php if (isset($categories) && count($categories) > 0): ?>
			<?php $count = 0; ?>
			<?php foreach ($categories as $subCategory): ?>
				<?php $count++; ?>

				<?php if ($count % 3 == 1): ?>
					<div class="row mt-0">
				<?php endif; ?>
				<?php if (strpos($_SERVER[REQUEST_URI], 'fraternity')): ?>
					<div class="col-sm-4">
						<div class="product">
							<h4>
								<a href="/fraternity/products/<?php echo $category['url'];?>/<?php echo $subCategory['url'];?>" title="<?php echo $subCategory['title']; ?>">
									<?php echo $subCategory['title']; ?>
								</a>
								<i class="arrow-dbl-left"></i>
							</h4>
							<?php if (isset($subCategory['image']['fullpath'])): ?>
								<a href="/fraternity/products/<?php echo $category['url'];?>/<?php echo $subCategory['url'];?>" title="<?php echo $subCategory['title']; ?>">
									<img src="<?php echo $subCategory['image']['fullpath']; ?>" alt="<?php echo $subCategory['title']; ?>" title="<?php echo $subCategory['title']; ?>">
								</a>
							<?php else: ?>
								<a href="/fraternity/products/<?php echo $category['url'];?>/<?php echo $subCategory['url'];?>" title="<?php echo $subCategory['title']; ?>">
									<img src="/assets/images/sm-pl-hld.jpg" alt="<?php echo $subCategory['title']; ?>" title="<?php echo $subCategory['title']; ?>">
								</a>
							<?php endif; ?>
						</div>
					</div>
				<?php else: ?>
					<div class="col-sm-4">
						<div class="product">
							<h4>
								<a href="/products/<?php echo $category['url'];?>/<?php echo $subCategory['url'];?>" title="<?php echo $subCategory['title']; ?>">
									<?php echo $subCategory['title']; ?>
								</a>
								<i class="arrow-dbl-left"></i>
							</h4>
							<?php if (isset($subCategory['image']['fullpath'])): ?>
								<a href="/products/<?php echo $category['url'];?>/<?php echo $subCategory['url'];?>" title="<?php echo $subCategory['title']; ?>">
									<img src="<?php echo $subCategory['image']['fullpath']; ?>" alt="<?php echo $subCategory['title']; ?>" title="<?php echo $subCategory['title']; ?>">
								</a>
							<?php else: ?>
								<a href="/products/<?php echo $category['url'];?>/<?php echo $subCategory['url'];?>" title="<?php echo $subCategory['title']; ?>">
									<img src="/assets/images/sm-pl-hld.jpg" alt="<?php echo $subCategory['title']; ?>" title="<?php echo $subCategory['title']; ?>">
								</a>
							<?php endif; ?>
						</div>
					</div>
				<?php endif; ?>

				<?php if ($count % 3 == 0): ?>
					</div>
				<?php endif; ?>
			<?php endforeach; ?>
		<?php else: ?>
			<h3 class="mt-0">Sorry! There are no categories for this section yet. Please be sure to check back later!</h3>
		<?php endif; ?>
	</div><!-- /.col-md-9 -->
	</div><!-- yes this is supposed to be here -->
<?php endif; ?>
