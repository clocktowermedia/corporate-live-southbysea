<?php
/********************************************************************************************************
/
/ Product Images
/
********************************************************************************************************/
class Product_image_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'ProductImages';

	//primary key
	public $primary_key = 'productImageID';

	//relationships
	public $has_single = array(
		'product' => array('model' => 'products/product_model', 'primary_key' => 'productID', 'join_key' => 'productID'),
		'color' => array('model' => 'products/product_color_model', 'primary_key' => 'productColorID', 'join_key' => 'productColorID')
	);

	//callbacks/observers (use function names)

	//validation
	public $validate = array(
		'productID' => array(
			'field' => 'productID',
			'label' => 'Associated Product',
			'rules' => 'required|is_natural_no_zero'
		),
		'filename' => array(
			'field' => 'filename',
			'label' => 'Filename',
			'rules' => 'required'
		),
		'filepath' => array(
			'field' => 'filepath',
			'label' => 'Filepath',
			'rules' => 'required'
		),
		'fullpath' => array(
			'field' => 'fullpath',
			'label' => 'Fullpath',
			'rules' => 'required'
		),
	);

	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/

	/**
	* Used to change the order in which images are displayed based on drag/drop re-arranging done by user
	*/
	public function reorder($images = null)
	{
		//make sure there are images to loop through
		if (!is_null($images))
		{
			//determine the total number of images
			$total_images = count($images);

			//loop through each gallery and update the order in the db
			for ($i = 0; $i < $total_images; $i++)
			{
				$this->db->where($this->primary_key, $images[$i])
					->set('displayOrder', $i)
					->update($this->_table);
			}

			return true;
		}

		return false;
	}

	/**********************************************************************************************
	*
	* Validity checker functions & helper functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
}