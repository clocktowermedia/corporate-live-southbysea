<?php
/********************************************************************************************************
/
/ Product Color
/
********************************************************************************************************/
class Product_color_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'ProductColors';

	//primary key
	public $primary_key = 'productColorID';

	//relationships
	public $has_single = array(
		'product' => array('model' => 'products/product_model', 'primary_key' => 'productID', 'join_key' => 'productID'),
		'product_image' => array('model' => 'products/product_image_model', 'primary_key' => 'productImageID')
	);

	//callbacks/observers (use function names)

	//validation
	public $validate = array(
		'productID' => array(
			'field' => 'productID',
			'label' => 'Associated Product',
			'rules' => 'required|is_natural_no_zero'
		)
	);

	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/

	/**
	* Used to change the order in which images are displayed based on drag/drop re-arranging done by user
	*/
	public function reorder($images = null)
	{
		//make sure there are images to loop through
		if (!is_null($images))
		{
			//determine the total number of images
			$total_images = count($images);

			//loop through each gallery and update the order in the db
			for ($i = 0; $i < $total_images; $i++)
			{
				$this->db->where($this->primary_key, $images[$i])
					->set('displayOrder', $i)
					->update($this->_table);
			}

			return true;
		}

		return false;
	}

	/**
	 * Gets a color dropdown w/ hex code and images, used in form builder
	 * @param  integer $productID [description]
	 * @return array
	 */
	public function getOptionsForDropdown($productID = 0)
	{
		//get colors
		if ($productID > 0)
		{
			$colors = $this->get_many_by('productID', (int) $productID);
		} else {
			$colors = $this->get_all();
		}

		//set empty options array
		$options = array();

		//loop through and format
		if (!empty($colors) && count($colors) > 0)
		{
			foreach ($colors as $color)
			{
				$options[$color[$this->primary_key]] = array('text' => $color['title'], 'attributes' => array('data-hex' => $color['hexCode'], 'data-image' => $color['thumbFullpath']));
			}
		}

		return $options;
	}

	/**********************************************************************************************
	*
	* Validity checker functions & helper functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
}