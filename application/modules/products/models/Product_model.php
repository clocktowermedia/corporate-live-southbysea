<?php
/********************************************************************************************************
/
/ Products
/ Explanation: Products
/
********************************************************************************************************/
class Product_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'Products';

	//primary key
	public $primary_key = 'productID';

	//relationships
	public $has_single = array(
		'category' => array('model' => 'products/product_category_model', 'primary_key' => 'productCategoryID', 'join_key' => 'productID'),
		'image' => array('model' => 'products/product_image_model', 'primary_key' => 'productImageID', 'join_key' => 'productID', 'where' => array('defaultImg' => '1')),
		'model_image' => array('model' => 'products/product_image_model', 'primary_key' => 'productImageID', 'join_key' => 'productID', 'where' => array('modelImg' => '1'))
	);

	public $has_many = array(
		'images' => array('model' => 'products/product_image_model', 'primary_key' => 'productImageID', 'join_key' => 'productID'),
		'tags' => array('model' => 'products/product_tag_model', 'primary_key' => 'productTagID', 'join_key' => 'productID'),
		'colors' => array('model' => 'products/product_color_model', 'primary_key' => 'productColorID', 'join_key' => 'productID'),
	);

	//callbacks/observers (use function names)
	public $before_create = array('_create_timestamp', '_uppercase_tags');
	public $before_update = array('_create_update_timestamp', '_uppercase_tags');

	//validation
	public $validate = array(
		'title' => array(
			'field' => 'title',
			'label' => 'Title',
			'rules' => 'required|min_length[2]'
		),
		'url' => array(
			'field' => 'url',
			'label' => 'URL',
			'rules' => 'required|seo_url'
		),
		'productCategoryID' => array(
			'field' => 'productCategoryID',
			'label' => 'Category',
			'rules' => 'required|is_natural_no_zero'
		),
		'status' => array(
			'field' => 'status',
			'label' => 'Status',
			'rules' => 'required'
		)
	);

	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/

	/**
	* Soft delete of entry from db by id - change status to deleted
	*/
	function soft_delete($productID = 0)
	{
		//make sure product id is valid
		if ($productID > 0)
		{
			$this->db->where($this->primary_key, $productID)->set('status', 'deleted')->update($this->_table);
		}

		return false;
	}

	/**
	 * Get products by category id
	 * @param  integer $categoryID
	 * @param  boolean $featured   [return featured products only?]
	 * @return array
	 */
	function getByCategory($categoryID = 0, $featured = false)
	{
		//make sure category id is valid
		if ($categoryID > 0)
		{
			//load model we need
			$this->load->model('products/product_image_model');

			//determine if we are reordering featured or not
			$reorderField = ($featured == true)? 'p.displayOrderFeatured' : 'p.displayOrder';

			//start defining query
			$this->db->select('p.*')
				->select('pi.fullpath')
				->select('pi.thumbFullpath')
				->from($this->_table . ' AS p')
				->join($this->product_image_model->_table . ' as pi', 'pi.productID = p.productID', 'left outer')
				->where('p.productCategoryID', $categoryID)
				->where('p.status !=', 'deleted');

			//only select featured categories if applicable
			if ($featured == true)
			{
				$this->db->where('p.featuredInCategory', (int) $featured);
			}

			$results = $this->db->order_by($reorderField)
				->group_by('p.productID')
				->get()
				->result_array();

			return $results;
		}

		return false;
	}

	/**
	* Used to change the order in which products are displayed based on drag/drop re-arranging done by user
	*/
	function reorder(array $products = null, $featured = false)
	{
		//mke sure data passed is valid
		if (!is_null($products) && count($products) > 0)
		{
			//determine the total number of products
			$totalProducts = count($products);

			//determine if we are reordering featured or not
			$reorderField = ($featured == true)? 'displayOrderFeatured' : 'displayOrder';

			//loop through each gallery and update the order in the db
			for ($i = 0; $i < $totalProducts; $i++)
			{
				$this->db->where($this->primary_key, $products[$i])
					->set($reorderField, $i)
					->update($this->_table);
			}

			return true;
		}

		return false;
	}

	/**
	 * Returns products that match what the user is typing
	 * @param  string  $query  [search term]
	 * @param  integer $offset
	 * @param  integer $limit
	 * @return array
	 */
	function search($query = '', $limit = 20, $offset = 0)
	{
		//make sure we have a valid search term
		if (!is_null($query) && $query != '')
		{

			$results = array();

			//load model to get tablename(s) that we will need
			$this->load->model($this->has_single['category']['model']);
			$this->load->model($this->has_many['tags']['model'], 'tag_model');
			$this->load->model($this->has_many['colors']['model'], 'color_model');
			$this->load->model($this->has_single['image']['model'], 'image_model');

			//start query
			$this->db->select('p.*')
				->select('c.url AS categoryUrl')
				->select('pc.url AS parentCategoryUrl')
				->select('i.thumbFullpath AS image');

			//if there are spaces in the search query, loop through each term
			if (strrpos($query, ',') !== false)
			{
				$terms = explode(',', $query);
				foreach ($terms as $term)
				{
					if (strlen($term) >= 3)
					{
						$termArray[] = '(p.tags LIKE "%' . $term . '%" || t.tag LIKE "%' . $term . '%")';
					}
				}
				$termString = implode(' OR ', $termArray);

				$this->db->where('(' . $termString . ')')
					->from($this->_table . ' AS p')
					->join($this->product_category_model->_table . ' AS c', 'c.productCategoryID = p.productCategoryID')
					->join($this->product_category_model->_table . ' AS pc', 'pc.productCategoryID = c.parentID')
					->join($this->tag_model->_table . ' AS t', 't.productID = p.productID', 'left outer')
					->join($this->color_model->_table . ' AS co', 'co.productID = p.productID', 'left outer')
					->join($this->image_model->_table . ' AS i', 'i.productID = p.productID AND i.defaultImg = "1"', 'left outer');

				//add limit/offset
				if ($limit > 0)
				{
					$this->db->limit($limit, $offset);
				}

				//get/return results
				$results1 = $this->db->where('p.status', 'enabled')
					->group_by('p.' . $this->primary_key)->order_by('dateUpdated','desc')->get()->result_array();
			}

			//load model to get tablename(s) that we will need
			$this->load->model($this->has_single['category']['model']);
			$this->load->model($this->has_many['tags']['model'], 'tag_model');
			$this->load->model($this->has_many['colors']['model'], 'color_model');
			$this->load->model($this->has_single['image']['model'], 'image_model');

			//start query
			$this->db->select('p.*')
				->select('c.url AS categoryUrl')
				->select('pc.url AS parentCategoryUrl')
				->select('i.thumbFullpath AS image');

			//if there are spaces in the search query, loop through each term
			if (strrpos($query, ',') !== false)
			{
				$terms = explode(',', $query);
				foreach ($terms as $term)
				{
					if (strlen($term) >= 3)
					{
						$termArray[] = '(p.tags LIKE "%' . $term . '%" || t.tag LIKE "%' . $term . '%")';
					}
				}
				$termString = implode(' OR ', $termArray);
			} else {
				if (strrpos($query, ' ') !== false)
				{
					$terms = explode(' ', $query);
					foreach ($terms as $term)
					{
						if (strlen($term) >= 3)
						{
							$termArray[] = '(p.tags LIKE "%' . $term . '%" || t.tag LIKE "%' . $term . '%")';
						}
					}
					$termString = implode(' OR ', $termArray);
				} else {
					$termString = '(p.tags LIKE "%' . $query . '%" || t.tag LIKE "%' . $query . '%")';
				}
			}

			$this->db->where('(' . $termString . ')')
				->from($this->_table . ' AS p')
				->join($this->product_category_model->_table . ' AS c', 'c.productCategoryID = p.productCategoryID')
				->join($this->product_category_model->_table . ' AS pc', 'pc.productCategoryID = c.parentID')
				->join($this->tag_model->_table . ' AS t', 't.productID = p.productID', 'left outer')
				->join($this->color_model->_table . ' AS co', 'co.productID = p.productID', 'left outer')
				->join($this->image_model->_table . ' AS i', 'i.productID = p.productID AND i.defaultImg = "1"', 'left outer');

			//add limit/offset
			if ($limit > 0)
			{
				$this->db->limit($limit, $offset);
			}

			//get/return results
			$results2 = $this->db->where('p.status', 'enabled')
				->group_by('p.' . $this->primary_key)->order_by('dateUpdated','desc')->get()->result_array();

			//load model to get tablename(s) that we will need
			$this->load->model($this->has_single['category']['model']);
			$this->load->model($this->has_many['tags']['model'], 'tag_model');
			$this->load->model($this->has_many['colors']['model'], 'color_model');
			$this->load->model($this->has_single['image']['model'], 'image_model');

			//start query
			$this->db->select('p.*')
				->select('c.url AS categoryUrl')
				->select('pc.url AS parentCategoryUrl')
				->select('i.thumbFullpath AS image');

			//if there are spaces in the search query, loop through each term
			if (strrpos($query, ',') !== false)
			{
				$terms = explode(',', $query);
				foreach ($terms as $term)
				{
					if (strlen($term) >= 3)
					{
						$termArray[] = '(c.title LIKE "%' . $term . '%" || p.title LIKE "%' . $term . '%" || co.title LIKE "%' . $term . '%")';
					}
				}
				$termString = implode(' OR ', $termArray);
			} else {
				// if (strrpos($query, ' ') !== false)
				// {
					$terms = explode(' ', $query);
					foreach ($terms as $term)
					{
						if (strlen($term) >= 3)
						{
							$termArray[] = '(c.title LIKE "%' . $term . '%" || p.title LIKE "%' . $term . '%" || co.title LIKE "%' . $term . '%")';
						}
					}
					$termString = implode(' OR ', $termArray);
				// } else {
				// 	$termArray[] = '(c.title LIKE "%' . $term . '%" || p.title LIKE "%' . $term . '%" || co.title LIKE "%' . $term . '%")';
				// }
			}

			$this->db->where('(' . $termString . ')')
				->from($this->_table . ' AS p')
				->join($this->product_category_model->_table . ' AS c', 'c.productCategoryID = p.productCategoryID')
				->join($this->product_category_model->_table . ' AS pc', 'pc.productCategoryID = c.parentID')
				->join($this->tag_model->_table . ' AS t', 't.productID = p.productID', 'left outer')
				->join($this->color_model->_table . ' AS co', 'co.productID = p.productID', 'left outer')
				->join($this->image_model->_table . ' AS i', 'i.productID = p.productID AND i.defaultImg = "1"', 'left outer');

			//add limit/offset
			if ($limit > 0)
			{
				$this->db->limit($limit, $offset);
			}

			//get/return results
			$results3 = $this->db->where('p.status', 'enabled')
				->group_by('p.' . $this->primary_key)->order_by('dateUpdated','desc')->get()->result_array();

			$final_array = array();
			$results4 = array();

			foreach($results1 as $results){
				$object = new StdClass();
				$object = json_decode(json_encode($results),FALSE);
				if(!isset($final_array[$object->url])){
					$final_array[$object->url]=$object;
					array_push($results4,$object);
				}
			}
			foreach($results2 as $results){
				$object = new StdClass();
				$object = json_decode(json_encode($results),FALSE);
				if(!isset($final_array[$object->url])){
					$final_array[$object->url]=$object;
					array_push($results4,$object);
				}
			}
			foreach($results3 as $results){
				$object = new StdClass();
				$object = json_decode(json_encode($results),FALSE);
				if(!isset($final_array[$object->url])){
					$final_array[$object->url]=$object;
					array_push($results4,$object);
				}
			}

			return $results4;
		}
		return false;
	}

	/**
	 * Only grab information relevant for the sitemap xml
	 * @return array
	 */
	function getSitemapXmlInfo()
	{
		//load category model
		$this->load->model($this->has_single['category']['model'], 'product_category_model');
		$productCategoryTable = 'ProductCategories';
		$joinKey = $this->has_single['category']['primary_key'];

		//define query
		return $this->db->select('p.dateUpdated')
			->select('CONCAT("/products/", pc.url, "/", c.url, "/", p.url) AS link', false)
			->where('p.status', 'enabled')
			->from($this->_table . ' AS p')
			->join($productCategoryTable . ' AS c', "c.$joinKey = p.$joinKey")
			->join($productCategoryTable . ' AS pc', "pc.$joinKey = c.parentID")
			->get()
			->result_array();
	}

	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
	protected function _create_timestamp($data)
	{
		$data['dateCreated'] = $data['dateUpdated'] = date('Y-m-d H:i:s');
		return $data;
	}

	protected function _create_update_timestamp($data)
	{
		$data['dateUpdated'] = date('Y-m-d H:i:s');
		return $data;
	}

	protected function _uppercase_tags($data)
	{
		if (isset($data['tags']) && $data['tags'])
		{
			$data['tags'] = implode(',', array_map('ucwords', explode(',', $data['tags'])));
		}
		return $data;
	}
}
