<?php
/********************************************************************************************************
/
/ Product Categories
/
********************************************************************************************************/
class Product_category_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'ProductCategories';

	//primary key
	public $primary_key = 'productCategoryID';

	//relationships
	public $has_single = array(
		'image' => array('model' => 'products/product_category_image_model', 'primary_key' => 'productCategoryImageID', 'join_key' => 'productCategoryImageID'),
	);

	public $belongs_to = array(
		'parent' => array('model' => 'products/product_category_model', 'primary_key' => 'parentID')
	);

	public $has_many = array(
		'products' => array('model' => 'products/product_model', 'primary_key' => 'productCategoryID', 'join_key' => 'productID')
	);

	//callbacks/observers (use function names)
	public $before_create = array('_create_timestamp');
	public $before_update = array('_create_update_timestamp');

	//validation
	public $validate = array(
		'title' => array(
			'field' => 'title',
			'label' => 'Title',
			'rules' => 'required|min_length[2]'
		),
		'url' => array(
			'field' => 'url',
			'label' => 'URL',
			'rules' => 'required|seo_url'
		)
	);

	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/

	/**
	* Soft delete of entry from db by id - change status to deleted
	*/
	function soft_delete($categoryID = 0)
	{
		//make sure product id is valid
		if ($categoryID > 0)
		{
			$this->db->where($this->primary_key, $categoryID)->set('status', 'deleted')->update($this->_table);
		}

		return false;
	}

	/**
	* Grabs all categories for use in a dropdown selector (if they have more than 2 levels of categories, not currently used)
	*/
	public function getDropdown($maxDepth = 0, $parentID = 0, $options = array(), $exclude = array())
	{
		//get categories
		$categories = $this->getHierarchicalList($parentID, $exclude);

		//add an initial option
		if (!isset($options['']))
		{
			$options[''] = '--- Select a Parent Category ---';
		}

		//loop through categories, add to dropdown, do depth checking and get children
		if ($categories != false && count($categories) > 0)
		{
			foreach ($categories as $category)
			{
				//add to dropdown
				$options[$category[$this->primary_key]] = str_repeat('&nbsp;', ($category['depth'] * 3)) . $category['title'];

				//check to make sure we aren't at max depth
				if ($maxDepth == 0 || $maxDepth > $category['depth'])
				{
					//grab children
					$newOptions = $this->getDropdown($maxDepth, $category[$this->primary_key], $options, $exclude);

					//append
					$options = $options + $newOptions;
				}
			}
		}

		return $options;
	}

	/**
	 * Get the HTML we will be using for re-ordering
	 * @param  integer $maxDepth [max level of depth, ie if you only want three levels of categories]
	 * @param  integer $parentID [which category to start with]
	 * @param  string  $html     [any html you want to start with]
	 * @param  array   $exclude  [category ID's to exclude]
	 * @return string - html string
	 */
	public function getReorderHtml($maxDepth = 0, $parentID = 0, $html = '', $exclude = array())
	{
		//get categories
		$categories = $this->getHierarchicalList($parentID, $exclude);

		//add ol wrapper
		if (count($categories) > 0)
		{
			$html .= ($html != '')? '<ol>' : '<ol class="sortable">';
		}

		//loop through categories, add to dropdown, do depth checking and get children
		if ($categories != false && count($categories) > 0)
		{
			foreach ($categories as $category)
			{
				//add to html
				$html .= '<li id="categories_' . $category[$this->primary_key] . '">
					<div class="sortable-container">
						<div class="reorder-title pull-left">
							<i class="fa fa-arrows"></i> ' .
							$category['title'] .
						'</div>
					</div>';

				//check to make sure we aren't at max depth
				if ($maxDepth == 0 || $maxDepth > $category['depth'])
				{
					//grab children
					$html = $this->getReorderHtml($maxDepth, $category[$this->primary_key], $html, $exclude);
				}

				$html .= '</li>';
			}
		}

		//add ol end wrapper
		$html .= (count($categories) > 0)? '</ol>' : '';

		//return html
		return $html;
	}

	/**
	 * Get two levels of options for dropdown usage
	 * @param  integer $maxDepth
	 * @param  array   $exclude  [id's to exclude]
	 * @return array
	 */
	public function getOptions($maxDepth = 0, $exclude = array())
	{
		$options = array();

		//get first level categories
		$this->db->from($this->_table)->where('status', 'enabled')->where('parentID', 0);
		if (is_array($exclude) && count($exclude) > 0)
		{
			$this->db->where_not_in($this->primary_key, $exclude);
		} else if (!is_array($exclude) && !is_null($exclude) && $exclude != '') {
			$this->db->where($this->primary_key . ' !=', $exclude);
		}
		$categories = $this->db->order_by('displayOrder ASC')->get()->result_array();

		//get their children
		foreach ($categories as $category)
		{
			if ($maxDepth <= 0 || $maxDepth > 1)
			{
				$this->db->from($this->_table)->where('status', 'enabled')->where('parentID', $category[$this->primary_key]);
				if (is_array($exclude) && count($exclude) > 0)
				{
					$this->db->where_not_in($this->primary_key, $exclude);
				} else if (!is_array($exclude) && !is_null($exclude) && $exclude != '') {
					$this->db->where($this->primary_key . ' !=', $exclude);
				}
				$children = $this->db->order_by('displayOrder ASC')->get()->result_array();
				if (count($children) > 0)
				{
					foreach ($children as $child)
					{
						$options[$category[$this->primary_key]]['children'][$child[$this->primary_key]] = $child['title'];
					}
				} else {
					$options[$category[$this->primary_key]]['children'] = array();
				}
			} else {
				$options[$category[$this->primary_key]]['children'] = array();
			}

			$options[$category[$this->primary_key]]['text'] = $category['title'];
		}

		//return array
		return $options;
	}

	/**
	 * Get category list
	 */
	public function getHierarchicalList($parentID = 0, $exclude = array())
	{
		//get categories
		$this->db->from($this->_table)
			->where('status', 'enabled');

		//get by parent id if applicable
		if ($parentID > 0)
		{
			$this->db->where('parentID', $parentID);
		} else {
			$this->db->where('parentID', 0);
		}

		//exclude if applicable
		if (is_array($exclude) && count($exclude) > 0)
		{
			$this->db->where_not_in($this->primary_key, $exclude);

		} else if (!is_array($exclude) && !is_null($exclude) && $exclude != '') {

			$this->db->where($this->primary_key . ' !=', $exclude);
		}

		//get categories as an array
		$categories = $this->db->order_by('displayOrder ASC')
			->get()
			->result_array();

		//return the pages
		return $categories;
	}

	/**
	* Used to change the order in which categories are displayed
	*/
	public function reorder($categories = null)
	{
		//make sure there are pages to loop through
		if (!is_null($categories))
		{
			//loop through each gallery and update the order in the db
			$count = 0;
			foreach ($categories as $key => $value)
			{
				//extract the values that we need
				list($parentID, $depth) = explode(',', $value);

				//increase count
				$count++;

				//call db to update
				$this->db->where($this->primary_key, $key)
					->set('displayOrder', $count)
					->set('depth', $depth)
					->set('parentID', $parentID)
					->update($this->_table);
			}

			return true;
		}

		return false;
	}

	/**
	 * Only grab information relevant for the sitemap xml
	 * @return array
	 */
	function getSitemapXmlInfo()
	{
		//get first level categories
		$firstLevel = $this->db->select('dateUpdated')
			->select('CONCAT("/products/", url) AS link', false)
			->where('parentID', 0)
			->where('status', 'enabled')
			->from($this->_table)
			->get()
			->result_array();

		//get second level categories
		$secondLevel = $this->db->select('c.dateUpdated')
			->select('CONCAT("/products/", pc.url, "/", c.url) AS link', false)
			->where('c.status', 'enabled')
			->from($this->_table . ' AS c')
			->join($this->_table . ' AS pc', "pc.$this->primary_key = c.parentID")
			->get()
			->result_array();

		return array_merge($firstLevel, $secondLevel);
	}

	/**********************************************************************************************
	*
	* Validity checker functions & helper functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
	protected function _create_timestamp($data)
	{
		$data['dateCreated'] = $data['dateUpdated'] = date('Y-m-d H:i:s');
		return $data;
	}

	protected function _create_update_timestamp($data)
	{
		$data['dateUpdated'] = date('Y-m-d H:i:s');
		return $data;
	}
}
