<?php
/********************************************************************************************************
/
/ Product Categories
/
********************************************************************************************************/
class Product_category_image_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'ProductCategoryImages';

	//primary key
	public $primary_key = 'productCategoryImageID';

	//relationships
	public $has_single = array(
		'category' => array('model' => 'products/product_category_model', 'primary_key' => 'productCategoryID', 'join_key' => 'productCategoryID')
	);

	//callbacks/observers (use function names)

	//validation
	public $validate = array(
		'filename' => array(
			'field' => 'filename',
			'label' => 'Filename',
			'rules' => 'required'
		),
		'filepath' => array(
			'field' => 'filepath',
			'label' => 'Filepath',
			'rules' => 'required'
		),
		'fullpath' => array(
			'field' => 'fullpath',
			'label' => 'Fullpath',
			'rules' => 'required'
		),
	);

	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Validity checker functions & helper functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
}