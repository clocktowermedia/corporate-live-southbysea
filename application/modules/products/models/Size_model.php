<?php
/********************************************************************************************************
/
/ Sizes
/ Explanation: Sizes
/
********************************************************************************************************/
class Size_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'Sizes';

	//primary key
	public $primary_key = 'sizeID';

	//relationships

	//callbacks/observers (use function names)

	//validation
	public $validate = array(
		'title' => array(
			'field' => 'title',
			'label' => 'Title',
			'rules' => 'required|min_length[2]'
		),
		'abbrev' => array(
			'field' => 'abbrev',
			'label' => 'Abbreviation',
			'rules' => 'required'
		),
		'required' => array(
			'field' => 'required',
			'label' => 'Required?',
			'rules' => 'required|is_natural'
		),
		'showByDefault' => array(
			'field' => 'showByDefault',
			'label' => 'Show by Default?',
			'rules' => 'required|is_natural'
		),
		'displayOrder' => array(
			'field' => 'displayOrder',
			'label' => 'Display Order',
			'rules' => 'required|is_natural'
		)
	);

	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/

}