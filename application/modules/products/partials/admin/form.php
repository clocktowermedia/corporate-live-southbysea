<div class="nav-tabs-custom">
	<ul class="nav nav-tabs" id="session-form-tabs">
		<li class="active"><a href="#product" data-toggle="tab">Product</a></li>

		<?php
			$data['creating'] = ($this->uri->segment($this->uri->total_segments()) == 'create')? true : false;
			$actionType = ($data['creating'] == true)? 'create' : 'edit';
			$disableClass = ($data['creating'] == true)? 'disabled' : '';
		?>
		<li class="<?php echo $disableClass;?>"><a href="#images" data-toggle="">Images</a></li>
		<li class="<?php echo $disableClass;?>"><a href="#colors" data-toggle="">Colors</a></li>
	</ul>

	<div class="tab-content">

		<div class="tab-pane active" id="product">
			<?php echo form_open('admin/products/' . $formAction, array('id' => 'main-product-form', 'data-action-type' => $actionType, 'data-action' => '/admin/products/json/' . $formAction, 'data-id' => $productID)); ?>
			<fieldset>
				<?php
					//title
					$this->form_builder->text('title', 'Title', (isset($product['title']))? $product['title'] : '', '', '', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));

					//url
					$urlReadOnly = ($data['creating'] == false)? array() : array();
					$this->form_builder->text('url', 'URL', (isset($product['url']))? $product['url'] : '', '', '', 'Alphanumeric characters only. For SEO purposes, use lowercase letters and dashes to indicate spaces.', '', array('pattern' => '^[a-z0-9-]+$', 'required' => 'required') + $urlReadOnly);

					//category
					$this->form_builder->select('productCategoryID', 'Category', $categories, (isset($product['productCategoryID']))? $product['productCategoryID'] : '', '', '', '', array('required' => 'required'), true);
                    $this->form_builder->select('type', 'Type', array('S'=>'Sorority','F'=>'Fraternity','U'=>'Both'), (isset($product['type']))? $product['type'] : '', '', '', '', array('required' => 'required'), true);
?>

					<?php


					//short description
					$this->form_builder->textarea('shortDesc', 'Short Description', (isset($product['shortDesc']))? $product['shortDesc'] : '', array('inputClass' => 'wysiwyg short'), 10, '', '', '', array('required' => 'required'));

					//long description
					$this->form_builder->textarea('longDesc', 'Long Description', (isset($product['longDesc']))? $product['longDesc'] : '', array('inputClass' => 'wysiwyg'), 10, '', '', '', array('required' => 'required'));

					//tags
					$this->form_builder->psuedo_hidden('tags', "Tags (Separate tags using commas or by pressing the 'Enter' key)", (isset($product['tags']))? $product['tags'] : '', '', '', '', '', array('id' => 'tags'));

					//featured product
					$this->form_builder->checkbox('featuredInCategory', 'Yes', 0, 1, (isset($product['featuredInCategory']))? $product['featuredInCategory'] : '', '', 'Featured on Category Page?', '', array());

					//status
					$this->form_builder->select('status', 'Status', array('enabled' => 'enabled', 'disabled' => 'disabled'), (isset($product['status']))? $product['status'] : '', '', '', '', array('required' => 'required'), true);
				?>
			</fieldset>
			</form>
		</div>

		<div class="tab-pane" id="images">

			<div id="add-product-images">
				<h4>Add Images</h4>
				<div id="upload-photos" class="text-center upload-container">
					<h4>Drag &amp; Drop Images Here</h4>
					<h5>-or-</h5>
					<label>
						<input type="button" id="upload-btn" class="btn btn-info btn-large" value="Click to Add Files">
						<input id="upload-trigger" type="file" name="userfile[]" class="hide" multiple="multiple" title="Click to add Files">
					</label>
				</div>
			</div>

			<hr/>

			<div id="edit-product-images">
				<h4>Current Images</h4>

				<div id="product-images">
				</div>

				<script id="product-images-listing" type="x-handlebars-template">
					{{#if images}}
						<div class="row">
							{{#each images}}
								<div class="col-md-1 col-sm-2 col-xs-3 thumbnail-img" id="images-{{productImageID}}">
									<a data-toggle="popover" data-id="{{productImageID}}" data-title="Actions">
										<img src="{{thumbFullpath}}" class="img-responsive">
									</a>
								</div>

								<div id="popover-content-{{productImageID}}" class="popover-content hide">
									{{#equal defaultImg 0}}
										<a data-id="{{productImageID}}" class="btn btn-block btn-info set-as-default"><i class="fa fa-check-square-o"></i> Set as Default</a>
									{{/equal}}
									{{#equal modelImg 0}}
										<a data-id="{{productImageID}}" class="btn btn-block btn-info set-as-model"><i class="fa fa-user"></i>Set as Model Img</a>
									{{/equal}}
									<a href="/admin/products/edit-image/{{productImageID}}" data-id="{{productImageID}}" data-toggle="ajax-modal" class="btn btn-block btn-info edit-image" title="Edit Image"><i class="fa fa-pencil"></i> Edit</a>
									<a data-id="{{productImageID}}" class="btn btn-block btn-info delete-image"><i class="fa fa-trash-o"></i> Delete</a>
								</div>
							{{/each}}
						</div>
					{{else}}
						<p>No images for this product.</p>
					{{/if}}
				</script>
			</div>

			<hr/>

			<a class="btn btn-info btn-block" role="button" data-toggle="collapse" href="#images-table" aria-controls="images-table" aria-expanded="false"><i class="fa fa-image"></i> Edit Images/Images Listing</a><br/>

			<div class="collapse" id="images-table">

				<?php if (isset($product)): ?>
					<div class="well">
						<h4>Actions</h4>
						<a class="btn btn-sm btn-danger hijack-image-table" data-images="all" data-parent-id="<?php echo $product['productID'];?>">Delete All Images</a>
						<a class="btn btn-sm btn-warning hijack-image-table" data-parent-id="<?php echo $product['productID'];?>">Delete Checked Images</a>
					</div>

					<div class="table-responsive">
						<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered datatable" width="100%" id="product-image-table" data-url="/admin/products/datatables/images/<?php echo $product['productID'];?>">
							<thead>
								<tr>
									<th data-hide="phone, tablet">Preview</th>
									<th data-class="expand">File</th>
									<th>Delete?</th>
									<th>Color</th>
								</tr>
							</thead>

							<tbody>
							</tbody>
						</table>
					</div>
				<?php endif; ?>
			</div>

		</div>

		<div class="tab-pane" id="colors">

			<div id="add-product-color-images">
				<h4>Add Color Images</h4>

				<div id="add-product-colors-dragdrop">
					<div id="upload-colors" class="text-center upload-container">
						<h4>Drag &amp; Drop Color Images Here</h4>
						<h5>-or-</h5>
						<label>
							<input type="button" id="upload-btn-colors" class="btn btn-info btn-large" value="Click to Add Images">
							<input id="upload-trigger-colors" type="file" name="userfile[]" class="hide" multiple="multiple" title="Click to add Images">
						</label>
					</div>
				</div>
			</div>

			<hr>

			<div id="add-product-colors">
				<h4>Add Colors w/ Colorpicker</h4>

				<div id="add-colors-form-container">
				</div>

				<script id="add-colors-form" type="x-handlebars-template">
					{{#if qty}}
						<form id="colorpicker-form">
							<div class="row form-horizontal">
								<div class="col-xs-12">
									After pressing submit, you will be able to add more colors with the colorpicker again.<br/><br/>
								</div>
								{{#times qty}}
									<div class="col-sm-4 col-ms-6 col-xs-12 colorpicker-group">
										<div class="form-group">
											<label for="title{{this}}" class="col-xs-2 control-label">Title: </label>
											<div class="col-xs-10">
												<input type="text" name="title{{this}}" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label for="hexCode{{this}}" class="col-xs-2 control-label">Color: </label>
											<div class="col-xs-10">
												<input type="text" name="hexCode{{this}}" class="form-control colorpicker">
											</div>
										</div>
									</div>
								{{/times}}
								<input type="hidden" name="qty" value={{qty}}>
								<input type="hidden" name="productID" value="{{productID}}">
							</div>

							<button type="button" class="btn btn-default" id="submit-color-qty">Submit</button>
						</form>
					{{else}}
						<div id="add-color-quantity" class="form-inline">
							<div class="form-group input-append">
								<label for="colorQuantity">How many colors would you like to add?</label>
								<input type="number" name="colorQuantity" id="colorQuantity" class="form-control">
								<button type="button" class="form-control btn btn-default" id="add-color-qty-btn">Go</button>
							</div>
						</div>
					{{/if}}
				</script>

			</div>

			<hr/>

			<div id="edit-product-colors">
				<h4>Current Colors</h4>

				<div id="product-colors">
				</div>

				<script id="product-colors-listing" type="x-handlebars-template">
					{{#if colors}}
						<div class="row">
							{{#each colors}}
								<div class="col-md-1 col-sm-2 col-xs-3 thumbnail-img" id="images-{{productColorID}}">
									<a data-toggle="popover" data-id="{{productColorID}}" data-title="Actions">
										{{#if hexCode}}
											<span style="background:{{hexCode}};" class="color-swatch-admin"></span>
										{{else}}
											<img src="{{thumbFullpath}}" class="img-responsive">
										{{/if}}
									</a>
								</div>

								<div id="popover-color-content-{{productColorID}}" class="popover-color-content hide">
									<a href="/admin/products/edit-color/{{productColorID}}" data-toggle="ajax-modal" data-id="{{productColorID}}" class="btn btn-block btn-info edit-color" title="Edit Color"><i class="fa fa-pencil"></i> Edit</a>
									<a data-id="{{productColorID}}" class="btn btn-block btn-info delete-color"><i class="fa fa-trash-o"></i> Delete</a>
								</div>
							{{/each}}
						</div>
					{{else}}
						<p>No colors for this product.</p>
					{{/if}}
				</script>
			</div>


			<hr/>

			<a class="btn btn-info btn-block" role="button" data-toggle="collapse" href="#colors-table" aria-controls="images-table" aria-expanded="false"><i class="fa fa-paint-brush"></i> Edit Colors/Colors Listing</a><br/>

			<div class="collapse" id="colors-table">
				<div class="table-responsive">
					<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered datatable" width="100%" id="product-color-table" data-url="/admin/products/datatables/colors/<?php echo $product['productID'];?>">
						<thead>
							<tr>
								<th data-hide="phone, tablet">Preview</th>
								<th data-class="expand">File</th>
								<th>Title</th>
							</tr>
						</thead>

						<tbody>
						</tbody>
					</table>
				</div>
			</div>

		</div>

	</div>
</div>
