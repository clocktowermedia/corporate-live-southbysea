<?php
	//title
	$this->form_builder->text('title', 'Title', (isset($category['title']))? $category['title'] : '', '', '', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));

	//url
	$urlReadOnly = ($this->uri->segment(4) == 'edit')? array() : array();
	$this->form_builder->text('url', 'URL', (isset($category['url']))? $category['url'] : '', '', '', 'Alphanumeric characters only. For SEO purposes, use lowercase letters and dashes to indicate spaces.', '', array('pattern' => '^[a-z0-9-]+$', 'required' => 'required') + $urlReadOnly);

	//description
	$this->form_builder->textarea('shortDesc', 'Short/Meta Description', (isset($category['shortDesc']))? $category['shortDesc'] : '', '', 3, '', '', '', array());

	//meta keywords
	$this->form_builder->textarea('metaKeywords', 'Meta Keywords', (isset($category['metaKeywords']))? $category['metaKeywords'] : '', '', 3, '', 'Product tags will be used instead if there is no entry here', '', array());
	// $this->form_builder->textarea('type', 'Type', (isset($category['type']))? $category['type'] : '', '', 3, '', '', '', array());
	$this->form_builder->select('type', 'Type', array('S'=>'Sorority','F'=>'Fraternity','U'=>'Both'), (isset($category['type']))? $category['type'] : '', '', '', '', array('required' => 'required'), true);


?>



<!-- <fieldset disabled>
	<div class="form-group">
		<label for="disabledSelect" class="control-label ">Site selector:</label><br/>
		<div class = "">
			<select id="disabledSelect" class = "form-control">
					<option value="S">Sorority</option>
					<option value="F">Fraternity</option>
					<option value="U">Both</option>
			</select>
		</div>
	</div>
</fieldset> -->


	<?php if (isset($category['productCategoryImageID']) && $category['productCategoryImageID'] > 0): ?>
		<div class="form-group">
			<label class="control-label" for="userfile">Current Image</label><br/>
			<img src="<?php echo $category['image']['thumbFullpath']; ?>" class="img-thumbnail">
		</div>
	<?php endif; ?>

	<div class="form-group" id="category-image-upload">
		<label class="control-label" for="userfile">Image</label>
		<input type="file" name="userfile" id="userfile" size="20" class="form-control"/>
		<?php if (isset($category['productCategoryImageID']) && $category['productCategoryImageID'] > 0): ?>
			<p class="help-block">If you upload a new image, the image above will be replaced.</p>
		<?php endif; ?>
	</div>

<?php
	//category
	if (count($categories) > 0 && !empty($categories))
	{
		$this->form_builder->select('parentID', 'Parent Category', $categories, (isset($category['parentID']))? $category['parentID'] : '', '', '', '', array());
	}
?>
