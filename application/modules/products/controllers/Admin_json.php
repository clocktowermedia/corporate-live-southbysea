<?php
class Admin_json extends Admin_controller {

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//load relevant models
		$this->load->model('products/product_model');
		$this->load->model('products/product_image_model');
		$this->load->model('products/product_color_model');
		$this->load->model('products/product_tag_model');
	}

	function create()
	{
		//set status to false by default
		$data['status'] = 'fail';

		if ($this->is_post())
		{
			//load form validation library
			$this->load->library('form_validation');
			$validationRules = $this->product_model->validate;

			//set form validation rules
			$validationRules['url']['rules'] .= '|is_unique[' . $this->product_model->_table . '.url]';
			$this->form_validation->set_rules($validationRules);

			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				$data['showMessage'] = true;
				$data['message'] = validation_errors();

			} else {

				//insert product
				$productID = $this->product_model->insert($this->input->post(NULL, TRUE));

				//update tags if applicable
				if ($this->input->post('tags', TRUE) != '')
				{
					$tags = explode(',', $this->input->post('tags', TRUE));
					foreach ($tags as $tag)
					{
						$this->product_tag_model->insert(array(
							'productID' => $productID,
							'tag' => $tag
						));
					}
				}

				//send back data
				$data['status'] = 'ok';
				$data['message'] = 'Successfully created product.';
				$data['productID'] = $productID;
			}

		} else {
			$data['message'] = 'Must post data.';
		}

		$this->render_json($data);
	}

	function edit($productID = 0)
	{
		//set status to false by default
		$data['status'] = 'fail';

		//set product ID to integer
		$productID = (int) $productID;

		if ($productID > 0)
		{
			if ($this->is_post())
			{
				//make sure product was found
				$product = $this->product_model->get($productID);
				if (isset($product['productID']) && $product['productID'] > 0)
				{
					//load form validation library & set form validation rules
					$this->load->library('form_validation');
					$validationRules = $this->product_model->validate;
					$this->form_validation->set_rules($validationRules);

					//make sure form is valid
					if ($this->form_validation->run() == FALSE)
					{
						$data['showMessage'] = true;
						$data['message'] = validation_errors();

					} else if ($this->input->post('url', TRUE) != $product['url']) {

						$data['message'] = 'Product was not updated, an error occurred. Please reload the page and try again.';

					} else {

						//update product
						$this->product_model->update($productID, $this->input->post(NULL, TRUE));

						//update tags if applicable
						if ($this->input->post('tags', TRUE) != '')
						{
							$tags = explode(',', $this->input->post('tags', TRUE));
							$this->product_tag_model->delete_by('productID', $productID);
							foreach ($tags as $tag)
							{
								$this->product_tag_model->insert(array(
									'productID' => $productID,
									'tag' => $tag
								));
							}
						}

						//send back data
						$data['status'] = 'ok';
						$data['message'] = 'Successfully updated product information.';
						$data['productID'] = $productID;
					}

				} else {
					$data['message'] = 'Could not find that product.';
				}

			} else {
				$data['message'] = 'Must post data.';
			}
		} else {
			$data['message'] = 'Invalid product ID.';
		}

		$this->render_json($data);
	}

	function reorder_products($categoryID = 0, $featured = false)
	{
		try
		{
			//get the category
			$categoryID = (int) $categoryID;
			$this->load->model('products/product_category_model');
			$category = $this->product_category_model->get($categoryID);

			//throw exception if category not found
			if (empty($category) || $category == false)
			{
				throw new Exception('Category not found.');
			}

			//make sure we variable we need from post
			if ($this->input->post('products', TRUE) == '')
			{
				throw new Exception('Must reorder products.');
			}

			//try to reorder the products
			$featured = ($featured == 'featured')? true : false;
			$this->product_model->reorder($this->input->post('products', TRUE), $featured);

			//set status message
			$data['status'] = 'ok';
			$data['message'] = 'Successfully saved product order.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}

	function images()
	{
		try
		{
			//make sure we have a valid request
			if (!$this->is_get())
			{
				throw new Exception('Invalid request type.');
			}

			//make sure we have a valid product
			$productID = (int) $this->input->get('id', TRUE);

			//make sure product was found
			$product = $this->product_model->get($productID);
			if (!isset($product['productID']) || $product['productID'] <= 0)
			{
				throw new Exception('Could not find that product.');
			}

			//get images
			$images = $this->product_image_model->order_by('displayOrder')
				->get_many_by('productID', $productID);

			$data['status'] = 'ok';
			$data['message'] = ($images == false || empty($images))? 'No images for this product.' : 'Successfully retrieved product images.';
			$data['images'] = $images;

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}

	function colors()
	{
		try
		{
			//make sure we have a valid request
			if (!$this->is_get())
			{
				throw new Exception('Invalid request type.');
			}

			//make sure we have a valid product
			$productID = (int) $this->input->get('id', TRUE);

			//make sure product was found
			$product = $this->product_model->get($productID);
			if (!isset($product['productID']) || $product['productID'] <= 0)
			{
				throw new Exception('Could not find that product.');
			}

			$term = $this->input->get('q', TRUE);

			//get images
			$colors = $this->product_color_model->select($this->product_color_model->primary_key)
				->select('title')
				->select('hexCode')
				->select('thumbFullpath')
				->where("title LIKE '%$term%'", null)
				->order_by('displayOrder')
				->get_many_by('productID', $productID);

			$data['status'] = 'ok';
			$data['message'] = ($colors == false || empty($colors))? 'No colors for this product.' : 'Successfully retrieved product colors.';
			$data['colors'] = $colors;

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}

	function add_color()
	{
		try
		{
			//make sure we are posting
			if (!$this->is_post())
			{
				throw new Exception('Must post data.');
			}

			//make sure a valid quantity was passed
			$qty = (int) $this->input->post('qty', TRUE);
			if ($qty <= 0)
			{
				throw new Exception('Invalid color quantity.');
			}

			//set product id to int
			$productID = (int) $this->input->post('productID', TRUE);

			//make sure product is found
			$product = $this->product_model->get($productID);
			if (!isset($product['productID']) || $product['productID'] <= 0)
			{
				throw new Exception('Product not found.');
			}

			//loop through colors and save
			for ($i = 1; $i <= $qty; $i++)
			{
				if ($this->input->post('hexCode' . $i, TRUE) != '')
				{
					$this->product_color_model->insert(array(
						'title' => $this->input->post('title' . $i, TRUE),
						'hexCode' => $this->input->post('hexCode' . $i, TRUE),
						'productID' => $productID
					));
				}
			}

			//send back success message
			$data['status'] = 'ok';
			$data['message'] = 'Successfully added product colors.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}

	function upload_color()
	{
		try
		{
			//make sure we are posting
			if (!$this->is_post())
			{
				throw new Exception('Must post data.');
			}

			//set product id to int
			$productID = (int) $this->input->post('productID', TRUE);

			//make sure product is found
			$product = $this->product_model->get($productID);
			if (!isset($product['productID']) || $product['productID'] <= 0)
			{
				throw new Exception('Product not found.');
			}

			//try to upload the image
			$imageID = modules::run('products/admin_image_handler/_upload_image', $this->input->post(NULL, TRUE), 'color');

			//check to see if we got a image uploaded
			if (is_int($imageID) && $imageID > 0)
			{
				//if image upload was successful, return the image id
				$data['status'] = 'ok';
				$data['productColorID'] = $imageID;
				$data['message'] = 'Successfully uploaded color image.';
			} else {
				throw new Exception($imageID);
			}

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}

	function update_color($colorID = 0)
	{
		try
		{
			//make sure we are posting
			if (!$this->is_post())
			{
				throw new Exception('Must post data.');
			}

			//get color
			$colorID = (int) $colorID;
			$color = $this->product_color_model->get($colorID);

			//if color not found
			if (empty($color) || $color == false)
			{
				throw new Exception('Product color not found.');
			}

			//update the color
			$this->product_color_model->skip_validation()->update($colorID, $this->input->post(NULL, TRUE));

			//return status
			$data['status'] = 'ok';
			$data['message'] = 'Successfully updated color.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}

	function delete_color($colorID = 0)
	{
		try
		{
			//get color
			$colorID = (int) $colorID;
			$color = $this->product_color_model->get($colorID);

			//if color not found
			if (empty($color) || $color == false)
			{
				throw new Exception('Product color not found.');
			}

			//delete the color
			$this->product_color_model->delete($colorID);

			//return status
			$data['status'] = 'ok';
			$data['message'] = 'Successfully deleted color.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}

	function upload_image()
	{
		try
		{
			//make sure we are posting
			if (!$this->is_post())
			{
				throw new Exception('Must post data.');
			}

			//set product id to int
			$productID = (int) $this->input->post('productID', TRUE);

			//make sure product is found
			$product = $this->product_model->get($productID);
			if (!isset($product['productID']) || $product['productID'] <= 0)
			{
				throw new Exception('Product not found.');
			}

			//try to upload the image
			$imageID = modules::run('products/admin_image_handler/_upload_image', $this->input->post(NULL, TRUE), 'product');

			//check to see if we got a image uploaded
			if (is_int($imageID) && $imageID > 0)
			{
				//if image upload was successful, return the image id
				$data['status'] = 'ok';
				$data['productImageID'] = $imageID;
				$data['message'] = 'Successfully uploaded image.';
			} else {
				throw new Exception($imageID);
			}

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}

	function update_image($imageID = 0)
	{
		try
		{
			//make sure we are posting
			if (!$this->is_post())
			{
				throw new Exception('Must post data.');
			}

			//get image
			$imageID = (int) $imageID;
			$image = $this->product_image_model->get($imageID);

			//if image not found
			if (empty($image) || $image == false)
			{
				throw new Exception('Product image not found.');
			}

			//update the image
			$this->product_image_model->skip_validation()->update($imageID, $this->input->post(NULL, TRUE));

			//return status
			$data['status'] = 'ok';
			$data['message'] = 'Successfully updated image.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}

	function delete_image($imageID = 0)
	{
		try
		{
			//get image
			$imageID = (int) $imageID;
			$image = $this->product_image_model->get($imageID);

			//if image not found
			if (empty($image) || $image == false)
			{
				throw new Exception('Product image not found.');
			}

			//delete the image
			$this->product_image_model->delete($imageID);

			//return status
			$data['status'] = 'ok';
			$data['message'] = 'Successfully deleted image.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}

	function delete_images()
	{
		try
		{
			//determine if we are deleting all images or not
			$images = $this->input->post('images', TRUE);
			if (is_array($images))
			{
				if (count($images) <= 0)
				{
					throw new Exception('Must select images to delete.');
				}

				foreach ($images AS $imageID)
				{
					$this->product_image_model->delete($imageID);
				}
			} else {
				$this->product_image_model->delete_by('productID', (int) $this->input->post('productID', TRUE));
			}

			//return status
			$data['status'] = 'ok';
			$data['message'] = 'Successfully deleted image.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}

	function set_default_image($imageID = 0)
	{
		try
		{
			//get image
			$imageID = (int) $imageID;
			$image = $this->product_image_model->get($imageID);

			//if image not found
			if (empty($image) || $image == false)
			{
				throw new Exception('Product image not found.');
			}

			//make sure no other images are set as default and set this one to default
			$this->product_image_model->skip_validation()->update_by(array('productID' => $image['productID']), array('defaultImg' => '0'));
			$this->product_image_model->skip_validation()->update($imageID, array('defaultImg' => '1'));

			//return status
			$data['status'] = 'ok';
			$data['message'] = 'Successfully set image as default for product.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}

	function set_model_image($imageID = 0)
	{
		try
		{
			//get image
			$imageID = (int) $imageID;
			$image = $this->product_image_model->get($imageID);

			//if image not found
			if (empty($image) || $image == false)
			{
				throw new Exception('Product image not found.');
			}

			//make sure no other images are set as default and set this one to default
			$this->product_image_model->skip_validation()->update_by(array('productID' => $image['productID']), array('modelImg' => '0'));
			$this->product_image_model->skip_validation()->update($imageID, array('modelImg' => '1'));

			//return status
			$data['status'] = 'ok';
			$data['message'] = 'Successfully set image as model image for product.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}

	function tags()
	{
		//load model we need
		$this->load->model('products/product_tag_model');

		//get all tags
		$tags = $this->product_tag_model->get_unique_tags();

		//output tags
		$this->render_json($tags);
	}

	function editable($type = 'color')
	{
		try
		{
			//make sure valid id was passed
			$id = (int) $this->input->post('pk', TRUE);
			if ($id <= 0)
			{
				throw new Exception('Invalid ID');
			}

			//get model
			$this->load->model('products/product_' . (string) $type . '_model', 'item_model');
			$item = $this->item_model->get($id);

			//if item not found
			if (empty($item) || $item == false)
			{
				throw new Exception('Not found.');
			}

			//update item
			$updateData[(string) $this->input->post('name', TRUE)] = $this->input->post('value', TRUE);
			$this->item_model->skip_validation()->update($id, $updateData);

			//return status
			$data['status'] = 'ok';
			$data['message'] = 'Successfully updated.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/


}