<?php
class Products extends MY_Controller {

	//layout variables
	public $defaultLayout;

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//set layout variable(s)
		$this->defaultLayout = $this->get_default_layout();

		//load relevant models
		$this->load->model('products/product_model');
		$this->load->model('products/product_tag_model');
		$this->load->model('products/product_category_model');
	}

	/**
	* Shows product categories
	*/
	function index()
	{
		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->get_view();
		$data['title'] = 'Products';

		if (strpos($_SERVER[REQUEST_URI], 'fraternity')) {
				//get list of product categories
				$data['categories'] = $this->product_category_model->append('image')->order_by('displayOrder')
					->get_many_by(array(
						'status' => 'enabled',
						'parentID' => 0
					));
		}else{
			//get list of product categories
			$data['categories'] = $this->product_category_model->append('image')->order_by('displayOrder')
				->get_many_by(array(
					'status' => 'enabled',
					'parentID' => 0
				));
		}

		//get product page content for seo purposes
		$data['page'] = modules::run('pages/get', 'products');
		$data['metaKeywords'] = ($data['page']['metaKeywords'] != '')? $data['page']['metaKeywords'] : $this->product_tag_model->get_unique_tags('text');

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}

	/**
	* Shows product category w/ its subcategories
	*/
	function category($categoryUrl = '')
	{
		if (strpos($_SERVER[REQUEST_URI], 'fraternity')) {

			//make sure category is valid
			$categoryUrl = $this->_category_is_valid($categoryUrl);

			//get category info
			$data['category'] = $this->product_category_model->get_by(array(
				'status' => 'enabled',
				'url' => $categoryUrl
			));

			if ($this->input->get('filters', TRUE) != '')
			{
				// $data['designs'] = $this->design_model->getByTags(0, $this->input->get('filters', TRUE), $this->perPage, $offset);
				// $data['totalDesigns'] = $this->design_model->countByTags(0, $this->input->get('filters', TRUE));
				// get subcategories
				$data['categories'] = $this->product_category_model->where('type !=','S')->append('image')->order_by('displayOrder')
					->get_many_by(array(
						'status' => 'enabled',
						'parentID' => $data['category']['productCategoryID']
					));


			} else {
				//get subcategories
				$data['categories'] = $this->product_category_model->where('type !=','S')->append('image')->order_by('displayOrder')
					->get_many_by(array(
						'status' => 'enabled',
						'parentID' => $data['category']['productCategoryID']
					));
			}

			//tell it what view to use, this is always called the view variable
			$data['view'] = $this->get_view();
			$data['title'] = $data['category']['title'];

			//seo stuff
			$data['metaDesc'] = $data['category']['shortDesc'];
			$data['metaKeywords'] = ($data['category']['metaKeywords'] != '')? $data['category']['metaKeywords'] : $this->product_tag_model->get_unique_category_tags($data['category']['productCategoryID'], 'text');

			// var_dump($categoryUrl);

			// tell it what layout to use and load
			$this->load->view($this->defaultLayout, $data);
			// $this->load->view('../modules/products/views/frat-product',$data);
		}else{
			//make sure category is valid
			$categoryUrl = $this->_category_is_valid($categoryUrl);

			//get category info
			$data['category'] = $this->product_category_model->get_by(array(
				'status' => 'enabled',
				'url' => $categoryUrl
			));

			if ($this->input->get('filters', TRUE) != '')
			{
				// $data['designs'] = $this->design_model->getByTags(0, $this->input->get('filters', TRUE), $this->perPage, $offset);
				// $data['totalDesigns'] = $this->design_model->countByTags(0, $this->input->get('filters', TRUE));
				// get subcategories
				$data['categories'] = $this->product_category_model->where('type !=','F')->append('image')->order_by('displayOrder')
					->get_many_by(array(
						'status' => 'enabled',
						'parentID' => $data['category']['productCategoryID']
					));


			} else {
				//get subcategories
				$data['categories'] = $this->product_category_model->where('type !=','F')->append('image')->order_by('displayOrder')
					->get_many_by(array(
						'status' => 'enabled',
						'parentID' => $data['category']['productCategoryID']
					));
			}

			//tell it what view to use, this is always called the view variable
			$data['view'] = $this->get_view();
			$data['title'] = $data['category']['title'];

			//seo stuff
			$data['metaDesc'] = $data['category']['shortDesc'];
			$data['metaKeywords'] = ($data['category']['metaKeywords'] != '')? $data['category']['metaKeywords'] : $this->product_tag_model->get_unique_category_tags($data['category']['productCategoryID'], 'text');

			// var_dump($categoryUrl);

			// tell it what layout to use and load
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	* Shows product subcategory w/ its products
	*/
	function subcategory($categoryUrl = '', $subcategoryUrl = '')
	{
		if (strpos($_SERVER[REQUEST_URI], 'fraternity')) {
			//make sure category & subcategory is valid
			$categoryUrl = $this->_category_is_valid($categoryUrl);
			$subcategoryUrl = $this->_category_is_valid($subcategoryUrl);

			//get category & parent category info
			$data['category'] = $this->product_category_model->append('parent')->get_by(array(
				'status' => 'enabled',
				'url' => $subcategoryUrl
			));

			//get list of product categories
			$data['categories'] = $this->product_category_model->where('type !=','S')->order_by('displayOrder')
				->get_many_by(array(
					'status' => 'enabled',
					'parentID' => $data['category']['parent']['productCategoryID']
				));

			//get products
			$data['products'] = $this->product_model->where('type !=','S')->append('image')->order_by('displayOrder')
				->get_many_by(array(
					'status' => 'enabled',
					'productCategoryID' => $data['category']['productCategoryID']
				));

			//tell it what view to use, this is always called the view variable
			$data['view'] = $this->get_view();
			$data['title'] = $data['category']['title'];
			$data['pageTitle'] = $data['category']['parent']['title'] . ' - ' . $data['category']['title'];

			//seo stuff
			$data['metaDesc'] = $data['category']['shortDesc'];
			$data['metaKeywords'] = ($data['category']['metaKeywords'] != '')? $data['category']['metaKeywords'] : $this->product_tag_model->get_unique_category_tags($data['category']['productCategoryID'], 'text');

			// tell it what layout to use and load
			$this->load->view($this->defaultLayout, $data);
		}else{
			//make sure category & subcategory is valid
			$categoryUrl = $this->_category_is_valid($categoryUrl);
			$subcategoryUrl = $this->_category_is_valid($subcategoryUrl);

			//get category & parent category info
			$data['category'] = $this->product_category_model->append('parent')->get_by(array(
				'status' => 'enabled',
				'url' => $subcategoryUrl
			));

			//get list of product categories
			$data['categories'] = $this->product_category_model->where('type !=','F')->order_by('displayOrder')
				->get_many_by(array(
					'status' => 'enabled',
					'parentID' => $data['category']['parent']['productCategoryID']
				));

			//get products
			$data['products'] = $this->product_model->where('type !=','F')->append('image')->order_by('displayOrder')
				->get_many_by(array(
					'status' => 'enabled',
					'productCategoryID' => $data['category']['productCategoryID']
				));

			//tell it what view to use, this is always called the view variable
			$data['view'] = $this->get_view();
			$data['title'] = $data['category']['title'];
			$data['pageTitle'] = $data['category']['parent']['title'] . ' - ' . $data['category']['title'];

			//seo stuff
			$data['metaDesc'] = $data['category']['shortDesc'];
			$data['metaKeywords'] = ($data['category']['metaKeywords'] != '')? $data['category']['metaKeywords'] : $this->product_tag_model->get_unique_category_tags($data['category']['productCategoryID'], 'text');

			// tell it what layout to use and load
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	 * Product Page
	 * @param  string $categoryUrl
	 * @param  string $subcategoryUrl
	 * @param  string $productUrl
	 */
	function product($categoryUrl = '', $subcategoryUrl = '', $productUrl = '')
	{
		//make sure all are valid
		$categoryUrl = $this->_category_is_valid($categoryUrl);
		$subcategoryUrl = $this->_category_is_valid($subcategoryUrl);
		$productUrl = $this->_product_is_valid($productUrl);

		//get product info
		$data['product'] = $this->product_model->append('image')->append('model_image')->get_by(array(
			'status' => 'enabled',
			'url' => $productUrl
		));

		//get product colors
		$this->load->model('products/product_color_model');
		$data['colors'] = $this->product_color_model->append('product_image')->append_select('fullpath, thumbFullpath')->get_many_by(array(
			'productID' => $data['product']['productID']
		));

		if (isset($data['product']['image']['productColorID']) && $data['product']['image']['productColorID'] > 0)
		{
			$data['defaultColor'] = $this->product_color_model->get($data['product']['image']['productColorID']);
		}

		//get category info
		$data['category'] = $this->product_category_model->append('parent')->get_by(array(
			'status' => 'enabled',
			'productCategoryID' => $data['product']['productCategoryID']
		));

		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->get_view();
		$data['title'] = $data['product']['title'];

		//seo stuff
		$data['metaDesc'] = $data['product']['shortDesc'];
		$data['metaKeywords'] = $data['product']['tags'];

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

	/**
	* Checks to see that product is valid and that the product exists
	*/
	public function _product_is_valid($productUrl = '')
	{
		//make sure product url is valid
		if (!is_null($productUrl) && $productUrl != '')
		{
			//check to see if the product exists
			$exists = $this->product_model->select('title')->get_by(array(
				'url' => $productUrl,
				'status' => 'enabled'
			));
			$exists2 = $this->product_model->select('title')->get_by(array(
				'url' => str_replace('_', '-', $productUrl),
				'status' => 'enabled'
			));

			//redirect if not found
			if (($exists == false || empty($exists)) && ($exists2 == false || empty($exists2)))
			{
				$this->session->set_flashdata('error', 'Product not found.');
				redirect('/products');
			}

			//send back proper version of url
			$productUrl = (!empty($exists2))? str_replace('_', '-', $productUrl) : $productUrl;
			return $productUrl;

		} else {
			$this->session->set_flashdata('error', 'Product not found.');
			redirect('/products');
		}
	}

	/**
	* Checks to see that category is valid and that the product exists
	*/
	public function _category_is_valid($categoryUrl = '')
	{
		//make sure category url is valid
		if (!is_null($categoryUrl) && $categoryUrl != '')
		{
			//check to see if the product exists
			$exists = $this->product_category_model->select('title')->get_by(array(
				'url' => $categoryUrl,
				'status' => 'enabled'
			));
			$exists2 = $this->product_category_model->select('title')->get_by(array(
				'url' => str_replace('_', '-', $categoryUrl),
				'status' => 'enabled'
			));

			//redirect if not found
			if (($exists == false || empty($exists)) && ($exists2 == false || empty($exists2)))
			{
				$this->session->set_flashdata('error', 'Category not found.');
				redirect('/products');
			}

			//send back proper version of url
			$categoryUrl = (!empty($exists2))? str_replace('_', '-', $categoryUrl) : $categoryUrl;
			return $categoryUrl;

		} else {
			$this->session->set_flashdata('error', 'Category not found.');
			redirect('/products');
		}
	}
}
