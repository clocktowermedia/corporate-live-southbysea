<?php
class Admin extends Admin_controller {

	//layout variables
	public $defaultLayout;

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//set layout variable(s)
		$this->defaultLayout = $this->get_admin_layout();

		//load relevant models
		$this->load->model('products/product_model');
		$this->load->model('products/product_category_model');
		$this->load->model('products/product_tag_model');
	}

	/**
	* Shows all products in a datatable
	*/
	function index()
	{
		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Products';

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}

	/**
	* view and action for adding a product
	*/
	function create()
	{
		//tell it what view file to use
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Add Product';

		//grab categories for dropdown
		$data['categories'] = $this->product_category_model->getOptions();
		$data['formAction'] = 'create';
		$data['productID'] = '';

		//load form validation library
		$this->load->library('form_validation');

		//get validation rules from model, set extra options as neccessary
		$validationRules = $this->product_model->validate;
		$validationRules['url']['rules'] .= '|is_unique[' . $this->product_model->_table . '.url]';

		//set form validation rules
		$this->form_validation->set_rules($validationRules);

		//if the form was submitted, update database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the product and show form errors
				$data['product'] = $this->input->post(NULL, TRUE);
				$this->load->view($this->defaultLayout, $data);
			} else {

				//insert product
				$productID = $this->product_model->insert($this->input->post(NULL, TRUE));

				//update tags if applicable
				if ($this->input->post('tags', TRUE) != '')
				{
					$tags = explode(',', $this->input->post('tags', TRUE));
					foreach ($tags as $tag)
					{
						$this->product_tag_model->insert(array(
							'productID' => $productID,
							'tag' => $tag
						));
					}
				}

				//display success message & redirect
				$this->session->set_flashdata('success', "Product was successfully created. You can now update product the product's images and colors.");
				redirect('/admin/products/edit/' . $productID);
			}
		} else {
			// tell it what layout to use and load the product
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	* view and action for editing a product
	*/
	function edit($productID = 0)
	{
		//make sure product exists
		$this->_product_is_valid($productID);

		//grab product info for view
		$data['product'] = $this->product_model->get($productID);

		//tell it what view file to use
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Edit ' . $data['product']['title'];

		//get categories
		$data['categories'] = $this->product_category_model->getOptions();
		$data['formAction'] = 'edit/' . $productID;
		$data['productID'] = $productID;

		//load form validation library
		$this->load->library('form_validation');

		//get validation rules from model, set extra options as neccessary
		$validationRules = $this->product_model->validate;

		$this->form_validation->set_rules($validationRules);

		//if the form was submitted, update database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the product and show form errors
				$this->load->view($this->defaultLayout, $data);
			} else {
				//double check that the url does not already exist
				$exists = $this->product_model->get_by(array(
					'url' => $this->input->post('url', TRUE),
					'productID !=' => $productID
				));
				if (!empty($exists) || isset($exists['productID']))
				{
					$this->session->set_flashdata('error', 'Product was not updated, an error occurred.');
					redirect('/admin/products/edit/' . $productID);
				}

				//update product
				$this->product_model->update($productID, $this->input->post(NULL, TRUE));

				//update tags if applicable
				if ($this->input->post('tags', TRUE) != '')
				{
					$tags = explode(',', $this->input->post('tags', TRUE));
					$this->product_tag_model->delete_by('productID', $productID);
					foreach ($tags as $tag)
					{
						$this->product_tag_model->insert(array(
							'productID' => $productID,
							'tag' => $tag
						));
					}
				}

				//display success message & redirect
				$this->session->set_flashdata('success', 'Product was successfully updated.');
				redirect('/admin/products');
			}
		} else {
			// tell it what layout to use and load the product
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	* Changes product status to deleted
	*/
	function delete($productID = 0)
	{
		//make sure product exists
		$this->_product_is_valid($productID);

		//set status to deleted
		$this->product_model->soft_delete($productID);
		$this->product_tag_model->soft_delete($productID);

		//redirect upon success
		$this->session->set_flashdata('success', 'Product was deleted.');
		redirect('/admin/products/');
	}

	/**
	 * View for editing product color (modal)
	 * @param  integer $colorID [description]
	 */
	function edit_color($colorID = 0)
	{
		//get color information
		$this->load->model('products/product_color_model');
		$data['color'] = $this->product_color_model->get((int)$colorID);

		//load view, plain
		$data['view'] = '/admin/modals/' . $this->get_view();
		$this->load->view($data['view'], $data);
	}

	/**
	 * View for editing image (modal)
	 * @param  integer $imageID [description]
	 */
	function edit_image($imageID = 0)
	{
		//get color information & dropdown for colors
		$this->load->model('products/product_image_model');
		$this->load->model('products/product_color_model');
		$data['image'] = $this->product_image_model->get((int)$imageID);
		$data['colors'] = $this->product_color_model->getOptionsForDropdown($data['image']['productID']);

		//load view, plain
		$data['view'] = '/admin/modals/' . $this->get_view();
		$this->load->view($data['view'], $data);
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

	/**
	* Checks to see that product ID is valid and that the product exists
	*/
	public function _product_is_valid($productID = 0)
	{
		//make sure product id is an integer
		$productID = (int) $productID;

		//make sure product id is greater than zero
		if ($productID > 0)
		{
			//check to see if the product exists
			$exists = $this->product_model->get($productID);

			if ($exists == false)
			{
				$this->session->set_flashdata('error', 'Invalid product id.');
				redirect('/admin/products');
			}

		} else {
			$this->session->set_flashdata('error', 'Invalid product id.');
			redirect('/admin/products');
		}
	}
}