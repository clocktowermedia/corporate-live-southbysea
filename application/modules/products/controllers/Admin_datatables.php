<?php
class Admin_datatables extends Admin_controller {

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//load model
		$this->load->model('products/product_model');
		$this->load->model('products/product_category_model');

		//load library
		$this->load->library('datatables');
	}

	/**
	* uses datatables library to get list of products in a json format for use in admin index
	*/
	function data()
	{
		//grab data from admins table in db
		$output = $this->datatables->select('p.productID, p.title, p.url')
			->select('pc.title AS category', false)
			->select('p.status')
			->select('p.dateUpdated')
			->add_column('actions',  '<div class="btn-group">
					<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-tag"></i> Product <span class="caret"></span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li><a href="/admin/products/edit/$1"><i class="fa fa-edit"></i> Edit</a></li>
						<li><a data-confirm="Are you sure you want to delete this?" href="/admin/products/delete/$1"><i class="fa fa-trash-o"></i> Delete</a></li>
					</ul>
				</div>', 'p.productID')
			->unset_column('p.productID')
			->from($this->product_model->_table . ' p')
			->join($this->product_category_model->_table . ' pc', 'p.productCategoryID = pc.productCategoryID', 'left outer')
			->where('p.status !=', 'deleted')
			->generate();

		echo $output;
	}

	/**
	* uses datatables library to get list of categories in a json format for use in admin index
	*/
	function categories()
	{
		//grab data from admins table in db
		$output = $this->datatables->select('c.productCategoryID, c.title')
			->select('pc.title AS parentCategory', false)
			->select('c.dateUpdated')
			->select('CASE WHEN c.depth > 1 THEN CONCAT("<li><a href=\"/admin/products/categories/reorder-products/", c.productCategoryID, "\"><i class=\"fa fa-arrows\"></i> Reorder Products</a></li>") ELSE "" END AS reorderButton', false)
			->add_column('actions',  '<div class="btn-group">
					<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-outdent"></i> Category <span class="caret"></span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li><a href="/admin/products/categories/edit/$1"><i class="fa fa-edit"></i> Edit</a></li>
						$2
						<li><a data-confirm="Are you sure you want to delete this?" href="/admin/products/categories/delete/$1"><i class="fa fa-trash-o"></i> Delete</a></li>
					</ul>
				</div>', 'c.productCategoryID, reorderButton')
			->unset_column('c.productCategoryID')
			->unset_column('reorderButton')
			->from($this->product_category_model->_table . ' AS c')
			->join($this->product_category_model->_table . ' pc', 'pc.productCategoryID = c.parentID', 'left outer')
			->where('c.status !=', 'deleted')
			->generate();

		echo $output;
	}

	function colors($productID = 0)
	{
		if ($productID > 0)
		{
			$this->load->model('products/product_color_model');

			$output = $this->datatables->select('c.productColorID')
				->select('CASE WHEN c.hexCode = "" THEN CONCAT("<img src=\"", c.thumbFullpath ,"\" class=\"image-preview\" >") ELSE CONCAT("<div style=\"background: ", c.hexCode , ";\" class=\"color-swatch\"></div>") END AS preview', false)
				->select('CASE WHEN c.hexCode = "" THEN c.fileName ELSE c.hexCode END AS file', false)
				->select('CASE WHEN c.title = "" THEN "" ELSE c.title END AS editLink', false)
				->add_column('editTitle', '<a href="#" data-url="/admin/products/json/editable/color" data-name="title" class="editable text" data-title="Title" data-pk="$1">$2</a>', 'c.productColorID, editLink')
				->unset_column('c.productColorID')
				->unset_column('editLink')
				->from($this->product_color_model->_table . ' AS c')
				->where('c.productID', $productID)
				->order_by('c.displayOrder')
				->generate();

			echo $output;
		}
	}

	function images($productID = 0)
	{
		if ($productID > 0)
		{
			$this->load->model('products/product_image_model');
			$this->load->model('products/product_color_model');

			$output = $this->datatables->select('i.productImageID, i.productID, i.productColorID')
				->select('CASE WHEN i.productColorID > 0 THEN c.title ELSE "Select Color" END AS colorTitle', false)
				->select('CONCAT("<img src=\"", i.thumbFullpath ,"\" class=\"image-preview\" >") AS preview', false)
				->select('i.filename')
				->select('"" AS checkbox', false)
				->edit_column('checkbox', '<input type="checkbox" class="image-checkbox" value="$1" name="images[]" data-form="images">', 'i.productImageID')
				->add_column('editLink', '<a href="#" data-url="/admin/products/json/editable/image/" data-name="productColorID" class="editable select" data-title="Color" data-parent-id="$2" data-pk="$1" value="$3">$4</a>', 'i.productImageID, i.productID, i.productColorID, colorTitle')
				->unset_column('i.productImageID')
				->unset_column('i.productColorID')
				->unset_column('i.productID')
				->unset_column('colorTitle')
				->from($this->product_image_model->_table . ' AS i')
				->join($this->product_color_model->_table . ' AS c', 'c.productColorID = i.productColorID', 'left outer')
				->where('i.productID', $productID)
				->order_by('i.displayOrder')
				->generate();

			echo $output;
		}
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

}