<?php
class Admin_product_categories_json extends Admin_controller {

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//load relevant models
		$this->load->model('products/product_category_model');
	}

	/* Tags are now assigned to products themselves
	function tags()
	{
		//load model we need
		$this->load->model('products/category_tag_model');

		//get all tags
		$tags = $this->category_tag_model->get_unique_tags();

		//output tags
		$this->render_json($tags);
	}

	function category_tags($category = '')
	{
		//load model we need
		$this->load->model('products/category_tag_model');

		//get all tags
		$tags = $this->category_tag_model->get_unique_tags();

		//output tags
		$this->render_json($tags);
	}
	*/

	function reorder()
	{
		try
		{
			//make sure we variable we need from post
			if ($this->input->post('categories', TRUE) == '')
			{
				throw new Exception('Must reorder categories.');
			}

			//try to reorder the categories
			$this->product_category_model->reorder($this->input->post('categories', TRUE));

			//set status message
			$data['status'] = 'ok';
			$data['message'] = 'Successfully saved category order.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/
}