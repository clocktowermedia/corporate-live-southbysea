<?php
class Admin_categories extends Admin_controller {

	//layout variables
	public $defaultLayout;

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//set layout variable(s)
		$this->defaultLayout = $this->get_admin_layout();

		//load relevant models
		$this->load->model('products/product_category_model');
	}

	/**
	* Shows all products in a datatable
	*/
	function index()
	{
		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->_get_custom_view();
		$data['title'] = 'Product Categories';

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}

	/**
	* view and action for adding a category
	*/
	function create()
	{
		//tell it what view file to use
		$data['view'] = $this->_get_custom_view();
		$data['title'] = 'Add Product Category';

		//grab categories for dropdown
		$data['categories'] = array('' => 'Select a Parent Category') + $this->product_category_model->getOptions(1);

		//load form validation library
		$this->load->library('form_validation');

		//get validation rules from model, set extra options as neccessary
		$validationRules = $this->product_category_model->validate;
		$validationRules['url']['rules'] .= '|is_unique[' . $this->product_category_model->_table . '.url]';

		//set form validation rules
		$this->form_validation->set_rules($validationRules);

		//if the form was submitted, update database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the category and show form errors
				$data['category'] = $this->input->post(NULL, TRUE);
				$this->load->view($this->defaultLayout, $data);
			} else {

				$insertData = $this->input->post(NULL, TRUE);

				//upload image if applicable
				if (isset($_FILES) && $_FILES['userfile']['name'] != '')
				{
					$imageID = modules::run('products/admin_image_handler/_upload_image', array(), 'category');
					if (is_int($imageID) && $imageID > 0)
					{
						$insertData['productCategoryImageID'] = $imageID;
					} else {
						$imageID = (gettype($imageID) != 'string' && gettype($imageID) != 'boolean')? 'Image could not be uploaded.' : $imageID;
						$this->session->set_flashdata('error', $imageID);
						redirect('/admin/products/categories/edit/' . $categoryID);
					}
				}

				//set depth
				if ($this->input->post('parentID', TRUE) != '' && $this->input->post('parentID', TRUE) > 0)
				{
					$parentPage = $this->product_category_model->get($insertData['parentID']);
					$insertData['depth'] = $parentPage['depth'] + 1;
				} else {
					$insertData['depth'] = 1;
				}

				// insert record to db & redirect
				$categoryID = $this->product_category_model->insert($insertData);

				//display success message & redirect
				$this->session->set_flashdata('success', 'Product category successfully created.');
				redirect('/admin/products/categories');
			}
		} else {
			// tell it what layout to use and load the category
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	* view and action for editing a category
	*/
	function edit($categoryID = 0)
	{
		//make sure category exists
		$this->_category_is_valid($categoryID);

		//grab category info for view
		$data['category'] = $this->product_category_model->append('image')->get($categoryID);

		//tell it what view file to use
		$data['view'] = $this->_get_custom_view();
		$data['title'] = 'Edit ' . $data['category']['title'];

		//get categories
		$data['categories'] = array('' => 'Select a Parent Category') + $this->product_category_model->getOptions(1);

		//load form validation library
		$this->load->library('form_validation');

		//get validation rules from model, set extra options as neccessary
		$validationRules = $this->product_category_model->validate;
		$this->form_validation->set_rules($validationRules);

		//if the form was submitted, update database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the category and show form errors
				$this->load->view($this->defaultLayout, $data);
			} else {
				//double check that the url does not already exist
				$exists = $this->product_category_model->get_by(array(
					'url' => $this->input->post('url', TRUE),
					'productCategoryID !=' => $categoryID
				));
				if (!empty($exists) || isset($exists['productCategoryID']))
				{
					$this->session->set_flashdata('error', 'A category with this url alredy exists.');
					redirect('/admin/products/categories/edit/' . $categoryID);
				}

				$updateData = $this->input->post(NULL, TRUE);

				//upload image if applicable
				if (isset($_FILES) && $_FILES['userfile']['name'] != '')
				{
					$imageID = modules::run('products/admin_image_handler/_upload_image', array(), 'category');
					if (is_int($imageID) && $imageID > 0)
					{
						$updateData['productCategoryImageID'] = $imageID;
					} else {
						$imageID = (gettype($imageID) != 'string' && gettype($imageID) != 'boolean')? 'Image could not be uploaded.' : $imageID;
						$this->session->set_flashdata('error', $imageID);
						redirect('/admin/products/categories/edit/' . $categoryID);
					}
				}

				//set depth
				if ($updateData['parentID'] > 0)
				{
					$parentPage = $this->product_category_model->get($updateData['parentID']);
					$updateData['depth'] = $parentPage['depth'] + 1;
				} else {
					$updateData['depth'] = 1;
				}

				// update record to db & redirect
				$this->product_category_model->update($categoryID, $updateData);

				//display success message & redirect
				$this->session->set_flashdata('success', 'Product category was successfully updated.');
				redirect('/admin/products/categories');
			}
		} else {
			// tell it what layout to use and load the category
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	* Changes category status to deleted
	*/
	function delete($categoryID = 0)
	{
		//make sure category exists
		$this->_category_is_valid($categoryID);

		//set status to deleted
		$this->product_category_model->soft_delete($categoryID);

		//redirect upon success
		$this->session->set_flashdata('success', 'Product category was deleted.');
		redirect('/admin/products/categories');
	}

	/**
	* View for re-ordering categories
	*/
	function reorder()
	{
		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->_get_custom_view();
		$data['title'] = 'Reorder Categories';

		//get a list of items
		$data['list_html'] = $this->product_category_model->getReorderHtml();

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}

	/**
	* View for re-ordering products (featured and regular) w/n a category
	*/
	function reorder_products($categoryID = 0)
	{
		//make sure category exists
		$this->_category_is_valid($categoryID);

		//get category info
		$data['category'] = $this->product_category_model->get($categoryID);

		//get product info
		$this->load->model('products/product_model');
		$data['featuredProducts'] = $this->product_model->getByCategory($categoryID, true);
		$data['products'] = $this->product_model->getByCategory($categoryID);

		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->_get_custom_view();
		$data['title'] = 'Reorder ' . $data['category']['title'] . ' Products';

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

	private function _get_custom_view()
	{
		return 'admin/categories/' . $this->get_view();
	}

	/**
	* Checks to see that category ID is valid and that the category exists
	*/
	public function _category_is_valid($categoryID = 0)
	{
		//make sure category id is an integer
		$categoryID = (int) $categoryID;

		//make sure category id is greater than zero
		if ($categoryID > 0)
		{
			//check to see if the category exists
			$exists = $this->product_category_model->get($categoryID);

			if ($exists == false)
			{
				$this->session->set_flashdata('error', 'Invalid product category id.');
				redirect('/admin/products/categories');
			}

		} else {
			$this->session->set_flashdata('error', 'Invalid product category id.');
			redirect('/admin/products/categories');
		}
	}
}