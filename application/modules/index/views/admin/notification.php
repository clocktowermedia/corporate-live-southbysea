<div class="row">
	<div class="col-xs-12">
		<?php echo form_open('admin/settings/notification'); ?>
		<fieldset>
			<?php
				$containerArray = array('');

				//content
				$this->form_builder->textarea('content', 'Content', $setting['content'], array('inputClass' => 'wysiwyg short'), 5, '', '', '', '');

				//type
				$this->form_builder->select('displayType', 'Status', array('container' => 'Container', 'full' => 'Full'), $setting['displayType'], '', 'The "Full" option will span the entire width of the browser. The "Container" option will span the width of the page content.', '', array('required' => 'required'), true);

				//color
				$this->form_builder->select('color', 'Color', array('gray' => 'Default', 'success' => 'Green', 'info' => 'Blue', 'warning' => 'Yellow', 'danger' => 'Red'), $setting['color'], '', '', '', array('required' => 'required'), true);

				//link field
    			$this->form_builder->text('link', 'Link:', $setting['link'], '', '', 'If you want the entire notification to be a clickable link, set the link in here. Otherwise, leave blank.', '', array());

				//status
				$this->form_builder->select('status', 'Status', array('enabled' => 'enabled', 'disabled' => 'disabled'), $setting['status'], '', '', '', array('required' => 'required'), true);

				//submit button
				$this->form_builder->button('submit', 'Update', array('inputClass' => 'btn btn-info'), '', '');
			?>
		</fieldset>
		</form>
	</div>
</div>
