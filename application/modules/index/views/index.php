<div class="container landing-page">
    <div class="row">
        <div class="inner-row">
            <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 text-center no-float">
                <img src="/assets/images/logo-lrg.png" width="300" height="300" alt="South by Sea" class="lrg-logo"/>
            </div>

            <div class="clearfix"></div>

            <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 text-center hp-links mt-20">
              <a href="?site=college" class="lrg-link" title="College">College</a>
              <a href="/fraternity" class="lrg-link" title="Fraternity"><span style="font-familty:NORTHWEST Bold;text-transform:uppercase;">Fraternity</span></a>
            </div>

        </div>
    </div>
</div>
