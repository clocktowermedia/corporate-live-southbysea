<h1>404 Page Not Found</h1>
<p>The page you requested was not found.</p>
<a class="btn btn-primary" href="/"><i class="fa fa-home"></i> Return to Home Page</a>