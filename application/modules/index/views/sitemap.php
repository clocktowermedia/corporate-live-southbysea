<?php
	$domain = 'http://' . $_SERVER['HTTP_HOST'];
?>
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
	<?php if ($urls != false && count($urls) > 0):?>
		<?php foreach ($urls as $url):?>
			<url>
				<loc><?php echo $domain . $url['link'];?></loc>
				<lastmod><?php echo date('r', strtotime($url['dateUpdated']));?></lastmod>
			</url>
		<?php endforeach;?>
	<?php endif;?>
</urlset>