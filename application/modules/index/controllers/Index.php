<?php

class Index extends MY_Controller {

	//layout variables
	public $defaultLayout;
	public $homeLayout;
	public $splashLayout;

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//set layout variable(s)
		$this->homeLayout = $this->get_layout_path() . 'home';
		$this->defaultLayout = $this->get_default_layout();
		$this->splashLayout = $this->get_layout_path() . 'styles-only';
	}

	/**
	* View for home page of your site
	*/
	function index()
	{
		//see if we have home page content or not
		$this->load->model('pages/page_model');
		$data['page'] = $this->page_model->get_by('urlPath', '/');
		$data['view'] = 'college';
		$data['bodyClass'] = 'college';

		// tell it what layout to use and load
		$this->load->view($this->homeLayout, $data);


		//determine if we are showing the college page or the landing page
		// if ($this->input->get('site', TRUE) == 'college' || (isset($_COOKIE['site']) && $_COOKIE['site'] == 'college'))
		// {
		// 	if (!isset($_COOKIE['site']) || $_COOKIE['site'] != 'college')
		// 	{
		// 		$expires = time()+60*60*24*30; #30 days
		// 		setcookie('site', 'college', $expires);
		// 	}
    //
		// 	//tell it what view to use, this is always called the view variable
		// 	$data['view'] = 'college';
		// 	$data['bodyClass'] = 'college';
    //
		// 	//get images for homepage
		// 	/*
		// 	$this->load->model('pages/page_image_model');
		// 	$pages = array('/designs', '/proof-request', '/products/apparel', '/products/accessories');
		// 	foreach ($pages as $page)
		// 	{
		// 		$data['page_images'][$page] = $this->page_image_model->get_by('pageUrlPath', $page);
		// 	}
		// 	*/
    // 		$numbers = range(1, 4);
    // 		shuffle($numbers);
    // 		$data['numbers'] = $numbers;
    //
		// 	// tell it what layout to use and load
		// 	$this->load->view($this->homeLayout, $data);
    //
		// } else if ($this->input->get('site', TRUE) == 'music') {
		// 	//redirect to music site
		// 	redirect('http://www.southxsea.com/');
		// } else {
    //
		// 	//tell it what view to use, this is always called the view variable
		// 	$data['view'] = $this->get_view();
		// 	$data['bodyClass'] = 'splash';
    //
		// 	// tell it what layout to use and load
		// 	$this->load->view($this->splashLayout, $data);
		// }
	}

	function sitemap()
	{
		//load the models we need, this will change on a per project basis
		$this->load->model('pages/page_model');
		$this->load->model('menus/menu_model');
		$this->load->model('campus_managers/rep_model');
		$this->load->model('campus_managers/school_model');
		$this->load->model('designs/design_model');
		$this->load->model('products/product_model');
		$this->load->model('designs/design_category_model');
		$this->load->model('products/product_category_model');

		//get information we need to send in the proper format
		$pages = $this->page_model->getSitemapXmlInfo();
		$menuItems = $this->menu_model->getSitemapXmlInfo();
		$reps = $this->rep_model->getSitemapXmlInfo();
		$schools = $this->school_model->getSitemapXmlInfo();
		$designs = $this->design_model->getSitemapXmlInfo();
		$products = $this->product_model->getSitemapXmlInfo();
		$designCategories = $this->design_category_model->getSitemapXmlInfo();
		$productCategories = $this->product_category_model->getSitemapXmlInfo();

		//combine all this information together
		$data['urls'] = array_merge(array_values($pages), array_values($menuItems), array_values($designCategories), array_values($designs), array_values($products), array_values($productCategories), array_values($reps), array_values($schools));

		//load the view
		$this->output->set_header('Content-Type: application/xml');
		$this->load->view($this->get_view(), $data);
	}

	/**
	* 404 Page not Found view
	*/
	function page_404()
	{
		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->get_view();

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}
}
