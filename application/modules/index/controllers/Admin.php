<?php
class Admin extends Admin_controller {

	//layout variables
	public $defaultLayout;

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//set layout variable(s)
		$this->defaultLayout = $this->get_admin_layout();

		//load relevant models
		$this->load->model('index/site_setting_model');
	}

	/**
	* view and action for editing the site notification
	*/
	function notification()
	{
		//grab setting info for view
		$setting = $this->site_setting_model->get_by('slug', 'notification');
		$data['setting'] = json_decode($setting['content'], TRUE);

		//tell it what view file to use
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Edit Site Notification';

		//if the form was submitted, update database
		if ($this->is_post())
		{
			//update design
			$this->site_setting_model->skip_validation()->update($setting['settingID'], array(
				'content' => json_encode($this->input->post(NULL, TRUE))
			));

			//display success message & redirect
			$this->session->set_flashdata('success', 'Site notification was successfully updated.');
			redirect('/admin');

		} else {
			// tell it what layout to use and load the design
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

}