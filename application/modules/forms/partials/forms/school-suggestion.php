<?php

//remove labels
if ($this->uri->segment(1) != 'admin')
{
	$this->form_builder->set_labels(false);
}
$this->form_builder->hidden('formType', 'school-suggestion', array('data-storage' => 'false'));
$this->form_builder->hidden('textarea_list', '', array('id' => 'textarea-list', 'data-storage' => 'false'));
?>

<h4>Contact Information</h4>
<div class="row mt-0 mb-0">
	<div class="col-sm-6">
		<?php
			//First Name Field
			$this->form_builder->text('firstName', 'First Name', (isset($submission['firstName']))? $submission['firstName'] : '', '', 'First Name', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));
		?>
	</div>

	<div class="col-sm-6">
		<?php
			//Last Name Field
			$this->form_builder->text('lastName', 'Last Name', (isset($submission['lastName']))? $submission['lastName'] : '', '', 'Last Name', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));
		?>
	</div>
</div>

<div class="row mt-0 mb-0">
	<div class="col-sm-6">
		<?php
    //Email field
    $this->form_builder->email('email', 'Email', (isset($submission['email']))? $submission['email'] : '', '', 'Email', '', '', array('required' => 'required'));
		?>
	</div>

	<div class="col-sm-6">
		<?php
    //Phone Field
    $this->form_builder->tel('phone', 'Phone', (isset($submission['phone']))? $submission['phone'] : '', '', 'Phone', '', '', array('required' => 'required', 'data-validation-minlength' => '10'));
		?>
	</div>
</div>

<div class="row mt-0 mb-0">
	<div class="col-sm-6">
		<?php
    //School field
    $this->form_builder->text('school', 'School', (isset($submission['school']))? $submission['school'] : '', '', 'School / City', '', '', array('required' => 'required'));
		?>
	</div>

	<div class="col-sm-6">
		<?php
    //Location
    $this->form_builder->text('state', 'State', (isset($submission['state']))? $submission['state'] : '', '', 'State', '', '', array('required' => 'required'));
		?>
	</div>
</div>

<?php
$this->form_builder->text('howDidYouHear', 'How did you hear about us?', (isset($submission['howDidYouHear']))? $submission['howDidYouHear'] : '', '', 'How did you hear about us?', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));
