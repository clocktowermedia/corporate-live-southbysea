<?php
	//remove labels
	if ($this->uri->segment(1) != 'admin')
	{
		$this->form_builder->set_labels(false);
	}

	//Form type field
	$this->form_builder->hidden('formType', 'catalog-request', array('data-storage' => 'false'));

	//Name Field
	$this->form_builder->text('firstName', 'First Name', (isset($submission['firstName']))? $submission['firstName'] : '', '', 'First Name', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));

	//Name Field
	$this->form_builder->text('lastName', 'Last Name', (isset($submission['lastName']))? $submission['lastName'] : '', '', 'Last Name', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));

	//email field
	$this->form_builder->email('email', 'Email', (isset($submission['email']))? $submission['email'] : '', '', 'Email', '', '', array('required' => 'required'));
?>

<?php
	//address
	$this->form_builder->text('address1', 'Address', (isset($submission['address1']))? $submission['address1'] : '', '', 'Address', '', '', array('required' =>'required', 'data-validation-minlength' => '2'));

	//address
	$this->form_builder->text('address2', 'Address Line 2', (isset($submission['address2']))? $submission['address2'] : '', '', 'Address Line 2', '', '', array());
?>

<div class="row mt-0 mb-0">
	<div class="col-sm-6">
		<?php
			//city
			$this->form_builder->text('city', 'City',  (isset($submission['city']))? $submission['city'] : '', '', 'City', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));
		?>
	</div>
	<div class="col-sm-6">

		<div class="row mt-0 mb-0">
			<div class="col-xs-6">
				<?php
					//state
					$this->load->helper('dropdown');
					$this->form_builder->select('state', 'State', array('' => 'State') + usStates(), (isset($submission['state']))? $submission['state'] : '', '', '', '', array('required' => 'required', 'data-validation-minlength' => '2', 'data-validation-maxlength' => '2'));
				?>
			</div>
			<div class="col-xs-6">
				<?php
					//zip
					$this->form_builder->text('zip', 'Zip Code', (isset($submission['zip']))? $submission['zip'] : '', '', 'Zip Code', '', '', array('required' => 'required', 'data-validation-minlength' => '5', 'data-validation-maxlength' => '10'));
				?>
			</div>
		</div>
	</div>
</div>

<?php
	//school field
	$this->form_builder->text('school', 'School', (isset($submission['school']))? $submission['school'] : '', '', 'School', '', '', array());

	//organization field
	$this->form_builder->text('chapter', 'Chapter', (isset($submission['chapter']))? $submission['chapter'] : '', '', 'Chapter', '', '', array('data-validation-minlength' => '2'));

	//chapter position field
	$this->form_builder->text('chapterPosition', 'Chapter Position', (isset($submission['chapterPosition']))? $submission['chapterPosition'] : '', '', 'Chapter Position', '', '', array());

	//how did you hear
	$howDidYouHearOpt = array(
		'' => 'How did you hear about us?',
		'Facebook' => 'Facebook',
		'Pinterest' => 'Pinterest',
		'Instagram' => 'Instagram',
		'Twitter' => 'Twitter',
		'Google' => 'Google',
		'Sales Representative' => 'Sales Representative',
		'Returning Customer' => 'Returning Customer',
		'Internet Search' => 'Internet Search',
		'Friend/Family' => 'Friend/Family',
		'Other' => 'Other'
	);
	$this->form_builder->select('howDidYouHear', 'How did you hear about us?', $howDidYouHearOpt, (isset($submission['howDidYouHear']))? $submission['howDidYouHear'] : '', '', '', '', array('required' => 'required'));
	$this->form_builder->text('howDidYouHearOther', 'Let us know how you heard about us', (isset($submission['howDidYouHearOther']))? $submission['howDidYouHearOther'] : '', array('inputClass' => 'show-if'), 'Let us know how you heard about us', '', '', array('data-linked-field' => 'howDidYouHear', 'data-show-when' => "['Other','Friend/Family']", 'data-show-req' => true, 'data-validation-minlength' => '2'));