<?php
	//remove labels
	if ($this->uri->segment(1) != 'admin')
	{
		$this->form_builder->set_labels(false);
	} else {
		$this->load->helper('array');
	}

	//Form type field
	$this->form_builder->hidden('formType', 'order-form', array('data-storage' => 'false'));
	$this->form_builder->hidden('textarea_list', '', array('id' => 'textarea-list', 'data-storage' => 'false'));
	$this->form_builder->hidden('files', 'bagAndTagSpreadsheet,customNamesSpreadsheet', array('data-storage' => 'false'));

	$appendQuestion = ($this->uri->segment(1) != 'admin')? array('append' => array('type' => 'button', 'content' => '<h4 class="mt-0"><i class="fa fa-question-circle"></i></h4>', 'class' => 'btn btn-default form-control')) : '';
?>

<h4>Contact Information</h4>
<div class="row mt-0 mb-0">
	<div class="col-sm-6">
		<?php
			//First Name Field
			$this->form_builder->text('firstName', 'First Name', (isset($submission['firstName']))? $submission['firstName'] : '', '', 'First Name', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));
		?>
	</div>

	<div class="col-sm-6">
		<?php
			//Last Name Field
			$this->form_builder->text('lastName', 'Last Name', (isset($submission['lastName']))? $submission['lastName'] : '', '', 'Last Name', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));
		?>
	</div>
</div>

<div class="row mt-0 mb-0">
	<div class="col-sm-6">
		<?php
			//email field
			$this->form_builder->email('email', 'Email', (isset($submission['email']))? $submission['email'] : '', '', 'Email', '', '', array('required' => 'required'));
		?>
	</div>
	<div class="col-sm-6">
		<?php
			//phone field
			$this->form_builder->tel('phone', 'Phone', (isset($submission['phone']))? $submission['phone'] : '', '', 'Phone', '', '', array('required' => 'required', 'data-validation-minlength' => '10'));
		?>
	</div>
</div>

<div class="row mt-0 mb-0">
	<div class="col-sm-6">
		<?php
			//name to invoice
			$this->form_builder->text('nameToInvoice', 'Name of Person to Invoice', (isset($submission['nameToInvoice']))? $submission['nameToInvoice'] : '', '', 'Name of Person to Invoice', '', '', array('required' => 'required', 'data-validation-minlength' => '4'));
		?>
	</div>
	<div class="col-sm-6">
		<?php
			//contact email
			$this->form_builder->email('invoiceContactEmail', 'Invoice Contact Email', (isset($submission['invoiceContactEmail']))? $submission['invoiceContactEmail'] : '', '', 'Invoice Contact Email', '', '', array('required' => 'required'));
		?>
	</div>
</div>

<h4 style="padding-top:30px;">Delivery Information</h4>
<?php

	if($this->uri->segment(1) != 'admin'){
		if (strpos($_SERVER[REQUEST_URI], 'fraternity')){
			$this->form_builder->hidden('site', 'branded.southbysea.com','');
		}else{
			$this->form_builder->hidden('site',  'branded.southbysea.com','');
		}
	}else{
		$this->form_builder->text('site', 'Site', (isset($submission['site']))? $submission['site'] : 'Null', '', 'branded.southbysea.com', '', '',array('disabled' => 'disabled'));
	}

	//Recipient Name
	$this->form_builder->text('recipientName', 'Delivery Address', (isset($submission['recipientName']))? $submission['recipientName'] : '', '', 'Recipient Name', '', '', array('required' =>'required', 'data-validation-minlength' => '2'));

	//address
	$this->form_builder->text('address1', 'Delivery Address', (isset($submission['address1']))? $submission['address1'] : '', '', 'Delivery Address', '', '', array('required' =>'required', 'data-validation-minlength' => '2'));

	//address
	$this->form_builder->text('address2', 'Delivery Address Line 2', (isset($submission['address2']))? $submission['address2'] : '', '', 'Delivery Address Line 2', '', '', array());
?>

<div class="row mt-0 mb-0">
	<div class="col-sm-6">
		<?php
			//city
			$this->form_builder->text('city', 'Delivery City',  (isset($submission['city']))? $submission['city'] : '', '', 'Delivery City', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));
		?>
	</div>
	<div class="col-sm-6">

		<div class="row mt-0 mb-0">
			<div class="col-xs-6">
				<?php
					//state
					$this->load->helper('dropdown');
					$this->form_builder->select('state', 'Delivery State', array('' => 'Delivery State') + usStates(), (isset($submission['state']))? $submission['state'] : '', '', '', '', array('required' => 'required', 'data-validation-minlength' => '2', 'data-validation-maxlength' => '2'));
				?>
			</div>
			<div class="col-xs-6">
				<?php
					//zip
					$this->form_builder->text('zip', 'Delivery Zip Code', (isset($submission['zip']))? $submission['zip'] : '', '', 'Delivery Zip Code', '', '', array('required' => 'required', 'data-validation-minlength' => '5', 'data-validation-maxlength' => '10'));
				?>
			</div>
		</div>
	</div>
</div>

<div class="row mt-0 mb-0">
	<div class="col-sm-6">
		<?php
			//order due date
			$this->form_builder->text('dueDate', 'Date Needed', (isset($submission['dueDate']))? $submission['dueDate'] : '', array('inputClass' => 'date no-sunday'), 'Date Needed', '', $appendQuestion, array('required' => 'required', 'data-validation-minlength' => '10'));

			//bag & tag
			$this->form_builder->set_labels(true);
			$this->form_builder->radios('bagAndTag', 'Bag & Tag?', array('yes' => 'Yes', 'no' => 'No'), (isset($submission['bagAndTag']))? $submission['bagAndTag'] : 'no', array('groupClass' => 'inline', 'labelClass' => 'mr-10'), '', array('required' => 'required'));

?>


<?php


			//bag & tag upload
			if ($this->uri->segment(1) == 'admin')
			{
				$spreadsheetFile = recursive_array_search('bagAndTagSpreadsheet', $submission['files']);
				$spreadsheetLink = ($spreadsheetFile !== false)? '<a target="_blank" href="' . $submission['files'][$spreadsheetFile]['fullpath'] . '">' . $submission['files'][$spreadsheetFile]['filename'] . '</a>' : 'N/A';
				echo '<div class="form-group">
					<label for="dueDate" class="control-label">Bag & Tag Spreadsheet:</label> '
					. $spreadsheetLink .
				'</div>';
			} else {
				$this->form_builder->file('bagAndTagSpreadsheet', 'Bag & Tag Spreadsheet', '', '', '', 'There will be an additional cost for Bag & Tag orders.<br/>XLSX, XLS, or CSV files accepted. 25MB filesize limit.', '', array('data-validation-trigger' => 'change', 'data-validation-fileextensions' => 'xlsx,xls,csv'));
			}

		?>
	</div>

	<div class="col-sm-6">
		<?php
			$this->form_builder->radios('firmDeadline', 'Firm Deadline?', array('yes' => 'Yes', 'no' => 'No'), (isset($submission['firmDeadline']))? $submission['firmDeadline'] : '', array('groupClass' => 'inline', 'labelClass' => 'mr-10'), '', array('required' => 'required'));

			//custom names/numbers
			$this->form_builder->radios('customNames', 'Custom names or numbers?', array('yes' => 'Yes', 'no' => 'No'), (isset($submission['customNames']))? $submission['customNames'] : 'no', array('groupClass' => 'inline', 'labelClass' => 'mr-10'), '', array('required' => 'required'));

			//custom names upload
			if ($this->uri->segment(1) == 'admin')
			{
				$spreadsheetFile = recursive_array_search('customNamesSpreadsheet', $submission['files']);
				$spreadsheetLink = ($spreadsheetFile !== false)? '<a target="_blank" href="' . $submission['files'][$spreadsheetFile]['fullpath'] . '">' . $submission['files'][$spreadsheetFile]['filename'] . '</a>' : 'N/A';
				echo '<div class="form-group">
					<label for="dueDate" class="control-label">Custom Names/Numbers Spreadsheet:</label> '
					. $spreadsheetLink .
				'</div>';
			} else {
				$this->form_builder->file('customNamesSpreadsheet', 'Custom Names/Numbers Spreadsheet', '', '', '', 'XLSX, XLS, CSV or PDF files accepted. 25MB filesize limit.', '', array('data-validation-trigger' => 'change', 'data-validation-fileextensions' => 'xlsx,xls,csv,pdf'));
			}
		?>
	</div>
</div>

<?php
	if ($this->uri->segment(1) != 'admin')
	{
		$this->form_builder->set_labels(false);
	}
?>

<h4 style="padding-top:30px;">Order Information</h4>
<div class="row mt-0 mb-0">
	<div class="col-sm-6">
		<?php
			//order id
			$this->form_builder->text('orderID', 'Order ID', (isset($submission['orderID']))? $submission['orderID'] : '', array('inputClass' => 'numeric'), 'Order ID', '', $appendQuestion, array('required' => 'required'));
		?>
	</div>
	<div class="col-sm-6">
		<?php
			//payment method
			$paymentMethods = array(
				'' => 'Payment Method',
				'Credit Card' => 'Credit Card',
				'Check' => 'Check',
				'PayPal' => 'PayPal',
				'Other' => 'Other (Please specify in comments)'
			);
			$this->form_builder->select('paymentMethod', 'Payment Method', $paymentMethods, (isset($submission['paymentMethod']))? $submission['paymentMethod'] : '', '', '', '', array('required' =>'required'));
		?>
	</div>
</div>

<?php
	//mock numbers
	$this->form_builder->text('versionNumbers', 'Version Number(s)', (isset($submission['versionNumbers']))? $submission['versionNumbers'] : '', '', 'Version Number(s)', '', '', array('required' => 'required'));

	//garments/products
	$this->form_builder->textarea('product', 'Garment(s)/Product(s)', (isset($submission['product']))? $submission['product'] : '', array(), 5, 'Garment(s)/Product(s)', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));

	//colors
	$this->form_builder->text('colors', 'Garment/Product Color(s)', (isset($submission['colors']))? $submission['colors'] : '', '', 'Garment/Product Color(s)', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));
?>

<div class="row mt-0 mb-0">
	<div class="col-sm-6">
		<?php
			//quoted price
			//$prepend = array('prepend' => array('type' => 'text', 'content' => '$'));
			$this->form_builder->text('priceQuoted', 'Price Quoted', (isset($submission['priceQuoted']))? $submission['priceQuoted'] : '', array('inputClass' => ''), 'Price Quoted', '', '', array('required' => 'required'));
		?>
	</div>

	<div class="col-sm-6">
		<?php
			//order quantity
			$this->form_builder->number('quantity', 'Total Quantity', (isset($submission['quantity']))? $submission['quantity'] : '', '', 'Total Quantity', '', '', array('required' => 'required', 'min' => 12));
		?>
	</div>
</div>


<h4 style="padding-top:30px;">Size Breakdown</h4>

<!-- <style>
table , td, th {
	border: 1px solid #595959;
	border-collapse: collapse;
}
td, th {
	padding: 3px;
	width: 60px;
	height: 50px;
}
th {
	background: #f0e6cc;
}
.even {
	background: #fbf8f0;
}
.odd {
	background: #fefcf9;
}
table td input {
 width: 100%;
}
</style> -->

<!-- <table id="myTable">
	<tbody>
		<tr>
			<td style="background:#e3eded;"></td>
			<td style="background:#e3eded;"></td>
			<td style="background:#e3eded;"></td>
			<td colspan="8" align="center"> Sizing Information</td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr style="background:#F7F7F7;" align="center">
			<td colspan="3">Product # / Color</td>
			<td> N/A </td>
			<td>XS</td>
			<td>S</td>
			<td>M</td>
			<td>L</td>
			<td>XL</td>
			<td>2XL</td>
			<td></td>
			<td colspan="2"><button onclick="myCreateFunction()"><small>Need Larger Sizes?</small></button></td>
			<td colspan="2">Subtotal</td>
		</tr>
		<tr>
			<td style="background:#F7F7F7;" colspan="3"><input type="text" placeholder="G200 - Black"/></td>
			<td ><input class='txtCal' type="number"/></td>
			<td><input class='txtCal' type="number"/></td>
			<td><input class='txtCal' type="number"/></td>
			<td><input class='txtCal' type="number"/></td>
			<td><input class='txtCal' type="number"/></td>
			<td><input class='txtCal' type="number"/></td>
			<td><input class='txtCal' type="number"/></td>
			<td><input class='txtCal' type="number"/></td>
			<td colspan="2"></td>
			<td colspan="2" align="center" id="total_sum_value"></td>
		</tr>
		<tr>
			<td style="background:#F7F7F7;" colspan="3"><button onclick="myCreateFunction()"><small>Add another product # / color</small></button></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td colspan="2"></td>
			<td colspan="2"></td>
		</tr>
		<tr>
			<td colspan="3"></td>
			<td></td>
			<td></td>
			<td></td>
			<td colspan="7" style="background:#e3eded;" align="right"> Total Quantity</td>
			<td colspan="2" style="background:#e3eded;" align="center">0</td>
		</tr>
	</tbody>
</table>
<br>

</style>
<table>
	<tbody>
		<tr>
			<td style="background:#e3eded;"></td>
			<td style="background:#e3eded;"></td>
			<td style="background:#e3eded;"></td>
			<td colspan="3" align="center"> Sizing Information</td>
		</tr>
		<tr style="background:#F7F7F7;" align="center">
			<td colspan="3">Product # / Color</td>
			<td> QTY </td>
			<td></td>
			<td>Subtotal</td>
		</tr>
		<tr>
			<td style="background:#F7F7F7;" colspan="3"><input type="text" placeholder="G200 - Black"/></td>
			<td ><input type="number"/></td>
			<td><input type="number"/></td>
			<td colspan="1" align="center">0</td>
		</tr>
		<tr>
			<td style="background:#F7F7F7;" colspan="3"><button onclick="myCreateFunction()"><small>Add another product # / color</small></button></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td style="background:#F7F7F7;" colspan="3"></td>
			<td colspan="2" style="background:#e3eded;" align="right"> Total Quantity</td>
			<td colspan="1" style="background:#e3eded;" align="center">0</td>
		</tr>
	</tbody>
</table>
<br> -->

<div class="form-group custom-checkbox">
	<input type="hidden" name="noSizing" value="0">
	<input name="noSizing" id="noSizing" type="checkbox" value="1"/>
	<label for="noSizing">
		<span>One size fits most or no sizing?</span>
	</label>
</div>

<?php if ($this->uri->segment(1) != 'admin'): ?>
	<div id="size-breakdown-container">
		<?php
			$this->load->model('products/size_model');
			$defaultOptions = $this->size_model->order_by('displayOrder', 'ASC')->get_many_by('showByDefault', 1);
			$extraOptions = $this->size_model->order_by('displayOrder', 'ASC')->get_many_by('showByDefault', 0);
			$extraSizes = $this->size_model->where('showByDefault', 0)->order_by('displayOrder', 'ASC')->dropdown('sizeID', 'title');
		?>

		<div class="size-group-container">
			<?php
				$this->form_builder->text('garmentStyle[]', '', '', array('inputClass' => 'garment-input'), 'Color/Style', '', '', array('required' => 'required', 'data-storage' => false));
			?>
			<?php foreach ($defaultOptions AS $option): ?>
				<div class="row mb-0 mt-0">
					<div class="col-md-2 col-sm-3 col-xs-3">
						<?php echo $option['title']; ?>:
					</div>
					<div class="col-md-10 col-sm-9 col-xs-9">
						<?php
							$requiredArray = ($option['required'] == TRUE)? array('required' => 'required') : array();
							$this->form_builder->number('size_' . $option['sizeID'] . '[]', '', '', array('inputClass' => 'quantity-input'), 'Enter 0 if none', '', '', $requiredArray + array('data-storage' => false));
						?>
					</div>
				</div>
			<?php endforeach; ?>

			<span>Need more sizes? Click <a href="" class="toggle-extra-sizes">here</a>.</span>

			<div class="extra-sizes hide">
				<?php if (count($extraOptions) > 0 && $extraOptions != false): ?>
					<?php $this->form_builder->select('extraSizes', '', $extraSizes, '', '', '', '', array('multiple' => 'multiple', 'data-storage' => false, 'data-validation-excluded' => true)); ?>

					<div class="extra-size-inputs">
						<?php foreach ($extraOptions AS $option): ?>
							<div class="size-container hide" data-size-id="<?php echo $option['sizeID'];?>">
								<div class="row mb-0 mt-0">
									<div class="col-md-2 col-sm-3 col-xs-3">
										<?php echo $option['title']; ?>:
									</div>
									<div class="col-md-10 col-sm-9 col-xs-9">
										<?php
											$requiredArray = ($option['required'] == TRUE)? array('required' => 'required') : array();
											$this->form_builder->number('size_' . $option['sizeID'] . '[]', '', '', array('inputClass' => 'quantity-input'), 'Enter 0 if none', '', '', $requiredArray + array('data-storage' => false));
										?>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>

				<span class="hide">Don't need more sizes? Click <a href="" class="toggle-extra-sizes">here</a>.</span>
			</div>
		</div>

		<div class="alert alert-warning hide mt-15" id="extra-sizes-msg">
			Please be aware that these sizes are not always available for your selected garment. Please contact your account manager if you need to confirm available sizing.
		</div>


		<div class="row text-center">
			<div class="form-group mb-0">
				<a id="add-style-btn" class="btn btn-info pull-none" style="background:url(/assets/images/click-to-add-style.jpg) !important;"><small style="color:white;opacity:.7;">Add Style</small></a>
			</div>
		</div>

	</div>
<?php else: ?>
	<?php
		//size breakdown
		$this->form_builder->textarea('sizeBreakdown', 'Size Breakdown', (isset($submission['sizeBreakdown']))? $submission['sizeBreakdown'] : '', array('inputClass' => 'cust-textarea'), 5, 'Size Breakdown', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));
	?>
<?php endif; ?>

<div class="clearfix mt-10"></div>

<?php
	//tshirt size for coordinator
	// $this->form_builder->text('coordinatorFreebieSize', 'Coordinator Freebie Size', (isset($submission['coordinatorFreebieSize']))? $submission['coordinatorFreebieSize'] : '', '', 'Coordinator Size', '', '', array('required' => 'required'));
?>

<h4 style="padding-top:30px;">UPLOAD FINAL PROOF(S)</h4>

<?php if ($this->uri->segment(1) != 'admin'):?>
	<div id="upload-container" class="text-center upload-container">
		<h5>Drag &amp; Drop Images Here</h5>
		<h5>-or-</h5>
		<label>
			<div class="form-group">
				<button type="button" id="upload-btn" class="btn btn-info"><br></button>
			</div>
			<input id="upload-trigger" type="file" name="userfile[]" class="hide" multiple="multiple" title="Click to add Files">
		</label>
		<h5>(25MB filesize limit)</h5>
	</div>

	<div id="uploaded-images" class="mt-20">
	</div>

	<script id="form-images-listing" type="x-handlebars-template">
		{{#if image}}
			<div id="attached-image-{{image.imageID}}" class="attached-image">
				<div class="col-sm-4 col-xs-6 thumbnail-img" id="images-{{image.imageID}}" data-id="{{image.imageID}}" data-queue="{{image.queueID}}">
					<a data-id="{{image.imageID}}" data-queue="{{image.queueID}}" title="Remove Image" class="delete-img">
						<img src="/assets/images/close.png" alt="Remove Image" title="Remove Image"/>
					</a>
					{{#compare image.mimeType "~=" 'image'}}
						<img src="{{image.thumbFullpath}}" class="img-responsive width-100">
					{{/compare}}
					{{#compare image.mimeType "!~=" 'image'}}
						<div class="filename">
							{{image.filename}}
						</div>
					{{/compare}}
				</div>
				<input type="hidden" name="images[]" value="{{image.imageID}}">
			</div>
		{{/if}}
	</script>
<?php else: ?>
	<?php if (isset($submission['images']) && count($submission['images']) > 0): ?>
	<div class="row" id="uploaded-images">
		<?php foreach ($submission['images'] AS $image): ?>
			<div class="col-sm-2 col-xs-4 thumbnail-img">
				<?php if (substr($image['mimeType'], 0, 5) === 'image'): ?>
					<img src="<?php echo $image['thumbFullpath'];?>" class="img-responsive">
				<?php else: ?>
					<?php echo $image['filename']; ?>
				<?php endif; ?>
			</div>
		<?php endforeach; ?>
	</div>
	<?php endif; ?>
<?php endif; ?>

<?php if ($this->uri->segment(1) != 'admin'):?>
	<div class="form-group custom-checkbox">
		<input type="hidden" name="agreeToTerms" value="0">
		<input name="agreeToTerms" id="agreeToTerms" type="checkbox" value="1" required="required" />
		<label for="agreeToTerms">
			<span class="text-uppercase">
				I agree to the <a href="/faq/return-policy-terms-and-conditions/?modal=true" data-toggle="ajax-modal" title="Return Policy - Terms &amp; Conditions">terms and conditions</a>
			</span>
		</label>
	</div>
<?php endif; ?>
