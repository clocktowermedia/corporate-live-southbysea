<?php
if ($this->uri->segment(1) != 'admin')
{
	$this->form_builder->set_labels(false);
}
$this->form_builder->hidden('formType', 'chapter-rep', array('data-storage' => 'false'));
?>

<div class="row mt-0 mb-0">
	<div class="col-sm-6">
		<?php
			//Name Field
			$this->form_builder->text('firstName', 'First Name', (isset($submission['firstName']))? $submission['firstName'] : '', '', 'First Name', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));

			//Email field
			$this->form_builder->email('email', 'Email', (isset($submission['email']))? $submission['email'] : '', '', 'Email', '', '', array('required' => 'required'));
		?>
	</div>
	<div class="col-sm-6">
		<?php
			//Name Field
			$this->form_builder->text('lastName', 'Last Name', (isset($submission['lastName']))? $submission['lastName'] : '', '', 'Last Name', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));

			//Phone Field
			$this->form_builder->tel('phone', 'Phone', (isset($submission['phone']))? $submission['phone'] : '', '', 'Phone', '', '', array('required' => 'required', 'data-validation-minlength' => '10'));
		?>
	</div>
</div>

<div class="row mt-0 mb-0">
	<div class="col-sm-6">
		<?php
			//Name Field
			$this->form_builder->text('chapter', 'Sorority/Fraternity', (isset($submission['chapter']))? $submission['chapter'] : '', '', 'Sorority/Fraternity', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));

			//Email field
			$this->form_builder->text('school', 'School', (isset($submission['school']))? $submission['school'] : '', '', 'School', '', '', array('required' => 'required'));
		?>
	</div>
	<div class="col-sm-6">
		<?php
			//Name Field
			$this->form_builder->text('major', 'Major', (isset($submission['major']))? $submission['major'] : '', '', 'Major', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));

			//Phone Field
			$this->form_builder->text('yearInSchool', 'Graduation Date (Semester and Year)', (isset($submission['yearInSchool']))? $submission['yearInSchool'] : '', '', 'Graduation Date (Semester and Year)', '', '', array('required' => 'required', 'data-validation-minlength' => '10'));
		?>
	</div>
</div>

<div class="row mt-0 mb-0">
	<div class="col-sm-6">
		<?php
			//Name Field
			$this->form_builder->text('instagramHandle', 'Instagram Handle', (isset($submission['instagramHandle']))? $submission['instagramHandle'] : '', '', 'Instagram Handle', '', '', array());

			//Email field
			$this->form_builder->text('twitterHandle', 'Twitter Handle', (isset($submission['twitterHandle']))? $submission['twitterHandle'] : '', '', 'Twitter Handle', '', '', array());
		?>
	</div>
	<div class="col-sm-6">
		<?php
			//Name Field
			$this->form_builder->text('pinterestHandle', 'Pinterest Handle', (isset($submission['pinterestHandle']))? $submission['pinterestHandle'] : '', '', 'Pinterest Handle', '', '', array());

			//Phone Field
			$this->form_builder->text('etcSocialmedia', 'Other Social Media', (isset($submission['etcSocialmedia']))? $submission['etcSocialmedia'] : '', '', 'Other Social Media', '', '', array());
		?>
	</div>
</div>

<hr><br>

<?php
	//address
	$this->form_builder->text('position1', 'Why do you want this position', (isset($submission['position1']))? $submission['position1'] : '', '', 'Why do you want this position', '', '', array('required' =>'required', 'data-validation-minlength' => '2'));

	//address
	$this->form_builder->text('position2', 'Have you held any leadership positions? If so, what were they and how do they apply to this position?', (isset($submission['position2']))? $submission['position2'] : '', '', 'Have you held any leadership positions? If so, what were they and how do they apply to this position?', '', '', array());
?>

<?php
	//address
	$this->form_builder->text('caption', 'Give us your best caption for an interest post for this order below! Details: 30 qty minimum $25.90 each All Inclusive Pricing! ', (isset($submission['caption']))? $submission['caption'] : '', '', 'Give us your best caption for an interest post for this order below! Details: 30 qty minimum $25.90 each All Inclusive Pricing!', '', '', array('required' =>'required', 'data-validation-minlength' => '2'));
?>
<span style="display: inline-block;margin-left: auto;margin-right: auto;height:auto;width:auto;margin-bottom:20px;" class="image">
		<img style="height:60%;width:60%;" src="/assets/slideshow/applicationtest.jpg">
</span><br>
<?php
	//address
	$this->form_builder->text('swapSales', 'What ideas do you have for increasing swap sales', (isset($submission['swapSales']))? $submission['swapSales'] : '', '', 'What ideas do you have for increasing swap sales', '', '', array());
?>

<?php
	//address
	$this->form_builder->text('brag', 'Brag about yourself here! Let\'s hear more about YOU!', (isset($submission['brag']))? $submission['brag'] : '', '', 'Brag about yourself here! Let\'s hear more about YOU!', '', '', array('required' =>'required', 'data-validation-minlength' => '2'));

	//address
	$this->form_builder->text('motto', 'What\'s your life motto?', (isset($submission['motto']))? $submission['motto'] : '', '', 'What\'s your life motto?', '', '', array());
?>
