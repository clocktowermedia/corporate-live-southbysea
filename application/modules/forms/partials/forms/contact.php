<?php
	//remove labels
	if ($this->uri->segment(1) != 'admin')
	{
		$this->form_builder->set_labels(false);
	}

	//Form type field
	$this->form_builder->hidden('formType', 'contact', array('data-storage' => 'false'));
	$this->form_builder->hidden('textarea_list', '', array('id' => 'textarea-list', 'data-storage' => 'false'));

	//Name Field
	$this->form_builder->text('firstName', 'First Name', (isset($submission['firstName']))? $submission['firstName'] : '', '', 'First Name', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));

	//Name Field
	$this->form_builder->text('lastName', 'Last Name', (isset($submission['lastName']))? $submission['lastName'] : '', '', 'Last Name', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));

	//email field
	$this->form_builder->email('email', 'Email', (isset($submission['email']))? $submission['email'] : '', '', 'Email', '', '', array('required' => 'required'));

	//Comments Field
	$this->form_builder->textarea('comments', 'Comments', (isset($submission['comments']))? $submission['comments'] : '', array('inputClass' => 'cust-textarea'), 10, 'Comments', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));
?>