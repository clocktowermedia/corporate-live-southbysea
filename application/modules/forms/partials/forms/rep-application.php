<?php

//remove labels
if ($this->uri->segment(1) != 'admin')
{
	$this->form_builder->set_labels(false);
}
$this->form_builder->hidden('formType', 'rep-application', array('data-storage' => 'false'));
$this->form_builder->hidden('textarea_list', '', array('id' => 'textarea-list', 'data-storage' => 'false'));

//Name Field
$this->form_builder->text('firstName', 'First Name', (isset($submission['firstName']))? $submission['firstName'] : '', '', 'First Name', '', '', array('required' => 'required'));

//Name Field
$this->form_builder->text('lastName', 'Last Name', (isset($submission['lastName']))? $submission['lastName'] : '', '', 'Last Name', '', '', array('required' => 'required'));

//User Image
/*
if ($this->uri->segment(1) == 'admin')
{
	if (isset($submission['image']) && $submission['image'] > 0): ?>
		<div class="form-group">
			<label class="control-label" for="userfile">Image:</label><br/>
			<a target="_blank" href="<?php echo $submission['images'][0]['fullpath']; ?>"><img src="<?php echo $submission['images'][0]['thumbFullpath']; ?>" class="img-thumbnail"></a>
		</div>
	<?php endif;
} else {
	$this->form_builder->hidden('image', 'userfile');
	$this->form_builder->file('userfile', 'Campus Rep Image', '', '', '', '5 MB max file size (JPG, PNG, GIF files only)', '', array());
}
*/

//Email field
$this->form_builder->email('email', 'Email', (isset($submission['email']))? $submission['email'] : '', '', 'Email', '', '', array('required' => 'required'));

//Phone Field
$this->form_builder->tel('phone', 'Phone', (isset($submission['phone']))? $submission['phone'] : '', '', 'Phone', '', '', array('required' => 'required', 'data-validation-minlength' => '10'));

//Facebook Field
//$this->form_builder->url('facebookLink', 'Facebook Link', (isset($submission['facebookLink']))? $submission['facebookLink'] : '', '', 'http://facebook.com/username', '', '', array());

//Twitter Field
//$this->form_builder->url('twitterLink', 'Twitter Link', (isset($submission['twitterLink']))? $submission['twitterLink'] : '', '', 'http://twitter.com/username', '', '', array());

//Pinterest Field
//$this->form_builder->url('pinterestLink', 'Pinterest Link', (isset($submission['pinterestLink']))? $submission['pinterestLink'] : '', '', 'http://pinterest.com/username', '', '', array());

//School field
$this->form_builder->text('school', 'Campus', (isset($submission['school']))? $submission['school'] : '', '', 'School Name', '', '', array('required' => 'required'));

//Chapter Field
$this->form_builder->text('chapter', 'Chapter', (isset($submission['chapter']))? $submission['chapter'] : '', '', 'Sorority/Fraternity', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));

//Chapter positions
$this->form_builder->textarea('chapterPositions', 'Have you held any positions in your chapter?', (isset($submission['chapterPositions']))? $submission['chapterPositions'] : '', array('inputClass' => 'cust-textarea'), 5, 'Have you held any positions in your chapter?', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));

//Clubs?
$this->form_builder->textarea('clubs', 'Are you a member of any clubs/other organizations on campus?', (isset($submission['clubs']))? $submission['clubs'] : '', array('inputClass' => 'cust-textarea'), 5, 'Are you a member of any clubs/other organizations on campus?', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));

//Major
$this->form_builder->text('major', "What's your major?", (isset($submission['major']))? $submission['major'] : '', '', 'What’s your major?', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));

//Grduation?
$this->form_builder->text('graduating', "When are you graduating?", (isset($submission['graduating']))? $submission['graduating'] : '', '', 'When are you graduating?', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));

//Other Popular Companies
$this->form_builder->textarea('otherPopularCompanies', 'What t-shirt companies are popular amongst chapters on your campus?', (isset($submission['otherPopularCompanies']))? $submission['otherPopularCompanies'] : '', array('inputClass' => 'cust-textarea'), 5, 'What t-shirt companies are popular amongst chapters on your campus?', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));

//Why do you want to be a rep?
$this->form_builder->textarea('whyApplying', 'Why do you want to be a Campus Manager for South By Sea?', (isset($submission['whyApplying']))? $submission['whyApplying'] : '', array('inputClass' => 'cust-textarea'), 5, 'Why do you want to be a Campus Manager for South By Sea?', '', '', array('required' => 'required', 'data-validation-minlength' => '15'));

//How would you promote SBS?
$this->form_builder->textarea('promotionTactics', 'How would you promote South By Sea on your campus?', (isset($submission['promotionTactics']))? $submission['promotionTactics'] : '', array('inputClass' => 'cust-textarea'), 5, 'How would you promote South By Sea on your campus?', '', '', array('required' => 'required', 'data-validation-minlength' => '15'));

//5 words to describe yourself
$this->form_builder->text('fiveWords', "What are 5 words you would use to describe yourself?", (isset($submission['fiveWords']))? $submission['fiveWords'] : '', '', 'What are 5 words you would use to describe yourself?', '', '', array('required' => 'required', 'data-validation-minlength' => '15'));

//life motto
$this->form_builder->textarea('lifeMotto', 'What is your life motto?', (isset($submission['lifeMotto']))? $submission['lifeMotto'] : '', array('inputClass' => 'cust-textarea'), 5, 'What is your life motto?', '', '', array('required' => 'required', 'data-validation-minlength' => '15'));

/*
//how did you hear
$howDidYouHearOpt = array(
	'' => 'How did you hear about us?',
	'Facebook' => 'Facebook',
	'Pinterest' => 'Pinterest',
	'Instagram' => 'Instagram',
	'Twitter' => 'Twitter',
	'Google' => 'Google',
	'Catalog' => 'Catalog',
	'Postcard' => 'Postcard',
	'Airstream' => 'Airstream',
	'Sales Representative' => 'Sales Representative',
	'Returning Customer' => 'Returning Customer',
	'Internet Search' => 'Internet Search',
	'Friend/Family' => 'Friend/Family',
	'Other' => 'Other'
);
$this->form_builder->select('howDidYouHear', 'How did you hear about us?', $howDidYouHearOpt, (isset($submission['howDidYouHear']))? $submission['howDidYouHear'] : '', '', '', '', array('required' => 'required'));
*/
$this->form_builder->text('howDidYouHear', 'How did you hear about us?', (isset($submission['howDidYouHear']))? $submission['howDidYouHear'] : '', '', 'How did you hear about us?', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));

//if they are an admin, provide a way to use this info to create a campus rep quickly
if ($this->uri->segment(1) == 'admin' && isset($submission))
{
	echo '<a href="/admin/campus-managers/promote/' . $submission['submissionID'] . '" class="btn btn-info"><i class="fa fa-institution"></i> Promote to Campus Manager</a>';
}
