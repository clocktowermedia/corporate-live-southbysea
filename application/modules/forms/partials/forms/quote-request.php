<div class="">
<?php
	//remove labels
	if ($this->uri->segment(1) != 'admin')
	{
		$this->form_builder->set_labels(false);

	} else {
		$this->form_builder->text('confirmationNumber', 'Confirmation #', (isset($submission['confirmationNumber']))? $submission['confirmationNumber'] : '', '', '', '', '', array('required' => 'required'));
	}

	//Form type field
	$this->form_builder->hidden('formType', 'quote-request', array('data-storage' => 'false'));
	$this->form_builder->hidden('page', $view, array('data-storage' => 'false'));
	$this->form_builder->hidden('textarea_list', '', array('id' => 'textarea-list', 'data-storage' => 'false'));

	$appendQuestion = ($this->uri->segment(1) != 'admin')? array('append' => array('type' => 'button', 'content' => '<h4 class="mt-0"><i class="fa fa-question-circle"></i></h4>', 'class' => 'btn btn-default form-control')) : '';
?>

<?php if ($view != 'quote-request' && (!isset($submission['page']) || (isset($submission['page']) && $submission['page'] != 'quote-request'))): ?>

	<?php
		//Design Field
		$frontDesignRequired = ($view == '')? array() : array();
		$this->form_builder->textarea('frontDesign', 'Front Design Instructions', (isset($submission['frontDesign']))? $submission['frontDesign'] : '', array('inputClass' => 'cust-textarea'), 5, 'Front Design Instructions', '', '', array());

		//Design Field
		$frontDesignRequired = ($view == '')? array() : array();
		$this->form_builder->textarea('backDesign', 'Back Design Instructions', (isset($submission['backDesign']))? $submission['backDesign'] : '', array('inputClass' => 'cust-textarea'), 5, 'Back Design Instructions', '', '', array());

		//Design Field
		$this->form_builder->textarea('comments', 'Other Instructions/Comments', (isset($submission['comments']))? $submission['comments'] : '', array('inputClass' => 'cust-textarea'), 5, 'Other Instructions/Comments', '', '', array());
	?>

	<?php if ($this->uri->segment(1) != 'admin'):?>
		<div class="row" id="upload-container">
			<div class="col-xs-12">
				<span><i class="arrow-single-left"></i> Have a design to upload? <a id="upload-btn">select your images</a>
				<small><br/>&nbsp;&nbsp;&nbsp;&nbsp;(AI, EPS, or PDF files preferred for original artwork, 25MB filesize limit)</small></span>
				<input id="upload-trigger" type="file" name="userfile[]" class="hide" multiple="multiple" title="Click to add Files">
			</div>
		</div>

		<div id="uploaded-images">
		</div>

		<script id="form-images-listing" type="x-handlebars-template">
			{{#if image}}
				<div id="attached-image-{{image.imageID}}" class="attached-image">
					<div class="col-sm-4 col-xs-6 thumbnail-img" id="images-{{image.imageID}}" data-id="{{image.imageID}}" data-queue="{{image.queueID}}">
						<a data-id="{{image.imageID}}" data-queue="{{image.queueID}}" title="Remove Image" class="delete-img">
							<img src="/assets/images/close.png" alt="Remove Image" title="Remove Image" />
						</a>
						{{#compare image.mimeType "~=" 'image'}}
							<img src="{{image.thumbFullpath}}" class="img-responsive width-100">
						{{/compare}}
						{{#compare image.mimeType "!~=" 'image'}}
							<a href="{{image.fullpath}}" target="_blank">
								<img src="http://dummyimage.com/200x200/eee/57656e&text=No+Preview" class="img-responsive width-100">
							</a>
						{{/compare}}
					</div>
					<input type="hidden" name="images[]" value="{{image.imageID}}">
				</div>
			{{/if}}
		</script>
	<?php else: ?>
		<?php if (isset($submission['images']) && count($submission['images']) > 0): ?>
		<div class="row" id="uploaded-images">
			<div class="col-xs-12">
				<h4>Uploaded Images</h4>
			</div>
			<?php foreach ($submission['images'] AS $image): ?>
				<div class="col-sm-2 col-xs-4 thumbnail-img">
					<a href="<?php echo $image['fullpath'];?>" target="_blank">
						<?php if (isset($image['thumbWidth']) && $image['thumbWidth'] != ''): ?>
							<img src="<?php echo $image['thumbFullpath'];?>" class="img-responsive">
						<?php else: ?>
							<img src="http://dummyimage.com/200x200/eee/57656e&text=No+Preview" class="img-responsive">
						<?php endif; ?>
					</a>
				</div>
			<?php endforeach; ?>
		</div>
		<br/>
		<?php endif; ?>
	<?php endif;?>
<?php endif; ?>

<h4>CUSTOMER INFORMATION</h4>

<?php
	//Name Field
	$this->form_builder->text('firstName', 'First Name', (isset($submission['firstName']))? $submission['firstName'] : '', '', 'First Name', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));

	//Name Field
	$this->form_builder->text('lastName', 'Last Name', (isset($submission['lastName']))? $submission['lastName'] : '', '', 'Last Name', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));

	//email field
	$this->form_builder->email('email', 'Email', (isset($submission['email']))? $submission['email'] : '', '', 'Email', '', '', array('required' => 'required'));

	//phone field
	$this->form_builder->tel('phone', 'Phone', (isset($submission['phone']))? $submission['phone'] : '', '', 'Phone', '', '', array());

	//school field
	$this->form_builder->text('school', 'Company', (isset($submission['school']))? $submission['school'] : '', '', 'Company', '', '', array('required' => 'required'));


	//how did you hear
	$howDidYouHearOpt = array(
		'' => 'How did you hear about us?',
		'Facebook' => 'Facebook',
		'Pinterest' => 'Pinterest',
		'Instagram' => 'Instagram',
		'Twitter' => 'Twitter',
		'Google' => 'Google',
		'Catalog' => 'Catalog',
		'Postcard' => 'Postcard',
		'Airstream' => 'Airstream',
		'Sales Representative' => 'Sales Representative',
		'Returning Customer' => 'Returning Customer',
		'Internet Search' => 'Internet Search',
		'Friend/Family' => 'Friend/Family',
		'Other' => 'Other'
	);
	$this->form_builder->select('howDidYouHear', 'How did you hear about us?', $howDidYouHearOpt, (isset($submission['howDidYouHear']))? $submission['howDidYouHear'] : '', '', '', '', array('required' => 'required'));
	$this->form_builder->text('howDidYouHearOther', 'Let us know how you heard about us', (isset($submission['howDidYouHearOther']))? $submission['howDidYouHearOther'] : '', array('inputClass' => 'show-if'), 'Let us know how you heard about us', '', '', array('data-linked-field' => 'howDidYouHear', 'data-show-when' => "['Other','Friend/Family']", 'data-show-req' => true, 'data-validation-minlength' => '2'));
	$this->form_builder->text('salerep', 'Sales Representative', (isset($submission['salerep']))? $submission['salerep'] : '', '', 'Sales Representative', '', '', array());


	?>


	<br><br><h4>ORDER INFORMATION</h4>
	<?php
	//product & design, different views for admin and regular user
	if ($this->uri->segment(1) != 'admin')
	{
		//selected design (only applicable on design page)
		if ($view == 'design')
		{
			$this->form_builder->hidden('design', (isset($submission['design']))? $submission['design'] : $design['title'], array('required' => 'required', 'data-storage' => 'false'));
		}

		//product
		if ($view == 'product')
		{
			$this->form_builder->hidden('product', (isset($submission['product']))? $submission['product'] : $product['title'], array('required' => 'required', 'data-storage' => 'false'));
		} else {
			$this->form_builder->text('product', '', (isset($submission['product']))? $submission['product'] : '', '', 'Product', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));
		}

	} else {

		if (isset($submission['page']))
		{
			if ($submission['page'] == 'design')
			{
				$this->form_builder->text('design', 'Design', (isset($submission['design']))? $submission['design'] : '', '', 'Design', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));
			}
			$this->form_builder->text('product', 'Product', (isset($submission['product']))? $submission['product'] : '', '', 'Product', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));
		}
	}

	//product color
	$colors = (isset($submission['colors']))? $submission['colors'] : '';
	$colors = (isset($defaultColor) && $colors == '')? $defaultColor['title']: $colors;
	$this->form_builder->text('colors', 'Product Color(s)', $colors, '', 'Product Color(s)', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));

	//order quantity
	$this->form_builder->number('quantity', 'Estimated Quantity', (isset($submission['quantity']))? $submission['quantity'] : '', '', 'Estimated Quantity', '', '', array('required' => 'required', 'min' => 12));

	//order budget
	$this->form_builder->text('budget', 'Budget', (isset($submission['budget']))? $submission['budget'] : '', '', 'Budget', '', '', array('required' => 'required', 'data-validation-minlength' => '1'));

	//order due date
	$this->form_builder->text('dueDate', 'Date Needed', (isset($submission['dueDate']))? $submission['dueDate'] : '', array('inputClass' => 'date no-sunday'), 'Date Needed', '', $appendQuestion, array('required' => 'required', 'data-validation-minlength' => '10'));

?>

	<br><br><h4>DESIGN INFORMATION</h4>

<?php if ($view == 'quote-request' || (isset($submission['page']) && $submission['page'] == 'quote-request')): ?>

	<?php

		//Design Field
		$frontDesignRequired = ($view == '')? array() : array();
		$this->form_builder->textarea('frontDesign', 'Front Design Instructions', (isset($submission['frontDesign']))? $submission['frontDesign'] : '', array('inputClass' => 'cust-textarea'), 5, 'Front Design Instructions', '', '', array());

		//Design Field
		$frontDesignRequired = ($view == '')? array() : array();
		$this->form_builder->textarea('backDesign', 'Back Design Instructions', (isset($submission['backDesign']))? $submission['backDesign'] : '', array('inputClass' => 'cust-textarea'), 5, 'Back Design Instructions', '', '', array());

		//Design Field
		$this->form_builder->textarea('comments', 'Other Design Information', (isset($submission['comments']))? $submission['comments'] : '', array('inputClass' => 'cust-textarea'), 5, 'Other Design Information', '', '', array());

	?>

	<br><br><h4>ADDITIONAL INFORMATION</h4>

<?php
	//Design Field
	$this->form_builder->textarea('comments', 'What Else Should We Know About Your Project?', (isset($submission['comments']))? $submission['comments'] : '', array('inputClass' => 'cust-textarea'), 5, 'What Else Should We Know About Your Project?', '', '', array());
?>


	<?php if ($this->uri->segment(1) != 'admin'):?>
		<div class="row" id="upload-container">
			<div class="col-xs-12">
				<span><i class="arrow-single-left"></i> Have a design to upload? <a id="upload-btn">select your images</a> <small>(AI, EPS, or PDF files preferred for original artwork, 25MB filesize limit)</small></span>
				<input id="upload-trigger" type="file" name="userfile[]" class="hide" multiple="multiple" title="Click to add Files">
			</div>
		</div>

		<div class="row" id="uploaded-images">
		</div>

		<script id="form-images-listing" type="x-handlebars-template">
			{{#if image}}
				<div id="attached-image-{{image.imageID}}" class="attached-image">
					<div class="col-sm-4 col-xs-6 thumbnail-img" id="images-{{image.imageID}}" data-id="{{image.imageID}}" data-queue="{{image.queueID}}">
						<a data-id="{{image.imageID}}" data-queue="{{image.queueID}}" title="Remove Image" class="delete-img">
							<img src="/assets/images/close.png" alt="Remove Image" title="Remove Image" />
						</a>
						{{#compare image.mimeType "~=" 'image'}}
							<img src="{{image.thumbFullpath}}" class="img-responsive width-100">
						{{/compare}}
						{{#compare image.mimeType "!~=" 'image'}}
							<div class="filename">
								<a href="{{image.fullpath}}" target="_blank">{{image.filename}}</a>
							</div>
						{{/compare}}
					</div>
					<input type="hidden" name="images[]" value="{{image.imageID}}">
				</div>
			{{/if}}
		</script>
	<?php else: ?>
		<?php if (isset($submission['images']) && count($submission['images']) > 0): ?>
		<div class="row" id="uploaded-images">
			<div class="col-xs-12">
				<h4>Uploaded Images</h4>
			</div>
			<?php foreach ($submission['images'] AS $image): ?>
				<div class="col-sm-2 col-xs-4 thumbnail-img">
					<a href="<?php echo $image['fullpath'];?>" target="_blank">
						<?php if (substr($image['mimeType'], 0, 5) === 'image'): ?>
							<img src="<?php echo $image['thumbFullpath'];?>" class="img-responsive">
						<?php else: ?>
							<a href="<?php echo $image['fullpath'];?>" target="_blank">
								<?php echo $image['filename']; ?>
							</a>
						<?php endif; ?>
					</a>
				</div>
			<?php endforeach; ?>
		</div>
		<br/>
		<?php endif; ?>
	<?php endif;?>
<?php endif; ?>

<?php if ($this->uri->segment(1) != 'admin'):?>
	<div class="form-group custom-checkbox">
		<input type="hidden" name="agreeToTerms" value="0">
		<input name="agreeToTerms" id="agreeToTerms" type="checkbox" value="1" required="required" />
  		<label for="agreeToTerms">
  			<span class="text-uppercase">
				I agree to the <a href="/faq/uploading-content-terms-and-conditions/?modal=true" data-toggle="ajax-modal" title="Uploading Content - Terms &amp; Conditions">terms and conditions</a>
			</span>
  		</label>
	</div>
<?php endif; ?>

</div>
