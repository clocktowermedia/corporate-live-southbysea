<?php
	//create array for classes
	$containerArray = array('labelClass' => 'col-sm-1', 'containerClass' => 'col-sm-11');

	//first name field
	$this->form_builder->text('text', 'Text:', (isset($option['text']))? $option['text'] : '', $containerArray, '', '', '', array('required' => 'required'));

	//last name field
	$this->form_builder->text('value', 'Value:', (isset($option['value']))? $option['value'] : '', $containerArray, '', '', '', array('required' => 'required'));

	//category
	if ($view == 'create' && !isset($option['formOptCategoryID']))
	{
		$this->form_builder->select('formOptCategoryID', 'Category:', $categories, (isset($option['formOptCategoryID']))? $option['formOptCategoryID'] : '', $containerArray, '', '', array('required' => 'required'));
	} else {
		$this->form_builder->hidden('formOptCategoryID', $option['formOptCategoryID']);
	}
?>