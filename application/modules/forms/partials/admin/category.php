<?php
	//create array for classes
	$containerArray = array('labelClass' => 'col-sm-1', 'containerClass' => 'col-sm-11');

	//title field
	$this->form_builder->text('title', 'Title:', (isset($category['title']))? $category['title'] : '', $containerArray, '', '', '', array('required' => 'required'));

	//url field
	$urlAttributes = ($this->uri->segment(5) == 'edit')? array('readonly' => 'readonly', 'disabled' => 'diabled') : array('required' => 'required');
	$this->form_builder->text('url', 'URL:', (isset($category['url']))? $category['url'] : '', $containerArray, '', '', '', $urlAttributes);
?>