<?php
/********************************************************************************************************
/
/ Rep application Form
/
/ Stores information from the campus rep application form
/
********************************************************************************************************/
class Rep_application_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'RepApplications';

	//primary key
	public $primary_key = 'repApplicationID';

	//relationships
	public $belongs_to = array(
		'form_submissions' => array('model' => 'forms/form_submission_model', 'primary_key' => 'submissionID')
	);

	//callbacks/observers (use function names)

	//validation
	public $validate = array(
		'submissionID' => array(
			'field' => 'submissionID',
			'label' => 'Submission ID',
			'rules' => 'required|is_natural_no_zero'
		),
		'phone' => array(
			'field' => 'phone',
			'label' => 'Phone',
			'rules' => 'required|min_length[10]'
		),
		'school' => array(
			'field' => 'school',
			'label' => 'School',
			'rules' => 'required'
		),
		/*
		'facebookLink' => array(
			'field' => 'facebookLink',
			'label' => 'Facebook Link',
			'rules' => 'valid_url'
		),
		'twitterLink' => array(
			'field' => 'twitterLink',
			'label' => 'Twitter Link',
			'rules' => 'valid_url'
		),
		'pinterestLink' => array(
			'field' => 'pinterestLink',
			'label' => 'Pinterest Link',
			'rules' => 'valid_url'
		),
		*/
		'chapter' => array(
			'field' => 'chapter',
			'label' => 'Chapter',
			'rules' => 'required|min_length[2]'
		),
		'major' => array(
			'field' => 'major',
			'label' => 'Major',
			'rules' => 'required|min_length[2]'
		),
		'graduating' => array(
			'field' => 'graduating',
			'label' => 'When are you graduating?',
			'rules' => 'required|min_length[2]'
		),
		'fiveWords' => array(
			'field' => 'fiveWords',
			'label' => '5 words you would use to describe yourself?',
			'rules' => 'required|min_length[15]'
		),
		'chapterPositions' => array(
			'field' => 'chapterPositions',
			'label' => 'Have you held any positions in your chapter?',
			'rules' => 'required|min_length[2]'
		),
		'clubs' => array(
			'field' => 'clubs',
			'label' => 'Are you a member of any clubs/other organizations on campus?',
			'rules' => 'required|min_length[2]'
		),
		'otherPopularCompanies' => array(
			'field' => 'otherPopularCompanies',
			'label' => 'What t-shirt companies are popular amongst chapters on your campus?',
			'rules' => 'required|min_length[2]'
		),
		'whyApplying' => array(
			'field' => 'whyApplying',
			'label' => 'Why do you want to be a rep for South By Sea?',
			'rules' => 'required|min_length[15]'
		),
		'promotionTactics' => array(
			'field' => 'promotionTactics',
			'label' => 'How would you promote South By Sea on your campus?',
			'rules' => 'required|min_length[15]'
		),
		'lifeMotto' => array(
			'field' => 'lifeMotto',
			'label' => 'What is your life motto?',
			'rules' => 'required|min_length[15]'
		),
		'howDidYouHear' => array(
			'field' => 'howDidYouHear',
			'label' => 'How did you hear about us?',
			'rules' => 'required|min_length[2]'
		)
	);

	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Validity checker functions & helper functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
}