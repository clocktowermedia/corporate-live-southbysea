<?php
/********************************************************************************************************
/
/ Proof Request Form
/
/ Stores information from the proof request form
/
********************************************************************************************************/
class Proof_request_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'ProofRequests';

	//primary key
	public $primary_key = 'proofRequestID';

	//relationships
	public $belongs_to = array(
		'form_submissions' => array('model' => 'forms/form_submission_model', 'primary_key' => 'submissionID')
	);

	//callbacks/observers (use function names)

	//validation
	public $validate = array(
		'submissionID' => array(
			'field' => 'submissionID',
			'label' => 'Submission ID',
			'rules' => 'required|is_natural_no_zero'
		),
		'school' => array(
			'field' => 'school',
			'label' => 'School',
			'rules' => 'required'
		),
		'product' => array(
			'field' => 'product',
			'label' => 'Product',
			'rules' => 'required|min_length[2]'
		),
		'colors' => array(
			'field' => 'colors',
			'label' => 'Product Color(s)',
			'rules' => 'required|min_length[2]'
		),
		'quantity' => array(
			'field' => 'quantity',
			'label' => 'Estimated Quantity',
			'rules' => 'required|is_natural_no_zero|greater_than[11]'
		),
		'dueDate' => array(
			'field' => 'dueDate',
			'label' => 'Date Needed',
			'rules' => 'required|min_length[10]'
		),
		'coordinatorFreebieSize' => array(
			'field' => 'coordinatorFreebieSize',
			'label' => 'Coordinate T-Shirt Size'
		),
		'howDidYouHear' => array(
			'field' => 'howDidYouHear',
			'label' => 'How did you hear about us?',
			'rules' => 'required|min_length[2]'
		)
	);

	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Validity checker functions & helper functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
}
