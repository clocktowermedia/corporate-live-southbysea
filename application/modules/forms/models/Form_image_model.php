<?php
/********************************************************************************************************
/
/ Form Images
/
********************************************************************************************************/
class Form_image_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'FormImages';

	//primary key
	public $primary_key = 'imageID';

	//relationships
	public $has_single = array(
		'submission' => array('model' => 'forms/form_submission_model', 'primary_key' => 'submissionID', 'join_key' => 'submissionID'),
	);

	//callbacks/observers (use function names)

	//validation
	public $validate = array(
		/*
		'submissionID' => array(
			'field' => 'submissionID',
			'label' => 'Associated Form Submission',
			'rules' => 'required|is_natural_no_zero'
		),
		*/
		'filename' => array(
			'field' => 'filename', 
			'label' => 'Filename',
			'rules' => 'required'
		),
		'filepath' => array(
			'field' => 'filepath', 
			'label' => 'Filepath',
			'rules' => 'required'
		),
		'fullpath' => array(
			'field' => 'fullpath', 
			'label' => 'Fullpath',
			'rules' => 'required'
		),
	);

	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/
	

	/**********************************************************************************************
	*
	* Validity checker functions & helper functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
}