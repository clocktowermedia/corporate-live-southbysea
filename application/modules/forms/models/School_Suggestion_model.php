<?php
/********************************************************************************************************
/
/ Rep application Form
/
/ Stores information from the campus rep application form
/
********************************************************************************************************/
class School_Suggestion_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'SchoolSuggestion';

	//primary key
	public $primary_key = 'schoolSuggestionID';

	//relationships
	public $belongs_to = array(
		'form_submissions' => array('model' => 'forms/form_submission_model', 'primary_key' => 'submissionID')
	);

	//callbacks/observers (use function names)

	//validation
	public $validate = array(
		'submissionID' => array(
			'field' => 'submissionID',
			'label' => 'Submission ID',
			'rules' => 'required|is_natural_no_zero'
		),
		'phone' => array(
			'field' => 'phone',
			'label' => 'Phone',
			'rules' => 'required|min_length[10]'
		),
		'school' => array(
			'field' => 'school',
			'label' => 'School',
			'rules' => 'required'
		)
	);

	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Validity checker functions & helper functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
}
