<?php
/********************************************************************************************************
/
// Form Options
// Explanation: Used to populate select boxes
/
********************************************************************************************************/
class Form_option_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'FormOptions';
	public $primary_key = 'optionID';

	//callbacks/observers (use function names)

	//relationships
	public $belongs_to = array(
		'category' => array('model' => 'forms/form_option_category_model', 'primary_key' => 'formOptCategoryID')
	);

	//validation
	public $validate = array(
		'text' => array(
			'field' => 'text',
			'label' => 'Text',
			'rules' => 'required'
		),
		'value' => array(
			'field' => 'value',
			'label' => 'Value',
			'rules' => 'required'
		),
		'formOptCategoryID' => array(
			'field' => 'formOptCategoryID',
			'label' => 'Category',
			'rules' => 'required|is_natural_no_zero'
		)
	);

	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/
	/**
	 * Get all options by form name
	 */
	function getByFormName($name = '', $includePrompt = true)
	{
		//make sure there is sufficient data for a query
		if (!is_null($name) && $name != '')
		{
			//get form name
			$this->load->model('forms/form_model');
			$form = $this->form_model->select('formID')->get_by('formUrl', $name);

			if ($form != false && !empty($form))
			{
				$this->load->model($this->belongs_to['category']['model'], 'category_model');
				$categories = $this->category_model->get_many_by('formID', $form['formID']);

				if (count($categories) > 0)
				{
					foreach ($categories AS $category)
					{
						$options[$category['url']]['category'] = $category;
						$options[$category['url']]['options'] = $this->getByFieldname($category['url'], $includePrompt);
					}

					return $options;
				}
			}

			return false;
		}

		return false;
	}

	/**
	* Get dropdown information by field name, can be used in dropdown
	*/
	function getByFieldNameFlat($name = '', $includePrompt = true)
	{
		//make sure there is sufficient data
		if (!is_null($name) && $name != '')
		{
			$this->load->model($this->belongs_to['category']['model'], 'category_model');
			$category = $this->category_model->where('(url = "' . $name . '" OR url = "' . str_replace('_', '-', $name) . '")')->get_all();

			$options = array();
			if (count($category) > 0)
			{
				$query = $this->db->from($this->_table)
					->where('formOptCategoryID', $category[0]['formOptCategoryID'])
					->get();

				//return empty array if no match is found
				if ($query->num_rows() <= 0)
				{
					return array();
				}

				//loop through results and formtat them into an array
				$results = $query->result_array();

				if ($includePrompt == true)
				{
					$options[''] = 'Select a option';
				}

				if (count($results) > 0)
				{
					foreach ($results as $option)
					{
						$options[$option['value']] = $option['text'];
					}
				}
			}

			return $options;
		}

		return array();
	}

	/**
	* Get dropdown information by field name, includes shortcuts to other functions
	*/
	function getByFieldname($name = '', $format = 'dropdown')
	{
		//make sure there is sufficient data
		if (!is_null($name) && $name != '')
		{
			$this->load->model($this->belongs_to['category']['model'], 'category_model');
			$category = $this->category_model->where('(url = "' . $name . '" OR url = "' . str_replace('_', '-', $name) . '")')->get_all();

			//determine if we have any option groups/hierarchies of any sort
			$optGroup = $this->db->from($this->_table)
				->where('formOptCategoryID', $category[0]['formOptCategoryID'])
				->where('isOptGroup', 1)
				->get();
			$isOptGroup = ($optGroup->num_rows() <= 0)? false : true;

			//format differently depending on if we have hierarchy or not
			if ($isOptGroup == true)
			{
				if ($format == 'dropdown')
				{
					return $this->getByFieldNameHierarchy($name);
				}

				return false;
			} else {
				return $this->getByFieldNameFlat($name);
			}
		}

		return array();
	}

	function getByFieldNameHierarchy($name = '')
	{
		//make sure there is sufficient data
		if (!is_null($name) && $name != '')
		{
			$this->load->model($this->belongs_to['category']['model'], 'category_model');
			$category = $this->category_model->where('(url = "' . $name . '" OR url = "' . str_replace('_', '-', $name) . '")')->get_all();

			//determine if we have any option groups/hierarchies of any sort
			$query = $this->db->from($this->_table)
				->where('isOptGroup', 1)
				->where('formOptCategoryID', $category[0]['formOptCategoryID'])
				->get();

			//return empty array if no match is found
			if ($query->num_rows() <= 0)
			{
				return array();
			}

			//set default for options
			$options = array();

			//loop through groups & get children
			$optGroups = $query->result_array();
			foreach ($optGroups as $group)
			{
				$children = $this->getChildren($group['optionID']);

				if ($group['disabled'] == 0) { $options[$group['value']]['value'] = $group['value']; }
				$options[$group['value']]['text'] = $group['text'];
				$options[$group['value']]['children'] = $children;
			}

			return $options;
		}

		return array();
	}

	/**
	 * Gets sub options
	 * @param  integer $optionID
	 * @return array or boolean
	 */
	function getChildren($optionID = 0)
	{
		//make sure valid id was passed
		if ($optionID > 0)
		{
			$query = $this->db->from($this->_table)
				->where('parentOptionID', $optionID)
				->get();

			//return empty array if no match is found
			if ($query->num_rows() <= 0)
			{
				return array();
			}

			//return results as array
			return $query->result_array();
		}

		return false;
	}

	/**
	 * Get by field name and value
	 */
	function getByFieldNameAndValue($name = null, $value = null)
	{
		//make sure there is sufficient data
		if (!is_null($name) && $name != '')
		{
			$this->load->model($this->belongs_to['category']['model'], 'category_model');
			$category = $this->category_model->where('(url = "' . $name . '" OR url = "' . str_replace('_', '-', $name) . '")')->get_all();

			$query = $this->db->from($this->_table)
				->where('value', $value)
				->where('formOptCategoryID', $category[0]['formOptCategoryID'])
				->get();

			//return empty array if no match is found
			if ($query->num_rows() <= 0)
			{
				return array();
			}

			//return result as an array
			return $query->row_array();
		}

		return array();
	}

	/**********************************************************************************************
	*
	* Validity checker functions & helper functions, may contain redirects
	*
	***********************************************************************************************/
}