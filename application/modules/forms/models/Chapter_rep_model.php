<?php
/********************************************************************************************************
/
/ Chapter Rep Form
/
/ Stores information from the chapter rep form
/
********************************************************************************************************/
class Chapter_rep_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'ChapterReps';

	//primary key
	public $primary_key = 'chapterRepID';

	//relationships
	public $belongs_to = array(
		'form_submissions' => array('model' => 'forms/form_submission_model', 'primary_key' => 'submissionID')
	);

	//callbacks/observers (use function names)

	//validation
	public $validate = array(
		'submissionID' => array(
			'field' => 'submissionID',
			'label' => 'Submission ID',
			'rules' => 'required|is_natural_no_zero'
		),
		'phone' => array(
			'field' => 'phone',
			'label' => 'Phone',
			'rules' => 'required|min_length[10]'
		),
		'school' => array(
			'field' => 'school',
			'label' => 'School',
			'rules' => 'required'
		),
		'chapter' => array(
			'field' => 'chapter',
			'label' => 'Chapter',
			'rules' => 'required|min_length[2]'
		),
		'yearInSchool' => array(
			'field' => 'yearInSchool',
			'label' => 'Graduation Year',
			'rules' => 'required|min_length[2]'
		)
	);

	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Validity checker functions & helper functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
}
