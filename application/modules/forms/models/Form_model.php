<?php
/********************************************************************************************************
/
/ Form model
/
/ Forms on the site that users can submit - links to form submisison model and an optional additional model
/
********************************************************************************************************/
class Form_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'Forms';
	public $_submissionTable = 'FormSubmissions';

	//primary key
	public $primary_key = 'formID';

	//relationships
	public $has_many = array(
		'form_submissions' => array('model' => 'forms/form_submission_model', 'primary_key' => 'submissionID')
	);

	//callbacks/observers (use function names)

	//validation
	public $validate;

	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/

	//get all unread message counts
	public function getUnreadCounts()
	{
		//define query
		$query = $this->db->select('f.formName, f.formUrl')
			->select('COUNT(fs.submissionID) AS unreadCount', false)
			->from($this->_table . ' f')
			->where('fs.status', 'unread')
			->where('f.displayInAdmin', 1)
			->join($this->_submissionTable . ' fs', 'fs.formID = f.formID')
			->group_by('fs.formID')
			->get();

		//return empty array if no matches found
		if ($query->num_rows() <= 0)
		{
			return array();
		}

		//return query as array
		return $query->result_array();
	}

	//returns an array of all fields in db
	public function list_db_fields()
	{
		return $this->db->list_fields($this->_table);
	}

	/**********************************************************************************************
	*
	* Validity checker functions & helper functions
	*
	***********************************************************************************************/

	
	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
}