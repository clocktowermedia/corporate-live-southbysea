<?php
/********************************************************************************************************
/
/ Form Submission Model
/
/ Most forms have the same basic fields (name, email, etc.), that is what this is, links to
/ additional model for additional fields
/
********************************************************************************************************/
class Form_submission_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'FormSubmissions';
	public $primary_key = 'submissionID';

	//relationships
	public $belongs_to = array(
		'forms' => array('model' => 'forms/form_model', 'primary_key' => 'formID'),
	);

	public $has_many = array(
		'images' => array('model' => 'forms/form_image_model', 'primary_key' => 'imageID', 'join_key' => 'submissionID'),
		'files' => array('model' => 'forms/form_file_model', 'primary_key' => 'fileID', 'join_key' => 'submissionID'),
	);

	//callbacks/observers (use function names)
	public $before_create = array('_create_timestamp');

	//validation
	public $validate = array(
		'formID' => array(
			'field' => 'formID',
			'label' => 'Form Type',
			'rules' => 'required|is_natural_no_zero'
		),
		'firstName' => array(
			'field' => 'firstName',
			'label' => 'First Name',
			'rules' => 'required|min_length[2]'
		),
		'lastName' => array(
			'field' => 'lastName',
			'label' => 'Last Name',
			'rules' => 'required|min_length[2]'
		),
		'email' => array(
			'field' => 'email',
			'label' => 'Form Type',
			'rules' => 'required|valid_email'
		)
	);


	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/

	/**
	 * Get full form information by submission id
	 * @param  integer $submissionID
	 * @param  boolean $merge		[do we want to merge the secondary table results to the original form submission?]
	 * @return array				 [returns an array with data or false if not found]
	 */
	public function getFullByID($submissionID = 0, $merge = false)
	{
		//get form info first
		$formInfo = $this->with('forms')->get($submissionID);

		if (isset($formInfo['formUrl']) && $formInfo['formUrl'] != '')
		{
			//determine which additional model we need to call
			$this->has_single[$formInfo['formUrl']] = array('model' => 'forms/' . $formInfo['formModel'], 'primary_key' => $this->primary_key);

			//get a new record
			if ($merge == true)
			{
				$formSubmission = $this->with('forms')->with($formInfo['formUrl'])->append('images')->append('files')->get($submissionID);
			} else {
				$formSubmission = $this->with('forms')->append($formInfo['formUrl'])->append('images')->append('files')->get($submissionID);
			}

			//unset the merge
			unset($this->has_single[$formInfo['formUrl']]);

			//return the form
			return $formSubmission;

		} else {

			return false;
		}
	}

	public function searchResults(array $filters = array())
	{
		if (isset($filters['formUrl']))
		{
			$this->load->model($this->belongs_to['forms']['model']);
			$formModel = basename($this->belongs_to['forms']['model']);
			$formInfo = $this->{$formModel}->select('formModel')->get_by('formUrl', $filters['formUrl']);

			//determine which additional model we need to call
			$this->load->model('forms/'. $formInfo['formModel']);
			$this->db->from($this->_table . ' AS s')
				->join($this->{$formInfo['formModel']}->_table . ' i', "s.submissionID = i.submissionID", 'left outer');
		} else {
			$this->db->from($this->_table . ' AS s');
		}

		if (isset($filters['field']) && $filters['field'] == 'name')
		{
			$this->db->where('(s.firstName LIKE "%' . $filters['query'] . '%" || s.lastName LIKE "%' . $filters['query'] . '%")');

		} else if (isset($filters['field'])) {

			//check where field exists
			if (isset($filters['formUrl']))
			{
				$existsOriginal = $this->field_exists($filters['field']);
				$existsSecondary = $this->{$formInfo['formModel']}->field_exists($filters['field']);
				$alias = ($existsOriginal === true)? 's' : 'i';
			} else {
				$alias = 's';
			}

			$this->db->select($alias . '.' . $filters['field'])->like($alias . '.' . $filters['field'], $filters['query']);
		}

		$results = $this->db->select('s.submissionID, s.dateCreated, firstName, lastName, subject')->get()->result_array();
		return $results;
	}

	/**********************************************************************************************
	*
	* Validity checker functions & helper functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
	protected function _create_timestamp($data)
	{
		$data['dateCreated'] = date('Y-m-d H:i:s');
		return $data;
	}
}