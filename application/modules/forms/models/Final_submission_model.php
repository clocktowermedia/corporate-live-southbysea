<?php
/********************************************************************************************************
/
/ Final Submission Form
/
/ Stores information from the final submission form
/
********************************************************************************************************/
class Final_submission_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'FinalSubmissions';

	//primary key
	public $primary_key = 'finalSubmissionID';

	//relationships
	public $belongs_to = array(
		'form_submissions' => array('model' => 'forms/form_submission_model', 'primary_key' => 'submissionID')
	);

	//callbacks/observers (use function names)

	//validation
	public $validate = array(
		'submissionID' => array(
			'field' => 'submissionID',
			'label' => 'Submission ID',
			'rules' => 'required|is_natural_no_zero'
		),
		'phone' => array(
			'field' => 'phone',
			'label' => 'Phone',
			'rules' => 'required|min_length[10]'
		),
		'nameToInvoice' => array(
			'field' => 'nameToInvoice',
			'label' => 'Name of Person to Invoice',
			'rules' => 'required|min_length[4]'
		),
		'invoiceContactEmail' => array(
			'field' => 'invoiceContactEmail',
			'label' => 'Invoice Contact Email',
			'rules' => 'required|valid_email'
		),
		'orderID' => array(
			'field' => 'orderID',
			'label' => 'Order ID',
			'rules' => 'required'
		),
		'product' => array(
			'field' => 'product',
			'label' => 'Garment(s)/Product(s)',
			'rules' => 'required|min_length[2]'
		),
		'colors' => array(
			'field' => 'colors',
			'label' => 'Garment/Product Color(s)',
			'rules' => 'required|min_length[2]'
		),
		'priceQuoted' => array(
			'field' => 'priceQuoted',
			'label' => 'Price Quoted',
			'rules' => 'required'
		),
		'quantity' => array(
			'field' => 'quantity',
			'label' => 'Total Quantity',
			'rules' => 'required|is_natural_no_zero'
		),
		'dueDate' => array(
			'field' => 'dueDate',
			'label' => 'Date Needed',
			'rules' => 'required|min_length[10]'
		),
		'firmDeadline' => array(
			'field' => 'firmDeadline',
			'label' => 'Firm Deadline?',
			'rules' => 'required'
		),
		'recipientName' => array(
			'field' => 'recipientName',
			'label' => 'Recipient Name',
			'rules' => 'required'
		),
		'address1' => array(
			'field' => 'address1',
			'label' => 'Delivery Address',
			'rules' => 'required|min_length[2]'
		),
		'city' => array(
			'field' => 'city',
			'label' => 'Delivery City',
			'rules' => 'required|min_length[2]'
		),
		'state' => array(
			'field' => 'state',
			'label' => 'Delivery State',
			'rules' => 'required|min_length[2]|max_length[2]'
		),
		'zip' => array(
			'field' => 'zip',
			'label' => 'Delivery Zip Code',
			'rules' => 'required|min_length[5]|max_length[10]'
		),
		'paymentMethod' => array(
			'field' => 'paymentMethod',
			'label' => 'Payment Method',
			'rules' => 'required'
		),
		'bagAndTag' => array(
			'field' => 'bagAndTag',
			'label' => 'Bag & Tag?',
			'rules' => 'required'
		),
		'customNames' => array(
			'field' => 'customNames',
			'label' => 'Custom names or numbers?',
			'rules' => 'required'
		),
		'coordinatorFreebieSize' => array(
			'field' => 'coordinatorFreebieSize',
			'label' => 'Coordinator Freebie Size'
		)
	);

	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Validity checker functions & helper functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
}
