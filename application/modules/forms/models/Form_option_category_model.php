<?php
/********************************************************************************************************
/
// Form Option Categories
// Explanation: For grouping options together
/
********************************************************************************************************/
class Form_option_category_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'FormOptionCategories';
	public $primary_key = 'formOptCategoryID';

	//callbacks/observers (use function names)

	//relationships
	public $belongs_to = array(
		'form' => array('model' => 'forms/form_model', 'primary_key' => 'formID')
	);
	public $has_many = array(
		'options' => array('model' => 'forms/form_option_model', 'primary_key' => 'optionID', 'join_key' => 'formOptCategoryID')
	);

	//validation
	public $validate = array(
		'title' => array(
			'field' => 'title',
			'label' => 'Title',
			'rules' => 'required'
		),
		'url' => array(
			'field' => 'url',
			'label' => 'Url',
			'rules' => 'required'
		),
		'formID' => array(
			'field' => 'formID',
			'label' => 'Form',
			'rules' => 'required|is_natural_no_zero'
		),
	);

	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Validity checker functions & helper functions, may contain redirects
	*
	***********************************************************************************************/
}