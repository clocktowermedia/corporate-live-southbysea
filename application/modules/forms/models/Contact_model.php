<?php
/********************************************************************************************************
/
/ Contact Form
/
/ Stores information from the contact form
/
********************************************************************************************************/
class Contact_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'Contact';

	//primary key
	public $primary_key = 'contactID';

	//relationships
	public $belongs_to = array(
		'form_submissions' => array('model' => 'forms/form_submission_model', 'primary_key' => 'submissionID')
	);

	//callbacks/observers (use function names)

	//validation
	public $validate = array(
		'submissionID' => array(
			'field' => 'submissionID', 
			'label' => 'Submission ID',
			'rules' => 'required|is_natural_no_zero'
		)
	);
	
	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Validity checker functions & helper functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
}