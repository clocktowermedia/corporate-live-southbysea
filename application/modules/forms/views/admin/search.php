<div class="row">
	<div class="col-xs-12">

		<?php echo form_open('admin/forms/search'); ?>
		<fieldset>
			<?php
				//form type
				$this->form_builder->select('formUrl', 'Form Type', array('' => 'Select a Form') + $formTypes, (isset($submission['formUrl']))? $submission['formUrl'] : '', '', '', '', array('required' => 'required'));

				//search field type
				$this->form_builder->select('field', 'Search Field', array('' => 'Select a Form Type First'), (isset($submission['field']))? $submission['field'] : '', '', '', '', array('required' => 'required', 'placeholder' => 'Select a Field', 'data-value' => (isset($field['id']))? $field['id'] : '', 'data-value-text' => (isset($field['text']))? $field['text'] : ''));

				//value
				$this->form_builder->text('query', 'Search Value', (isset($submission['query']))? $submission['query'] : '', '', 'Search Value', '', '', array('required' => 'required'));

				//submit button
				$this->form_builder->button('submit', 'Search', array('inputClass' => 'btn btn-info'), '', '');
			?>
		</fieldset>
		</form>

		<?php if (isset($results) && count($results) > 0): ?>

			<hr>

			<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered datatable custom-table" width="100%">
				<thead>
					<tr>
						<th>First Name</th>
						<th>Last Name</th>
						<th data-hide="phone">Subject</th>
						<?php if (isset($field['id']) && $field['id'] != ''): ?>
							<th data-hide="tablet, phone"><?php echo $field['text'];?></th>
						<?php endif; ?>
						<th data-hide="phone" class="text-right">Date</th>
						<th></th>
					</tr>
				</thead>

				<tbody>
					<?php foreach ($results AS $result): ?>
						<tr>
							<td><?php echo $result['firstName']; ?></td>
							<td><?php echo $result['lastName']; ?></td>
							<td><?php echo $result['subject']; ?></td>
							<?php if (isset($field['id']) && $field['id'] != ''): ?>
								<td><?php echo $result[$field['id']]; ?></td>
							<?php endif; ?>
							<td><?php echo date('n/d/Y', strtotime($result['dateCreated']));?></td>
							<td>
								<a href="/admin/forms/read/<?php echo $result['submissionID']; ?>" class="btn btn-info" target="_blank"><i class="fa fa-eye"></i> View</a>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php else: ?>
			<h3>No Results Found</h3>
		<?php endif; ?>

	</div>
</div>
