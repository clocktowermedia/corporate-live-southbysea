<div class="mailbox row">
	<div class="col-xs-12">
		<div class="box box-solid">
			<div class="box-body">
				<div class="row">
					<div class="col-md-3 col-sm-4">
						<div class="box-header">
							<i class="fa fa-inbox"></i>
							<h3 class="box-title">INBOX</h3>
							<button id="refresh-inbox" type="button" class="btn pull-right">
								<i class="fa fa-refresh"></i>
								<span class="sr-only">Refresh</span>
							</button>
						</div>
						<div>
							<ul class="nav nav-pills nav-stacked" id="mailbox-form-types" data-value="<?php echo (isset($form['formUrl']))? $form['formUrl'] : ''; ?>">
								<li class="header">Folders</li>
								<li <?php if (!isset($form)): ?> class="active" <?php endif; ?>><a href="/admin/forms" data-filter-category=""><i class="fa fa-inbox"></i> All <span class="unread-count"></span></a></li>
								<?php if (isset($forms) && $forms != false):?>
									<?php foreach ($forms as $thisForm):?>
										<li <?php if (isset($form['formUrl']) && $form['formUrl'] == $thisForm['formUrl']): ?> class="active" <?php endif; ?>><a href="/admin/forms/category/<?php echo $thisForm['formUrl'];?>" data-filter-category="<?php echo $thisForm['formName'];?>"><i class="fa <?php echo $thisForm['adminIcon'];?>"></i> <?php echo $thisForm['formName'];?> <span class="unread-count"></span></a></li>
									<?php endforeach;?>
								<?php endif;?>
							</ul>
						</div>
					</div><!-- /.col (LEFT) -->

					<div class="col-md-9 col-sm-8" id="inbox-container">

						<div class="row" id="mailbox-header">
							<div class="col-xs-6">

								<div class="pull-left">
									<label class="check-all-label">
										<input type="checkbox" name="check-all" id="check-all"/>
										<span class="sr-only">Check All</span>
									</label>
								</div>

								<div class="btn-group">
									<button type="button" class="btn btn-default btn-sm btn-flat dropdown-toggle" data-toggle="dropdown">
										Action <span class="caret"></span>
									</button>
									<ul class="dropdown-menu" role="menu" id="mailbox-actions">
										<li><a href="#" data-hijack="read">Mark as Read</a></li>
										<li><a href="#" data-hijack="unread">Mark as Unread</a></li>
										<li class="divider"></li>
										<li><a href="#" data-hijack="deleted">Delete</a></li>
									</ul>
								</div>

								<div class="btn-group">
									<button type="button" class="btn btn-default btn-sm btn-flat dropdown-toggle" data-toggle="dropdown">
										Filter <span class="caret"></span>
									</button>
									<ul class="dropdown-menu" role="menu" id="mailbox-filters">
										<li class="active"><a href="#" data-view="">All</a></li>
										<li><a href="#" data-view="read">Read</a></li>
										<li><a href="#" data-view="unread">Unread</a></li>
									</ul>
								</div>

								<div class="btn-group">
									<button type="button" class="btn btn-default btn-sm btn-flat dropdown-toggle" data-toggle="dropdown">
										Sort <span class="caret"></span>
									</button>
									<ul class="dropdown-menu" role="menu" id="mailbox-sort">
										<li class="active"><a href="#" data-sort="desc">Descending</a></li>
										<li><a href="#" data-sort="asc">Ascending</a></li>
									</ul>
								</div>
							</div>

							<div class="col-xs-6">
								<div class="input-group" id="mailbox-search-form">
									<input type="text" name="mailbox-query" class="form-control input-sm" placeholder="Search" />
									<div class="input-group-btn">
										<div class="btn-group" role="group">
											<div class="dropdown dropdown-lg">
												<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>
												<div class="dropdown-menu dropdown-menu-right" role="menu">
													<form target="_blank" action="/admin/forms/search" class="form-horizontal" role="form" method="POST">
														<div class="form-group text-center"><h4>Search Forms</h4></div>
														<?php $this->form_builder->set_labels(false); ?>
														<?php $this->form_builder->select('formUrl', 'Search Submissions', array('' => 'Select a Form Type') + $formTypes, (isset($submission['formUrl']))? $submission['formUrl'] : '', '', '', '', array('required' => 'required')); ?>
														<div class="clearfix"></div>
														<?php $this->form_builder->select('field', 'Field to Search', array('' => 'Select a Form Type First'), (isset($submission['field']))? $submission['field'] : '', '', '', '', array('required' => 'required', 'data-url' => '/admin/forms/json/get-fields', 'placeholder' => 'Select a Field')); ?>
														<div class="clearfix"></div>
														<?php $this->form_builder->text('query', 'Search Value', (isset($submission['query']))? $submission['query'] : '', '', 'Search Value', '', '', array('required' => 'required')); ?>
														<div class="clearfix"></div>
														<?php $this->form_builder->button('submit', 'Search', array('inputClass' => 'btn btn-info pull-right'), '', ''); ?>
													</form>
												</div>
											</div>
											<button type="submit" name="q" class="btn btn-sm btn-info"><i class="fa fa-search"></i></button>
										</div>
									</div>
								</div>
							</div>

							<div class="col-sm-12">
								<div class="table-responsive">
									<table cellpadding="0" cellspacing="0" border="0" class="table datatable table-mailbox custom-table" width="100%">
										<thead>
											<tr>
												<th></th>
												<th>Name</th>
												<th data-hide="phone">Subject</th>
												<th>Form</th>
												<!--<th data-hide="tablet, phone">Confirmation #</th>-->
												<th data-hide="phone" class="text-right">Date</th>
											</tr>
										</thead>

										<tbody>
										</tbody>
									</table>
								</div>
							</div>

					</div><!-- /.col (RIGHT) -->
				</div><!-- /.row -->
			</div><!-- /.box-body -->

		</div><!-- /.box -->
	</div><!-- /.col (MAIN) -->
</div>