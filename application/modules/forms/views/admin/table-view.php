<table>
	<tbody>
		<?php $this->load->helper('array'); ?>

		<?php if (isset($fields)): ?>
			<?php foreach ($fields as $field): ?>
				<tr>
					<?php if (!isset($submission[$field]) || $field == ''): ?>
						<td valign="top"><br/></td>
						<td valign="top"><br/></td>
					<?php else: ?>
						<td valign="top"><?php echo format_key($field); ?>:</td>
						<td valign="top"><?php echo $submission[$field]; ?></td>
					<?php endif; ?>
				</tr>
			<?php endforeach; ?>
		<?php else: ?>
			<?php foreach ($submission as $key => $value): ?>
				<?php if (!in_array($key, $table_ignore)): ?>
					<tr>
						<td width="30%" valign="top"><?php echo format_key($key); ?>:</td>
						<td valign="top"><?php echo $value; ?></td>
					</tr>
				<?php endif; ?>
			<?php endforeach; ?>
		<?php endif; ?>

		<?php if (count($submission['images']) > 0): ?>
			<?php foreach ($submission['images'] AS $imageCount => $image): ?>
				<tr>
					<td>Image <?php echo $imageCount + 1; ?>:</td>
					<td>
						<?php $link = 'http://' . $_SERVER['HTTP_HOST'] . $image['fullpath']; ?>
						<a href="<?php echo $link; ?>"><?php echo $link; ?></a>
					</td>
				</tr>
			<?php endforeach; ?>
		<?php endif; ?>

		<?php if (count($submission['files']) > 0): ?>
			<?php foreach ($submission['files'] AS $fileCount => $file): ?>
				<tr>
					<td>File <?php echo $fileCount + 1; ?>:</td>
					<td>
						<?php $link = 'http://' . $_SERVER['HTTP_HOST'] . $file['fullpath']; ?>
						<a href="<?php echo $link; ?>"><?php echo $link; ?></a>
					</td>
				</tr>
			<?php endforeach; ?>
		<?php endif; ?>
	</tbody>
</table>