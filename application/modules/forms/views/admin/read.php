<div class="row">
	<div class="col-xs-12">

		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs" id="form-tabs">
				<li class="active"><a href="#form" data-toggle="tab">Form Submission</a></li>
				<li><a href="#text" data-toggle="tab">Form Text</a></li>
			</ul>

			<div class="tab-content">
				<div class="tab-pane" id="text">

					<div class="box-tools pull-right">
						<?php if ($submission['formUrl'] == 'rep-application'): ?>
							<a href="/admin/campus-managers/promote/<?php echo $submission['submissionID']; ?>"><button class="btn btn-info btn-sm"><i class="fa fa-institution"></i> Promote to Campus Rep</button></a>
						<?php endif; ?>
						<a href="mailto:<?php echo $submission['email'];?>"><button class="btn btn-info btn-sm"><i class="fa fa-envelope"></i> Reply</button></a>
						<a href="/admin/forms/unread/<?php echo $submission['submissionID'];?>"><button class="btn btn-info btn-sm"><i class="fa fa-undo"></i> Mark as Unread</button></a>
						<a href="/admin/forms"><button class="btn btn-info btn-sm"><i class="fa fa-inbox"></i> Back to Inbox</button></a>
					</div>
					<div class="clearfix"></div>

					<?php echo $table; ?>

				</div>

				<div class="tab-pane active" id="form">

					<div class="box-tools pull-right">
						<?php if ($submission['formUrl'] == 'rep-application'): ?>
							<a href="/admin/campus-managers/promote/<?php echo $submission['submissionID']; ?>"><button class="btn btn-info btn-sm"><i class="fa fa-institution"></i> Promote to Campus Rep</button></a>
						<?php endif; ?>
						<a href="mailto:<?php echo $submission['email'];?>"><button class="btn btn-info btn-sm"><i class="fa fa-envelope"></i> Reply</button></a>
						<a href="/admin/forms/unread/<?php echo $submission['submissionID'];?>"><button class="btn btn-info btn-sm"><i class="fa fa-undo"></i> Mark as Unread</button></a>
						<a href="/admin/forms"><button class="btn btn-info btn-sm"><i class="fa fa-inbox"></i> Back to Inbox</button></a>
					</div>
					<div class="clearfix"></div>

					<?php $this->load->view($submission['partialPath']); ?>

				</div>
			</div>
		</div>

	</div>
</div>

