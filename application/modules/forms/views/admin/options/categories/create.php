<div class="row">
	<div class="col-xs-12">
		<div class="form-horizontal">
			<?php echo form_open('admin/forms/options/categories/create'); ?>
			<fieldset>
				<?php $this->load->view('../modules/'. $this->router->fetch_module() .'/partials/admin/category');?>
				<?php
					//submit button
					$this->form_builder->button('submit', 'Add Category', array('containerClass' => 'col-sm-offset-1 col-sm-11', 'inputClass' => 'btn btn-info'), '', '');
				?>
			</fieldset>
			</form>
		</div>
	</div>
</div>