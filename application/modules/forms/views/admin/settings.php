<div class="row">
	<div class="col-xs-12">

		<div class="box box-primary">
			<div class="box-header" data-toggle="tooltip" title="" data-original-title="Actions">
				<h3 class="box-title">Form Email Settings</h3>
				<div class="box-tools pull-right">
					<button class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></button>
					<button class="btn btn-primary btn-xs" data-widget="remove"><i class="fa fa-times"></i></button>
				</div>
			</div>

			<div class="box-body no-pad-top">
				<hr class="no-pad-top">

				<?php echo form_open('admin/forms/settings'); ?>
				<fieldset>
					<?php
						foreach ($forms as $form)
						{
							echo '<div class="col-sm-6">';
								$this->form_builder->set_labels(true);
								$this->form_builder->set_class('groupClass', 'form-group mb-0');
								$this->form_builder->text('emails_' . $form['formUrl'], $form['formName'], $form['adminEmails'], '', '', 'Use a comma to separate multiple email addresses', '', array());
								$this->form_builder->set_labels(false);
								$this->form_builder->checkbox('ccuser_' . $form['formUrl'], 'CC User?', 0, 1, (isset($form['emailToUser']))? $form['emailToUser'] : '', '', '', '', array());
							echo '</div>';
						}

						//submit button
						echo '<div class="clearfix"></div>';
						$this->form_builder->button('submit', 'Submit', array('inputClass' => 'btn btn-info', 'containerClass' => 'col-xs-12'), '', '');
					?>
				</fieldset>
				</form>

			</div>
		</div>

	</div>
</div>

