<?php

class Forms extends MY_Controller {

	//layout variables
	public $defaultLayout;
	public $emailLayout;

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//set layout variable(s)
		$this->defaultLayout = $this->get_default_layout();
		$this->emailLayout = $this->get_email_layout();

		//load relevant models
		$this->load->model('pages/page_model');
		$this->load->model('forms/form_submission_model');
	}

	// action for submitting the contact form
	function contact()
	{
		//get form info
		$formInfo = $this->_get_formType_info('contact');

		//if the form was submitted, add to database
		if ($this->is_post())
		{
			//get form id
			$formInfo = $this->_get_formType_info($this->input->post('formType', TRUE));
			$_POST['formID'] = $formInfo['formID'];
			$_POST = $this->_format_post($this->input->post(NULL, TRUE));

			//load form validation library
			$this->load->library('form_validation');

			//get validation rules from model, set extra options as neccessary
			$validationRules = $this->form_submission_model->validate;

			//set form validation rules
			$this->form_validation->set_rules($validationRules);
			$this->form_validation->set_rules('comments', 'Comments', 'required');

			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the page and show form errors
				$this->session->set_flashdata('error', validation_errors());
				redirect($formInfo['redirectUrl']);

			} else {

				//save the form submission, send an email, redirect with success message
				$this->_save_submission($this->input->post(NULL, TRUE));
			}

		} else {
			$this->_error_occurred($formInfo, false);
		}
	}

	// action for submitting the catalog request form
	function catalog_request()
	{
		//get form info
		$formInfo = $this->_get_formType_info('catalog-request');

		//if the form was submitted, add to database
		if ($this->is_post())
		{
			//get form id
			$formInfo = $this->_get_formType_info($this->input->post('formType', TRUE));
			$_POST['formID'] = $formInfo['formID'];
			$_POST = $this->_format_post($this->input->post(NULL, TRUE));

			//load form validation library
			$this->load->library('form_validation');

			//get validation rules from model, set extra options as neccessary
			$validationRules = $this->form_submission_model->validate;

			//set form validation rules
			$this->form_validation->set_rules($validationRules);

			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the page and show form errors
				$this->session->set_flashdata('error', validation_errors());
				redirect($formInfo['redirectUrl']);

			} else {

				//save the form submission, send an email, redirect with success message
				$this->_save_submission($this->input->post(NULL, TRUE));
			}

		} else {
			$this->_error_occurred($formInfo, false);
		}
	}

	// action for submitting the chapter rep form
	function chapter_rep()
	{
		//get form info
		$formInfo = $this->_get_formType_info('chapter-rep');

		//if the form was submitted, add to database
		if ($this->is_post())
		{
			//get form id
			$formInfo = $this->_get_formType_info($this->input->post('formType', TRUE));
			$_POST['formID'] = $formInfo['formID'];
			$_POST = $this->_format_post($this->input->post(NULL, TRUE));

			//load form model
			$this->load->model('forms/' . $formInfo['formModel'], 'secondary_form_model');

			//load form validation library
			$this->load->library('form_validation');

			//get validation rules from model, set extra options as neccessary
			$validationRules = $this->form_submission_model->validate + $this->secondary_form_model->validate;
			unset($validationRules['submissionID'], $validationRules['comments']);

			//set form validation rules
			$this->form_validation->set_rules($validationRules);

			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the page and show form errors
				$this->session->set_flashdata('error', validation_errors());
				redirect($formInfo['redirectUrl']);

			} else {

				//save the form submission, send an email, redirect with success message
				$this->_save_submission($this->input->post(NULL, TRUE));
			}

		} else {
			$this->_error_occurred($formInfo, false);
		}
	}

	// action for submitting the chapter rep form
	function rep_application()
	{
		//get form info
		$formInfo = $this->_get_formType_info('rep-application');

		//if the form was submitted, add to database
		if ($this->is_post())
		{
			//get form id
			$formInfo = $this->_get_formType_info($this->input->post('formType', TRUE));
			$_POST['formID'] = $formInfo['formID'];
			$_POST = $this->_format_post($this->input->post(NULL, TRUE));

			//load form model
			$this->load->model('forms/' . $formInfo['formModel'], 'secondary_form_model');

			//load form validation library
			$this->load->library('form_validation');

			//get validation rules from model, set extra options as neccessary
			$validationRules = $this->form_submission_model->validate + $this->secondary_form_model->validate;
			unset($validationRules['submissionID'], $validationRules['comments']);

			//set form validation rules
			$this->form_validation->set_rules($validationRules);

			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the page and show form errors
				$this->session->set_flashdata('error', validation_errors());
				redirect($formInfo['redirectUrl']);

			} else {

				//save the form submission, send an email, redirect with success message
				$this->_save_submission($this->input->post(NULL, TRUE));
			}

		} else {
			$this->_error_occurred($formInfo, false);
		}
	}

	function school_suggestion()
	{
		//get form info
		$formInfo = $this->_get_formType_info('school-suggestion');

		//if the form was submitted, add to database
		if ($this->is_post())
		{
			//get form id
			$formInfo = $this->_get_formType_info($this->input->post('formType', TRUE));
			$_POST['formID'] = $formInfo['formID'];
			$_POST = $this->_format_post($this->input->post(NULL, TRUE));

			//load form model
			$this->load->model('forms/' . $formInfo['formModel'], 'secondary_form_model');

			//load form validation library
			$this->load->library('form_validation');

			//get validation rules from model, set extra options as neccessary
			$validationRules = $this->form_submission_model->validate + $this->secondary_form_model->validate;
			unset($validationRules['submissionID'], $validationRules['comments']);

			//set form validation rules
			$this->form_validation->set_rules($validationRules);

			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the page and show form errors
				$this->session->set_flashdata('error', validation_errors());
				redirect($formInfo['redirectUrl']);

			} else {

				//save the form submission, send an email, redirect with success message
				$this->_save_submission($this->input->post(NULL, TRUE));
			}

		} else {
			$this->_error_occurred($formInfo, false);
		}
	}

	//action for submitting the proof request form
	function quote_request()
	{
		//get form info
		$formInfo = $this->_get_formType_info('quote-request');

		//if the form was submitted, add to database
		if ($this->is_post())
		{
			//get form id
			$formInfo = $this->_get_formType_info($this->input->post('formType', TRUE));
			$_POST['formID'] = $formInfo['formID'];
			$_POST = $this->_format_post($this->input->post(NULL, TRUE));

			//generate confirmation number
			$_POST['confirmationNumber'] = $this->_generate_qq_id();

			//load form model
			$this->load->model('forms/' . $formInfo['formModel'], 'secondary_form_model');

			//load form validation library
			$this->load->library('form_validation');

			//get validation rules from model, set extra options as neccessary
			$validationRules = $this->form_submission_model->validate + $this->secondary_form_model->validate;
			unset($validationRules['submissionID']);

			//add subject and other validation rules
			switch ($this->input->post('page', TRUE))
			{
				case 'quote-request':
					$_POST['subject'] = $this->input->post('product', TRUE);
					if (strpos($_SERVER[REQUEST_URI], 'fraternity')){
						$formInfo['redirectUrl'] = '/fraternity/quote-request/submission-thank-you';
					}else{
						$formInfo['redirectUrl'] = $urlArray['path'];
					}
					break;
				case 'design':
					unset($validationRules['product']);
					$validationRules['design'] = array('field' => 'design', 'label' => 'Design', 'rules' => 'required|min_length[2]');
					$this->load->model('designs/design_model');
					$design = $this->design_model->get_by('url', $this->input->post('design', TRUE));
					$_POST['design'] = (isset($design['title']))? $design['title'] : $this->input->post('design', TRUE);
					$_POST['subject'] = (isset($design['title']))? 'Design - ' . $design['title'] : 'Design';
					$urlArray = parse_url($_SERVER['HTTP_REFERER']);
					if (strpos($_SERVER[REQUEST_URI], 'fraternity')){
						$formInfo['redirectUrl'] = '/fraternity/quote-request/submission-thank-you';
					}else{
						$formInfo['redirectUrl'] = $urlArray['path'];
					}
					break;
				case 'product':
					$this->load->model('products/product_model');
					$product = $this->product_model->get_by('url', $this->input->post('product', TRUE));
					$_POST['product'] = (isset($product['title']))? $product['title'] : $this->input->post('product', TRUE);
					$_POST['subject'] = (isset($product['title']))? 'Product - ' . $product['title'] : 'Product';
					$urlArray = parse_url($_SERVER['HTTP_REFERER']);
					if (strpos($_SERVER[REQUEST_URI], 'fraternity')){
						$formInfo['redirectUrl'] = '/fraternity/quote-request/submission-thank-you';
					}else{
						$formInfo['redirectUrl'] = $urlArray['path'];
					}
					break;
			}
			$this->form_validation->set_rules($validationRules);

			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the page and show form errors
				$this->session->set_flashdata('error', validation_errors());
				redirect($formInfo['redirectUrl']);

			} else {

				//save the form submission, send an email, redirect with success message
				$this->_save_submission($this->input->post(NULL, TRUE));
			}

		} else {
			$this->_error_occurred($formInfo, false);
		}
	}

	//action for submitting the final quote form
	function order_form()
	{
		///get form info
		$formInfo = $this->_get_formType_info('order-form');

		//if the form was submitted, add to database
		if ($this->is_post())
		{
			//get form id
			$formInfo = $this->_get_formType_info($this->input->post('formType', TRUE));
			unset($_POST['extraSizes']);
			$_POST['formID'] = $formInfo['formID'];
			$_POST = $this->_format_post($this->input->post(NULL, TRUE));
			$_POST['sizeBreakdown'] = $this->_format_finalsub_sizes();

			//load form model
			$this->load->model('forms/' . $formInfo['formModel'], 'secondary_form_model');

			//load form validation library
			$this->load->library('form_validation');

			//get validation rules from model, set extra options as neccessary
			$validationRules = $this->form_submission_model->validate + $this->secondary_form_model->validate;
			unset($validationRules['submissionID'], $validationRules['comments']);
			$this->form_validation->set_rules($validationRules);

			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the page and show form errors
				$this->session->set_flashdata('error', validation_errors());
				redirect($formInfo['redirectUrl']);

			} else {
				//try to find the matching potential TODO, this is just a guess, unsure which field will link..
				/*
				if (ENVIRONMENT == 'development')
				{
					if ($this->input->post('orderID') != '')
					{
						$this->load->model('potentials/potential_model');
						$potential = $this->potential_model->select('potentialID')->get_by('potentialID', (int) $this->input->post('orderID', TRUE));

						if (!empty($potential) && $potential !== false)
						{
							//extract data that matches
							$potential_keys = $this->potential_model->list_fields();
							$potential_keys = array_combine(array_values($potential_keys), array_values($potential_keys));
							$insertData = array_intersect_key($this->input->post(NULL, TRUE), $potential_keys);

							//save
							$this->potential_model->skip_validation()->update($potential['potentialID'], $insertData);
						}
					}
				}
				*/

				//save the form submission, send an email, redirect with success message
				$this->_save_submission($this->input->post(NULL, TRUE));
			}

		} else {
			$this->_error_occurred($formInfo, false);
		}
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

	/**
	 * Save the form submission from post
	 * @param  [type] $data [description]
	 * @return [type]	   [description]
	 */
	private function _save_submission($data = null)
	{
		//make sure form type was passed
		if (isset($data['formType']))
		{
			//get form type from post
			$formInfo = $this->_get_formType_info($data['formType']);

			//get redirect url if applicable then unset
			if ($data['formType'] == 'quote-request')
			{
					$urlArray = parse_url($_SERVER['HTTP_REFERER']);
					$redirectUrl = $urlArray['path'];
				#$formInfo['redirectUrl'] .= '?from=' . $redirectUrl;
			}

			//get form id and unset stuff we don't need
			unset($data['formType']);
			$data['formID'] = $formInfo['formID'];

			//split up data to prepare it for submission, start by getting the fields in form submission model
			$submissionFields = $this->form_submission_model->list_fields();

			//unset duplicate data and if applicable
			$this->load->helper('array');
			$secondaryData = array_unset_keys($data, $submissionFields);
			$submissionData = (count($secondaryData) > 0)? array_unset_keys($data, array_keys($secondaryData)) : $data;

			//submit into form submission db
			$submissionID = $this->form_submission_model->insert($submissionData);

			//redirect if an error occurred
			if ((int) $submissionID <= 0)
			{
				$this->_error_occurred($formInfo);
			}

			//submit secondary information
			if (count($secondaryData) > 0)
			{
				//add submission id
				$secondaryData['submissionID'] = $submissionID;

				//if there are images/files, handle those
				if (isset($secondaryData['images']) && is_array($secondaryData['images']))
				{
					//load model
					$this->load->model('forms/form_image_model');
					foreach ($secondaryData['images'] AS $imageID)
					{
						$this->form_image_model->skip_validation()->update($imageID, array('submissionID' => $submissionID));
					}

					$secondaryData['images'] = implode(',', $secondaryData['images']);
				}

				if (isset($secondaryData['image']))
				{
					if (isset($_FILES[$secondaryData['image']]['name']) && $_FILES[$secondaryData['image']]['name'] != '')
					{
						$imageID = modules::run('forms/file_handler/_upload_image', array('submissionID' => $submissionID), $formInfo['formUrl']);
						if (!is_int($imageID) || $imageID <= 0)
						{
							$this->session->set_flashdata('error',  $_FILES[$secondaryData['image']]['name'] . ' - ' . $imageID);
							redirect($redirectUrl);
						} else {
							$fileArray[] = $imageID;
							$secondaryData['image'] = $imageID;
						}
					}
				}

				if (isset($_FILES) && count($_FILES) > 0 && isset($secondaryData['files']))
				{
					//get list of files
					$fileList = explode(',', $secondaryData['files']);
					$fileArray = array();
					foreach ($fileList as $file)
					{
						if (isset($_FILES[$file]['name']) && $_FILES[$file]['name'] != '')
						{
							$fileID = modules::run('forms/file_handler/_upload_file', $file, array('submissionID' => $submissionID, 'fieldName' => $file), $formInfo['formUrl']);
							if (!is_int($fileID) || $fileID <= 0)
							{
								$this->session->set_flashdata('error',  $_FILES[$file]['name'] . ' - ' . $fileID);
								redirect($redirectUrl);
							} else {
								$fileArray[] = $fileID;
								$secondaryData[$file] = $fileID;
							}
						}
					}
					$secondaryData['files'] = implode(',', $fileArray);
				}

				//submit into second db
				$this->load->model('forms/' . $formInfo['formModel'], 'secondary_form_model');
				$secondaryDataID = $this->secondary_form_model->insert($secondaryData);

				if ((int) $secondaryDataID <= 0)
				{
					$this->_error_occurred($formInfo);
				}
			}

			//send an email if applicable
			/*
			if ($formInfo['emailToUser'] == true)
			{
				$this->_send_email($formInfo);
			}
			commented out because we are just cc'ing users with the admin email
			*/

			//send to admin as well if applicable
			if ($formInfo['emailToAdmin'] == true){
				$this->_send_admin_email($formInfo, $submissionID, $secondaryData);
			}


			//show success message & redirect
			$this->_success($formInfo);

		} else {
			$this->_error_occurred();
		}
	}

	/**
	 * Show the user an error message and redirect
	 * @param  [type] $formInfo [description]
	 * @return [type]		   [description]
	 */
	private function _error_occurred($formInfo = null, $showErrors = true)
	{
		//set message & redirect location
		$errorMsg = (!is_null($formInfo) && $formInfo != false)? $formInfo['errorContent'] : 'An error occurred. Please try again.';
		$redirect = (!is_null($formInfo) && $formInfo != false)? $formInfo['redirectUrl'] : '/';

		//actually redirect them
		if ($showErrors == true)
		{
			$this->session->set_flashdata('error', $errorMsg);
		}
		redirect($redirect);
	}

	/**
	 * Show the user a success message and redirect
	 * @param  [type] $formInfo [description]
	 * @return [type]		   [description]
	 */
	private function _success($formInfo = null)
	{
		//set message & redirect location
		$successMsg = (!is_null($formInfo) && $formInfo != false)? $formInfo['thankYouContent'] : 'Your form was successfully submitted.';
		$redirect = (!is_null($formInfo) && $formInfo != false)? $formInfo['redirectUrl'] : '/';

		if (strpos($_SERVER['HTTP_REFERER'], 'fraternity')){
			if ($redirect === "/contact"){
				$redirect = '/fraternity/contact';
			}
			if ($redirect === "/quote-request/submission-thank-you"){
				$redirect = '/fraternity/quote-request/submission-thank-you';
			}
			if ($redirect === "/order-form"){
				$redirect = '/fraternity/order-form';
			}
			if ($redirect === "/campus-managers/apply"){
				$redirect = '/fraternity/campus-managers/apply';
			}
			if ($redirect === "/rep-submission"){
				$redirect = '/fraternity/rep-submission';
			}
			if ($redirect === "/catalog/request/thank-you"){
				$redirect = '/fraternity/catalog/request/thank-you';
			}
			if ($redirect === "/airstream"){
				$redirect = '/fraternity/airstream';
			}
		}

		//actually redirect them, lol
		$this->session->set_flashdata('success', $successMsg);
		redirect($redirect);
	}

	/**
	 * Sends an email confirmation to site users
	 * @param  [type] $formInfo [description]
	 * @return [type]		   [description]
	 */
	private function _send_email($formInfo = null)
	{
		//load email & parser library
		$this->load->library('email');
		$this->load->library('parser');
		$this->config->load('email');

		//send confirmation to user
		$this->email->to($this->input->post('email', TRUE), $this->input->post('firstName', TRUE) . ' ' . $this->input->post('lastName', TRUE));

		//set reply and from info
		$this->email->from($this->config->item('site_email'), $this->config->item('site_email_name'));
		$this->email->reply_to($this->config->item('site_email'), $this->config->item('site_email_name'));

		//email subject
		$data['subject'] = ($formInfo['emailSubjectUser'] != '')? $formInfo['emailSubjectUser'] : $this->config->item('site_name') . ' ' . $formInfo['formName'] . ' Confirmation';

		//get content
		$data['additionalContent'] = ($formInfo['emailContentUser'] != '')? $formInfo['emailContentUser'] : '<p>Thank you for your form submission. We will get back to you shortly.</p>';

		//format content into table
		$data['content'] = $this->convert_text_to_html_email_table($data['subject'], $data['additionalContent']);
		$this->load->library('css_to_inline_styles');
		$this->css_to_inline_styles->setHtml($data['content']);
		$this->css_to_inline_styles->setCSS(file_get_contents(APPPATH . '../assets/css/email.css'));
		$data['html'] = $this->css_to_inline_styles->convert();

		//tell it what view to use & get content
		$data['view'] = $this->get_email_view();
		$content = $this->parser->parse($this->emailLayout, $data, TRUE);

		//add content to email
		$this->email->subject($data['subject']);
		$this->email->message($content);

		//send email
		$this->email->sbs_send();
	}

	private function _admin_email($formInfo = null)
	{
		if ($formInfo['emailToUser'] == true)
		{
			$emails['user'] = $this->input->post('email', TRUE);
			$emails['admins'] = $this->config->item('dev_email');

		} else {

		}
	}

	/**
	 * Sends an email to site admins
	 * @param  [type] $formInfo [description]
	 * @return [type]		   [description]
	 */
	private function _send_admin_email($formInfo = null, $submissionID = 0, $secondaryData = null)
	{
		//load parser
		$this->load->library('parser');

		//email subject
		$data['subject'] = ($formInfo['emailSubjectAdmin'] != '')? $formInfo['emailSubjectAdmin'] : $this->config->item('site_name') . ' ' . str_replace(' Form', '', $formInfo['formName']);
		if ($formInfo['addFieldToSubject'] == TRUE)
		{
			$formData = $this->form_submission_model->getFullByID($submissionID, (count($secondaryData) > 0)? TRUE : FALSE);
			$data['subject'] .= (is_numeric($formData[$formInfo['fieldInSubject']]))? ' #' : ' ';
			$data['subject'] .= $formData[$formInfo['fieldInSubject']];
		}

		//additional content
		$data['additionalContent'] = ($formInfo['emailContentAdmin'] != '')? $formInfo['emailContentAdmin'] : '';

		//if there are images or files, link to them
		if ($this->input->post('images', TRUE) != '')
		{
			//load model
			$this->load->model('forms/form_image_model');
			$images = (is_array($this->input->post('images')))? $this->input->post('images', TRUE) : explode(',', $this->input->post('images', TRUE));
			foreach ($images AS $key => $imageID)
			{
				$image = $this->form_image_model->get($imageID);
				$link = 'http://' . $_SERVER['HTTP_HOST'] . $image['fullpath'];
				$_POST['image ' . ($key+1)] = '<a href="' . $link . '">' . $link . '</a>';
			}
		}
		if ($this->input->post('image', TRUE) != '')
		{
			//load model
			$this->load->model('forms/form_image_model');
			$image = $this->form_image_model->get($this->input->post('image', TRUE));
			$link = 'http://' . $_SERVER['HTTP_HOST'] . $image['fullpath'];
			$_POST['image'] = '<a href="' . $link . '">' . $link . '</a>';
		}
		if ($this->input->post('files', TRUE) != '')
		{
			//load model
			$this->load->model('forms/form_file_model');
			$files = (is_array($this->input->post('files', TRUE)))? $this->input->post('files', TRUE) : explode(',', $this->input->post('files', TRUE));
			foreach ($files AS $key => $fileID)
			{
				$file = $this->form_file_model->get($fileID);
				$link = 'http://' . $_SERVER['HTTP_HOST'] . $file['fullpath'];
				$link = '<a href="' . $link . '">' . $link . '</a>';
				if (isset($file['fieldName']) && $file['fieldName'] != '')
				{
					$_POST[$file['fieldName']] = $link;
				} else {
					$_POST['file ' . ($key+1)] = $link;
				}
			}
		}

		//unset extra content
		unset($_POST['formType'], $_POST['images'], $_POST['files'], $_POST['formID'], $_POST['submissionID'], $_POST['subject']);

		//email customization
		$postData = $this->_email_customization($formInfo, $this->input->post(NULL, TRUE));

		//format content into table
		$data['content'] = $this->convert_1D_array_to_html_email_table($data['subject'], $postData, $data['additionalContent']);
		$data['content'] = $this->_htmlentitiesExcludeHTMLTags($data['content'], ENT_QUOTES);
		$this->load->library('css_to_inline_styles');
		$this->css_to_inline_styles->setHtml($data['content']);
		$this->css_to_inline_styles->setCSS(file_get_contents(APPPATH . '../assets/css/email.css'));
		$data['html'] = $this->css_to_inline_styles->convert();

		//tell it what view to use & get content
		$data['view'] = $this->get_email_view();
		$content = $this->parser->parse($this->emailLayout, $data, TRUE);

		//load email & parser library
		$this->load->library('email');
		$this->config->load('email');

		//build email array for sending
		if ($formInfo['emailToUser'] == true)
		{
			$emails[] = array('type' => 'user', 'email' => $this->input->post('email', TRUE));

		}
		#$adminEmails = $this->config->item('dev_email');
		$adminEmails = ($formInfo['adminEmails'] != '')? ',' . $formInfo['adminEmails'] : '';
		$emails[] = array('type' => 'admin', 'email' => $adminEmails);

		foreach ($emails as $email)
		{
			//set reply and from info
			$this->email->from($this->config->item('site_email'), $this->config->item('site_email_name'));
			$this->email->reply_to($this->input->post('email', TRUE), $this->input->post('firstName', TRUE) . ' ' . $this->input->post('lastName', TRUE));

			$this->email->to($email['email']);
			$this->email->bcc('forms@southbysea.com', 'South by Sea [Form Email]');

			//add content to email
			$this->email->subject($data['subject']);
			$this->email->message($content);

			//build CSV
			if ($email['type'] == 'admin')
			{
				$whitespace = array(
					'quote-request' => array('frontDesign', 'backDesign', 'comments')
				);
				foreach ($this->input->post(null, TRUE) AS $key => $value) //client does not want to use special formatting here for now
				{
					if (isset($whitespace[$formInfo['formUrl']]) && in_array($key, $whitespace[$formInfo['formUrl']]))
					{
						$csvArray[] = array('', '');
					}
					$csvArray[] = array(format_key($key), strip_tags($value));
				}

				$this->load->library('format', $csvArray);
				$this->load->helper('file');
				$csvData = $this->format->to_csv();
				$path = 'assets/uploads/' . $formInfo['formUrl'] . '-' . date('y-m-d') . '.csv';
				write_file($path, $csvData);
				$this->email->attach($path);
			}

			//send email
			$this->email->sbs_send();

			//delete file if applicable
			if (isset($path) && $path != '')
			{
				unlink($_SERVER['DOCUMENT_ROOT'] . '/' . $path);
			}
		}
	}

	function _htmlentitiesExcludeHTMLTags($htmlText, $ent)
	{
	    $matches = Array();
	    $sep = '###HTMLTAG###';

	    preg_match_all(":</{0,1}[a-z]+[^>]*>:i", $htmlText, $matches);

	    $tmp = preg_replace(":</{0,1}[a-z]+[^>]*>:i", $sep, $htmlText);
	    $tmp = explode($sep, $tmp);

	    for ($i=0; $i<count($tmp); $i++)
	    {
	        $tmp[$i] = htmlentities($tmp[$i], $ent, 'UTF-8', false);
	    }

	    $tmp = join($sep, $tmp);

	    for ($i=0; $i<count($matches[0]); $i++)
	    {
	        $tmp = preg_replace(":$sep:", $matches[0][$i], $tmp, 1);
	    }

	    return $tmp;
	}

	function _get_formType_info($formType = '')
	{
		//make sure we have a valid form type
		if (is_null($formType) || $formType == '')
		{
			$this->_error_occurred(null, false);
		}

		//get form type from post
		$this->load->model('form_model');
		$formInfo = $this->form_model->get_by('formUrl', $formType);

		if (empty($formInfo) || $formInfo == false)
		{
			$this->_error_occurred(null, false);
		}

		return $formInfo;
	}

	function _format_post()
	{
		//format textareas
		if ($this->input->post('textarea_list', TRUE) != '')
		{
			$textareas = explode(',', $this->input->post('textarea_list', TRUE));
			foreach ($textareas as $textarea)
			{
				$_POST[$textarea] = str_replace("\n",'<br/>', $this->input->post($textarea, TRUE));
			}
		}
		unset($_POST['textarea_list']);
		return $this->input->post(NULL, TRUE);
	}

	function _generate_qq_id()
	{
		//set confirmation #
		$this->load->helper('string');
		$string = random_string('alnum', 16);

		//make sure it doesnt already exist
		$this->load->model('forms/proof_request_model');
		$exists = $this->proof_request_model->get_by('confirmationNumber', $string);
		if (empty($exists) || $exists == false)
		{
			return $string;
		} else {
			$this->_generate_qq_id();
		}
	}

	function _format_finalsub_sizes(array $postData = array())
	{
		$postData = ($this->input->post(NULL, TRUE))? $this->input->post(NULL, TRUE) : $postData;

		$this->load->model('products/size_model');
		$sizes = $this->size_model->order_by('displayOrder', 'ASC')->get_all();

		//loop through each color/style first
		if (count($postData['garmentStyle']) > 0)
		{
			foreach ($postData['garmentStyle'] AS $key => $style)
			{
				//loop through sizes
				if (count($sizes) > 0)
				{
					foreach ($sizes AS $size)
					{
						if (isset($postData['size_' . $size['sizeID']][$key]) && $postData['size_' . $size['sizeID']][$key] !== '')
						{
							$entry = $size['abbrev'] . ': ' .  $postData['size_' . $size['sizeID']][$key] . ' ' . $style;
							$garmentRow[] = ($size['showByDefault'] == FALSE)? '<em>' . $entry . '</em>' : $entry;
						}
					}

					$dataRows[] = implode('<br/>', $garmentRow);
					unset($garmentRow);
				}
			}

			//unset this and all sizes
			unset($_POST['garmentStyle']);
			if (count($sizes) > 0)
			{
				foreach ($sizes AS $size)
				{
					unset($_POST['size_' . $size['sizeID']]);
				}
			}
		}

		return (isset($dataRows) && count($dataRows) > 0)? implode('<br/><br/>', $dataRows) : '';
	}

	function _format_finalsub_address(array $formData = array(), $removeExtra = TRUE)
	{
		$this->load->helper('array');
		$insertData['address'] = $formData['recipientName'] . "<br/>" . $formData['address1'] . "<br/>";
		$insertData['address'] .= ($formData['address2'] != '')? $formData['address2'] . "<br/>" : '';
		$insertData['address'] .= $formData['city'] . ', ' . $formData['state'] . ' ' . $formData['zip'];
		$returnData = array_splice_after_key($formData, 'recipientName', $insertData);

		if ($removeExtra == TRUE)
		{
			unset($returnData['address1'], $returnData['address2'], $returnData['city'], $returnData['state'], $returnData['zip'], $returnData['recipientName']);
		}
		return $returnData;
	}

	function _email_customization(array $formInfo = array(), array $postData = array())
	{
		if ($formInfo['formUrl'] == 'order-form')
		{
			$postData = $this->_format_finalsub_address($postData);
		}

		return $postData;
	}
}
