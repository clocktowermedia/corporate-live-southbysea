<?php

class Admin_option_categories extends Admin_controller {

	//layouts
	protected $defaultLayout;

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//set layout variable(s)
		$this->defaultLayout = $this->get_admin_layout();

		//load relevant models
		$this->load->model('forms/form_option_category_model');
	}

	/**
	 * View with a listing of form option categories
	 */
	function index()
	{
		//tell it what view to use
		$data['view'] = $this->_get_custom_view();
		$data['title'] = 'Form Option Categories';

		//load the page
		$this->load->view($this->defaultLayout, $data);
	}

	function options($categoryID = 0)
	{
		//make sure category is valid
		$this->_category_is_valid($categoryID);

		//get category
		$data['category'] = $this->form_option_category_model->get($categoryID);

		//tell it what view to use
		$data['view'] = $this->_get_custom_view();
		$data['title'] = 'Options for ' . $data['category']['title'];

		//load the page
		$this->load->view($this->defaultLayout, $data);
	}

	function create()
	{
		//tell it what view file to use
		$data['view'] = $this->_get_custom_view();
		$data['title'] = 'Add Option Category';

		//load form validation library
		$this->load->library('form_validation');

		//get validation rules from model, set extra options as neccessary
		$validationRules = $this->form_option_category_model->validate;
		$this->form_validation->set_rules($validationRules);

		//if the form was submitted, add to database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the page and show form errors
				$this->load->view($this->defaultLayout, $data);
			} else {

				//update record in db
				$categoryID = $this->form_option_category_model->insert($this->input->post(NULL, TRUE));

				if (!is_int($categoryID) || (int) $categoryID <= 0)
				{
					//show error message
					$this->session->set_flashdata('fail', 'An error occured, please try again.');
					redirect('/admin/forms/options/categories/create');
				} else {
					//display success message & redirect
					$this->session->set_flashdata('success', 'Option category successfully added.');
					redirect('/admin/forms/options/categories');
				}
			}
		} else {
			// tell it what layout to use and load the page
			$this->load->view($this->defaultLayout, $data);
		}
	}

	function edit($categoryID = 0)
	{
		//make sure category is valid
		$this->_category_is_valid($categoryID);

		//tell it what view file to use
		$data['view'] = $this->_get_custom_view();
		$data['title'] = 'Edit Option Category';

		//get option info
		$data['category'] = $this->form_option_category_model->get($categoryID);

		//load form validation library
		$this->load->library('form_validation');

		//get validation rules from model, set extra options as neccessary
		$validationRules = $this->form_option_category_model->validate;
		unset($validationRules['url']);
		$this->form_validation->set_rules($validationRules);

		//if the form was submitted, add to database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the page and show form errors
				$this->load->view($this->defaultLayout, $data);
			} else {

				//update record in db
				$this->form_option_category_model->skip_validation()->update($categoryID, $this->input->post(NULL, TRUE));

				//display success message & redirect
				$this->session->set_flashdata('success', 'Option category successfully edited.');
				redirect('/admin/forms/options/categories');
			}
		} else {
			// tell it what layout to use and load the page
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/
	private function _get_custom_view()
	{
		return 'admin/options/categories/' . $this->get_view();
	}

	/**
	* Checks to see that category ID exists
	*/
	public function _category_is_valid($categoryID = 0)
	{
		//make sure contact form category id is an integer
		$categoryID = (int) $categoryID;

		//make sure contact form category id is greater than zero
		if ($categoryID > 0)
		{
			//check to see if the contact form category exists
			$exists = $this->form_option_category_model->get($categoryID);

			if ($exists == false)
			{
				$this->session->set_flashdata('error', 'Invalid option category id.');
				redirect('/admin/forms/options/categories');
			}

		} else {
			$this->session->set_flashdata('error', 'Invalid option category id.');
			redirect('/admin/forms/options/categories');
		}
	}
}