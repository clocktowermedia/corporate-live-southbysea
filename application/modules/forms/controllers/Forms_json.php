<?php
class Forms_json extends MY_Controller {

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//load relevant models
		$this->load->model('forms/form_model');
		$this->load->model('forms/form_submission_model');
	}

	/**
	 * Upload image
	 * @return json
	 */
	function upload_image()
	{
		try
		{
			//make sure we are posting
			if (!$this->is_post())
			{
				throw new Exception('Must post data.');
			}

			//make sure valid form type was sent
			$formInfo = $this->form_model->get_by('formUrl', (string) $this->input->post('formType', TRUE));
			if (empty($formInfo) || $formInfo == false)
			{
				throw new Exception('Invalid form type.');
			}

			//try to upload the file
			unset($_POST['formType']);
			$formData = ($this->input->post(NULL, TRUE) == false)? array() : $this->input->post(NULL, TRUE);
			$imageID = modules::run('forms/file_handler/_upload_image', $formData, $formInfo['formUrl']);

			//check to see if we got a image uploaded
			if (is_int($imageID) && $imageID > 0)
			{
				//load model
				$this->load->model('forms/form_image_model');
				$image = $this->form_image_model->get($imageID);

				//if image upload was successful, return the image id
				$data['status'] = 'ok';
				$data['image'] = $image;
				$data['message'] = 'Successfully uploaded image.';
			} else {
				throw new Exception($imageID);
			}

		} catch (Exception $e) {
			set_status_header(400, strip_tags($e->getMessage()));
			$data['status'] = 'fail';
			$data['message'] = strip_tags($e->getMessage());
		}

		$this->render_json($data);
	}

	/**
	 * Upload file
	 * @return json
	 */
	function upload_file()
	{
		try
		{
			//make sure we are posting
			if (!$this->is_post())
			{
				throw new Exception('Must post data.');
			}

			//make sure valid form type was sent
			$formInfo = $this->form_model->get_by('formUrl', (string) $this->input->post('formType', TRUE));
			if (empty($formInfo) || $formInfo == false)
			{
				throw new Exception('Invalid form type.');
			}

			//try to upload the file
			unset($_POST['formType']);
			$formData = ($this->input->post(NULL, TRUE) == false)? array() : $this->input->post(NULL, TRUE);
			$fileID = modules::run('forms/file_handler/_upload_file', $formData, $formInfo['formUrl']);

			//check to see if we got a file uploaded
			if (is_int($fileID) && $fileID > 0)
			{
				//load model
				$this->load->model('forms/form_file_model');
				$file = $this->form_file_model->get($fileID);

				//if file upload was successful, return the file id
				$data['status'] = 'ok';
				$data['file'] = $file;
				$data['message'] = 'Successfully uploaded file.';
			} else {
				throw new Exception($fileID);
			}

		} catch (Exception $e) {
			set_status_header(400, strip_tags($e->getMessage()));
			$data['status'] = 'fail';
			$data['message'] = strip_tags($e->getMessage());
		}

		$this->render_json($data);
	}

	/**
	 * Used to get a list of options for select boxes/checklists in forms throughout the site
	 * @param  string $request   [which type of dropdown do you want]
	 * @param  string $fieldType [what type of field is this - select or checklist?]
	 * @return [json]            [json array of options]
	 */
	public function get_dropdown($request = 'yes-no', $fieldType = 'select')
	{
		try
		{
			if ($request == 'countries')
			{
				$this->load->helper('dropdown');
				$formOptions = allCountries();
			} else if ($request == 'states') {
				$this->load->helper('dropdown');
				$formOptions = states();
			} else {
				//load this model to the view as we will use its methods
				$this->load->model('forms/form_option_model');
				$formOptions = $this->form_option_model->getByFieldName($request, 'select2');
			}

			//format ths results into what select2 needs
			if ($formOptions != false && count($formOptions) > 0)
			{
				foreach ($formOptions as $key => $value)
				{
					if ($fieldType == 'checklist')
					{
						$data['options'][] = array('value' => $key, 'text' => $value);
					} else {
						$data['options'][] = array('id' => $key, 'text' => $value);
					}
				}

			} else {

				//try to see if this is a hierarchy instead
				$formOptionsHierarchy = $this->form_option_model->getByFieldNameHierarchy($request);

				if ($formOptionsHierarchy != false && count($formOptionsHierarchy) > 0)
				{
					foreach ($formOptionsHierarchy as $optGroup)
					{
						//add children
						if (isset($optGroup['children']) != false && count($optGroup['children']) > 0)
						{
							foreach ($optGroup['children'] as $child)
							{
								$children[] = array('id' => $child['value'], 'text' => $child['text']);
							}
							$formatted['children'] = $children;
						} else {
							$children = array();
						}

						//add group to array
						$formatted = array('text' => $optGroup['text'], 'children' => $children);
						if (isset($optGroup['value'])) { $formatted['id'] = $optGroup['value']; }
						$data['options'][] = $formatted;
					}
				} else {
					throw new Exception('Invalid dropdown category.');
				}

				//send data back to view
				$data['status'] = 'ok';
				$data['message'] = 'Successfully retrieved options.';
			}
		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}

	/**
	 * Pass a dropdown category id and value, find match and return autopopulate info
	 */
	public function get_linked_fields()
	{
		try
		{
			//make sure we are posting
			if (!$this->is_get())
			{
				throw new Exception('Must get data.');
			}

			//make sure valid form type was sent
			$categoryID = (int) $this->input->get('categoryID', TRUE);
			if ($categoryID <= 0)
			{
				throw new Exception('Invalid category.');
			}

			//load model, try to find matching value
			$this->load->model('forms/form_option_model');
			$match = $this->form_option_model->select('linkedFields')->get_by(array(
				'formOptCategoryID' => $categoryID,
				'value' => $this->input->get('value', TRUE)
			));
			if (empty($match) || $match == false)
			{
				throw new Exception('Could not find that option.');
			}

			//show success message
			$data['linkedFields'] = json_decode(str_replace("'", '"', $match['linkedFields']), true);
			$data['status'] = 'ok';
			$data['message'] = 'Retrived linked fields.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}

	public function get_allowed_files()
	{
		try
		{
			if (!$this->is_get())
			{
				throw new Exception('Must get data.');
			}

			//make sure valid parameters were sent
			if ($this->input->get('form') == '' && $this->input->get('field') == '')
			{
				throw new Exception('Invalid form or field name.');
			}

			//get allowed file extensions
			$fileInfo = modules::run('forms/file_handler/_set_file_options', $this->input->get('form', TRUE), $this->input->get('field', TRUE));

			//get file extensions
			$fileTypes = array_unique(explode('|', strtolower($fileInfo['fileTypes'])));

			//format to mime type
			if ($this->input->get('format') == 'mimes')
			{
				if (count($fileTypes) > 0)
				{
					$mimes = get_mimes();
					$mimeTypes = array();
					foreach ($fileTypes AS $extension)
					{
						$mimeTypes = array_merge($mimeTypes, $mimes[$extension]);
					}

					$fileTypes = $mimeTypes;
				}
			}

			$data['fileTypes'] = $fileTypes;
			$data['status'] = 'ok';
			$data['message'] = 'Retrived file types.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}

	public function get_mime_list()
	{
		try
		{
			if (!$this->is_get())
			{
				throw new Exception('Must get data.');
			}

			//make sure valid parameters were sent
			if ($this->input->get('extensions') == '')
			{
				throw new Exception('Invalid extension list.');
			}

			//get file extensions
			$fileTypes = explode(',', $this->input->get('extensions'));
			if (count($fileTypes) > 0)
			{
				$mimes = get_mimes();
				$mimeTypes = array();
				foreach ($fileTypes AS $extension)
				{
					$mimeTypes = array_merge($mimeTypes, $mimes[$extension]);
				}

				$fileTypes = $mimeTypes;
			}

			$data['mimes'] = $fileTypes;
			$data['status'] = 'ok';
			$data['message'] = 'Retrived mimes.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/
}