<?php
class Admin_datatables extends Admin_controller {

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		$this->load->library('datatables');
	}

	/**
	* uses datatables library to get list of form submissions in a json format for use in admin index
	*/
	function data($return = false)
	{
		//get form info if applicable
		if ($this->input->post('formUrl') != '')
		{
			$this->load->model('forms/form_model');
			$form = $this->form_model->get_by('formUrl', $this->input->post('formUrl', TRUE));
			$formModel = 'forms/' . $form['formModel'];
			$this->load->model($formModel);
		}

		$this->datatables->select('"" AS checkbox', false)
			->edit_column('checkbox', '<input type="checkbox" class="mailbox-checkbox" value="$1" name="messages[]" data-form="contact">', 'c.submissionID')
			->select('CONCAT(firstName, " ", lastName) AS userName', false)
			->select('subject, f.formName')
			->from('Forms f')
			->join('FormSubmissions c', 'c.formID = f.formID');

		if ($this->input->post('formUrl') != '')
		{
			$this->datatables->where('f.formID', $form['formID']);

			$fieldExists = $this->{$form['formModel']}->field_exists('confirmationNumber');
			if ($fieldExists === true)
			{
				$this->datatables->select('i.confirmationNumber')
					->join($this->{$form['formModel']}->_table . ' i', 'i.submissionID = c.submissionID');
			}
		}

		$this->datatables->select('CASE WHEN DATE(c.dateCreated) = DATE("' . date('Y-m-d') . '") THEN DATE_FORMAT(c.dateCreated, "%l:%i %p") ELSE DATE_FORMAT(c.dateCreated, "%c/%d/%Y @ %l:%i %p") END AS dateFormatted', false)
			->select('status, c.submissionID')
			->unset_column('c.dateCreated');

		if ($this->input->post('status') != '')
		{
			$this->datatables->where('status', $this->input->post('status', TRUE));
		}

		if ($this->input->post('sort') != '')
		{
			$this->datatables->order_by('c.dateCreated', $this->input->post('sort'));
		} else {
			$this->datatables->order_by('c.dateCreated', 'desc');
		}

		$output = $this->datatables->where('status !=', 'deleted')->generate();

		//return or echo the data
		if ($return == true)
		{
			return $output;
		}

		echo $output;
	}

	function option_categories($return = false)
	{
		$output = $this->datatables->select('c.formOptCategoryID, c.title, f.formName')
			->add_column('actions',  '<div class="btn-group">
						<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
							<i class="fa fa-folder"></i> Category <span class="caret"></span>
						</button>
						<ul class="dropdown-menu">
							<li><a href="/admin/forms/options/categories/options/$1"><i class="fa fa-th-list"></i> See Options</a></li>
							<!--<li><a href="/admin/forms/options/categories/edit/$1"><i class="fa fa-edit"></i> Edit</a></li>-->
						</ul>
					</div>', 'c.formOptCategoryID')
			->join('Forms f', 'f.formID = c.formID')
			->unset_column('c.formOptCategoryID')
			->from('FormOptionCategories c')
			->generate();

		//return or echo the data
		if ($return == true)
		{
			return $output;
		}

		echo $output;
	}

	function options($categoryID = 0, $return = false)
	{
		$this->datatables->select('o.optionID, o.text')
			->add_column('actions',  '<div class="btn-group">
						<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
							<i class="fa fa-check-circle-o"></i> Option <span class="caret"></span>
						</button>
						<ul class="dropdown-menu">
							<!--<li><a href="/admin/forms/options/edit/$1"><i class="fa fa-edit"></i> Edit</a></li>-->
							<li><a data-confirm="Are you sure you want to delete this?" href="/admin/forms/options/delete/$1"><i class="fa fa-trash-o"></i> Delete</a></li>
						</ul>
					</div>', 'o.optionID')
			->unset_column('o.optionID')
			->from('FormOptions o')
			->join('FormOptionCategories c', 'o.formOptCategoryID = c.formOptCategoryID', 'left outer');

		if ($categoryID > 0)
		{
			$this->datatables->where('o.formOptCategoryID', $categoryID);
		}

		$output = $this->datatables->generate();

		//return or echo the data
		if ($return == true)
		{
			return $output;
		}

		echo $output;
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

}