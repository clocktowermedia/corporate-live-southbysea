<?php
class Admin_json extends Admin_controller {

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//load relevant models
		$this->load->model('forms/form_model');
		$this->load->model('forms/form_submission_model');
	}

	/**
	 * Update statuses of selected messages
	 * @param  string $status [description]
	 * @return [type]		 [description]
	 */
	function update_status($status = 'read')
	{
		try
		{
			//make sure we are passing the valid information
			if (!$this->is_post() || $this->input->post('messages', TRUE) == '')
			{
				throw new Exception('Must post messages.');
			}

			//loop through messages and set to the passed status
			foreach ($this->input->post('messages', TRUE) as $messageID)
			{
				$this->form_submission_model->skip_validation()->update((int) $messageID, array('status' => $status));
			}

			//send success message back
			$data['status'] = 'ok';
			$data['message'] = 'Updated status of messages.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = 'Failed updating status. ' . $e->getMessage();
		}

		//send back json
		$this->render_json($data);
	}

	/**
	 * Get number of unread messages
	 * @param  [type] $formName [description]
	 * @return [type]		   [description]
	 */
	function get_unread_count($formName = null, $returnType = 'single')
	{
		//get form and return type from post if applicable
		$formName = ($this->input->post('formName', TRUE) != '')? $this->input->post('formName', TRUE) : $formName;
		$returnType = ($this->input->post('returnType', TRUE) != '')? $this->input->post('returnType', TRUE) : $returnType;

		//check to see if we want to return a count for a single form or all forms
		if ($returnType != 'all')
		{
			if (!is_null($formName) && $formName != '')
			{
				$formInfo = $this->form_model->get_by('formName', $formName);
				$data['count'] = ($formInfo != false)? $this->form_submission_model->count_by(array('formID' => $formInfo['formID'], 'status' => 'unread')) : false;
			} else {
				$data['count'] = $this->form_submission_model->count_by(array('status' => 'unread'));
			}

		} else {

			//load helper in case we need it
			$this->load->helper('array');

			//get all unread counts
			$counts = $this->form_model->getUnreadCounts();
			$data['counts'] = (!is_null($formName) && $formName != '')? $counts[recursive_array_search($formName, $counts)]['unreadCount'] : $counts;
		}

		//send back json
		$this->render_json($data);
	}

	/**
	 * Return list of fields for a given form
	 * @param  [string] $formUrl
	 * @return [array]
	 */
	function get_fields($formUrl = null)
	{
		try
		{
			//get form info
			$formUrl = ($this->input->post('formUrl', TRUE) != '')? $this->input->post('formUrl', TRUE) : $formUrl;
			$formInfo = $this->form_model->get_by('formUrl', $formUrl);

			//get fields list
			$fields = $this->form_submission_model->list_fields();
			$this->load->model('forms/' . $formInfo['formModel'], 'secondary_form_model');
			$secondaryFields = $this->secondary_form_model->list_fields();
			$this->load->helper('array');
			$last_key = key( array_slice($fields, -1, 1, TRUE));
			$fields = array_unique(array_splice_after_key($fields, $last_key, $secondaryFields));
			array_multisort($fields);

			//format
			foreach ($fields AS $field)
			{
				#$newFields[] = array('id' => $field, 'text' => format_key($field));
				$newFields[$field] =  format_key($field);
			}
			unset($newFields['submissionID'], $newFields['formID'], $newFields[$this->secondary_form_model->primary_key], $newFields['status'], $newFields['dateCreated']);
			$newFields['name'] = 'Full Name';

			$data['results'] = $newFields;
			$data['status'] = 'ok';
			$data['message'] = 'Successfully retrieved fields.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = 'Failed updating status. ' . $e->getMessage();
		}

		//send back json
		$this->render_json($data);
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/
}