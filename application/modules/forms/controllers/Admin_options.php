<?php

class Admin_options extends Admin_controller {

	//layouts
	protected $defaultLayout;

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//set layout variable(s)
		$this->defaultLayout = $this->get_admin_layout();

		//load relevant models
		$this->load->model('forms/form_option_model');
		$this->load->model('forms/form_option_category_model');
	}

	/**
	 * redirect to category page
	 */
	function index()
	{
		redirect('/admin/forms/options/categories');
	}

	/**
	 * View and action for creating an option
	 */
	function create($categoryID = 0)
	{
		//tell it what view file to use
		$data['view'] = $this->_get_custom_view();
		$data['title'] = 'Add Option';

		//get category dropdown
		$data['categories'] = $this->form_option_category_model->dropdown('formOptCategoryID', 'title');

		if ($categoryID > 0)
		{
			modules::run('forms/admin_option_categories/_cateogory_is_valid', $categoryID);
			$data['category'] = $this->form_option_category_model->get($categoryID);
			$data['option']['formOptCategoryID'] = $categoryID;
			$data['title'] .= ' for ' . $data['category']['title'];
		}

		//load form validation library
		$this->load->library('form_validation');

		//get validation rules from model, set extra options as neccessary
		$validationRules = $this->form_option_model->validate;
		$this->form_validation->set_rules($validationRules);

		//if the form was submitted, add to database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the page and show form errors
				$this->load->view($this->defaultLayout, $data);
			} else {

				//update record in db
				$optionID = $this->form_option_model->insert($this->input->post(NULL, TRUE));

				if (!is_int($optionID) || (int) $optionID <= 0)
				{
					//show error message
					$this->session->set_flashdata('fail', 'An error occured, please try again.');
					redirect('/admin/forms/options/add-option');
				} else {
					//display success message & redirect
					$option = $this->form_option_model->get($optionID);
					$this->session->set_flashdata('success', 'Option successfully added.');
					redirect('/admin/forms/options/categories/options/' . $option['formOptCategoryID']);
				}
			}
		} else {
			// tell it what layout to use and load the page
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	 * View and action for editing an option
	 * @param  integer $optionID
	 */
	function edit($optionID = 0)
	{
		//make sure option is valid
		$this->_option_is_valid($optionID);

		//tell it what view file to use
		$data['view'] = $this->_get_custom_view();
		$data['title'] = 'Edit Option';

		//get option info
		$data['option'] = $this->form_option_model->get($optionID);

		//get category dropdown
		$data['categories'] = $this->form_option_category_model->dropdown('formOptCategoryID', 'title');

		//load form validation library
		$this->load->library('form_validation');

		//get validation rules from model, set extra options as neccessary
		$validationRules = $this->form_option_model->validate;
		unset($validationRules['formOptCategoryID']);
		$this->form_validation->set_rules($validationRules);

		//if the form was submitted, add to database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the page and show form errors
				$this->load->view($this->defaultLayout, $data);
			} else {

				//update record in db
				$this->form_option_model->skip_validation()->update($optionID, $this->input->post(NULL, TRUE));

				//display success message & redirect
				$this->session->set_flashdata('success', 'Option successfully edited.');
				redirect('/admin/forms/options/categories/options/' . $data['option']['formOptCategoryID']);
			}
		} else {
			// tell it what layout to use and load the page
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	 * Action for deleting an option
	 * @param  integer $optionID
	 */
	function delete($optionID = 0)
	{
		//make sure option exists
		$this->_option_is_valid($optionID);

		//get option info
		$option = $this->form_option_model->get($optionID);

		//delete
		$this->form_option_model->delete($optionID);

		//redirect and show success message
		$this->session->set_flashdata('success', 'Option successfully deleted.');
		redirect('/admin/forms/options/categories/options/' . $option['formOptCategoryID']);
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

	private function _get_custom_view()
	{
		return 'admin/options/' . $this->get_view();
	}

	/**
	* Checks to see that category ID exists
	*/
	public function _category_is_valid($categoryID = 0)
	{
		//make sure contact form category id is an integer
		$categoryID = (int) $categoryID;

		//make sure contact form category id is greater than zero
		if ($categoryID > 0)
		{
			//check to see if the contact form category exists
			$exists = $this->form_option_category_model->get($categoryID);

			if ($exists == false)
			{
				$this->session->set_flashdata('error', 'Invalid option category id.');
				redirect('/admin/forms/options');
			}

		} else {
			$this->session->set_flashdata('error', 'Invalid option category id.');
			redirect('/admin/forms/options');
		}
	}

	/**
	* Checks to see that option ID exists
	*/
	public function _option_is_valid($optionID = 0)
	{
		//make sure contact form option id is an integer
		$optionID = (int) $optionID;

		//make sure contact form option id is greater than zero
		if ($optionID > 0)
		{
			//check to see if the contact form option exists
			$exists = $this->form_option_model->get($optionID);

			if ($exists == false)
			{
				$this->session->set_flashdata('error', 'Invalid option id.');
				redirect('/admin/forms');
			}

		} else {
			$this->session->set_flashdata('error', 'Invalid option id.');
			redirect('/admin/forms');
		}
	}
}