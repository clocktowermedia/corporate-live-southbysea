<?php
class File_handler extends MY_Controller {

	//image upload variables
	protected $uploadFolder;
	protected $imgUploadFolder;
	protected $imgThumbUploadFolder;

	//all actions should call this function
	function __construct()
	{
		parent::__construct();
		$this->uploadFolder = 'assets/uploads/';
	}

	public function _set_image_options($formType = 'quote-request')
	{
		//all have the same model
		$modelName = 'forms/form_image_model';

		//change upload path according to form type
		$this->imgUploadFolder = 'assets/uploads/images/forms/' . $formType . '/';
		$this->imgThumbUploadFolder = 'assets/uploads/images/forms/' . $formType . '/thumbnails/';
		$thumbConfig['width'] = 380;
		$thumbConfig['height'] = 600;

		return array('modelName' => $modelName, 'thumbConfig' => $thumbConfig);
	}

	public function _set_file_options($formType = 'quote-request', $fieldName = '')
	{
		//all have the same model
		$modelName = 'forms/form_file_model';

		//change upload path according to form type
		$this->uploadFolder = 'assets/uploads/' . $formType . '/';
		$fileTypes = 'xlsx|xls|pdf|csv|XLSX|XLS|PDF|CSV';

		//certain fields only allow certain types
		if ($fieldName == 'bagAndTagSpreadsheet')
		{
			$fileTypes = 'xlsx|xls|csv|XLSX|XLS|CSV';
		}

		return array('modelName' => $modelName, 'fileTypes' => $fileTypes);
	}

	/**
	* Function for uploading a file
	*/
	public function _upload_file($fieldName = 'userfile', array $formData = array(), $formType = 'quote-request')
	{
		//get model we want to save this to & load it
		$typeInfo = $this->_set_file_options($formType, $fieldName);
		$this->load->model($typeInfo['modelName'], 'file_model');

		//retrieve file names & replace yucky characters
		$name = $_FILES[$fieldName]['name'];
		$name = strtr($name, 'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
		$name = preg_replace('/([^.a-z0-9]+)/i', '_', $name);
		$name = mktime() . '-' . $name;

		//get file extension
		$extension = strrchr($name, '.');

		//Your upload directory and some config settings
		$config['upload_path'] = $this->uploadFolder;
		$config['max_size'] = '25000';
		$config['file_name'] = $name;
		$config['allowed_types'] = $typeInfo['fileTypes'];

	   	//Load the upload library
		$this->load->library('upload', $config);

		//attempt upload
	   	if ($this->upload->do_upload($fieldName))
		{
			//get upload data
			$data = $this->upload->data();

			//regular file data
			$fileData = array(
				'filename' => $name,
				'filepath' => '/' . $config['upload_path'],
				'fullpath' => '/' . $config['upload_path'] . $name,
				'extension' => $data['file_ext'],
				'mimeType' => $data['file_type'],
				'fileSize' => $data['file_size']
			);

			//merge image, file data, and form data together
			$fileData = array_merge($fileData, $formData);

			//insert in db
			$fileID = $this->file_model->insert($fileData);

			//return file id
			$this->_unset_post(array_keys($fileData));
			return $fileID;

		} else {

			//return errors
			return $this->upload->display_errors();
		}
	}

	/**
	* Function for uploading a file
	*/
	public function _upload_image(array $formData = array(), $formType = 'quote-request')
	{
		//get model we want to save this to & load it
		$typeInfo = $this->_set_image_options($formType);
		$this->_set_file_options($formType);
		$this->load->model($typeInfo['modelName'], 'image_model');

		//retrieve file names & replace yucky characters
		$name = $_FILES['userfile']['name'];
		$name = strtr($name, 'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
		$name = preg_replace('/([^.a-z0-9]+)/i', '_', $name);
		$name = mktime() . '-' . $name;

		//get file extension
		$extension = strrchr($name, '.');

		//define image types
		$imageTypes = array('.gif', '.jpg', '.jpeg', '.png');

		//Your upload directory and some config settings
		$config['upload_path'] = (in_array($extension, $imageTypes))? $this->imgUploadFolder : $this->uploadFolder;
		$config['max_size'] = '25000';
		$config['file_name'] = $name;
		$config['allowed_types'] = 'gif|jpg|png|JPG|GIF|PNG|ai|eps|pdf|AI|EPS|PDF';

	   	//Load the upload library
		$this->load->library('upload', $config);

		//attempt upload
	   	if ($this->upload->do_upload())
		{
			//get upload data
			$data = $this->upload->data();

			//if the file is an image...
			if ($data['is_image'] == true)
			{
				//Config for thumbnail creation
				$thumbConfig = $typeInfo['thumbConfig'];
				$thumbConfig['new_image'] = $this->imgThumbUploadFolder;
				$thumbConfig['image_library'] = 'gd2';
				$thumbConfig['source_image'] = $config['upload_path'] . $data['file_name'];
				$thumbConfig['create_thumb'] = TRUE; //this adds _thumb to the end of the file name
				$thumbConfig['maintain_ratio'] = TRUE;

				//create thumbnails
				$this->load->library('image_lib', $thumbConfig);
				$this->image_lib->initialize($thumbConfig);
				if (!$this->image_lib->resize())
				{
					return $this->image_lib->display_errors();
				}

				//grab thumbnail info
				$thumbName = str_replace($extension, '', $data['file_name']) . '_thumb' . $extension;
				$thumbPath = $this->imgThumbUploadFolder . $thumbName;
				$thumbFilesize = round ((filesize($thumbPath) / 1024 ), 2);
				$thumbInfo = getimagesize($thumbPath);

				//set image data
				$imageData = array(
					'width' => $data['image_width'],
					'height' => $data['image_height'],
					'widthHeightString' => $data['image_size_str'],
					'thumbFilename' => $thumbName,
					'thumbFilepath' => '/' . $this->imgThumbUploadFolder,
					'thumbFullPath' => '/' . $this->imgThumbUploadFolder . $thumbName,
					'thumbExtension' => $extension,
					'thumbMimeType' => $thumbInfo['mime'],
					'thumbFileSize' => $thumbFilesize,
					'thumbWidth' => $thumbInfo[0],
					'thumbHeight' => $thumbInfo[1],
					'thumbWidthHeightString' => $thumbInfo[3]
				);

			} else {
				//set image data to empty
				$imageData = array();
			}

			//regular file data
			$fileData = array(
				'filename' => $data['file_name'],
				'filepath' => '/' . $config['upload_path'],
				'fullpath' => '/' . $config['upload_path'] . $data['file_name'],
				'extension' => $data['file_ext'],
				'mimeType' => $data['file_type'],
				'fileSize' => $data['file_size']
			);

			//merge image, file data, and form data together
			$fileData = array_merge($fileData, $imageData, $formData);

			//insert in db
			$imageID = $this->image_model->insert($fileData);

			//return file id
			$this->_unset_post(array_keys($fileData));
			return $imageID;

		} else {

			//return errors
			return $this->upload->display_errors();
		}
	}

	public function _unset_post($keys = array())
	{
		foreach ($keys as $key)
		{
			unset($_POST[$key]);
		}
	}
}
