<?php

class Admin extends Admin_controller {

	//layouts
	protected $defaultLayout;

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//set layout variable(s)
		$this->defaultLayout = $this->get_admin_layout();

		//load relevant models
		$this->load->model('forms/form_model');
		$this->load->model('forms/form_submission_model');
	}

	function index()
	{
		//tell it what view to use
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Form Submissions';

		//get a list of forms we want to show
		$data['forms'] = $this->form_model->get_many_by('displayInAdmin', 1);
		$data['formTypes'] = $this->form_model->where('displayInAdmin', 1)->dropdown('formUrl', 'formName');

		//load the page
		$this->load->view($this->defaultLayout, $data);
	}

	function category($formUrl = '')
	{
		//get form info
		$data['form'] = $this->form_model->get_by('formUrl', $formUrl);

		//tell it what view to use
		$data['view'] = 'admin/index';
		$data['title'] = 'Form Submissions - ' . $data['form']['formName'];

		//get a list of forms we want to show
		$data['forms'] = $this->form_model->get_many_by('displayInAdmin', 1);

		//load the page
		$this->load->view($this->defaultLayout, $data);
	}

	function search()
	{
		//tell it what view to use
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Form Submissions - Search';

		//get list of forms
		$data['formTypes'] = $this->form_model->where('displayInAdmin', 1)->dropdown('formUrl', 'formName');

		//set form validation rules
		$this->load->library('form_validation');
		$this->form_validation->set_rules('formUrl', 'Form Type', 'required');

		//if the form was submitted, update database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the design and show form errors
				$data['submission'] = $this->input->post(NULL, TRUE);
				$this->load->view($this->defaultLayout, $data);

			} else {

				$formInfo = $this->form_model->get_by('formUrl', $this->input->post('formUrl', TRUE));
				$this->load->model('forms/' . $formInfo['formModel']);

				//form search results
				$data['submission'] = $this->input->post(NULL, TRUE);
				$this->load->helper('array');
				$data['field'] = array('id' => $data['submission']['field'], 'text' => format_key($data['submission']['field']));

				//get results
				$data['results'] = $this->form_submission_model->searchResults($this->input->post(NULL, TRUE));

				//if only one result, just redirect to that result
				if (count($data['results']) == 1)
				{
					redirect('/admin/forms/read/' . $data['results'][0]['submissionID']);
				}

				//load data
				$this->load->view($this->defaultLayout, $data);
			}
		} else {
			// tell it what layout to use and load the design
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	* Displays information for the form submission
	*/
	function read($submissionID = 0)
	{
		//set to int
		$submissionID = (int) $submissionID;

		//make sure the form submission id is valid
		$this->_submission_is_valid($submissionID);

		//set status to read
		$this->form_submission_model->skip_validation()->update($submissionID, array('status' => 'read'));

		//grab the information from the db
		$data['submission'] = $this->form_submission_model->getFullByID($submissionID, true);

		//set custom field order
		if ($data['submission']['formUrl'] == 'quote-request')
		{
			$data['fields'] = array('page', 'firstName', 'lastName', 'email', 'phone', 'school', 'site', 'design', 'product', 'colors', 'quantity', 'budget', 'dueDate', 'howDidYouHear','salerep',  'confirmationNumber', '', 'frontDesign', '', 'backDesign', '', 'comments', '');
		}

		//set custom formatting for final submission
		if ($data['submission']['formUrl'] == 'order-form')
		{
			$data['submission'] = modules::run('forms/_format_finalsub_address', $data['submission'], FALSE);
			$ignore = array('address1','address2','city','state','zip','recipientName','subject','comments','coordinatorFreebieSize','dateCreated');
		}

		//get table
		$this->load->library('parser');
		$data['table_ignore'] = array_merge(array('submissionID', 'formID', 'images', 'files'), $this->form_model->list_db_fields());
		$data['table_ignore'] = (isset($ignore))? array_merge($data['table_ignore'], $ignore) : $data['table_ignore'];
		$data['table'] = $this->parser->parse('admin/table-view', $data, true);

		//tell it what view to use
		$data['view'] = $this->get_admin_view();
		$data['title'] = $data['submission']['formName'] .' - ' . '<a href="mailto:' . $data['submission']['email'] . '">' . $data['submission']['firstName'] . ' ' . $data['submission']['lastName'] . '</a>';
		$data['pageTitle'] = $data['submission']['formName'] .' - ' . $data['submission']['firstName'] . ' ' . $data['submission']['lastName'];

		//load the page
		$this->load->view($this->defaultLayout, $data);
	}

	/**
	 * Marks a form as un-read
	 * @param  integer $submissionID [description]
	 * @return [type]				[description]
	 */
	function unread($submissionID = 0)
	{
		//set to int
		$submissionID = (int) $submissionID;

		//make sure the form submission id is valid
		$this->_submission_is_valid($submissionID);

		//set status to unread
		$this->form_submission_model->update($submissionID, array('status' => 'unread'));

		//show success message and redirect
		$this->session->set_flashdata('success', 'Message marked as un-read.');
		redirect('/admin/forms');
	}

	/**
	 * View & action for form (email) settings
	 */
	function settings()
	{
		//tell it what view to use
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Settings';

		//get a list of forms we want to show
		$data['forms'] = $this->form_model->get_many_by('displayInAdmin', 1);

		//load form validation library
		$this->load->library('form_validation');

		//set form validation rules
		foreach ($data['forms'] as $form)
		{
			$this->form_validation->set_rules($form['formUrl'], $form['formName'], 'valid_emails');
		}

		//if the form was submitted, update database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the page and show form errors
				$this->load->view($this->defaultLayout, $data);
			} else {
				//get postdata
				$postData = $this->input->post(NULL, TRUE);

				//save each form
				foreach ($postData as $key => $item)
				{
					if (strrpos($key, 'emails_') !== false && substr_count($key, 'emails_') == 1)
					{
						//update record in db
						$form = str_replace('emails_', '', $key);
						$this->form_model->update_by('formUrl', $form, array('adminEmails' => $postData[$key]));

					} else if (strrpos($key, 'ccuser_') !== false && substr_count($key, 'ccuser_') == 1) {

						// update record to db & redirect
						$form = str_replace('ccuser_', '', $key);
						$this->form_model->update_by('formUrl', $form, array('emailToUser' => $postData[$key]));
					}
				}

				//display success message & redirect
				$this->session->set_flashdata('success', 'Settings successfully updated.');
				redirect('/admin/forms/settings');
			}
		} else {
			// tell it what layout to use and load the page
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

	/**
	* Checks to see that submission ID exists
	*/
	public function _submission_is_valid($submissionID = 0)
	{
		//make sure contact form submission id is an integer
		$submissionID = (int) $submissionID;

		//make sure contact form submission id is greater than zero
		if ($submissionID > 0)
		{
			//check to see if the contact form submission exists
			$exists = $this->form_submission_model->with('forms')->get($submissionID);

			if ($exists == false)
			{
				$this->session->set_flashdata('error', 'Invalid submission id.');
				redirect('/admin/forms');
			}

		} else {
			$this->session->set_flashdata('error', 'Invalid submission id.');
			redirect('/admin/forms');
		}
	}
}
