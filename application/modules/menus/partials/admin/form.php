<?php
	//create array for classes
	$containerArray = array('labelClass' => 'col-sm-1', 'containerClass' => 'col-sm-11');

	//title field
	$this->form_builder->text('title', 'Title:', (isset($menu['title']))? $menu['title'] : '', $containerArray, '', '', '', array('required' => 'required'));

	//checkbox for including in sitemap
	$this->form_builder->checkbox('inSitemapXml', 'Include in Sitemap.xml?', 0, 1, (isset($menu['inSitemapXml']))? $menu['inSitemapXml'] : '', $containerArray, 'SEO Settings:', '', array());
?>