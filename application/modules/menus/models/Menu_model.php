<?php
/********************************************************************************************************
/
/ Menus
/
/
********************************************************************************************************/
class Menu_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'Menus';
	private $itemTable = 'MenuItems';
	private $moduleActionTable = 'MenuModuleActions';
	private $pageTable = 'Pages';

	//primary key
	public $primary_key = 'menuID';

	//relationships


	/**
	* Adds a menu entry, this is for menu title and settings only
	*/
	public function add($data = null)
	{
		//make sure there is sufficient data to insert
		if (!is_null($data))
		{
			//insert into db
			$insert = $this->db->set($data)
				->set('dateCreated', date('Y-m-d H:i:s'))
				->insert($this->_table);

			//return new menu id
			return $this->db->insert_id();
		}

		return false;
	}

	/**
	* Grabs information for the menu by id
	*/
	public function findByID($menuID = 0)
	{
		//make sure menu id is valid
		if ($menuID > 0)
		{
			//define query
			$query = $this->db->from($this->_table)
				->where('menuID', $menuID)
				->get();

			//return false if no matches are found
			if ($query->num_rows() <= 0)
			{
				return false;
			}

			//grab matching value and format as an array
			return $query->row_array();
		}

		return false;
	}

	/**
	* Soft delete of menu entry from db by id - change status to deleted
	*/
	public function soft_delete($menuID = 0)
	{
		//make sure menu id is valid
		if ($menuID > 0)
		{
			$this->db->where('menuID', $menuID)
				->set('status', 'deleted')
				->update($this->_table);

			return true;
		}

		return false;
	}

	/**
	 * Get menu information by title
	 */
	public function findByTitle($title = '')
	{
		if (!is_null($title) && $title != '')
		{
			//grab menu information
			$query = $this->db->from($this->_table)
				->where('title', $title)
				->get();

			//return false if no match is found
			if ($query->num_rows() <= 0)
			{
				return false;
			}

			//return item as array
			return $query->row_array();
		}

		return false;
	}

	/**
	* Grab all menus
	*/
	public function getMenuList()
	{
		return $this->db->from($this->_table)->get()->result_array();
	}

	/**
	* Adds an individual item to the menu
	*/
	public function addMenuItem($data = null)
	{
		//make sure there is sufficient data to insert
		if (!is_null($data))
		{
			//insert into db
			$insert = $this->db->set($data)
				->insert($this->itemTable);

			//return new menu item id
			return $this->db->insert_id();
		}

		return false;
	}

	/**
	* updates a menu item
	*/
	public function updateMenuItem($menuItemID = 0, $data = null)
	{
		//make sure menu item id and data are valid
		if ($menuItemID > 0 && !is_null($data))
		{
			//update entry in db
			$this->db->where('menuItemID', $menuItemID)
				->set($data)
				->update($this->itemTable);

			return true;
		}

		return false;
	}

	/**
	* Deletes an individual item from the menu
	*/
	public function removeMenuItem($menuItemID = 0)
	{
		//make sure item id is valid
		if ($menuItemID > 0)
		{
			//delete entry in db
			$this->db->where('menuItemID', $menuItemID)
				->delete($this->itemTable);

			return true;
		}

		return false;
	}

	/*
	* Find menu item by menu item id
	*/
	public function findMenuItem($menuItemID = 0, $depth = 0)
	{
		if ($menuItemID > 0)
		{
			$this->db->select('i.*')
				->select('CASE WHEN p.pageID IS NOT NULL AND i.title = "" THEN p.title ELSE i.title END AS title', false)
				->select('CASE WHEN p.pageID IS NOT NULL THEN p.urlPath WHEN i.type = "action" THEN CONCAT("/", i.link) ELSE i.link END AS link', false)
				->from($this->itemTable . ' AS i')
				->where('i.menuItemID', $menuItemID)
				->join($this->pageTable . ' AS p', 'p.pageID = i.uniqueIdentifier AND i.type="page"', 'left outer');

			//if depth is defined, add it to the query
			if ($depth > 0)
			{
				$this->db->where('i.depth <=', $depth);
			}

			//execute query
			$query = $this->db->get();

			//return false if no matches found
			if ($query->num_rows() <= 0)
			{
				return false;
			}

			//grab matching values and format as an array
			return $query->row_array();
		}

		return false;
	}

	/**
	* Add items to the menu
	*/
	public function addItems($menuID = 0, $items = null)
	{
		//make sure menu id and items are valid
		if ($menuID > 0 && !is_null($items))
		{
			//update each entry in db
			$i = 0;
			foreach ($items as $key => $value)
			{
				$i++;
				$menuItemID = $key;
				//list($menuItemID, $url, $title, $parentID, $depth, $uniqueID, $type) = $items[$key];
				list($parentID, $depth) = explode(',', $items[$menuItemID]);

				$menuItem = array(	'parentID' => $parentID,
									'depth' => $depth,
									'displayOrder' => $i
							);

				$this->updateMenuItem($menuItemID, $menuItem);
			}

			return true;
		}

		return false;
	}

	/**
	* Removes all the old items from the menu
	*/
	public function removeOldItems($menuID = 0)
	{
		//make sure menu id is valid
		if ($menuID > 0)
		{
			//delete all matching entries
			$this->db->where('menuID', $menuID)
				->delete($this->itemTable);

			return true;
		}

		return false;
	}

	/**
	* Grab all menu items by menu ID
	*/
	public function findItemsByMenuID($menuID = 0)
	{
		//make sure menu id is valid
		if ($menuID > 0)
		{
			//define query
			$query = $this->db->from($this->itemTable)
				->where('menuID', $menuID)
				->order_by('displayOrder')
				->get();

			//return false if no matches are found
			if ($query->num_rows() <= 0)
			{
				return false;
			}

			//grab matching values and format as an array
			return $query->result_array();
		}

		return false;
	}

	/**
	* formats the menu items into a json array representing parent/child
	*/
	public function addJsonTree($menuID = 0)
	{
		//make sure menu id is valid
		 if ($menuID > 0)
		 {
			//grab menu items
			$items = $this->findItemsByMenuID($menuID);

			//grab the parent child tree, format it as json
			$tree = json_encode($this->buildParentChildArray($items));

			//insert into db
			$this->db->where('menuID', $menuID)
				->set('items', $tree)
				->update($this->_table);

			return true;
		 }

		 return false;
	}

	/**
	* creates a parent/child array
	*/
	function buildParentChildArray($items = null, $parentID = 0)
	{
		//make sure there are items to iterate through
		if (!is_null($items))
		{
			//this will hold our parent child array
			$array = array();

			//loop through each item
			foreach ($items as $item)
			{
				//check if there are children
				if ($item['parentID'] == $parentID)
				{
					// using recursion, check if each entry has children
					$children =  $this->buildParentChildArray($items, $item['menuItemID']);

					//if thare are children, add them to the array
					if($children)
					{
						$array[] = array('id' => $item['menuItemID'], 'children' => $children);
					} else {
						$array[] = array('id' => $item['menuItemID']);
					}
				}
			}

			return $array;
		}

		return false;
	}

	/**
	 * Grabs entire menu by title in array format
	 */
	public function getMenuArray($title = '', $depth = 0)
	{
		//grab menu information
		$menu = $this->findByTitle($title);

		//return false if no match is found
		if ($menu == false)
		{
			return false;
		}

		//grab the formated html ul/li list
		$menuArray = $this->getMenuArrayItems(json_decode($menu['items'], true), $depth);
		return $menuArray;
	}

	/**
	* Grabs the individual items in the menu in array format
	*/
	public function getMenuArrayItems($items = null, $depth = 0)
	{
		//if matches found...start looping through the items
		if (!is_null($items))
		{
			//iterate through each item in the menu
			foreach ($items as $key => $value)
			{
				//get item information
				$itemInfo = $this->findMenuItem($items[$key]['id']);

				//if depth is specified, only grab items of that depth
				if ($depth > 0)
				{
					if ($itemInfo['depth'] <= $depth)
					{
						//add item to array
						$menuItems[] = $itemInfo;
					}
				} else {
					//add item to array
					$menuItems[] = $itemInfo;
				}

				//check if there are children, if so add them
				if (!empty($items[$key]['children']))
				{
					//get item key
					$itemKey = end((array_keys($menuItems)));
					$menuItems[$itemKey]['children'] = $this->getMenuArrayItems($items[$key]['children'], $depth);
				}
			}

		} else {
			$menuItems = array();
		}

		//return array
		return $menuItems;
	}

	/**
	* Grabs the menu and formats it into an html list
	*/
	public function getMenuHtmlByTitle($title = '', $depth = 0, $id = null, $class = null, $extraContent = '')
	{
		//grab menu information
		$menu = $this->findByTitle($title);

		//return false if no match is found
		if ($menu == false)
		{
			return false;
		}

		//grab the formated html ul/li list
		$html = $this->getMenuHtmlItems(json_decode($menu['items'], true), $depth, $id, $class, $extraContent);
		return $html;
	}

	/**
	* Grabs the individual items in the menu for the front end formatted in ul/li format
	*/
	public function getMenuHtmlItems($items = null, $depth = 0, $id = null, $class = null, $extraContent = '')
	{
		//start the html
		$html = '';

		//check for html id and class, add them if they exist
		$selectors = '';
		if (!is_null($id) && $id != '')
		{
			$selectors .= ' id="' . $id . '"';
		}

		if (!is_null($class) && $class != '')
		{
			$selectors .= ' class="' . $class . '"';
		}

		//start the list
		$html .= '<ul' . $selectors . '>';

		//make sure there are menu items to iterate through
		if (!is_null($items) && count($items) > 0)
		{
			//iterate through each item in the menu
			foreach ($items as $key => $value)
			{
				//get item information
				$itemInfo = $this->findMenuItem($items[$key]['id']);

				//add class/id if applicable
				$itemClass = ($itemInfo['classes'] != '')? ' class="' . $itemInfo['classes'] . '"' : '';
				$itemID = ($itemInfo['elementID'] != '')? ' id="' . $itemInfo['elementID'] . '"' : '';
				$itemHtml = '<li' . $itemClass . $itemID . '>
								<a href="' . $itemInfo['link'] . '">' .
									$itemInfo['title'] .
								'</a>';

				//if depth is specified, only grab items of that depth
				if ($depth > 0)
				{
					if ($itemInfo['depth'] <= $depth)
					{
						//add item html
						$html .= $itemHtml;
					}
				} else {
					//add item html
					$html .= $itemHtml;
				}

				//check if there are children, if so add them
				if (!empty($items[$key]['children']))
				{
					$html .= $this->getMenuHtmlItems($items[$key]['children'], $depth);
				}

				//close the li tag, check for depth to avoid closing tags w/o a opening tag!
				if ($depth > 0)
				{
					if ($itemInfo['depth'] <= $depth)
					{
						$html .= '</li>';
					}
				} else {
					$html .= '</li>';
				}
			}
		}

		if ($extraContent != '')
		{
			$html .= $extraContent;
		}

		//end the list
		$html .= '</ul>';

	   return $html;
	}

	/**
	* Grabs the menu and formats it into an html list for the admin section
	*/
	public function getMenuHtmlforAdminByID($menuID = 0, $depth = 0, $id = null, $class = null)
	{
		//make sure menu id is valid
		if ($menuID > 0)
		{
			//grab menu information
			$menu = $this->findByID($menuID);

			//return false if no matches found
			if ($menu == false)
			{
				return false;
			}

			//grab the formated html ul/li list
			$html = $this->getMenuHtmlItemsforAdmin(json_decode($menu['items'], true), $depth, $id, $class);
			return $html;
		}

		return false;
	}

	/**
	* Grabs the individual items in the menu for the admin section
	*/
	public function getMenuHtmlItemsforAdmin($items = null, $depth = 0, $id = null, $class = null)
	{
		//start the html
		$html = '';

		//check for id and class, add them if they exist
		$selectors = '';
		if (!is_null($id) && $id != '')
		{
			$selectors .= ' id="' . $id . '"';
		}

		if (!is_null($class) && $class != '')
		{
			$selectors .= ' class="' . $class . '"';
		}

		//start the list
		$html .= '<ul' . $selectors . '>';

		//make sure there are items to iterate through
		if (!is_null($items) && count($items) > 0)
		{
			//iterate through each item in the menu
			foreach ($items as $key => $value)
			{
				//get item information
				$itemInfo = $this->findMenuItem($items[$key]['id'], $depth);

				//define item html
				$itemHtml = '<li id="list_' . $items[$key]['id'] . '">
								<div class="sortable-container">
									<div class="reorder-title pull-left">
										<i class="fa fa-arrows"></i> 
										<a href="' . $itemInfo['link'] . '" data-type="' . $itemInfo['type'] . '" data-unique="' . $itemInfo['uniqueIdentifier'] . '" data-classes="' . $itemInfo['classes'] . '" data-elementID="' . $itemInfo['elementID'] . '" data-sitemap="' . $itemInfo['inSitemapXml'] . '">' .
											$itemInfo['title'] .
										'</a>
									</div>
									<span class="pull-right menu-item-actions">
										<a href="#editLink" data-toggle="modal" title="Edit" class="edit" data-id="' . $items[$key]['id'] . '"><i class="fa fa-edit"></i></a>
										<a href="" title="Delete" class="delete" data-id="' . $items[$key]['id'] . '"><i class="fa fa-trash-o"></i></a>
									</span>
								</div>';

				//if depth is specified, only grab items of that depth
				if ($depth > 0)
				{
					if ($itemInfo['depth'] <= $depth)
					{
						//add item html
						$html .= $itemHtml;
					}
				} else {

					//add item html
					$html .= $itemHtml;
				}

				//check if there are children and add them to the list
				if (!empty($items[$key]['children']))
				{
					$html .= $this->getMenuHtmlItemsforAdmin($items[$key]['children'], $depth);
				}

				//close the li tag, check for depth to avoid closing tags w/o a opening tag!
				if ($depth > 0)
				{
					if ($itemInfo['depth'] <= $depth)
					{
						$html .= '</li>';
					}
				} else {
					$html .= '</li>';
				}
			}
		}

		//end the list
		$html .= '</ul>';

		//return the html
		return $html;
	}

	/**
	* Creates a list of unavailable page id's
	*/
	public function getUnavailablePageList($menuID = 0)
	{
		//make sure the menu id is valid
		if ($menuID > 0)
		{
			//define the query
			$query = $this->db->from($this->itemTable)
				->select('uniqueIdentifier, menuItemID')
				->where('menuID', $menuID)
				->where('type', 'page')
				->get();

			//return false if no matches are found
			if ($query->num_rows() <= 0)
			{
				return false;
			}

			//grab matching values and format as an array
			$pages = $query->result_array();

			//format these into an array that can be used in an sql statement
			$list = array();
			$i = 0;
			$totalPages = count($pages);
			foreach ($pages as $page)
			{
				$list[$page['menuItemID']] = (int) $page['uniqueIdentifier'];
			}

			return $list;
		}

		return false;
	}

	/**
	* Gets a list of module actions that are available
	*/
	public function getAvailableModuleActions($menuID = 0)
	{
		//make sure menu id is valid
		if ($menuID > 0)
		{
			//grab a list of all module actions and a list of module actions already in use by the menu
			$all = $this->moduleActionArray();
			$unavailable = $this->unavailableModuleActionArray($menuID);

			//if there are actions that are already in user, remove them
			if ($unavailable != false)
			{
				foreach ($unavailable as $module => $values)
				{
					foreach ($unavailable[$module]['actions'] as $action)
					{
						//remove actions from the list that are already in the menu
						$keyToRemove = array_search($action, $all[$module]['actions']);
						unset($all[$module]['actions'][$keyToRemove]);
					}
				}
			}

			return $all;
		}

		return false;
	}

	/**
	* Gets a list of module actions that are unavailable
	*/
	public function unavailableModuleActions($menuID = 0)
	{
		//make sure menu id is valid
		 if ($menuID > 0)
		 {
			//define query
			$query = $this->db->from($this->itemTable)
				->select('uniqueIdentifier')
				->where('menuID', $menuID)
				->where('type', 'action')
				->get();

			//return false if no matches are found
			if ($query->num_rows() <= 0)
			{
				return false;
			}

			//return results as an array
			return $query->result_array();
		 }

		 return false;
	}

	/**
	* Formats the unavailable module actions into an array format that is more useful
	*/
	public function unavailableModuleActionArray($menuID = 0)
	{
		//make sure menu id is valid
		if ($menuID > 0)
		{
			//grabs the list of unavilable modules
			$actions = $this->unavailableModuleActions($menuID);

			//return false if there are no matches
			if ($actions == false)
			{
				return false;
			}

			//loop through the actions and format them into a nice array format with module name as an index
			$array = array();
			foreach ($actions as $action)
			{
				list($module, $action) = explode('/', $action['uniqueIdentifier']);
				$array[$module]['actions'][] = $action;
			}

			//return the newly formatted array
			return $array;
		}

		return false;
	}

	/**
	* Gets a list of module actions that the admin has designated to be part of the menu builder
	*/
	public function moduleActions()
	{
		//define the query
		$query = $this->db->from($this->moduleActionTable)
			->get();

		//return false if no matches are found
		if ($query->num_rows() <= 0)
		{
			return false;
		}

		//return results as an array
		return $query->result_array();
	}

	/**
	* formats the module actions into an array that is more usable
	*/
	public function moduleActionArray()
	{
		//grab a list of all module actions
		$actions = $this->moduleActions();

		//return false if no module actions are found
		if ($actions == false)
		{
			return false;
		}

		//loop through each action and add it to our array, formatted where the module name is the index
		$array = array();
		foreach ($actions as $action)
		{
			$moduleAction = json_decode($action['actions'], true);

			//check to make sure that its not empty
			if ($moduleAction[0] != '')
			{
				$array[$action['module']] = array('actions' => $moduleAction);
			}
		}

		//return our newly formatted array
		return $array;
	}

	/**
	* Adds a module action to the db
	*/
	public function addModuleAction($data = null)
	{
		//make sure there is data to insert
		if (!is_null($data))
		{
			//insert data into db
			$this->db->set($data)
				->insert($this->moduleActionTable);

			return true;
		}

		return false;
	}

	/**
	* Edits available module actions in the db
	*/
	public function updateModuleAction($module = null, $data = null)
	{
		//make sure the module name is valid and that there is data to insert
		if ($module != '' & !is_null($module) && !is_null($data))
		{
			//update db entry
			$this->db->where('module', $module)
				->set($data)
				->update($this->moduleActionTable);

			return true;
		}

		return false;
	}

	/**
	* removes entries from the menus when a page is deleted
	*/
	public function deletePage($pageID = 0)
	{
		//make sure page id is valid
		if ($pageID > 0)
		{
			//get list of affected menus
			$query = $this->db->from($this->itemTable)
				->select('menuID')
				->where('uniqueIdentifier', $pageID)
				->where('type', 'page')
				->group_by('menuID')
				->get();

			//return false if there is no menu affected
			if ($query->num_rows() <= 0)
			{
				return false;
			}

			//grab an array of menu ids
			$menuIDs = $query->result_array();

			//delete items that match the page id
			$this->db->where('uniqueIdentifier', $pageID)
				->where('type', 'page')
				->delete($this->itemTable);

			//loop through each menu id
			foreach ($menuIDs as $item)
			{
				//update the json array
				$this->addJsonTree($item['menuID']);
			}
		}

		return false;
	}

	/**
	 * Get information we need for the sitemap xml in the format that we need
	 * @return array
	 */
	public function getSitemapXmlInfo()
	{
		//define query
		return $this->db->select('DATE_FORMAT(NOW(), "%Y-%m-%d") AS dateUpdated', false)
			->select('CONCAT("/", i.link) AS link', false)
			->where('m.inSitemapXml', 1)
			->where('i.type', 'action')
			->from($this->_table . ' AS m')
			->join($this->itemTable . ' AS i', 'i.menuID = m.menuID AND i.inSitemapXml = "1"')
			->group_by('i.link')
			->get()
			->result_array();
	}

	/**********************************************************************************************
	*
	* Validity checker functions & helper functions
	*
	***********************************************************************************************/
	/**
	* Makes sure the menu exists by id
	*/
	public function _exists($menuID = 0)
	{
		//make sure menu id is valid
		if ($menuID > 0)
		{
			//define query
			$query = $this->db->from($this->_table)
				->where('menuID', $menuID)
				->get();

			//return false if no match is found
			if ($query->num_rows() <= 0)
			{
				return false;
			}

			return true;
		}

		return false;
	}
}