<?php
class Admin extends Admin_controller {

	//layout variables
	public $defaultLayout;
	public $jsonLayout;

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//set layout variable(s)
		$this->defaultLayout = $this->get_admin_layout();
		$this->jsonLayout = $this->get_json_layout();

		//load relevant models
		$this->load->model('pages/page_model');
		$this->load->model('menu_model');
	}

	/**
	* Shows all pages in a datatable
	*/
	function index()
	{
		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Menus';

		//grab menus
		$data['menus'] = $this->menu_model->getMenuList();

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}

	/**
	* View and action for creating a menu
	*/
	function create()
	{
		//tell it what view file to use
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Add Menu';

		//set form validation rules
		$this->load->library('form_validation');
		$this->form_validation->set_rules('title', 'Title', 'required|is_unique[Menus.title]');

		//if the form was submitted, update database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the page and show form errors
				$data['menu'] = $this->input->post(NULL, TRUE);
				$this->load->view($this->defaultLayout, $data);
			} else {

				// update record to db & redirect
				$menuID = $this->menu_model->add($this->input->post(NULL, TRUE));

				//display success message & redirect
				$this->session->set_flashdata('success', 'Menu was successfully created.');
				redirect('/admin/menus/edit/' . $menuID);
			}
		} else {
			// tell it what layout to use and load the page
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	* View & action for updating menu settings
	*/
	function settings($menuID = 0)
	{
		//make sure menu ID is integer
		$menuID = (int) $menuID;

		//make sure account is valid & enabled
		//$this->_menu_is_valid($menuID);

		//grab menu info
		$data['menu'] = $this->menu_model->findByID($menuID);

		//tell it what view file to use
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Update ' . $data['menu']['title'] . ' Menu Settings';

		//set form validation rules
		$this->load->library('form_validation');
		$this->form_validation->set_rules('title', 'Title', 'required');

		//if the form was submitted, add to database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the page and show form errors
				$this->load->view($this->defaultLayout, $data);
			} else {
				//make sure title is unique
				$titleUsed = $this->menu_model->get_by(array('title' => $this->input->post('title', TRUE), 'menuID !=' => $menuID));

				if ($titleUsed == false || empty($titleUsed))
				{
					//redirect and show error message
					$this->session->set_flashdata('error', "This menu title is already in use.");
					redirect('/admin/menus/settings/' . $menuID);

				} else {
					//update menu information in db
					$this->menu_model->update($menuID, $this->input->post(NULL, TRUE));

					//redirect and show success message
					$this->session->set_flashdata('success', "Menu settings successfully updated.");
					redirect('/admin/menus');
				}
			}
		} else {
			// tell it what layout to use and load the page
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	* View for editing menu items
	*/
	function edit($menuID = 0)
	{
		//make sure menu ID is integer
		$menuID = (int) $menuID;

		//grab menu information
		$data['currentMenu'] = $this->menu_model->findByID($menuID);

		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Edit ' . $data['currentMenu']['title'] . ' Menu';

		//grab current menu
		$data['items'] = $this->menu_model->getMenuHtmlforAdminByID($menuID, 0, '', 'menu-sortable');

		//grab available pages & available module actions
		$data['pages'] = $this->page_model->getAvailablePageList($menuID);
		$data['moduleActions'] = $this->menu_model->getAvailableModuleActions($menuID);

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}

	/**
	* Used to create and update entries in the db for module actions that will be available in the menu builder
	*/
	public function edit_modules()
	{
		//tell it what view file to use
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Add/Edit Module Actions for Menu Builder';

		//get list of modules & send to view
		$data['modules'] = $this->getModuleActionList();

		//get information currently in the database
		$data['currentModules'] = $this->menu_model->moduleActionArray();

		//if the form was submitted, update database
		if ($this->is_post())
		{
			// update record to db & redirect
			foreach ($this->input->post(NULL, TRUE) as $key => $value)
			{
				//replace spaces with empty and underscores with dashes
				$old = array(' ', '_');
				$new = array('', '-');
				$action = str_replace($old, $new, $value);

				//put the values in an array for easy entry into db.
				$item = array('module' => $key, 'actions' => json_encode(explode(',', $action)));

				//if the db is empty, add them, else update the entries
				if ($data['currentModules'] == false)
				{
					$this->menu_model->addModuleAction($item);
				} else {
					unset($item['module']);
					$this->menu_model->updateModuleAction($key, $item);
				}
			}

			//display success message & redirect
			$this->session->set_flashdata('success', 'Module Actions were successfully added to menu builder.');
			redirect('/admin/menus');
		} else {
			// tell it what layout to use and load the page
			$this->load->view($this->defaultLayout, $data);
		}
	}
}