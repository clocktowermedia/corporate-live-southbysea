<?php
class Admin_json extends Admin_controller {

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//load relevant models
		$this->load->model('pages/page_model');
		$this->load->model('menu_model');
	}

	/**
	* action for adding an item to the menu
	*/
	function add_item($menuID = 0)
	{
		try
		{
			//make sure menu exists

			//add entry in db
			$menuItem = array(	'menuID' => $menuID,
								'link' => ($this->input->post('link') != '')? $this->input->post('link', TRUE) : '',
								'title' => ($this->input->post('title') != '')? $this->input->post('title', TRUE) : '',
								'parentID' => (int) $this->input->post('parentID', TRUE),
								'depth' => (int) $this->input->post('depth', TRUE),
								'displayOrder' => $this->input->post('order', TRUE),
								'uniqueIdentifier'=> $this->input->post('uniqueID', TRUE),
								'type'=> $this->input->post('type', TRUE)
							);
			$itemID = $this->menu_model->addMenuItem($menuItem);
			$menuItem['menuItemID'] = $itemID;

			//add to item ID to hierarchical array
			$menu = $this->menu_model->findByID($menuID);

			//get menu items, if there's no result create a new array
			$items = json_decode($menu['items'], true);
			if ($items == false)
			{
				$items = array();
			}

			//add the parent/child tree to the menu table
			$items[] = array('id' => $itemID);
			$menuInfo = array('items' => json_encode($items));
			$this->menu_model->update($menuID, $menuInfo);

			//if this is a 'page', we need to get the data from the db as well to send back to view
			if ($menuItem['type'] == 'page')
			{
				$menuItem = $this->menu_model->findMenuItem($itemID);
			}

			//send response to view
			$data['item'] = $menuItem;
			$data['status'] = 'ok';
			$data['message'] = 'Successfully added menu item.';
		} catch (Exception $e) {
			$data['item'] = '';
			$data['status'] = 'fail';
			$data['message'] = 'Error adding menu item. ' . $e->getMessage();
		}

		//send back json
		$this->render_json($data);
	}

	/**
	* Action for deleting an item from the menu
	*/
	function remove_item($menuID)
	{
		try
		{
			//make sure menu exists

			//try to delete the item from the items db
			$this->menu_model->removeMenuItem((int) $this->input->post('menuItemID', TRUE));

			//send response to view
			$data['item'] = $this->input->post(NULL, TRUE);
			$data['status'] = 'ok';
			$data['message'] = 'Successfully deleted menu item.';
		} catch (Exception $e) {
			$data['item'] = '';
			$data['status'] = 'fail';
			$data['message'] = 'Error deleting menu item. ' . $e->getMessage();
		}

		//send back json
		$this->render_json($data);
	}

	/**
	* Action for editing the menu item
	*/
	function edit_item($itemID = 0)
	{
		try
		{
			//make sure item exists

			//try to update the item
			$this->menu_model->updateMenuItem($itemID, $this->input->post(NULL, TRUE));

			//get newly updated item to send back to view
			$item = $this->menu_model->findMenuItem($itemID);

			//send response to view
			$data['item'] = $item;
			$data['status'] = 'ok';
			$data['message'] = 'Successfully updated menu item.';
		} catch (Exception $e) {
			$data['item'] = '';
			$data['status'] = 'fail';
			$data['message'] = 'Error updating menu item. ' . $e->getMessage();
		}

		//send back json
		$this->render_json($data);
	}

	/**
	* Action for saving the menu
	*/
	function save($menuID = 0)
	{
		try
		{
			//make sure the menu exists

			//throw exception if nothing was posted
			if ($this->input->post('list', TRUE) == '')
			{
				throw new Exception('Must post menu items.');
			}

			//update each item
			$i = 0;
			foreach ($this->input->post('list', TRUE) as $key => $value)
			{
				//get the values we need
				list($parentID, $depth) = explode(',', $value);

				//update item
				$i++;
				$this->menu_model->updateMenuItem($key, array('parentID' => $parentID, 'depth' => $depth, 'displayOrder' => $i));
			}

			//update the json array
			$this->menu_model->addJsonTree($menuID);

			//send response to view
			$data['status'] = 'ok';
			$data['message'] = 'Successfully updated menu.';
		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = 'Error saving item order. ' . $e->getMessage();
		}

		//send back json
		$this->render_json($data);
	}

	/**
	* Json list of available module actions
	*/
	public function action_list($menuID = 0)
	{
		try
		{
			//get the module action list
			$data['moduleActions'] = $this->menu_model->getAvailableModuleActions($menuID);

			//send data to view
			$data['data'] = $data;
			$data['status'] = 'ok';
			$data['message'] = 'Successfully retreived page list.';

		} catch (Exception $e) {
			$data['data'] = '';
			$data['status'] = 'fail';
			$data['message'] = 'Error retreiving action list. ' . $e->getMessage();
		}

		//send back json
		$this->render_json($data);
	}

	/**
	* Json list of available pages, no hierarchy
	*/
	public function page_list($menuID = 0)
	{
		try
		{
			//get the page list
			$data['pages'] = $this->page_model->getAvailablePageList($menuID);

			//send data to view
			$data['status'] = 'ok';
			$data['message'] = 'Successfully retreived page list.';

		} catch (Exception $e) {
			$data['pages'] = '';
			$data['status'] = 'fail';
			$data['message'] = 'Error retreiving page list. ' . $e->getMessage();
		}

		//send back json
		$this->render_json($data);
	}

	/**
	* Json list of available pages, with hierarchy
	*/
	public function page_list_tree($menuID = 0)
	{
		try
		{
			//get the page list
			$data['listing'] = $this->page_model->getMenuArray($menuID);

			//send data to view
			$data['status'] = 'ok';
			$data['message'] = 'Successfully retreived page list.';

		} catch (Exception $e) {
			$data['listing'] = '';
			$data['status'] = 'fail';
			$data['message'] = 'Error retreiving page list. ' . $e->getMessage();
		}

		//send back json
		$this->render_json($data);
	}
}