<div class="row">
	<div class="col-xs-12">
		<?php $this->form_builder->set_form_class('form-vertical'); ?>

		<?php echo form_open('admin/menus/edit-modules'); ?>
		<fieldset>
			<?php foreach ($modules as $module => $actions): ?>
				<?php 
					//add input field
					$this->form_builder->text($module, ucwords($module) . ' Module (Comma seperated list)', (isset($currentModules[$module]['actions']))? implode(',', $currentModules[$module]['actions']) : '', '', '', '', '', ''); 
				?>
			<?php endforeach;?>

			<?php
				//submit button
				$this->form_builder->button('submit', 'Add/Edit Module Actions', array('inputClass' => 'btn btn-info'), '', '');
			?>
		</fieldset>
		</form>

	</div>
</div>