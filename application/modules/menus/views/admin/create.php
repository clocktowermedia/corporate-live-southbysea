<div class="row">
	<div class="col-xs-12">
		<div class="form-horizontal">
			<?php echo form_open('admin/menus/create'); ?>
				<fieldset>
					<?php
						//include partial
						$this->load->view('../modules/'. $this->router->fetch_module() .'/partials/admin/form');

						//submit button
						$this->form_builder->button('submit', 'Create Menu', array('containerClass' => 'col-sm-offset-1 col-sm-11', 'inputClass' => 'btn btn-info'), '', '');
					?>
				</fieldset>
			</form>
		</div>
	</div>
</div>