<div class="row">
	<div class="col-sm-3">

		<div class="box box-solid box-info">
			<div class="box-header">
				<h3 class="box-title">CMS Pages</h3>
				<div class="box-tools pull-right">
					<button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div>
			<div class="box-body">
				
				<div id="list-placeholder">
				</div>

				<script id="page-listing" type="x-handlebars-template">
					<li class="list-group-item" id="addpage-{{pageID}}">
						{{#if available}}
							<a data-title="{{title}}" data-link="{{urlPath}}" data-type="page" data-unique="{{pageID}}" href="#" class="addpage pull-right">Add</a>
						{{else}}
							<a data-title="{{title}}" data-link="{{urlPath}}" data-type="page" data-unique="{{pageID}}" data-id="{{menuItemID}}" href="#" class="delete pagelist pull-right">Remove</a>
						{{/if}}
						<strong>{{title}}</strong>
						{{#if children}}
							<ul>
								{{#each children}}
									{{> pages}}
								{{/each}}
							</ul>
						{{/if}}
					</li>
					{{#unless url}}
					<hr class="small-seperator">
					{{/unless}}
				</script>

				<script id="page-list-container" type="x-handlebars-template">
					{{#if listing}}
						<ul class="list-group no-border" id="page-list">
							{{#each listing}}
								{{> pages}}
							{{/each}}
						</ul>
					{{else}}
						<p>You have no pages.</p>
					{{/if}}
				</script>
			</div>
		</div>

		<div class="box box-solid box-info">
			<div class="box-header">
				<h3 class="box-title">Custom Pages</h3>
				<div class="box-tools pull-right">
					<button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div>
			<div class="box-body" id="module-list">

				<?php if (count($moduleActions) > 0):?>
					<div class="panel-group block">
						<?php foreach ($moduleActions as $key => $value): ?>
							<div class="panel panel-default">
								<div class="panel-heading">
									<a data-toggle="collapse" href="#<?php echo $key;?>" class="regular-link collapsed">
										<h6 class="panel-title panel-trigger pull-left"><?php echo ucwords($key);?></h6>
										<div class="collapse-icon-header pull-right"></div>
									</a>
									<div class="clearfix"></div>
								</div>
								<div id="<?php echo $key;?>" class="panel-collapse collapse">
									<div class="panel-body">
										<?php if (count($value['actions']) > 0):?>
											<ul class="list-group no-border">
												<?php foreach ($value['actions'] as $action): ?>
													<li class="list-group-item" id="addaction-<?php echo $key . '_' . $action; ?>">
														<a data-title="<?php echo ucwords(str_replace('-', ' ', $action));?>" data-link="<?php echo $key . '/' . $action;?>" href="#" class="addaction pull-right">Add</a>
														<strong><?php echo ucwords(str_replace('-', ' ', $action));?></strong>
													</li>
												<?php endforeach; ?>
											</ul>
										<?php else:?>
											<span class="none-available">No actions available.</span>
										<?php endif;?>
									</div>
								</div>
							</div>
						<?php endforeach;?>
					</div>
				<?php else:?>
					<p>No module actions available.</p>
				<?php endif;?>
			</div>
		</div>

		<a class="btn btn-info btn-block btn-lg standalone-btn-lg" href="#addExternalLink" data-toggle="modal" title="Add External Link"><i class="fa fa-plus"></i> Add External Link</a>

	</div>

	<div class="col-sm-9">
		 <div class="box box-info">
			<div class="box-header">
				<h3 class="box-title">Menu Items<br/>
				<small>Drag & Drop to re-order</small></h3>

				<div class="box-tools pull-right">
					<div class="mb-15"></div>
					<a href="#help" data-toggle="modal"><i class="fa fa-question-circle fa-2x"></i></a>
				</div>
			</div>
			<div class="box-body no-pad-top">
				<hr class="no-pad-top">
				<?php if (!is_null($items)):?>
					<?php echo $items;?>
				<?php endif;?>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="help" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">x</button>
				<h3 class="modal-title">Help</h3>
			</div>

			<div class="modal-body">
				<h4>How do I change a link for one of the items in my menu?</h4>
				<p>Click on the &nbsp;<i class="fa fa-edit"></i> icon on the far right of the item. Please note you will only be able to edit items added as "External Links".</p>
				<p>If you want to change the URL for a CMS Page, do so in the <a href="/admin/pages">Pages</a> section.</p>
				<p>Custom Pages can only have their URL changed by a developer, so if you would like to change a URL for one of these pages, please contact your Clocktower Media project manager or the <a href="mailto:updates@clocktowermedia.com">Clocktower Media Update Department</a> with your request.</p>

				<div class="mb-30"></div>

				<h4>How do I change the text for one of the items in my menu?</h4>
				<p>Click on the &nbsp;<i class="fa fa-edit"></i> icon on the far right of the item.</p>

				<div class="mb-30"></div>

				<h4>How do I remove an item from menu?</h4>
				<p>Click on the &nbsp;<i class="fa fa-trash-o"></i> icon on the far right of the item.</p>

				<div class="mb-30"></div>

				<h4>How do I add the menu I created to my site?</h4>
				<p>Please contact your Clocktower Media project manager or the <a href="mailto:updates@clocktowermedia.com">Clocktower Media Update Department</a> with your request.</p>
			</div>

			<div class="modal-footer">
				<a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
			</div>

		</div>
	</div>
</div>

<?php $this->form_builder->set_form_class('form-vertical'); ?>
<div class="modal fade" id="addExternalLink" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">x</button>
				<h3 class="modal-title">Add External Link</h3>
			</div>

			<div class="modal-body">
				<?php echo form_open('admin/menus/json/add-item/' . $currentMenu['menuID']); ?>
				<fieldset>

					<?php
						//title field
						$this->form_builder->text('title', 'Title', '', '', '', '', '', array('required' => 'required', 'id' => 'title'));

						//link field
						$this->form_builder->url('link', 'Link', '', '', '', '', '', array('required' => 'required', 'id' => 'link'));
					?>
			</div>

			<div class="modal-footer">
				<button class="btn btn-info" type="submit" id="external-link-button">Add to Menu</button>
				<a href="#" class="btn btn-default" data-dismiss="modal">Close</a>

				</fieldset>
				</form>
			</div>

		</div>
	</div>
</div>


<div class="modal fade" id="editLink" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">x</button>
				<h3 class="modal-title">Edit Menu Entry</h3>
			</div>

			<div class="modal-body">
				<?php echo form_open('admin/menus/json/edit-item/'); ?>
				<fieldset>

					<?php
						//title field
						$this->form_builder->text('title', 'Title', '', '', '', '', '', array('required' => 'required'));
						
						//link field
						$this->form_builder->text('link', 'Link', '', '', '', '', '', array('required' => 'required'));

						//classes field
						$this->form_builder->text('classes', 'Classes', '', '', '', 'Most commonly used for styling purposes shared across multiple menu items', '', array());

						//id field
						$this->form_builder->text('elementID', 'Unique ID', '', '', '', 'Most commonly used for styling purposes on a single menu item', '', array());

						//include in sitemap setting, only shown for 'actions'
						$this->form_builder->checkbox('inSitemapXml', 'Include in Sitemap.xml?', 0, 1, 0, '', 'SEO Settings', '', array());
					?>
			</div>

			<div class="modal-footer">
				<button class="btn btn-info" type="submit" id="edit-link-button">Save</button>
				<a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
				</fieldset>
				</form>
			</div>

		</div>
	</div>
</div>