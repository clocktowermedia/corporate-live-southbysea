<div class="row">
	<div class="col-xs-12">
		<div class="box box-primary">
			<div class="box-header" data-toggle="tooltip" title="" data-original-title="Actions">
				<h3 class="box-title">Shortcuts:</h3>
				<div class="box-tools pull-right">
					<a href="/admin/menus/create"><button class="btn btn-info btn-sm"><i class="fa fa-plus"></i> Create Menu</button></a>
				</div>
			</div>
		</div>
	</div>
</div>

<?php if (count($menus) > 0):?>
	<div class="row">
	<?php foreach ($menus as $menu): ?>
		<div class="col-sm-3 col-md-3">
			<div class="thumbnail">
				<div class="caption text-center">
					<h3><?php echo $menu['title'];?></h3>
					<p>
						<a href="/admin/menus/edit/<?php echo $menu['menuID']; ?>" class="btn btn-info" role="button"><i class="fa fa-edit"></i> Edit</a>
						<a href="/admin/menus/settings/<?php echo $menu['menuID']; ?>" class="btn btn-info" role="button"><i class="fa fa-cog"></i> Settings</a>
						<a href="#delete-menu" data-toggle="modal" class="btn btn-default" role="button"><i class="fa fa-trash-o"></i> Delete</a>
					</p>
				</div>
			</div>
		</div>
	<?php endforeach; ?>
	</div>
<?php else:?>
	<p>You have no menus.</p>
<?php endif;?>

<div class="modal fade" id="delete-menu" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">x</button>
				<h3 class="modal-title">How do I delete a menu?</h3>
			</div>

			<div class="modal-body">
				<p>In order to make sure that you are not deleting a menu that is currently in use on your site, menus cannot be deleted via the CMS. Please contact your Clocktower Media project manager or the <a href="mailto:updates@clocktowermedia.com">Clocktower Media Update Department</a> with your request.</p>
			</div>

			<div class="modal-footer">
				<a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
			</div>

		</div>
	</div>
</div>