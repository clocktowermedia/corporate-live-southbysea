<?php
class Json extends MY_Controller {

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//load relevant models
		$this->load->model('campus_managers/school_model');
		$this->load->model('campus_managers/rep_model');
	}

	function schools()
	{
		try
		{
			//make sure we variable we need from post
			if ($this->input->get('query', TRUE) == '')
			{
				throw new Exception('Must enter a search term.');
			}

			if (strlen($this->input->get('query', TRUE)) < 3)
			{
				throw new Exception('Search term not long enough.');
			}

			//try to find matching schools
			$schools = $this->school_model->autosuggest($this->input->get('query', TRUE));

			//set response
			$data['results'] = $schools;
			$data['status'] = 'ok';
			$data['message'] = 'Successfully retrieved schools';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}

	function all_reps()
	{
		try
		{
			//get reps in those schools
			$reps = $this->rep_model->getAllReps();

			//set return data
			$data['search'] = true;
			$data['reps'] = $reps;
			$data['status'] = 'ok';
			$data['message'] = 'Successfully retrieved campus reps.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		//output
		$this->render_json($data);
	}

	function reps()
	{
		try
		{
			//make sure we variable we need from post
			if ($this->input->get('q', TRUE) == '')
			{
				throw new Exception('Must enter a search term.');
			}

			if (strlen($this->input->get('q', TRUE)) < 3)
			{
				throw new Exception('Search term not long enough.');
			}

			//get schools that match the query
			$schoolIDs = $this->school_model->autosuggest_ids($this->input->get('q', TRUE));

			//get reps in those schools
			$reps = $this->rep_model->getBySchoolIDs($schoolIDs);

			//set return data
			$data['search'] = true;
			$data['reps'] = $reps;
			$data['status'] = 'ok';
			$data['message'] = 'Successfully retrieved campus reps.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		//output
		$this->render_json($data);
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/
}