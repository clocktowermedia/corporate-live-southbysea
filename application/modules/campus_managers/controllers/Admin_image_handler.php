<?php
class Admin_image_handler extends Admin_controller {

	//image upload variables
	protected $uploadFolder;
	protected $imgUploadFolder;
	protected $imgThumbUploadFolder;
	protected $imgCroppedUploadFolder;

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		$this->uploadFolder = 'assets/uploads/';
	}

	public function _cropped_image($formData = null)
	{
		try {

			//cropped image
			if (isset($formData['image']) && $formData['image'] != '')
			{
				//make sure image id was passed
				$imageID = (int) $formData['imageID'];
				if ($imageID <= 0)
				{
					throw new Exception('Invalid Image ID. Make sure you have already uploaded an image for this user.');
				}

				//get model
				$typeInfo = $this->_set_type('rep');
				$this->load->model($typeInfo['modelName'], 'image_model');

				//try to find image, make sure it exists
				$currentImage = $this->image_model->get($imageID);
				if ($currentImage == false || empty($currentImage))
				{
					throw new Exception('Invalid Image. Make sure you have already uploaded an image for this user.');
				}

				//set filename
				$extension = strrchr($currentImage['filename'], '.');
				$file_cropped = str_replace($extension, '_cropped' . $extension, $currentImage['filename']);

				// parse out the base64 encoded cropped image
				$croppedFileBase64 = $formData['image'];
				list($type, $croppedFileBase64) = explode(';', $croppedFileBase64);
				list(, $croppedFileBase64)      = explode(',', $croppedFileBase64);
				$croppedFile = base64_decode($croppedFileBase64);

				//make sure we can write to this folder
				if (!is_writable($this->imgCroppedUploadFolder))
				{
					throw new Exception('Unable to save your cropped image to the server.');

				} else {

					// save the file
					$result = file_put_contents($this->imgCroppedUploadFolder . $file_cropped, $croppedFile);

					if ($result === false)
					{
						throw new Exception('There was a problem saving your cropped image.');
					} else {
						$this->image_model->skip_validation()->update($imageID, array(
							'croppedFullpath' => '/' . $this->imgCroppedUploadFolder . $file_cropped,
							'croppedLeft' => $formData['image_left'],
							'croppedTop' => $formData['image_top']
						));

						//send status back to view
						$data['image'] = '/' . $this->imgCroppedUploadFolder . $file_cropped;
						$data['status'] = 'ok';
						$data['message'] = 'Your cropped profile image has been saved.';
					}
				}
			} else {

				throw new Exception('Missing form data.');
			}

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['response'] = $e->getMessage();
		}

		return $data;
	}

	public function _set_type($imageType = 'rep')
	{
		switch ($imageType)
		{
			case 'rep':
				$this->imgUploadFolder = 'assets/uploads/images/campus-reps/';
				$this->imgThumbUploadFolder = 'assets/uploads/images/campus-reps/thumbnails/';
				$this->imgCroppedUploadFolder = 'assets/uploads/images/campus-reps/cropped/';
				$modelName = 'campus_managers/rep_image_model';
				$thumbConfig['width'] = 209;
				$thumbConfig['height'] = 284;
				break;
		}

		return array('modelName' => $modelName, 'thumbConfig' => $thumbConfig);
	}

	/**
	* Function for uploading a file
	*/
	public function _upload_image(array $formData = array(), $imageType = 'rep')
	{
		//get model we want to save this to & load it
		$typeInfo = $this->_set_type($imageType);
		$this->load->model($typeInfo['modelName'], 'image_model');

		//retrieve file names & replace yucky characters
		$name = $_FILES['userfile']['name'];
		$name = strtr($name, 'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
		$name = preg_replace('/([^.a-z0-9]+)/i', '_', $name);

		//get file extension
		$extension = strrchr($name, '.');

		//define image types
		$imageTypes = array('.gif', '.jpg', '.jpeg', '.png');

		//Your upload directory and some config settings
		$config['upload_path'] = (in_array($extension, $imageTypes))? $this->imgUploadFolder : $this->uploadFolder;
		$config['max_size'] = '10000';
		$config['file_name'] = $name;
		$config['allowed_types'] = 'gif|jpg|png|JPG|GIF|PNG';

	   	//Load the upload library
		$this->load->library('upload', $config);

		//attempt upload
	   	if ($this->upload->do_upload())
		{
			//get upload data
			$data = $this->upload->data();

			//if the file is an image...
			if ($data['is_image'] == true)
			{
				//Config for thumbnail creation
				$thumbConfig = $typeInfo['thumbConfig'];
				$thumbConfig['new_image'] = $this->imgThumbUploadFolder;
				$thumbConfig['image_library'] = 'gd2';
				$thumbConfig['source_image'] = $config['upload_path'] . $data['file_name'];
				$thumbConfig['create_thumb'] = TRUE; //this adds _thumb to the end of the file name
				$thumbConfig['maintain_ratio'] = TRUE;

				//create thumbnails
				$this->load->library('image_lib', $thumbConfig);
				$this->image_lib->initialize($thumbConfig);
				if (!$this->image_lib->resize())
				{
					return $this->image_lib->display_errors();
				}

				//grab thumbnail info
				$thumbName = str_replace($extension, '', $data['file_name']) . '_thumb' . $extension;
				$thumbPath = $this->imgThumbUploadFolder . $thumbName;
				$thumbFilesize = round((filesize($thumbPath) / 1024 ), 2);
				$thumbInfo = getimagesize($thumbPath);

				//set image data
				$imageData = array(
					'width' => $data['image_width'],
					'height' => $data['image_height'],
					'widthHeightString' => $data['image_size_str'],
					'thumbFilename' => $thumbName,
					'thumbFilepath' => '/' . $this->imgThumbUploadFolder,
					'thumbFullPath' => '/' . $this->imgThumbUploadFolder . $thumbName,
					'thumbExtension' => $extension,
					'thumbMimeType' => $thumbInfo['mime'],
					'thumbFileSize' => $thumbFilesize,
					'thumbWidth' => $thumbInfo[0],
					'thumbHeight' => $thumbInfo[1],
					'thumbWidthHeightString' => $thumbInfo[3]
				);

			} else {
				//set image data to empty
				$imageData = array();
			}

			//regular file data
			$fileData = array(
				'filename' => $data['file_name'],
				'filepath' => '/' . $config['upload_path'],
				'fullpath' => '/' . $config['upload_path'] . $data['file_name'],
				'extension' => $data['file_ext'],
				'mimeType' => $data['file_type'],
				'fileSize' => $data['file_size']
			);

			//merge image, file data, and form data together
			$fileData = array_merge($fileData, $imageData, $formData);

			//insert in db
			$imageID = $this->image_model->insert($fileData);

			//return file id
			return $imageID;

		} else {

			//return errors
			return $this->upload->display_errors();
		}
	}
}