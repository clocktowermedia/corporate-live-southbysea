<?php
class Admin_datatables extends Admin_controller {

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//load model
		$this->load->model('campus_managers/rep_model');
		$this->load->model('campus_managers/school_model');

		//load library
		$this->load->library('datatables');
	}

	/**
	* uses datatables library to get list of pages in a json format for use in admin index
	*/
	function data()
	{
		//grab data from admins table in db
		$output = $this->datatables->select('r.repID, r.firstName, r.lastName')
			->select('s.school', false)
			->select('r.dateUpdated')
			->add_column('actions',  '<div class="btn-group">
					<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-user"></i> Campus Manager <span class="caret"></span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li><a href="/admin/campus-managers/edit/$1"><i class="fa fa-edit"></i> Edit</a></li>
						<li><a data-confirm="Are you sure you want to delete this?" href="/admin/campus-managers/delete/$1"><i class="fa fa-trash-o"></i> Delete</a></li>
					</ul>
				</div>', 'r.repID')
			->unset_column('r.repID')
			->from($this->rep_model->_table . ' r')
			->join($this->school_model->_table . ' s', 'r.schoolID = s.schoolID', 'left outer')
			->where('r.status !=', 'deleted')
			->generate();

		echo $output;
	}

	/**
	* uses datatables library to get list of pages in a json format for use in admin index
	*/
	function schools()
	{
		//grab data from admins table in db
		$output = $this->datatables->select('s.schoolID, s.school, s.dateCreated')
			->add_column('actions',  '<div class="btn-group">
					<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-institution"></i> School <span class="caret"></span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li><a href="/admin/campus-managers/schools/edit/$1"><i class="fa fa-edit"></i> Edit</a></li>
						<li><a data-toggle="ajax-modal" title="Delete this campus?" href="/admin/campus-managers/schools/merge/$1"><i class="fa fa-trash"></i> Delete</a></li>
					</ul>
				</div>', 's.schoolID')
			->unset_column('s.schoolID')
			->from($this->school_model->_table . ' s')
			->generate();

		echo $output;
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

}