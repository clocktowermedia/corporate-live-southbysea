<?php
class Campus_managers extends MY_Controller {

	//layout variables
	public $defaultLayout;

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//set layout variable(s)
		$this->defaultLayout = $this->get_default_layout();

		//load relevant models
		$this->load->model('campus_managers/rep_model');
		$this->load->model('campus_managers/school_model');
	}

	/**
	* Shows landing page
	*/
	function index()
	{
		//get page content for seo purposes
		$data['page'] = modules::run('pages/get', 'campus-managers', false);
		$data['hideBreadcrumbs'] = false;

		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->get_view();

		//show all reps by default
		$data['reps'] = $this->rep_model->getAllReps();

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}

	function careers()
	{
		//get page content for seo purposes
		$data['page'] = modules::run('pages/get', 'campus-managers', false);
		$data['hideBreadcrumbs'] = true;

		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->get_view();

		//show all reps by default
		$data['reps'] = $this->rep_model->getAllReps();

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}
	function positions($position ='')
	{
		//get page content for seo purposes
		$data['page'] = modules::run('pages/get', 'campus-managers', false);
		$data['hideBreadcrumbs'] = true;

		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->get_view();

		//show all reps by default
		$data['reps'] = $this->rep_model->getAllReps();
		$data['positions'] = $position;
		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}

	function positions_apply($position ='')
	{
		//get page content for seo purposes
		$data['page'] = modules::run('pages/get', 'campus-managers', false);
		$data['hideBreadcrumbs'] = true;

		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->get_view();

		//show all reps by default
		$data['reps'] = $this->rep_model->getAllReps();
		$data['positions'] = $position;
		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	} //positions. where do we apply at. there is nothing else to do about this except. where are you guys going to work away from.

	function apply()
	{
		//get page content for seo purposes
		$data['page'] = modules::run('pages/get', 'campus-managers/apply');
		$data['hideBreadcrumbs'] = true;

		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->get_view();

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}

	function school($schoolUrl = '')
	{
		//make sure school is valid
		$schoolUrl = $this->_school_is_valid($schoolUrl);

		//get school info
		$data['school'] = $this->school_model->get_by(array(
			'url' => $schoolUrl
		));

		//get list of reps in that school
		$schoolID[] = $data['school']['schoolID'];
		$data['reps'] = $this->rep_model->getBySchoolIDs($schoolID);

		//build breadcrumb
		$this->load->model('pages/page_model');
		$parentPage = $this->page_model->get_by(array('status' => 'enabled', 'url' => 'campus-managers'));
		$data['breadcrumb'] = '<li><a href="' . $parentPage['urlPath'] . '">' . $parentPage['title'] . '</a></li>';

		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->get_view();
		$data['title'] = $data['school']['school'];
		$data['hideBreadcrumbs'] = true;

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}

	function managers($schoolUrl = '', $repUrl = '')
	{
		//make sure all are valid
		$schoolUrl = $this->_school_is_valid($schoolUrl);
		$repUrl = $this->_rep_is_valid($repUrl);

		//get school info
		$data['school'] = $this->school_model->get_by(array(
			'url' => $schoolUrl
		));

		//get rep info
		$data['rep'] = $this->rep_model->append('school')->append('rep_image')->get_by(array(
			'status' => 'enabled',
			'url' => $repUrl,
			'schoolID' => $data['school']['schoolID']
		));

		//build breadcrumb
		$this->load->model('pages/page_model');
		$parentPage = $this->page_model->get_by(array('status' => 'enabled', 'url' => 'campus-managers'));
		$data['breadcrumb'] = '<li><a href="' . $parentPage['urlPath'] . '">' . $parentPage['title'] . '</a></li>';
		$data['breadcrumb'] .= '<li><a href="/campus-managers/school/' . $data['school']['url'] . '">' . $data['school']['school'] . '</a></li>';

		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->get_view();
		$data['title'] = $data['rep']['firstName'] . ' ' . $data['rep']['lastName'];
		$data['hideBreadcrumbs'] = true;

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

	/**
	* Checks to see that rep is valid and exists
	*/
	public function _rep_is_valid($repUrl = '')
	{
		//make sure design url is valid
		if (!is_null($repUrl) && $repUrl != '')
		{
			//check to see if the design exists
			$exists = $this->rep_model->select('repID')->get_by(array(
				'url' => $repUrl,
				'status' => 'enabled'
			));
			$exists2 = $this->rep_model->select('repID')->get_by(array(
				'url' => str_replace('_', '-', $repUrl),
				'status' => 'enabled'
			));

			//redirect if not found
			if (($exists == false || empty($exists)) && ($exists2 == false || empty($exists2)))
			{
				$this->session->set_flashdata('error', 'Campus Rep not found.');
				redirect('/campus-managers');
			}

			//send back proper version of url
			$repUrl = (!empty($exists2))? str_replace('_', '-', $repUrl) : $repUrl;
			return $repUrl;

		} else {
			$this->session->set_flashdata('error', 'Campus Rep not found.');
			redirect('/campus-managers');
		}
	}

	/**
	* Checks to see that school is valid and that it  exists
	*/
	public function _school_is_valid($schoolUrl = '')
	{
		//make sure school url is valid
		if (!is_null($schoolUrl) && $schoolUrl != '')
		{
			//check to see if the school exists
			$exists = $this->school_model->select('schoolID')->get_by(array(
				'url' => $schoolUrl
			));
			$exists2 = $this->school_model->select('schoolID')->get_by(array(
				'url' => str_replace('_', '-', $schoolUrl)
			));

			//rediret if not found
			if (($exists == false || empty($exists)) && ($exists2 == false || empty($exists2)))
			{
				$this->session->set_flashdata('error', 'School not found.');
				redirect('/campus-managers');
			}

			//send back proper version of url
			$schoolUrl = (!empty($exists2))? str_replace('_', '-', $schoolUrl) : $schoolUrl;
			return $schoolUrl;

		} else {
			$this->session->set_flashdata('error', 'School not found.');
			redirect('/campus-managers');
		}
	}
}
