<?php
class Admin extends Admin_controller {

	//layout variables
	public $defaultLayout;

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//set layout variable(s)
		$this->defaultLayout = $this->get_admin_layout();

		//load relevant models
		$this->load->model('campus_managers/rep_model');
		$this->load->model('campus_managers/school_model');
	}

	/**
	* Shows all designs in a datatable
	*/
	function index()
	{
		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Campus Reps';

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}

	/**
	* view and action for adding a design
	*/
	function create()
	{
		//tell it what view file to use
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Add Campus Rep';

		//grab categories for dropdown
		$data['schools'] = $this->school_model->order_by('school', 'asc')->dropdown('schoolID', 'school');

		//load form validation library
		$this->load->library('form_validation');

		//get validation rules from model, set extra options as neccessary
		$validationRules = $this->rep_model->validate;
		unset($validationRules['schoolID']);
		$validationRules['url']['rules'] .= '|is_unique[' . $this->rep_model->_table . '.url]';
		$validationRules['email']['rules'] .= '|is_unique[' . $this->rep_model->_table . '.email]';

		//set form validation rules
		$this->form_validation->set_rules($validationRules);

		//if the form was submitted, update database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the rep and show form errors
				$data['rep'] = $this->input->post(NULL, TRUE);
				$this->load->view($this->defaultLayout, $data);

			} else {

				//get insert data
				$insertData = $this->input->post(NULL, TRUE);

				//upload image if applicable
				if (isset($_FILES) && $_FILES['userfile']['name'] != '')
				{
					$imageID = modules::run('campus_managers/admin_image_handler/_upload_image', array(), 'rep');
					if (is_int($imageID) && $imageID > 0)
					{
						$insertData['repImageID'] = $imageID;
					} else {
						$imageID = (gettype($imageID) != 'string' && gettype($imageID) != 'boolean')? 'Image could not be uploaded.' : $imageID;
						$this->session->set_flashdata('error', $imageID);
						redirect('/admin/campus-managers/create');
					}
				}

				//save school if applicable
				if ($this->input->post('schoolName', TRUE) != '')
				{
					$schoolID = $this->school_model->insert(array('school' => $this->input->post('schoolName', TRUE)));
					$insertData['schoolID'] = $schoolID;
					unset($insertData['schoolName']);
				}

				//insert design
				$repID = $this->rep_model->insert($insertData);

				//display success message & redirect
				$this->session->set_flashdata('success', 'Campus rep was successfully created.');
				redirect('/admin/campus-managers');
			}
		} else {
			// tell it what layout to use and load the design
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	* view and action for editing a rep
	*/
	function edit($repID = 0)
	{
		//make sure rep exists
		$this->_rep_is_valid($repID);

		//grab rep info for view
		$data['rep'] = $this->rep_model->append('rep_image')->get($repID);

		//tell it what view file to use
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Edit ' . $data['rep']['firstName'] . ' ' . $data['rep']['lastName'];

		//grab categories for dropdown
		$data['schools'] = $this->school_model->order_by('school', 'asc')->dropdown('schoolID', 'school');

		//load form validation library
		$this->load->library('form_validation');

		//get validation rules from model, set extra options as neccessary
		$validationRules = $this->rep_model->validate;
		$this->form_validation->set_rules($validationRules);

		//if the form was submitted, update database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the rep and show form errors
				$this->load->view($this->defaultLayout, $data);
			} else {
				//double check that the url was not edited
				$exists = $this->rep_model->get_by(array(
					'url' => $this->input->post('url', TRUE),
					'repID !=' => $repID
				));
				$existsEmail = $this->rep_model->get_by(array(
					'email' => $this->input->post('email', TRUE),
					'repID !=' => $repID
				));
				if (!empty($exists) || isset($exists['repID']))
				{
					$this->session->set_flashdata('error', 'Campus Rep with this url already exists.');
					redirect('/admin/campus-managers/edit/' . $repID);
				}
				if (!empty($existsEmail) || isset($existsEmail['repID']))
				{
					$this->session->set_flashdata('error', 'Campus Rep with this email already exists.');
					redirect('/admin/campus-managers/edit/' . $repID);
				}

				//get insert data
				$updateData = $this->input->post(NULL, TRUE);

				//upload image if applicable
				if (isset($_FILES) && $_FILES['userfile']['name'] != '')
				{
					$imageID = modules::run('campus_managers/admin_image_handler/_upload_image', array(), 'rep');
					if (is_int($imageID) && $imageID > 0)
					{
						$updateData['repImageID'] = $imageID;
					} else {
						$imageID = (gettype($imageID) != 'string' && gettype($imageID) != 'boolean')? 'Image could not be uploaded.' : $imageID;
						$this->session->set_flashdata('error', $imageID);
						redirect('/admin/campus-managers/edit/' . $repID);
					}
				}

				//update design
				$this->rep_model->update($repID, $updateData);

				//display success message & redirect
				$this->session->set_flashdata('success', 'Campus Rep was successfully updated.');
				redirect('/admin/campus-managers');
			}
		} else {
			// tell it what layout to use and load the design
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	* Changes rep status to deleted
	*/
	function delete($repID = 0)
	{
		//make sure rep exists
		$this->_rep_is_valid($repID);

		//set status to deleted
		//$this->rep_model->soft_delete($repID);

		//delete record
		$this->rep_model->delete($repID);

		//redirect upon success
		$this->session->set_flashdata('success', 'Campus rep was deleted.');
		redirect('/admin/campus-managers/');
	}

	function promote($submissionID = 0)
	{
		if ($submissionID > 0)
		{
			//load submission model
			$this->load->model('forms/form_submission_model');
			$submission = $this->form_submission_model->getFullByID($submissionID, true);

			//find school
			$school = $this->school_model->get_by('school', $submission['school']);

			//get fields we need to insert
			$repFields = $this->rep_model->list_fields();
			$repFields = array_combine($repFields, $repFields);
			$insertData = array_intersect_key($submission, $repFields);
			$insertData['schoolID'] = (isset($school['schoolID']))? $school['schoolID'] : 0;
			unset($insertData['dateCreated'], $insertData['dateUpdated'], $insertData['status']);

			//insert the rep
			$exists = $this->rep_model->get_by('email', $insertData['email']);
			if ($exists == false || empty($exists))
			{
				$repID = $this->rep_model->skip_validation()->insert($insertData);
			} else {
				$this->session->set_flashdata('error', 'A campus rep with this email already exists.');
				redirect('/admin/forms/read/' . $submissionID);
			}

			//make sure this worked
			if ($repID > 0)
			{
				//copy over rep image
				if (isset($submission['images'][0]['imageID']) && $submission['images'][0]['imageID'] > 0)
				{
					$image = $submission['images'][0];
					unset($image['imageID'], $image['submissionID']);
					$this->load->model('campus_managers/rep_image_model');
					$imageID = $this->rep_image_model->insert($image);
					$this->rep_model->update($repID, array('repImageID' => $imageID));
				}

				//show success and redirect
				$this->session->set_flashdata('info', 'Please make sure to add any missing information for the campus rep below.');
				redirect('/admin/campus-managers/edit/' . $repID);

			} else {
				//show error and redirect
				$this->session->set_flashdata('error', 'An error occurred, please try again.');
				redirect('/admin/forms/read/' . $submissionID);
			}

		} else {
			redirect('/admin/campus-managers');
		}
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

	/**
	* Checks to see that rep ID is valid and that the rep exists
	*/
	public function _rep_is_valid($repID = 0)
	{
		//make sure rep id is an integer
		$repID = (int) $repID;

		//make sure rep id is greater than zero
		if ($repID > 0)
		{
			//check to see if the rep exists
			$exists = $this->rep_model->get($repID);

			if ($exists == false)
			{
				$this->session->set_flashdata('error', 'Invalid campus rep id.');
				redirect('/admin/campus-managers');
			}

		} else {
			$this->session->set_flashdata('error', 'Invalid campus rep id.');
			redirect('/admin/campus-managers');
		}
	}
}