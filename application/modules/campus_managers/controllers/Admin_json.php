<?php
class Admin_json extends Admin_controller {

	//all actions should call this function
	function __construct()
	{
		parent::__construct();
	}

	/**
	* Shows all designs in a datatable
	*/
	function upload()
	{
		try
		{
			//make sure we variable we need from post
			if (!$this->is_post())
			{
				throw new Exception('Must post data.');
			}

			//try to find matching schools
			if ($this->input->post('which', TRUE) == 'cropped')
			{
				$data = modules::run('campus_managers/admin_image_handler/_cropped_image', $this->input->post(NULL, FALSE));
			} else {

				//make sure rep ID was posted
				if ((int) $this->input->post('repID', TRUE) <= 0)
				{
					throw new Exception('Invalid campus manager');
				}

				$imageID = modules::run('campus_managers/admin_image_handler/_upload_image', array(), 'rep');
				if (is_int($imageID) && $imageID > 0)
				{
					$this->load->model('campus_managers/rep_model');
					$this->rep_model->skip_insert()->update((int) $this->input->post('repID', TRUE), array(
						'repImageID' => $imageID
					));

					$data['status'] = 'ok';
					$data['image'] = '';
					$data['message'] = 'Successfully uploaded new image.';

				} else {
					throw new Exception('Image could not be uploaded.' . $imageID);
				}

			}

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

}