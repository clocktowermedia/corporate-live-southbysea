<?php
class Admin_schools extends Admin_controller {

	//layout variables
	public $defaultLayout;

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//set layout variable(s)
		$this->defaultLayout = $this->get_admin_layout();

		//load relevant models
		$this->load->model('campus_managers/school_model');
	}

	/**
	* Shows all designs in a datatable
	*/
	function index()
	{
		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->_get_custom_view();
		$data['title'] = 'Campus Names';

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}

	/**
	* view and action for editing a school
	*/
	function edit($schoolID = 0)
	{
		//make sure school exists
		$this->_school_is_valid($schoolID);

		//grab school info for view
		$data['school'] = $this->school_model->get($schoolID);

		//tell it what view file to use
		$data['view'] = $this->_get_custom_view();
		$data['title'] = 'Edit ' . $data['school']['school'];

		//grab categories for dropdown
		$data['schools'] = $this->school_model->order_by('school', 'asc')->dropdown('schoolID', 'school');

		//load form validation library
		$this->load->library('form_validation');

		//get validation rules from model, set extra options as neccessary
		$validationRules = $this->school_model->validate;
		unset($validationRules['url']);
		$this->form_validation->set_rules($validationRules);

		//if the form was submitted, update database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the school and show form errors
				$this->load->view($this->defaultLayout, $data);
			} else {

				//update school
				$this->school_model->update($schoolID, $this->input->post(NULL, TRUE));

				//display success message & redirect
				$this->session->set_flashdata('success', 'Campus name was successfully updated.');
				redirect('/admin/campus-managers/schools');
			}
		} else {
			// tell it what layout to use and load the design
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	 * View (modal) for merging a school
	 * @param  integer $schoolID
	 */
	function merge($schoolID = 0)
	{
		//make sure school is valid
		$this->_school_is_valid($schoolID);

		//get school info
		$data['school'] = $this->school_model->get($schoolID);

		//see if school has users or not
		$data['schoolHasUsers'] = $this->_school_has_users($schoolID, false);
		if ($data['schoolHasUsers'] == true)
		{
			//get schools dropdown
			$data['schools'] = $this->school_model->where('schoolID !=', $schoolID)->order_by('school', 'asc')->dropdown('schoolID', 'school');
		}

		//tell it what view file to use
		$data['view'] = $this->_get_custom_view();
		$data['title'] = 'Delete ' . $data['school']['school'];

		// tell it what layout to use and load the design
		$this->load->view($data['view'], $data);
	}

	/**
	 * Action for merging a school
	 * @param  integer $schoolID
	 */
	function merging($schoolID = 0)
	{
		//if the form was submitted, update database
		if ($this->is_post())
		{
			//make sure we are posting
			$this->load->library('form_validation');
			$this->form_validation->set_rules('schoolID', 'Campus', 'required|is_natural_no_zero');

			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//redirect and show error message
				$this->session->set_flashdata('error', 'Campus must be selected.');
				redirect('/admin/campus-managers/schools');

			} else {

				//move campus managers to new school
				$this->load->model('rep_model');
				$this->rep_model->skip_validation()->update_by('schoolID', $schoolID, array('schoolID' => (int) $this->input->post('schoolID', TRUE)));

				//display success message & redirect
				$this->delete($schoolID);
			}
		}
	}

	/**
	 * Action for deleting a school
	 * @param  integer $schoolID
	 */
	function delete($schoolID = 0)
	{
		//make sure school is valid
		$this->_school_is_valid($schoolID);

		//make sure school doesn't have users
		$this->_school_has_users($schoolID);

		//delete school
		$this->school_model->delete($schoolID);

		//display success message & redirect
		$this->session->set_flashdata('success', 'Campus was successfully deleted.');
		redirect('/admin/campus-managers/schools');
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

	/**
	* Checks to see that school ID is valid and that the school exists
	*/
	public function _school_is_valid($schoolID = 0)
	{
		//make sure school id is an integer
		$schoolID = (int) $schoolID;

		//make sure school id is greater than zero
		if ($schoolID > 0)
		{
			//check to see if the school exists
			$exists = $this->school_model->get($schoolID);

			if ($exists == false)
			{
				$this->session->set_flashdata('error', 'Invalid campus name.');
				redirect('/admin/campus-managers/schools');
			}

		} else {
			$this->session->set_flashdata('error', 'Invalid campus name.');
			redirect('/admin/campus-managers/schools');
		}
	}

	private function _school_has_users($schoolID = 0, $redirect = true)
	{
		$school = $this->school_model->append('reps')->get($schoolID);

		//make sure school has no users
		if (count($school['reps']) > 0)
		{
			if ($redirect == true)
			{
				$this->session->set_flashdata('error', 'This school already has.');
				redirect('/admin/campus-managers/schools');
			} else {
				return true;
			}
		}

		return false;
	}

	private function _get_custom_view()
	{
		return 'admin/schools/' . $this->get_view();
	}
}