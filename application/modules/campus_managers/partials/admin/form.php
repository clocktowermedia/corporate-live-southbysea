<?php

	//Name Field
	$this->form_builder->text('firstName', 'First Name', (isset($rep['firstName']))? $rep['firstName'] : '', '', '', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));

	//Name Field
	$this->form_builder->text('lastName', 'Last Name', (isset($rep['lastName']))? $rep['lastName'] : '', '', '', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));

	//URL field
	$this->form_builder->text('url', 'URL', (isset($rep['url']))? $rep['url'] : '', '', '', 'Alphanumeric characters only. For SEO purposes, use lowercase letters and dashes to indicate spaces.', '', array('pattern' => '^[a-z0-9-]+$', 'required' => 'required'));
	$this->form_builder->text('instagramLink', 'Instagram Link', (isset($rep['instagramLink']))? $rep['instagramLink'] : '', '', '', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));

?>

<?php if (isset($rep['repImageID']) && $rep['repImageID'] > 0): ?>
	<div class="form-group">
		<label class="control-label" for="userfile">Current Image (Original):</label><br/>
		<a target="_blank" href="<?php echo $rep['rep_image']['fullpath']; ?>"><img src="<?php echo $rep['rep_image']['thumbFullpath']; ?>" class="img-thumbnail"></a>
	</div>
<?php endif; ?>

<?php
	//file field
	$fileMessage = (isset($rep['repImageID']) && $rep['repImageID'] > 0)? 'This will replace the image shown above' : '';
	$this->form_builder->file('userfile', 'Campus Rep Image', '', '', '', $fileMessage, '', array());

	//Email field
	$this->form_builder->email('email', 'Email', (isset($rep['email']))? $rep['email'] : '', '', '', '', '', array());

	//School field
	$this->form_builder->select('schoolID', 'Select School', array('' => 'Select a School') + $schools + array('N/A' => 'Not Listed'), (isset($rep['schoolID']))? $rep['schoolID'] : '', array('inputClass' => 'select2'), '', '', array('required' =>'required'));
	$this->form_builder->set_class('groupClass', 'form-group hide');
	$this->form_builder->text('schoolName', 'School Name', '', array('groupClass' => 'hide'), '', '', '', array('disabled' => 'disabled'));

	//account manager fields
	$this->form_builder->text('act_mgr_firstName', 'Account Manager First Name', (isset($rep['act_mgr_firstName']))? $rep['act_mgr_firstName'] : '', '', '', '', '', array());
	$this->form_builder->text('act_mgr_lastName', 'Account Manager Last Name', (isset($rep['act_mgr_lastName']))? $rep['act_mgr_lastName'] : '', '', '', '', '', array());
	$this->form_builder->email('act_mgr_email', 'Account Manager Email', (isset($rep['act_mgr_email']))? $rep['act_mgr_email'] : '', '', '', '', '', array());


	//Comments Field
	$this->form_builder->textarea('content', 'Content', (isset($rep['content']))? $rep['content'] : '', array('inputClass' => 'wysiwyg'), 10, '', 'To avoid any invalid markup, please paste your content into a notepad program first to strip out any formatting', '', array('id' => 'content'));
