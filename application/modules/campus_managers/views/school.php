<div class="col-xs-12">
	<h1><?php echo $school['school']; ?></h1>

	<div id="school-listing">
		<?php if (isset($reps) && count($reps) > 0): ?>
			<?php foreach ($reps as $rep) :?>
				 <div class="row mt-0 mb-0">
					<a href="/campus-managers/managers/<?php echo $rep['schoolUrl'];?>/<?php echo $rep['url'];?>" title="<?php echo $rep['school'] . ' - ' . $rep['firstName'] . ' ' . $rep['lastName']; ?>">
						<div class="col-xs-12">
							<div class="rep-container">
								<h4 class="pull-left rep-school"><?php echo $rep['school'];?></h4>
								<span class="pull-right rep-name"><?php echo $rep['firstName'] . ' ' . $rep['lastName']; ?></span>
							</div>
						</div>
					</a>
				</div>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>

</div>
