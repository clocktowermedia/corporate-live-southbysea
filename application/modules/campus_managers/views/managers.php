<?php if (strpos($_SERVER[REQUEST_URI], 'fraternity')): ?>
<div class="col-md-6">
    <?php if (isset($rep['rep_image']['croppedFullpath']) && $rep['rep_image']['croppedFullpath'] != ''): ?>
        <img class="img-full" src="<?php echo $rep['rep_image']['croppedFullpath']; ?>" alt="<?php echo $rep['firstName'] . ' ' . $rep['lastName']; ?>" title="<?php echo $rep['firstName'] . ' ' . $rep['lastName']; ?>" />
	<?php elseif (isset($rep['rep_image']['fullpath']) && $rep['rep_image']['fullpath'] != ''): ?>
        <img class="img-full" src="<?php echo $rep['rep_image']['fullpath']; ?>" alt="<?php echo $rep['firstName'] . ' ' . $rep['lastName']; ?>" title="<?php echo $rep['firstName'] . ' ' . $rep['lastName']; ?>" />
    <?php else: ?>
        <img class="img-full" src="/assets/images/prod-img-lg.jpg" alt="<?php echo $rep['firstName'] . ' ' . $rep['lastName']; ?>" title="<?php echo $rep['firstName'] . ' ' . $rep['lastName']; ?>" />
    <?php endif; ?>
</div><!-- /.col-md-6 -->

<div class="col-md-6">
    <h1 class="mt-0" style="font-family:NORTHWEST Bold;"><?php echo $rep['firstName'] . ' ' . $rep['lastName']; ?>
    <h4 class="bnr-ttl5-f-cm mt-0" style="font-family:NORTHWEST Bold;"><span style="">COntact</span></h4>
    <?php $this->load->helper('social'); ?>
    <div class="text-center">
      <ul class="social-icons icon-circle list-unstyled list-inline">
        <?php if ($rep['email'] != ''): ?>
            <a class="social campus-rep-social" style="padding-right:5px;" href="mailto:<?php echo $rep['email'];?>" title="<?php echo $rep['firstName'] . ' ' . $rep['lastName']; ?>'s Email">
                <i class="fa fa-envelope fa-2x" style="background-color:#D3DAE0;" aria-hidden="true"></i>
            </a>
        <?php endif; ?>
        <?php if ($rep['instagramLink'] != ''): ?>
          <a class = "campus-rep-social" href="<?php echo $rep['instagramLink']; ?>"><i class="fa fa-instagram fa-2x" style="background-color:#D3DAE0;" aria-hidden="true"></i></a>
        <?php endif; ?>
      </ul>
    </div>

    <?php if ($rep['act_mgr_firstName'] != ''): ?>
        <p class="mt-20">For questions about orders, feel free to reach out to our <?php echo $rep['school']['school']; ?> account manager <?php echo $rep['act_mgr_firstName']; ?>.</p>
        <p><?php echo $rep['act_mgr_firstName']; ?> <?php echo $rep['act_mgr_lastName']; ?>: <a href="mailto:<?php echo $rep['act_mgr_email']; ?>"><?php echo $rep['act_mgr_email']; ?></a></p>
    <?php endif; ?>

    <h4 class="bnr-ttl5-f-cm mt-0" style="font-family:NORTHWEST Bold;">About</h4>
    <p><?php echo $rep['content']; ?></p>

</div><!-- /.col-md-6 -->
<?php else: ?>
  <div class="col-md-6">
      <?php if (isset($rep['rep_image']['croppedFullpath']) && $rep['rep_image']['croppedFullpath'] != ''): ?>
          <img class="img-full" src="<?php echo $rep['rep_image']['croppedFullpath']; ?>" alt="<?php echo $rep['firstName'] . ' ' . $rep['lastName']; ?>" title="<?php echo $rep['firstName'] . ' ' . $rep['lastName']; ?>" />
  	<?php elseif (isset($rep['rep_image']['fullpath']) && $rep['rep_image']['fullpath'] != ''): ?>
          <img class="img-full" src="<?php echo $rep['rep_image']['fullpath']; ?>" alt="<?php echo $rep['firstName'] . ' ' . $rep['lastName']; ?>" title="<?php echo $rep['firstName'] . ' ' . $rep['lastName']; ?>" />
      <?php else: ?>
          <img class="img-full" src="/assets/images/prod-img-lg.jpg" alt="<?php echo $rep['firstName'] . ' ' . $rep['lastName']; ?>" title="<?php echo $rep['firstName'] . ' ' . $rep['lastName']; ?>" />
      <?php endif; ?>
  </div><!-- /.col-md-6 -->

  <div class="col-md-6">
      <h1 class="mt-0"><?php echo $rep['firstName'] . ' ' . $rep['lastName']; ?>
      <h4 class="bnr-ttl5-s-cm mt-0" style="font-size:55px;">Contact</h4>
      <?php $this->load->helper('social'); ?>
      <div class="text-center">
        <ul class="social-icons icon-circle list-unstyled list-inline">
          <?php if ($rep['email'] != ''): ?>
              <a class="social campus-rep-social" style="padding-right:5px;" href="mailto:<?php echo $rep['email'];?>" title="<?php echo $rep['firstName'] . ' ' . $rep['lastName']; ?>'s Email">
                  <i class="fa fa-envelope fa-2x" style="background-color:#EDF1F0;" aria-hidden="true"></i>
              </a>
          <?php endif; ?>
          <?php if ($rep['instagramLink'] != ''): ?>
            <a class = "campus-rep-social" href="<?php echo $rep['instagramLink']; ?>"><i class="fa fa-instagram fa-2x" style="background-color:#EDF1F0;" aria-hidden="true"></i></a>
          <?php endif; ?>
        </ul>
      </div>

      <?php if ($rep['act_mgr_firstName'] != ''): ?>
          <p class="mt-20">For questions about orders, feel free to reach out to our <?php echo $rep['school']['school']; ?> account manager <?php echo $rep['act_mgr_firstName']; ?>.</p>
          <p><?php echo $rep['act_mgr_firstName']; ?> <?php echo $rep['act_mgr_lastName']; ?>: <a href="mailto:<?php echo $rep['act_mgr_email']; ?>"><?php echo $rep['act_mgr_email']; ?></a></p>
      <?php endif; ?>

      <h4 class="bnr-ttl5-s-cm mt-0" style="font-size:55px;">About</h4>
      <p><?php echo $rep['content']; ?></p>

  </div><!-- /.col-md-6 -->
<?php endif; ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/social_media_icons.css">
