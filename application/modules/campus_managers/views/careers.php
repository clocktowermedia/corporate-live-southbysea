<?php if (strpos($_SERVER[REQUEST_URI], 'fraternity')): ?>

	<ol class="breadcrumb special-case">
				<li><a href="/">Home</a></li>
	      <li class="active">careers</li>
	</ol>

	<div class="col-md-6">
		<?php if (isset($page['page_image']['fullpath'])): ?>
			<img src="<?php echo $page['page_image']['fullpath']; ?>" alt="<?php echo $page['title']; ?>">
		<?php else: ?>
			<img src="/assets/images/comp-coffee.jpg" alt="">
		<?php endif; ?>
	</div><!-- /.col-md-6 -->
	<div class="col-md-6">
		<h4 class="mt-0" style="font-family:Northwest Regular,Times New Roman, serif;text-transform:lowercase;text-align:center;font-size:35px;">South by Sea Careers</h4></br>
		<p>South By Sea Campus Managers are passionate, motivated college students who all have one thing in common - they love the products and designs we create at South By Sea!  Each Campus Manager serves as our personal representative on campus, helping their fellow students find the perfect products and designs for their organization's custom apparel needs.  Meet your Campus Manager by finding her or him listed below.

	If your campus is not currently represented and you would like to join our team, click the button below and let us know why you would be a great fit!</p><br>
		<!-- <span style = "display: block; margin-left: auto; margin-right: auto;">
				<a href="/campus-managers/apply" title="Apply"><img src="/assets/images/NewSApplyToday.jpg" height= "40%;" width="40%;"</a>
		</span> -->
		<div class="row mb-0">
			<div class="col-xs-12">
				<h4 class="bnr-nttl4" style="color: white; font-family:CampPress Regular,Times New Roman, serif;font-size:25px; letter-spacing: 4px;">No open positions at this time</h4>
			</div>
		</div>

	</div><!-- /.col-md-6 -->

	<div class="clearfix"></div>

	<!-- <div class="row mb-0">
		<div class="col-xs-12">
			<h4 class="bnr-ttl3">&mdash; Open Positions &mdash;</h4>
		</div>
	</div> -->

	<div class="col-xs-12 hidden">
		<div id="school-listing">

					 <div class="row mt-0 mb-0">
						<a href="careers/open-positions/1" title="1">
							<div class="col-xs-12">
								<div class="rep-container">
									<h4 class="pull-left rep-school">Licensing specialist (Nashville)</h4>
									<span class="pull-right rep-name">view details</span>
								</div>
							</div>
						</a>
					</div>

	        <div class="row mt-0 mb-0">
	          <a href="careers/open-positions/2" title="2">
	           <div class="col-xs-12">
	             <div class="rep-container">
	               <h4 class="pull-left rep-school">Marketing Manager (Nashville)</h4>
	               <span class="pull-right rep-name">view details</span>
	             </div>
	           </div>
	         </a>
	       </div>

	       <div class="row mt-0 mb-0">
	         <a href="careers/open-positions/3" title="3">
	          <div class="col-xs-12">
	            <div class="rep-container">
	              <h4 class="pull-left rep-school">Payroll and Accounts Receivable Specialist (Edmonds)</h4>
	              <span class="pull-right rep-name">view details</span>
	            </div>
	          </div>
	        </a>
	      </div>

	      <div class="row mt-0 mb-0">
	        <a href="careers/open-positions/4" title="4">
	         <div class="col-xs-12">
	           <div class="rep-container">
	             <h4 class="pull-left rep-school">Airtream Street Team</h4>
	             <span class="pull-right rep-name">view details</span>
	           </div>
	         </div>
	       </a>
	     </div>


		</div>

		<!-- <script id="results-template" type="x-handlebars-template">
			{{#if search}}
				{{#if reps}}
					{{#each reps}}
						<div class="row mt-0 mb-0">
							<a href="/campus-managers/managers/{{schoolUrl}}/{{url}}" title="{{school}} - {{firstName}} {{lastName}}">
								<div class="col-xs-12">
									<div class="rep-container">
										<h4 class="pull-left rep-school">{{school}}</h4>
										<span class="pull-right rep-name">{{firstName}} {{lastName}}</span>
									</div>
								</div>
							</a>
						</div>
					{{/each}}
				{{else}}
					<p>No matches found.</p>
				{{/if}}
			{{/if}}
		</script> -->

	</div>



<?php else: ?>


<ol class="breadcrumb ">
			<li><a href="/">Home</a></li>
      <li class="active">careers</li>
</ol>

<div class="col-md-6">
	<?php if (isset($page['page_image']['fullpath'])): ?>
		<img src="<?php echo $page['page_image']['fullpath']; ?>" alt="<?php echo $page['title']; ?>">
	<?php else: ?>
		<img src="/assets/images/comp-coffee.jpg" alt="">
	<?php endif; ?>
</div><!-- /.col-md-6 -->
<div class="col-md-6">
	<h4 class="mt-0" style="font-family:Hello Stockholm Alt,Times New Roman, serif;text-transform:lowercase;text-align:center;font-size:100px;">South by Sea Careers</h4>
	<p>South By Sea Campus Managers are passionate, motivated college students who all have one thing in common - they love the products and designs we create at South By Sea!  Each Campus Manager serves as our personal representative on campus, helping their fellow students find the perfect products and designs for their organization's custom apparel needs.  Meet your Campus Manager by finding her or him listed below.

If your campus is not currently represented and you would like to join our team, click the button below and let us know why you would be a great fit!</p><br>
	<!-- <span style = "display: block; margin-left: auto; margin-right: auto;">
			<a href="/campus-managers/apply" title="Apply"><img src="/assets/images/NewSApplyToday.jpg" height= "40%;" width="40%;"</a>
	</span> -->
	<div class="row mb-0">
		<div class="col-xs-12">
			<h4 class="bnr-nttl3" style="color: white; font-family:Hello Stockholm Alt,Times New Roman, serif;font-size:50px; letter-spacing: 4px;">No open positions at this time</h4>
		</div>
	</div>

</div><!-- /.col-md-6 -->

<div class="clearfix"></div>

<!-- <div class="row mb-0">
	<div class="col-xs-12">
		<h4 class="bnr-ttl3">&mdash; Open Positions &mdash;</h4>
	</div>
</div> -->

<div class="col-xs-12 hidden">
	<div id="school-listing">

				 <div class="row mt-0 mb-0 hidden">
					<a href="careers/open-positions/1" title="1">
						<div class="col-xs-12">
							<div class="rep-container">
								<h4 class="pull-left rep-school">Licensing specialist (Nashville)</h4>
								<span class="pull-right rep-name">view details</span>
							</div>
						</div>
					</a>
				</div>

        <div class="row mt-0 mb-0 hidden">
          <a href="careers/open-positions/2" title="2">
           <div class="col-xs-12">
             <div class="rep-container">
               <h4 class="pull-left rep-school">Marketing Manager (Nashville)</h4>
               <span class="pull-right rep-name">view details</span>
             </div>
           </div>
         </a>
       </div>

       <div class="row mt-0 mb-0 hidden">
         <a href="careers/open-positions/3" title="3">
          <div class="col-xs-12">
            <div class="rep-container">
              <h4 class="pull-left rep-school">Payroll and Accounts Receivable Specialist (Edmonds)</h4>
              <span class="pull-right rep-name">view details</span>
            </div>
          </div>
        </a>
      </div>

      <div class="row mt-0 mb-0 hidden">
        <a href="careers/open-positions/4" title="4">
         <div class="col-xs-12">
           <div class="rep-container">
             <h4 class="pull-left rep-school">Airtream Street Team</h4>
             <span class="pull-right rep-name">view details</span>
           </div>
         </div>
       </a>
     </div>


	</div>

	<!-- <script id="results-template" type="x-handlebars-template">
		{{#if search}}
			{{#if reps}}
				{{#each reps}}
					<div class="row mt-0 mb-0">
						<a href="/campus-managers/managers/{{schoolUrl}}/{{url}}" title="{{school}} - {{firstName}} {{lastName}}">
							<div class="col-xs-12">
								<div class="rep-container">
									<h4 class="pull-left rep-school">{{school}}</h4>
									<span class="pull-right rep-name">{{firstName}} {{lastName}}</span>
								</div>
							</div>
						</a>
					</div>
				{{/each}}
			{{else}}
				<p>No matches found.</p>
			{{/if}}
		{{/if}}
	</script> -->

</div>

<?php endif; ?>
