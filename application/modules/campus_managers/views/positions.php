<ol class="breadcrumb ">
			<li><a href="/">Home</a></li>
      <li><a href="/careers">Careers</a></li>
      <li class="active">open-positions</li>
</ol>

<div class="col-md-12">
  <?php if ($positions==="1"): ?>
      <h4 class="bnr-ttl3 mt-0">&mdash; Licensing specialist (Nashville) &mdash;</h4>
  <?php endif; ?>
  <?php if ($positions==="2"): ?>
      <h4 class="bnr-ttl3 mt-0">&mdash; Marketing Manager (Nashville) &mdash;</h4>
  <?php endif; ?>
  <?php if ($positions==="3"): ?>
      <h4 class="bnr-ttl3 mt-0">&mdash; Payroll and Accounts Receivable Specialist (Edmonds) &mdash;</h4>
  <?php endif; ?>
  <?php if ($positions==="4"): ?>
      <h4 class="bnr-ttl3 mt-0">&mdash; Airtream Street Team &mdash;</h4>
  <?php endif; ?>

  <?php if ($positions==="1"): ?>
    <p><section id="postingbody">
      <b>Who we are</b><br>
      From our office in Edmonds, we sell custom decorated apparel, accessories, and promotional items specializing in the collegiate market online and are able to run production from our state of the art print shop located in Nashville, Tennessee. Recently we were recognized as the #638 fastest growing company in the 2016 INC 5000 as well as the #21 fastest growing company in the 2016 PSBJ, displaying 3 year growth of 608%. Our success has largely stemmed from a team of dedicated and passionate employees combined with a fun, exciting work environment.<br>
      <br>
      <b>Who we are looking for</b><br>
      Seeking an extremely organized and detail oriented licensing specialist. The perfect candidate excels in a fast paced environment, is a self motivated individual, is capable of multitasking, extremely organized, and capable of prioritizing a range of duties while maintaining the standards of our clients and company.<br>
      <br>
      <b>Responsibilities</b><br>
      Execute daily tasks with accuracy. Duties include but are not limited to: data entry, licensing review, logistics, etc.<br>
      Independently and quickly resolve a wide array of daily challenges and order complications requiring immediate attention<br>
      Prioritize and effectively handle a consistent stream of emails requests from account managers and licensors.<br>
      Obtain and maintain collegiate licenses through various application processes <br>
      Regulate submitting and approving all designs through each licensing outlet<br>
      Maintain licensee accounts<br>
      Monitor online retail sites and various social media posts<br>
      Generate and submit quarterly royalty reports    <br>
      <br>
      <b>Qualifications</b><br>
      Bachelor's Degree Preferred <br>
      Ability to multi-task and prioritize responsibilities<br>
      Proactively problem solve and make sound decisions under pressure and deadlines<br>
      Strong organization and time management skills<br>
      Proficiency in Microsoft Excel, Word, Outlook/Gmail<br>
      Ability to quickly learn and adhere to very specific and detailed processes<br>
      Positive attitude and self-motivated <br>
      Team player, willing to take on multiple roles<br>
      Knowledge of Photoshop a huge plus!<br>
      <br>
      <b>Benefits</b><br>
      Medical, Dental and Vision Benefits<br>
      Generous Paid Time Off program<br>
      10 Paid Holidays including Summer Solstice and Black Friday<br>
      Career advancement opportunities within a young and fast growing company<br>
      Energetic, Creative and Fun work environment<br>
      Subsidized Gym Membership<br>
      <br>
      <b>Compensation</b><br>
      Full Time<br>
      DOE, paid as full time base salary, plus Bonus Opportunities<br>
      Flexible work schedule from Edmonds office<br>
      <br>
      We are an Equal Employment Opportunity employer. All qualified applicants will receive consideration for employment without regard to race, color, religion, gender, national origin, disability status, protected veteran status or any other characteristic protected by law.<br>
      <br>
    </section></p>
  <?php endif; ?>
  <?php if ($positions==="2"): ?>
    <p><section id="postingbody">
      <b>Who we are</b><br>
      From our office in Edmonds, we sell custom screen printed and embroidered apparel, accessories, and promotional items. We primarily sell to the collegiate market online and are able to run production from our state of the art print shop located in Nashville Tennessee. Recently we were recognized as the #638 fastest growing company in the 2016 INC 5000 as well as the #21 fastest growing company in the 2016 PSBJ, displaying 3 year growth of 608%. Our success has largely stemmed from a team of dedicated and passionate employees combined with a fun, exciting work environment.<br>
      <br>
      <b>Who we are looking for</b><br>
      Seeking a self-motivated individual to manage a host of duties related to marketing and social media. The position will work directly with the sales team and Creative Director to help plan, launch, and analyze marketing strategies and content creation across all social media platforms. They will work closely with the sales team to bring content to life and represent the South by Sea brand across a multi-platform landscape. The successful candidate must possess strong creative and strategic thinking skills, is analytical and highly organized. They are a leader and prioritize relationships and collaboration.<br>
      <br>
      <b>Responsibilities</b><br>
      Manage and grow all South by Sea social media channels (Facebook, Twitter, Instagram, Pinterest, etc)<br>
      Work directly with the sales team and collaborate on social media content, campaigns, and promotion budgets<br>
      Manage expenses for advertising and creative services within project budget guidelines<br>
      Create relevant content based on current industry trends or customer interests<br>
      Respond to media inquiries<br>
      Work closely with Creative Director to align content with company branding and vision<br>
      Work closely with sales team to develop short, medium and long term social media channel and content strategies<br>
      Concept, plan and execute innovative social media content, strategies and tactics targeted at engaging new and existing customers.<br>
      Plan and manage annual content calendar <br>
      Collect and analyze data and identify trends<br>
      Develop relationships and collaborate with relevant external partners, sponsorships, and events to increase engagement and growth<br>
      Develop and continuously update best practices and manage recommended rules of engagement<br>
      Create and manage individual campus Instagram accounts<br>
      <br>
      <br>
      <b>Qualifications</b><br>
      Bachelors Degree in marketing, communications or a relevant field of study<br>
      Relevant experience in social media marketing including writing, editing, and crafting content<br>
      Creative eye and can produce on-the-fly content<br>
      Experience in web analytics software and social media content management tools<br>
      Advanced knowledge of social media analytics measurement, reporting and analysis<br>
      A passion for the social media, building community, and keen awareness of online trends<br>
      Direct experience producing and developing branded social content that will engage customers<br>
      Ability to engage customers<br>
      Excellent attention to detail and organizational skills<br>
      Superior written and verbal communication skills <br>
      Strong PC skills<br>
      Highly motivated and collaborative<br>
      Strategic, analytic and structured thinking ability<br>
      A driven, service-oriented team player motivated to innovate and take smart risks.<br>
      Passion for trending fashion, lifestyle and design<br>
      Ability to work professionally with a wide range of internal and external personalities<br>
      Excellent problem-solving skills and the ability to prioritize and multi-task in a fast-paced environment<br>
      <br>
      <b>Benefits</b><br>
      Medical, Dental and Vision Benefits<br>
      Generous Paid Time Off program<br>
      10 Paid Holidays including Summer Solstice and Black Friday<br>
      Career advancement opportunities within a young and fast growing company<br>
      Energetic, Creative and Fun work environment<br>
      Subsidized Gym Membership<br>
      <br>
      <b>Compensation</b><br>
      DOE, paid as full time base salary<br>
      Please include one tweet (140 characters or less) that sums up your personality and your Instagram account handle<br>
      <br>
    </section></p>
  <?php endif; ?>
  <?php if ($positions==="3"): ?>
    <p><section id="postingbody">
      <b>WHO WE ARE </b><br>
      As a company we make t-shirts for the collegiate market and jointly produce/release music. Our new warehouse in Nashville hosts screen printing equipment, a shipping department, and a fully equipped recording studio. From our office in Edmonds, we coordinate the marketing, sales and customer service for all of our t-shirt orders. We are a young, fast-paced, and rapidly growing company. We attribute our success to our team of dedicated and passionate employees combined with a fun, exciting work environment. <br>
      <br>
      <b>RESPONSIBILITIES</b><br>
      Accounts Receivable<br>
      -Create invoices for bulk orders <br>
      -Post weekly CC transactions<br>
      -Manage AR Aging<br>
      -Maintain customer accounts<br>
      -Process received checks<br>
      -Prepare bank deposits<br>
      -Manage collection of larger customer balances<br>
      Payroll<br>
      -Enter new employee and 1099 contractor information<br>
      -Generate commission reports<br>
      -Process Bi-monthly payroll, and weekly payroll.<br>
      <br>
      <b>QUALIFICATIONS</b><br>
      -Bachelor's Degree Preferred (Accounting/Business/HR)<br>
      -1-2 years of accounting experience<br>
      -Excellent communicator and creative thinker<br>
      -Strong work ethic<br>
      -Positive attitude and self-motivated <br>
      -Team player, willing to take on multiple roles<br>
      <br>
      <b>COMPENSATION</b><br>
      -Full Time<br>
      -Flexible work schedule from Edmonds office<br>
      -DOE<br>
      <br>
    </section></p>
  <?php endif; ?>
  <?php if ($positions==="4"): ?>
    <p><section id="postingbody"><b>Job Overview</b><br>
      South by Sea is seeking two upbeat, adventurous individuals to travel from campus to campus for a 3 month contract in our vintage Airstream converted into a mobile showroom. As representatives of South by Sea, the goal is to increase sales on college campuses all over the country by showcasing products & services we offer and building relationships with t-shirt chairs while providing a unique, hands-on experience.
      <br> <br>
      <b>Job Functions</b><br>
      - Plan route and schedule stops in detail with Area Sales Manager & National Sales Manager <br>
      - Review sales reports and notes provided by Area Sales Manager & National Sales Manager before meeting with customers <br>
      - Adhere to a strict schedule to ensure maximum number of campuses visited & increase efficiency <br>
      - Provide product knowledge of all garments showcased <br>
      - Merchandise showroom to fit customer’s needs <br>
      - Coordinate with Area Sales Manager, National Sales Manager and campus manager on location for the Airstream <br>
      - Cultivate positive relationships with campus managers and maintain front-of-mind brand awareness and loyalty <br>
      - Hand out promotional items in Airstream, but also door-to-door <br>
      - Ensure professionalism, exceptional communication, and positive interactions with t-shirt chairs as the face of the company <br>
      - Have general knowledge of the company to answer questions and relay information on site <br>
      - Report back to Area Sales Manager & National Sales Manager with any feedback and insights provided during interactions <br><br>
      <b>Requirements</b><br>
      - very comfortable driving an SUV while pulling a medium sized trailer***no training provided <br>
      - must be able to work consecutive days: non-stop travel for 3 months, 7 days a week <br>
      - must have a clean driving record - extremely personable with a fun, upbeat personality <br>
      - excellent, natural communication skills <br>
      - quick learner <br>
      - team player <br>
      - maintain brand appearance and wear proper attire <br>
      - arrive on time and adhere to strict schedule <br>
      - strict start date of week of August 21, starting in Nashville, TN <br>
      - greek affiliation preferred<br>

      Compensation: - $5,000/mo salary ($15,000 total earnings) - per diem for food and hotels TBD
      <br>
    </section></p>
  <?php endif; ?>

  <div class="form-group text-center mt-100">
		<a href="<?php echo $positions; ?>/apply" title="Apply"><button type="button" class="btn btn-info pull-none">Apply</button></a>
	</div>

</div><!-- /.col-md-6 -->

<div class="clearfix"></div>
