<?php if (strpos($_SERVER[REQUEST_URI], 'fraternity')): ?>

	<div class="col-xs-12">
		<h1 style="font-family:NORTHWEST Bold;text-transform:uppercase;"><?php echo $page['title'];?></h1><br>
		<p><?php echo $page['content']; ?></p>

		<?php echo form_open_multipart('/forms/rep-application', array('id' => 'rep-app-form', 'class' => 'persist', 'accept-charset' => 'UTF-8')); ?>
		<fieldset>
		    <?php
		        //add the form
	            $this->load->view('../../forms/partials/forms/rep-application');

		        //submit button
		        $this->form_builder->button('submit', '', array('inputClass' => 'frat-special btn btn-info'), '', '');
		    ?>
		</fieldset>
		</form>
	</div>


<?php else: ?>

	<div class="col-xs-12">
		<h1><?php echo $page['title'];?></h1>
		<p><?php echo $page['content']; ?></p>

		<?php echo form_open_multipart('/forms/rep-application', array('id' => 'rep-app-form', 'class' => 'persist', 'accept-charset' => 'UTF-8')); ?>
		<fieldset>
		    <?php
		        //add the form
	            $this->load->view('../../forms/partials/forms/rep-application');

		        //submit button
		        $this->form_builder->button('submit', '', array('inputClass' => 'btn btn-info'), '', '');
		    ?>
		</fieldset>
		</form>
	</div>


<?php endif; ?>
