<div class="col-xs-12">
	<h1>Apply</h1>
	<p>South By Sea is always looking for passionate, motivated employees!<br><br>
		If you would like to join our team, attach a resume and we will get in touch!</p><br>

	<?php echo form_open_multipart('/forms/rep-application', array('id' => 'rep-app-form', 'class' => 'persist', 'accept-charset' => 'UTF-8')); ?>
	<?php echo form_open('/forms/contact', array('accept-charset' => 'UTF-8')); ?>
	<fieldset>
		<?php
			//add the form
			$this->load->view('../../forms/partials/forms/contact');
		?>
		<?php if ($this->uri->segment(1) != 'admin'):?>
			<div class="row" id="upload-container">
				<div class="col-xs-12">
					<div class="form-group pull-left">
						<span><button type="button"class ="btn btn-info"><i class="arrow-single-left"></i><a id="upload-btn">Upload your resume here!</a></button><br>
							<?php if ($positions==="1"): ?>
						      <small><a href="mailto:victoria@southbysea.com?subject=Licensing%20Specialist%20Application">Resume format not correct? Email us here</a></small>
						  <?php endif; ?>
							<?php if ($positions==="2"): ?>
						      <small><a href="mailto:robyn@southbysea.com?subject=Marketing%20Manager%20Application">Resume format not correct? Email us here</a></small>
						  <?php endif; ?>
							<?php if ($positions==="3"): ?>
						      <small><a href="mailto:accounting@southbysea.com?subject=Payroll%20and%20Accounts%20Receivable%20Application">Resume format not correct? Email us here</a></small>
						  <?php endif; ?>
							<?php if ($positions==="4"): ?>
						      <small><a href="mailto:victoria@southbysea.com?subject=Airtream%20Street%20Team%20Application">Resume format not correct? Email us here</a></small>
						  <?php endif; ?>
					</div><br>
					<input id="upload-trigger" type="file" name="userfile[]" class="hide" multiple="multiple" title="Click to add Files">
				</div>
			</div>

			<div id="uploaded-images">
			</div>

			<script id="form-images-listing" type="x-handlebars-template">
				{{#if image}}
					<div id="attached-image-{{image.imageID}}" class="attached-image">
						<div class="col-sm-4 col-xs-6 thumbnail-img" id="images-{{image.imageID}}" data-id="{{image.imageID}}" data-queue="{{image.queueID}}">
							<a data-id="{{image.imageID}}" data-queue="{{image.queueID}}" title="Remove Image" class="delete-img">
								<img src="/assets/images/close.png" alt="Remove Image" title="Remove Image" />
							</a>
							{{#compare image.mimeType "~=" 'image'}}
								<img src="{{image.thumbFullpath}}" class="img-responsive width-100">
							{{/compare}}
							{{#compare image.mimeType "!~=" 'image'}}
								<a href="{{image.fullpath}}" target="_blank">
									<img src="http://dummyimage.com/200x200/eee/57656e&text=No+Preview" class="img-responsive width-100">
								</a>
							{{/compare}}
						</div>
						<input type="hidden" name="images[]" value="{{image.imageID}}">
					</div>
				{{/if}}
			</script>
		<?php else: ?>
			<?php if (isset($submission['images']) && count($submission['images']) > 0): ?>
			<div class="row" id="uploaded-images">
				<div class="col-xs-12">
					<h4>Uploaded Images</h4>
				</div>
				<?php foreach ($submission['images'] AS $image): ?>
					<div class="col-sm-2 col-xs-4 thumbnail-img">
						<a href="<?php echo $image['fullpath'];?>" target="_blank">
							<?php if (isset($image['thumbWidth']) && $image['thumbWidth'] != ''): ?>
								<img src="<?php echo $image['thumbFullpath'];?>" class="img-responsive">
							<?php else: ?>
								<img src="http://dummyimage.com/200x200/eee/57656e&text=No+Preview" class="img-responsive">
							<?php endif; ?>
						</a>
					</div>
				<?php endforeach; ?>
			</div>
			<br/>
			<?php endif; ?>
			<?php endif; ?>

		<?php
			//submit button
			$this->form_builder->button('submit', 'Submit', array('inputClass' => 'btn btn-info'), '', '');
		?>
	</fieldset>
	</form>
</div>
