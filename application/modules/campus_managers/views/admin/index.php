<div class="row">
	<div class="col-xs-12">

		<div class="table-responsive">
			<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered datatable" width="100%">
				<thead>
					<tr>
						<th data-class="phone, tablet">First Name</th>
						<th data-hide="expand">Last Name</th>
						<th data-hide="phone">School</th>
						<th data-hide="phone, tablet">Date Updated</th>
						<th>Actions</th>
					</tr>
				</thead>

				<tbody>
				</tbody>
			</table>
		</div>

	</div>
</div>