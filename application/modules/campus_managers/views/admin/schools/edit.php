<div class="row">
	<div class="col-xs-12">

		<?php echo form_open_multipart('admin/campus-managers/schools/edit/' . $school['schoolID']); ?>
		<fieldset>
			<?php
				//add the form
				$this->form_builder->text('school', 'Campus Name', (isset($school['school']))? $school['school'] : '', '', '', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));

				//submit button
				$this->form_builder->button('submit', 'Update Campus Name', array('inputClass' => 'btn btn-info'), '', '');
			?>
		</fieldset>
		</form>

	</div>
</div>