<div class="row">
	<div class="col-xs-12">

		<div class="table-responsive">
			<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered datatable" width="100%">
				<thead>
					<tr>
						<th data-hide="expand">School</th>
						<th data-hide="phone, tablet">Date Created</th>
						<th>Actions</th>
					</tr>
				</thead>

				<tbody>
				</tbody>
			</table>
		</div>

	</div>
</div>