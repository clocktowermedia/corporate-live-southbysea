<?php if ($schoolHasUsers): ?>

	<div class="modal-body">
		<p>This campus currently has users assigned to it. Please select a new campus from the list below to assign these users to.</p>

		<?php echo form_open_multipart('admin/campus-managers/schools/merging/' . $school['schoolID']); ?>
		<fieldset>
			<?php
				//add the form
				$this->form_builder->select('schoolID', '', array('' => 'Select a School') + $schools, '', '', '', '', array('required' =>'required'));

				//submit button
				$this->form_builder->button('submit', 'Delete Campus', array('inputClass' => 'btn btn-info'), '', '');
			?>
		</fieldset>
		</form>
	</div>

<?php else: ?>
	<div class="modal-body">
		<p>Are you sure you want to delete this school?</p>
	</div>
	<div class="modal-footer">
		<a class="btn btn-info" href="/admin/campus-managers/schools/delete/<?php echo $school['schoolID']; ?>">Confirm Deletion</a>
		<a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a>
	</div>
		
<?php endif; ?>