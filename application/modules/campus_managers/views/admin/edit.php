<div class="row">
	<div class="col-xs-12">

		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs">
				<li role="presentation" class="active"><a aria-controls="profile" href="#profile" data-toggle="tab" role="tab">Profile</a></li>
				<li role="presentation"><a aria-controls="image" href="#image" data-toggle="tab" role="tab">Crop Image</a></li>
			</ul>
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="profile">
					<?php echo form_open_multipart('admin/campus-managers/edit/' . $rep['repID']); ?>
					<fieldset>
						<?php
							//add the form
							$this->load->view('../partials/admin/form');

							//submit button
							$this->form_builder->button('submit', 'Update Campus Rep', array('inputClass' => 'btn btn-info'), '', '');
						?>
					</fieldset>
					</form>
				</div>

				<div role="tabpanel" class="tab-pane" id="image">

					<div class="row">
						<?php if (isset($rep['repImageID']) && $rep['repImageID'] > 0): ?>
						<div class="col-xs-12 col-sm-6">
							<h3 class="mt-0">Crop the Original</h3>
							<div id="profile-image-cropper">
								<img src="<?php echo $rep['rep_image']['fullpath']; ?>?t=<?php echo rand(1, 10000000); ?>" data-left="<?php echo $rep['rep_image']['croppedLeft'];?>" data-top="<?php echo $rep['rep_image']['croppedTop'];?>"/>
							</div><br/>

							<div id="profile-image-cropper-btns" class="">
								<button class="btn btn-lg zoom_in"><i class="fa fa-plus fa-2"></i></button>
								<button class="btn btn-lg zoom_out"><i class="fa fa-minus fa-2"></i></button>
								<button class="btn btn-lg done">Done</button>
							</div>

							<input id="zoom-slider" class="slider form-control" type="hidden">

							<form id="cropperForm" action="/admin/campus-managers/json/upload" method="post">
								<input type="hidden" name="imageID" value="<?php echo $rep['repImageID']; ?>" />
								<input type="hidden" name="which" value="cropped" />
								<input type="hidden" name="image_left" value="" />
								<input type="hidden" name="image_top" value="" />
								<input type="hidden" name="image" value="" />
							</form>

						</div>
						<?php endif; ?>

						<div class="col-xs-12 col-sm-6">
							<h3 class="mt-0">Cropped Image</h3>
							<div class="cropped-img-container">
								<?php if (isset($rep['repImageID']) && $rep['rep_image']['croppedFullpath'] != ''): ?>
									<img class="image-full img-full profile-image-cropped" src="<?php echo $rep['rep_image']['croppedFullpath']; ?>?t=<?php echo rand(1, 10000000); ?>" />
								<?php else: ?>
									<p>Looks like you haven't cropped the image image yet.</p>
								<?php endif; ?>
							</div>

							<!--
							<form id="uploadForm" action="/admin/campus-managers/json/upload" method="post" enctype="multipart/form-data">
								<input type="hidden" name="which" value="original" />
								<input type="file" name="userfile" />
								<input type="hidden" name="repID" value="<?php echo $rep['repID']; ?>" />
								<input type="submit" class="btn" value="Upload" />
							</form>
							-->
						</div>
					</div>

				</div>
			</div>
		</div>

	</div>
</div>