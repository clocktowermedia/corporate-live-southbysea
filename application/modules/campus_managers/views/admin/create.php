<div class="row">
	<div class="col-xs-12">

		<?php echo form_open_multipart('admin/campus-managers/create'); ?>
		<fieldset>
			<?php
				//add the form
				$this->load->view('../partials/admin/form');

				//submit button
				$this->form_builder->button('submit', 'Create Campus Rep', array('inputClass' => 'btn btn-info'), '', '');
			?>
		</fieldset>
		</form>

	</div>
</div>