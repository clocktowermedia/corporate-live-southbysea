<?php
/********************************************************************************************************
/
/ Schools
/ Explanation: Campus Reps belong to a school
/
********************************************************************************************************/
class School_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'Schools';

	//primary key
	public $primary_key = 'schoolID';

	//relationships
	public $has_many = array(
		'reps' => array('model' => 'campus_managers/rep_model', 'primary_key' => 'repID', 'join_key' => 'schoolID')
	);

	//callbacks/observers (use function names)
	public $before_create = array('_create_timestamp', '_uppercase_school', '_create_url');
	public $before_update = array('_uppercase_school', '_create_url');

	//validation
	public $validate = array(
		'school' => array(
			'field' => 'school',
			'label' => 'School',
			'rules' => 'required|min_length[2]'
		)
	);

	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/

	/**
	 * Provides a list of matching school names based on what the user is typing
	 * @param  string  $query [the string the user is typing]
	 * @param  integer $limit [limit the number of matches returned]
	 * @return array
	 */
	function autosuggest($query = '', $limit = 20)
	{
		//make sure we have a valid search term
		if (!is_null($query) && $query != '')
		{
			//load model to get tablename(s) that we will need
			$this->load->model($this->has_many['reps']['model'], 'rep_model');

			//start query
			$this->db->select('s.school AS id', false)
				->select('s.school AS text', false)
				->select('COUNT(r.repID) AS repsPerSchool', false)
				->like("s.school", $query)
				->from($this->_table . ' AS s')
				->join($this->rep_model->_table . ' AS r', "r.$this->primary_key = s.$this->primary_key", 'left outer')
				->having('repsPerSchool >', 0);

			//add limit/offset
			if ($limit > 0)
			{
				$this->db->limit($limit);
			}

			//get/return results
			$results = $this->db->group_by('s.' . $this->primary_key)->get()->result_array();
			return $results;
		}

		return false;
	}

	/**
	 * Provides a list of school ID's based on what the user is typing
	 * @param  string  $query [the string the user is typing]
	 * @param  integer $limit [limit the number of matches returned]
	 * @return array
	 */
	function autosuggest_ids($query = '', $limit = 20)
	{
		//make sure we have a valid search term
		if (!is_null($query) && $query != '')
		{
			//load model to get tablename(s) that we will need
			$this->load->model($this->has_many['reps']['model'], 'rep_model');

			//start query
			$this->db->select("s.$this->primary_key")
				->select('COUNT(r.repID) AS repsPerSchool', false)
				->like("s.school", $query)
				->from($this->_table . ' AS s')
				->join($this->rep_model->_table . ' AS r', "r.$this->primary_key = s.$this->primary_key", 'left outer')
				->having('repsPerSchool >', 0);

			//add limit/offset
			if ($limit > 0)
			{
				$this->db->limit($limit);
			}

			//get/return results
			$results = $this->db->group_by('s.' . $this->primary_key)->get()->result_array();

			if ($results != false && count($results) > 0)
			{
				foreach ($results as $result)
				{
					$idArray[] = $result[$this->primary_key];
				}

				return $idArray;
			}

			return false;
		}

		return false;
	}

	/**
	 * Only grab information relevant for the sitemap xml
	 * @return array
	 */
	public function getSitemapXmlInfo()
	{
		return $this->db->select('CONCAT("/campus-managers/school/", url) AS link', false)
			->select('DATE_FORMAT(NOW(), "%Y-%m-%d") AS dateUpdated', false)
			->from($this->_table)
			->get()
			->result_array();
	}

	/**********************************************************************************************
	*
	* Validity checker functions & helper functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
	protected function _create_timestamp($data)
	{
		$data['dateCreated'] = date('Y-m-d H:i:s');
		return $data;
	}

	protected function _create_url($data)
	{
		if (isset($data['url']) && $data['url'] != '')
		{
			$data['url'] = strtolower($data['url']);
		} else {
			$data['url'] = strtolower(url_title($data['school']));
		}
		return $data;
	}

	protected function _uppercase_school($data)
	{
		if (isset($data['school']) && $data['school'] != '')
		{
			$data['school'] = ucwords($data['school']);
		}
		return $data;
	}
}