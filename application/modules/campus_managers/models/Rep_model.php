<?php
/********************************************************************************************************
/
/ Campus Reps
/
********************************************************************************************************/
class Rep_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'Reps';

	//primary key
	public $primary_key = 'repID';

	//relationships
	public $has_single = array(
		'school' => array('model' => 'campus_managers/school_model', 'primary_key' => 'schoolID', 'join_key' => 'schoolID'),
		'rep_image' => array('model' => 'campus_managers/rep_image_model', 'primary_key' => 'repImageID', 'join_key' => 'repImageID')
	);

	//callbacks/observers (use function names)
	public $before_create = array('_create_timestamp', '_uppercase_names', '_uppercase_mgr_names', '_create_url');
	public $before_update = array('_create_update_timestamp', '_uppercase_names', '_uppercase_mgr_names', '_create_url');

	//validation
	public $validate = array(
		'firstName' => array(
			'field' => 'firstName',
			'label' => 'First Name',
			'rules' => 'required|min_length[2]'
		),
		'lastName' => array(
			'field' => 'lastName',
			'label' => 'First Name',
			'rules' => 'required|min_length[2]'
		),
		'url' => array(
			'field' => 'url',
			'label' => 'URL',
			'rules' => 'required|seo_url'
		),
		'email' => array(
			'field' => 'email',
			'label' => 'Email',
			'rules' => 'valid_email'
		),
		'schoolID' => array(
			'field' => 'schoolID',
			'label' => 'School',
			'rules' => 'required|is_natural_no_zero'
		)
	);


	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/
	/**
	 * Return all reps in the specified schools by ID
	 * @param  array  $schoolIDs [list of school IDs]
	 * @return array
	 */
	function getBySchoolIDs(array $schoolIDs = array())
	{
		if (count($schoolIDs) > 0)
		{
			//load school model to get info for joining
			$this->load->model($this->has_single['school']['model'], 'school_model');
			$schoolTable = (string) $this->school_model->_table;

			//get all reps in that school
			$reps = $this->rep_model->with_select("school, $schoolTable.url AS schoolUrl")->with('school')
				->append('rep_image')
				->where_in($this->_table . '.schoolID', $schoolIDs)
				->order_by('school')
				->get_many_by('status', 'enabled');

			return $reps;
		}

		return false;
	}

	/**
	 * Get all school reps with some appended data
	 * @return array
	 */
	function getAllReps()
	{
		//load school model to get info for joining
		$this->load->model($this->has_single['school']['model'], 'school_model');
		$schoolTable = (string) $this->school_model->_table;

		//get matching results
		$reps = $this->rep_model->with_select("school, $schoolTable.url AS schoolUrl")->with('school')
			->append('rep_image')
			->order_by('school')
			->get_many_by('status', 'enabled');

		//return results
		return $reps;
	}

	/**
	 * Only grab information relevant for the sitemap xml
	 * @return array
	 */
	function getSitemapXmlInfo()
	{
		//load school model to get info for joining
		$this->load->model($this->has_single['school']['model'], 'school_model');
		$schoolTable = (string) $this->school_model->_table;
		$joinKey = $this->has_single['school']['primary_key'];

		//define query & return results
		return $this->db->select('r.dateUpdated')
			->select('CONCAT("/campus-managers/managers/", s.url, "/", r.url) AS link', false)
			->where('r.status', 'enabled')
			->from($this->_table . ' AS r')
			->join($schoolTable . ' AS s', "s.$joinKey = r.$joinKey")
			->get()
			->result_array();
	}

	/**
	* Soft delete of entry from db by id - change status to deleted
	*/
	function soft_delete($repID = 0)
	{
		//make sure a valid rep id was passed
		if ($repID > 0)
		{
			//set status to deleted
			$this->db->where($this->primary_key, $repID)->set('status', 'deleted')->update($this->_table);
		}

		return false;
	}

	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
	protected function _create_timestamp($data)
	{
		$data['dateCreated'] = $data['dateUpdated'] = date('Y-m-d H:i:s');
		return $data;
	}

	protected function _create_update_timestamp($data)
	{
		$data['dateUpdated'] = date('Y-m-d H:i:s');
		return $data;
	}

	protected function _create_url($data)
	{
		if (isset($data['url']) && $data['url'] != '')
		{
			$data['url'] = strtolower($data['url']);
		}
		return $data;
	}

	protected function _uppercase_names($data)
	{
		if (isset($data['firstName']))
		{
			$data['firstName'] = ucwords($data['firstName']);
			$data['lastName'] = ucwords($data['lastName']);
		}
		return $data;
	}

	protected function _uppercase_mgr_names($data)
	{
		if (isset($data['act_mgr_firstName']))
		{
			$data['act_mgr_firstName'] = ucwords($data['act_mgr_firstName']);
			$data['act_mgr_lastName'] = ucwords($data['act_mgr_lastName']);
		}
		return $data;
	}
}