<?php
/********************************************************************************************************
/
/ Campus Rep Images
/
********************************************************************************************************/
class Rep_image_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'RepImages';

	//primary key
	public $primary_key = 'repImageID';

	//relationships
	public $has_single = array(
		'rep' => array('model' => 'campus_managers/rep_model', 'primary_key' => 'repID', 'join_key' => 'repID')
	);

	//callbacks/observers (use function names)

	//validation
	public $validate = array(
		'filename' => array(
			'field' => 'filename', 
			'label' => 'Filename',
			'rules' => 'required'
		),
		'filepath' => array(
			'field' => 'filepath', 
			'label' => 'Filepath',
			'rules' => 'required'
		),
		'fullpath' => array(
			'field' => 'fullpath', 
			'label' => 'Fullpath',
			'rules' => 'required'
		),
	);

	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Validity checker functions & helper functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
}