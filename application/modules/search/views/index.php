<div class="col-xs-12">

	<form action="" method="get" id="search-form" class="ignore-validation">
		<div class="form-group">
			<input type="text" name="q" class="form-control" value="<?php echo $this->input->post('q', TRUE) ?>">
		</div>

		<div class="form-group">
			<button type="submit" class="btn btn-info pull-right"></button>
		</div>

		<span class= "pull-left"> Search results for "<?php echo $this->input->post('q', TRUE) ?>"... </span> </br>
			<!-- <a href="/search"> <small><span class= "pull-left" style="text-decoration: underline;font-style:italic"> products "18 results"... </span></small></a> </br>
			<a href-"/search"> <small><span class= "pull-left" style="text-decoration: underline;font-style:italic"> designs "12 results"... </span></small></a> -->
	</form>

	<form id="product-show-more" class="show-more-form" data-type="product">
		<input type="hidden" name="type" value="product">
		<input type="hidden" name="p" value="">
	</form>

	<form id="design-show-more" class="show-more-form" data-type="design">
		<input type="hidden" name="type" value="design">
		<input type="hidden" name="p" value="">
	</form>

	<div class="clearfix"></div>

	<div id="products-container" class="search-results-section">
	</div>

	<div id="designs-container" class="search-results-section">
	</div>

	<script id="results-template" type="x-handlebars-template">
	{{#if search}}
		{{#if results}}
			{{#compare page "==" "1"}}
				<h3>{{title}}</h3>
			{{/compare}}

			{{#compare type "==" "design"}}
				{{#everyNth results 4}}
					{{#if isModZeroNotFirst}}
						</div>
					{{/if}}
					{{#if isModZero}}
						<div class="row">
		  			{{/if}}

							<div class="col-sm-3">
								<div class="product">
									<h4>
										<a href="/designs/{{categoryUrl}}/{{url}}">{{title}}</a>
										<i class="arrow-dbl-left"></i>
									</h4>
									{{#if image}}
										<a href="/designs/{{categoryUrl}}/{{url}}">
											<img src="{{image}}" alt="{{title}}" title="{{title}}">
										</a>
									{{else}}
										<a href="/designs/{{categoryUrl}}/{{url}}">
											<img src="/assets/images/sm-pl-hld.jpg" alt="{{title}}" title="{{title}}">
										</a>
									{{/if}}
								</div>
							</div>

					{{#if isLast}}
						</div>
		  			{{/if}}
				{{/everyNth }}
			{{/compare}}

			{{#compare type "==" "product"}}
				{{#everyNth results 4}}
					{{#if isModZeroNotFirst}}
						</div>
					{{/if}}
					{{#if isModZero}}
						<div class="row">
		  			{{/if}}

							<div class="col-sm-3">
								<div class="product">
									<h4>
										<a href="/products/{{parentCategoryUrl}}/{{categoryUrl}}/{{url}}">{{title}}</a>
										<i class="arrow-dbl-left"></i>
									</h4>
									{{#if image}}
										<a href="/products/{{parentCategoryUrl}}/{{categoryUrl}}/{{url}}">
											<img src="{{image}}" alt="{{title}}" title="{{title}}">
										</a>
									{{else}}
										<a href="/products/{{parentCategoryUrl}}/{{categoryUrl}}/{{url}}">
											<img src="/assets/images/sm-pl-hld.jpg" alt="{{title}}" title="{{title}}">
										</a>
									{{/if}}
								</div>
							</div>

					{{#if isLast}}
						</div>
		  			{{/if}}
				{{/everyNth }}
			{{/compare}}

			{{#if showMore}}
				<div class="form-group">
					<button type="button" class="btn btn-info width-100 show-more" data-type="{{type}}">Show More</button>
				</div>
			{{/if}}
		{{else}}
			<p>No matches found in {{title}}.</p>
		{{/if}}
	{{/if}}
	</script>

</div>
