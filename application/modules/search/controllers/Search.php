<?php
class Search extends MY_Controller {

	//layout variables
	public $defaultLayout;

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//set layout variable(s)
		$this->defaultLayout = $this->get_default_layout();
	}

	/**
	* Shows product categories
	*/
	function index()
	{
		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->get_view();
		$data['title'] = 'Search';

		//get page info for SEO
		$data['page'] = modules::run('pages/get', 'search');

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}
}