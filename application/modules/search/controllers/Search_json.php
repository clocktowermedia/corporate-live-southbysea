<?php
class Search_json extends MY_Controller {

	//all actions should call this function
	function __construct()
	{
		parent::__construct();
	}

	function tags()
	{
		try
		{
			//make sure we have a valid request
			if (!$this->is_get())
			{
				throw new Exception('Invalid search.');
			}

			//make sure we have a valid query
			if ($this->input->get('query', TRUE) == '')
			{
				throw new Exception('Must enter a search term.');
			}

			//make sure search term is not too short
			if (strlen($this->input->get('query', TRUE)) < 3)
			{
				throw new Exception('Search term too short.');
			}

			//set defaults for limit/offset
			$limit = ($this->input->get('show', TRUE) == '')? 5 : (int) $this->input->get('show', TRUE);
			$limit = ($limit > 10)? 10 : $limit; //make sure limit is not over 10

			//designate our models
			$models = array(
				'product_tag_model' => 'products/product_tag_model',
				'design_tag_model' => 'designs/design_tag_model'
			);

			//load appropriate model
			$results = array();
			foreach ($models as $modelName => $model)
			{
				$this->load->model($model);
				if (empty($results))
				{
					$results = $this->{$modelName}->search($this->input->get('query', TRUE), $limit);
				} else {
					$this->load->helper('array');
					$exclude = array_pluck($results, 'text');
					$results2 = $this->{$modelName}->search($this->input->get('query', TRUE), $limit, $exclude);
					$results = array_merge($results, $results2);
				}
			}

			//send back data
			$data['results'] = $results;
			$data['status'] = 'ok';
			$data['message'] = ($results == false || empty($results))? 'No results found' : 'Found matches.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}

	/*
	function tags()
	{
		try
		{
			//make sure we have a valid request
			if (!$this->is_get())
			{
				throw new Exception('Invalid search.');
			}

			//make sure we have a valid query
			if (!$this->input->get('query', TRUE))
			{
				throw new Exception('Must enter a search term.');
			}

			//make sure search term is not too short
			if (strlen($this->input->get('query', TRUE)) < 3)
			{
				throw new Exception('Search term too short.');
			}

			//default type
			$_GET['type'] = ($this->input->get('type', TRUE) == '')? 'products' : $this->input->get('type', TRUE);

			//make sure we have a valid search type
			if ($this->input->get('type', TRUE) != 'products' && $this->input->get('type', TRUE) != 'designs')
			{
				throw new Exception('Must search for products or designs');
			}

			//set defaults for limit/offset
			$limit = ($this->input->get('show', TRUE) == '')? 5 : (int) $this->input->get('show', TRUE);
			$limit = ($limit > 10)? 10 : $limit; //make sure limit is not over 10

			//load appropriate model
			switch ($this->input->get('type', TRUE))
			{
				case 'products':
					$this->load->model('products/product_tag_model', 'tag_model');
					break;
				case 'designs':
					$this->load->model('designs/design_tag_model', 'tag_model');
					break;
			}

			//perform search & send data back to view
			$results = $this->tag_model->search($this->input->get('query', TRUE), $limit);

			//send back data
			$data['results'] = $results;
			$data['status'] = 'ok';
			$data['message'] = ($results == false || empty($results))? 'No results found' : 'Found matches.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}
	*/

	function results()
	{
		try
		{

			//make sure we have a valid request
			if (!$this->is_get())
			{
				throw new Exception('Invalid search.');
			}

			//make sure we have a valid query
			if ($this->input->get('q', TRUE) == '')
			{
				throw new Exception('Must enter a search term.');
			}

			//default type
			$_GET['type'] = ($this->input->get('type', TRUE) == '')? 'product' : $this->input->get('type', TRUE);

			//make sure we have a valid search type
			if ($this->input->get('type', TRUE) != 'product' && $this->input->get('type', TRUE) != 'design')
			{
				throw new Exception('Must search for products or designs');
			}

			//set defaults for limit/offset
			$limit = ($this->input->get('show', TRUE) == '')? 12 : (int) $this->input->get('show', TRUE);
			$limit = ($limit > 100)? 100 : $limit; //make sure limit is not over 100
			$offset = ($this->input->get('p', TRUE) == '')? 0 : (int) $this->input->get('p', TRUE) * $limit;

			//load appropriate model
			switch ($this->input->get('type', TRUE))
			{
				case 'product':
					$this->load->model('products/product_model', 'item_model');
					break;
				case 'design':
					$this->load->model('designs/design_model', 'item_model');
					break;
			}

			//perform search & send data back to view
			$results = $this->item_model->search($this->input->get('q', TRUE), $limit, $offset);
			// log_message('error','$results');
			// log_message('error',print_r($results,TRUE));

			//if matches were found, see if there are more
			if (!empty($results) && count($results) > 0)
			{
				$nextOffset = ((int) $this->input->get('p', TRUE) + 1) * $limit;
				$nextPageItem = $this->item_model->search($this->input->get('q', TRUE), 1, $nextOffset);
				if (!empty($nextPageItem) && count($nextPageItem) > 0)
				{
					$data['showMore'] = true;
				}
			}

			//load helper for title
			$this->load->helper('inflector');

			//send data back to view
			$data['search'] = true;
			$data['type'] = $this->input->get('type', TRUE);
			$data['title'] = plural($data['type']);
			$data['page'] = (int) $this->input->get('p', TRUE) + 1;
			$data['results'] = $results;
			$data['status'] = 'ok';
			$data['message'] = ($results == false || empty($results))? 'No results found' : 'Found matches.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		$this->render_json($data);
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/
}
