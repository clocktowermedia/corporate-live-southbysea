<?php
/********************************************************************************************************
/
/ Admin Validation
/
/ Used for admins reseting or setting their account password
/
********************************************************************************************************/
class Admin_validation_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'AdminValidations';

	//primary key
	public $primary_key = 'validationID';

	//relationships
	public $belongs_to = array(
		'admin' => array('model' => 'admins/admin_model', 'primary_key' => 'adminID', 'join_key' => 'adminID')
	);

	//callbacks/observers (use function names)
	public $before_create = array('_create_timestamp', '_create_link', '_set_expiration');

	//validation
	public $validate = array(
		'adminID' => array(
			'field' => 'adminID',
			'label' => 'Admin ID',
			'rules' => 'required|is_natural_no_zero'
		)
	);


	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/

	/**
	* Grabs information for the user reset by user id and code
	*/
	public function getResetLink($adminID = 0, $code = null)
	{
		//make sure a valid admin id and code were passed
		if ($adminID > 0 && !is_null($code))
		{
			//define query
			$query = $this->db->from($this->_table)
				->where('adminID', $adminID)
				->where('code', $code)
				->get();

			//return false if no match was found
			if ($query->num_rows() <= 0)
			{
				return false;
			}

			//return result formatted as an array
			return $query->row_array();
		}

		return false;
	}

	/**
	* Deletes user entry from db by user ID and code
	*/
	public function deleteLink($adminID = 0, $code = null)
	{
		//make sure a valid admin id and code were passed
		if ($adminID > 0 && !is_null($code))
		{
			//delete record from db
			$this->db->from($this->_table)
				->where('adminID', $adminID)
				->where('code', $code)
				->delete();

			return true;
		}

		return false;
	}

	/**********************************************************************************************
	*
	* Validity checker functions
	*
	***********************************************************************************************/

	/**
	* Checks to see if the link is valid, checks expiration date
	*/
	public function _resetLinkIsValid($adminID = 0, $code = null)
	{
		//make sure that a valid admin id and code were passed
		if ($adminID > 0 && !is_null($code))
		{
			//define query
			$query = $this->db->from($this->_table)
				->where('adminID', $adminID)
				->where('code', $code)
				->where('dateExpires >', date('Y-m-d H:i:s'))
				->get();

			//return false if no match was found
			if ($query->num_rows() <= 0)
			{
				return false;
			}

			return true;
		}

		return false;
	}

	/**
	* Checks to see if the link is valid, does not check expiration date
	*/
	public function _linkIsValid($adminID = 0, $code = null)
	{
		//make sure admin id and code are valid
		if ($adminID > 0 && !is_null($code))
		{
			//define query
			$query = $this->db->from($this->_table)
				->where('adminID', $adminID)
				->where('code', $code)
				->get();

			//return false if no match was found
			if ($query->num_rows() <= 0)
			{
				return false;
			}

			return true;
		}

		return false;
	}

	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
	protected function _create_timestamp($data)
	{
		$data['dateCreated'] = date('Y-m-d H:i:s');
		return $data;
	}

	protected function _create_link($data)
	{
		$data['code'] = substr(md5(uniqid()), 0, 8);
		return $data;
	}

	protected function _set_expiration($data)
	{
		if (isset($data['expires']) && $data['expires'] == true)
		{
			$data['dateExpires'] = date('Y-m-d H:i:s', strtotime('+2 hours'));
			unset($data['expires']);
		}

		return $data;
	}
}