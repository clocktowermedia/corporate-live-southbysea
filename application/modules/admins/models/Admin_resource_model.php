<?php
/********************************************************************************************************
/
/ Admin Resources
/
/ Basically lists what pages/urls any admin would be able to view, permissions are in another table
/
********************************************************************************************************/
class Admin_resource_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'AdminResources';

	//primary key
	public $primary_key = 'resourceID';

	//relationships
	public $has_many = array(
		'roles' => array('model' => 'admins/admin_role_resource_model', 'primary_key' => 'resourceID')
	);

	//callbacks/observers (use function names)

	//validation
	public $validate;


	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/

	/**
	 * Gets a dropdown for use in the admin section when creating/editing roles
	 * @param  string $type	[do we want sections or actions?]
	 * @param  string $section [are we limiting this to a single section]
	 * @return array
	 */
	public function getDropdown($type = 'section', $section = '')
	{
		//define query
		$this->db->select('a.resourceID, a.text, a.section, a.url, a.parentID');

		//add type
		$this->db->where('a.type', $type);

		//add section if applicable
		if (!is_null($section) && $section != '')
		{
			$this->db->where('a.section', $section);
		}

		//define query
		$query = $this->db->from($this->_table . ' a')
			->get();

		//return empty array if no matches found
		if ($query->num_rows() <= 0)
		{
			return array();
		}

		//get results as an array
		$results = $query->result_array();

		//loop through results and format into a dropdown friendly array
		foreach ($results as $result)
		{
			if ($type == 'section')
			{
				$optionArray[$result['resourceID']] = ($result['text'] != '')? $result['text'] . ' Section' : ucwords($result['url']) . ' Section';
			} else {
				//get parent
				if ($result['parentID'] > 0)
				{
					$parent = $this->select('text')->get($result['parentID']);
				}
				$result['sectionText'] = (isset($parent) && $parent != false)? $parent['text'] : '';

				$index = ($result['sectionText'] != '')? $result['sectionText'] . ' Section' : ucwords($result['section']) . ' Section';

				//add to types if doesn't exist
				if (!isset($optionArray[$result['parentID']]))
				{
					$optionArray[$result['parentID']] = array(
						'text' => $index,
						'items' => array()
					);
				}

				//add action
				$optionArray[$result['parentID']]['items'][$result['resourceID']] = ($result['text'] != '')? $result['text'] : (($result['url'] != '')? ucwords($result['url']) : 'List All');
			}
		}

		//return array
		return $optionArray;
	}

	/**********************************************************************************************
	*
	* Validity checker functions & helper functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
}