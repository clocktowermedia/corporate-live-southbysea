<?php
/********************************************************************************************************
/
// Admin Accounts
// Explanation: Site admins
/
********************************************************************************************************/
class Admin_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'Admins';

	//primary key
	public $primary_key = 'adminID';

	//relationships
	public $has_many = array(
		'validation_links' => array('model' => 'admins/admin_validation_model', 'primary_key' => 'validationID', 'join_key' => 'adminID')
	);
	public $has_single = array(
		'role' => array('model' => 'admins/admin_role_model', 'primary_key' => 'roleID', 'join_key' => 'adminID')
	);

	//callbacks/observers (use function names)
	public $before_create = array('_create_timestamp', '_create_password');

	//validation
	public $validate = array(
		'firstName' => array(
			'field' => 'firstName',
			'label' => 'First Name',
			'rules' => 'required|min_length[2]'
		),
		'lastName' => array(
			'field' => 'lastName',
			'label' => 'Last Name',
			'rules' => 'required|min_length[2]'
		),
		'email' => array(
			'field' => 'email',
			'label' => 'Email',
			'rules' => 'required|valid_email'
		),
		'roleID' => array(
			'field' => 'roleID',
			'label' => 'Role',
			'rules' => 'integer'
		)
	);


	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/

	/**
	* Update user status to enabled by id
	*/
	public function enable($adminID = 0)
	{
		//make sure the admin id is valid
		if ($adminID > 0)
		{
			//set status of record in db to enabled
			$this->db->where('adminID', $adminID)
				->set('status', 'enabled')
				->update($this->_table);

			return true;
		}

		return false;
	}

	/**
	* Update user status to banned by id
	*/
	public function ban($adminID = 0)
	{
		//make sure the admin id is valid
		if ($adminID > 0)
		{
			//set status of record in db to banned
			$this->db->where('adminID', $adminID)
				->set('status', 'banned')
				->update($this->_table);

			return true;
		}

		return false;
	}

	public function getRoleDropdown($roleName = '', $includePrompt = true)
	{
		if ($roleName != '')
		{
			$this->load->model($this->has_single['role']['model'], 'role_model');
			$role = $this->role_model->get_by('title', $roleName);
			if ($role == false || empty($role))
			{
				return false;
			}

			//grab names
			$results = $this->db->select('roleID, adminID, firstName, lastName')->where('roleID', $role['roleID'])->from($this->_table)->get()->result_array();

			//build dropdown
			$options = array();
			if ($includePrompt == true)
			{
				$options[''] = 'Select a option';
			}

			if (count($results) > 0)
			{
				foreach ($results as $result)
				{
					$options[$result['adminID']] = $result['firstName'] . ' ' . $result['lastName'];
				}
			}

			return $options;
		}

		return false;
	}

	/**
	* Update user password by user id
	*/
	public function resetPassword($adminID = 0, $newPassword = null)
	{
		//make sure the admin id and new password are valid
		if ($adminID > 0 && !is_null($newPassword))
		{
			//create the salt encryption key for the password
			$salt = $this->_createSalt();

			//define query, update record in db
			$this->db->where('adminID', $adminID)
				->set('password', sha1($newPassword.$salt))
				->set('passwordSalt', $salt)
				->set('status', 'enabled')
				->update($this->_table);

			return true;
		}

		return false;
	}

	/**
	* Grabs information for the user by id
	*/
	public function findByID($adminID = 0)
	{
		//make sure admin id is valid
		if ($adminID > 0)
		{
			//define query
			$query = $this->db->from($this->_table)
				->select('adminID')
				->select('firstName')
				->select('lastName')
				->select('email')
				->select('roleID')
				->select('dateCreated')
				->where('adminID', $adminID)
				->get();

			//return false if no match was found
			if ($query->num_rows() <= 0)
			{
				return false;
			}

			//grab matching value and format as an array
			return $query->row_array();
		}

		return false;
	}

	/**
	* Grabs information for the user by email
	*/
	public function findByEmail($email = null, $adminID = 0)
	{
		//make sure a valid email was passed
		if (!is_null($email))
		{
			//define query
			$this->db->from($this->_table)
				->where('email', $email);

			//if admin id was passed, add it query (exlcude it because we are checking that the email is used by someone besides the current admin)
			if ($adminID > 0)
			{
				$this->db->where('adminID !=', $adminID);
			}

			$query = $this->db->get();

			//return false if no match was found
			if ($query->num_rows() <= 0)
			{
				return false;
			}

			//return the match formatted as an array
			return $query->row_array();
		}

		return false;
	}

	/**
	* Authenticates/logs a user into the site
	*/
	public function authenticate($data)
	{
		if (!is_null($data))
		{
			//make sure user exists
			$user = $this->findByEmail($data['email']);
			if ($user == false)
			{
				return false;
			}

			//make sure user is enabled
			if ($user['status'] != 'enabled')
			{
				return false;
			}

			//make sure password matches
			$check = $this->_checkPassword($user['adminID'], $data['password']);
			if ($check == true)
			{
				return $user['adminID'];
			}
			return false;
		}

		return false;
	}

	/**********************************************************************************************
	*
	* Validity checker functions & helper functions, may contain redirects
	*
	***********************************************************************************************/

	/**
	* Checks if password is correct
	*/
	public function _checkPassword($adminID = 0, $password = null)
	{
		//make sure admin id and password are valid
		if ($adminID > 0 && !is_null($password))
		{
			//define query
			$query = $this->db->from($this->_table)
				->where('adminID', $adminID)
				->get();

			//make sure the current password is correct
			if (sha1($password . $query->row('passwordSalt')) == $query->row('password'))
			{
				return true;
			}

			return false;
		}
		return false;
	}

	/**
	* Creates a salt hash to be used in authentication
	*/
	protected function _createSalt()
	{
		$this->load->helper('string');
		return sha1(random_string('alnum', 32));
	}

	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
	protected function _create_timestamp($data)
	{
		$data['dateCreated'] = date('Y-m-d H:i:s');
		return $data;
	}

	protected function _create_password($data)
	{
		//if password was passed, generate salt & encrypt password
		if (isset($data['password']))
		{
			$salt = $this->_createSalt();

			//add password and salt to data
			$data['password'] = sha1($data['password'] . $salt);
			$data['passwordSalt'] = $salt;
		}

		return $data;
	}
}