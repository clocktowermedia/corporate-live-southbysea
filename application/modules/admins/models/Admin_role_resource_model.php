<?php
/********************************************************************************************************
/
/ Admin Role Resources
/
/ Basically permissions for admin roles - specifies which roles can view/visit which pages/urls in admin section
/
********************************************************************************************************/
class Admin_role_resource_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'AdminRoleResources';

	//primary key
	public $primary_key = 'roleResourceID';

	//relationships
	public $belongs_to = array(
		'role' => array('model' => 'admins/admin_role_model', 'primary_key' => 'roleID', 'join_key' => 'roleID'),
		'resource' => array('model' => 'admins/admin_resource_model', 'primary_key' => 'resourceID', 'join_key' => 'resourceID', 'merge' => true)
	);

	//callbacks/observers (use function names)

	//validation
	public $validate = array(
		'roleID' => array(
			'field' => 'roleID',
			'label' => 'Role',
			'rules' => 'required|is_natural_no_zero'
		),
		'resourceID' => array(
			'field' => 'resourceID',
			'label' => 'Resource',
			'rules' => 'required|is_natural_no_zero'
		)
	);


	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/

	/**
	 * Insert several items at once
	 * @param  integer $roleID
	 * @param  array $resources [items to insert]
	 */
	public function add_batch($roleID = 0, $resources = null)
	{
		//make sure proper values were passed
		if ($roleID > 0 && !is_null($resources) && count($resources) > 0)
		{
			//loop through each resource
			foreach ($resources as $resource)
			{
				//insert into db
				$this->db->set('roleID', $roleID)
					->set('resourceID', $resource)
					->insert($this->_table);
			}

			return true;
		}

		return false;
	}

	/**
	 * Get the allowed actions or sections for a specified role
	 * @param  integer $roleID [description]
	 * @param  string  $type   [action or section]
	 * @return array
	 */
	public function getAllowed($roleID = 0, $type = 'section')
	{
		//make sure proper role id passed, if 0 they are allowed to do everything
		if ($roleID > 0)
		{
			//get roles
			$roles = $this->with('resource')
				->add_relation_where('belongs_to', 'resource', 'type', $type)
				->get_many_by('roleID', $roleID);

			//format in a way that is easier to parse
			if ($roles != false && count($roles) > 0)
			{
				foreach ($roles as $role)
				{
					$formattedRoles[$role['resourceID']] = $role;
				}

				return $formattedRoles;
			}

			return $roles;
		}

		return false;
	}

	/**
	 * Get the allowed actions or sections for a specified role, formatted to include section -> url array structure
	 * @param  integer $roleID [description]
	 * @param  string  $type   [action or section]
	 * @return array
	 */
	public function getAllowedArray($roleID = 0, $type = 'section')
	{
		//make sure proper role id passed, if 0 they are allowed to do everything
		if ($roleID > 0)
		{
			//get allowed actions
			$allowed = $this->getAllowed($roleID, $type);
			$formattedArray = $this->_formatAllowedArray($allowed, $type);

			return $formattedArray;
		}

		return false;
	}

	/**
	 * Get all resources and allowed status for specified role, formatted to include section -> url array structure
	 * @param  integer $roleID [description]
	 * @param  string  $type   [action or section]
	 * @return array
	 */
	public function getFullAllowedArray($roleID = 0, $type = 'section')
	{
		//load the model we need
		$this->load->model($this->belongs_to['resource']['model'], 'resource_model');
		$joinKey = (string) $this->resource_model->primary_key;

		//load all resources, determine if it is allowed
		$resources = $this->db->select('r.resourceID, r.text, r.section, r.url')
			->select("CASE WHEN $roleID <= 0 THEN 1 WHEN p.roleID IS NULL THEN 0 ELSE 1 END AS allowed", false)
			->where('r.type', $type)
			->from($this->resource_model->_table . ' AS r')
			->join($this->_table . ' AS p', "p.$joinKey = r.$joinKey AND p.roleID = $roleID", 'left outer')
			->get()
			->result_array();

		$formattedArray = $this->_formatAllowedArray($resources, $type);
		return $formattedArray;
	}

	/**
	 * Format permissions in a way that is easier to parse through
	 * @param  integer $roleID
	 * @param  string  $type   [action or section]
	 * @return array
	 */
	private function _formatAllowedArray($allowed = array(), $type = 'section')
	{
		//set default value
		$allowedArray = array();

		//if they have any allowed actions, create an array for it
		if (count($allowed) > 0)
		{
			foreach ($allowed as $allowed)
			{
				if ($type == 'section')
				{
					$index = $allowed['url'];

					//add section if not added already
					if (!isset($allowedArray[$index]))
					{
						$allowedArray[$index] = (isset($allowed['allowed']))? $allowed['allowed'] : true;
					}

				} else {
					$index = $allowed['section'];
					$action = $allowed['section'] . '/' . $allowed['url'];

					//add section if not added already
					if (!isset($allowedArray[$index]))
					{
						$allowedArray[$index] = array();
					}

					if (!isset($allowedArray[$index][$allowed['url']]))
					{
						$allowedArray[$index][$allowed['url']] = (isset($allowed['allowed']))? $allowed['allowed'] : true;;
					}
				}
			}
		}

		return $allowedArray;
	}

	/**********************************************************************************************
	*
	* Validity checker functions & helper functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
}