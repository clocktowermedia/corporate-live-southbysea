<?php
/********************************************************************************************************
/
/ Admin Roles
/
/ Roles are permission levels for admin, links to other tables to actually get allowed items
/
********************************************************************************************************/
class Admin_role_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'AdminRoles';

	//primary key
	public $primary_key = 'roleID';

	//relationships
	public $has_many = array(
		'resources' => array('model' => 'admins/admin_role_resource_model', 'primary_key' => 'roleID')
	);

	//callbacks/observers (use function names)
	public $before_create = array('_create_timestamp');

	//validation
	public $validate = array(
		'title' => array(
			'field' => 'title',
			'label' => 'Title',
			'rules' => 'required'
		)
	);

	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Validity checker functions & helper functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
	protected function _create_timestamp($data)
	{
		$data['dateCreated'] = $data['dateUpdated'] = date('Y-m-d H:i:s');
		return $data;
	}
}