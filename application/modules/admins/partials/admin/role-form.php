<?php
	if (isset($role))
	{
		$selectedSections = json_decode($role['sections']);
	}

	//create array for classes
	$containerArray = array('labelClass' => 'col-sm-1', 'containerClass' => 'col-sm-11');

	//role title field
	$this->form_builder->text('title', 'Title:', (isset($role['title']))? $role['title'] : '', $containerArray, '', '', '', array('required' => 'required'));

	//add section field
	$this->form_builder->select('sections[]', 'Sections', $sections, (isset($role['sections']))? $selectedSections : '', $containerArray, 'Hold "Ctrl" or "Command" to select multiple items', '', array('required' => 'required', 'multiple' => 'multiple'));

?>

	<div class="form-group">
		<div class="col-sm-offset-1 col-sm-11">

			<?php foreach ($actions as $sectionID => $section):?>
				<?php 
					$sectionSelected = (isset($role['sections']) && in_array($sectionID, $selectedSections) != false)? true : false;
					$sectionAllowed = (isset($role['allowed_sections']) && array_key_exists($sectionID, $role['allowed_sections']))? true : false;
					$sectionClass = ($sectionSelected == true)? '' : ' hide';

					$sectionChecked['label'] = ($sectionAllowed == true)? 'active' : '';
					$sectionChecked['input'] = ($sectionAllowed == true)? 'checked="checked"' : '';
					$sectionChecked['class'] = ($sectionAllowed == true)? '' : 'hide';
					$manageChecked['label'] = ($sectionAllowed == false && $sectionSelected == true)? 'active' : '';
					$manageChecked['input'] = ($sectionAllowed == false && $sectionSelected == true)? 'checked="checked"' : '';
					$manageChecked['class'] = ($sectionAllowed == false && $sectionSelected == true)? '' : 'hide';
				?>
				<div class="box box-solid box-dk-gray box-bordered section-block <?php echo $sectionClass;?>" id="section-<?php echo $sectionID;?>" data-id="">
					<div class="box-header">
						<p class="box-title smaller"><?php echo $section['text'];?></p>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12">
							<div class="btn-group btn-block" data-toggle="buttons">
								<label class="btn btn-default col-sm-6 col-xs-12 no-radius <?php echo $sectionChecked['label'];?>">
									<input class="no-icheck" type="radio" name="permType[<?php echo $sectionID;?>]" value="<?php echo $sectionID;?>" <?php echo $sectionChecked['input'];?> data-validation-ui-enabled="false">
									<i class="fa fa-unlock"></i> Allow All
								</label>
								<label class="btn btn-default col-sm-6 col-xs-12 no-radius <?php echo $manageChecked['label'];?>">
									<input class="no-icheck" type="radio" name="permType[<?php echo $sectionID;?>]" value="" <?php echo $manageChecked['input'];?> data-validation-ui-enabled="false">
									<i class="fa fa-cog"></i> Manage Permissions
								</label>
							</div>
						</div>
					</div>


					<div class="box-body">
						<div class="row section-description">
							<div class="col-sm-12">
								<div class="manage <?php echo $manageChecked['class'];?>">
									<h4><i class="fa fa-cog"></i> Manage Permissions</h4>
									<p>Select which pages and functionality you would like users assigned to this role to be able to access. If new functionality is added to the CMS, this role's permissions will need to be updated accordingly.</p>
								</div>

								<div class="allow <?php echo $sectionChecked['class'];?>">
									<h4><i class="fa fa-unlock"></i> Allow All</h4>
									<p>By selecting this option, users assigned to this role access to all functionality in the section. If new functionality is added to this section, users assigned to this role will automatically have access to that functionality.</p>
								</div>
							</div>
						</div>

						<div class="manage-section-permissions <?php echo $manageChecked['class'];?>">
							<hr>

							<div class="row">
								<div class="col-ms-6 col-xs-12">
									<a class="btn btn-default btn-block" data-checkboxes="check"><i class="fa fa-check-square-o"></i> Check All</a>
								</div>

								<div class="col-ms-6 col-xs-12">
									<a class="btn btn-default btn-block" data-checkboxes="uncheck"><i class="fa fa-square-o"></i> Un-check All</a>
								</div>
							</div>

							<br/>

							<div class="row">
								<?php foreach ($section['items'] as $key => $action):?>
								<?php $actionChecked = (isset($role['allowed_actions']) && array_key_exists($key, $role['allowed_actions']))? 'checked="checked"' : ''; ?>
								<div class="col-md-3 col-sm-3 col-ms-6">
									<div class="checkbox">
									<label class="">
										<input type="checkbox" class="" name="resources[<?php echo $sectionID;?>][]" value="<?php echo $key;?>" <?php echo $actionChecked;?> data-validation-ui-enabled="false">
										<input type="checkbox" class="actions hide" name="actions[]" value="<?php echo $key;?>" <?php echo $actionChecked;?> data-validation-ui-enabled="false">
										<?php echo $action;?>
									</label>
									</div>
								</div>
								<?php endforeach;?>
							</div>
						</div>

					</div>
				</div>
			<?php endforeach;?>

		</div>
	</div>