<?php
	//create array for classes
	$containerArray = array('labelClass' => 'col-sm-1', 'containerClass' => 'col-sm-11');

	//first name field
	$this->form_builder->text('firstName', 'First Name:', (isset($admin['firstName']))? $admin['firstName'] : '', $containerArray, '', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));

	//last name field
	$this->form_builder->text('lastName', 'Last Name:', (isset($admin['lastName']))? $admin['lastName'] : '', $containerArray, '', '', '', array('required' => 'required', 'data-validation-minlength' => '2'));

	//email field
	$this->form_builder->email('email', 'Email:', (isset($admin['email']))? $admin['email'] : '', $containerArray, '', '', '', array('required' => 'required'));

	//role field
	$this->form_builder->select('roleID', 'Role:', array('' => 'Select an option') + $roles, (isset($admin['roleID']))? $admin['roleID'] : '', $containerArray, 'If you do not select a role, user will have access to all pages within the admin area', '', array());
?>