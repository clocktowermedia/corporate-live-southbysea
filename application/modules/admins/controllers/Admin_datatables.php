<?php
class Admin_datatables extends Admin_controller {

	//all actions should call this function
	function __construct()
	{
		parent::__construct();
	}

	/**
	* uses datatables library to get list of admins in a json format for use in admin index
	*/
	function data()
	{
		//load library
		$this->load->library('datatables');

		//grab data from admins table in db
		$output = $this->datatables->select('a.adminID, a.firstName, a.lastName, a.email, r.title, a.status')
			->select('IF (status = "banned", "unban", "ban") AS banType', false)
			->select('IF (status = "banned", "<i class=\"fa fa-undo\"></i> Un-Ban", "<i class=\"fa fa-ban\"></i> Ban") AS banIcon', false)
			->add_column('actions',  '<div class="btn-group">
					<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-user"></i> Admin <span class="caret"></span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li><a href="/admin/admins/edit/$1"><i class="fa fa-edit"></i> Edit</a></li>
						<li><a data-confirm="Are you sure you want to delete this?" href="/admin/admins/delete/$1"><i class="fa fa-trash-o"></i> Delete</a></li>
						<li><a href="/admin/admins/$2/$1">$3</li>
					</ul>
				</div>', 'a.adminID, banType, banIcon')
			->unset_column('a.adminID')
			->unset_column('banType')
			->unset_column('banIcon')
			->from('Admins a')
			->join('AdminRoles r', 'r.roleID = a.roleID', 'left outer')
			->generate();

		echo $output;
	}

	/**
	* uses datatables library to get list of roles in a json format for use in admin index
	*/
	function roles()
	{
		//load library
		$this->load->library('datatables');

		//grab data from admins table in db
		$output = $this->datatables->select('roleID, title')
			->add_column('actions',  '<div class="btn-group">
					<button type="button" class="btn btn-info" href="#"><i class="fa fa-users"></i> Role</button>
					<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" href="#">
						<span class="caret"></span>
						<span class="sr-only">Toggle Dropdown</span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li><a href="/admin/admins/edit-role/$1"><i class="fa fa-edit"></i> Edit</a></li>
						<li><a data-confirm="Are you sure you want to delete this role? Users assigned to this role will need to be assigned to a new role." href="/admin/admins/delete-role/$1"><i class="fa fa-trash-o"></i> Delete</a></li>
					</ul>
				</div>', 'roleID')
			->from('AdminRoles')
			->generate();

		echo $output;
	}


	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

}