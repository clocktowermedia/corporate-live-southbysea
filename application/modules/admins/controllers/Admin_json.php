<?php
class Admin_json extends Admin_controller {

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//load relevant models
		$this->load->model('admins/admin_model');
	}

	function is_dev_admin()
	{
		//get admin id from session
		$adminID = (int) $this->session->userdata('adminID');

		//if the user is logged in, get their information
		if ($adminID > 0)
		{
			//grab admin info
			$admin = $this->admin_model->findByID($adminID);

			//make sure we found a match
			if ($admin != false)
			{
				$data['devAdmin'] = (strpos($admin['email'], 'clocktowermedia') == true)? 'true' : 'false';
				$data['status'] = 'ok';
				$data['message'] = 'Retrieved status.';
			} else {
				$data['devAdmin'] = 'false';
				$data['status'] = 'ok';
				$data['message'] = 'Retrieved status.';
			}

		} else {
			$data['devAdmin'] = 'false';
			$data['status'] = 'ok';
			$data['message'] = 'Retrieved status.';
		}

		$this->render_json($data);
	}

	/**
	 * Returns whether the user is logged in or not
	 * @return boolean [description]
	 */
	function logged_in()
	{
		$data['admin'] = $this->_isLoggedIn();
		$data['status'] = 'ok';
		$data['message'] = '';

		$this->render_json($data);
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/
}