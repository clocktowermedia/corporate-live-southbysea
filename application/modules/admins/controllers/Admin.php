<?php
class Admin extends Admin_controller {

	//layout variables
	public $defaultLayout;
	public $emailLayout;

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//set layout variable(s)
		$this->defaultLayout = $this->get_admin_layout();
		$this->emailLayout = $this->get_email_layout();

		//load relevant models
		$this->load->model('admin_model');
		$this->load->model('admin_role_model');
	}

	/**
	* page that lists all admins
	*/
	function index()
	{
		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Admin Logins';

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}

	/**
	* View and action for creating an admin account
	*/
	function create()
	{
		//load relevant models
		$this->load->model('admin_validation_model');

		//tell it what view file to use
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Create Admin Login';

		//get dropdown
		$data['roles'] = $this->admin_role_model->dropdown('title');

		//load form validation library
		$this->load->library('form_validation');

		//get validation rules from model, set extra options as neccessary
		$validationRules = $this->admin_model->validate;
		$validationRules['email']['rules'] .= '|is_unique[' . $this->admin_model->_table . '.email]';

		//set form validation rules
		$this->form_validation->set_rules($validationRules);

		//if the form was submitted, add to database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the page and show form errors
				$data['admin'] = $this->input->post(NULL, TRUE);
				$this->load->view($this->defaultLayout, $data);
			} else {
				// add record to db
				$adminID = $this->admin_model->insert($this->input->post(NULL, TRUE));

				//redirect if account creation failed
				if ((int)$adminID <= 0)
				{
					$this->session->set_flashdata('error', 'Account creation failed.');
					redirect('/admin/admins/create');
				}

				//get user information, use it to create a validation link
				$admin = $this->admin_model->findByID($adminID);
				$setID = $this->admin_validation_model->insert(array('adminID' => $admin['adminID']));
				$set = $this->admin_validation_model->get($setID);

				//send user a link to set their password
				$this->_send_new_account_email($admin, $set);

				//display success message & redirect
				$this->session->set_flashdata('success', 'Account was successfully created.');
				redirect('/admin/admins');
			}
		} else {
			// tell it what layout to use and load the page
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	* View and action for editing an admin account
	*/
	function edit($adminID = 0)
	{
		//make sure account is found
		$this->_admin_is_valid($adminID);

		//grab admin info
		$data['admin'] = $this->admin_model->findByID($adminID);

		//tell it what view file to use
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Update Admin';

		//get dropdown
		$data['roles'] = array('' => 'Select a Role') + $this->admin_role_model->dropdown('title');

		//load form validation library
		$this->load->library('form_validation');

		//get validation rules from model, set extra options as neccessary
		$validationRules = $this->admin_model->validate;

		//set form validation rules
		$this->form_validation->set_rules($validationRules);

		//if the form was submitted, add to database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the page and show form errors
				$this->load->view($this->defaultLayout, $data);
			} else {
				//make sure email is unique, cannot rely on form validation rules for this because it does not take into account when you don't edit these fields
				$emailUsed = $this->admin_model->findByEmail($this->input->post('email', TRUE), $adminID);

				if ($emailUsed == false)
				{
					//if email was changed, send out an email notifying the user
					if ($this->input->post('email', TRUE) != $data['admin']['email'])
					{
						$this->_send_email_changed_email($data['admin'], $this->input->post(NULL, TRUE));
					}

					//update user information in db
					$this->admin_model->update($adminID, $this->input->post(NULL, TRUE));

					//show success message and redirect
					$this->session->set_flashdata('success', 'Admin information was updated.');
					redirect('/admin/admins');
				} else {
					//show error message and redirect
					$this->session->set_flashdata('error', 'This email is already in use.');
					redirect('/admin/admins/edit/' . $adminID);
				}
			}
		} else {
			// tell it what layout to use and load the page
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	* Action, initiates a password reset email to the specified admin
	*/
	function reset_password($adminID = 0)
	{
		// make sure admin exists
		$this->_admin_is_valid($adminID);

		//grab admin info
		$admin = $this->admin_model->findByID($adminID);

		//if account does exist, add to reset db
		$this->load->model('admin_validation_model');
		$resetID = $this->admin_validation_model->insert(array('adminID' => $admin['adminID']));

		//get reset information from db
		$reset = $this->admin_validation_model->get($resetID);

		if ($reset != false)
		{
			//send reset email
			modules::run('admin_auth/_send_reset_email', $admin, $reset);

			//display success message
			$this->session->set_flashdata('success', 'Admin should recieve an email shortly with instructions on how to reset their password.');
			redirect('/admin/admins');
		}
	}

	/**
	* Action, deletes an admin account
	*/
	function delete($adminID = 0)
	{
		//make sure admin exists
		$this->_admin_is_valid($adminID);

		//do not allow accounts to be deleted if they are assigned as an account manager to something
		$this->load->model('ecommerce/store_model');
		$numStores = $this->store_model->count_by('acct_mgr_id', $adminID);
		if ($numStores > 0)
		{
			$this->session->set_flashdata('error', 'This user is currently assigned as a manager an ecommerce store(s). Please select another manager for those <a href="/admin/ecommerce">stores</a> before deleting this account.');
			redirect('/admin/admins');
		}

		//grab admin info
		$admin = $this->admin_model->findByID($adminID);

		//delete record from db
		$this->admin_model->delete($adminID);

		//redirect upon success
		$this->session->set_flashdata('success', 'Admin login was deleted.');
		redirect('/admin/admins');
	}

	/**
	* action for banning an account
	*/
	public function ban($adminID = 0)
	{
		//make sure admin exists
		$this->_admin_is_valid($adminID);

		//grab user info
		$admin = $this->admin_model->findByID($adminID);

		//set status to banned
		$this->admin_model->ban($adminID);

		//redirect upon success
		$this->session->set_flashdata('success', "User was banned and may no longer log into the site.");
		redirect('/admin/admins/');
	}

	/**
	* action for un-banning an account
	*/
	public function unban($adminID = 0)
	{
		//make sure admin exists
		$this->_admin_is_valid($adminID);

		//grab user info
		$admin = $this->admin_model->findByID($adminID);

		//set status to banned
		$this->admin_model->enable($adminID);

		//redirect upon success
		$this->session->set_flashdata('success', "User was un-banned and may log into the site again.");
		redirect('/admin/admins/');
	}

	/*
	function roles()
	{
		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Admin Roles';

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}

	/**
	* View and action for creating a role
	*/
	/*
	function create_role()
	{
		//tell it what view file to use
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Create Role';

		//get dropdown list(s)
		$this->load->model('admin_resource_model');
		$data['sections'] = $this->admin_resource_model->getDropdown();
		$data['actions'] = $this->admin_resource_model->getDropdown('action');

		//load form validation library
		$this->load->library('form_validation');

		//get validation rules from model, set extra options as neccessary
		$validationRules = $this->admin_role_model->validate;
		$validationRules['title']['rules'] .= '|is_unique[' . $this->admin_role_model->_table . '.title]';

		//set form validation rules
		$this->form_validation->set_rules($validationRules);

		//if the form was submitted, add to database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the page and show form errors
				$data['role'] = $this->input->post(NULL, TRUE);
				$this->load->view($this->defaultLayout, $data);

			} else {

				//load the models we need
				$this->load->model('admin_role_resource_model');

				//unset sections that were selected but have no actions selected and some other stuff
				$_POST = $this->_cleanup_role_post();

				//create the role
				$roleID = $this->admin_role_model->insert(array(
					'title' => $this->input->post('title', TRUE),
					'sections' => json_encode($this->input->post('sections', TRUE))
				));

				//redirect if role creation failed
				if ((int)$roleID <= 0)
				{
					$this->session->set_flashdata('error', 'Role creation failed.');
					redirect('/admin/admins/create-role');
				}

				//create the permissions
				if ($this->input->post('permType', TRUE) != '' && count($this->input->post('permType', TRUE)) > 0)
				{
					$this->admin_role_resource_model->add_batch($roleID, $this->input->post('permType', TRUE)); //full sections
				}

				if ($this->input->post('actions', TRUE) != '' && count($this->input->post('actions', TRUE)) > 0)
				{
					$this->admin_role_resource_model->add_batch($roleID, $this->input->post('actions', TRUE)); //individual actions
				}

				//display success message & redirect
				$this->session->set_flashdata('success', 'Role was successfully created.');
				redirect('/admin/admins/roles');
			}
		} else {
			// tell it what layout to use and load the page
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	* View and action for editing a role
	*/
/*
	function edit_role($roleID = 0)
	{
		//load the models we need
		$this->load->model('admin_resource_model');
		$this->load->model('admin_role_resource_model');

		//make sure role is valid
		//$this->_role_is_valid($roleID);

		//get role information
		$data['role'] = $this->admin_role_model->get($roleID);
		$data['role']['allowed_sections'] = $this->admin_role_resource_model->getAllowed($roleID, 'section');
		$data['role']['allowed_actions'] = $this->admin_role_resource_model->getAllowed($roleID, 'action');

		//get dropdown list(s)
		$data['sections'] = $this->admin_resource_model->getDropdown();
		$data['actions'] = $this->admin_resource_model->getDropdown('action');

		//tell it what view file to use
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Edit ' . $data['role']['title']  . ' Role';

		//load form validation library
		$this->load->library('form_validation');

		//get validation rules from model, set extra options as neccessary
		$validationRules = $this->admin_role_model->validate;

		//set form validation rules
		$this->form_validation->set_rules($validationRules);

		//if the form was submitted, add to database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the page and show form errors
				$this->load->view($this->defaultLayout, $data);

			} else {

				//unset sections that were selected but have no actions selected
				$_POST = $this->_cleanup_role_post();

				//update the role
				$this->admin_role_model->update($roleID, array(
					'title' => $this->input->post('title', TRUE),
					'sections' => json_encode($this->input->post('sections', TRUE))
				));

				//unset old permissions
				$this->admin_role_resource_model->delete_by('roleID', $roleID);

				//create the permissions
				if ($this->input->post('permType', TRUE) != '' && count($this->input->post('permType', TRUE)) > 0)
				{
					$this->admin_role_resource_model->add_batch($roleID, $this->input->post('permType', TRUE)); //full sections
				}
				if ($this->input->post('actions', TRUE) != '' && count($this->input->post('actions', TRUE)) > 0)
				{
					$this->admin_role_resource_model->add_batch($roleID, $this->input->post('actions', TRUE)); //individual actions
				}

				//display success message & redirect
				$this->session->set_flashdata('success', 'Role was successfully updated.');
				redirect('/admin/admins/roles');
			}
		} else {
			// tell it what layout to use and load the page
			$this->load->view($this->defaultLayout, $data);
		}
	}

	public function delete_role($roleID = 0)
	{
		//make sure role is valid
		//$this->_role_is_valid($roleID);

		//load models we need
		$this->load->model('admin_role_resource_model');

		//delete all entries
		$this->admin_role_model->delete_by('roleID', $roleID);
		$this->admin_role_resource_model->delete_by('roleID', $roleID);

		//reset user role
		$this->admin_model->update_by('roleID', $roleID, array('roleID' => 0));

		//redirect upon success
		$this->session->set_flashdata('success', 'Role was deleted. All users previously assigned to that role will need to be assigned to a new role.');
		redirect('/admin/admins/roles');
	}
	*/

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

	private function _cleanup_role_post()
	{
		foreach ($this->input->post('sections', TRUE) as $key => $section)
		{
			//do not select the section if there's no actions for it
			if (!isset($_POST['permType'][$section]) || ($_POST['permType'][$section] == '' && isset($_POST['resources']) && count($_POST['resources'][$section]) <= 0))
			{
				unset($_POST['sections'][$key]);
			}

			//we don't need to post empty items
			if (isset($_POST['permType'][$section]) && $_POST['permType'][$section] == '')
			{
				unset($_POST['permType'][$section]);
			}
		}

		return $_POST;
	}

	/**
	* Checks to see that the admin account exists, redirects if it does not
	*/
	public function _admin_is_valid($adminID = 0)
	{
		//set type to int
		$adminID = (int) $adminID;

		//make sure admin id is valid
		if ($adminID > 0)
		{
			//make sure the account exists
			$exists = $this->admin_model->get($adminID);

			if ($exists == false)
			{
				//show error message and redirect
				$this->session->set_flashdata('error', 'Invalid admin account ID.');
				redirect('/admin/admins');
			}
		} else {
			//show error message and redirect
			$this->session->set_flashdata('error', 'Invalid admin account ID.');
			redirect('/admin/admins');
		}
	}

	/**
	* Sends an email to the admin to set their password when their account is first created
	*/
	public function _send_new_account_email($admin = null, $set = null)
	{
		//load email & parser library
		$this->load->library('email');
		$this->load->library('parser');
		$this->config->load('email');

		//get sending options from config
		$this->email->from($this->config->item('site_email'), $this->config->item('site_email_name'));

		//send to admin
		$this->email->to($admin['email'], $admin['firstName'] . ' ' . $admin['lastName']);

		//email content
		$data['subject'] = $this->config->item('site_name') . ' Admin Account Created';

		//build content
		$url = 'http://' . $_SERVER['HTTP_HOST'] . '/admin-auth/set-password/' . $set['adminID'] . '/' . $set['code'];
		$text = '<p>Visit the URL below in your browser to set your password:</p>';
		$text .= '<p>Link: <a href="'. $url .'">'. $url . '</a></p>';
		$data['text'] = $text;

		//format content into table
		$data['content'] = $this->convert_text_to_html_email_table($data['subject'], $data['text']);

		$this->load->library('css_to_inline_styles');
		$this->css_to_inline_styles->setHtml($data['content']);
		$this->css_to_inline_styles->setCSS(file_get_contents(APPPATH . '../assets/css/email.css'));
		$data['html'] = $this->css_to_inline_styles->convert();

		//tell it what view to use
		$data['view'] = $this->get_email_view();

		//get content into layout/view
		$content = $this->parser->parse($this->emailLayout, $data, true);

		//set subject & message
		$this->email->subject($data['subject']);
		$this->email->message($content);

		//send email
		$this->email->sbs_send();
	}

	/**
	* Sends an email to the admin notifying them if their account password was changed
	*/
	public function _send_email_changed_email($admin = null, $input = null)
	{
		//load email & parser library
		$this->load->library('email');
		$this->load->library('parser');
		$this->config->load('email');

		//get sending options from config
		$this->email->from($this->config->item('site_email'), $this->config->item('site_email_name'));

		//send to admin
		$this->email->to($admin['email'], $admin['firstName'] . ' ' . $admin['lastName']);

		//email content
		$data['subject'] = $this->config->item('site_name') . ' Admin Account Email Changed';

		//build content
		$text = '<p>This is a notification that the email you use for logging into the ' . $this->config->item('site_name') . ' admin area has been changed to ' . $input['email']  . '. You will no longer be able to log in with this current email address (' . $admin['email'] . ').</p><p>If you did not change your email address or request for your email address to be changed, please log in and review your account.</p>';

		//format content into table
		$data['content'] = $this->convert_text_to_html_email_table($data['subject'], $text);

		$this->load->library('css_to_inline_styles');
		$this->css_to_inline_styles->setHtml($data['content']);
		$this->css_to_inline_styles->setCSS(file_get_contents(APPPATH . '../assets/css/email.css'));
		$data['html'] = $this->css_to_inline_styles->convert();

		//tell it what view to use
		$data['view'] = $this->get_email_view();

		//get content into layout/view
		$content = $this->parser->parse($this->emailLayout, $data, true);

		//set subject and message
		$this->email->subject($data['subject']);
		$this->email->message($content);

		//send email
		$this->email->sbs_send();
	}
}