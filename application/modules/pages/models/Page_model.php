<?php
/********************************************************************************************************
/
/ Pages
/
/ CMS Pages
/
********************************************************************************************************/
class Page_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'Pages';
	private $archiveTable = 'PageArchives';

	//primary key
	public $primary_key = 'pageID';

	//validation
	public $validate = array(
		'title' => array(
			'field' => 'title',
			'label' => 'Title',
			'rules' => 'required'
		),
		'url' => array(
			'field' => 'url',
			'label' => 'Url',
			'rules' => 'required|seo_url'
		),
		'urlPath' => array(
			'field' => 'urlPath',
			'label' => 'Full Url Path',
			'rules' => 'required'
		),
		'parentID' => array(
			'field' => 'parentID',
			'label' => 'Parent Page',
			'rules' => 'integer'
		)
	);

	//relationships
	public $has_single = array(
		'page_image' => array('model' => 'pages/page_image_model', 'primary_key' => 'imageID', 'join_key' => 'pageID')
	);

	public $has_many = array(
		'page_images' => array('model' => 'pages/page_image_model', 'primary_key' => 'imageID', 'join_key' => 'pageID'),
		'page_archives' => array('model' => 'pages/page_archive_model', 'primary_key' => 'archiveID', 'join_key' => 'pageID')
	);

	//callbacks/observers (use function names)
	public $before_create = array('_create_timestamp');
	public $before_update = array('_update_page');


	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/

	/**
	* Update page information by id
	*/
	public function save($pageID = 0, $data = null)
	{
		//make sure page id is valid and that there is data from post
		if ($pageID > 0 && !is_null($data))
		{
			//save the text into the draft field
			$this->db->where('pageID', $pageID)
				->set('contentDraft', $data['content'])
				->update($this->_table);

			return true;
		}

		return false;
	}

	/**
	* Publishes the draft content
	*/
	public function publish($pageID = 0)
	{
		//make sure page id is valid
		if ($pageID > 0)
		{
			//grab page info
			$page = $this->get($pageID);

			//return false is page not found or if there is no draft content
			if ($page == false || $page['draftContent'] == '')
			{
				return false;
			}

			//update page
			$this->db->where('pageID', $pageID)
				->set('content', $page['contentDraft'])
				->set('contentDraft', '')
				->update($this->_table);

			return true;
		}

		return false;
	}

	/**
	* Soft delete of entry from db by id - change status to deleted
	*/
	public function soft_delete($pageID = 0)
	{
		//make sure page id is valid
		if ($pageID > 0)
		{
			//set status of page to deleted
			$this->db->from($this->_table)
				->where('pageID', $pageID)
				->set('status', 'deleted')
				->update();

			return true;
		}

		return false;
	}

	/**
	* Changes status of page to enabled
	*/
	public function enable($pageID = 0)
	{
		//make sure page id is valid
		if ($pageID > 0)
		{
			//update db entry
			$this->db->where('pageID', $pageID)
				->set('status', 'enabled')
				->update($this->_table);

			return true;
		}

		return false;
	}

	/**
	* Restores the page to the archived version selected
	*/
	public function restore($archiveID = 0)
	{
		//make sure the archive id is valid
		if ($archiveID > 0)
		{
			//grab the archive information
			$this->load->model('page_archive_model');
			$archive = $this->page_archive_model->get($archiveID);

			//return false if no archive match found
			if ($archive == false)
			{
				return false;
			}

			//unset this variable because it does not exist int he pages table
			unset($archive['archiveID']);

			//update page to match archived version
			$this->update($archive['pageID'], $archive);
		}

		return false;
	}

	/**
	 * Get the parents of a page
	 * @param  integer $pageID
	 * @return array
	 */
	public function getParents($pageID = 0)
	{
		if ($pageID > 0)
		{
			//get page info
			$page = $this->get($pageID);

			//if has parent..
			if ($page['parentID'] > 0)
			{
				//append parent
				$parent = $this->get($page['parentID']);
				$parents[] = $parent;

				//get parents of parent
				if ($parent['parentID'] > 0)
				{
					$parents[] = $this->getParents($parent['parentID']);
				}

				return array_reverse($parents);
			}
		}
	}

	/**
	 * Only grab information relevant for the sitemap xml
	 * @return array
	 */
	public function getSitemapXmlInfo()
	{
		return $this->db->select('urlPath AS link', false)
			->select('dateUpdated')
			->from($this->_table)
			->where('status', 'enabled')
			->get()
			->result_array();
	}

	/**
	* Counts all pages that are not approved
	*/
	public function countUnpublished()
	{
		return $this->db->from($this->_table)
			->where('contentDraft !=', '')
			->where('status !=', 'deleted')
			->count_all_results();
	}

	/**
	* Grabs all pages for use in a dropdown selector
	*/
	public function getDropdown($maxDepth = 0, $parentID = 0, $options = array(), $exclude = array())
	{
		//exclude the homepage as well
		$homepage = $this->get_by('url', '');
		if ($homepage != false) { array_push($exclude, $homepage['pageID']); }

		//get pages
		$pages = $this->getHierarchicalPageList($parentID, $exclude);

		//add an initial option
		if (!isset($options['']))
		{
			$options[''] = '--- Select a Parent Page ---';
		}

		//loop through pages, add to dropdown, do depth checking and get children
		if ($pages != false && count($pages) > 0)
		{
			foreach ($pages as $page)
			{
				//add to dropdown
				$options[$page['pageID']] = str_repeat('&nbsp;', ($page['depth'] * 3)) . $page['title'];

				//check to make sure we aren't at max depth
				if ($maxDepth == 0 || $maxDepth > $page['depth'])
				{
					//grab children
					$newOptions = $this->getDropdown($maxDepth, $page['pageID'], $options, $exclude);

					//append
					$options = $options + $newOptions;
				}
			}
		}

		return $options;
	}

	/**
	 * Get page list
	 */
	public function getHierarchicalPageList($parentID = 0, $exclude = array())
	{
		//get pages
		$this->db->from($this->_table)
			->where('status', 'enabled');

		//get by parent id if applicable
		if ($parentID > 0)
		{
			$this->db->where('parentID', $parentID);
		} else {
			$this->db->where('parentID', 0);
		}

		//exclude if applicable
		if (is_array($exclude) && count($exclude) > 0)
		{
			$this->db->where_not_in('pageID', $exclude);

		} else if (!is_array($exclude) && !is_null($exclude) && $exclude != '') {

			$this->db->where('pageID !=', $exclude);
		}

		//get pages as an array
		$pages = $this->db->order_by('displayOrder asc')
			->get()
			->result_array();

		//return the pages
		return $pages;
	}

	/**********************************************************************************************
	*
	* Menu Integration
	*
	***********************************************************************************************/

	/**
	 * Get menu array
	 */
	public function getMenuArray($menuID = 0, $maxDepth = 0, $parentID = 0)
	{
		//grab the list of unavailable pages that are already in the menu
		$this->load->model('menus/menu_model');
		$unavailable = $this->menu_model->getUnavailablePageList($menuID);

		$menuArray = $this->getArray($maxDepth, $parentID, $unavailable);
		return $menuArray;
	}

	/**
	* Grabs all pages for use in a dropdown selector
	*/
	public function getArray($maxDepth = 0, $parentID = 0, $exclude = array())
	{
		//get all pages
		$pages = $this->getHierarchicalPageList($parentID);

		//loop through pages, add to array, do depth checking and get children
		if ($pages != false && count($pages) > 0)
		{
			foreach ($pages as $page)
			{
				//determine if available or not
				$index = array_search($page['pageID'], (array) $exclude);
				$page['available'] = ($index === false)? true : false;
				$page['menuItemID'] = $index;

				//add to array
				$pageArray[] = $page;

				//check to make sure we aren't at max depth
				if ($maxDepth == 0 || $maxDepth > $page['depth'])
				{
					//grab children
					$children = $this->getArray($maxDepth, $page['pageID'], $exclude);

					//append children if applicable
					$itemKey = end((array_keys($pageArray)));
					$pageArray[$itemKey]['children'] = $children;
				}
			}
		} else {
			$pageArray = array();
		}

		return $pageArray;
	}

	/**
	 * Get html to use in the admin section
	 * @param  integer $maxDepth [description]
	 * @param  integer $parentID [description]
	 * @param  array   $exclude  [description]
	 * @return [type]            [description]
	 */
	public function getAdminHtml($maxDepth = 0, $parentID = 0, $exclude = array(), $id = null, $class = null)
	{
		//set html as empty
		$html = '';

		//check for id and class, add them if they exist
		$selectors = '';
		if (!is_null($id) && $id != '')
		{
			$selectors .= ' id="' . $id . '"';
		}

		if (!is_null($class) && $class != '')
		{
			$selectors .= ' class="' . $class . '"';
		}

		//exclude the homepage as well
		$homepage = $this->get_by('url', '');
		if ($homepage != false) { array_push($exclude, $homepage['pageID']); }

		//add homepage html
		if ($parentID <= 0)
		{
			$html .= '<ul' . $selectors . '>
					<li id="page_' . $homepage['pageID'] . '">
						<div class="sortable-container">
							<div class="reorder-title pull-left">
								<i class="fa fa-arrows"></i>
								<a href="/admin/pages/edit/' . $homepage['pageID'] . '">' .
									$homepage['title'] .
								'</a>
							</div>' .
							'<div class="pull-right menu-item-actions">
								<a href="/admin/pages/edit/' . $homepage['pageID'] . '" title="Edit Page"><i class="fa fa-edit"></i></a>
								<a href="/admin/pages/previewpage/' . $homepage['pageID'] . '" title="Preview Page"><i class="fa fa-eye"></i></a>
								<a href="/admin/pages/restore/' . $homepage['pageID'] . '" title="Rollback Page"><i class="fa fa-undo"></i></a>
							</div>
						</div>
					</li>
				</ul>';
		}

		//get all pages
		$pages = $this->getHierarchicalPageList($parentID, $exclude);

		//start the html
		$html .= '<ul' . $selectors . '>';

		//loop through pages, add to array, do depth checking and get children
		if ($pages != false && count($pages) > 0)
		{
			foreach ($pages as $page)
			{
				//define the html we want to use
				$protected = (isset($page['protected']) && $page['protected'] == true)? 'protected' : '';
				$itemLi = '<li id="page_' . $page['pageID'] . '" class="' . $protected . '">
							<div class="sortable-container ' . $protected . '">
								<div class="reorder-title pull-left">
									<i class="fa fa-arrows"></i>
									<a href="/admin/pages/edit/' . $page['pageID'] . '">' .
										$page['title'] .
									'</a>
								</div>' .
								'<div class="pull-right menu-item-actions">
									<a href="/admin/pages/edit/' . $page['pageID'] . '" title="Edit Page"><i class="fa fa-edit"></i></a>
									<a href="/admin/pages/previewpage/' . $page['pageID'] . '" title="Preview Page"><i class="fa fa-eye"></i></a>
									<a href="/admin/pages/restore/' . $page['pageID'] . '" title="Rollback Page"><i class="fa fa-undo"></i></a>';
									if (!isset($page['protected']) || $page['protected'] == false)
									{
										$itemLi .= '<a href="/admin/pages/delete/' . $page['pageID'] . '" title="Delete Page" data-confirm="Are you sure you want to delete this?"><i class="fa fa-trash-o"></i></a>';
									}
								$itemLi .= '</div>
							</div>';

				//add to html
				$html .= $itemLi;

				//check to make sure we aren't at max depth
				if ($maxDepth == 0 || $maxDepth > $page['depth'])
				{
					$html .= $this->getAdminHtml($maxDepth, $page['pageID'], $exclude);
				}

				$html .= '</li>';
			}
		}

		//end the list
		$html .= '</ul>';

		//reutrn the html
		return $html;
	}

	private function menuHtml($pages = null)
	{
		//start the html
		$html = '<ul' . $selectors . '>';

		//loop through pages, add to array, do depth checking and get children
		if ($pages != false && count($pages) > 0)
		{
			foreach ($pages as $page)
			{
				//define the html we want to use
				$itemLi = '<li id="page_' . $page['pageID'] . '">
							<div class="sortable-container">
								<div class="reorder-title pull-left">
									<i class="fa fa-arrows"></i>
									<a href="/admin/pages/edit/' . $page['pageID'] . '">' .
										$page['title'] .
									'</a>
								</div>' .
								'<div class="pull-right menu-item-actions">
									<a href="/admin/pages/edit/' . $page['pageID'] . '" title="Edit Page"><i class="fa fa-edit"></i></a>
									<a href="/admin/pages/previewpage/' . $page['pageID'] . '" title="Preview Page"><i class="fa fa-eye"></i></a>
									<a href="/admin/pages/restore/' . $page['pageID'] . '" title="Rollback Page"><i class="fa fa-undo"></i></a>
									<a href="/admin/pages/delete/' . $page['pageID'] . '" title="Delete Page" data-confirm="Are you sure you want to delete this?"><i class="fa fa-trash-o"></i></a>
								</div>
							</div>';

				//add to html
				$html .= $itemLi;

				//check to make sure we aren't at max depth
				if ($maxDepth == 0 || $maxDepth > $page['depth'])
				{
					$html .= $this->getAdminHtml($maxDepth, $page['pageID'], $exclude);
				}

				$html .= '</li>';
			}
		}

		//end the list
		$html .= '</ul>';

		//reutrn the html
		return $html;
	}

	/**
	* Used to change the order in which pages are displayed
	*/
	public function reorder($pages = null)
	{
		//make sure there are pages to loop through
		if (!is_null($pages))
		{
			//loop through each gallery and update the order in the db
			$count = 0;
			foreach ($pages as $key => $value)
			{
				//extract the values that we need
				list($parentID, $depth) = explode(',', $value);

				//increase count
				$count++;

				//depending on the depth, the url would have changed
				$urlSuffix = $this->get($key);
				if ($depth > 1)
				{
					$urlPrefix = $this->get($parentID);
					$urlPath = $urlPrefix['urlPath'] . '/' . $urlSuffix['url'];
				} else {
					$urlPath = '/' . $urlSuffix['url'];
				}

				//call db to update
				$this->db->where('pageID', $key)
					->set('displayOrder', $count)
					->set('depth', $depth)
					->set('parentID', $parentID)
					->set('urlPath', $urlPath)
					->update($this->_table);
			}

			return true;
		}

		return false;
	}

	/**
	* Grabs all pages that are currently available (ie: not already in) the menu
	*/
	public function getAvailablePageList($menuID = 0)
	{
		//grab the list of unavailable pages that are already in the menu
		$this->load->model('menus/menu_model');
		$unavailable = $this->menu_model->getUnavailablePageList($menuID);

		//grab all pages that are enabled and not already in use
		$query = $this->db->from($this->_table)
			->where_not_in('pageID', $unavailable)
			->where('status', 'enabled')
			->get();

		//return false if no matches found
		if ($query->num_rows() <= 0)
		{
			return false;
		}

		//return results formatted as an array
		return $query->result_array();
	}

	/**********************************************************************************************
	*
	* Validity checker functions & helper functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
	protected function _create_timestamp($data)
	{
		$data['dateCreated'] = $data['dateUpdated'] = date('Y-m-d H:i:s');
		return $data;
	}

	protected function _update_page($data)
	{
		$data['contentDraft'] = '';
		$data['datePublished'] = date('Y-m-d H:i:s');
		return $data;
	}
}