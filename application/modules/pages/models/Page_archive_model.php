<?php
/********************************************************************************************************
/
/ Pages
/
/ CMS Page Archives
/
********************************************************************************************************/
class Page_archive_model extends MY_Model {

	//set return type as array
	protected $return_type = 'array';

	//table names
	public $_table = 'PageArchives';

	//primary key
	public $primary_key = 'archiveID';

	//relationships
	public $belongs_to = array(
		'page' => array('model' => 'pages/page_model', 'primary_key' => 'pageID', 'join_key' => 'pageID')
	);

	//callbacks/observers (use function names)
	public $before_create = array('_create_timestamp');

	//validation
	public $validate = array(
		'pageID' => array(
			'field' => 'pageID', 
			'label' => 'Page ID',
			'rules' => 'required|is_natural_no_zero'
		),
		'title' => array(
			'field' => 'title', 
			'label' => 'Title',
			'rules' => 'required'
		),
		'url' => array(
			'field' => 'url', 
			'label' => 'Url',
			'rules' => 'required|seo_url'
		),
		'urlPath' => array(
			'field' => 'urlPath', 
			'label' => 'Full Url Path',
			'rules' => 'required'
		),
		'parentID' => array(
			'field' => 'parentID', 
			'label' => 'Parent Page',
			'rules' => 'integer'
		)
	);


	/**********************************************************************************************
	*
	* General Use Functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Validity checker functions & helper functions
	*
	***********************************************************************************************/


	/**********************************************************************************************
	*
	* Callbacks/Observers
	*
	***********************************************************************************************/
	protected function _create_timestamp($data)
	{
		$data['dateCreated'] = $data['dateUpdated'] = date('Y-m-d H:i:s');
		return $data;
	}
}