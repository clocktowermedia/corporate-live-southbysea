<?php
class Pages extends MY_Controller {

	//layout variables
	public $defaultLayout;

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//set layout variable(s)
		$this->defaultLayout = $this->get_default_layout();

		//load relevant models
		$this->load->model('page_model');
	}

	/**
	* Used for viewing the page on the front end
	*/
	function view($url = null)
	{
    		$numbers = range(1, 4);
    		shuffle($numbers);
    		$data['numbers'] = $numbers;

		$url = ($url == '' || is_null($url))? $this->uri->uri_string() : $url;



		//replace the underscores with hypens (to undo the previous replacements done to find matching controllers)
		$url = str_replace('_', '-', $url);
		$url = str_replace('fraternity/', '', $url);

		// var_dump($url);
		// die();

		//grab page info for view
		$data['page'] = $this->page_model->append('page_image')->get_by(array(
			'urlPath' => '/' . $url,
			'status' => 'enabled'
		));

		//set page id
		$pageID = ($data['page'] == false)? 0 : $data['page']['pageID'];

		//make sure page is valid
		$this->_page_is_valid($pageID);

		//get parents
		$data['page']['parents'] = $this->page_model->getParents($pageID);

		//check to see if there is a special view for this page...
		if (file_exists(APPPATH . 'modules/pages/views/' . $url . '.php')) {

			$data['view'] = $url;

		} else if (file_exists(APPPATH . 'modules/pages/views/' . basename($url) . '.php')) {

			$data['view'] = basename($url);

		} else {

			//tell it what view to use, this is always called the view variable
			$data['view'] = 'view';
		}

		//determine what layout to use
		if ($this->input->get('modal', TRUE) != '')
		{
			$layout = $data['view'];
		} else {
			$layout = $this->defaultLayout;
		}


		// tell it what layout to use and load
		$this->load->view($layout, $data);
	}

	/**
	* Action for grabbing the page, used for modules/module pages that grab content from a cms page
	*/
	function get($url = null, $includeImage = false, $redirect = true)
	{


		//grab page info for view
		$page = ($includeImage == true)? $this->page_model->append('page_image')->get_by(array('status' => 'enabled', 'urlPath' => '/' . $url)) : $this->page_model->get_by('urlPath', '/' . $url);

		//if not found, try full path instead
		if (empty($page) || $page == false)
		{
			$page = ($includeImage == true)? $this->page_model->append('page_image')->get_by(array('status' => 'enabled', 'url' => $url)) : $this->page_model->get_by('url', $url);
		}

		//set page id
		$pageID = ($page == false)? 0 : $page['pageID'];

		//make sure page is valid
		if ($redirect == true)
		{
			$this->_page_is_valid($pageID);
		}

		//get parents
		$page['parents'] = $this->page_model->getParents($pageID);

		//return page if valid
		return $page;
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

	/**
	* Checks to see that page ID is valid and that the page exists/is enabled
	*/
	public function _page_is_valid($pageID = 0)
	{
		//make sure page id is an integer
		$pageID = (int) $pageID;

		//make sure event id is greater than zero
		if ($pageID > 0)
		{
			//check to see if the event exists
			$exists = $this->page_model->get_by(array(
				'pageID' => $pageID,
				'status' => 'enabled'
			));

			if ($exists == false)
			{
				show_404();
			}

		} else {
			show_404();
		}
	}
}
