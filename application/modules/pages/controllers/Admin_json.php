<?php
class Admin_json extends Admin_controller {

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//load relevant models
		$this->load->model('pages/page_model');
	}

	/**
	* Sends back the url path of a specified page
	*/
	public function generate_url_path($parentID = 0)
	{
		try
		{
			//get the parent page
			$parentID = (int) $parentID;
			$page = $this->page_model->get($parentID);

			//throw exception if page not found
			if ($page == false)
			{
				throw new Exception('Parent page not found.');
			}

			//send data to view
			$data['urlPath'] = $page['urlPath'];
			$data['status'] = 'ok';
			$data['message'] = 'Successfully retreived page list.';

		} catch (Exception $e) {
			$data['urlPath'] = '';
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		// render data
		$this->render_json($data);
	}

	/**
	 * Reorders the pages based upon user drag/drop reordering
	 * @return [type] [description]
	 */
	public function reorder()
	{
		try
		{
			//make sure we variable we need from post
			if ($this->input->post('page', TRUE) == '')
			{
				throw new Exception('Must reorder pages.');
			}

			//try to reorder the pages
			$this->page_model->reorder($this->input->post('page', TRUE));

			//set status message
			$data['status'] = 'ok';
			$data['message'] = 'Successfully saved page order.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		// render data
		$this->render_json($data);
	}

	/**
	 * Gets images for a page
	 * @param  integer $pageID [description]
	 * @return [type]          [description]
	 */
	public function images($pageID = 0)
	{
		try
		{
			//get the page
			$pageID = (int) $pageID;
			$page = $this->page_model->get($pageID);

			//throw exception if page not found
			if ($page == false)
			{
				throw new Exception('Page not found.');
			}

			//get images
			$this->load->model('pages/page_image_model');
			$images = $this->page_image_model->order_by('displayOrder', 'ASC')
				->get_many_by('pageID', $pageID);

			//send data to view
			$data['images'] = $images;
			$data['status'] = 'ok';
			$data['message'] = 'Successfully retreived images.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		// render data
		$this->render_json($data);
	}

	/**
	 * Reorders the images based upon user drag/drop reordering
	 * @return [type] [description]
	 */
	public function reorder_images($pageID = 0)
	{
		try
		{
			//get the parent page
			$pageID = (int) $pageID;
			$page = $this->page_model->get($pageID);

			//throw exception if page not found
			if ($page == false)
			{
				throw new Exception('Page not found.');
			}

			//make sure we variable we need from post
			if ($this->input->post('images', TRUE) == '')
			{
				throw new Exception('Must reorder images.');
			}

			//try to reorder the pages
			$this->load->model('pages/page_image_model');
			$this->page_image_model->reorder($this->input->post('images', TRUE));

			//set status message
			$data['status'] = 'ok';
			$data['message'] = 'Successfully saved image order.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		// render data
		$this->render_json($data);
	}

	function upload($pageID = 0)
	{
		try
		{
			//get the parent page
			$pageID = (int) $pageID;
			$page = $this->page_model->get($pageID);

			//throw exception if page not found
			if ($page == false)
			{
				throw new Exception('Page not found.');
			}

			//try to upload the image
			$imageID = $this->_upload_image();;

			//see if it worked
			if (!is_int($imageID) || $imageID == false || $imageID <= 0)
			{
				throw new Exception($imageID);
			}

			//set status message
			$data['imageID'] = $imageID;
			$data['status'] = 'ok';
			$data['message'] = 'Successfully uploaded image.';

		} catch (Exception $e) {
			$data['status'] = 'fail';
			$data['message'] = $e->getMessage();
		}

		// render data
		$this->render_json($data);
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

	//call the proper module
	function _upload_image()
	{
		//set file ID to zero
		$imageID = 0;

		//try to upload
		$imageID = modules::run('pages/admin/_upload_image', $this->input->post(NULL, TRUE));

		//return image ID
		return $imageID;
	}
}