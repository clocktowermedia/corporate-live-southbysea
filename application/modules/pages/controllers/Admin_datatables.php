<?php
class Admin_datatables extends Admin_controller {

	//all actions should call this function
	function __construct()
	{
		parent::__construct();
	}

	/**
	* uses datatables library to get list of pages in a json format for use in admin index
	*/
	function data()
	{
		//load library
		$this->load->library('datatables');

		//grab data from admins table in db
		$output = $this->datatables->select('pageID, title, url, urlPath, dateUpdated')
			->select('CASE WHEN contentDraft != "" THEN "Unpublished" ELSE "Published" END as published', false)
			->select('status')
			->select('CASE WHEN protected = "0" THEN CONCAT("<li><a data-confirm=\"Are you sure you want to delete this?\" href=\"/admin/pages/delete/", pageID, "\"><i class=\"fa fa-trash-o\"></i> Delete</a></li>") ELSE "" END AS deleteButton', false)
			->add_column('actions',  '<div class="btn-group">
					<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-file"></i> Page <span class="caret"></span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li><a href="/admin/pages/edit/$1"><i class="fa fa-edit"></i> Edit</a></li>
						<li><a href="/admin/pages/previewpage/$1"><i class="fa fa-eye"></i> Preview</a></li>
						<li><a href="/admin/pages/restore/$1"><i class="fa fa-undo"></i> Rollback</a></li>
						$2
					</ul>
				</div>', 'pageID, deleteButton')
			->unset_column('pageID')
			->unset_column('deleteButton')
			->from('Pages')
			->where('status !=', 'deleted')
			->generate();

		echo $output;
	}

	/**
	* uses datatables library to get list of page archives in a json format for use in admin index
	*/
	function archived($pageID = 0)
	{
		//load library
		$this->load->library('datatables');

		//grab data from admins table in db
		$output = $this->datatables->select('archiveID, title, url, urlPath, dateCreated')
			->select('CASE WHEN contentDraft != "" THEN "Unpublished" ELSE "Published" END as published', false)
			->add_column('actions',  '<div class="btn-group">
					<button type="button" class="btn btn-info" href="#"><i class="fa fa-file"></i> Archive</button>
					<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" href="#">
						<span class="caret"></span>
						<span class="sr-only">Toggle Dropdown</span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li><a href="/admin/pages/restore/$1"><i class="fa fa-undo"></i> Restore</a></li>
						<li><a href="/admin/pages/preview/$1"><i class="fa fa-eye"></i> Preview</a></li>
					</ul>
				</div>', 'archiveID')
			->unset_column('archiveID')
			->from('PageArchives')
			->where('pageID', $pageID)
			->order_by('dateCreated', 'DESC')
			->generate();

		echo $output;
	}

	/**
	* uses datatables library to get list of deleted pages in a json format for use in admin index
	*/
	function deleted()
	{
		//load library
		$this->load->library('datatables');

		//grab data from admins table in db
		$output = $this->datatables->select('pageID, title, url, urlPath, dateUpdated')
			->select('CASE WHEN contentDraft != "" THEN "Unpublished" ELSE "Published" END as published', false)
			->add_column('actions',  '<div class="btn-group">
					<button type="button" class="btn btn-info" href="#"><i class="fa fa-file"></i> Archive</button>
					<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" href="#">
						<span class="caret"></span>
						<span class="sr-only">Toggle Dropdown</span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li><a href="/admin/pages/previewpage/$1"><i class="fa fa-eye"></i> Preview</a></li>
						<li><a href="/admin/pages/restore-page/$1"><i class="fa fa-undo"></i> Restore</a></li>
						<li><a data-confirm="Are you sure you want to permanently delete this?" href="/admin/pages/finalize-deletion/$1"><i class="fa fa-trash-o"></i> Delete</a></li>
					</ul>
				</div>', 'pageID')
			->unset_column('pageID')
			->from('Pages')
			->where('status', 'deleted')
			->order_by('dateUpdated', 'DESC')
			->generate();

		echo $output;
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

}