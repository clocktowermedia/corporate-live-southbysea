<?php
class Admin extends Admin_controller {

	//layout variables
	public $defaultLayout;

	//image upload variables
	protected $uploadFolder;
	protected $imgUploadFolder;
	protected $imgThumbUploadFolder;

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//set layout variable(s)
		$this->defaultLayout = $this->get_admin_layout();

		//set upload paths
		$this->uploadFolder = 'assets/uploads/';
		$this->imgUploadFolder = 'assets/uploads/images/';
		$this->imgThumbUploadFolder = 'assets/uploads/images/thumbnails/';

		//load relevant models
		$this->load->model('pages/page_model');
		$this->load->model('pages/page_archive_model');
	}

	/**
	* Shows all pages in a datatable
	*/
	function index()
	{
		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Pages';

		//grab pages
		$data['pageHtml'] = $this->page_model->getAdminHtml(0, 0, array(), '', 'menu-sortable');

		// tell it what layout to use and load
		$this->load->view($this->defaultLayout, $data);
	}

	/**
	* view and action for adding a page
	*/
	function create()
	{
		//tell it what view file to use
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Add Page';

		//grab pages for dropdown
		$data['pages'] = $this->page_model->getDropdown();

		//load form validation library
		$this->load->library('form_validation');

		//get validation rules from model, set extra options as neccessary
		$validationRules = $this->page_model->validate;
		$validationRules['url']['rules'] .= '|is_unique[' . $this->page_model->_table . '.url]';

		//set form validation rules
		$this->form_validation->set_rules($validationRules);

		//if the form was submitted, update database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the page and show form errors
				$data['page'] = $this->input->post(NULL, TRUE);
				$this->load->view($this->defaultLayout, $data);
			} else {
				//determine full path & depth
				if ($this->input->post('parentID', TRUE) > 0)
				{
					$urlPrefix = $this->page_model->get((int) $this->input->post('parentID', TRUE));
					$_POST['urlPath'] = $urlPrefix['urlPath'] . '/' . $this->input->post('url', TRUE);
					$_POST['depth'] = $urlPrefix['depth'] + 1;
				} else {
					$_POST['urlPath'] = '/' . $this->input->post('url', TRUE);
					$_POST['depth'] = 1;
				}

				// update record to db & redirect
				$pageID = $this->page_model->insert($this->input->post(NULL, TRUE));
				$this->page_archive_model->insert($this->input->post(NULL, TRUE) + array('pageID' => $pageID));

				//display success message & redirect
				$this->session->set_flashdata('success', 'Page was successfully created.');
				redirect('/admin/pages');
			}
		} else {
			// tell it what layout to use and load the page
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	* view and action for editing a page
	*/
	function edit($pageID = 0)
	{
		//make sure page exists
		$this->_page_is_valid($pageID);

		//grab page info for view
		$data['page'] = $this->page_model->get($pageID);

		//tell it what view file to use
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Edit ' . $data['page']['title'];

		//get dropdown for pages, exclude the current page
		$data['pages'] = $this->page_model->getDropdown(0, 0, null, array($pageID));

		//load form validation library
		$this->load->library('form_validation');

		//get validation rules from model, set extra options as neccessary
		$validationRules = $this->page_model->validate;
		if ($data['page']['urlPath'] == '/')
		{
			unset($validationRules['url']);
		}

		//set form validation rules
		$this->form_validation->set_rules($validationRules);

		//if the form was submitted, update database
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the page and show form errors
				$this->load->view($this->defaultLayout, $data);
			} else {
				//double check that the url was not edited
				if ($this->input->post('url', TRUE) != $data['page']['url'] || $this->input->post('urlPath', TRUE) != $data['page']['urlPath'])
				{
					$this->session->set_flashdata('error', 'Page was not updated, an error occurred.');
					redirect('/admin/pages/edit/' . $pageID);
				}

				//update path if applicable
				if ($this->input->post('parentID', TRUE) > 0)
				{
					$urlPrefix = $this->page_model->get((int) $this->input->post('parentID', TRUE));
					$_POST['urlPath'] = $urlPrefix['urlPath'] . '/' . $this->input->post('url', TRUE);
					$_POST['depth'] = $urlPrefix['depth'] + 1;
				} else {
					$_POST['urlPath'] = '/' . $this->input->post('url', TRUE);
					$_POST['depth'] = 1;
				}

				// update record to db & redirect
				$this->page_model->update($pageID, $this->input->post(NULL, TRUE));
				$this->page_archive_model->insert($this->input->post(NULL, TRUE) + array('pageID' => $pageID));

				//display success message & redirect
				$this->session->set_flashdata('success', 'Page was successfully updated.');
				redirect('/admin/pages');
			}
		} else {
			// tell it what layout to use and load the page
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	* action for saving a page draft
	*/
	function save($pageID = 0)
	{
		//make sure page exists
		$this->_page_is_valid($pageID);

		// update record to db & redirect
		$this->page_model->save($pageID, $this->input->post(NULL, TRUE));
		$this->page_archive_model->insert($this->input->post(NULL, TRUE) + array('pageID' => $pageID));

		//display success message & redirect
		$this->session->set_flashdata('success', 'Draft was successfully saved.');
		redirect('/admin/pages/edit/' . $pageID);
	}

	/**
	* Action for pushing draft content live
	*/
	function publish($pageID = 0)
	{
		//make sure the page is valid/exists
		$this->_page_is_valid($pageID);

		//grab page info, make sure there is draft content
		$page = $this->page_model->get($pageID);

		//make sure we actually have something to publish
		if ($page['contentDraft'] == '')
		{
			$this->session->set_flashdata('error', 'This page has already been published.');
			redirect('/admin/pages');
		}

		//set the draft content to be live
		$this->page_model->publish($pageID);

		//add to archive
		$this->page_archive_model->insert($page + array('pageID' => $pageID));

		//display success message & redirect
		$this->session->set_flashdata('success', 'Page was successfully published.');
		redirect('/admin/pages');
	}

	/**
	* Changes page status to deleted
	*/
	function delete($pageID = 0)
	{
		//make sure page exists
		$this->_page_is_valid($pageID);

		//get page info
		$page = $this->page_model->get($pageID);

		//set status to deleted
		if ($page['protected'] == false)
		{
			//show success message
			$this->page_model->soft_delete($pageID);
			$this->session->set_flashdata('success', 'Page was deleted.');

			//remove from menu as well
			$this->load->model('menus/menu_item_model');
			$this->menu_item_model->delete_by(array('uniqueIdentifier' => $pageID));

		} else {
			$this->session->set_flashdata('error', 'You do not have permission to delete this page.');
		}

		redirect('/admin/pages/');
	}

	/**
	* Deletes entry from the db entirely
	*/
	function finalize_deletion($pageID = 0)
	{
		//make sure the page exists
		$this->_page_is_valid($pageID);

		//delete page from page db
		$this->page_model->delete($pageID);

		//delete entries from achive db
		$this->page_archive_model->delete_by('pageID', $pageID);

		//redirect upon success
		$this->session->set_flashdata('success', 'Page was removed from recycle bin.');
		redirect('/admin/pages/');
	}

	/**
	* View for letting the user look at all their previous versions of the page
	*/
	function restore($pageID = 0)
	{
		//make sure page exists
		$this->_page_is_valid($pageID);

		//grab page info
		$data['page'] = $this->page_model->get($pageID);

		//load view
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Restore ' . $data['page']['title'] . ' to a Previous Version';
		$this->load->view($this->defaultLayout, $data);
	}

	/**
	* Action for restoring a page to a previous archived version
	*/
	function rollback($archiveID = 0)
	{
		//make sure that archive entry exists
		$this->_archive_is_valid($archiveID);

		//restore page to an archived version
		$this->page_model->restore($archiveID);

		//show success message and redirect
		$this->session->set_flashdata('success', 'Page was successfully restored to a previous version.');
		redirect('/admin/pages');
	}

	/**
	* View for seeing all the deleted pages
	*/
	function recycle_bin()
	{
		//grab pages that have been deleted
		$data['pages'] = $this->page_model->get_by('status', 'deleted');

		//load view
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Recycle Bin for Pages';
		$this->load->view($this->defaultLayout, $data);
	}

	/**
	* Action for restoring the page from the recycle bin
	*/
	function restore_page($pageID = 0)
	{
		//make sure the page exists
		$this->_page_is_valid($pageID);

		//change status to enabled
		$this->page_model->enable($pageID);

		//redirect and show success message
		$this->session->set_flashdata('success', 'Page was successfully restored.');
		redirect('/admin/pages');
	}

	/**
	* Allows the user to preview the page, shows draft content if applicable
	*/
	function previewpage($pageID = 0)
	{
		//make sure the page is valid/exists
		$this->_page_is_valid($pageID);

		//grab page info
		$data['page'] = $this->page_model->get($pageID);

		//if there is draft content, show that instead
		$data['page']['content'] = ($data['page']['contentDraft'] != '')? $data['page']['contentDraft'] : $data['page']['content'];

		//check to see if there is a special view for this page...
		if (file_exists(APPPATH . 'modules/pages/views/' . $data['page']['url'] . '.php')) {
			
			$data['view'] = $data['page']['url'];
			
		} else {

			//tell it what view to use, this is always called the view variable
			$data['view'] = 'view';
		}

		//load view
		$this->load->view($this->get_default_layout(), $data);
	}

	/**
	* Allows the user to preview the page (archived version)
	*/
	function preview($archiveID = 0)
	{
		//make sure the archive entry exists
		$this->_archive_is_valid($archiveID);

		//grab page info
		$data['page'] = $this->page_archive_model->get($archiveID);

		//check to see if there is a special view for this page...
		if (file_exists(APPPATH . 'modules/pages/views/' . $data['page']['url'] . '.php')) {
			
			$data['view'] = $data['page']['url'];
			
		} else {

			//tell it what view to use, this is always called the view variable
			$data['view'] = 'view';
		}

		//load view
		$this->load->view($this->get_default_layout(), $data);
	}

	/**
	 * Custom page for south by sea to edit 
	 * @return [type] [description]
	 */
	function images()
	{
		//tell it what view to use, this is always called the view variable
		$data['view'] = $this->get_admin_view();
		$data['title'] = 'Page Images';

		//get list of pages that have images, append any extras
		$this->load->model('pages/page_image_model');
		$data['pages'] = $this->page_model->append('page_image')->get_many_by('hasSplashImage', '1');
		$data['pages']['apparel'] = array(
			'title' => 'Apparel', 
			'url' => 'apparel', 
			'urlPath' => '/products/apparel', 
			'image' => $this->page_image_model->get_by('pageUrlPath', '/products/apparel')
		);
		$data['pages']['accessories'] = array(
			'title' => 'Accessories', 
			'url' => 'accessories', 
			'urlPath' => '/products/accessories',
			'image' => $this->page_image_model->get_by('pageUrlPath', '/products/accessories')
		);

		//if the form was submitted, update database
		if ($this->is_post())
		{
			//loop through post
			foreach ($_FILES as $name => $image)
			{
				if (isset($_FILES[$name]) && $_FILES[$name]['name'] != '')
				{
					//get page info
					$url = str_replace('userfile_', '', $name);
					$pageInfo = $this->page_model->get_by('url', $url);
					if (empty($pageInfo) || $pageInfo == false)
					{
						$pageInfo = $data['pages'][$url];
					}

					//format extra data for image insertion
					$insertData = array(
						'pageID' => (isset($pageInfo['pageID']))? $pageInfo['pageID'] : 0,
						'pageUrlPath' => $pageInfo['urlPath'],
					);

					//delete old image
					$this->page_image_model->delete_by('pageUrlPath', $insertData['pageUrlPath']);

					//try to upload new image
					$imageID = $this->_upload_image($name, $insertData);

					//if an error occurred during upload, show it, else continue
					if (!is_int($imageID) || $imageID <= 0)
					{
						$this->session->set_flashdata('error', $imageID);
						redirect('/admin/pages/images');
					}
				}
			}

			//show success message and redirect
			$this->session->set_flashdata('success', 'Page Images successfully uploaded.');
			redirect('/admin/pages/images');

		} else {
			// tell it what layout to use and load
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

	/**
	* Checks to see that page ID is valid and that the page exists
	*/
	public function _page_is_valid($pageID = 0)
	{
		//make sure page id is an integer
		$pageID = (int) $pageID;

		//make sure page id is greater than zero
		if ($pageID > 0)
		{
			//check to see if the page exists
			$exists = $this->page_model->get($pageID);

			if ($exists == false)
			{
				$this->session->set_flashdata('error', 'Invalid page id.');
				redirect('/admin/pages');
			}

		} else {
			$this->session->set_flashdata('error', 'Invalid page id.');
			redirect('/admin/pages');
		}
	}

	/**
	* Checks to see that the archive exists
	*/
	public function _archive_is_valid($archiveID = 0)
	{
		$archiveID = (int) $archiveID;

		//make sure archive id is greater than 0
		if ($archiveID > 0)
		{
			$exists = $this->page_archive_model->get($archiveID);

			if ($exists == false)
			{
				$this->session->set_flashdata('error', 'Invalid page archive id.');
				redirect('/admin/pages');
			}
		} else {
			$this->session->set_flashdata('error', 'Invalid page archive id.');
			redirect('/admin/pages');
		}
	}

	/**
	* Function for uploading a file
	*/
	public function _upload_image($fieldName = 'userfile', $formData = array())
	{
		//retrieve file names & replace yucky characters
		$name = $_FILES[$fieldName]['name'];
		$name = strtr($name, '����������������������������������������������������', 'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
		$name = preg_replace('/([^.a-z0-9]+)/i', '_', $name);

		//get file extension
		$extension = strrchr($name, '.');

		//define image types
		$imageTypes = array('.gif', '.jpg', '.jpeg', '.png');

		//Your upload directory and some config settings
		$config['upload_path'] = (in_array($extension, $imageTypes))? $this->imgUploadFolder : $this->uploadFolder;
		$config['max_size'] = '10000';
		$config['file_name'] = $name;
		$config['allowed_types'] = 'gif|jpg|png|JPG|GIF|PNG';

	   	//Load the upload library
		$this->load->library('upload', $config);

		//attempt upload
	   	if ($this->upload->do_upload($fieldName))
		{
			//get upload data
			$data = $this->upload->data();

			//if the file is an image...
			if ($data['is_image'] == true)
			{
				//Config for thumbnail creation
				$config['new_image'] = $this->imgThumbUploadFolder;
				$config['image_library'] = 'gd2';
				$config['source_image'] = $config['upload_path'] . $data['file_name'];
				$config['create_thumb'] = TRUE; //this adds _thumb to the end of the file name
				$config['maintain_ratio'] = TRUE;
				$config['width'] = 60;
				$config['height'] = 60;

				//create thumbnails
				$this->load->library('image_lib', $config);
				$this->image_lib->initialize($config);
				if (!$this->image_lib->resize())
				{
					return $this->image_lib->display_errors();
				}

				//grab thumbnail info
				$thumbName = str_replace($extension, '', $data['file_name']) . '_thumb' . $extension;
				$thumbPath = $this->imgThumbUploadFolder . $thumbName;
				$thumbFilesize = round ((filesize($thumbPath) / 1024 ), 2);
				$thumbInfo = getimagesize($thumbPath);

				//set image data
				$imageData = array(
					'width' => $data['image_width'],
					'height' => $data['image_height'],
					'widthHeightString' => $data['image_size_str'],
					'thumbFilename' => $thumbName,
					'thumbFilepath' => '/' . $this->imgThumbUploadFolder,
					'thumbFullPath' => '/' . $this->imgThumbUploadFolder . $thumbName,
					'thumbExtension' => $extension,
					'thumbMimeType' => $thumbInfo['mime'],
					'thumbFileSize' => $thumbFilesize,
					'thumbWidth' => $thumbInfo[0],
					'thumbHeight' => $thumbInfo[1],
					'thumbWidthHeightString' => $thumbInfo[3]
				);

			} else {
				//set image data to empty
				$imageData = array();
			}

			//regular file data
			$fileData = array(
				'filename' => $data['file_name'],
				'filepath' => '/' . $config['upload_path'],
				'fullpath' => '/' . $config['upload_path'] . $data['file_name'],
				'extension' => $data['file_ext'],
				'mimeType' => $data['file_type'],
				'fileSize' => $data['file_size']
			);

			//merge image, file data, and form data together
			$fileData = array_merge($fileData, $imageData, $formData);

			//load appropriate model
			$this->load->model('pages/page_image_model', 'image_model');

			//insert in db
			$imageID = $this->image_model->insert($fileData);

			//return file id
			return $imageID;

		} else {

			//return errors
			return $this->upload->display_errors();
		}
	}
}