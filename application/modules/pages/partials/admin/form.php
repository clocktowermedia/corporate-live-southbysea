<?php $this->form_builder->set_form_class('form-vertical'); ?>

<div class="nav-tabs-custom">
	<ul class="nav nav-tabs">
		<li class="active"><a href="#page-content" data-toggle="tab">Page Content</a></li>
		<li><a href="#meta" data-toggle="tab">SEO / Meta Tags</a></li>
		<li><a href="#settings" data-toggle="tab">Settings</a></li>
		<?php if (isset($page['urlPath']) && $page['urlPath'] == '/'): ?>
			<!--<li><a href="#slider" data-toggle="tab">Slider</a></li>-->
		<?php endif; ?>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="page-content">
			<?php
				//title field
				$this->form_builder->text('title', 'Title', (isset($page['title']))? $page['title'] : '', '', '', '', '', array('required' => 'required'));

				//content
				$content = (isset($page['contentDraft']) && $page['contentDraft'] != '')? $page['contentDraft'] : '';
				$content = ($content == '' && isset($page['content']) && $page['content'] != '')? $page['content'] : $content;
				$this->form_builder->textarea('content', 'Content', $content, array('inputClass' => 'wysiwyg'), 10, '', '', '', '');
			?>
		</div>
		<div class="tab-pane" id="meta">
			<?php
				//meta title field
				$this->form_builder->text('metaTitle', 'Meta Title', (isset($page['metaTitle']))? $page['metaTitle'] : '', '', '', '', '', array());

				//meta keywords
				$this->form_builder->textarea('metaKeywords', 'Meta Keywords', (isset($page['metaKeywords']))? $page['metaKeywords'] : '', '', 3, '', '', '', '');

				//meta description
				$this->form_builder->textarea('metaDesc', 'Meta Description', (isset($page['metaDesc']))? $page['metaDesc'] : '', '', 3, '', '', '', '');
			?>
		</div>
		<div class="tab-pane" id="settings">
				<?php
					//url field
					$readonlyUrl = ($this->uri->segment(3) == 'edit')? array('readonly' => 'readonly') : array();
					$required = (isset($page) && $page['urlPath'] == '/')? array() : array('required' => 'required');
					$this->form_builder->text('url', 'URL', (isset($page['url']))? $page['url'] : '', '', '', 'Alphanumeric characters only. For SEO purposes, use lowercase letters and dashes to indicate spaces.', '', array('pattern' => '^[a-z0-9-]+$') + $readonlyUrl + $required);

					//parent field
					$this->form_builder->select('parentID', 'Parent Page', $pages, (isset($page['parentID']))? $page['parentID'] : '', '', '', '', array());

					//display full url
					$this->form_builder->text('urlPath', 'Full URL Path', (isset($page['urlPath']))? $page['urlPath'] : '', '', '', '', '', array('required' => 'required', 'readonly' => 'readonly'));
				?>
		</div>

		<!--
		<div class="tab-pane" id="slider">

			<h3>Add Images</h3>
			<div id="upload-photos">
				<div id="" class="text-center upload-container">
					<h4>Drag &amp; Drop Images Here</h4>
					<h5>-or-</h5>
					<label>
						<input type="button" id="upload-btn" class="btn btn-info btn-large" value="Click to Add Images">
						<input id="upload-trigger" type="file" name="userfile[]" class="hide" multiple="multiple" title="Click to add Files">
					</label>
				</div>
			</div>

			<hr>

			<h3>Current Images</h3>
			<div id="slider-images">
			</div>

			<script id="slider-images-listing" type="x-handlebars-template">
				{{#if images}}
					<div class="row">
						{{#each images}}
							<div class="col-md-1 col-sm-2 col-xs-3 thumbnail-img" id="images-{{imageID}}">
								<a data-toggle="popover" data-id="{{imageID}}" data-title="Actions">
									<img src="{{thumbFullpath}}" class="img-responsive">
								</a>
							</div>

							<div id="popover-content-{{imageID}}" class="popover-content hide">
								<a data-id="{{imageID}}" class="btn btn-block btn-info edit-image"><i class="fa fa-edit"></i> Edit</a>
								<a data-id="{{imageID}}" class="btn btn-block btn-info delete-image"><i class="fa fa-trash-o"></i> Delete</a>
							</div>

							<div class="modal fade" id="image-modal-{{imageID}}">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
											<h4 class="modal-title">Edit Image</h4>
										</div>
										<form class="form-horizontal image-modal-form">
											<div class="modal-body">
												<div class="form-group">
													<label for="link" class="col-sm-2 control-label">Link:</label>
													<div class="col-sm-10"><input type="text" name="link" class="form-control"></div>
												</div>

												<div class="form-group">
													<label for="title" class="col-sm-2 control-label">Alt Text:</label>
													<div class="col-sm-10"><input type="text" name="title" class="form-control"></div>
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												<button type="submit" class="btn btn-info">Save</button>
											</div>
										</form>
									</div>
								</div>
							</div>

						{{/each}}
					</div>
				{{else}}
					<p>No images.</p>
				{{/if}}
			</script>

		</div>-->

	</div><!-- /.tab-content -->
</div><!-- nav-tabs-custom -->