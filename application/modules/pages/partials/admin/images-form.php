<div class="row">

	<?php $count = 0; ?>
	<?php foreach ($pages as $page): ?>
		<?php $count++; ?>
		<div class="col-xs-6">
			<div class="box box-solid">
				<div class="box-header">
					<h3 class="box-title"><?php echo $page['title'];?></h3>
					<div class="box-tools pull-right">
						<button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
						<button class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
					</div>
				</div>
				<div class="box-body">
					
					<?php if (isset($page['page_image']['thumbFullpath']) && $page['page_image']['thumbFullpath'] != ''): ?>
						<div class="form-group">
							<label class="col-sm-1 control-label">Current Image:</label>
							<div class="col-sm-11">
								<a target="_blank" href="<?php echo $page['page_image']['fullpath']; ?>"><img src="<?php echo $page['page_image']['thumbFullpath']; ?>" class="img-thumbnail"></a>
							</div>
						</div>
					<?php endif; ?>

					<div class="form-group">
						<label class="col-sm-1 control-label" for="userfile_<?php echo $page['url'];?>">Image:</label>
						<div class="col-sm-11">
							<input type="file" name="userfile_<?php echo $page['url'];?>" size="20" class="form-control"/>
							<?php if (isset($page['page_image']['thumbFullpath']) && $page['page_image']['thumbFullpath'] != ''): ?>
								<p class="help-block">If you upload a new image, the image above will be replaced.</p>
							<?php endif; ?>
						</div>
					</div>

					<div class="clearfix"></div>

				</div><!-- /.box-body -->
			</div>
		</div>
		<?php if ($count % 2 == 0): ?>
			<div class="clearfix"></div>
		<?php endif; ?>
	<?php endforeach; ?>

</div>