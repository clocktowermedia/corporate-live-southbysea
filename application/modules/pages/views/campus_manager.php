<?php if (strpos($_SERVER[REQUEST_URI], 'fraternity')): ?>
	<div class = "container">
		<div class="clearfix"></div>
		<div class="col-md-6">
			<?php if (isset($page['page_image']['fullpath'])): ?>
				<img src="<?php echo $page['page_image']['fullpath']; ?>" alt="<?php echo $page['title']; ?>">
			<?php else: ?>
				<img src="/assets/images/campus-manager.jpg" alt="">
			<?php endif; ?>
		</div><!-- /.col-md-6 -->
		<div class="col-md-6">
			<h4 class="mt-0" style="font-family:Hello Stockholm Alt,Times New Roman, serif;text-transform:lowercase;text-align:center;font-size:100px;"><?php echo $page['title']; ?></h4>
			<p><?php echo $page['content']; ?></p><br>
			<span style = "display: block; margin-left: auto; margin-right: auto;">
					<a href="/campus-managers/apply" title="Apply"><img src="/assets/images/NewSApplyToday.jpg" height= "40%;" width="40%;"</a>
			</span>
		</div><!-- /.col-md-6 -->
	</div>

	<div class="clearfix"></div>

	<div class="row mb-0">
		<div class="col-xs-12">
			<h4 class="bnr-nttl4" style="font-family:Northwest Regular,Times New Roman, serif;font-size:50px;">Find Your Campus Manager</h4>
		</div>
	</div>
	<div class = "container">
		<div class="col-xs-12 ">
			<?php echo form_open('/campus-managers', array('id' => 'campus-rep-search')); ?>
			<fieldset>
				<?php
					//add the field
					$this->form_builder->text('q', '', '', '', 'search by school name', '', '', array('id' => 'school-autosuggest'));
					//$this->form_builder->psuedo_hidden('q', '', '', '', '', '', '', array('id' => 'school-autosuggest'));
				?>
			</fieldset>
			</form>

			<div id="school-listing">
				<?php if (isset($reps) && count($reps) > 0): ?>
					<?php foreach ($reps as $rep) :?>
						 <div class="row mt-0 mb-0">
							<a href="/campus-managers/managers/<?php echo $rep['schoolUrl'];?>/<?php echo $rep['url'];?>" title="<?php echo $rep['school'] . ' - ' . $rep['firstName'] . ' ' . $rep['lastName']; ?>">
								<div class="col-xs-12">
									<div class="rep-container">
										<h4 class="pull-left rep-school"><?php echo $rep['school'];?></h4>
										<span class="pull-right rep-name"><?php echo $rep['firstName'] . ' ' . $rep['lastName']; ?></span>
									</div>
								</div>
							</a>
						</div>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>

			<script id="results-template" type="x-handlebars-template">
				{{#if search}}
					{{#if reps}}
						{{#each reps}}
							<div class="row mt-0 mb-0">
								<a href="/campus-managers/managers/{{schoolUrl}}/{{url}}" title="{{school}} - {{firstName}} {{lastName}}">
									<div class="col-xs-12">
										<div class="rep-container">
											<h4 class="pull-left rep-school">{{school}}</h4>
											<span class="pull-right rep-name">{{firstName}} {{lastName}}</span>
										</div>
									</div>
								</a>
							</div>
						{{/each}}
					{{else}}
						<p>No matches found.</p>
					{{/if}}
				{{/if}}
			</script>
		</div>
	</div>
<?php else: ?>
	<div class = "container">
		<div class="clearfix"></div>
		<div class="col-md-6">
			<?php if (isset($page['page_image']['fullpath'])): ?>
				<img src="<?php echo $page['page_image']['fullpath']; ?>" alt="<?php echo $page['title']; ?>">
			<?php else: ?>
				<img src="/assets/images/campus-manager.jpg" alt="">
			<?php endif; ?>
		</div><!-- /.col-md-6 -->
		<div class="col-md-6">
			<h4 class="mt-0" style="font-family:Hello Stockholm Alt,Times New Roman, serif;text-transform:lowercase;text-align:center;font-size:100px;"><?php echo $page['title']; ?></h4>
			<p><?php echo $page['content']; ?></p><br>
			<span style = "display: block; margin-left: auto; margin-right: auto;">
					<a href="/campus-managers/apply" title="Apply"><img src="/assets/images/NewSApplyToday.jpg" height= "40%;" width="40%;"</a>
			</span>
		</div><!-- /.col-md-6 -->
	</div>

	<div class="clearfix"></div>

	<div class="row mb-0">
		<div class="col-xs-12">
			<h4 class="bnr-nttl3" style="font-family:Hello Stockholm Alt,Times New Roman, serif;font-size:50px;">Find Your Campus Manager</h4>
		</div>
	</div>
	<div class = "container">
		<div class="col-xs-12 ">
			<?php echo form_open('/campus-managers', array('id' => 'campus-rep-search')); ?>
			<fieldset>
				<?php
					//add the field
					$this->form_builder->text('q', '', '', '', 'search by school name', '', '', array('id' => 'school-autosuggest'));
					//$this->form_builder->psuedo_hidden('q', '', '', '', '', '', '', array('id' => 'school-autosuggest'));
				?>
			</fieldset>
			</form>

			<div id="school-listing">
				<?php if (isset($reps) && count($reps) > 0): ?>
					<?php foreach ($reps as $rep) :?>
						 <div class="row mt-0 mb-0">
							<a href="/campus-managers/managers/<?php echo $rep['schoolUrl'];?>/<?php echo $rep['url'];?>" title="<?php echo $rep['school'] . ' - ' . $rep['firstName'] . ' ' . $rep['lastName']; ?>">
								<div class="col-xs-12">
									<div class="rep-container">
										<h4 class="pull-left rep-school"><?php echo $rep['school'];?></h4>
										<span class="pull-right rep-name"><?php echo $rep['firstName'] . ' ' . $rep['lastName']; ?></span>
									</div>
								</div>
							</a>
						</div>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>

			<script id="results-template" type="x-handlebars-template">
				{{#if search}}
					{{#if reps}}
						{{#each reps}}
							<div class="row mt-0 mb-0">
								<a href="/campus-managers/managers/{{schoolUrl}}/{{url}}" title="{{school}} - {{firstName}} {{lastName}}">
									<div class="col-xs-12">
										<div class="rep-container">
											<h4 class="pull-left rep-school">{{school}}</h4>
											<span class="pull-right rep-name">{{firstName}} {{lastName}}</span>
										</div>
									</div>
								</a>
							</div>
						{{/each}}
					{{else}}
						<p>No matches found.</p>
					{{/if}}
				{{/if}}
			</script>
		</div>
	</div>
<?php endif; ?>
