
<style type="text/css">
@font-face {
    font-family: 'Hello Stockholm Regular';
    src: url('/assets/fonts/HelloStockholm-Regular.ttf') format('truetype');
    font-weight: normal;
    font-style: normal;
}
@font-face {
    font-family: 'Hello Stockholm Alt';
    src: url('/assets/fonts/HelloStockholm-Alt.ttf') format('truetype');
    font-weight: normal;
    font-style: normal;
}
.category-title,
.bnr-nttl3{
	background:url(/assets/images/NewSBanner.jpg) no-repeat 50% 0;
	background-size:100% 100%;
	text-align:center;
	box-sizing:border-box;
	width:100%;
	padding:12px 20px;
	clear:both;
	text-transform: lowercase;
}
.bnr-nttl3:before{
	content: "";
	display:block;
	height:0;
	clear:both;
}
h3.wgt-title{
	/*font-family:"Dawing of a New Day", "Times New Roman", serif;*/
  font-family:"Hello Stockholm Alt", "Times New Roman", serif;
	text-transform:lowercase;
	font-size:28px;
	position:relative;
	padding:0 5px;
	margin-bottom:20px;
	display:inline-block;
}
h3.wgt-title-hello-alt{
	font-family:"Hello Stockholm Alt", "Times New Roman", serif;
	text-transform:lowercase;
	font-size:28px;
	position:relative;
	padding:0 5px;
	margin-bottom:20px;
	display:inline-block;
}
h3.wgt-title-hello-reg{
	font-family:"Hello Stockholm Reg", "Times New Roman", serif;
	font-size:28px;
	position:relative;
	padding:0 5px;
	margin-bottom:20px;
	display:inline-block;
}
</style>

<div class="col-xs-12">
	<!-- <h1>South By Sea Air-Stream!</h1>
	<p><?php echo $page['content']; ?></p> -->

  <div class="s-padding-left-small s-padding-right-small s-span-page">
    <div class="bxc-grid__row   bxc-grid__row--light ">
		    <div class="bxc-grid__column  bxc-grid__column--12-of-12   bxc-grid__column--light">
		        <div class="bxc-grid__content   bxc-grid__content--light">
              <div class="bxc-grid__image   bxc-grid__image--light">
	               <img src="/assets/images/airstreamtext3updated.jpg" alt="We&amp;#039;re going places">
               </div>
		        </div>
		    </div>
		</div><br>
    <div class="bxc-grid__row   bxc-grid__row--light ">
		    <div class="bxc-grid__column  bxc-grid__column--12-of-12   bxc-grid__column--light">
		        <div class="bxc-grid__content   bxc-grid__content--light">
              <div class="bxc-grid__image   bxc-grid__image--light">
	               <img src="/assets/images/airstreammap.jpg" alt="Map">
              </div>
		         </div>
		    </div>
		</div>
  </div>
	<a href="https://twitter.com/sxscollege" class="twitter-follow-button" data-show-count="false">Follow @sxscollege</a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

</div>
</div>
	<div>
    <hr>
      <div class="col-md-6" dir="auto" >
        <br>
        <!-- <h4 class="bnr-ttl3 mt-0">&mdash; SXS Airstream! &mdash;</h4> -->
        <div class="bxc-grid__row   bxc-grid__row--light ">

		<div class="bxc-grid__column  bxc-grid__column--4-of-12   bxc-grid__column--light">

		<div class="bxc-grid__content   bxc-grid__content--light">

<div class="bxc-grid__image   bxc-grid__image--light">
	<img src="/assets/images/airstreamtext2.jpg" alt="Pumpkins in front of the truck">
</div>

		</div>

		</div>

		<hr>

		</div>
  		</div>
			<div class="col-md-6" dir="auto" >
				<div class="bxc-grid__row   bxc-grid__row--light ">

		<div class="bxc-grid__column  bxc-grid__column--6-of-12   bxc-grid__column--light">

		<div class="bxc-grid__content   bxc-grid__content--light">

      <div class="bxc-grid__image   bxc-grid__image--light">
        <a class="twitter-timeline"  href="https://twitter.com/hashtag/SxSAirstream" data-widget-id="903307059814420480" data-height="500">#SxSAirstream Tweets</a>
        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
      </div>

				</div>

				</div>
		</div>
      <div class="clearfix"></div>
      <br>
		</div>
    <div class="row mb-0">
    	<div class="col-xs-12">
        <a href="/airstream/school-suggestion" title="submissionsform" class="no-underline">
    		    <!-- <h4 class="bnr-nttl3" style="font-family:Hello Stockholm Alt,Times New Roman, serif;font-size:50px;">Click here to suggest a place near you</h4> -->
            <h4 class="bnr-nttl3"
                style="font-family:Hello Stockholm Alt,Times New Roman, serif;
                      font-size:50px;
                      background:url(/assets/images/NewSBanner.jpg) no-repeat 50% 0;
                      background-size:100% 100%;
                      text-align:center;
                      box-sizing:border-box;
                      width:100%;
                      padding:12px 20px;
                      clear:both;
                      text-transform: lowercase;
                      font-size: calc(4px + 7vw);">
                      Click here to suggest a place near you
            </h4>

        </a>
    	</div>
    </div>
    <div class="clearfix"></div>
		<div>
			<!-- <a class="twitter-grid" href="https://twitter.com/TwitterDev/timelines/539487832448843776">South By Sea</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script> -->
			<!-- SnapWidget -->
			<!-- SnapWidget -->
			<!-- SnapWidget -->
			<script src="https://snapwidget.com/js/snapwidget.js"></script>
			<iframe src="https://snapwidget.com/embed/432519" class="snapwidget-widget" allowTransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden; width:100%; "></iframe>
					</div>
			<hr>
			<div class="clearfix"></div>
			<div class="col-md-12" dir="auto" >
				<div class="col-md-6" dir="auto" >
					<a class="twitter-timeline" data-width="600" data-height="975" href="https://twitter.com/sxscollege">Tweets by sxscollege</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
				</div>
			<div class="col-md-6" dir="auto" >
				<div class="s-padding-left-small s-padding-right-small s-span-page">
					<div class="bxc-grid__row   bxc-grid__row--light ">
							<div class="bxc-grid__column  bxc-grid__column--12-of-12   bxc-grid__column--light">
									<div class="bxc-grid__content   bxc-grid__content--light">
										<div class="bxc-grid__image   bxc-grid__image--light">
											 <img src="/assets/images/airstreamtext1.jpg" alt="We&amp;#039;re going places">
										 </div>
									</div>
							</div>
						</div>
					</div>
					<hr>
					<!-- SnapWidget -->
					<iframe src="https://snapwidget.com/embed/432527" class="snapwidget-widget" allowTransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden; width:700px; height:700px"></iframe>			</div>
		</div>
    <div class = "clearfix"></div>
    <div class = "col-md-12" dir="auto">
      <script type="text/javascript">
      var feed = "http://www.shopsouthbysea.com/blog?format=RSS";


      </script>
</div>
</div>
