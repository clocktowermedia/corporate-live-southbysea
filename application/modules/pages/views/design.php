<div  class="col-lg-12">
	<div class="col-lg-3">
		<div class="sidebar">
			<div class="clearfix"></div>
	        <h3 class="wgt-title" style = "font-size:100px;margin-left:-55px;"> Designs old</h3>
	        <?php if (isset($categories) && count($categories) > 0): ?>
	            <ul class="categories" style = "font-size:35px;">
	              <li class="active"><a href="/designs"><small>All Designs</small></a> <i class="arrow-single-left"></i><br><br>
	              <li><a href="/designs/new"><small>New</small></a><br><br>
	              <?php foreach ($categories as $category): ?>
	              	<li>
	                    <a href="/designs/<?php echo $category['url'];?>" title="<?php echo $category['title']; ?>"><small>
	                        <?php echo $category['title']; ?>
	                    </small></a>
	                </li><br>
	              <?php endforeach; ?>
	            </ul>
	        <?php endif; ?>
					<div class= "pull-right">
						<form action="/designs" method="get" class="design_search">
								<label for="filters"></label>
								<input type="text" name="filters" value="<?php echo $this->input->post('filters', TRUE); ?>" placeholder="&#xF002; search" style="font-size: 1em; padding-right:16px; opacity: 0.55;outline: none; border-color: transparent; border: none; background: url(/assets/images/search-icon.png) 195px 2px no-repeat;
			; font-family:Helvetica Neue, FontAwesome" class="text-right">
						</form>
					</div>
	    </div>
	</div><!-- /.col-md-3 -->

	<!-- <div class="col-lg-9" id="results-container">
		<div class="col-md-6">
			<span style = "text-align:center;">
				<span style="display: inline-block; margin-left: auto;margin-right: auto;height:300px;width:auto;"class="image">
						<img src="/assets/images/NewSBackground.jpg" style="height:94%;">
				</span>
			</span>
		</div>

		<div class="col-md-6">
			<span style = "text-align:center;">
				<span style="display: inline-block; margin-left: auto;margin-right: auto;height:300px;width:auto;" class="image">
						<img src="/assets/slideshow/2 - Medium Top Right.jpg" style="height:100%;">
				</span>
			</span>
		</div>
		<br><br><br> -->

	    <?php if (isset($designs) && count($designs) > 0): ?>
	        <?php $count = 0; ?>
	        <?php foreach ($designs as $design): ?>
	            <?php $count++; ?>

	            <?php if ($count % 3 == 1): ?>
	                <div class="row mt-0">
	            <?php endif; ?>
	                    <div class="col-sm-4">
	                        <div class="product" id="<?php echo print_r($design['tags'],true)?>">
	                            <h4>
	                                <a href="/designs/<?php echo $design['design_category']['url'];?>/<?php echo $design['url'];?>" title="<?php echo $design['title']; ?>"><?php echo $design['title']; ?></a>
	                                <i class="arrow-dbl-left"></i>
	                            </h4>
	                            <?php if (isset($design['design_image']['thumbFullpath'])): ?>
	                                <a href="/designs/<?php echo $design['design_category']['url'];?>/<?php echo $design['url'];?>" title="<?php echo $design['title']; ?>">
	                                    <img src="<?php echo $design['design_image']['thumbFullpath']; ?>" alt="<?php echo $design['title']; ?>" title="<?php echo $design['title']; ?>">
	                                </a>
	                            <?php else: ?>
	                                <a href="/designs/<?php echo $design['design_category']['url'];?>/<?php echo $design['url'];?>" title="<?php echo $design['title']; ?>">
	                                    <img src="/assets/images/sm-pl-hld.jpg" alt="<?php echo $design['title']; ?>" title="<?php echo $design['title']; ?>">
	                                </a>
	                            <?php endif; ?>
	                        </div>
	                    </div><!-- /.col-sm-4 -->
	            <?php if ($count % 3 == 0): ?>
	                </div>
	            <?php endif; ?>
	        <?php endforeach; ?>

	        <div class="clearfix"></div>

	        <div class="sbs-paging">
	            <div class="pull-right"><?php echo $pagination; ?></div>
	        </div>
	    <?php else: ?>
	        <h3 class="mt-0">Sorry! No designs have been added yet. Please be sure to check back later!</h3>
	    <?php endif; ?>
	</div><!-- /.col-md-9 -->
	<div class= "clearfix"></div>
</div>
