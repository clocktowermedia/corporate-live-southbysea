
	<div class="col-xs-12">
		<h1 style="padding-bottom:5px;"><span style="font-family:museo-sans; text-transform: uppercase;">ORDER FORM</span></h1>
		<p><?php echo $page['content'];?></p>

		<?php echo form_open_multipart('/forms/order-form', array('id' => 'final-submission-form', 'class' => 'persist', 'accept-charset' => 'UTF-8')); ?>
		<fieldset>
		    <?php
		        //add the form
		        $this->load->view('../../forms/partials/forms/order-form');

		        //submit button
		        echo '<div class="clearfix"></div>';
		        $this->form_builder->button('submit', '', array('inputClass' => 'btn btn-info'), '', '');
		    ?>
		</fieldset>
		</form>
	</div>
