<div class="col-xs-12">
	<h1>SXS Campaign Captain Application</h1>
	<p><?php echo $page['content']; ?></p>
	<div>
		<div dir="auto">
			<small>Apply to be a South by Sea Campaign Captain for your sorority! There will be a maximum of 5 CC's per sorority!</small><br>
			<hr>
			<b>Details:</b><br>
			- You'll be working on a team with other members of your swap page to sell SXS Campaigns!<br>
			- Your team will manage your sorority's Campaign Instagram account!<br>
			- 6% Commission and a freebie of the item from your Campaign!<br><br>
			<b>Requirements:</b><br>
			- Need to be a part of your sorority's swap page<br>
			- Be able to work with a team <br>
			- Be trendy/fashionable<br><br>
			<p>
				Your application will be reviewed within the next week and if chosen you'll receive an email from the Sales Team!<br><br>
			</p>
			<p>Thanks and Good Luck!! <br><br></p>
			</div><br>
		</div>

	<?php echo form_open_multipart('/forms/chapter-rep', array('id' => 'chapter-form', 'class' => 'persist', 'accept-charset' => 'UTF-8')); ?>
	<fieldset>
	    <?php
	        //add the form
            $this->load->view('../../forms/partials/forms/chapter-rep');

	        //submit button
	        $this->form_builder->button('submit', '', array('inputClass' => 'btn btn-info'), '', '');
	    ?>
	</fieldset>
	</form>
</div>
