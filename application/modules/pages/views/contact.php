
<div class="col-lg-12 col-md-12 hidden-sm hidden-xs"style="">
  <div class="col-md-8 col-sm-8" style = "padding-right:0px;">
    <img class ="pull-left" src="/assets/images/contact-us-left-big.jpg"alt="">
  </div>
  <div class="col-md-4 col-sm-4" style = "padding-right:0px;padding-left:0px;">
    <img class ="" src="/assets/images/contact-pull-right.jpg" style = "">
  </div>
</div>

<div class="hidden-lg hidden-md col-sm-12 col-xs-12">

  <div class="col-sm-12 col-xs-12">
    <img class ="" src="/assets/slideshow/3c - Medium Top Right.jpg" style = "transform: scale(1.04, 1.04);"alt="">
  </div>
  <br/><br/>

  <div class="col-sm-12 col-xs-12" style="background:#EAEEEF;">
    <p style = "font-family:museo-sans; font-size:calc(7px + .4vw);">
      <br/><br/>
        At South by Sea we deliver custom apparel and accessories across the nation. Our mission extends beyond great design and products; we are the quality experience. Passion and determination drives our dedicated team, making each inspired design tailored to our customer’s needs. Our in-house printing ensures that care is put into each order. Creativity, adventure, passion, lifestyle, fashion, quality, and affordability are all part of what makes the South by Sea experience special.
<br/><br/>
    </p>
  </div>
</div>

<div class="col-md-12">
</br>
</div>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

    <div class="col-md-1"></div>
    <div class="col-md-5 col-sm-6 col-xs-12" style=" margin-left: auto; margin-right: auto; ">
      <h3 class="wgt-title" style="font-size:20px;padding-left:0px; letter-spacing:4px;">Corporate Info</h3>
      <p style=" text-transform:uppercase !important; font-family:museo-sans; opacity: 0.75; font-size:12px;">209 dayton street ste 205<br>
      edmonds, wa 98020</p>
      <p style="font-family:museo-sans; text-transform:uppercase !important; opacity: 0.75; font-size:12px;">p: 888/855/7960<br>
      f: 888/261/5353</p>
      <p style="font-family:museo-sans; text-transform:uppercase !important; opacity: 0.75; font-size:12px;">hours: m-f, 8:30a-5p, PST</p>
    </div>
    <div class="col-md-5 col-sm-6 col-xs-12 col-md-offset-1">
      <h3 class="wgt-title" style="font-size:20px;padding-left:0px; letter-spacing:4px;"> Network with us</h3>

      <div>
        <img src="/assets/images/icons/corp-facebook.png" style = "display:inline-block; vertical-align:middle;" width="40px;"><span style="font-family:museo-sans; text-transform:uppercase !important; letter-spacing: 2px; padding: 0px 0 0px 15px; opacity:0.75; font-size:12px;">facebook</span>
      </div>
      </br>
      <div>
        <img src="/assets/images/icons/corp-pin.png" style = "display:inline-block; vertical-align:middle;" width="40px;"><span style="font-family:museo-sans; text-transform:uppercase !important; letter-spacing: 2px; padding: 0px 0 0px 15px; opacity:0.75; font-size:12px;">pinterest</span>
      </div>
      </br>
      <div>
        <img src="/assets/images/icons/corp-instagram.png" style = "display:inline-block; vertical-align:middle;" width="40px;"><span style="font-family:museo-sans; text-transform:uppercase !important; letter-spacing: 2px; padding: 0px 0 0px 15px; opacity:0.75; font-size:12px;">instagram</span>
        </a>
      </div>
    </div>
    <div class="col-md-1"></div>
    </div>
  </div>


  <div class="col-md-12 ol-sm-8 col-xs-8">
  </br></br></br>
  </div>

  <div class="col-md-8 col-sm-12 col-xs-12 ">
     <?php echo form_open('/forms/contact', array('accept-charset' => 'UTF-8')); ?>
     <fieldset>
     	<?php
     		//add the form
     		$this->load->view('../../forms/partials/forms/contact');

     		//submit button
     		$this->form_builder->button('submit', ' ', array('inputClass' => 'btn btn-info'), '', '');
     	?>
     </fieldset>
     </form>

 </div><!-- /.col-md-8 -->

 <div class="col-md-4 hidden-sm hidden-xs image-wrapper" style="font-size:calc(5px + 1.5vw);">
   <img src="/assets/images/contact-us-email-us.jpg" alt="">
   <!-- <div style="letter-spacing:5px;"class="S-top-right pull-right">Have a question</br></br><span style="">or comment?</span></div> -->
   <!-- <div style="font-family:Hello Stockholm Alt; font-size:calc(40px + 2vw);" class="S-centered">Email Us</div> -->
 </div>
