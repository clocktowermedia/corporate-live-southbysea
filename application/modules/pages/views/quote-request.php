<div class="col-xs-12">
	<h1 style="padding-bottom:5px;"><span style="font-family:CACHE; text-transform: uppercase;">QUOTE REQUEST</span></h1>
	<p><?php echo $page['content'];?></p>

	<?php echo form_open_multipart('/forms/quote-request', array('id' => 'proof-request-form', 'class' => 'persist', 'accept-charset' => 'UTF-8')); ?>
	<fieldset>
	    <?php
	        //add the form
	        $this->load->view('../../forms/partials/forms/quote-request');


	        //submit button
	        $this->form_builder->button('submit', '', array('inputClass' => 'btn btn-info'), '', '');
	    ?>
	</fieldset>
	</form>
</div>
