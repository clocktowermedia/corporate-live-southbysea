<div class="col-xs-12">
	<h1 class="mt-0"><?php echo $page['title'];?></h1>
	<p><?php echo $page['content'];?></p>

	<div class="clearfix"></div>

	<div class="book-container">
		<div id="book">
			<?php for ($i = 1; $i <= 50; $i++): ?>
				<?php if ($i <= 1): ?>
					<div class="cover" id="page<?php echo $i; ?>">
						<img src="/assets/flipbook/FINAL-PRINT-<?php echo $i;?>.jpg" width="100%" height="100%" alt="South by Sea Catalog - Page <?php echo $i; ?>"/>
					</div>
				<?php else: ?>
					<div id="page<?php echo $i; ?>" class="cover" data-image="/assets/flipbook/FINAL-PRINT-<?php echo $i;?>.jpg"></div>
				<?php endif; ?>
			<?php endfor; ?>
		</div>
	</div>

</div>
