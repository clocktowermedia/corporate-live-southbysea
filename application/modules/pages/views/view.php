<?php if (($_SERVER[REQUEST_URI] === '/fraternity') || ($_SERVER[REQUEST_URI] === '/fraternity/')): ?>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style = "padding-right:0px;padding-left:0px;">
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style = "padding-right:0px;padding-left:0px;">
			<img class =" fraternity-slideshow" src="/assets/images/NewFSlideshow1-1.jpg" style = " ">
		</div>
		<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8" style="padding-right:0px;padding-left:0px;">
			<img class =" fraternity-slideshow" src="/assets/images/NewFSlideshow1-2.jpg" style = " ">
		</div>
	</div>
	<a href="/fraternity/designs"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fraternity-slideshow-banner">
	</div></a>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style = "padding-right:0px;padding-left:0px;">
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style = "padding-right:0px;padding-left:0px;">
			<a href="/fraternity/products/apparel"><img class ="fraternity-slideshow-2" src="/assets/images/NewFSlideshow2-1a.jpg" style = ""></a>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style = "padding-right:0px;padding-left:0px;">
			<img class ="fraternity-slideshow-2" src="/assets/images/NewFSlideshow2-2a.jpg" style = " ">
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style = "padding-right:0px;padding-left:0px;">
			<img class ="fraternity-slideshow-2" src="/assets/images/NewFSlideshow2-3a.jpg" style = " ">
		</div>
	</div>

	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><br><br>
	</div>
	<a href="/fraternity/quote-request"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fraternity-slideshow-banner-2">
	</div></a>

<?php elseif(strpos($_SERVER[REQUEST_URI], 'fraternity')): ?>

	<?php if ($this->input->get('modal')): ?>
		<p><?php echo $page['content']; ?></p>
	<?php else: ?>
		<div class="col-xs-12">
			<h1 class="mt-0" style="font-family:NORTHWEST Bold;"><?php echo $page['title'];?></h1>
			<p><?php echo $page['content'];?></p>
		</div>
	<?php endif; ?>

<?php else: ?>

	<?php if ($this->input->get('modal')): ?>
		<p><?php echo $page['content'];?></p>
	<?php else: ?>
		<div class="col-xs-12">
			<h1 class="mt-0" style=""><?php echo $page['title'];?></h1>
			<p><?php echo $page['content'];?></p>
		</div>
	<?php endif; ?>

<?php endif; ?>
