<script type="text/javascript">
$(document).ready(function() {
    $("#breadcrumb_id").remove();
    document.getElementById('breadcrumb_id').style.display = 'none';
});
</script>


<div class="row mt-0">
    <div class="col-md-10 col-md-offset-1">
        <div class="content">
            <div class="row mt-0">
                <div class="col-md-6 text-center">
                    <div class="lrg-ft-img">
                        <div class="slide-container">
                                <div class="image">
                                    <img src="/assets/slideshow/1 - Large Left.jpg">
                                </div>
                        </div>
                        <a class="new-btn-lrg mt-15 no-underline" href="/designs" title="Designs" class="no-underline">Designs</a>
                    </div><!-- /.lrg-ft-img -->
                </div><!-- /.col-md-6" -->
                <div class="col-md-6 grey-bg-rtt text-center">
                    <div class="md-ft-img">
                        <div class="slide-container">
                                <div class="image">
                                    <img src="/assets/slideshow/1 - Medium Top Right.jpg">
                                </div>
                        </div>
                        <a class="new-btn-lrg mt-15 no-underline" href="/products/apparel" title="Apparel">Apparel</a>
                    </div><!-- /.md-ft-img -->
                    <div class="row">
                        <div class="col-xs-6 sm-ft-img text-center">
                            <div class="ft1">
                                <div class="slide-container">
                                        <div class="image">
                                            <img src="/assets/slideshow/1 - Small Bottom Left.jpg">
                                        </div>
                                </div>
                                <a class="new-btn-sm mt-15 no-underline" href="/products/accessories" title="Accessories" class="no-underline">Accessories</a>
                            </div><!-- /.sm-ft-img -->
                        </div><!-- /.col-xs-6 -->
                        <div class="col-xs-6 sm-ft-img text-center">
                            <div class="ft2">
                                <div class="slide-container">
                                        <div class="image">
                                            <img src="/assets/slideshow/1 - Small Bottom Right.jpg">
                                        </div>
                                </div>
                                <a class="new-btn-sm mt-15 no-underline" href="/quote-request" title="Submit a Proof Request">Proof Request</a>
                            </div><!-- /.sm-ft-img -->
                        </div><!-- /.col-xs-6 -->
                    </div><!-- /.row -->
                </div><!-- /.col-md-6" -->
            </div><!-- /.row -->
        </div><!-- /.content -->
    </div>
</div>
