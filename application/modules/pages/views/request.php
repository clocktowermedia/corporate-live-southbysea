<div class="col-xs-12">
	<h1><?php echo $page['title'];?></h1>
	<p><?php echo $page['content'];?></p>

	<?php echo form_open_multipart('/forms/catalog-request', array('id' => 'catalog-request-form', 'class' => 'persist', 'accept-charset' => 'UTF-8')); ?>
	<fieldset>
	    <?php
	        //add the form
	        $this->load->view('../../forms/partials/forms/catalog-request');

	        //submit button
	        echo '<div class="clearfix"></div>';
	        $this->form_builder->button('submit', 'Submit', array('inputClass' => 'btn btn-info'), '', '');
	    ?>
	</fieldset>
	</form>
</div>