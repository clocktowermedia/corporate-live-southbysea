
	<div class="col-xs-12">
		<div class="text-center">
			<h1 class="mt-0"><?php echo $page['title'];?></h1>
			<p><?php echo $page['content'];?></p>
			<br/>
			<div class="col-lg-12">
				<div class="col-lg-3 col-md-3">
				</div>
				<div class="col-lg-3 col-md-3">
					<a href="/">
						<img class ="pull-left" src="/assets/images/corp-home.jpg"alt="">
					</a>
				</div>
				<div class="col-lg-4 col-md-4">
						<a href="https://www.promoplace.com/southbysea/apparel-and-accessories.htm">
							<img class ="pull-left" src="/assets/images/browse_products.jpg"alt="">
						</a>
				</div>
				<div class="col-lg-2 col-md-2">
				</div>
			</div>
		</div>

		<?php if (isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] != '' && ENVIRONMENT == 'production'): ?>

			<!--Pinterest Conversion Code -->
			<img height="1" width="1" style="display:none;" alt="" src="https://ct.pinterest.com/?tid=UYNGHh7SnLv&value=0.00&quantity=1"/>

			<!-- Google Code for Proof Request Conversion Page -->
			<script type="text/javascript">
				/* <![CDATA[ */
				var google_conversion_id = 934726891;
				var google_conversion_language = "en";
				var google_conversion_format = "3";
				var google_conversion_color = "ffffff";
				var google_conversion_label = "dmfECN3x7WMQ65nbvQM";
				var google_remarketing_only = false;
				/* ]]> */
			</script>
			<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
			<noscript>
				<div style="display:inline;">
					<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/934726891/?label=dmfECN3x7WMQ65nbvQM&amp;guid=ON&amp;script=0"/>
				</div>
			</noscript>

		<?php endif; ?>
	</div>
