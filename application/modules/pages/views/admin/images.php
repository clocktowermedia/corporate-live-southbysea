<div class="row">
	<div class="col-xs-12">
		
		<?php echo form_open_multipart('admin/pages/images', array('id' => 'image-form')); ?>
		<fieldset>
			<?php $this->load->view('../modules/'. $this->router->fetch_module() .'/partials/admin/images-form');?>
			<button class="btn btn-info" type="submit">Update Images</button>
		</fieldset>
		</form>

	</div>
</div>