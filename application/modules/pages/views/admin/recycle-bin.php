<div class="row">
	<div class="col-xs-12">
		<div class="box box-primary">
			<div class="box-header" data-toggle="tooltip" title="" data-original-title="Actions">
				<h3 class="box-title">Shortcuts:</h3>
				<div class="box-tools pull-right">
					<a href="/admin/pages"><button class="btn btn-info btn-sm"><i class="fa fa-arrow-left"></i> Back to Pages</button></a>
				</div>
			</div>

			<div class="box-body table-responsive no-pad-top">
				<hr class="no-pad-top">

				<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered datatable" width="100%">
					<thead>
						<tr>
							<th data-class="expand">Title</th>
							<th data-hide="phone">Page Url</th>
							<th data-hide="phone">Full Url</th>
							<th data-hide="phone, tablet">Date Deleted</th>
							<th data-hide="phone">Published</th>
							<th>Actions</th>
						</tr>
					</thead>

					<tbody>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>