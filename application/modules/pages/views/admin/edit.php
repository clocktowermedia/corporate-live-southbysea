<div class="row">
	<div class="col-xs-12">
		<?php echo form_open('admin/pages/edit/' . $page['pageID'], array('id' => 'page-form')); ?>
		<fieldset>
			<?php $this->load->view('../modules/'. $this->router->fetch_module() .'/partials/admin/form');?>
			<button class="btn btn-info save-draft" type="submit">Save Draft</button>
			<button class="btn btn-info" type="submit">Update Page & Publish</button>
		</fieldset>
		</form>
	</div>
</div>