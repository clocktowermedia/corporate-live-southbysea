<div class="row">
	<div class="col-xs-12">
		<?php echo form_open('admin/pages/create', array('id' => 'page-form')); ?>
		<fieldset>
			<?php $this->load->view('../modules/'. $this->router->fetch_module() .'/partials/admin/form');?>
			<button class="btn btn-info" type="submit">Create Page</button>
		</fieldset>
		</form>
	</div>
</div>