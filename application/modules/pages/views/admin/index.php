<div class="row">
	<div class="col-xs-12">
		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#tree-view" data-toggle="tab">Tree View</a></li>
				<li><a href="#flat-view" data-toggle="tab">Flat View</a></li>
				<li class="pull-right">
					<a href="/admin/pages/create" class="nav-tabs-custom-btn"><button class="btn btn-info btn-sm"><i class="fa fa-plus"></i> Add Page</button></a>
				</li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="tree-view">
					<p>Drag & Drop pages to re-order them. Re-ordering pages changes their URL accordingly. IE: A page with the URL "contact" that is moved under a page with the URL "about-us", would become "about-us/contact".<p>
					<p class="mb-20"><em>Please note that if you want to change the order of links in your site's main navigation or in the footer, this needs to be done in the <a href="/admin/menus">Menus</a> section.</em></p>
					
					<?php echo $pageHtml;?>
				</div>

				<div class="tab-pane" id="flat-view">
					<div class="table-responsive">
						<br/>
						<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered datatable" width="100%">
							<thead>
								<tr>
									<th data-class="expand">Title</th>
									<th data-hide="phone, tablet">Page Url</th>
									<th data-hide="phone">Full Url</th>
									<th data-hide="phone, tablet">Date Updated</th>
									<th data-hide="phone">Published</th>
									<th>Status</th>
									<th>Actions</th>
								</tr>
							</thead>

							<tbody>
							</tbody>
						</table>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>