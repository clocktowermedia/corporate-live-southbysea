<?php
	//old password field
	$this->form_builder->password('password', 'Password:', '', $containerArray, '', 'Must be between 8 to 40 characters in length, and include at least one number, one uppercase letter, and one lower case letter.', '', array('required' => 'required', 'id' => 'password', 'pattern' => '^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,40}$'));

	//new password field
	$this->form_builder->password('password2', 'Confirm Password:', '', $containerArray, '', '', '', array('required' => 'required', 'pattern' => '^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,40}$', 'data-validation-equalto' => '#password', 'data-validation-equalto-message' => 'Password fields must match.'));
