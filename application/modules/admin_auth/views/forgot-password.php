<div class="form-horizontal">
	<?php echo form_open('admin-auth/forgot-password'); ?>
	<fieldset>
		<div class="body bg-gray">
			<?php
				//create array for classes
				$containerArray = array('labelClass' => 'col-sm-2', 'containerClass' => 'col-sm-10');

				//email field
				$this->form_builder->email('email', 'Email:', '', $containerArray, '', '', '', array('required' => 'required'));
			?>
		</div>

		<div class="footer">
			<button type="submit" class="btn btn-info btn-block">Submit</button>
		</div>

	</fieldset>
	</form>
</div>
