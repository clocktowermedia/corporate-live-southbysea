<div class="form-horizontal">
	<?php echo form_open('admin-auth/set-password/' . $admin['adminID'] . '/' . $code); ?>
	<fieldset>
		<div class="body bg-gray">
			<?php 
				//set container array
				$data['containerArray'] = array('labelClass' => 'col-sm-5', 'containerClass' => 'col-sm-7');

				//load the form
				$this->load->view('../modules/'. $this->router->fetch_module() .'/partials/password-form', $data);
			?>
		</div>

		<div class="footer">
			<button type="submit" class="btn btn-info btn-block">Submit</button>
		</div>

	</fieldset>
	</form>
</div>
