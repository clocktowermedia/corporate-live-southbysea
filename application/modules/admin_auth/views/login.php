<div class="form-horizontal">
	<?php echo form_open('admin-auth/login'); ?>
	<fieldset>
		<div class="body bg-gray">
			<?php
				//create array for classes
				$containerArray = array('labelClass' => 'col-sm-3', 'containerClass' => 'col-sm-9');

				//email field
				$this->form_builder->email('email', 'Email:', $this->input->post('email', TRUE), $containerArray, '', '', '', array('required' => 'required'));

				// password field
				$this->form_builder->password('password', 'Password:', '', $containerArray, '', '', '', array('required' => 'required', 'id' => 'password'));
			?>
		</div>

		<div class="footer">
			<button type="submit" class="btn btn-info btn-block">Log Me In</button>
			<p><a href="/admin-auth/forgot-password">Forgot Password?</a></p>
		</div>

	</fieldset>
	</form>
</div>
