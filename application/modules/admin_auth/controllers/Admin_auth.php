<?php

class Admin_auth extends MY_Controller {

	//layout variables
	public $defaultLayout;
	public $emailLayout;

	//all actions should call this function
	function __construct()
	{
		parent::__construct();

		//set layout variable(s)
		//$this->defaultLayout = $this->get_admin_layout();
		$this->defaultLayout = $this->get_layout_path() . 'admin-small-form';
		$this->emailLayout = $this->get_email_layout();

		//load relevant models
		$this->load->model('admins/admin_model');
	}

	/**
	* Redirects them to the login page or to the admin section
	*/
	function index()
	{
		//redirect to main admin section if already logged in
		$this->_ifLoggedIn();

		//if not logged in, redirect to login page
		redirect('/admin-auth/login');
	}

	/**
	* Login action and view
	*/
	function login()
	{
		//redirect admin users who are already logged in
		$this->_ifLoggedIn();

		//tell it what view file to use
		$data['view'] = $this->get_view();
		$data['title'] = 'Log In';

		//set form validation rules
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'required');

		//if the form was submitted
		if ($this->is_post())
		{
				// try to log the user in
				$admin = true;

				//try to find the user
				//$admin = $this->admin_model->findByEmail($this->input->post('email', TRUE));
				$adminID = 1;

				// redirect if login failed
				if ((int) $adminID <= 0)
				{
					if ($admin['password'] == '')
					{
						//show error message and redirect
						$this->session->set_flashdata('error', 'You have not yet created a password for your account. Please check your email for a link to set your password.');
						redirect('/admin-auth/login');
					}

					//show error message and redirect
					$this->session->set_flashdata('error', 'Login Failed.');
					redirect('/admin-auth/login');
				}

				//get admin role information
				$this->load->model('admins/admin_role_model');
				$roleName = $this->admin_role_model->get($admin['roleID']);

				//set role name
				$admin['roleName'] = ($admin['roleID'] > 0 && isset($roleName['title']))? $roleName['title'] : '';
				$admin['roleName'] = ($admin['roleID'] <= 0)? 'Administrator' : $admin['roleName'];

				//add adminID and user's name to session
				$this->session->set_userdata('adminID', $adminID);
				$this->session->set_userdata('adminName', $admin['firstName'] . ' ' . $admin['lastName']);
				$this->session->set_userdata('adminDate', $admin['dateCreated']);
				$this->session->set_userdata('adminRole', $admin['roleName']);

				// redirect to admin dashboard
				redirect('/admin');
			}
		 else {
			// tell it what layout to use and load the page
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	* Logs the user out and redirects them to the homepage
	*/
	function logout()
	{
		$this->session->sess_destroy();
		redirect('/');
	}

	/**
	* View & Action for forgot password
	*/
	public function forgot_password()
	{
		//load relevant model
		$this->load->model('admins/admin_validation_model');

		//redirect users who are already logged in
		$this->_ifLoggedIn();

		//tell it what view file to use
		$data['view'] = $this->get_view();
		$data['title'] = 'Forgot Password?';

		//set form validation rules
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');

		//if the form was submitted
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the page and show form errors
				$this->load->view($this->defaultLayout, $data);
			} else {
				//find admin account, redirect if doesnt exist
				$admin = $this->admin_model->findByEmail($this->input->post('email', TRUE));

				if ($admin == false || $admin['status'] == 'banned')
				{
					//show error message and redirect
					$this->session->set_flashdata('error', 'There is no account associated with that email address.');
					redirect('/admin-auth/forgot-password');
				}

				//if account does exist, add to reset db
				$resetID = $this->admin_validation_model->insert(array('adminID' => $admin['adminID'], 'expires' => true));

				//get reset information from db
				$reset = $this->admin_validation_model->get($resetID);

				if ($reset != false)
				{
					//send reset email
					$this->_send_reset_email($admin, $reset);

					//display success message & redirect
					$this->session->set_flashdata('success', 'You should recieve an email shortly with instructions on how to reset your password.');
					redirect('/admin-auth/forgot-password');
				} else {
					//display error message & redirect
					$this->session->set_flashdata('error', 'An error occurred with your request, please try again.');
					redirect('/admin-auth/forgot-password');
				}
			}
		} else {
			// tell it what layout to use and load the page
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	* View & Action for reseting a password based on the link a user recieved in email after clicking forgot password
	*/
	public function reset_password($adminID = 0, $code = null)
	{
		//load relevant model
		$this->load->model('admins/admin_validation_model');

		//redirect if user is logged in
		$this->_ifLoggedIn();

		//set adminID to int
		$adminID = (int) $adminID;

		//get reset link information
		$link = $this->admin_validation_model->getResetLink($adminID, $code);

		//make sure that the link is valid
		if ((int)$link['dateExpires'] > 0)
		{
			//if an expiration is set, check the link and make sure its not expired, no expiration date for reset initiated by other admins
			$reset = $this->admin_validation_model->_resetLinkIsValid($adminID, $code);
		} else {
			//else just make sure the link exists
			$reset = $this->admin_validation_model->_linkIsValid($adminID, $code);
		}

		//redirect if link is invalid/not found
		if ($reset == false)
		{
			//show error message and redirect
			$this->session->set_flashdata('error', 'Invalid reset link.');
			redirect('/admin-auth/forgot-password');
		}

		//tell it what view file to use
		$data['view'] = $this->get_view();
		$data['title'] = 'Reset Password';

		//send info to view
		$data['admin'] = $this->admin_model->findByID($adminID);
		$data['code'] = $code;

		//set form validation rules
		$this->load->library('form_validation');
		$this->form_validation->set_rules('password', 'Password', 'required|strong_pw');
		$this->form_validation->set_rules('password2', 'Password Confirmation', 'required|strong_pw|matches[password]');

		//if the form was submitted
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the page and show form errors
				$this->load->view($this->defaultLayout, $data);
			} else {
				//update password in db
				$this->admin_model->resetPassword($adminID, $this->input->post('password', TRUE));

				//remove entry from reset table
				$this->admin_validation_model->deleteLink($adminID, $code);

				//display success message
				$this->session->set_flashdata('success', 'Your password has been reset. You may now login using the form below.');
				redirect('/admin-auth/login');
			}
		} else {
			// tell it what layout to use and load the page
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/**
	* View & Action for setting a password based on the link a user recieved in email after an account was created for them
	*/
	public function set_password($adminID = 0, $code = null)
	{
		//load relevant model
		$this->load->model('admins/admin_validation_model');

		//redirect if user is logged in
		$this->_ifLoggedIn();

		//set adminID to int
		$adminID = (int) $adminID;

		//make sure that the link is valid, redirect if link not found
		$set = $this->admin_validation_model->_linkIsValid($adminID, $code);

		if ($set == false)
		{
			//show error message and redirect
			$this->session->set_flashdata('error', 'Invalid password link.');
			redirect('/admin');
		}

		//tell it what view file to use
		$data['view'] = $this->get_view();
		$data['title'] = 'Set Password';

		//get info and send to view
		$data['admin'] = $this->admin_model->findByID($adminID);
		$data['code'] = $code;

		//set form validation rules
		$this->load->library('form_validation');
		$this->form_validation->set_rules('password', 'Password', 'required|strong_pw');
		$this->form_validation->set_rules('password2', 'Password Confirmation', 'required|strong_pw|matches[password]');

		//if the form was submitted
		if ($this->is_post())
		{
			//make sure form is valid
			if ($this->form_validation->run() == FALSE)
			{
				//reload the page and show form errors
				$this->load->view($this->defaultLayout, $data);
			} else {
				//update password in db & set status to enabled
				$this->admin_model->resetPassword($adminID, $this->input->post('password', TRUE));
				$this->admin_model->enable($adminID);

				//remove entry from reset table
				$this->admin_validation_model->deleteLink($adminID, $code);

				//display success message
				$this->session->set_flashdata('success', 'Your password has been set. You may now login using the form below.');
				redirect('/admin-auth/login');
			}
		} else {
			// tell it what layout to use and load the page
			$this->load->view($this->defaultLayout, $data);
		}
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

	public function _send_reset_email($admin = null, $reset = null)
	{
		//load email & parser library
		$this->load->library('email');
		$this->load->library('parser');
		$this->config->load('email');

		//get sending options from config
		$this->email->from($this->config->item('site_email'), $this->config->item('site_email_name'));

		//send to admin
		$this->email->to($admin['email'], $admin['firstName'] . ' ' . $admin['lastName']);

		//email subject
		$data['subject'] = $this->config->item('site_name') . ' Admin Account Password Reset';

		//build content
		$url = 'http://' . $_SERVER['HTTP_HOST'] . '/admin-auth/reset-password/' . $reset['adminID'] . '/' . $reset['code'];
		$text = '<p>Visit the URL below in your browser to reset your password:</p>';
		$text .= '<p>Link: <a href="'. $url .'">'. $url . '</a></p>';
		$data['text'] = $text;

		//format content into table, add css
		$data['content'] = $this->convert_text_to_html_email_table($data['subject'], $data['text']);

		$this->load->library('css_to_inline_styles');
		$this->css_to_inline_styles->setHtml($data['content']);
		$this->css_to_inline_styles->setCSS(file_get_contents(APPPATH . '../assets/css/email.css'));
		$data['html'] = $this->css_to_inline_styles->convert();

		//tell it what view to use, same name as function that initiated the email
		$data['view'] = $this->get_email_view();

		//get content from layout/view
		$content = $this->parser->parse($this->emailLayout, $data, true);

		//set subject & message
		$this->email->subject($data['subject']);
		$this->email->message($content);

		//send email
		$this->email->sbs_send();
	}

	/**
	* Returns true if a user is logged in
	*/
	public function _isLoggedIn()
	{
		if ((int) $this->session->userdata('adminID') > 0)
		{
			return true;
		}

		return false;
	}

	/**
	* Redirects a user to the account dashboard if they are already logged in, used to prevent viewing register page if already logged in, etc.
	*/
	public function _ifLoggedIn()
	{
		//check to see if the user is/was logged in
		$loggedIn = $this->_isLoggedIn();

		//redirect
		if ($loggedIn == true)
		{
			redirect('/admin');
		}
	}

	/**
	* Require a user to be logged in, redirect them if they are not
	* @parameter $message, show error message if true
	*/
	public function _requireLoggedIn($message = true)
	{
		//check to see if the user is logged in
		$loggedIn = $this->_isLoggedIn();

		if ($loggedIn == false)
		{
			//if we want to show a message, add it
			if ($message == true)
			{
				$this->session->set_flashdata('error', 'You need to log in to view this page or perform that action.');
			}

			//redirect
			redirect('/admin-auth/login');
		}
	}
}
