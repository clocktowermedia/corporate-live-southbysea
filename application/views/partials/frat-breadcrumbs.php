<?php $breadcrumbClass = (!isset($hideBreadcrumbs) || $hideBreadcrumbs == false)? '' : 'hidden-breadcrumb'; ?>

<ol class="breadcrumb <?php echo $breadcrumbClass;?>" id = "breadcrumb_id" style="background-color:#879FAA; opacity: 0.55; font-style: italic; font-family:Hello Stockholm Reg,Times New Roman, serif; color:white;">

	<?php if (($_SERVER[REQUEST_URI] === '/fraternity') || ($_SERVER[REQUEST_URI] === '/fraternity/')): ?>
	<?php else: ?>

	<?php if (!isset($hideBreadcrumbs) || $hideBreadcrumbs == false): ?>
		<li><a href="/"><span style ="color:white;">Home</span></a></li>

		<?php if ($this->router->fetch_method() != 'index'): ?>
			<?php if ($this->router->fetch_module() == 'products'): ?>
				<!--<li> Remove per request
					<a href="/<?php //echo $this->router->fetch_module();?>">
						<?php //echo ucwords($this->router->fetch_module());?>
					</a>
				</li>-->

				<?php if (isset($products)): ?>
					<li>
						<a href="/<?php echo $this->router->fetch_module();?>/<?php echo $category['parent']['url'];?>">
							<span style ="color:white;"><?php echo $category['parent']['title'];?></span>
						</a>
					</li>
				<?php endif; ?>

				<?php if (isset($product)): ?>
					<li>
						<a href="/<?php echo $this->router->fetch_module();?>/<?php echo $category['parent']['url'];?>">
							<span style ="color:white;"><?php echo $category['parent']['title'];?></span>
						</a>
					</li>
					<li>
						<a href="/<?php echo $this->router->fetch_module();?>/<?php echo $category['parent']['url'];?>/<?php echo $category['url'];?>">
							<span style ="color:white;"><?php echo $category['title'];?></span>
						</a>
					</li>
				<?php endif; ?>

			<?php elseif ($this->router->fetch_module() == 'designs'): ?>

				<li>
					<a href="/<?php echo $this->router->fetch_module();?>">
						<span style ="color:white;"><?php echo ucwords($this->router->fetch_module());?></span>
					</a>
				</li>

				<?php if (isset($design)): ?>
					<li>
						<a href="/<?php echo $this->router->fetch_module();?>/<?php echo $category['url'];?>">
							<span style ="color:white;"><?php echo $category['title'];?></span>
						</a>
					</li>
				<?php endif; ?>

			<?php elseif ($this->router->fetch_module() == 'pages'): ?>

				<?php if (isset($page['parents']) && count($page['parents']) > 0):?>
					<?php foreach ($page['parents'] as $parentPage): ?>
						<li>
							<a href="<?php echo $parentPage['urlPath']; ?>">
								<span style ="color:white;"><?php echo $parentPage['title'];?></span>
							</a>
						</li>
					<?php endforeach; ?>
				<?php endif; ?>

			<?php else: ?>
				<?php if (isset($breadcrumb)) :?>
					<?php echo $breadcrumb; ?>
				<?php else :?>
					<li>
						<a href="/<?php echo $this->router->fetch_module();?>">
							<span style ="color:white;"><?php echo ucwords(str_replace('_', ' ', $this->router->fetch_module()));?></span>
						</a>
					</li>
				<?php endif; ?>
			<?php endif;?>
		<?php endif;?>

		<li class="active">
      <span style ="color:white;">
			<?php if (isset($page)): ?>
				<?php echo $page['title']; ?>
			<?php elseif (isset($title)): ?>
				<?php echo $title; ?>
			<?php elseif (isset($pageTitle)): ?>
				<?php echo $pageTitle;?>
			<?php endif; ?>
    </span>
		</li>
	<?php endif; ?>
<?php endif; ?>

</ol>
<div class="frat-wrapper-breadcrub"></div>
