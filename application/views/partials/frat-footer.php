<!-- <footer id="mainFooter" style=""> -->
<div class="push clearfix"></div>
  <footer id="mainFooter" >
    <div style="background:#D3DAE0;">
        <div class="frat-wrapper-footer-lg hidden-md hidden-sm hidden-xs" style=""></div>
        <div class="frat-wrapper-footer-md hidden-lg hidden-sm hidden-xs" style=""></div>
        <div class="frat-wrapper-footer-sm hidden-lg hidden-md hidden-xs" style=""></div>
        <div class="frat-wrapper-footer-xs hidden-xs hidden-lg hidden-md hidden-sm" style=""></div>
  <div class="container">
    <div class="row">
      <div class="col-lg-4 col-md-4 hidden-sm hidden-xs"style="font-family:Northwest Bold; letter-spacing: 5px; margin-top: 10px; font-size:calc(12px + .8vw);">
        <br><p style="font-family:Northwest Bold;"><a style="text-decoration:none;"href="/fraternity/quote-request">PROOF REQUEST</a></p><br><br>
        <p style="font-family:Northwest Bold;"><a style="text-decoration:none;"href="/fraternity/order-form" >FINAL SUBMISSION</a></p><br><br>
        <p style="font-family:Northwest Bold;"><a style="text-decoration:none;"href="/fraternity/contact">CONTACT</a></p><br><br>
        <p style="font-family:Northwest Bold;"><a style="text-decoration:none;"href="/fraternity/faq">FAQ</a></p>
      </div>
      <div class="hidden-lg hidden-md col-sm-4 col-xs-4"style="font-family:Northwest Bold; letter-spacing: 2px; margin-top: 10px; font-size:calc(2px + 1.4vw);">
        <br><p style="font-family:Northwest Bold;"><a style="text-decoration:none;"href="/fraternity/quote-request">PROOF REQUEST</a></p><br>
        <p style="font-family:Northwest Bold;"><a style="text-decoration:none;"href="/fraternity/order-form" >FINAL SUBMISSION</a></p><br>
        <p style="font-family:Northwest Bold;"><a style="text-decoration:none;"href="/fraternity/contact">CONTACT</a></p><br>
        <p style="font-family:Northwest Bold;"><a style="text-decoration:none;"href="/fraternity/faq">FAQ</a></p><br class="hidden-xs">
      </div>

      <div class="col-lg-4 col-md-4 hidden-sm hidden-xs" align="center">
        <br><p style="font-family:Campground Regular; font-size:calc(10px + 2.2vw);
            white-space: nowrap;">Connect with Us</p><br class="hidden-sm hidden-xs">
        <span style="font-family:Northwest Bold; opacity: 0.8;letter-spacing: 2px; font-size:calc(4px + .55vw);">
        <div style="padding-top:2px;"><span style="opacity: 0.8;">209</span> DAYTON STREET</div>
        <div style="padding-top:1px;">SUITE <span style="opacity: 0.8;">205</span></div>
        <div style="padding-top:0px;">EDMONDS, WA<span style="opacity: 0.8;">98020</span></div>
        <div style="padding-top:20px;">888/855/7960</div>
        <div style="padding-top:20px;">SALES@SOUTHBYSEA.COM</div>
      </span>
        <ul class="social-nav hidden-md" style="padding-left:65px;padding-top:15px;">
            <li><a href="https://www.facebook.com/southbyseacollege" target="_blank"><img src="/assets/images/frat-fb-icon.png" alt="Facebook" title="South by Sea's Facebook" width="50px"heigth="50px"></a></li>
            <li><a href="http://www.pinterest.com/southbysea" target="_blank"><img src="/assets/images/frat-pin-icon.png" alt="Pinterest" title="South by Sea's Pinterest" width="50px"heigth="50px"></a></li>
            <li><a href="https://www.instagram.com/southbyseafraternity/" target="_blank"><img src="/assets/images/frat-insta-icon.png" alt="Instagram" title="South by Sea's Instagram" width="50px"heigth="50px"></a></li>
        </ul>
      </div>

      <div class="hidden-lg hidden-md col-sm-4 col-xs-4" align="center">
        <br><p style="font-family:Campground Regular; font-size:calc(10px + 2.2vw);
            white-space: nowrap;padding-top:10px;">Connect with Us</p><br class="hidden-sm hidden-xs">
        <span style="font-family:Northwest Bold; opacity: 0.8;letter-spacing: 2px; font-size:calc(4px + .55vw);">
        <div style="padding-top:2px;"><span style="opacity: 0.8;">209</span> DAYTON STREET</div>
        <div style="padding-top:1px;">SUITE <span style="opacity: 0.8;">205</span></div>
        <div style="padding-top:0px;">EDMONDS, WA<span style="opacity: 0.8;">98020</span></div>
        <div style="padding-top:10px;">888/855/7960</div>
        <div style="padding-top:10px;">INFO@SOUTHBYSEA.COM</div>
      </span>
      </div>

      <div class="col-lg-4 col-md-4 hidden-sm hidden-xs">
        <div class="pull-right" style= "width:calc(100px + 2.5vw);height:calc(100px + 2.5vw);">
          <img src="/assets/images/FL1.png" alt="Collegiate Licensed Product"><br>
          <img src="/assets/images/FL2.png" alt="Official Licensed Product">
        </div>
      </div>
      <div class="hidden-lg hidden-md col-sm-4 col-xs-4">
        <div class="pull-right" style= "width:calc(80px + 4vw);height:calc(80px + 4vw);">
          <img src="/assets/images/FL1.png" alt="Collegiate Licensed Product"><br>
          <img src="/assets/images/FL2.png" alt="Official Licensed Product">
        </div>
      </div>
    </div>
  </div>
</div>
</footer><!-- end footer -->
<div class="col-lg-12 col-md-12 col-sm-12" style = "overflow: hidden;
    white-space: nowrap; text-align: center; margin-top:8px; font-family:Northwest Regular,Times New Roman, serif;">
  <span style = "opacity: 0.55;letter-spacing: 4px; font-size:1vw !important; text-transform: uppercase;" id="copyright">&#169; <?php echo date('Y');?> <?php echo $this->config->item('site_name');?> &emsp; / &emsp; <a href="http://www.clocktowermedia.com" title="Seattle Web Design">Seattle Web Design</a> &emsp; / &emsp; <a syle= "text-decoration:none;" href="/fraternity/privacy-policy" target="_blank"> Privacy policy </a> &emsp; /  &emsp; <a syle= "text-decoration:none;" href="/fraternity/faq/uploading-content-terms-and-conditions" target="_blank"> Terms & Conditions </a></span><br><br>
</div>

<div class="hidden device-xs visible-xs"></div>
<div class="hidden device-sm visible-sm"></div>
<div class="hidden device-md visible-md"></div>
<div class="hidden device-lg visible-lg"></div>

<!-- JavaScript -->
<script src="/assets/js/site.js"></script>

<?php
if (file_exists(APPPATH . 'modules/'. $this->uri->segment(1) .'/partials/js.php'))
{
$this->load->view('../modules/'. $this->uri->segment(1) .'/partials/js');
}

?>

<?php if (ENVIRONMENT == 'production'): ?>
<script type="text/javascript">
  var __lc = {};
  __lc.license = 6005771;

  (function() {
    var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
    lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
  })();
</script>

<img height="1" width="1" class="hide" alt="" src="https://ct.pinterest.com/?tid=HzApQaenBx9"/>

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
  n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
  document,'script','//connect.facebook.net/en_US/fbevents.js');

  fbq('init', '1478653212465338');
  fbq('track', "PageView");
</script>
<noscript><img height="1" width="1" class="hide" src="https://www.facebook.com/tr?id=1478653212465338&ev=PageView&noscript=1"/></noscript>

<!-- Track Certain Pages on FB -->
<script>
  <?php if (isset($view) && $view == 'submission-thank-you'): ?>
    fbq('track', 'ViewContent');
  <?php endif; ?>
</script>

<!-- Drip -->
<script type="text/javascript">
  var _dcq = _dcq || [];
  var _dcs = _dcs || {};
  _dcs.account = '3100540';

  (function() {
    var dc = document.createElement('script');
    dc.type = 'text/javascript'; dc.async = true;
    dc.src = '//tag.getdrip.com/3100540.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(dc, s);
  })();
</script>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-74861541-1', 'auto');
  ga('send', 'pageview');
</script>
<?php endif; ?>

<?php $this->load->view('partials/meta-footer'); ?>
