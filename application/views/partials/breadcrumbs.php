<?php $breadcrumbClass = (!isset($hideBreadcrumbs) || $hideBreadcrumbs == false)? '' : 'hidden-breadcrumb'; ?>
<ol class="breadcrumb <?php echo $breadcrumbClass;?>" style="color:white !important;background-color:#b8d1d1;font-style: italic;font-family:Hello Stockholm Reg,Times New Roman, serif;">
	<?php if (!isset($hideBreadcrumbs) || $hideBreadcrumbs == false): ?>
		<li><a href="/">Home</a></li>

		<?php if ($this->router->fetch_method() != 'index'): ?>
			<?php if ($this->router->fetch_module() == 'products'): ?>
				<!--<li> Remove per request
					<a href="/<?php //echo $this->router->fetch_module();?>">
						<?php //echo ucwords($this->router->fetch_module());?>
					</a>
				</li>-->

				<?php if (isset($products)): ?>
					<li>
						<a href="/<?php echo $this->router->fetch_module();?>/<?php echo $category['parent']['url'];?>">
							<?php echo $category['parent']['title'];?>
						</a>
					</li>
				<?php endif; ?>

				<?php if (isset($product)): ?>
					<li>
						<a href="/<?php echo $this->router->fetch_module();?>/<?php echo $category['parent']['url'];?>">
							<?php echo $category['parent']['title'];?>
						</a>
					</li>
					<li>
						<a href="/<?php echo $this->router->fetch_module();?>/<?php echo $category['parent']['url'];?>/<?php echo $category['url'];?>">
							<?php echo $category['title'];?>
						</a>
					</li>
				<?php endif; ?>

			<?php elseif ($this->router->fetch_module() == 'designs'): ?>

				<li>
					<a href="/<?php echo $this->router->fetch_module();?>">
						<?php echo ucwords($this->router->fetch_module());?>
					</a>
				</li>

				<?php if (isset($design)): ?>
					<li>
						<a href="/<?php echo $this->router->fetch_module();?>/<?php echo $category['url'];?>">
							<?php echo $category['title'];?>
						</a>
					</li>
				<?php endif; ?>

			<?php elseif ($this->router->fetch_module() == 'pages'): ?>

				<?php if (isset($page['parents']) && count($page['parents']) > 0):?>
					<?php foreach ($page['parents'] as $parentPage): ?>
						<li>
							<a href="<?php echo $parentPage['urlPath']; ?>">
								<?php echo $parentPage['title'];?>
							</a>
						</li>
					<?php endforeach; ?>
				<?php endif; ?>

			<?php else: ?>
				<?php if (isset($breadcrumb)) :?>
					<?php echo $breadcrumb; ?>
				<?php else :?>
					<li>
						<a href="/<?php echo $this->router->fetch_module();?>">
							<?php echo ucwords(str_replace('_', ' ', $this->router->fetch_module()));?>
						</a>
					</li>
				<?php endif; ?>
			<?php endif;?>
		<?php endif;?>

		<li class="active">
			<?php if (isset($page)): ?>
				<?php echo $page['title']; ?>
			<?php elseif (isset($title)): ?>
				<?php echo $title; ?>
			<?php elseif (isset($pageTitle)): ?>
				<?php echo $pageTitle;?>
			<?php endif; ?>
		</li>
	<?php endif; ?>
</ol>
