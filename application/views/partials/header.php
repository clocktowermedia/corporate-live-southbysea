<?php $this->load->view('partials/meta-header'); ?>
<link rel="stylesheet" href="https://use.typekit.net/akn4ktw.css">

	<?php if ($site_notification != false && $site_notification['displayType'] === 'full' && $site_notification['content'] != ''): ?>
		<?php if ($site_notification['link'] != ''): ?><a class="no-underline" href="<?php echo $site_notification['link'];?>"><?php endif; ?>

			<img src="/assets/BannerImage/RC-1500.jpg" style= "width: 100%; min-height: 45px; max-height: 125px;"/>
				<?php if ($site_notification['link'] == ''): ?>
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<?php endif; ?>

		<?php if ($site_notification['link'] != ''): ?></a><?php endif; ?>
	<?php endif; ?>

<style>
	body    {
		font-family: "museo-sans";
	  font-size:11px;
		letter-spacing: 2px !important;
			/* opacity: 0.75; */
			/* font-size:100px; */
					}

	.nav>li>a:hover, .nav>li>a:focus {
		background-color: transparent;
	}

	.overlay {
	    height: 100%;
	    width: 0;
	    position: fixed;
	    z-index: 1;
	    top: 0;
	    left: 0;
	    background-color: rgb(0,0,0);
	    background-color: rgba(0,0,0, 0.9);
	    overflow-x: hidden;
	    transition: 0.5s;
	}

	.page-hader{
		margin:0px 0 0px !important;
	}

	.overlay-content {
	    position: relative;
	    top: 10%;
	    width: 100%;
	    text-align: center;
	    margin-top: 30px;
	}

	.overlay a {
	    padding: 8px;
	    text-decoration: none;
	    font-size: 24px;
	    color: #818181;
	    display: block;
	    transition: 0.3s;
	}

	.overlay a:hover, .overlay a:focus {
	    color: #f1f1f1;
	}

	.overlay .closebtn {
	    position: absolute;
	    top: 20px;
	    right: 45px;
	    font-size: 60px;
	}

	/* @media screen and (max-width: 767px){
		.page-header{
			background-color:#EDEBEC;
		}
		.navbar{
			background-color:#EDEBEC;
		}
	} */


	@media screen and (max-height: 450px) {
	  .overlay a {font-size: 20px}
	  .overlay .closebtn {
	    font-size: 40px;
	    top: 5px;
	    right: 35px;
	  }
	}
</style>

<script>
	function openNav() {
	    document.getElementById("myNav").style.width = "100%";
	}

	function closeNav() {
	    document.getElementById("myNav").style.width = "0%";
	}
</script>

	<!-- <div class="container"> -->
		<header>
			<div class="page-header" style="margin-top:0px !important;margin-bottom:0px !important; padding-top:20px; padding-bottom:130px;">

				<?php if ($site_notification != false && $site_notification['content'] != '' && $site_notification['displayType'] !== 'full'): ?>
					<?php if ($site_notification['link'] != ''): ?><a class="no-underline" href="<?php echo $site_notification['link'];?>"><?php endif; ?>
						<div class="alert alert-<?php echo $site_notification['color']; ?> alert-dismissible text-center">
							<?php if ($site_notification['link'] == ''): ?>
								<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<?php endif; ?>
							<?php echo $site_notification['content']; ?>
						</div>
					<?php if ($site_notification['link'] != ''): ?></a><?php endif; ?>
				<?php endif; ?>

				<div class="row" style="margin-top:0px;">
					<div class="col-lg-12" style = "white-space: nowrap;">



						<script language="javascript">
								function yourFunction(){
									var special = document.getElementById("unique-keywords").value;
								    var action_src = "http://www.promoplace.com/southbysea/:quicksearch.htm?quicksearchbox=" + special;
								    var your_form = document.getElementById('your_form');
								    your_form.action = action_src ;
								}
						</script>

						<ul class="top-nav pull-left hidden-xs" style = "">
							<form id = "your_form" onsubmit="yourFunction()">
         				<input type="text" id="unique-keywords" value="" placeholder="search"style="font-family:Hello Stockholm Reg,Times New Roman, serif;opacity: 0.55;outline: none; border-color: transparent; border: none; font-family:Helvetica Neue, FontAwesome" class="text-center">
							</form>
							<span class="underline"></span>
						</ul>


						<ul class="top-nav pull-left hidden-lg hidden-md hidden-sm" style = "margin-top:-20px;padding-left:10px;">
							<div class="inine" style="padding-top:20px;padding-left:10px;">
							<span style="opacity: 0.55;cursor:pointer;font-family:museo-sans;font-size:20px; text-transform:uppercase !important;" onclick="openNav()">&#9776; menu</span>
						</div>
						</ul>


						<ul class="top-nav pull-right hidden-xs" style="font-family:museo-sans;font-size:8px; text-transform:uppercase !important; opacity: 0.55;">
							<li><a href="https://www.southbysea.com/"><span style="font-size:11px; text-transform:uppercase !important;">SORORITY</span></a></li>
							<li><a href="https://www.southbysea.com/fraternity/"><span style="font-size:11px; text-transform:uppercase !important;">FRATERNITY</span></a></li>
						</ul>
						<!-- <ul class="top-nav pull-left hidden-lg hidden-md" style="font-family:Hello Stockholm Reg,Times New Roman, serif;font-style: italic;opacity: 0.55;margin-top:-32px;"> -->
							<!-- <li><a href="/catalog"><span style="font-size:calc(10px + .4vw);">catalog</span></a></li>
	            <li><a href="https://www.shopsouthbysea.com/blog"><span style="font-size:calc(10px + .4vw);">blog</span></a></li>
							<li><a href="https://www.shopsouthbysea.com"><span style="font-size:calc(10px + .4vw);">shop</span></a></li>
							<li><a href="/fraternity/"><span style="font-size:calc(10px + .4vw);">fraternity</span></a></li> -->
							<!-- <li><span style="color: #889FAA;font-size:30px;cursor:pointer;font-family:Playfair Display;font-size:calc(7px + .8vw);" onclick="openNav()">&#9776; menu</span></li> -->
						<!-- </ul> -->
					</div>
				</div>


				<div class="row hidden-xs" style="">
					<div class="col-lg-12" style="max-height:70px;">
						<h1 class="logo" style="margin-top:0px">
								<span><a href="/">
									<img style = "max-width:300px; position:absolute; left: 50%; margin-left: -150px; margin-top: -25px;" src="/assets/images/corp-logo-2.png">
								</a></span>
						</h1>
					</div>
				</div>

				<div class="row hidden-lg hidden-md hidden-sm" style="">
					<div class="col-lg-12" style="max-height:70px;">
						<h1 class="logo" style="margin-top:0px">
								<span><a href="/">
									<img style = "max-width: 225px; position:absolute; left: 50%; margin-left: -112px; margin-top: -20px;" src="/assets/images/corp-logo-2.png">
								</a></span>
						</h1>
					</div>
				</div>



			</div>
		</header>
		<nav class="navbar no-overflow hidden-xs" style= "padding-bottom:60px; font-family: museo-sans; border: 0; margin-bottom:0px;display: inline-line;margin-left: auto;margin-right: auto; letter-spacing: 5px; opacity: 1;" role="navigation">
        	<div class="navbar-wrpr">
						<ul id="main-nav" class="nav nav-pills" style="font-size:calc(3px + .8vw);"><li>
								<a href="https://www.promoplace.com/southbysea/apparel-and-accessories.htm"><span style="font-family:museo-sans !important; padding-left:25px;">APPAREL</span></a></li><li>
								<a href="http://www.promoplace.com/southbysea"><span style="font-family:museo-sans !important; padding-left:25px;">PRODUCTS</span></a></li><li>
								<a href="/quote-request"><span style="font-family:museo-sans !important; padding-left:25px;">QUOTE REQUEST</span></a></li><li>
								<a href="/gallery"><span style="font-family:museo-sans !important; padding-left:25px;">GALLERY</span></a></li><li>
								<a href="/contact"><span style="font-family:museo-sans !important; padding-left:25px;">CONTACT</span></a></li><li>
            </div>
		</nav>
		<div id="myNav" class="overlay">
		  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
		  <div class="overlay-content" style="font-family:Cache; ">
				<a href="https://www.promoplace.com/southbysea/apparel-and-accessories.htm">APPAREL</a><br>
				<a href="http://www.promoplace.com/southbysea">PRODUCTS</a><br>
				<a href="/quote-request">QUOTE REQUEST</a><br>
				<a href="/gallery">GALLERY</span></a><br>
				<a href="/contact">CONTACT</a><br><br>
				<a href="southbysea.com">SORORITY</a><br>
				<a href="southbysea.com/fraternity/"><span style = "">FRATERNITY</span></a><br><br>
		  </div>
		</div>
