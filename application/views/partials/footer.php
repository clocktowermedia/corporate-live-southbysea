<!-- <footer id="mainFooter" style=""> -->
  <footer id="mainFooter" >
    <?php if (strpos($_SERVER[REQUEST_URI], 'quote-request') || strpos($_SERVER[REQUEST_URI], 'order-form')): ?>

    <div style="background-color:#575859; color:white;" align="center">
      <div style="padding-top:40px;padding-bottom:20px; font-family:CACHE; font-size:calc(12px + .85vw);">Don't know where to start? Overwhelmed? Reach out!</div>
      <div style="padding-bottom:40px; font-family:museo-sans; font-size:calc(8px + .85vw); opacity: 0.6;">(888) 855 - 7960 / SALES@SOUTHBYSEA.COM</div>
    </div>

  <?php endif; ?>
  <div style="background:#F7F7F7;">
  <div class="container" style="margin-top:0px;">
    <div class="row" style="margin-bottom:0px;padding-top:40px;padding-bottom:40px;" align="center">
      <p style="white-space: nowrap; font-family:CACHE; font-size:calc(7px + .9vw);letter-spacing: 8px;">CONNECT WITH US</p>
    </div>
    <div class="hidden-lg hidden-md hidden-sm hidden-xs" style="margin-top:0px;margin-bottom:0px;padding-left:0px;" align="left">
      <span style="font-family:museo-sans;letter-spacing: 5px; font-size:calc(5px + .55vw);">
        <div style="padding-top:5px;opacity: 0.45;">209 DAYTON STREET</div>
        <div style="padding-top:5px;opacity: 0.45;">SUITE 205</div>
        <div style="padding-top:5px;opacity: 0.45;">EDMONDS, WA 98020</div>
        <div style="padding-top:5px;opacity: 0.45;">888/855/7960</div>
        <div style="padding-top:5px; padding-bottom:20px;opacity: 0.45;">SALES@SOUTHBYSEA.COM</div>
      </span>
    </div>
    <div class="row" style="margin-top:0px;">
      <div class="col-lg-4 col-md-4 col-sm-4 hidden-xs"style="font-family:museo-sans;opacity: 0.65;font-size:calc(5px + .8vw); letter-spacing: 5px; margin-top: 0px;padding-top:10px;">
        <p><a syle="text-decoration:none; font-family:museo-sans;"href="/order-form" ><span style="font-family:museo-sans;font-size:calc(5px + .8vw);">ORDER FORM</span></a></p><br><br>
        <p><a syle="text-decoration:none; font-family:museo-sans;"href="/contact"><span style="font-family:museo-sans;font-size:calc(5px + .8vw);">CONTACT</span></a></p><br><br>
        <p><a syle="text-decoration:none; font-family:museo-sans;"href="/faq"><span style="font-family:museo-sans;font-size:calc(5px + .8vw);">FAQ</span></a></p><br>
      </div>
      <div class="hidden-lg hidden-md hidden-sm col-xs-6"style="font-family:museo-sans;opacity: 0.65;font-size:calc(5px + .8vw); letter-spacing: 5px; margin-top: 0px;padding-top:10px;">
        <p><a syle="text-decoration:none; font-family:museo-sans;"href="/order-form" ><span style="font-family:museo-sans;font-size:calc(5px + .8vw);">ORDER FORM</span></a></p><br><br><br>
        <p><a syle="text-decoration:none; font-family:museo-sans;"href="/contact"><span style="font-family:museo-sans;font-size:calc(5px + .8vw);">CONTACT</span></a></p><br><br><br>
        <p><a syle="text-decoration:none; font-family:museo-sans;"href="/faq"><span style="font-family:museo-sans;font-size:calc(5px + .8vw);">FAQ</span></a></p><br>
      </div>
      <!-- <div class="hidden-lg hidden-md col-sm-4 col-xs-4"style="font-family:museo-sans;opacity: 0.65;font-size:calc(5px + .6vw); letter-spacing: 2px; margin-top: 10px;">
        <br><p><a syle="text-decoration:none;"href="/order-form" >ORDER FORM</a></p><br><br>
        <p><a syle="text-decoration:none;"href="/contact">CONTACT</a></p><br><br>
        <p><a syle="text-decoration:none;"href="/faq">FAQ</a></p><br class="hidden-xs">
      </div> -->



      <div class="col-lg-4 col-md-4 hidden-sm hidden-xs" align="center">
        <span style="font-family:museo-sans;letter-spacing: 5px; font-size:calc(5px + .55vw);">
        <div style="padding-top:10px;opacity: 0.45;">209 DAYTON STREET</div>
        <div style="padding-top:20px;opacity: 0.45;">SUITE 205</div>
        <div style="padding-top:20px;opacity: 0.45;">EDMONDS, WA 98020</div>
        <div style="padding-top:20px;opacity: 0.45;">888/855/7960</div>
        <div style="padding-top:20px;opacity: 0.45;">SALES@SOUTHBYSEA.COM</div>
      </span>
      </div>

      <div class="hidden-lg hidden-md col-sm-4 hidden-xs padding-top:20px;" align="center">
        <span style="font-family:museo-sans;letter-spacing: 5px; font-size:calc(3px + .4vw);">
          <div style="padding-top:10px;opacity: 0.45;">209 DAYTON STREET</div>
          <div style="padding-top:20px;opacity: 0.45;">SUITE 205</div>
          <div style="padding-top:20px;opacity: 0.45;">EDMONDS, WA 98020</div>
          <div style="padding-top:20px;opacity: 0.45;">888/855/7960</div>
          <div style="padding-top:20px;opacity: 0.45;">SALES@SOUTHBYSEA.COM</div>
      </span>
      </div>

      <div class="col-lg-4 col-md-4 hidden-sm hidden-xs"style="font-family:museo-sans;opacity: 0.65; margin-top: 0px;padding-top:8px;">
        <div class="pull-right" style="padding-bottom:30px;">
          <span style="font-family:museo-sans;padding: 0px 20px 0px 0px;font-size:calc(5px + .8vw); opacity:0.75;">  FACEBOOK</span><img src="/assets/images/icons/corp-facebook.png" style = "display:inline-block; vertical-align:middle;" width="40px;">
        </div>
        </br></br></br></br></br>
        <div class="pull-right" style="padding-bottom:28px;">
          <span style="font-family:museo-sans;padding: 0px 20px 0px 0px;font-size:calc(5px + .8vw); opacity:0.75;">  INSTAGRAM</span><img src="/assets/images/icons/corp-instagram.png" style = "display:inline-block; vertical-align:middle;" width="40px;">
        </div>
        </br></br></br></br></br>
        <div class="pull-right">
          <span style="font-family:museo-sans;padding: 0px 20px 0px 0px;font-size:calc(5px + .8vw); opacity:0.75;">  PINTEREST</span><img src="/assets/images/icons/corp-pin.png" style = "display:inline-block; vertical-align:middle;" width="40px;">
        </div>
        </br>
      </div>

      <div class="hidden-lg hidden-md col-sm-4 col-xs-6"style="font-family:museo-sans;opacity: 0.65; margin-top: 0px;padding-top:8px;">
        <div class="pull-right" style="padding-bottom:18px;">
          <span style="font-family:museo-sans;padding: 0px 20px 0px 0px;font-size:calc(5px + .8vw); opacity:0.75;">  FACEBOOK</span><img src="/assets/images/icons/corp-facebook.png" style = "display:inline-block; vertical-align:middle;" width="40px;">
        </div>
      </br><br><br></br>
        <div class="pull-right" style="padding-bottom:18px;">
          <span style="font-family:museo-sans;padding: 0px 20px 0px 0px;font-size:calc(5px + .8vw); opacity:0.75;">  INSTAGRAM</span><img src="/assets/images/icons/corp-instagram.png" style = "display:inline-block; vertical-align:middle;" width="40px;">
        </div>
        </br><br><br></br>
        <div class="pull-right">
          <span style="font-family:museo-sans;padding: 0px 20px 0px 0px;font-size:calc(5px + .8vw); opacity:0.75;">  PINTEREST</span><img src="/assets/images/icons/corp-pin.png" style = "display:inline-block; vertical-align:middle;" width="40px;">
        </div>
        </br>
      </div>


    </div>
  </div>
</div>
</footer><!-- end footer -->
<div class="col-lg-12 col-md-12 col-sm-12" style = "overflow: hidden;
    white-space: nowrap; text-align: center; margin-top:8px;">
  <span style = "font-family:museo-sans;opacity: 0.55;letter-spacing: 4px; font-size:.7vw !important;" id="copyright">&#169; <?php echo date('Y');?> <?php echo $this->config->item('site_name');?> &emsp; / &emsp; <a href="http://www.clocktowermedia.com" title="Seattle Web Design">Seattle Web Design</a> &emsp; / &emsp; <a syle= "text-decoration:none;" href="/privacy-policy" target="_blank"> Privacy policy </a> &emsp; /  &emsp; <a syle= "text-decoration:none;" href="/faq/uploading-content-terms-and-conditions" target="_blank"> Terms & Conditions </a></span><br><br>
</div>


<div class="device-xs visible-xs"></div>
<div class="device-sm visible-sm"></div>
<div class="device-md visible-md"></div>
<div class="device-lg visible-lg"></div>

<!-- JavaScript -->
<script src="/assets/js/site.js"></script>

<?php
if (file_exists(APPPATH . 'modules/'. $this->uri->segment(1) .'/partials/js.php'))
{
$this->load->view('../modules/'. $this->uri->segment(1) .'/partials/js');
}

?>

<?php if (ENVIRONMENT == 'production'): ?>
<script type="text/javascript">
  var __lc = {};
  __lc.license = 6005771;

  (function() {
    var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
    lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
  })();
</script>

<img height="1" width="1" class="hide" alt="" src="https://ct.pinterest.com/?tid=HzApQaenBx9"/>

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
  n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
  document,'script','//connect.facebook.net/en_US/fbevents.js');

  fbq('init', '1478653212465338');
  fbq('track', "PageView");
</script>
<noscript><img height="1" width="1" class="hide" src="https://www.facebook.com/tr?id=1478653212465338&ev=PageView&noscript=1"/></noscript>

<!-- Track Certain Pages on FB -->
<script>
  <?php if (isset($view) && $view == 'submission-thank-you'): ?>
    fbq('track', 'ViewContent');
  <?php endif; ?>
</script>

<!-- <script type="text/javascript" src="//downloads.mailchimp.com/js/signup-forms/popup/embed.js" data-dojo-config="usePlainJson: true, isDebug: false"></script><script type="text/javascript"*>require(["mojo/signup-forms/Loader"], function(L) { L.start({"baseUrl":"mc.us11.list-manage.com","uuid":"639dad0240be2948a8c973d07","lid":"c8db9fd367"}) })</script> -->

<!-- Drip -->
<script type="text/javascript">
  var _dcq = _dcq || [];
  var _dcs = _dcs || {};
  _dcs.account = '3100540';

  (function() {
    var dc = document.createElement('script');
    dc.type = 'text/javascript'; dc.async = true;
    dc.src = '//tag.getdrip.com/3100540.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(dc, s);
  })();
</script>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-74861541-1', 'auto');
  ga('send', 'pageview');
</script>
<?php endif; ?>

<?php $this->load->view('partials/meta-footer'); ?>
