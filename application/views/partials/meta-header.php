<!DOCTYPE html>
<!--[if lt IE 7]>	<html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>		<html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>		<html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]>	<!--> <html class="no-js" lang="en"> <!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>
		<?php
			if (isset($page))
			{
				echo $page['title'] . ' | ' . $this->config->item('site_name');
			} else if (isset($pageTitle)) {
				echo $pageTitle  . ' | ' . $this->config->item('site_name');
			} else if (isset($title)) {
				echo $title  . ' | ' . $this->config->item('site_name');
			} else {
				echo $this->config->item('site_name');
			}
		?>
	</title>

	<!-- Meta Tags -->
	<meta name="author" content="<?php echo $this->config->item('site_name'); ?> | College" />
	<meta name="description" content="<?php if (isset($page)) {echo $page['metaDesc'];} ?> <?php if (isset($metaDesc)) {echo $metaDesc;} ?>" />
	<meta name="keywords" content="<?php if (isset($page)) {echo $page['metaKeywords'];} ?> <?php if (isset($metaKeywords)) {echo $metaKeywords;} ?>" />
	<meta name="p:domain_verify" content="43e9bb82e0872d102fc3ba6947593e24"/>

	<?php if ($view == 'design'): ?>
		<?php if (isset($design['design_image']['fullpath']) && $design['design_image']['fullpath'] != ''): ?>
			<meta property="og:image" content="http://<?php echo $_SERVER['HTTP_HOST'] . $design['design_image']['fullpath']; ?>" />
		<?php endif; ?>
		<!--<meta property="og:title" content="ShareThis Homepage" />-->
		<!--<meta property="og:site_name" content="ShareThis" />-->
		<!--<meta property="og:description" content="Sharing is great!" />-->
		<!--<meta property="og:url" content="http://sharethis.com" />-->
		<!--<meta property="og:type" content="Sharing Widgets" />-->
	<?php endif; ?>

	<!-- Modernizr & Script Loader -->
	<script src="/assets/js/modernizr-2.5.3.js"></script>
	<script src="/assets/js/head.load.min.js"></script>

	<!-- Common/Shared CSS -->
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="/assets/css/plugins.css">
	<link rel="stylesheet" href="/assets/css/style.css">

	<!-- Site Specific CSS -->
	<link rel="stylesheet" href="/assets/css/site.css">

	<!-- Module CSS -->
	<?php
		if (file_exists(APPPATH . 'modules/'. $this->router->fetch_module() .'/partials/css.php'))
		{
			$this->load->view('../modules/'. $this->router->fetch_module() .'/partials/css');
		}
	?>

	<!-- Favicon -->
	<link rel="icon" type="image/x-icon" sizes="16x16" href="/favicon.ico">

	<!-- For Chrome for Android: -->
	<link rel="icon" sizes="192x192" href="/assets/images/icons/touch-icon-192x192.png">
	<!-- For iPhone 6 Plus with @3× display: -->
	<link rel="apple-touch-icon-precomposed" sizes="180x180" href="/assets/images/icons/apple-touch-icon-180x180-precomposed.png">
	<!-- For iPad with @2× display running iOS ≥ 7: -->
	<link rel="apple-touch-icon-precomposed" sizes="152x152" href="/assets/images/icons/apple-touch-icon-152x152-precomposed.png">
	<!-- For iPad with @2× display running iOS ≤ 6: -->
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/assets/images/icons/apple-touch-icon-144x144-precomposed.png">
	<!-- For iPhone with @2× display running iOS ≥ 7: -->
	<link rel="apple-touch-icon-precomposed" sizes="120x120" href="/assets/images/icons/apple-touch-icon-120x120-precomposed.png">
	<!-- For iPhone with @2× display running iOS ≤ 6: -->
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/assets/images/icons/apple-touch-icon-114x114-precomposed.png">
	<!-- For the iPad mini and the first- and second-generation iPad (@1× display) on iOS ≥ 7: -->
	<link rel="apple-touch-icon-precomposed" sizes="76x76" href="/assets/images/icons/apple-touch-icon-76x76-precomposed.png">
	<!-- For the iPad mini and the first- and second-generation iPad (@1× display) on iOS ≤ 6: -->
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/assets/images/icons/apple-touch-icon-72x72-precomposed.png">
	<!-- For non-Retina iPhone, iPod Touch, and Android 2.1+ devices: -->
	<link rel="apple-touch-icon-precomposed" href="/assets/images/icons/apple-touch-icon-precomposed.png"><!-- 57×57px -->
	<link rel="shortcut icon" sizes="196x196" href="/assets/images/icons/touch-icon-196x196.png">
	<link rel="shortcut icon" href="/assets/images/icons/apple-touch-icon.png">
	<!-- Tile icon for Win8 (144x144 + tile color) -->
	<meta name="msapplication-TileImage" content="/assets/images/icons/apple-touch-icon-144x144-precomposed.png">
	<meta name="msapplication-TileColor" content="#ffffff">

</head>

<body class="<?php echo $this->uri->segment(1) . ' ' . $this->router->fetch_module() . ' ' . $this->router->fetch_class() . ' ' . $this->router->fetch_method() . '-pagetype'; if(isset($bodyClass)) { echo ' ' . $bodyClass; } if(isset($page)) { echo ' page-' . $page['pageID']; } ?>">
