<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <title>TITLE</title>

    <!-- Facebook sharing information tags -->
    <meta property="og:title" content="SUBJECT" />
</head>
<body alink="#114eb1" link="#114eb1" text="#333333" yahoo="fix">

	<!-- PAGE WRAPPER -->
    <div id="body_style">

        <!-- Wrapper/Container Table: Use a wrapper table to control the width and the background color consistently of your email. Use this approach instead of setting attributes on the body tag. -->
        <table cellpadding="0" cellspacing="0" border="0" align="center" id="spacer-table">
            <tr>
                <td>
                	<!--[if gte mso 9]>
						<v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:600px;">
						<v:fill type="tile" src="http://marissa.south-by-sea.dev.myclocktower.com/assets/images/bg.jpg" color="#E8E8E8" />
						<v:textbox style="mso-fit-shape-to-text:true" inset="0,0,0,0">
					<![endif]-->