<?php $section = $this->router->fetch_module();?>

<section class="sidebar">

	<!-- sidebar menu: : style can be found in sidebar.less -->
	<ul class="sidebar-menu">
		<!-- <li>
			<a href="/admin">
				<i class="fa fa-dashboard"></i> <span>Dashboard</span>
			</a>
		</li> -->
		<?php if (
			(!isset($_SESSION['adminPerms']['sections']['pages']) && !isset($_SESSION['adminPerms']['actions']['pages'])) ||
			(isset($_SESSION['adminPerms']['sections']['pages']) && $_SESSION['adminPerms']['sections']['pages'] == true) ||
			(isset($_SESSION['adminPerms']['actions']['pages']) && array_search(true, $_SESSION['adminPerms']['actions']['pages']) != false)
		):?>
		<li class="treeview <?php echo ($section == 'pages')? 'active' : ''; ?>">
			<a href="#">
				<i class="fa fa-file-o"></i>
				<span>Pages</span>
				<i class="fa fa-angle-left pull-right"></i>
			</a>
			<ul class="treeview-menu">
				<li><a href="/admin/pages/edit/20"><i class="fa fa-angle-double-right"></i> View FAQ</a></li>
				<li><a href="/admin/pages/edit/47"><i class="fa fa-angle-double-right"></i> Gallery</a></li>
			</ul>
		</li>
		<?php endif;?>

		<?php if (
			(!isset($_SESSION['adminPerms']['sections']['menus']) && !isset($_SESSION['adminPerms']['actions']['menus'])) ||
			(isset($_SESSION['adminPerms']['sections']['menus']) && $_SESSION['adminPerms']['sections']['menus'] == true) ||
			(isset($_SESSION['adminPerms']['actions']['menus']) && array_search(true, $_SESSION['adminPerms']['actions']['menus']) != false)
		):?>
		<li class="treeview <?php echo ($section == 'menus')? 'active' : ''; ?>">
			<a href="#">
				<i class="fa fa-list"></i>
				<span>Menus</span>
				<i class="fa fa-angle-left pull-right"></i>
			</a>
			<ul class="treeview-menu">
				<li><a href="/admin/menus"><i class="fa fa-angle-double-right"></i> View Menus</a></li>
				<li><a href="/admin/menus/create"><i class="fa fa-angle-double-right"></i> Create Menu</a></li>
				<li><a href="/admin/menus/edit-modules"><i class="fa fa-angle-double-right"></i> Add/Edit Menu Pages</a></li>
			</ul>
		</li>
		<?php endif;?>

		<?php if (
			(!isset($_SESSION['adminPerms']['sections']['products']) && !isset($_SESSION['adminPerms']['actions']['products'])) ||
			(isset($_SESSION['adminPerms']['sections']['products']) && $_SESSION['adminPerms']['sections']['products'] == true) ||
			(isset($_SESSION['adminPerms']['actions']['products']) && array_search(true, $_SESSION['adminPerms']['actions']['products']) != false)
		):?>
		<!-- <li class="treeview <?php echo ($section == 'products')? 'active' : ''; ?>">
			<a href="#">
				<i class="fa fa-tag"></i> <span>Products</span>
				<i class="fa fa-angle-left pull-right"></i>
			</a>
			<ul class="treeview-menu">
				<li><a href="/admin/products"><i class="fa fa-angle-double-right"></i> View Products</a></li>
				<li><a href="/admin/products/create"><i class="fa fa-angle-double-right"></i> Add Product</a></li>
				<li><a href="/admin/products/categories"><i class="fa fa-angle-double-right"></i> Product Categories</a></li>
				<li><a href="/admin/products/categories/create"><i class="fa fa-angle-double-right"></i> Add Product Category</a></li>
				<li><a href="/admin/products/categories/reorder"><i class="fa fa-angle-double-right"></i> Reorder Categories</a></li>
			</ul>
		</li> -->
		<?php endif;?>

		<?php if (
			(!isset($_SESSION['adminPerms']['sections']['designs']) && !isset($_SESSION['adminPerms']['actions']['designs'])) ||
			(isset($_SESSION['adminPerms']['sections']['designs']) && $_SESSION['adminPerms']['sections']['designs'] == true) ||
			(isset($_SESSION['adminPerms']['actions']['designs']) && array_search(true, $_SESSION['adminPerms']['actions']['designs']) != false)
		):?>
		<!-- <li class="treeview <?php echo ($section == 'designs')? 'active' : ''; ?>">
			<a href="#">
				<i class="fa fa-image"></i> <span>Designs</span>
				<i class="fa fa-angle-left pull-right"></i>
			</a>
			<ul class="treeview-menu">
				<li><a href="/admin/designs"><i class="fa fa-angle-double-right"></i> View Designs</a></li>
				<li><a href="/admin/designs/create"><i class="fa fa-angle-double-right"></i> Add Design</a></li>
				<li><a href="/admin/designs/categories"><i class="fa fa-angle-double-right"></i> Design Categories</a></li>
				<li><a href="/admin/designs/categories/create"><i class="fa fa-angle-double-right"></i> Add Design Category</a></li>
				<li><a href="/admin/designs/categories/reorder"><i class="fa fa-angle-double-right"></i> Reorder Categories</a></li>
			</ul>
		</li> -->
		<?php endif;?>

		<?php if (
			(!isset($_SESSION['adminPerms']['sections']['campus_managers']) && !isset($_SESSION['adminPerms']['actions']['campus_managers'])) ||
			(isset($_SESSION['adminPerms']['sections']['campus_managers']) && $_SESSION['adminPerms']['sections']['campus_managers'] == true) ||
			(isset($_SESSION['adminPerms']['actions']['campus_managers']) && array_search(true, $_SESSION['adminPerms']['actions']['campus_managers']) != false)
		):?>
		<!-- <li class="treeview <?php echo ($section == 'campus_managers')? 'active' : ''; ?>">
			<a href="#">
				<i class="fa fa-institution"></i>
				<span>Campus Managers</span>
				<i class="fa fa-angle-left pull-right"></i>
			</a>
			<ul class="treeview-menu">
				<li><a href="/admin/campus-managers"><i class="fa fa-angle-double-right"></i> View Campus Managers</a></li>
				<li><a href="/admin/campus-managers/create"><i class="fa fa-angle-double-right"></i> Add Campus Manager</a></li>
				<li><a href="/admin/campus-managers/schools"><i class="fa fa-angle-double-right"></i> Edit Campus Names</a></li>
			</ul>
		</li> -->
		<?php endif;?>

		<?php if (
			(!isset($_SESSION['adminPerms']['sections']['admins']) && !isset($_SESSION['adminPerms']['actions']['admins'])) ||
			(isset($_SESSION['adminPerms']['sections']['admins']) && $_SESSION['adminPerms']['sections']['admins'] == true) ||
			(isset($_SESSION['adminPerms']['actions']['admins']) && array_search(true, $_SESSION['adminPerms']['actions']['admins']) != false)
		):?>
		<li class="treeview <?php echo ($section == 'admins')? 'active' : ''; ?>">
			<a href="#">
				<i class="fa fa-user"></i> <span>Admin Accounts</span>
				<i class="fa fa-angle-left pull-right"></i>
			</a>
			<ul class="treeview-menu">
				<li><a href="/admin/admins"><i class="fa fa-angle-double-right"></i> View Admins</a></li>
				<li><a href="/admin/admins/create"><i class="fa fa-angle-double-right"></i> Create Admin Account</a></li>
			</ul>
		</li>
		<?php endif;?>

		<?php if (ENVIRONMENT !== 'production'): ?>
			<?php if (
				(!isset($_SESSION['adminPerms']['sections']['potentials']) && !isset($_SESSION['adminPerms']['actions']['potentials'])) ||
				(isset($_SESSION['adminPerms']['sections']['potentials']) && $_SESSION['adminPerms']['sections']['potentials'] == true) ||
				(isset($_SESSION['adminPerms']['actions']['potentials']) && array_search(true, $_SESSION['adminPerms']['actions']['potentials']) != false)
			):?>
			<!-- <li class="treeview <?php echo ($section == 'potentials')? 'active' : ''; ?>">
				<a href="#">
					<i class="fa fa-money"></i> <span>Potentials</span>
					<i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="treeview-menu">
					<li><a href="/admin/potentials"><i class="fa fa-angle-double-right"></i> View Potentials</a></li>
					<li><a href="/admin/potentials/create"><i class="fa fa-angle-double-right"></i> Create Potential</a></li>
					<li><a href="/admin/potentials/reporting/generate"><i class="fa fa-angle-double-right"></i> Reporting</a></li>
					<li><a href="/admin/potentials/reporting"><i class="fa fa-angle-double-right"></i> Modify Reports</a></li>
				</ul>
			</li> -->
			<?php endif;?>
		<?php endif; ?>

		<?php if (
			(!isset($_SESSION['adminPerms']['sections']['ecommerce']) && !isset($_SESSION['adminPerms']['actions']['ecommerce'])) ||
			(isset($_SESSION['adminPerms']['sections']['ecommerce']) && $_SESSION['adminPerms']['sections']['ecommerce'] == true) ||
			(isset($_SESSION['adminPerms']['actions']['ecommerce']) && array_search(true, $_SESSION['adminPerms']['actions']['ecommerce']) != false)
		):?>
		<!-- <li class="treeview <?php echo ($section == 'ecommerce')? 'active' : ''; ?>">
			<a href="#">
				<i class="fa fa-shopping-cart"></i> <span>SeaPort Stores</span>
				<i class="fa fa-angle-left pull-right"></i>
			</a>
			<ul class="treeview-menu">
				<li><a href="/admin/ecommerce"><i class="fa fa-angle-double-right"></i> View Stores</a></li>
				<li><a href="/admin/ecommerce/create"><i class="fa fa-angle-double-right"></i> Create Store</a></li>
				<li><a href="/admin/ecommerce/closing"><i class="fa fa-angle-double-right"></i> Closing Soon</a></li>
			</ul>
		</li> -->
		<?php endif;?>

		<?php if (
			(!isset($_SESSION['adminPerms']['sections']['forms']) && !isset($_SESSION['adminPerms']['actions']['forms'])) ||
			(isset($_SESSION['adminPerms']['sections']['forms']) && $_SESSION['adminPerms']['sections']['forms'] == true) ||
			(isset($_SESSION['adminPerms']['actions']['forms']) && array_search(true, $_SESSION['adminPerms']['actions']['forms']) != false)
		):?>
		<li>
			<a href="/admin/forms">
				<i class="fa fa-envelope"></i> <span>Form Submissions</span>
				<small class="badge pull-right bg-yellow" id="unread-message-notification"></small>
			</a>
		</li>
		<?php endif;?>

		<?php if (
			(!isset($_SESSION['adminPerms']['sections']['settings']) && !isset($_SESSION['adminPerms']['actions']['settings'])) ||
			(isset($_SESSION['adminPerms']['sections']['settings']) && $_SESSION['adminPerms']['sections']['settings'] == true) ||
			(isset($_SESSION['adminPerms']['actions']['settings']) && array_search(true, $_SESSION['adminPerms']['actions']['settings']) != false)
		):?>
		<li class="treeview <?php echo ($section == 'forms')? 'active' : ''; ?>">
			<a href="#">
				<i class="fa fa-cog"></i> <span>Settings</span>
				<i class="fa fa-angle-left pull-right"></i>
			</a>
			<ul class="treeview-menu">
				<li><a href="/admin/forms/settings"><i class="fa fa-angle-double-right"></i> Form Email Settings</a></li>
				<li><a href="/admin/settings/notification"><i class="fa fa-angle-double-right"></i> Site Notification</a></li>
				<?php if (ENVIRONMENT !== 'production'): ?>
					<li><a href="/admin/forms/options"><i class="fa fa-angle-double-right"></i> Dropdown Options</a></li>
				<?php endif; ?>
			</ul>
		</li>
		<?php endif;?>

	</ul>

</section>
