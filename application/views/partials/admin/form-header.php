<!DOCTYPE html>
<!--[if !IE]>	<html class="bg-dk-gray" lang="en"> <![endif]-->
<!--[if lt IE 7]>	<html class="bg-dk-gray no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>		<html class="bg-dk-gray no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>		<html class="bg-dk-gray no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]>	<!--> <html class=" bg-dk-gray no-js" lang="en"> <!--<![endif]-->

<head>
	<meta charset="utf-8">
	<title>
		<?php
			if (isset($pageTitle)) {
				echo $pageTitle  . ' | ' . $this->config->item('site_name');
			} else if (isset($title)) {
				echo $title  . ' | ' . $this->config->item('site_name');
			} else {
				echo $this->config->item('site_name');
			}
		?>
	</title>

	<!-- Meta Tags -->
	<meta name="author" content="Clocktower Media" />
	<meta name="description" content="<?php if (isset($page)) {echo $page['metaDesc'];} ?> <?php if (isset($metaDesc)) {echo $metaDesc;} ?>" />
	<meta name="keywords" content="<?php if (isset($page)) {echo $page['metaKeywords'];} ?> <?php if (isset($metaKeywords)) {echo $metaKeywords;} ?>" />

	<!-- Modernizr & Script Loader -->
	<script src="/assets/js/modernizr-2.5.3.js"></script>
	<script src="/assets/js/head.load.min.js"></script>

	<!-- Common/Shared CSS -->
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="/assets/admin/css/lte.css">
	<link rel="stylesheet" href="/assets/admin/css/plugins.css">
	<link rel="stylesheet" href="/assets/admin/css/admin.css">

	<!-- Site Specific CSS -->
	<!--<link rel="stylesheet" href="/assets/admin/css/site.css">-->

	<!-- Module CSS -->
	<?php
		if (file_exists(APPPATH . 'modules/'. $this->uri->segment(1)  .'/partials/admin/css.php'))
		{
			$this->load->view('../modules/'. $this->uri->segment(1) . '/partials/admin/css');
		}
	?>

</head>

<?php
	//load relevant config files
	$this->config->load('contact/config');
?>

<body class="bg-dk-gray <?php echo $this->router->fetch_module() . ' ' . $this->router->fetch_class() . ' ' . $this->router->fetch_method() . ' '; if(isset($page)) echo 'page-' . $page['pageID']; ?>">

	<?php
		//global partials
		$this->load->view('partials/admin/winks');
	?>

	<div class="form-box" id="login-box">
		<div class="header"><?php echo $title;?></div>


