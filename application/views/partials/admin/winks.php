<?php if ($this->session->flashdata('info') != '' || $this->session->flashdata('error') != '' || $this->session->flashdata('success') != '' || validation_errors() != null): ?>

  <?php if($this->session->flashdata('info') != '') : ?>
    <div class="alert alert-info alert-dismissable">
      <button class="close" data-dismiss="alert">x</button>
      <?php echo $this->session->flashdata('info');?>
    </div>
  <?php endif; ?>

   <?php if($this->session->flashdata('error') != '' || validation_errors() != null ) : ?>
    <div class="alert alert-danger alert-dismissable">
      <button class="close" data-dismiss="alert">x</button>
      <?php echo $this->session->flashdata('error'); ?>
      <?php echo validation_errors(); ?>
    </div>
   <?php endif; ?>

  <?php if($this->session->flashdata('success') != '') : ?>
    <div class="alert alert-success alert-dismissable">
      <button class="close" data-dismiss="alert">x</button>
      <?php echo $this->session->flashdata('success');?>
    </div>
  <?php endif; ?>

<?php endif; ?>