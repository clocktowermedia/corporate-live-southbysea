<!DOCTYPE html>
<!--[if lt IE 7]>	<html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>		<html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>		<html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]>	<!--> <html class="no-js" lang="en"> <!--<![endif]-->

<head>
	<meta charset="utf-8">
	<title>
		<?php
			if (isset($pageTitle)) {
				echo $pageTitle  . ' | ' . $this->config->item('site_name');
			} else if (isset($title)) {
				echo $title  . ' | ' . $this->config->item('site_name');
			} else {
				echo $this->config->item('site_name');
			}
		?>
	</title>

	<meta http-equiv="X-UA-Compatible" content="IE=edge" />

	<!-- Meta Tags -->
	<meta name="author" content="Clocktower Media" />
	<meta name="description" content="<?php if (isset($page)) {echo $page['metaDesc'];} ?> <?php if (isset($metaDesc)) {echo $metaDesc;} ?>" />
	<meta name="keywords" content="<?php if (isset($page)) {echo $page['metaKeywords'];} ?> <?php if (isset($metaKeywords)) {echo $metaKeywords;} ?>" />

	<!-- Modernizr & Script Loader -->
	<script src="/assets/js/modernizr-2.5.3.js"></script>
	<script src="/assets/js/head.load.min.js"></script>

	<!-- Common/Shared CSS -->
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css">
	<link rel="stylesheet" href="/assets/admin/css/lte.css">
	<link rel="stylesheet" href="//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css">
	<link rel="stylesheet" href="/assets/admin/css/plugins.css">
	<link rel="stylesheet" href="/assets/admin/css/admin.css">
	<link rel="stylesheet" href="/assets/admin/css/site.css">

	<!-- Site Specific CSS -->
	<!--<link rel="stylesheet" href="/assets/admin/css/site.css">-->

	<!-- Module CSS -->
	<?php
		if (file_exists(APPPATH . 'modules/'. $this->uri->segment(1)  .'/partials/admin/css.php'))
		{
			$this->load->view('../modules/'. $this->uri->segment(1) . '/partials/admin/css');
		}
	?>

</head>

<?php
	//load relevant config files
	$this->config->load('contact/config');
?>

<body class="skin-blue <?php echo $this->router->fetch_module() . ' ' . $this->router->fetch_class() . ' ' . $this->router->fetch_method() . ' '; if(isset($page)) echo 'page-' . $page['pageID']; ?>">

	<header class="header">
		<a href="/admin" class="logo">
			<img src="/assets/images/logo-small.png">
		</a>

		<!-- Header Navbar: style can be found in header.less -->
		<nav class="navbar navbar-static-top" role="navigation">

			<!-- Sidebar toggle button-->
			<a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			<div class="navbar-right">
				<ul class="nav navbar-nav">

					<!-- User Account: style can be found in dropdown.less -->
					<li class="dropdown user user-menu">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="glyphicon glyphicon-user"></i>
							<span><?php echo $_SESSION['adminName'];?> <i class="caret"></i></span>
						</a>
						<ul class="dropdown-menu">
							<!-- User image -->
							<li class="user-header bg-light-blue">
								<!--<img src="http://placehold.it/50x40" class="img-circle" alt="User Image" />-->
								<p>
									<?php echo $_SESSION['adminName'];?> - <?php echo $_SESSION['adminRole'];?>
									<small>Member since <?php echo date('M. Y', strtotime($_SESSION['adminDate']));?></small>
								</p>
							</li>
							<!-- Menu Footer-->
							<li class="user-footer">
								<div class="pull-left">
									<a href="/admin/change-password" class="btn btn-default btn-flat">Change Password</a>
								</div>
								<div class="pull-right">
									<a href="/admin-auth/logout" class="btn btn-default btn-flat">Log Out</a>
								</div>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</nav>
	</header>

	<div class="wrapper row-offcanvas row-offcanvas-left">
		<aside class="left-side sidebar-offcanvas">
			<?php $this->load->view('partials/admin/nav'); ?>
		</aside>

		<!-- Right side column. Contains the navbar and content of the page -->
		<aside class="right-side">

			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1><?php echo $title;?></h1>
				<ol class="breadcrumb">
					<li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
					<?php if ($this->router->fetch_method() != 'index'):?>
					   <li><a href="/admin/<?php echo $this->router->fetch_module();?>"><?php echo ucwords($this->router->fetch_module());?></a></li>
					<?php endif;?>
					<li class="active"><?php echo $title;?></li>
				</ol>
			</section>

			<!-- Main content -->
			<section class="content">