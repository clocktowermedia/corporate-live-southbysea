<?php
	//site header
	$this->load->view('partials/meta-header');

	//load the main content from the controller
	$this->load->view($view);

	//site footer
	$this->load->view('partials/meta-footer');