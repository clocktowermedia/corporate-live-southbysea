<?php

	if (strpos($_SERVER[REQUEST_URI], 'fraternity')) { // returns false if '?' isn't there
		//The parameter you need is present
		$this->load->view('partials/frat-header');

		//global partials
		$this->load->view('partials/frat-breadcrumbs');
		$this->load->view('partials/winks');

		//load containers
		$this->load->view('partials/container');

		//load the main content from the controller
		$this->load->view($view);

		//add the modal
		$this->load->view('partials/modal');

		//end containers
		$this->load->view('partials/container-end');

		//site footer
		$this->load->view('partials/frat-footer');

	}else{
		$this->load->view('partials/header');

		//global partials
		$this->load->view('partials/breadcrumbs');
		$this->load->view('partials/winks');

		//load containers
		$this->load->view('partials/container');

		//load the main content from the controller
		$this->load->view($view);

		//add the modal
		$this->load->view('partials/modal');

		//end containers
		$this->load->view('partials/container-end');

		//site footer
		$this->load->view('partials/footer');
}

	exit;
