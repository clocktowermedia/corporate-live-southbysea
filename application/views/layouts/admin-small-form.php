<?php 
	//site header
	$this->load->view('partials/admin/form-header');

	//module partials
	//$this->load->view('../modules/'. $this->router->fetch_module() .'/partials/admin/');

	//load the main content from the controller
	$this->load->view($view);

	//site footer
	$this->load->view('partials/admin/form-footer');