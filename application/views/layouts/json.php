<?php
$jsonReply = array(
	'status'	=> $status,
	'response'	=> $response,
	'data'		=> $data,
);

echo json_encode($jsonReply);
