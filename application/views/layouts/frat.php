<?php
		//site header
		$this->load->view('partials/frat-header');

		//global partials
		$this->load->view('partials/winks');

		//load the main content from the controller
		$this->load->view($view);

		//add the modal
		$this->load->view('partials/modal');

		//site footer
		$this->load->view('partials/frat-footer');
