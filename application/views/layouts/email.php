<?php 
	//email header
	$this->load->view('partials/email/header');

	//load the main content from the controller
	$this->load->view($view);

	//email footer
	$this->load->view('partials/email/footer');