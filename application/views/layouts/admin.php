<?php
	//site header
	$this->load->view('partials/admin/header');

	//global partials
	$this->load->view('partials/admin/winks');

	//module partials
	//$this->load->view('../modules/'. $this->router->fetch_module() .'/partials/admin/');

	//load the main content from the controller
	$this->load->view($view);

	//add the modal
	$this->load->view('partials/modal');

	//site footer
	$this->load->view('partials/admin/footer');