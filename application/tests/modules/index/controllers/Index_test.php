<?php

class index_Index_test extends TestCase
{
	public function test_index()
	{
		$output = $this->request('GET', '/');
		$this->assertContains('index index-pagetype', $output);

		$collegeOutput = $this->request('GET', '/?site=college');
		$this->assertContains('index index-pagetype college', $collegeOutput);

		/*
		$this->request('GET', '/?site=music');
        $this->assertRedirect('http://www.southxsea.com/');
        */
	}

	public function test_sitemap()
	{
		$this->request->setHeader('Accept', 'application/xml');
		$output = $this->request('GET', '/sitemap.xml');
		$this->assertResponseCode(200);
		$this->assertResponseHeader(
			'Content-Type', 'application/xml'
		);
	}

	public function test_page_404()
	{
		$this->request('GET', ['nocontroller', 'noaction']);
        $this->assertResponseCode(404);
	}
}