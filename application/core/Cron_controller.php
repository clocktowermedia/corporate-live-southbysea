<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
* Extend MY_Controller, basically check to make sure we have permission (via password)
*/
class Cron_controller extends MY_Controller
{
	//call before any function
	function __construct()
	{
		parent::__construct();

		//make sure a password was passed
		$this->_hasPw();
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/


	/**
	* Returns true if a user is logged in
	*/
	/**
	* Returns true if a user is logged in
	*/
	public function _hasPw()
	{
		try {

			if ($this->is_get())
			{
				if (is_null($this->input->get('cronPw')) || $this->input->get('cronPw') == '')
				{
					throw new Exception('Missing pw.');
				}

				if ($this->input->get('cronPw', TRUE) != $this->config->item('cronPw'))
				{
					throw new Exception('Invalid pw.');
				}

			} else if ($this->is_post()) {

				if (is_null($this->input->post('cronPw')) || $this->input->post('cronPw') == '')
				{
					throw new Exception('Missing pw.');
				}

				if ($this->input->post('cronPw', TRUE) != $this->config->item('cronPw'))
				{
					throw new Exception('Invalid pw.');
				}

			} else {
				throw new Exception();
			}


		} catch (Exception $e) {
			redirect('/');
		}
	}
}