<?php
class MY_Exceptions extends CI_Exceptions {

	private $CI;
	private $config;
	public $emailLayout;

	function __construct()
	{
		parent::__construct();

		//define CI & others
		$this->CI =& get_instance();
		$this->config =& get_config();
		$this->emailLayout = 'views/layouts/email';
	}

	function get_template_path()
	{
		$templates_path = config_item('error_views_path');
		if (empty($templates_path))
		{
			$templates_path = VIEWPATH.'errors'.DIRECTORY_SEPARATOR;
		}
		return $templates_path;
	}

	/**
	 * General Error Page
	 *
	 * Takes an error message as input (either as a string or an array)
	 * and displays it using the specified template.
	 *
	 * @param	string		$heading	Page heading
	 * @param	string|string[]	$message	Error message
	 * @param	string		$template	Template name
	 * @param 	int		$status_code	(default: 500)
	 *
	 * @return	string	Error page output
	 */
	public function show_error($heading, $message, $template = 'error_general', $status_code = 500)
	{
		$templates_path = $this->get_template_path();
		$email_template = 'errors'.DIRECTORY_SEPARATOR;

		if (is_cli())
		{
			$message = "\t".(is_array($message) ? implode("\n\t", $message) : $message);
			$templates_path .= 'cli'.DIRECTORY_SEPARATOR;
			$email_template .= 'cli'.DIRECTORY_SEPARATOR;
		} else {
			set_status_header($status_code);
			$message = '<p>'.(is_array($message) ? implode('</p><p>', $message) : $message).'</p>';
			$templates_path .= 'html'.DIRECTORY_SEPARATOR;
			$email_template .= 'html'.DIRECTORY_SEPARATOR;
		}
		if (ob_get_level() > $this->ob_level + 1)
		{
			ob_end_flush();
		}
		ob_start();

		//email error
		$this->CI->load->library('parser');
		$outputData = array(
			'heading' => $heading,
			'message' => $message,
		);
		$content = $this->CI->parser->parse($email_template . $template, $outputData, true);
		if ($status_code !== 404)
		{
			$this->report_error($heading, $message, $content);
		}

		//show error
		include($templates_path . $template . '.php');
		$buffer = ob_get_contents();
		ob_end_clean();
		return $buffer;
	}

	// --------------------------------------------------------------------
	public function show_exception($exception)
	{
		$templates_path = $this->get_template_path();
		$email_template = 'errors'.DIRECTORY_SEPARATOR;

		$message = $exception->getMessage();
		if (empty($message))
		{
			$message = '(null)';
		}

		if (is_cli())
		{
			$templates_path .= 'cli'.DIRECTORY_SEPARATOR;
			$email_template .= 'cli'.DIRECTORY_SEPARATOR;
		} else {
			set_status_header(500);
			$templates_path .= 'html'.DIRECTORY_SEPARATOR;
			$email_template .= 'html'.DIRECTORY_SEPARATOR;
		}

		if (ob_get_level() > $this->ob_level + 1)
		{
			ob_end_flush();
		}

		ob_start();

		//email error
		$this->CI->load->library('parser');
		$outputData = array(
			'exception' => $exception,
			'message' => $message,
		);
		$content = $this->CI->parser->parse($email_template . 'error_exception', $outputData, true);
		$this->report_error('Exception', $message, $content);

		//show error
		include($templates_path.'error_exception.php');
		$buffer = ob_get_contents();
		ob_end_clean();
		echo $buffer;
	}

	// --------------------------------------------------------------------

	/**
	 * Native PHP error handler
	 *
	 * @param	int	$severity	Error level
	 * @param	string	$message	Error message
	 * @param	string	$filepath	File path
	 * @param	int	$line		Line number
	 * @return	string	Error page output
	 */
	public function show_php_error($severity, $message, $filepath, $line)
	{
		$templates_path = $this->get_template_path();
		$email_template = 'errors'.DIRECTORY_SEPARATOR;

		$severity = isset($this->levels[$severity]) ? $this->levels[$severity] : $severity;

		// For safety reasons we don't show the full file path in non-CLI requests
		if (!is_cli())
		{
			$filepath = str_replace('\\', '/', $filepath);
			if (FALSE !== strpos($filepath, '/'))
			{
				$x = explode('/', $filepath);
				$filepath = $x[count($x)-2].'/'.end($x);
			}

			$templates_path .= 'html'.DIRECTORY_SEPARATOR;
			$email_template .= 'html'.DIRECTORY_SEPARATOR;
		} else {
			$templates_path .= 'cli'.DIRECTORY_SEPARATOR;
			$email_template .= 'html'.DIRECTORY_SEPARATOR;
		}

		if (ob_get_level() > $this->ob_level + 1)
		{
			ob_end_flush();
		}
		ob_start();

		//email error
		$this->CI->load->library('parser');
		$outputData = array(
			'severity' => $severity,
			'message' => $message,
			'filepath' => $filepath,
			'line' => $line
		);
		$content = $this->CI->parser->parse($email_template . 'error_exception', $outputData, true);
		$this->report_error($severity . ' Error', $message, $content);

		//show error
		include($templates_path.'error_php.php');
		$buffer = ob_get_contents();
		ob_end_clean();
		echo $buffer;
	}

	/**
	* gets the debug information
	*/
	function _get_debug_backtrace($br = "<BR>") {
		$trace = array_slice(debug_backtrace(), 3);
		$msg = '<code>';
		foreach($trace as $index => $info) {
		  if (isset($info['file']))
		  {
			$msg .= $info['file'] . ':' . $info['line'] . " -> " . $info['function'] . '(' . $info['args'] . ')' . $br;
		  }
		}
		$msg .= '</code>';
		return $msg;
	}

	/**
	* Function for emailing error messages to dev
	*/
	function report_error($subject, $message, $content)
	{
		//load email library & config
		$this->CI->load->library('email');
		$this->CI->load->config('email');

		//start body tag
		$body = '<h1>' . $message . '</h1>';
		$body .= '<p>Request: <br/><br/><code>';

		//loop through the request and add it to the code
		foreach ($_REQUEST as $k => $v) {
			$body .= $k . ' => ' . $v . '<br/>';
		}
		$body .= '</code></p>';

		//tell us the location of the error
		$body .= '<p>Server: <br/><br/><code>';
		foreach ($_SERVER as $k => $v) {
			$body .= $k . ' => ' . $v . '<br/>';
		}
		$body .= '</code></p>';

		/*
		//get stacktrace
		$body .= '<p>Stacktrace: <br/><br/>';
		$body .= $this->_get_debug_backtrace() . '</p>';
		*/

		$this->CI->email->from($this->config['site_email'], $this->config['site_name']);
		$this->CI->email->to($this->config['dev_email']);
		$this->CI->email->subject($subject);
		$this->CI->email->message($body . $content);;
		$this->CI->email->send();
	}
}
