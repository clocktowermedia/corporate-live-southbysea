<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
* Extend MX_Controller, add some new functions!
*/
class MY_Controller extends MX_Controller
{
	//called for any instance of this class
	function __construct()
	{
		parent::__construct();

		//this is used on all the front end pages for header and footer, so load it here
		$this->load->model('menus/menu_model');
		$this->load->model('index/site_setting_model');

		//global data
		$setting_data = $this->site_setting_model->get_by('slug', 'notification');
		$setting = json_decode($setting_data['content'], TRUE);
		$data['site_notification'] = ($setting['status'] === 'enabled')? $setting : false;
		$this->load->vars($data);
	}

	public function _remap($method, $params = array())
	{
		if (method_exists($this, $method)){
			return call_user_func_array(array($this, $method), $params);
		}

		//if the method does not exist, try finding the cms page
		modules::run('pages/view', $this->uri->uri_string());
	}

	/*
	* Returns request method ie: get, post, etc.
	*/
	public function method()
	{
		$method = 'undefined';

		if(isset($_SERVER['REQUEST_METHOD']))
		{
			$method = strToLower($_SERVER["REQUEST_METHOD"]);
		}
		return $method;
	}

	/**
	* Returns true if the request is get, otherwise returns false
	*/
	public function is_get()
	{
		return ($this->method() == 'get');
	}

	/**
	* Returns true if the request is post, otherwise returns false
	*/
	public function is_post()
	{
		return ($this->method() == 'post');
	}

	public function dump($array = array())
	{
		echo '<pre>';
		print_r($array);
		echo '</pre>';
	}

	public function render_json($data = array())
	{
		echo json_encode($data);
	}

	/**
	* Automatically determines what view file to grab based on the current action
	*/
	public function get_view()
	{
		return str_replace('_', '-', $this->router->fetch_method());
	}

	public function get_admin_view()
	{
		return '/admin/' . $this->get_view();
	}

	public function get_email_view()
	{
		$path = $this->get_view();
		$path = str_replace('_send_', '', $path);
		return '/emails/' . $path;
	}

	public function get_layout_path()
	{
		return '../views/layouts/';
	}

	public function get_module_layout_path()
	{
		return '../modules/' . $this->router->fetch_class() . '/layouts/';
	}

	public function get_default_layout()
	{
		return '../views/layouts/default';
	}

	public function get_default_module_layout()
	{
		return $this->get_module_layout_path() . 'default';
	}

	public function get_admin_layout()
	{
		return $this->get_layout_path() . 'admin';
	}

	public function get_json_layout()
	{
		return $this->get_layout_path() . 'json';
	}

	public function get_xml_layout()
	{
		return $this->get_layout_path() . 'xml';
	}

	public function get_email_layout()
	{
		return $this->get_layout_path() . 'email';
	}

	public function get_email_template($subject)
	{
		$template = array();
		$siteLink =  (($_SERVER['SERVER_PORT'] == 443 || (!empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on')))? 'https://' . $_SERVER['HTTP_HOST'] : 'http://' . $_SERVER['HTTP_HOST'];

		$header =
		'<table id="wrapper-table" width="100%" cellpadding="0" cellspacing="0" border="0" align="center" class="table">
			<tr>
				<td>
					<table id="header" width="100%" cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td id="logo-container" class="padding-left" width="25%">
								<a href="' . $siteLink . '?utm_medium=email" target="_blank">
									<img id="logo" src="' . $siteLink . '/assets/images/email-corp.png" border="0" alt="' . $this->config->item('site_name') . '" />
								</a>
							</td>
							<td width="75%" class="padding-left padding-right padding-top pull-right">'
							. '<h3 class="pull-right" style="font-size: 115%;">' . strtoupper(str_replace('South by Sea', '', $subject)) . '</h3>' .
							'</td>
						</tr>
					</table>
				</td>
			</tr>';

		/*
		$header = '<tr><th class="header" ' . $colspan . '>
			<a href="'. $siteLink . '" target="_blank">
				<img src="' . $siteLink . '/assets/images/email-logo.png" align="left"></a>' .
				'<h1>' . $subject . '</h1>
		</th></tr>';
		*/

		//$table = '<table cellspacing="0" class="wrapperTable" background="' . $siteLink . '/assets/images/bg.jpg">';

		$tableStart =
		'<tr>
			<td id="content">
				<table id="content-table" width="100%" cellpadding="0" cellspacing="0" border="0" align="center" class="innertable">
					<tr>
						<td>';
		$tableEnd = 	'</td>
					</tr>
				</table>
			</td>
		</tr>';

		$footer =
			'<tr>
				<td>
					<table width="100%" cellpadding="0" cellspacing="0" border="0" id="footer" style="background-color:#b8d1d1; color:#ffffff;">
						<tr>
							<td class="inner-padding pull-right" width="60%" style = "float: right;">
								<h2 class="pull-right" style="float: right; font-size: 115%;"><a href="' . $siteLink . '?utm_medium=email"><spa style="color:#ffffff;"> HTTPS://' . strtoupper(str_replace('www.', '', $_SERVER['HTTP_HOST'])) . ' </span></a></h2>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>';

		/*
		$footer = '<tr><th class="footer" ' . $colspan . '>
			<h1><a href="' . $siteLink . '?utm_medium=email">www.' . str_replace('www.', '', $_SERVER['HTTP_HOST']) . '</a></h1>
			<p class="social-container">
				<img src="' . $siteLink . '/assets/images/fb03.png">
				<img src="' . $siteLink . '/assets/images/pnt03.png">
				<img src="' . $siteLink . '/assets/images/tw03.png">
				<img class="last" src="' . $siteLink . '/assets/images/inst03.png">
			</p>
		</th></tr>';
		*/

		$template['header'] = $header;
		$template['table']['start'] = $tableStart;
		$template['table']['end'] = $tableEnd;
		$template['footer'] = $footer;

		return $template;
	}

	public function get_pdf_template($subject)
	{
		$template = array();
		$siteLink =  (($_SERVER['SERVER_PORT'] == 443 || (!empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on')))? 'https://' . $_SERVER['HTTP_HOST'] : 'http://' . $_SERVER['HTTP_HOST'];

		$header =
		'<table id="wrapper-table" width="100%" cellpadding="0" cellspacing="0" border="0" align="center" class="table">
			<tr>
				<td id="logo-container" align="center" width="100%">
					<a href="' . $siteLink . '" target="_blank">
						<img id="logo" src="' . $siteLink . '/assets/images/email-logo.png" border="0" alt="' . $this->config->item('site_name') . '" />
					</a>
				</td>
			</tr>';

		/*
		$header = '<tr><th class="header" ' . $colspan . '>
			<a href="'. $siteLink . '" target="_blank">
				<img src="' . $siteLink . '/assets/images/email-logo.png" align="left"></a>' .
				'<h1>' . $subject . '</h1>
		</th></tr>';
		*/

		//$table = '<table cellspacing="0" class="wrapperTable" background="' . $siteLink . '/assets/images/bg.jpg">';

		$tableStart =
		'<tr>
			<td id="content">
				<table id="content-table" width="100%" cellpadding="0" cellspacing="0" border="0" align="center" class="innertable">
					<tr>
						<td>';
		$tableEnd = 	'</td>
					</tr>
				</table>
			</td>
		</tr>';

		$footer =
			'<tr>
				<td class="inner-padding" width="100%" align="center">
					<h1><a class="pull-right" href="' . $siteLink . '">www.' . str_replace('www.', '', $_SERVER['HTTP_HOST']) . '</a></h1>
				</td>
			</tr>
		</table>';

		/*
		$footer = '<tr><th class="footer" ' . $colspan . '>
			<h1><a href="' . $siteLink . '?utm_medium=email">www.' . str_replace('www.', '', $_SERVER['HTTP_HOST']) . '</a></h1>
			<p class="social-container">
				<img src="' . $siteLink . '/assets/images/fb03.png">
				<img src="' . $siteLink . '/assets/images/pnt03.png">
				<img src="' . $siteLink . '/assets/images/tw03.png">
				<img class="last" src="' . $siteLink . '/assets/images/inst03.png">
			</p>
		</th></tr>';
		*/

		$template['header'] = $header;
		$template['table']['start'] = $tableStart;
		$template['table']['end'] = $tableEnd;
		$template['footer'] = $footer;

		return $template;
	}

	public function convert_1D_array_to_html_email_table($subject, $input, $additionalContent = null)
	{
		$template = $this->get_email_template($subject);

		//$subject = '<tr><td class="subject" colspan="2"><h2>' . $subject . '</h2></td></tr>';
		$additionalContent = (!is_null($additionalContent))? '<tr style="background-color:#b8d1d1;color:#ffffff !important;"><td class="inner-padding" colspan="2" style="color:#ffffff !important;">' . $additionalContent . '<br/></td></tr>' : '';

		$output = '';
		if (is_array($input) && count($input) > 0)
		{
			$this->load->helper('array');
			$count = 0;
			foreach ($input as $key => $value)
			{
				//form the key
				$count++;
				$class = ($count == 1)? 'first' : '';
				$key = format_key($key);

				$output .= '<tr valign="top">
					<td class="' . $class . ' inner-padding">'. $key .':</td>
					<td class="' . $class . ' inner-padding">'. $value .'</td>
				</tr>';
			}
		}

		$html = $template['header'] . $template['table']['start'];
		$html .= '<table width="100%" cellpadding="0" cellspacing="0" border="0">';
		$html .= $additionalContent . $output;
		$html .= '</table>';
		$html .= $template['table']['end'] . $template['footer'];
		return $html;
	}

	public function convert_text_to_html_email_table($subject, $input)
	{
		//get header and footer
		$template = $this->get_email_template($subject);

		//subject & content
		//$content = '<tr><td class="body"><h2>' . $subject . '</h2>'. $input .'</td></tr>';
		$content = '<tr><td class="inner-padding"><br/>'. $input .'</td></tr>';

		//put into table format
		$html = $template['header'] . $template['table']['start'];
		$html .= '<table width="100%" cellpadding="0" cellspacing="0" border="0">';
		$html .= $content;
		$html .= '</table>';
		$html .= $template['table']['end'] . $template['footer'];
		return $html;
	}

	public function convert_text_to_html_pdf_table($subject, $input)
	{
		//get header and footer
		$template = $this->get_pdf_template($subject);

		//subject & content
		//$content = '<tr><td class="body"><h2>' . $subject . '</h2>'. $input .'</td></tr>';
		$content = '<tr><td class="inner-padding"><br/>'. $input .'</td></tr>';

		//put into table format
		$html = $template['header'] . $template['table']['start'];
		$html .= '<table width="100%" cellpadding="0" cellspacing="0" border="0">';
		$html .= $content;
		$html .= '</table>';
		$html .= $template['table']['end'] . $template['footer'];
		return $html;
	}

	public function getModuleList()
	{
		$appModules = array();
		foreach(glob(APPPATH . '/modules/*') as $appModule)
		{
			$appModules[] = basename($appModule);
		}
		return $appModules;
	}

	public function getModuleListLinks()
	{
		return str_replace('_', '-', $this->getModuleList());
	}

	public function getActionList($module = null)
	{
		//create holder array
		$actions = array();

		//set file
		$file = APPPATH . 'modules/' . $module . '/controllers/' . $module .'.php';

		//make sure file exists
		if (!is_null($module) && file_exists($file) && $this->router->fetch_class() != $module)
		{
			include_once($file);

			$classActions = get_class_methods(ucfirst($module));
			foreach ($classActions as $actionName)
			{
				$actions[] = $actionName;
			}
		}

		return $actions;
	}

	public function getAdminActionList($module = null)
	{
		//create holder array
		$actions = array();

		//set file
		$file = APPPATH . 'modules/' . $module . '/controllers/admin.php';

		//make sure file exists
		if (!is_null($module) && file_exists($file) && $this->router->fetch_class() != $module)
		{
			include_once($file);

			$classActions = get_class_methods('Admin');
			foreach ($classActions as $actionName)
			{
				$actions[] = $actionName;
			}
		}

		return $actions;
	}

	public function getModuleActionList($type = 'user')
	{
		//get list of modules
		$modules = $this->getModuleList();

		//loop through modules, get actions
		foreach ($modules as $module)
		{
			$moduleActions[$module] = ($type == 'admin')? $this->getAdminActionList($module) : $this->getActionList($module);
		}

		return $moduleActions;
	}
}
