<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
* Extend MY_Controller, basically check to make sure they are logged in, or else redirect
*/
class Admin_controller extends MY_Controller
{
	//logged in admin id
	protected $adminID;
	protected $adminPerms;

	//call before any function
	function __construct()
	{
		parent::__construct();

		//get admin id
		$this->adminID = (int) $this->session->userdata('adminID');

		//make sure they are logged in as an admin
		$this->_requireLoggedIn();

		//load relevant models
		$this->load->model('admins/admin_model');
		$this->load->model('admins/admin_resource_model');
		$this->load->model('admins/admin_role_resource_model');

		//make sure user has permission
		#$this->_requirePermission();

		//get admin id & role info
		$admin = $this->admin_model->findByID($this->adminID);

		//get allowed sections and actions
		$this->adminPerms['sections'] = $this->admin_role_resource_model->getFullAllowedArray($admin['roleID'], 'section');
		$this->adminPerms['actions'] = $this->admin_role_resource_model->getFullAllowedArray($admin['roleID'], 'action');

		//add available sections to a session for nav/dashboard purposes
		$this->session->set_userdata('adminPerms', $this->adminPerms);
	}

	/************************************************************************************************************************************
	/
	/
	/ Helper functions
	/
	/
	/***********************************************************************************************************************************/

	/**
	* See if user is allowed to access url, redirect them if they are not
	* @parameter boolean $message show error message if true
	*/
	public function _requirePermission($message = true)
	{
		//determine if user is allowed to access this url
		$allowed = $this->_hasPermission();

		//redirect if not allowed
		if ($allowed == false)
		{
			//show error message if applicable
			if ($message == true)
			{
				$this->session->set_flashdata('error', 'You do not have permission to view this page or perform that action.');
			}

			//redirect
			redirect('/admin');
		}
	}

	/**
	 * Checks to see if the logged in admin has permission to view that page or perform that action
	 * @return boolean [allowed or not allowed to access url]
	 */
	public function _hasPermission()
	{
		//get admin info, if they have a role...
		$admin = $this->admin_model->findByID($this->adminID);

		//set allowed by default, we may want to set this to false later...
		$allowed = true;
		$allowedAction = true;
		$allowedSection = true;

		//if they have a role...
		if ($admin['roleID'] > 0)
		{
			//get current controller/action
			$section = $this->router->fetch_module();
			$action = $this->router->fetch_method();

			//get cooresponding section/action
			$actionResource = $this->admin_resource_model->get_by(array('type' => 'action', 'section' => $section, 'url' => $action));
			$sectionResource = $this->admin_resource_model->get_by(array('type' => 'section', 'section' => $section));

			//check action
			if ($actionResource != false && !empty($actionResource)) 
			{
				//check to see if the action is allowed
				$inPermissions = $this->admin_role_resource_model->select('resourceID')->get_by(array('roleID' => $admin['roleID'], 'resourceID' => $actionResource['resourceID']));
				$allowedAction = ($inPermissions == false || empty($inPermissions))? false : true;
			}

			//check section if we need to
			if ($allowedAction == false && $sectionResource != false && !empty($sectionResource))
			{
				//check to see if the section is allowed
				$inPermissions = $this->admin_role_resource_model->select('resourceID')->get_by(array('roleID' => $admin['roleID'], 'resourceID' => $sectionResource['resourceID']));
				$allowedSection = ($inPermissions == false || empty($inPermissions))? false : true;
			}

			//assign to variable
			$allowed = ($allowedAction == true || $allowedSection == true)? true : false;

			//return whether they are allowed or not
			return $allowed;
		}

		//return whether they are allowed or not
		return $allowed;
	}

	/**
	* Returns true if a user is logged in
	*/
	/**
	* Returns true if a user is logged in
	*/
	public function _isLoggedIn()
	{
		if ($this->adminID > 0)
		{
			//make sure account exists
			$this->load->model('admins/admin_model');
			$account = $this->admin_model->findByID($this->adminID);
			$exists = ($account != false)? true : false;

			//unset admin id from session
			if ($exists == false)
			{
				$this->session->unset_userdata('adminID');
			}

			//return whether account exists or not
			return $exists;
		} else {
			return false;
		}

		return false;
	}

	/**
	* Require a user to be logged in, redirect them if they are not
	* @parameter boolean $message, show error message if true
	*/
	public function _requireLoggedIn($message = true)
	{
		$loggedIn = $this->_isLoggedIn();

		if ($loggedIn == false)
		{
			if ($message == true)
			{
				$this->session->set_flashdata('error', "You need to log in to view this page or perform that action.");
			}
			redirect('/admin-auth/login');
		}
	}
}