<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Global Site Email
|--------------------------------------------------------------------------
|
| used in from field
|
*/
if (ENVIRONMENT == 'production')
{
	$config['site_email'] = 'forms@southbysea.com';
} else {
	$config['site_email'] = 'dev@clocktowermedia.com';
}

$config['dev_email'] = 'marissa@clocktowermedia.com';

/*
|--------------------------------------------------------------------------
| Global Site Email Display Name
|--------------------------------------------------------------------------
|
| used in from field
|
*/
$config['site_email_name'] = 'South By Sea Admin';

/*
|--------------------------------------------------------------------------
| Mail Type
|--------------------------------------------------------------------------
|
| html or text email
|
*/
$config['mailtype'] = 'html';
$config['charset'] = 'utf-8';
//$config['charset'] = 'iso-8859-1';

if (ENVIRONMENT == 'production')
{
	$config['protocol'] = 'smtp';

	$config['smtp_host'] = 'smtp.mailgun.org';
	$config['smtp_port'] = '25';
	$config['smtp_user'] = 'no-reply@southbysea.com';
	$config['smtp_pass'] = '73290e01b83b5ec2af22915367dbe968';
	$config['crlf'] = "\n";
	$config['newline'] = "\n";

	$config['smtp_host_2'] = 'ssl://smtp-relay.gmail.com';
	$config['smtp_port_2'] = '465';
	$config['smtp_user_2'] = 'forms@southbysea.com';
	$config['smtp_pass_2'] = 'UzVZxZ2nCJMk6KQ';
	$config['crlf_2'] = "\r\n";
	$config['newline_2'] = "\r\n";

} else {
	$config['protocol'] = 'smtp';
	$config['smtp_host'] = 'smtp.mailgun.org';
	$config['smtp_user'] = 'no-reply@dev.myclocktower.com';
	$config['smtp_pass'] = 'E6r5kXLyMsLatLy4';
}

/* End of file config.php */
/* Location: ./application/config/config.php */