<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'index';
$route['404_override'] = 'pages/view';

$route['translate_uri_dashes'] = TRUE;

/*
| -------------------------------------------------------------------------
| Skeleton Routes
| -------------------------------------------------------------------------
*/
$route['sitemap.xml'] = 'index/sitemap';

$route['fraternity/search/json/(.+)'] = 'search/search_json/$1';
$route['fraternity/forms/json/(.+)'] = 'forms/forms_json/$1';
$route['fraternity/designs/json/(.+)'] = 'designs/designs_json/$1';

$route['fraternity/products/(:any)/(:any)/(.+)'] = 'products/product/$1/$2/$3';
$route['fraternity/products/(:any)/(.+)'] = 'products/subcategory/$1/$2';
$route['fraternity/products/(.+)'] = 'products/category/$1';


$route['fraternity/designs/(:any)/(.+)'] = 'designs/design/$1/$2';
$route['fraternity/designs/new'] = 'designs/new_designs';
$route['fraternity/designs/(.+)'] = 'designs/category/$1';
$route['fraternity/designs'] = 'designs/index';

$route['fraternity/campus-manager/managers/(:any)/(.+)'] = 'campus_managers/managers/$1/$2';
$route['fraternity/campus-managers/apply'] = 'campus_managers/apply';
$route['fraternity/campus-manager'] = 'campus_managers/index';
$route['fraternity/careers'] = 'campus_managers/careers';
$route['fraternity/careers/open-positions/(.+)/apply'] = 'campus_managers/positions_apply/$1';
$route['fraternity/careers/open-positions/(.+)'] = 'campus_managers/positions/$1';


/*
| -------------------------------------------------------------------------
| Custom Routes for South By Sea
| -------------------------------------------------------------------------
|
*/
$route['preorder/(:any)/generate-pdf/(.+)'] = 'ecommerce/generate_pdf/$2';
$route['preorder/(:any)/subscribe'] = 'ecommerce/subscribe/$1';
$route['preorder/(:any)/login'] = 'ecommerce/login/$1';
$route['preorder/(:any)/paypal'] = 'ecommerce/paypal/$1';
$route['preorder/(:any)/thank-you'] = 'ecommerce/thank_you/$1';
$route['preorder/(:any)/contact'] = 'ecommerce/contact/$1';
$route['preorder/(:any)/cart/json/(.+)'] = 'ecommerce/cart_json/$2';
$route['preorder/(:any)/cart'] = 'ecommerce/cart/$1';
$route['preorder/(:any)/checkout'] = 'ecommerce/checkout/$1';
$route['preorder/(:any)/(.+)'] = 'ecommerce/product/$1/$2';
$route['preorder/(.+)'] = 'ecommerce/index/$1';
#$route['preorder/(:any)/checkout'] = 'ecommerce/checkout/$1';

$route['admin/campus-managers/schools/(.+)'] = 'campus_managers/admin_schools/$1';
$route['admin/campus-managers/schools'] = 'campus_managers/admin_schools/index';

$route['admin/products/categories/json/(.+)'] = 'products/admin_product_categories_json/$1';
$route['admin/designs/categories/json/(.+)'] = 'designs/admin_design_categories_json/$1';

$route['search/json/(.+)'] = 'search/search_json/$1';
$route['forms/json/(.+)'] = 'forms/forms_json/$1';
$route['designs/json/(.+)'] = 'designs/designs_json/$1';

$route['products/(:any)/(:any)/(.+)'] = 'products/product/$1/$2/$3';
$route['products/(:any)/(.+)'] = 'products/subcategory/$1/$2';
$route['products/(.+)'] = 'products/category/$1';

$route['designs/(:any)/(.+)'] = 'designs/design/$1/$2';
$route['designs/new'] = 'designs/new_designs';
$route['designs/(.+)'] = 'designs/category/$1';

$route['careers'] = 'campus_managers/careers';
$route['careers/open-positions/(.+)/apply'] = 'campus_managers/positions_apply/$1';
$route['careers/open-positions/(.+)'] = 'campus_managers/positions/$1';
/*
| -------------------------------------------------------------------------
| Used to create a /admin folder structure
| -------------------------------------------------------------------------
|
*/

//exceptions for admin functions
$route['admin'] = 'admin';
$route['admin/change-password'] = 'admin/change-password';

//route specific types (datatables, json, etc.)
$route['admin/(:any)/json/(:any)/(.+)'] = '$1/admin_json/$2/$3';
$route['admin/(:any)/json/(.+)'] = '$1/admin_json/$2';
$route['admin/(:any)/datatables/(:any)/(.+)'] = '$1/admin_datatables/$2/$3';
$route['admin/(:any)/datatables/(.+)'] = '$1/admin_datatables/$2';
$route['admin/(:any)/modals/(.+)'] = '$1/admin_modals/$2';

//custom form routes
$route['admin/forms/options/categories'] = 'forms/admin_option_categories/index';
$route['admin/forms/options/categories/(.+)'] = 'forms/admin_option_categories/$1';
$route['admin/forms/options/(.+)'] = 'forms/admin_options/$1';
$route['admin/forms/options'] = 'forms/admin_options/index';

//custom category routes
$route['admin/(:any)/categories/(:any)/(.+)'] = '$1/admin_categories/$2/$3';
$route['admin/(:any)/categories/(.+)'] = '$1/admin_categories/$2';
$route['admin/(:any)/categories'] = '$1/admin_categories/index';

//custom reporting routes
$route['admin/(:any)/reporting/(:any)/(.+)'] = '$1/admin_reporting/$2/$3';
$route['admin/(:any)/reporting/(.+)'] = '$1/admin_reporting/$2';
$route['admin/(:any)/reporting'] = '$1/admin_reporting/index';

//custom ecommerce routes
$route['admin/ecommerce/products/sizes/(:any)/(.+)'] = 'ecommerce/admin_product_sizes/$1/$2';
$route['admin/ecommerce/products/sizes/(.+)'] = 'ecommerce/admin_product_sizes/index/$1';
$route['admin/ecommerce/products/(:any)/(.+)'] = 'ecommerce/admin_products/$1/$2';
$route['admin/ecommerce/products/(.+)'] = 'ecommerce/admin_products/index/$1';
$route['admin/ecommerce/products'] = 'ecommerce/admin_products/index';
$route['admin/ecommerce/orders/(:any)/(.+)'] = 'ecommerce/admin_orders/$1/$2';
$route['admin/ecommerce/orders/(.+)'] = 'ecommerce/admin_orders/index/$1';
$route['admin/ecommerce/orders'] = 'ecommerce/admin_orders/index';

//custom setting route
$route['admin/settings/(:any)'] = 'index/admin/$1';

//route uri pair (events/admin -> admin/events)
$route['admin/([a-zA-Z_-]+)'] = '$1/admin/index';

//route uri triplet (events/admin/edit -> admin/events/edit)
$route['admin/([a-zA-Z_-]+)/(.+)'] = '$1/admin/$2';

//route uri quadruplet
$route['admin/([a-zA-Z_-]+)/(:any)/(.+)'] = '$1/admin/$2/$3';

/* End of file routes.php */
/* Location: ./application/config/routes.php */
