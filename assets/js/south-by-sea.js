var SouthBySea = {
	Loader: null,
	Forms: null,
	Pages: null,
	Designs: null,
	Products: null,
	Search: null
};

/**
 * SouthBySea.Loader
 *
 * Run every page through here and feed it only the JS it needs.
 */
SouthBySea.Loader = {
	init: function() {

		// Fire off JS based on URI segment
		var uri = document.location.pathname.split('/');

		SouthBySea.Loader.generalFunctions();

		if (uri[1] == '' || uri[1] == 'frat' || uri[1] == 'f'){
			SouthBySea.Pages.home();
		}

		// init search page
		if ((uri[1] == 'search')||(uri[2]=='search')){
			var search_element = document.getElementsByClassName("header_search")[0].setAttribute("id", "");

			head.js('//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.7.0/underscore-min.js', function() {
				SouthBySea.Search.init();
			});
		}else{
			head.js('//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.7.0/underscore-min.js', function() {

				var $form = $("#search-form");
				var $field = $form.find('input[name="q"]');

			  //init autocomplete
				$field.autocompleter({
					cache: false,
					highlightMatches: true,
					limit: 5,
					minLength: '3',
					source: '/search/json/tags',
					combine: function() {
						var $typeField = $form.find('input[name="type"]:checked');

						return {
							type: $typeField.val()
						};
					},
					offset: 'results',
					customLabel: 'text',
					customValue: 'id'
				});

				var $form2 = $(".sidebar");
				var $field2 = $form2.find('input[name="filters"]');

				//init autocomplete
				$field2.autocompleter({
					cache: false,
					highlightMatches: true,
					limit: 5,
					minLength: '3',
					source: '/search/json/tags',
					combine: function() {
						var $typeField = $form.find('input[name="type"]:checked');

						return {
							type: $typeField.val()
						};
					},
					offset: 'results',
					customLabel: 'text'
				});
			});
		}

		if (uri[2] == 'school-suggestion'){
			// head.js('/assets/modules/forms/js/school-suggestion.js');
		}

		if ((uri[2] == '2016')||(uri[3]=='2016')){
			SouthBySea.Pages.originalCatalogImages();
		}
		else if ((uri[1] == 'catalog')||(uri[2]=='catalog')) {
			SouthBySea.Pages.catalogImages();
		}

		//faq page
		if ((uri[1] == 'faq') || (uri[2] == 'faq')){

			SouthBySea.Pages.initFaq();
		}

		//load proof request js if applicable
		if ((uri[1] == 'quote-request' || (uri[1] == 'products' && uri[4] != '' && uri[4] != null) || (uri[1] == 'designs' && uri[3] != '' && uri[3] != null))
		|| (uri[2] == 'quote-request' || (uri[2] == 'products' && uri[5] != '' && uri[5] != null) || (uri[2] == 'designs' && uri[4] != '' && uri[4] != null))) {
			head.js('/assets/modules/forms/js/proof-request.js');
		}

		if((uri[4]=='apply')||(uri[5]=='apply')){
			head.js('/assets/modules/forms/js/positions-apply.js');
		}

		//load final submission js if applicable
		if ((uri[1] == 'order-form')||(uri[2]=='order-form')){
			head.js('/assets/modules/forms/js/final-submission.js');
		}

		//load design js if applicable (for filtering, thumbnails, & potentially pagination in the future)
		if ((uri[1] == 'designs' && (uri[3] == '' || uri[3] == null))
				|| (uri[2] == 'designs' && (uri[4] == '' || uri[4] == null))){
			head.js('//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.7.0/underscore-min.js', function() {
				SouthBySea.Designs.init();
			});
		}

		if ((uri[1] == 'designs' && (uri[3] != '' && uri[3] != null))
				||(uri[2] == 'designs' && (uri[4] != '' && uri[4] != null))){
			SouthBySea.Loader.shareThis();
			SouthBySea.Designs.designPage();
		}

		//load product js if applicable (for thumbnails, & potentially pagination in the future)
		if ((uri[1] == 'products' && (uri[4] != '' && uri[4] != null))
				|| (uri[2] == 'products' && (uri[5] != '' && uri[5] != null))){
			SouthBySea.Products.colors();
		}

		//init campus reps
		if ((uri[1] == 'campus-managers' || uri[1] == 'careers')
				|| (uri[2] == 'campus-manager' || uri[2] == 'careers')){
			SouthBySea.CampusReps.autosuggest();
		}

		if ((uri[1] == 'preorder')||(uri[2] == 'preorder')){
			var $clock = $('#clock');
			$('#clock').countdown($clock.attr('data-start')).on('update.countdown', function(event) {
				var html = '<span>%-D</span> day%!d '
					+ '<span>%-H</span> hour%!H '
					+ '<span>%-M</span> minute%!M '
					+ '<span>%S</span> second%!S';
				var $this = $(this).html(event.strftime(html));
			});
		}
	},

	generalFunctions: function() {
		//init form validation & other form stuff
		if ($('form').length > 0){
			var $form = $('form');

			//psersist form data where appropriate
			$('form.persist').formsticky();

			//form validation
			SouthBySea.Forms.formValidators();
			$form.not('.ignore-validation').validation({
				//successClass: "has-success",
				excluded: ':disabled',
				errorClass: 'has-error',
				classHandler: function (el) {
					var $container = el.$element.parent('div[class^="col-"]');
					return ($container.length > 0)? $container : el.$element.closest('div.form-group');
				},
				errorsContainer: function (el) {
					var $container = el.$element.parent('div[class^="col-"]');
					return ($container.length > 0)? $container : el.$element.closest('div.form-group');
				},
				errorsWrapper: "<span class='help-block has-error'></span>",
				errorTemplate: "<span></span>"
			});

			//form formatting plugins
			$form.find('input.date:not(.no-sunday)').pickadate({
				format: 'yyyy-mm-dd',
				selectYears: true,
				selectMonths: true,
				min: new Date()
			});
			$form.find('input.date.no-sunday').pickadate({
				format: 'yyyy-mm-dd',
				selectYears: true,
				selectMonths: true,
				min: new Date(),
				disable: [1]
			});
			$form.find('input[type="tel"]').mask('(999) 999-9999? x99999');
			$form.find('input.decimal').priceFormat({
				prefix: '',
				thousandsSeparator: ''
			});
			$form.find('input.numeric').masking('9#');
			$form.find('input[name="zip"], input.zip').mask('99999?-9999');
			$form.find('input.credit-card').mask('999999999999999?9');
			$form.find('input.cvc').mask('999?9');
			$form.find('input.months, input.years').mask('99');

			//weird, but we're going to create a list of all the customer textareas..this will be used to handle spacing
			var textareas = $form.find('textarea.cust-textarea').map(function() {
				return $(this).attr('name');
			}).get().join();
			if ($('#textarea-list').length > 0){
				$('#textarea-list').val(textareas);
			}

			SouthBySea.Forms.hideLinkedFields();
		}
	},

	shareThis: function() {
		//init sharethis
		head.js('//w.sharethis.com/button/buttons.js', function(){
			stLight.options({
				publisher:'8081bd82-a9f2-4946-9e0a-43bbc8099b35',
			});
		});
	},

	showAlert: function(alertType, alertMessage, placementContainer) {
		//set default for append after
		placementContainer = (typeof(placementContainer) !== "undefined")? placementContainer : 'div.content:first';

		//do not do anything if we don't have a type and message
		if (alertType != '' && alertMessage != '' && $(placementContainer).length > 0)
		{
			//build html
			var html = '<div class="alert alert-' + alertType + ' alert-dismissable">' +
				'<button class="close" data-dismiss="alert">x</button>' +
				alertMessage +
			'</div>';

			//add html
			$(placementContainer).prepend(html);
		}
	}
};

SouthBySea.Forms = {
	formValidators: function() {
		window.Validation.addValidator('filemaxmegabytes', {
			requirementType: 'string',
			validateString: function (value, requirement, instance) {
				if (!window.FormData) {
					return true;
				}

				var file = instance.$element[0].files;
				var maxBytes = requirement * 1048576;
				if (file.length == 0) {
					return true;
				}

				return file.length === 1 && file[0].size <= maxBytes;
			},
			messages: {
				en: 'File is to big'
			}
		})
		.addValidator('fileextensions', {
			requirementType: 'string',
			validateString: function (value, requirement, instance) {
				if (!window.FormData) {
					return true;
				}
				var file = instance.$element[0].files;
				if (file.length == 0) {
					return true;
				}
				var allowedExtensions = requirement.replace(/\s/g, "").split(',');
				var fileExtension = value.split('.').pop();
				return allowedExtensions.indexOf(fileExtension) !== -1;
			},
			messages: {
				en: 'File type not allowed.'
			}
		}).addValidator('filemimes', {
			requirementType: 'string',
			validateString: function (value, requirement, instance) {
				if (!window.FormData) {
					return true;
				}
				var file = instance.$element[0].files;
				if (file.length == 0) {
					return true;
				}
				//TODO, get file types from server
				//forms/json/get-allowed-files?form=&field=&format=mimes
				var allowedMimeTypes = requirement.replace(/\s/g, "").split(',');
				return allowedMimeTypes.indexOf(file[0].type) !== -1;
			},
			messages: {
				en: 'File type not allowed.'
			}
		});
	},

	hideLinkedFields: function() {
		//loop through all fields that need to be hidden and have a linked field
		var $linkedFields = $('input.show-if');
		if ($linkedFields.length > 0)
		{
			$linkedFields.each(function(e){

				//hide the field
				var $currentField = $(this);
				var $parentContainer = $currentField.parents('.form-group');
				$parentContainer.addClass('hide');

				//find the linked field
				var linkedFieldName = $currentField.attr('data-linked-field');
				var $linkedField = $('select[name="' + linkedFieldName + '"]');

				//get comparison values
				var comparisonValues = JSON.parse($currentField.attr('data-show-when').replace(/'/g, '"'));

				//add change method for linked field
				$linkedField.on('change', function(e){
					//get value
					var value = $(this).val();

					//if value
					if (comparisonValues.indexOf(value) !== -1)
					{
						$parentContainer.removeClass('hide');
						if (typeof $currentField.attr('data-show-req') !== 'undefined' && $currentField.attr('data-show-req') == true)
						{
							$currentField.attr('required', 'required');
						}
					} else {
						$parentContainer.addClass('hide');
						if (typeof $currentField.attr('data-show-req') !== 'undefined' && $currentField.attr('data-show-req') == true)
						{
							$currentField.removeAttr('required');
						}
					}
				});

				//trigger change
				$linkedField.trigger('change');
			});
		}
	}
};

SouthBySea.Pages = {
	initFaq: function() {
		$('div.spoiler-title').click(function() {
			//add plus or minus icon
			var $iconContainer = $(this).children().first();
			$iconContainer.toggleClass('show-icon').toggleClass('hide-icon');

			if ($iconContainer.hasClass('show-icon'))
			{
				$iconContainer.html('<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>');
			} else {
				$iconContainer.html('<span class="glyphicon glyphicon-minus" aria-hidden="true"></span>');
			}

			//show or hide associated anwers
			$(this).parent().children().last().toggle('fast');
		});

		//hide all answers on load
		$('div.spoiler-title').trigger('click');
	},

	originalCatalogImages: function() {
		//init book
		if ($('#book').length > 0){
			head.load('/assets/js/wow_book/wow_book.min.js', function(){
				var bookOptions = {
					//forceBasicPage: true,
					pageNumbers: false,
					centeredWhenClosed : true,
					mouseWheel: true,
					doubleClickToZoom: true,
					//pagesInMemory: 6,
					container: true,
					containerWidth: '100%',
					containerPadding: '20px 5px 20px 5px',
					navControls: false,
					responsiveNavControls: false,
					height: ((($('.book-container').width() - 10) / 2) * 11) / 8.5,
					width : $('.book-container').width() - 10,
					handleWidth: 100,
					responsiveHandleWidth : 50,
					responsiveSinglePage: function( book ){
						return $('.book-container').width() < 768;
					},
					toolbar : "lastLeft, left, right, lastRight, slideshow, zoomin, zoomout, flipsound, share, fullscreen",
					toolbarPosition: "bottom", // default "bottom"
					thumbnailsPosition : 'left',
					//flipSound: false,
					flipSoundFile : ["page-flip.mp3", "page-flip.ogg" ],
  					flipSoundPath : "/assets/js/wow_book/sound/",
					share: "facebook twitter, reddit",
					shareParams: {
						url : "https://southbysea.com/catalog", // url to share, default current url
						text: "Check out South by Sea's catalog!", // for twitter
						via : "sxscollege", // for twitter
						title: "Check out South by Sea's catalog!", // for stumbleupon, reddit, linkedin
						summary : "Check out South by Sea's catalog!", // for reddit
					}
				};
				$('#book').wowBook(bookOptions);
			});
			$(window).trigger('resize');
		}
	},

	catalogImages: function() {
		//init book
		if ($('#book').length > 0){
			head.load('/assets/js/wow_book/wow_book.min.js', function(){
				var bookOptions = {
					//forceBasicPage: true,
					pageNumbers: false,
					centeredWhenClosed : true,
					mouseWheel: true,
					doubleClickToZoom: true,

					//pagesInMemory: 6,

					container: true,
					containerWidth: '100%',
					containerPadding: '20px 5px 20px 5px',

					navControls: false,
					responsiveNavControls: false,

					height: ((($('.book-container').width() - 10) / 2) * 7) / 9.1,
					width : $('.book-container').width() - 10,
					handleWidth: 100,
					responsiveHandleWidth : 50,
					responsiveSinglePage: function( book ){
						return $('.book-container').width() < 768;
					},

					toolbar : "lastLeft, left, right, lastRight, slideshow, zoomin, zoomout, flipsound, share, fullscreen",
					toolbarPosition: "bottom", // default "bottom"
					thumbnailsPosition : 'left',

					//flipSound: false,
					flipSoundFile : ["page-flip.mp3", "page-flip.ogg" ],
  					flipSoundPath : "/assets/js/wow_book/sound/",

					share: "facebook twitter, reddit",
					shareParams: {
						url : "https://southbysea.com/catalog", // url to share, default current url
						text: "Check out South by Sea's catalog!", // for twitter
						via : "sxscollege", // for twitter
						title: "Check out South by Sea's catalog!", // for stumbleupon, reddit, linkedin
						summary : "Check out South by Sea's catalog!", // for reddit
					}
				};

				$('#book').wowBook(bookOptions);
			});

			$(window).trigger('resize');
		}
	},

	home: function() {
		$('.slide-container').slick({
			arrows: false,
			dots: false,
			autoplay: true,
			infinite: true,
			autoplaySpeed: 6500,
			speed: 1000,
			fade: true,
			cssEase: 'linear',
			pauseOnHover: false,
			pauseOnFocus: false,
		});
	}
};

SouthBySea.Designs = {
	init: function() {
		SouthBySea.Designs.filters();
	},

	designPage: function() {
		//disable right click
		$('img.main-product-img').on('contextmenu', function(e){
			return false;
		});
	},

	filters: function() {
		var $form = $('#design-filter-form');
		$form.find('select[name="filters"]').multiselect({
			includeSelectAllOption: false,
			enableCaseInsensitiveFiltering: true,
			numberDisplayed: 1,
			maxHeight: 200,
			enableFiltering: true,
			buttonWidth: '100%',
			includeSelectAllOption: false,
			buttonContainer: '<div class="btn-group filter-group" />',
			buttonClass: 'btn btn-default filter-btn',
			nonSelectedText: 'filter',
			allSelectedText: 'no more filters left',
			filterPlaceholder: 'search',
			buttonText: function(options, select) {
				if (options.length === 0) {
					return 'filter';
				}  else if (options.length > this.numberDisplayed) {
					return options.length + ' ' + this.nSelectedText;
		   		} else {
					var labels = [];
					options.each(function() {
						if ($(this).attr('label') !== undefined) {
							labels.push($(this).attr('label'));
						}
						else {
							labels.push($(this).html());
						}
					});
					return labels.join(', ') + ' ';
				}
			},
			onChange: function(option, checked, select) {
				//auto submit the form
				$form.submit();
			}
		});
	}
};

SouthBySea.Products = {
	colors: function() {
		//define our selectors
		var $mainImg = $('#main-img');
		var $defaultImg = $('#default-img');
		var $colors = $('#colors').find('li');

		//when a color is clicked
		$colors.on('click', function(e){
			//toggle active class
			$colors.removeClass('active');
			$(this).addClass('active');

			//populate form as well as color indicator
			var $form = $('#proof-request-form');
			$form.find('input[name="colors"]').val($(this).attr('data-alt'));
			$('#color-label').removeClass('hide').find('#color-selected').html($(this).attr('data-alt'));

			//if there an image for this color use it, else use default
			if ($(this).attr('data-img') != '')
			{
				var alt = ($(this).attr('data-alt') != '')? $mainImg.attr('alt') + ' - ' + $(this).attr('data-alt') : $mainImg.attr('alt');
				$mainImg.attr('src', $(this).attr('data-img'));
				$mainImg.attr('alt', alt);
			} else {
				$mainImg.attr('src', $defaultImg.attr('data-img'));
				$mainImg.attr('alt', $defaultImg.attr('data-alt'));
			}
		});
	}
};

SouthBySea.CampusReps = {
	autosuggest: function() {
		var $form = $('#campus-rep-search');
		var $field = $form.find('#school-autosuggest');

		//init autocomplete
		$field.autocompleter({
			cache: false,
			highlightMatches: true,
			limit: 5,
			minLength: 3,
			source: '/campus-managers/json/schools',
			offset: 'results',
			customLabel: 'text',
			customValue: 'id',
			callback: function(value, index) {
				SouthBySea.CampusReps.populate(value);
			}
		});

		$form.on('submit', function(e){
			e.preventDefault();
			SouthBySea.CampusReps.populate($field.val());
		});
	},

	populate: function(school) {
		//define the fields
		var $template = $("#results-template");
		var $populate = $("#school-listing");

		//call different url depending on value
		var url = (school != '')? '/campus-managers/json/reps' : '/campus-reps/json/all-reps';

		//make an ajax call to get the data we need
		$.ajax({
			type: 'get',
			url: url,
			data: {
				q: school
			},
			dataType: 'json',
			success: function(response) {

				var template = Handlebars.compile($template.html());
				$populate.html(template(response));
			},
			error: function() {
				alert('An error occurred. Please try again.');
			}
		});
	}
};

SouthBySea.Search = {
	selectors: {
		form: '#search-form',
		showMore: 'button.show-more',
		types: {
			design: {
				results: '#designs-container'
			},
			product: {
				results: '#products-container'
			}
		}
	},

	init: function() {
		SouthBySea.Search.initTags();
		SouthBySea.Search.initHelpers();
		SouthBySea.Search.hijackForm();
		SouthBySea.Search.hijackLoadMore();
	},

	hijackForm: function() {
		//define form
		var $form = $(SouthBySea.Search.selectors.form);

		//hijack submission
		$form.submit(function(e){
			e.preventDefault();

			//clear out current results
			$.each(SouthBySea.Search.selectors.types, function(type, item){
				var $container = $(item.results);
				$container.html('');
				$container.removeClass('has-results');

				//clear out pages from form
				var $form = $('form[data-type="' + type + '"]');
				$form.find('input[name="p"]').val('');

				//do ajax search
				SouthBySea.Search.loadMore(type);
			});
		});

		//if form has a value on load, submit it
		var $query = $form.find('input[name="q"]');
		if ($query.val() != '')
		{
			$form.submit();
		}
	},

	hijackLoadMore: function() {
		//do search when user clicks on "show more"
		$('body').on('click', SouthBySea.Search.selectors.showMore, function(e){
			e.preventDefault();
			var type = $(this).attr('data-type');
			SouthBySea.Search.loadMore(type);
		});
	},

	loadMore: function(type) {

		//define form & buttons we are using for search
		var $mainForm = $(SouthBySea.Search.selectors.form);
		var $form = $('form[data-type="' + type + '"]');
		var formData = $form.serializeArray();
		formData = formData.concat($mainForm.serializeArray());

		//define our templates/placeholders
		var $template = $("#results-template");
		var $populate = $(SouthBySea.Search.selectors.types[type].results);

		//perform search
		$.ajax({
			type: 'get',
			url: '/search/json/results',
			data: formData,
			dataType: 'json',
			success: function(response) {

				//update page number (used for determining offset)
				$form.find('input[name="p"]').val(response.page);

				//remove current show more button
				$populate.find(SouthBySea.Search.selectors.showMore).remove();

				// compile and apply the main template
				var template = Handlebars.compile($template.html());
				$populate.append(template(response));

				//add class if there are results
				if (response.results.length > 0)
				{
					$populate.addClass('has-results');
				}
			},
			error: function(response) {
				alert(JSON.parse(response.responseText));
			}
		});
	},

	initTags: function() {
		//define form & fields
		var $form = $(SouthBySea.Search.selectors.form);
		var $field = $form.find('input[name="q"]');

		//init autocomplete
		$field.autocompleter({
			cache: false,
			highlightMatches: true,
			limit: 5,
			minLength: '3',
			source: '/search/json/tags',
			combine: function() {
				var $typeField = $form.find('input[name="type"]:checked');

				return {
					type: $typeField.val()
				};
			},
			offset: 'results',
			customLabel: 'text',
			customValue: 'id'
		});
	},

	initHelpers: function() {
		//this is the equivalent of x % y
		Handlebars.registerHelper('everyNth', function(context, every, options) {
			var fn = options.fn, inverse = options.inverse;
			var ret = "";
			if(context && context.length > 0) {
				for(var i=0, j=context.length; i<j; i++) {
					var modZero = i % every === 0;
					ret = ret + fn(_.extend({}, context[i], {
						isModZero: modZero,
						isModZeroNotFirst: modZero && i > 0,
						isLast: i === context.length - 1
					}));
				}
			} else {
				ret = inverse(this);
			}
			return ret;
		});

		Handlebars.registerHelper("moduloIf", function(index_count,mod,block) {
			if(parseInt(index_count)%(mod)=== 0){
				return block.fn(this);
			}
		});

		Handlebars.registerHelper('compare', function (lvalue, operator, rvalue, options) {
			var operators, result;
			if (arguments.length < 3) {
				throw new Error("Handlerbars Helper 'compare' needs 2 parameters");
			}
			if (options === undefined) {
				options = rvalue;
				rvalue = operator;
				operator = "===";
			}
			operators = {
				'==': function (l, r) { return l == r; },
				'===': function (l, r) { return l === r; },
				'!=': function (l, r) { return l != r; },
				'!==': function (l, r) { return l !== r; },
				'<': function (l, r) { return l < r; },
				'>': function (l, r) { return l > r; },
				'<=': function (l, r) { return l <= r; },
				'>=': function (l, r) { return l >= r; },
				'typeof': function (l, r) { return typeof l == r; }
			};
			if (!operators[operator]) {
				throw new Error("Handlerbars Helper 'compare' doesn't know the operator " + operator);
			}
			result = operators[operator](lvalue, rvalue);

			if (result) {
				return options.fn(this);
			} else {
				return options.inverse(this);
			}
		});
	}
};

SouthBySea.Loader.init();
