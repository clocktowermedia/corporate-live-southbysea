var Site = {
	Loader: null
};

/**
 * Site.Loader
 *
 * Run every page through here and feed it only the JS it needs.
 */
Site.Loader = {
	init: function() {

		// Fire off JS based on URI segment
		var uri = document.location.pathname.split('/');

		//load the site js that's used on every page
		head.js('/assets/js/skeleton.js');
		head.js('/assets/js/south-by-sea.js');

		//load custom js for certain sections, hurray
		if (uri[1] == 'preorder')
		{
			head.js('/assets/modules/ecommerce/js/cart.js');
		}
	}
};

/**
 * We will load some scripts before the page is loaded...
 */
head.js(
   {jquery: '//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js'},
   {bootstrap: '//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js'}
);

/**
 * Once Jquery is ready, load some other stuff
 */
head.ready('jquery', function() {

	//load other plugins
	head.js('/assets/js/plugins.js');

	//check to see if the page has loaded, if so...
	$(function() {
		Site.Loader.init();
	});
});