/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For the complete reference:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	/*
	config.toolbar = [
		{ name: 'document', groups: ['document'], items: ['Print'] },
	];
	*/

	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.toolbarGroups = [
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
		{ name: 'styles' },
		{ name: 'colors' },
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
		'/',
		{ name: 'tools' },
		{ name: 'insert' },
		{ name: 'links' },
		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ]}
	];

	//remove plguin
	config.removePlugins = 'uploadcare,pastefromword';

	// Remove some buttons, provided by the standard plugins, which we don't
	// need to have in the Standard(s) toolbar.
	config.removeButtons = 'Styles,Font,Subscript,Superscript';

	// Se the most common block elements.
	config.format_tags = 'p;h1;h2;h3;h4;h5;h6';

	// Make dialogs simpler.
	config.removeDialogTabs = 'image:advanced;link:advanced';

	//set skin to something more colorful
	config.skin = 'moonocolor';

	/*
	//word doc stuff
	config.pasteFromWordPromptCleanup = true;
	config.pasteFromWordNumberedHeadingToList = true;
	config.pasteFromWordRemoveFontStyles = true;
	config.pasteFromWordRemoveStyles = true;
	*/
	config.forcePasteAsPlainText = true;
	config.removeFormatTags = 'b,big,code,del,dfn,em,font,i,ins,kbd,q,samp,small,span,strike,strong,sub,sup,tt,u,var,div';
	config.removeFormatAttributes = 'class,style,lang,width,height,align,hspace,valign';

	//open up pasting prompt
	config.keystrokes = [
    	[CKEDITOR.CTRL + 86, 'pastetext'], // Ctrl+V
	];

	//use kcfinder
	config.filebrowserBrowseUrl = '/assets/js/kcfinder/browse.php?opener=ckeditor&type=files';
	config.filebrowserImageBrowseUrl = '/assets/js/kcfinder/browse.php?opener=ckeditor&type=images';
	config.filebrowserFlashBrowseUrl = '/assets/js/kcfinder/browse.php?opener=ckeditor&type=flash';
	config.filebrowserUploadUrl = '/assets/js/kcfinder/upload.php?opener=ckeditor&type=files';
	config.filebrowserImageUploadUrl = '/assets/js/kcfinder/upload.php?opener=ckeditor&type=images';
	config.filebrowserFlashUploadUrl = '/assets/js/kcfinder/upload.php?opener=ckeditor&type=flash';
};