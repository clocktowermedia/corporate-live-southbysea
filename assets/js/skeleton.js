var Skeleton = {
	Controller: null,
	System: null
};

/**
 * Skeleton.Controller
 *
 * Run every page through here and feed it only the JS it needs.
 */
Skeleton.Controller = {
	init: function() {

		// Fire off JS based on URI segment
		var uri = document.location.pathname.split('/');

		//always initiate these functions
		Skeleton.System.init();
	}
};

Skeleton.System = {

	//initiate a bunch of stuff
	init: function() {
		Skeleton.System.ajaxModals();
	},

	//loads a page via ajax in a bootstrap modal
	ajaxModals: function () {
		// Hijack links/buttons that make ajax call to get modal
		$('body').on('click', '[data-toggle="ajax-modal"]:not(.btn-disabled)', function (e) {
			e.preventDefault();
			var $link = $(this);
			var modalTitle = ($link.attr('title')) ? $link.attr('title') : $link.text();
			var html = '';

			$.ajax({
				url: (typeof $link.attr('data-link') == 'undefined')? $link.attr('href') : $link.attr('data-link'),
				dataType: 'html',

				success: function (data) {
					html = data;
				},

				error: function (data, status, error) {
					modalTitle = '<h3>Oops! <small>Looks like something went wrong</small></h3>';
					html = '<div class="modal-body"><p>Sorry for the inconvenience, but it looks like your request didn\'t make it through. Please notify us about the error if this is a recurring issue.</p></div><div class="modal-footer"><a href="/contact" class="btn btn-primary">Contact Us</a><a href="#" class="btn" data-dismiss="modal">Close</a></div>';
				},

				complete: function () {
					// Add title from link text
					Skeleton.System.populateModal(modalTitle, html);
				}
			});
		});
	},

	populateModal: function(modalTitle, html) {
		$('#modal-layout').html(
			'<div class="modal-dialog">' +
				'<div class="modal-content">' +
					'<div class="modal-header">' +
						'<a class="close" data-dismiss="modal">x</a>' +
						'<h3 class="modal-title">' + modalTitle + '</h3>' +
					'</div>' +
					'<div class="modal-body">' +
						html +
					'</div>' +
				'</div>' +
			'</div>'
		).modal('show');
	},

	isBreakpoint: function (alias) {
		return $('.device-' + alias).is(':visible');
	}
};

/**
 * On Ready Event
 * Simple: Fire the appropriate init methods based on the url.
 * DO NOT STUFF JS CODE HERE. PUT IT WHERE IT BELONGS
 */
$(function() {
	Skeleton.Controller.init();
});