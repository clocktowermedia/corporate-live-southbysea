var SliderImages = {
	Loader: null,
	Upload: null,
	Markup: null,
	Actions: null
};

/**
 * Admin.Loader
 *
 * Run every page through here and feed it only the JS it needs.
 */
SliderImages.Loader = {
	init: function() {

		// Fire off JS based on URI segment
		var uri = document.location.pathname.split('/');

		//load images
		SliderImages.Markup.refresh(uri);
		SliderImages.Actions.init(uri);

		//hijack form submission
		SliderImages.Upload.init(uri);
	}
};

SliderImages.Upload = {
	init: function(uri) {
		SliderImages.Upload.addListeners();
		SliderImages.Upload.hijackForm(uri);
	},

	addListeners: function() {
		$('#upload-btn').on('click touch', function(){
			$('#upload-trigger').click();
		});
	},

	hijackForm: function(uri) {
		//hijack form to allow file uploading
		$('#upload-photos').dmUploader({
			method: 'POST',
			maxFileSize: 0,
			allowedTypes: 'image/*',
			extFilter: 'jpg;png;gif',
			url: '/admin/pages/json/upload/' + uri[4],
			dataType: 'json',
			extraData: {
				pageID: uri[4]
			},
			fileName: 'userfile',
			onNewFile: function(id, file){
			},
			onComplete: function(){
				//refresh and show new images
				SliderImages.Markup.refresh(uri);
			},
			onUploadProgress: function(id, percent){
				var percentStr = percent + '%';
			},
			onUploadSuccess: function(id, data){
				//console.log('Server Response for file #' + id + ': ' + JSON.stringify(data));
			},
			onUploadError: function(id, message){
				alert('An error occurred: ' + message);
			},
			onFileTypeError: function(file){
				alert('File \'' + file.name + '\' is not an accepted file type.');
			},
			onFileSizeError: function(file){
				alert('File \'' + file.name + '\' exceeds the file size limit.');
			},
			onFallbackMode: function(message){
				alert('Browser not supported: ' + message + '.');
			}
		});
	}
};

SliderImages.Markup = {
	refresh: function(uri) {

		//define our templates/placeholders
		var $template = $("#slider-images-listing");
		var $populate = $("#slider-images");

		$.ajax({
			type: 'get',
			url: '/admin/pages/json/images/' + uri[4],
			dataType: 'json',
			success: function(response) {

				// compile and apply the main template
				var template = Handlebars.compile($template.html());
				$populate.html(template(response));

				SliderImages.Markup.popovers();
				SliderImages.Actions.reorder(uri);
			},
			error: function(response) {
				$populate.html(JSON.parse(response.responseText));
			}
		});
	},

	popovers: function(action) {
		var $popovers = $("#slider-images").find('[data-toggle="popover"]');

		//perform popover action
		if (typeof action !== "undefined" && action !== '')
		{
			$popovers.popover(action);
		}

		$popovers.popover({
			html: true,
			placement: 'auto',
			trigger: 'click focus',
			content: function() {
				//get id
				var id = $(this).attr('data-id');

				//get content
				var html = $('#popover-content-' + id).html();
				return html;
			}
		});

		$popovers.on('show.bs.popover', function() {
			//hide other popovers
			$popovers.not(this).popover('hide');
		});
	}
};

SliderImages.Actions = {
	init: function(uri) {
		SliderImages.Actions.hijackDelete(uri);
		SliderImages.Actions.hijackEdit(uri);
	},

	hijackDelete: function(uri) {
		$('body').on('click', 'a.delete-image', function(){
			//get id
			var imageID = $(this).attr('data-id');

			//try to delete the image
			$.ajax({
				type: 'POST',
				url: '/admin/pages/json/delete-image',
				dataType: 'json',
				data: {
					imageID: imageID
				},
				success: function(response) {

					if (response.status == 'ok')
					{
						Cms.System.showAlert('success', response.message);
						SliderImages.Markup.refresh(uri);
					} else {
						Cms.System.showAlert('error', response.message);
					}
				},
				error: function(response) {
					alert('An error occurred. Please try again.');
				},
				complete: function() {
					SliderImages.Markup.popovers('hide');
				}
			});
		});
	},

	hijackEdit: function() {
		//when they click the edit button, show an edit modal
		$('body').on('click', 'a.edit-image', function(){
			//get id
			var imageID = $(this).attr('data-id');
			
			//show appropriate modal
			$('#image-modal-' + imageID).modal('show');
		});
	},

	hijackEditSubmit: function() {
		//hijack edit submission
		$('form.image-modal-form').on('submit', function(e){
			e.preventDefault();
		});
	},

	reorder: function(uri) {
		$('#slider-images').find('div.row').sortable({
			opacity: '0.5',
			items: '> div.thumbnail-img',
			update: function(e, ui) {
				serial = $(this).sortable('serialize');
				$.ajax({
					url: '/admin/pages/json/reorder-images/' + uri[4],
					type: 'POST',
					data: serial,
					success: function(feedback) {
					},
					error: function() {
						alert('An error occurred. Please try again.');
						return false;
					}
				});
			}
		});
	}
};

SliderImages.Loader.init();