var Admin = {
	Loader: null
};

/**
 * Admin.Loader
 *
 * Run every page through here and feed it only the JS it needs.
 */
Admin.Loader = {
	init: function() {

		// Fire off JS based on URI segment
		var uri = document.location.pathname.split('/');

		//load the site js that's used on every page
		head.js('/assets/admin/js/cms.js?v=' + Math.round(+new Date()/1000), '/assets/admin/js/south-by-sea.js?v=' + Math.round(+new Date()/1000));
	}
};

/**
 * We will load some scripts before the page is loaded...
 */
head.js(
   {jquery: '//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js'},
   {jqueryui: '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js'},
   {bootstrap: '//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js'},
   {plugins: '/assets/admin/js/plugins.js'}
);

/**
 * Once Jquery is ready, load some other stuff
 */
head.ready('jquery', function() {

	//load other plugins
	head.js('/assets/admin/js/admin-theme.js');

	//check to see if the page has loaded, if so...
	$(function() {
		Admin.Loader.init();
	});
});