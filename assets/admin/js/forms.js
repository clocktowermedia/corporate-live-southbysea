var Forms = {
	Loader: null,
	Datatables: null,
	Fields: null,
	Totals: null
};

/**
 * Forms.Loader
 *
 * Run every page through here and feed it only the JS it needs.
 */
Forms.Loader = {
	init: function() {

		// Fire off JS based on URI segment
		var uri = document.location.pathname.split('/');

		//run datatable
		if (uri[3] == 'read')
		{
			Forms.Fields.hideLinkedFields();
		}

		if (uri[3] == null || uri[3] == '')
		{
			Forms.Datatables.init(uri);
		}

		if (uri[3] == 'search')
		{
			Cms.Datatables.initBootstrap();
			$('table.datatable').dataTable();
			Forms.Fields.search();
		}
	}
};

Forms.Fields = {
	init: function() {

		Forms.Fields.checkAll();
		Forms.Fields.mailboxCheckboxes();
		Forms.Totals.init();
		Forms.Fields.search();
	},

	search: function() {
		var $select = $('select[name="formUrl"]');
		var $field = $('select[name="field"]');
		var fieldVal = (typeof $field.attr('data-value') !== "undefined" && $field.attr('data-value') != '')? $field.attr('data-value') : '';

		$select.on('change', function(){
			$field.select2('destroy');
			$field.html('');
			$field.append(new Option('', ''));

			$.getJSON('/admin/forms/json/get-fields/' + $(this).val(), function(data) {
				if (data.status === 'ok')
				{
					$.each(data.results, function(key, val) {
						if (fieldVal == key)
						{
							$field.append(new Option(val, key, true, true));
						} else {
							$field.append(new Option(val, key));
						}
					});
				}
			});

			$field.select2({
				placeholder: $field.attr('placeholder')
			});
		});
		$select.trigger('change');

		//set default value
		if (typeof $field.attr('data-value') !== "undefined" && $field.attr('data-value') != '')
		{
			$field.select2('data', {
				id: $field.attr('data-value'), text: $field.attr('data-value-text')
			});
		}
	},

	checkAll: function() {

		//check all messages on page
		$('#check-all').on('change', function(e){

			//determine if main checkbox is checked
			var isChecked = $(this).is(':checked');

			if (isChecked) {
				$('.mailbox-checkbox').prop('checked', true).icheck('checked');
			} else {
				$('.mailbox-checkbox').prop('checked', false).icheck('unchecked');
			}

			//add listeners for checkboxes
			Forms.Datatables.disableUI();
		});
	},

	mailboxCheckboxes: function() {

		//disable pagination and search if checkboxes are checked
		$('.mailbox-checkbox').on('change', function(){
			Forms.Datatables.disableUI();
		});
	},

	getCheckedCount: function() {
		return $('.mailbox-checkbox:checked').length;
	},

	clearCheckboxes: function() {
		$('input[type="checkbox"]').prop('checked', false).icheck('unchecked');
		Forms.Datatables.disableUI();
	},

	hideLinkedFields: function() {
		//loop through all fields that need to be hidden and have a linked field
		var $linkedFields = $('input.show-if');
		if ($linkedFields.length > 0)
		{
			$linkedFields.each(function(e){

				//hide the field
				var $currentField = $(this);
				var $parentContainer = $currentField.parents('.form-group');
				$parentContainer.addClass('hide');

				//find the linked field
				var linkedFieldName = $currentField.attr('data-linked-field');
				var $linkedField = $('select[name="' + linkedFieldName + '"]');

				//get comparison values
				var comparisonValues = JSON.parse($currentField.attr('data-show-when').replace(/'/g, '"'));

				//add change method for linked field
				$linkedField.on('change', function(e){
					//get value
					var value = $(this).val();

					//if value
					if (comparisonValues.indexOf(value) !== -1)
					{
						$parentContainer.removeClass('hide');
						if (typeof $currentField.attr('data-show-req') !== 'undefined' && $currentField.attr('data-show-req') == true)
						{
							$currentField.attr('required', 'required');
						}
					} else {
						$parentContainer.addClass('hide');
						if (typeof $currentField.attr('data-show-req') !== 'undefined' && $currentField.attr('data-show-req') == true)
						{
							$currentField.removeAttr('required');
						}
					}
				});

				//trigger change
				$linkedField.trigger('change');
			});
		}
	}
};

Forms.Datatables = {
	responsiveHelper: null,
	container: '#inbox-container',

	init: function(uri) {

		//define some stuff we will need
		var $filterContainer = $('#mailbox-filters');
		var $sortContainer = $('#mailbox-sort');
		var $typeContainer = $('#mailbox-form-types');

		//init bootstrap styling of tables
		Cms.Datatables.initBootstrap();

		//define our breakpoints
		//Forms.Datatables.responsiveHelper = undefined;
		var breakpointDefinition = {
			tablet: 1024,
			phone : 480
		};

		//kill old datatables instance and init new one
		var $tableElement = $('table.datatable');
		var mailboxTable = $tableElement.dataTable({
			"iDisplayLength": 50,
			"bDestroy": true,
			"aaSorting": [],
			"bSort": false,
			"bAutoWidth": false,
			"bProcessing": true,
			"bServerSide": true,
			"sServerMethod": "POST",
			"sAjaxSource": "/admin/forms/datatables/data",
			"fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
				aoData.push({"name":"status", "value": $filterContainer.attr('data-status')});
				aoData.push({"name":"sort", "value": $sortContainer.attr('data-sort')});
				//aoData.push({"name":"formUrl", "value": $typeContainer.attr('data-value')});

				oSettings.jqXHR = $.ajax({
					"dataType": 'json',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					"success": fnCallback
				});
			},
			"sDom": "<'row'<'col-xs-12'>r>" + "t" + "<'row'<'col-xs-6'i><'col-xs-6'p>>",
			"aoColumns": [
				{'sClass': 'small-col', 'bSortable': false},
				{'sClass': 'name', 'bSortable': false},
				{'sClass': 'subject', 'bSortable': false},
				{'bSortable': false},
				{'sClass': 'time', 'bSortable': false},
			],
			fnPreDrawCallback: function () {
				// Initialize the responsive datatables helper once.
				if (!Forms.Datatables.responsiveHelper) {
					Forms.Datatables.responsiveHelper = new ResponsiveDatatablesHelper($tableElement, breakpointDefinition);
				}
			},
			fnDrawCallback : function (oSettings) {
				Forms.Datatables.responsiveHelper.respond(Forms.Datatables.container);
				Forms.Fields.init();
				Cms.System.initIcheck();
			},
			fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				Forms.Datatables.responsiveHelper.createExpandIcon(nRow);

				//save the row
				var $currentRow = $(nRow);

				//add read or unread class
				$currentRow.addClass(aData[5]);

				//define all the td's except the first one, since we want to modify this
				var $clickable = $currentRow.find('td').not(':first-child');
				$clickable.on('click', function() {
					window.open('/admin/forms/read/' + aData[6], '_blank');
				});
				$clickable.css('cursor', 'pointer');

				//return the row
				return nRow;
			}
		});

		//on winow resize, make the table responsive, delay half a second since that's the transition time for the sidebar menu
		/*
		$(window).resize(function() {
			setTimeout(function(){
				Forms.Datatables.responsiveHelper.respond(Forms.Datatables.container);
			}, 500);
		});
		*/

		//on hide/show sidebar, make the table responsive, delay half a second since that's the transition time for the sidebar menu
		$(window).on('show_sidenav', function(e, params){
			setTimeout(function(){
				Forms.Datatables.responsiveHelper.respond(Forms.Datatables.container);
			}, 500);
		});

		//set to reload every 5 minutes
		var refresh = 60000 * 5;
		setInterval( function () {
			//redirect if we get a weird response
			$.ajax({
				url: '/admin/forms/datatables/data',
				dataType: 'json',
				success: function (response) {
					if (response.hasOwnProperty('sColumns') == false) {
						window.location.href = '/admin/dashboard';
					}
				}
			});

			//refresh forms table
			mailboxTable.fnStandingRedraw();

		}, (refresh));

		//search form
		var $searchForm = $('#mailbox-search-form');
		/*
		$searchForm.on('submit', function(e){
			//prevent default behavior
			e.preventDefault();

			//get search string
			var mailboxQuery = $(this).find('input[name="mailbox-query"]').val();

			//filter by search string
			mailboxTable.fnFilter(mailboxQuery);
		});
		*/

		$searchForm.find('input[name="mailbox-query"]').on('keyup', function(e){
			//filter by search string
			mailboxTable.fnFilter($(this).val());
		});

		//filter by type
		$typeContainer.find('li').find('a').on('click', function(e){
			e.preventDefault();

			//uncheck check all
			var $checkAll = $('#check-all');
			if ($checkAll.is(':checked'))
			{
				$checkAll.prop('checked', false).icheck('unchecked');
			}

			//remove active class from others and uncheck any boxes
			$typeContainer.find('li').each(function(){
				$(this).removeClass('active');
			});

			//set current to active
			$(this).parent('li').addClass('active');

			//get text we will use for filtering
			var filter = $(this).attr('data-filter-category');
			$('#mailbox-filter-form').find('select[name="formUrl"]').find('option:contains('+filter+')').prop('selected', true);

			//filter datatables
			if (filter != '') {
				mailboxTable.fnFilter(filter, 3);
			} else {
				mailboxTable.fnFilter('', 3);
				mailboxTable.fnFilter('');
			}
		});

		//filter by read/unread
		$filterContainer.find('a[data-view]').on('click', function(e){
			//set status
			$filterContainer.attr('data-status', $(this).attr('data-view'));
			$filterContainer.find('li').removeClass('active');
			$(this).parent('li').addClass('active');

			//reload the table
			mailboxTable.fnStandingRedraw();
		});

		//sort by selection
		$sortContainer.find('a[data-sort]').on('click', function(e){
			//set status
			$sortContainer.attr('data-sort', $(this).attr('data-sort'));
			$sortContainer.find('li').removeClass('active');
			$(this).parent('li').addClass('active');

			//reload the table
			mailboxTable.fnStandingRedraw();
		});

		//make an ajax call for adjusting status if applicable
		$('#mailbox-actions').find('a[data-hijack]').on('click', function(e){

			//make sure checkboxes are actually checked
			if (Forms.Fields.getCheckedCount() > 0)
			{
				//grab checked checkboxes as data
				var formData = $('input[name="messages[]"]').serialize();

				//get status
				var status = $(this).attr('data-hijack');

				//make an ajax call
				$.ajax({
					url: '/admin/forms/json/update-status/' + status,
					type: 'POST',
					data: formData,
					dataType: 'json',
					success: function(response) {

						if (response.status == 'ok')
						{
							//reload the table
							mailboxTable.fnStandingRedraw();

							//update read/unread in sidebar
							Cms.Totals.unreadMessages();
							Forms.Totals.categoryTotals();

						} else {

							//alert user that an error occurred
							alert(response.message);
						}
					},
					error: function(response) {

						//show error message
						alert('An error occurred, please refresh the page and try again.');
					},
					complete: function() {

						//clear checkboxes
						Forms.Fields.clearCheckboxes();
					}
				});

			} else {

				alert('No items are selected.');
			}
		});

		//enable refresh button
		$('#refresh-inbox').on('click', function(){
			mailboxTable.fnStandingRedraw();
		});
	},

	disableUI: function() {

		//if we have anything checked, disable search and pagination links
		if (Forms.Fields.getCheckedCount() > 0)
		{
			//disable pagination
			$('ul.pagination').find('li, li a').not('.disabled').each(function(){
				$(this).addClass('disabled').addClass('disable-init');
			});

			//disable search
			$('#mailbox-search-form').find('input, button').attr('disabled', 'disabled');

			//disable clicking
			$('ul.pagination').find('a').on('click', function(e){
				if ($(this).hasClass('disabled')) {
					e.preventDefault();
					return false;
				}
			});

		} else {

			//enable pagination
			$('ul.pagination').find('.disable-init').each(function(){
				$(this).removeClass('disabled').removeClass('disable-init');
			});

			//enable search
			$('#mailbox-search-form').find('input, button').removeAttr('disabled');
		}
	}
};

Forms.Totals = {
	init: function() {
		Forms.Totals.categoryTotals();
	},

	categoryTotals: function() {

		var postData = {
			returnType: 'all'
		};

		//make an ajax call to get category totals
		$.ajax({
			url: '/admin/forms/json/get-unread-count',
			type: 'POST',
			data: postData,
			dataType: 'json',
			success: function(response) {

				if (response.counts)
				{
					$(response.counts).each(function(key, item){

						var $countContainer = $('#mailbox-form-types').find('a[data-filter-category="' + item.formName + '"]');
						$countContainer.find('span.unread-count').html('(' + item.unreadCount + ')');

					});
				}
			}
		});
	}
};

//check to see if the page has loaded, if so...
$(function() {
	Forms.Loader.init();
});
