/*!
 * LTE Admin Theme
 **/

/*
 * Global variables. If you change any of these vars, don't forget
 * to change the values in the less files!
 */
var left_side_width = 220; //Sidebar width in pixels

//Make sure the body tag has the .fixed class
function fix_sidebar() {
	if (!$('body').hasClass('fixed')) {
		return;
	}
}

/*
 * Make sure that the sidebar is streched full height
 * ---------------------------------------------
 * We are gonna assign a min-height value every time the
 * wrapper gets resized and upon page load. We will use
 * Ben Alman's method (defined below) for detecting the resize event.
 *
 **/
function _fix() {

	//Get window height and the wrapper height
	var height = $(window).height() - $("body > header.header").height();

	//set wrapper to height, get that height
	var $wrapper = $("div.wrapper");
	$wrapper.css("min-height", height + "px");
	var content = $wrapper.height();
	
    //If the wrapper height is greater than the window
	if (content > height)
		//then set sidebar height to the wrapper
		$("aside.left-side, html, body").css("min-height", content + "px");
	else {
		//Otherwise, set the sidebar to the height of the window
		$("aside.left-side, html, body").css("min-height", height + "px");
	}
}

/*
 * SIDEBAR MENU
 * ------------
 * This is a custom plugin for the sidebar menu. It provides a tree view.
 *
 * Usage:
 * $(".sidebar).tree();
 *
 * Note: This plugin does not accept any options. Instead, it only requires a class
 *	   added to the element that contains a sub-menu.
 *
 * When used with the sidebar, for example, it would look something like this:
 * <ul class='sidebar-menu'>
 *	  <li class="treeview active">
 *		  <a href="#>Menu</a>
 *		  <ul class='treeview-menu'>
 *			  <li class='active'><a href=#>Level 1</a></li>
 *		  </ul>
 *	  </li>
 * </ul>
 *
 * Add .active class to <li> elements if you want the menu to be open automatically on page load.
 */
(function($) {
	"use strict";

	$.fn.tree = function() {

		return this.each(function() {
			var btn = $(this).children("a").first();
			var menu = $(this).children(".treeview-menu").first();
			var isActive = $(this).hasClass('active');

			//initialize already active menus
			if (isActive) {
				menu.show();
				btn.children(".fa-angle-left").first().removeClass("fa-angle-left").addClass("fa-angle-down");
			}
			//Slide open or close the menu on link click
			btn.click(function(e) {

				e.preventDefault();
				if (isActive) {
					//Slide up to close menu
					menu.slideUp();
					isActive = false;
					btn.children(".fa-angle-down").first().removeClass("fa-angle-down").addClass("fa-angle-left");
					btn.parent("li").removeClass("active");
				} else {
					//Slide down to open menu
					menu.slideDown();
					isActive = true;
					btn.children(".fa-angle-left").first().removeClass("fa-angle-left").addClass("fa-angle-down");
					btn.parent("li").addClass("active");
				}
			});

			/* Add margins to submenu elements to give it a tree look */
			menu.find("li > a").each(function() {
				var pad = parseInt($(this).css("margin-left")) + 10;

				$(this).css({"margin-left": pad + "px"});
			});

		});
	};

}(jQuery));

/* CENTER ELEMENTS */
(function($) {
	"use strict";
	jQuery.fn.center = function(parent) {
		if (parent) {
			parent = this.parent();
		} else {
			parent = window;
		}
		this.css({
			"position": "absolute",
			"top": ((($(parent).height() - this.outerHeight()) / 2) + $(parent).scrollTop() + "px"),
			"left": ((($(parent).width() - this.outerWidth()) / 2) + $(parent).scrollLeft() + "px")
		});
		return this;
	}
}(jQuery));

/*
 * jQuery resize event - v1.1 - 3/14/2010
 * http://benalman.com/projects/jquery-resize-plugin/
 *
 * Copyright (c) 2010 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */
(function($, h, c) {
	var a = $([]), e = $.resize = $.extend($.resize, {}), i, k = "setTimeout", j = "resize", d = j + "-special-event", b = "delay", f = "throttleWindow";
	e[b] = 250;
	e[f] = true;
	$.event.special[j] = {setup: function() {
			if (!e[f] && this[k]) {
				return false;
			}
			var l = $(this);
			a = a.add(l);
			$.data(this, d, {w: l.width(), h: l.height()});
			if (a.length === 1) {
				g();
			}
		}, teardown: function() {
			if (!e[f] && this[k]) {
				return false;
			}
			var l = $(this);
			a = a.not(l);
			l.removeData(d);
			if (!a.length) {
				clearTimeout(i);
			}
		}, add: function(l) {
			if (!e[f] && this[k]) {
				return false;
			}
			var n;
			function m(s, o, p) {
				var q = $(this), r = $.data(this, d);
				r.w = o !== c ? o : q.width();
				r.h = p !== c ? p : q.height();
				n.apply(this, arguments);
			}
			if ($.isFunction(l)) {
				n = l;
				return m;
			} else {
				n = l.handler;
				l.handler = m;
			}
		}};
	function g() {
		i = h[k](function() {
			a.each(function() {
				var n = $(this), m = n.width(), l = n.height(), o = $.data(this, d);
				if (m !== o.w || l !== o.h) {
					n.trigger(j, [o.w = m, o.h = l]);
				}
			});
			g();
		}, e[b]);
	}}
)(jQuery, this);

//on load
$(function() {
	"use strict";

	//Enable sidebar toggle
	$("[data-toggle='offcanvas']").click(function(e) {
		e.preventDefault();

		//If window is small enough, enable sidebar push menu
		if ($(window).width() <= 992) {
			$('div.row-offcanvas').toggleClass('active');
			$('aside.left-side').removeClass("collapse-left");
			$("aside.right-side").removeClass("strech");
			$('div.row-offcanvas').toggleClass("relative");
		} else {
			//Else, enable content streching
			$('aside.left-side').toggleClass("collapse-left");
			$("aside.right-side").toggleClass("strech");
		}

		//@auth Marissa - trigger the show sidenav event
		$(window).trigger('show_sidenav');
	});

	//Add hover support for touch devices
	$('.btn').bind('touchstart', function() {
		$(this).addClass('hover');
	}).bind('touchend', function() {
		$(this).removeClass('hover');
	});

	//Add collapse and remove events to boxes
	$("[data-widget='collapse']").click(function() {
		//Find the box parent
		var box = $(this).parents(".box").first();

		//Find the body and the footer
		var bf = box.find("div.box-body, div.box-footer");
		if (!box.hasClass("collapsed-box")) {
			box.addClass("collapsed-box");
			bf.slideUp();
		} else {
			box.removeClass("collapsed-box");
			bf.slideDown();
		}
	});

	//add option to hide boxes
	$("[data-widget='remove']").click(function() {
		//Find the box parent
		var box = $(this).parents("div.box").first();
		box.slideUp();
	});

	// Sidebar tree view
	$("section.sidebar").find('li.treeview').tree();

	//Fire upon load
	_fix();
	fix_sidebar();

	//Fire when wrapper is resized
	$(window).resize(function() {
		_fix();
		fix_sidebar();
	});
});