var Menus = {
	Loader: null,
	Pages: null,
	ActionPages: null,
	External: null,
	menuID: null,
	menuContainer: '.menu-sortable'
};

Menus.Loader = {
	init: function() {

		// Fire off JS based on URI segment
		var uri = document.location.pathname.split('/');

		//set menu id
		Menus.menuID = uri[4];

		//show page list
		Menus.Pages.refreshList();

		//init the menu
		Menus.Loader.menu();

		//bind all the actions we need for adding
		Menus.Pages.hijackAdd();
		Menus.ActionPages.hijackAdd();
		Menus.External.hijackAdd();

		//bind whatever we need for items
		Menus.Loader.hijackEdit();
		Menus.Loader.hijackDelete();
	},

	menu: function() {

		//initiate nested sortable on the menu
		$(Menus.menuContainer).nestedSortable({
			maxLevel: 0,
			listType: 'ul',
			handle: 'div',
			items: 'li',
			toleranceElement: '> div',
			forcePlaceholderSize: true,
			placeholder: 'placeholder',
			update: function() {
				//var reorderedItems = JSON.stringify($(Menus.menuContainer).nestedSortable('toHierarchy'));
				var individualItems = $(this).nestedSortable('serialize');
				$.ajax({
					type: 'post',
					url: '/admin/menus/json/save/' + Menus.menuID,
					data: individualItems,
					success: function(response) {
					},
					error: function() {
						alert('An error occurred. Please try again.');
					}
				});
			}
		});
	},

	addItem: function(menuItem, menuItemID) {

		//build html for new item
		var html = '<li id="list_' + menuItemID + '">' +
			'<div class="sortable-container">' +
				'<div class="reorder-title pull-left">' +
					'<i class="fa fa-arrows"></i>' +
					'<a href="' + menuItem.link + '" data-type="' + menuItem.type + '" data-unique="' + menuItem.uniqueID + '">' + menuItem.title + '</a>' +
				'</div>' +
				'<span class="pull-right menu-item-actions">' +
					'<a href="#editLink" data-toggle="modal" title="Edit" class="edit" data-id="' + menuItemID + '"><i class="fa fa-edit"></i></a>' +
					'<a href="" title="Delete" class="delete" data-id="' + menuItemID + '"><i class="fa fa-trash-o"></i></a>' +
				'</span>' +
			'</div>' +
		'</li>';

		$(Menus.menuContainer).append(html).nestedSortable('refresh');
	},

	hijackEdit: function() {
		//store the modal
		var $modal = $('#editLink');

		//hijack the edit link
		$(Menus.menuContainer).on("click", '.edit' ,function(e) {
			e.preventDefault();

			//define the menu item attributes
			var menuItem = {
				'menuItemID': $(this).attr('data-id'),
				'type': $(this).closest('div').find('a[data-type]').attr('data-type'),
				'uniqueID': $(this).closest('div').find('a[data-type]').attr('data-unique'),
				'text': $(this).closest('div').find('a[data-type]').text(),
				'link': $(this).closest('div').find('a[data-type]').attr('href'),
				'classes' : $(this).closest('div').find('a[data-type]').attr('data-classes'),
				'elementID' : $(this).closest('div').find('a[data-type]').attr('data-elementID'),
				'inSitemapXml': $(this).closest('div').find('a[data-type]').attr('data-sitemap')
			};

			//when the modal is shown
			$modal.on('shown.bs.modal', function() {

				//define the form
				var $form = $(this).find('form');

				//define our fields
				var $titleField = $form.find('input[name="title"]');
				var $linkField = $form.find('input[name="link"]');
				var $classField = $form.find('nput[name="classes"]');
				var $elementField = $form.find('nput[name="elementID"]');
				var $sitemapField = $form.find('checkbox[name="inSitemapXml"], input[name="inSitemapXml"]');

				//Append the menu item id to the form action
				$form.attr('action', '/admin/menus/json/edit-item/' + menuItem.menuItemID);

				//Hide the link field if it is not an external link
				if (menuItem.type != 'link')
				{
					//hide the field and disable it, disabling it prevents it from being posted
					$linkField.attr('readonly', 'readonly').attr('disabled', 'disabled').removeAttr('data-validation-type');
					$linkField.parents('.form-group').hide();
				} else {
					//show the field
					$linkField.removeAttr('readonly').removeAttr('disabled').attr('data-validation-type', 'url');
					$linkField.parents('.form-group').show();
				}

				//only show the sitemap field for "actions"
				if (menuItem.type != 'action')
				{
					//hide the field and disable it, disabling it prevents it from being posted
					$sitemapField.attr('readonly', 'readonly').attr('disabled', 'disabled');
					$sitemapField.parents('.form-group').hide();
				} else {
					//show the field
					$sitemapField.removeAttr('readonly').removeAttr('disabled');
					$sitemapField.parents('.form-group').show();
				}

				//populate the fields
				$titleField.attr('value', menuItem.text);
				$linkField.attr('value', menuItem.link);
				$classField.attr('value', menuItem.classes);
				$elementField.attr('value', menuItem.elementID);
				if (menuItem.inSitemapXml == 1)
				{
					$sitemapField.prop('checked', true).icheck('checked');
				} else {
					$sitemapField.prop('checked', false).icheck('unchecked');
				}
			});
		});

		//hijack the add external link button/submission
		$modal.find('form').submit(function(e) {
			e.preventDefault();

			//get the action from the form
			var url = $(this).attr('action');

			//update it in the db, close/refresh the modal
			$.ajax({
				type: 'post',
				url: url,
				data: $(this).serialize(),
				dataType: 'json',
				success: function(response) {

					//close the modal
					$modal.modal('hide');

					//clear the form
					$modal.find('input').attr('value', '');
					$modal.find(':checkbox').each(function() {
						$(this).prop('checked', false).icheck('unchecked');
					});

					//update the menu values
					var $menuContainer = $(Menus.menuContainer);
					$item = $menuContainer.find('#list_' + response.item.menuItemID).find('a[data-type]');
					$item.attr('href', response.item.link);
					$item.text(response.item.title);
					$item.attr('data-classes', response.item.classes);
					$item.attr('data-elementID', response.item.elementID);
					$item.attr('data-inSitemapXml', response.item.inSitemapXml);

					//refresh the menu
					$menuContainer.nestedSortable('refresh');
				}
			});
		});
	},

	hijackDelete: function() {

		//hijack the delete  button
		$(Menus.menuContainer + ', #page-list').on("click", '.delete', function(e) {
			e.preventDefault();

			//define menuitem, set to empty for now
			var menuItem = null;

			if ($(this).hasClass('pagelist'))
			{
				menuItem = {
					'menuItemID': $(this).attr('data-id'),
					'type': $(this).attr('data-type'),
					'uniqueID': $(this).attr('data-unique')
				};
			} else {
				menuItem = {
					'menuItemID': $(this).attr('data-id'),
					'type': $(this).closest('div').find('a[data-type]').attr('data-type'),
					'uniqueID': $(this).closest('div').find('a[data-unique]').attr('data-unique')
				};
			}

			//remove item from markup and then call the save function
			$.ajax({
				type: 'post',
				url: '/admin/menus/json/remove-item/' + Menus.menuID,
				data: menuItem,
				dataType: 'json',
				success: function(response) {
					//store menu container
					var $menuContainer = $(Menus.menuContainer);

					//remove the item from the menu builder
					$('#list_' + response.item.menuItemID).remove();
					$menuContainer.nestedSortable('refresh');

					//save the menu
					var individualItems = $menuContainer.nestedSortable('serialize');
					$.ajax({
						type: 'post',
						url: '/admin/menus/json/save/' + Menus.menuID,
						data: individualItems,
						success: function(response) {

							//refresh the page list or refresh the action list
							if (menuItem['type'] == 'page') {

								Menus.Pages.refreshList();

							} else if (menuItem['type'] == 'action') {

								Menus.ActionPages.refreshList();
							}
						}
					});
				}
			});
		});
	},

	getItemCount: function() {
		var $menuContainer = $(Menus.menuContainer);
		return parseInt($menuContainer.find('li').size());
	}
};

Menus.Pages = {
	init: function() {

	},

	hijackAdd: function() {

		//hijack the add page links
		$('body').on('click', '.addpage', function(e) {
			e.preventDefault();

			//define the menu item attributes, we aren't sending link/title, because those are pulled from cms pages
			var menuItem = {
				'parentID': 0,
				'depth': 1,
				'order':  Menus.Loader.getItemCount() + 1,
				'uniqueID': $(this).parent('li').attr('id').replace('addpage-', ''), /*Page ID is the unique id in this case */
				'type': 'page'
			};

			//add the menu item to the db & remove it from the list
			$.ajax({
				type: 'post',
				url: '/admin/menus/json/add-item/' + Menus.menuID,
				data: menuItem,
				dataType: 'json',
				success: function(response) {

					//add it to the sortable
					Menus.Loader.addItem(response.item, response.item.menuItemID);

					//refresh the page list
					Menus.Pages.refreshList();
				}
			});
		});
	},

	refreshList: function() {

		//define the fields
		var $initiator = $("#page-list-container");
		var $template = $("#page-listing");
		var $populate = $("#list-placeholder");

		//make an ajax call to get the data we need
		$.ajax({
			type: 'get',
			url: '/admin/menus/json/page-list-tree/' + Menus.menuID,
			dataType: 'json',
			success: function(response) {

				// compile and register the "pages" partial template
				Handlebars.compile($template.html());
				Handlebars.registerPartial("pages", $template.html());

				// compile and apply the main template
				var template = Handlebars.compile($initiator.html());
				$populate.html(template(response));

				//re-init delete
				Menus.Loader.hijackDelete();
			},
			error: function() {
				alert('An error occurred. Please try again.');
			}
		});
	}
};

Menus.ActionPages = {
	init: function() {

	},

	hijackAdd: function() {

		//hijack the add module button
		$('body').on('click', '.addaction', function(e) {
			e.preventDefault();

			//remove extra stuff from index links
			if ($(this).attr('data-title') == 'Index' || $(this).attr('data-title') == 'index')
			{
				var link = $(this).attr('data-link').replace('/index','');
				var title = link.charAt(0).toUpperCase() + link.substring(1);
			} else {
				var link = $(this).attr('data-link');
				var title = $(this).attr('data-title');
			}

			//define the menu item attributes
			var menuItem = {
				'link': link,
				'title': title,
				'parentID': 0,
				'depth': 1,
				'order':  Menus.Loader.getItemCount() + 1,
				'uniqueID': $(this).attr('data-link'), /*the link to the module action is the unique id in this case */
				'type': 'action'
			};

			//add the menu item to the db & remove it from the list
			$.ajax({
				type: 'post',
				url: '/admin/menus/json/add-item/' + Menus.menuID,
				data: menuItem,
				dataType: 'json',
				success: function(response) {

					//add it to the sortable
					Menus.Loader.addItem(response.item, response.item.menuItemID);

					//refresh the action list
					Menus.ActionPages.refreshList();
				}
			});
		});
	},

	refreshList: function() {

		//get list of actions via ajax
		$.ajax({
			type: 'get',
			url: '/admin/menus/json/action-list/' + Menus.menuID,
			dataType: 'json',
			success: function(response) {

				//assign actions to a variable
				var moduleActions = response.data.moduleActions;

				//store module list
				var $moduleList = $('#module-list');

				//clear the module actions list(s)
				$moduleList.find('ul.list-group').html('');

				//loop through and re-add the new actions
				$.each(moduleActions, function(i, item) {

					//check to see how many actions we have available
					if ($(item.actions).size() > 0)
					{
						$.each(item.actions, function(x, page) {

							//make the title prettier
							var title = page.replace(/(?:_| |\b)(\w)/g, function(str, p1) {return p1.toUpperCase()}).replace('-', ' ');

							//build html
							var html = '<li class="list-group-item" id="addaction-' + i + '_' + page + '">' +
								'<a data-title="' + title + '" data-link="' + i + '/' + page + '" href="#" class="addaction pull-right">Add</a>' +
								'<strong>' + title + '</strong>' +
								'</li>';

							//append html to the list
							$moduleList.find('#' + i).find('ul').append(html);
						});

					} else {

						//tell the user no actions are available
						var html = '<span class="none-available">No actions available.</span>';
						$moduleList.find('#' + i).find('ul').append(html);
					}
				});
			}
		});
	}
};

Menus.External = {

	hijackAdd: function() {

		//define the modal
		var $modal = $('#addExternalLink');

		//hijack the add external link button/submission
		$modal.find('form').submit(function(e) {
			e.preventDefault();

			//define the menu item attributes
			var menuItem = {
				'link': $(this).find('#link').val(),
				'title': $(this).find('#title').val(),
				'parentID': 0,
				'depth': 1,
				'order': Menus.Loader.getItemCount() + 1,
				'uniqueID': '',
				'type': 'link'
			};

			//add it to the db, close/refresh the modal
			$.ajax({
				type: 'post',
				url: $(this).attr('action'),
				data: menuItem,
				dataType: 'json',
				success: function(response) {
					//close the modal
					$modal.modal('hide');

					//clear the form
					$modal.find(':input').each(function() {
						$(this).val('');
					});

					//add it to the sortable
					Menus.Loader.addItem(menuItem, response.data.menuItemID);
				}
			});
		});
	}
};

Menus.Loader.init();