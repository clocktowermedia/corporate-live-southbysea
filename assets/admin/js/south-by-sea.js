var SouthBySea = {
	Loader: null,
	Functions: null
};

/**
 * SouthBySea.Loader
 *
 * Run every page through here and feed it only the JS it needs.
 */
SouthBySea.Loader = {
	init: function() {

		// Fire off JS based on URI segment
		var uri = document.location.pathname.split('/');

		//init things we always need
		Cms.System.ajaxModals();
		SouthBySea.Functions.openSections();

		//load datatables for admins, users, etc.
		if (uri[1] == 'admin' && (uri[2] != '' && uri[2] != null) && uri[3] == 'categories') {
			Cms.Datatables.initDatatable('/admin/' + uri[2] + '/datatables/' + uri[3]);
		}

		if (uri[1] == 'admin' && uri[2] == 'campus-managers' && uri[3] == 'schools') {
			Cms.Datatables.initDatatable('/admin/' + uri[2] + '/datatables/' + uri[3]);
		}

		if (uri[4] == 'create' || uri[4] == 'edit')
		{
			Cms.System.initWysiwyg();
			Cms.System.setUrlTitle(8);
		}

		if (uri[2] == 'products')
		{
			head.js('/assets/modules/products/js/admin.js');
		}

		if (uri[2] == 'ecommerce')
		{
			head.js('/assets/modules/ecommerce/js/admin.js');
		}

		if (uri[2] == 'designs')
		{
			head.js('/assets/modules/designs/js/admin.js');
		}

		if (uri[2] == 'potentials')
		{
			head.js('/assets/modules/potentials/js/potentials.js');
		}

		if (uri[2] == 'forms' && uri[3] == 'read')
		{
			//$('textarea.cust-textarea').fseditor();
			$('textarea.cust-textarea').trumbowyg({
				btns: []
			});
		}

		if (uri[2] == 'campus-managers' && uri[3] == 'create')
		{
			//school field
			var $form = $('form');
			var $schoolField = $form.find('select[name="schoolID"]');
			if ($schoolField.length > 0)
			{
				//find school name field
				var $schoolName = $form.find('input[name="schoolName"]');

				$schoolField.on('change', function(){
					if ($(this).val() == 'N/A')
					{
						$schoolName.parents('div.form-group').removeClass('hide');
						$schoolName.attr('required', 'required').removeAttr('disabled');
					} else {
						$schoolName.parents('div.form-group').addClass('hide');
						$schoolName.removeAttr('required').attr('disabled', 'disabled');
					}
				});
			}

			//campus rep url
			var $urlField = $form.find('input[name="url"]');
			var $firstName = $form.find('input[name="firstName"]');
			var $lastName = $form.find('input[name="lastName"]');
			$form.find('input[name="firstName"], input[name="lastName"]').on('keyup change', function(e){
				var fullName = $firstName.val() + ' ' + $lastName.val();
				var urlTitle = Cms.System.makeUrlTitle(fullName, 8);
				$urlField.val(urlTitle);
			});
		}

		if (uri[2] == 'campus-managers' && uri[3] == 'edit')
		{
			head.js('/assets/modules/campus_managers/js/admin.js');
		}

		if ((uri[2] == 'products' || uri[2] == 'designs') && (uri[3] == 'create' || uri[3] == 'edit'))
		{
			//on category page
			$('#tags').select2({
				tags: true,
				tokenSeparators: [","],
				createSearchChoice: function (term) {
					return {
						id: $.trim(term),
						text: $.trim(term)
					};
				},
				ajax: {
					url: '/admin/' + uri[2] + '/json/tags',
					dataType: 'json',
					data: function(term, page) {
						return {
							q: term
						};
					},
					results: function(data, page) {
						return {
							results: data
						};
					}
				},

				// Take default tags from the input value
				initSelection: function (element, callback) {
					var data = [];

					function splitVal(string, separator) {
						var val, i, l;
						if (string === null || string.length < 1) return [];
						val = string.split(separator);
						for (i = 0, l = val.length; i < l; i = i + 1) val[i] = $.trim(val[i]);
						return val;
					}
					$(splitVal(element.val(), ",")).each(function () {
						data.push({
							id: this,
							text: this
						});
					});
					callback(data);
				}
			});
		}
	}
};

SouthBySea.Functions = {
	openSections: function() {
		var hash = window.location.hash;
		var $sectionLink = $('[href="' + hash + '"][data-toggle="collapse"]');
		if ($sectionLink.length > 0)
		{
			$sectionLink.trigger('click');
		}
	}
};

SouthBySea.Loader.init();
