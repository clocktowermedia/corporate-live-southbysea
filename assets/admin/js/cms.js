var Cms = {
	Controller: null,
	System: null,
	Pages: null,
	Datatables: null,
	Totals: null,
	Sliders: null
};

/**
 * Cms.Controller
 *
 * Run every page through here and feed it only the JS it needs.
 */
Cms.Controller = {
	init: function() {

		// Fire off JS based on URI segment
		var uri = document.location.pathname.split('/');

		Cms.System.init();

		//load datatables for admins, users, etc.
		if (uri[1] == 'admin' && (uri[2] != '' && uri[2] != null) && (uri[3] == '' || uri[3] == null)) {
			Cms.Datatables.initDatatable('/admin/' + uri[2] + '/datatables/data');
		}

		if ($('table.datatable').length > 0)
		{
			$('table.datatable').each(function(){
				var selector = (typeof $(this).attr('id') !== 'undefined')? '#' + $(this).attr('id') : '';
				Cms.Datatables.initDatatable('', selector);
			});
		}

		//load the function that checks for saving draft vs. publishing
		if (uri[1] == 'admin' && uri[2] == 'pages') {
			Cms.Pages.init();

			if (uri[3] == 'create' || uri[3] == 'edit') {

				if (uri[3] == 'edit')
				{
					head.js('/assets/admin/js/slider-images.js?v=' + Math.round(+new Date()/1000));
				}

				Cms.Pages.tabValidation();
			}

			if (uri[3] == 'restore' && uri[4] > 0) {
				Cms.Datatables.initDatatable('/admin/pages/datatables/archived/' + uri[4]);
			}

			if (uri[3] == 'recycle-bin') {
				Cms.Datatables.initDatatable('/admin/pages/datatables/deleted');
			}
		}

		//load nestedSortables for menu module
		if (uri[1] == 'admin' && uri[2] == 'menus' && uri[3] == 'edit') {
			head.js('/assets/admin/js/menu.js?v=' + Math.round(+new Date()/1000));
		}

		//auto type urls and initiate wysiwyg
		if (uri[1] == 'admin' && (uri[2] != '' && uri[2] != null) && (uri[3] == 'create' || uri[3] == 'edit')) {
			Cms.System.setUrlTitle(8);
		}

		//on forms/mailbox page...load some js
		if (uri[2] == 'forms')
		{
			head.js('/assets/admin/js/forms.js?v=' + Math.round(+new Date()/1000));
		}

		//init roles table
		if (uri[2] == 'admins' && uri[3] == 'roles')
		{
			//init table
			Cms.Datatables.initDatatable('/admin/admins/datatables/roles');
		}

		if (uri[2] == 'admins' && (uri[3] == 'create-role' || uri[3] == 'edit-role'))
		{
			Cms.Roles.init();
		}
	}
};

/**
* Custom Functions Go Here
*/
Cms.System = {
	init: function() {
		Cms.System.forms();

		//init button groups
		if ($('.btn').length > 0)
		{
			$('.btn').button();
		}

		//init styling for checkboxes
		Cms.System.initIcheck();

		//check for disabled links
		Cms.System.disabledLinks();

		//get totals for messages
		Cms.Totals.init();

		//init wysiwyg
		Cms.System.initWysiwyg();
	},

	forms: function(selector) {
		selector = (typeof selector === 'undefined')? 'form:not(.ignore-validation)' : selector;
		var $form = $(selector);

		//init form validation
		if ($form.length > 0)
		{
			$form.validation({
				//successClass: "has-success",
				errorClass: "has-error",
				classHandler: function (el) {
					var $container = el.$element.parent('div[class^="col-"]');
					return ($container.length > 0)? $container : el.$element.closest('div.form-group');
				},
				errorsContainer: function (el) {
					var $container = el.$element.parent('div[class^="col-"]');
					return ($container.length > 0)? $container : el.$element.closest('div.form-group');
				},
				errorsWrapper: '<span class="help-block has-error"></span>',
				errorTemplate: '<span></span>'
			});
			$form.find('input[type="tel"]').mask('(999) 999-9999? x99999');
			$form.find('input.numeric').masking('9#');
			$form.find('input.decimal').priceFormat({
				prefix: '',
				thousandsSeparator: ''
			});
			$form.find('input[name="zip"]').mask('99999?-9999');
			$form.find('select.select2').select2();

			//init date ranges if applicable
			Cms.System.initDateRange();
			Cms.System.initTimeRange();
		}
	},

	fadeAlerts: function() {
		window.setTimeout(function () {
			$(".alert-success").fadeTo(400, 0).slideUp(400, function () {
				$(this).remove();
			});
		}, 4000);
	},

	// WYSIWYG STUFF
	initWysiwyg: function() {

		if ($('textarea.wysiwyg').length > 0)
		{
			//load ckeditor
			head.js('/assets/js/ckeditor/ckeditor.js', function() {

				//make sure ckeditor is defined
				if (typeof(CKEDITOR) === undefined) {
					return;
				}

				//make an ajax call to determine what sort of user they are and call different configs
				var config = '';
				$.ajax({
					url: '/admin/admins/json/is-dev-admin',
					dataType: 'json',
					success: function (data) {
						config = (data.status == 'ok' && data.devAdmin == 'true')? '/assets/js/ckeditor/admin-config.js' : '/assets/js/ckeditor/config.js';
					},

					error: function (data, status, error) {
						config = '/assets/js/ckeditor/config.js';
					},

					complete: function() {
						//loop through all textareas...
						$('textarea.wysiwyg').each(function() {

							//if this is a short wysiwyg
							var currentConfig = ($(this).hasClass('short') === true)? '/assets/js/ckeditor/simple-config.js' : config;

							//grab the field
							var instance = $(this).attr('name');
							var id = $(this).attr('id');

							//destroy any pre-existing instances
							if (CKEDITOR.instances[instance]) {
								CKEDITOR.instances[instance].destroy();
							}

							//init ckeditor
							var editor = CKEDITOR.replace(instance, {customConfig : currentConfig});

							//add a clearfix for styling purposes
							$(this).after('<div class="clearfix"></div>');
						});
					}
				});
			});
		}
	},

	//takes a string and turns into a url friendly one
	makeUrlTitle: function(text, wordCount) {
		text.replace(/[\w]+/g, ' ');
		words = text.split(' ', wordCount).join(' ').replace(/^ +| +$/g, '');
		return words.toLowerCase().replace(/[^\w ]+/g, '').replace(/ +/g, '-');
	},

	setUrlTitle: function(wordCount) {
		var $urlField = $('input[name="url"], input[name="url_slug"]');

		//only do this if it is not a readonly field
		if ($urlField.not('[readonly]').length > 0) {

			//check for certain fields and auto create url
			$('input[name="title"], input[name="name"]').keyup(function() {
				urlTitle = Cms.System.makeUrlTitle($(this).val(), wordCount);
				$urlField.val(urlTitle).trigger('change');
			});
		}
	},

	//modals for delete confirmation
	deleteModals: function () {
		$('body').on('click', 'a[data-confirm]', function (e) {
			e.preventDefault();

			//get the href & title of the clicked delete link
			var href = $(this).attr('href');
			var text = '<h4>' + $(this).attr('data-confirm') + '</h4>';

			//append the modal to the page and show it
			$('#modal-layout').html(
				'<div class="modal-dialog">' +
					'<div class="modal-content">' +
						'<div class="modal-header">' +
							'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
							'<h3 class="modal-title">Please Confirm</h3>' +
						'</div>' +
						'<div class="modal-body">' +
							text +
						'</div>' +
						'<div class="modal-footer">' +
							'<a class="btn btn-info" href="' + href + '">Confirm</button>' +
							'<a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a>' +
						'</div>' +
					'</div>' +
				'</div>'
			).modal('show');

		});
	},

	//loads a page via ajax in a bootstrap modal
	ajaxModals: function () {
		// Hijack links/buttons that make ajax call to get modal
		$('body').on('click', '[data-toggle="ajax-modal"]:not(.btn-disabled)', function (e) {
			e.preventDefault();
			var $link = $(this);
			var modalTitle = ($link.attr('title')) ? $link.attr('title') : $link.text();

			Cms.System.showModal($link.attr('href'), modalTitle);
		});
	},

	showModal: function (url, modalTitle) {
		var html = '';

		$.ajax({
			url: url,
			dataType: 'html',

			success: function (data) {
				html = data;
			},

			error: function (data, status, error) {
				modalTitle = '<h3>Oops! <small>Looks like something went wrong</small></h3>';
				html = '<div class="modal-body"><p>Sorry for the inconvenience, but it looks like your request didn\'t make it through. Please notify us about the error if this is a recurring issue.</p></div><div class="modal-footer"><a href="/contact" class="btn btn-primary">Contact Us</a><a href="#" class="btn" data-dismiss="modal">Close</a></div>';
			},

			complete: function () {
				// Add title from link text
				$('#modal-layout').html(
					'<div class="modal-dialog">' +
						'<div class="modal-content">' +
							'<div class="modal-header">' +
								'<a class="close" data-dismiss="modal">x</a>' +
								'<h3 class="modal-title">' + modalTitle + '</h3>' +
							'</div>' +
							html +
						'</div>' +
					'</div>'
				).modal('show');
			}
		});
	},

	disabledLinks: function() {
		$('a.disabled').unbind('click').on('click', function(e){
			e.preventDefault();
		});
	},

	showAlert: function(alertType, alertMessage, placementContainer) {
		//set default for append after
		placementContainer = (typeof(placementContainer) !== "undefined")? placementContainer : 'section.content:first';

		//do not do anything if we don't have a type and message
		if (alertType != '' && alertMessage != '')
		{
			//build html
			var html = '<div class="alert alert-' + alertType + ' alert-dismissable">' +
				'<button class="close" data-dismiss="alert">x</button>' +
				alertMessage +
			'</div>';

			//add html
			$(placementContainer).prepend(html);

			//fade alerts automatically
			Cms.System.fadeAlerts();
		}
	},

	initIcheck: function() {
		$('input[type="checkbox"]').not('.no-icheck, .hide').icheck('destroy').icheck({
			checkboxClass: 'icheckbox_minimal'
		});

		$('input[type="radio"]').not('.no-icheck, .hide').icheck('destroy').icheck({
			radioClass: 'iradio_minimal'
		});
	},

	dateDefaults: {'format' : 'yyyy-mm-dd', selectYears: true, selectMonths: true},

	initDateRange: function(selector, dateDefaults) {
		//if date defaults are not defined, set our own
		dateDefaults = (typeof dateDefaults === 'object')? dateDefaults : Cms.System.dateDefaults;
		selector = (typeof selector === 'undefined')? 'input.datepicker' : selector;

		//if we passed a selector, init it for that, otherwise loop through them all, whee
		if (typeof selector !== 'undefined' & selector != '')
		{
			//define this field
			var $startDateField = $(selector);
			var $startDate = $startDateField.pickadate(dateDefaults);
			var startPicker = $startDate.pickadate('picker');

			//try to find the matching end field
			var $endDateField = $('input.datepicker[data-start-field="#' + $startDate.attr('id') + '"]').filter('.end');
			if ($endDateField.length <= 0) { $endDateField = $('input.datepicker.end'); }

			//define the picker and such for end date
			if ($endDateField.length > 0)
			{
				var $endDate = $endDateField.pickadate(dateDefaults);
				var endPicker = $endDate.pickadate('picker');

				// When something is selected, update the “from” and “to” limits.
				startPicker.on('set', function(event) {
					if (event.select) {
						endPicker.set('min', startPicker.get('select'));
					} else if ('clear' in event) {
						//remove min date for end field
						endPicker.set('min', false);
					}
				});
				endPicker.on('set', function(event) {
					if (event.select) {
						startPicker.set('max', endPicker.get('select'));
					} else if ('clear' in event) {
						//remove max time for start picker
						startPicker.set('max', false);
					}
				});
			}

		} else {

			//loop through all the starting date fields
			$('input.datepicker.start').each(function(){

				//define this field
				var $startDateField = $(this);
				var $startDate = $startDateField.pickadate(dateDefaults);
				var startPicker = $startDate.pickadate('picker');

				//try to find the matching end field
				var $endDateField = $('input.datepicker[data-start-field="#' + $startDate.attr('id') + '"]').filter('.end');
				if ($endDateField.length <= 0) { $endDateField = $('input.datepicker.end'); }

				//define the picker and such for end date
				if ($endDateField.length > 0)
				{
					var $endDate = $endDateField.pickadate(dateDefaults);
					var endPicker = $endDate.pickadate('picker');

					// When something is selected, update the “from” and “to” limits.
					startPicker.on('set', function(event) {
						if (event.select) {
							endPicker.set('min', startPicker.get('select'));
						} else if ('clear' in event) {
							//remove min date for end field
							endPicker.set('min', false);
						}
					});
					endPicker.on('set', function(event) {
						if (event.select) {
							startPicker.set('max', endPicker.get('select'));
						} else if ('clear' in event) {
							//remove max time for start picker
							startPicker.set('max', false);
						}
					});
				}
			});
		}
	},

	timeDefaults : {'format' : 'h:i A'},

	initTimeRange: function(selector, timeDefaults) {
		//if date defaults are not defined, set our own
		timeDefaults = (typeof timeDefaults === 'object')? timeDefaults : Cms.System.timeDefaults;
		selector = (typeof selector === 'undefined')? 'input.timepicker' : selector;

		//if we passed a selector, init it for that, otherwise loop through them all, whee
		if (typeof selector !== 'undefined' & selector != '')
		{
			//define this field
			var $startTimeField = $(selector);
			var $startTime = $startTimeField.pickatime(timeDefaults);
			var startPicker = $startTime.pickatime('picker');

			//try to find the matching end field
			var $endTimeField = $('input.timepicker[data-start-field="#' + $startTime.attr('id') + '"]').filter('.end');
			if ($endTimeField.length <= 0) { $endTimeField = $('input.timepicker.end'); }

			//define the picker and such for end date
			if ($endTimeField.length > 0)
			{
				var $endTime = $endTimeField.pickatime({
					formatLabel: function(timeObject) {
						var minObject = this.get('min'),
						hours = timeObject.hour - minObject.hour,
						mins = (timeObject.mins - minObject.mins) / 60,
						pluralize = function( number, word ) {
							return number + ' ' + ( number === 1 ? word : word + 's' );
						},
						html = '';

						if (minObject.hour <= 0 && minObject.mins <= 0 && minObject.time <= 0 && minObject.pick <= 0)
						{
							html = timeDefaults.format;
						} else {
							html = timeDefaults.format + ' <sm!all>(' + pluralize( hours + mins, '!hour' ) + ')</sm!all>';
						}

						return html;
					}
				});
				var endPicker = $endTime.pickatime('picker');

				// When something is selected, update the “from” and “to” limits.
				startPicker.on('set', function(event) {
					if (event.select) {
						endPicker.set('min', startPicker.get('select'));
					} else if ('clear' in event) {
						//remove min date for end field
						endPicker.set('min', false);
					}
				});
				endPicker.on('set', function(event) {
					if (event.select) {
						startPicker.set('max', endPicker.get('select'));
					} else if ('clear' in event) {
						//remove max time for start picker
						startPicker.set('max', false);
					}
				});
			}

		} else {

			//loop through all the starting date fields
			$('input.timepicker.start').each(function(){

				//define this field
				var $startTimeField = $(this);
				var $startTime = $startTimeField.pickatime(timeDefaults);
				var startPicker = $startTime.pickatime('picker');

				//try to find the matching end field
				var $endTimeField = $('input.timepicker[data-start-field="#' + $startTime.attr('id') + '"]').filter('.end');
				if ($endTimeField.length <= 0) { $endTimeField = $('input.timepicker.end'); }

				//define the picker and such for end date
				if ($endTimeField.length > 0)
				{
					var $endTime = $endTimeField.pickatime(timeDefaults);
					var endPicker = $endTime.pickatime('picker');

					// When something is selected, update the “from” and “to” limits.
					startPicker.on('set', function(event) {
						if (event.select) {
							endPicker.set('min', startPicker.get('select'));
						} else if ('clear' in event) {
							//remove min date for end field
							endPicker.set('min', false);
						}
					});
					endPicker.on('set', function(event) {
						if (event.select) {
							startPicker.set('max', endPicker.get('select'));
						} else if ('clear' in event) {
							//remove max time for start picker
							startPicker.set('max', false);
						}
					});
				}
			});
		}
	}
};

/**
* Applies bootstrap styling to datatables
*/
Cms.Datatables = {
	initBootstrap: function() {

		/* Set the defaults for DataTables initialisation */
		$.extend( true, $.fn.dataTable.defaults, {
			"sDom":
				"<'row'<'col-xs-6'l><'col-xs-6'f>r>"+
				"t"+
				"<'row'<'col-xs-6'i><'col-xs-6'p>>",
			"oLanguage": {
				"sLengthMenu": "Display: _MENU_",
				"sInfo": "Displaying _START_ to _END_ of _TOTAL_ records",
				"sInfoEmpty": "Displaying 0 to 0 of 0 records"
			}
		});


		/* Default class modification */
		$.extend( $.fn.dataTableExt.oStdClasses, {
			"sWrapper": "dataTables_wrapper form-inline",
			"sFilterInput": "form-control input-sm",
			"sLengthSelect": "form-control input-sm"
		});

		// In 1.10 we use the pagination renderers to draw the Bootstrap paging,
		// rather than  custom plug-in
		if ( $.fn.dataTable.Api ) {
			$.fn.dataTable.defaults.renderer = 'bootstrap';
			$.fn.dataTable.ext.renderer.pageButton.bootstrap = function ( settings, host, idx, buttons, page, pages ) {
				var api = new $.fn.dataTable.Api( settings );
				var classes = settings.oClasses;
				var lang = settings.oLanguage.oPaginate;
				var btnDisplay, btnClass;

				var attach = function( container, buttons ) {
					var i, ien, node, button;
					var clickHandler = function ( e ) {
						e.preventDefault();
						if ( e.data.action !== 'ellipsis' ) {
							api.page( e.data.action ).draw( false );
						}
					};

					for ( i=0, ien=buttons.length ; i<ien ; i++ ) {
						button = buttons[i];

						if ( $.isArray( button ) ) {
							attach( container, button );
						}
						else {
							btnDisplay = '';
							btnClass = '';

							switch ( button ) {
								case 'ellipsis':
									btnDisplay = '&hellip;';
									btnClass = 'disabled';
									break;

								case 'first':
									btnDisplay = lang.sFirst;
									btnClass = button + (page > 0 ?
										'' : ' disabled');
									break;

								case 'previous':
									btnDisplay = lang.sPrevious;
									btnClass = button + (page > 0 ?
										'' : ' disabled');
									break;

								case 'next':
									btnDisplay = lang.sNext;
									btnClass = button + (page < pages-1 ?
										'' : ' disabled');
									break;

								case 'last':
									btnDisplay = lang.sLast;
									btnClass = button + (page < pages-1 ?
										'' : ' disabled');
									break;

								default:
									btnDisplay = button + 1;
									btnClass = page === button ?
										'active' : '';
									break;
							}

							if ( btnDisplay ) {
								node = $('<li>', {
										'class': classes.sPageButton+' '+btnClass,
										'aria-controls': settings.sTableId,
										'tabindex': settings.iTabIndex,
										'id': idx === 0 && typeof button === 'string' ?
											settings.sTableId +'_'+ button :
											null
									} )
									.append( $('<a>', {
											'href': '#'
										} )
										.html( btnDisplay )
									)
									.appendTo( container );

								settings.oApi._fnBindAction(
									node, {action: button}, clickHandler
								);
							}
						}
					}
				};

				attach(
					$(host).empty().html('<ul class="pagination"/>').children('ul'),
					buttons
				);
			}
		} else {
			// Integration for 1.9-
			$.fn.dataTable.defaults.sPaginationType = 'bootstrap';

			/* API method to get paging information */
			$.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
			{
				return {
					"iStart":		 oSettings._iDisplayStart,
					"iEnd":		   oSettings.fnDisplayEnd(),
					"iLength":		oSettings._iDisplayLength,
					"iTotal":		 oSettings.fnRecordsTotal(),
					"iFilteredTotal": oSettings.fnRecordsDisplay(),
					"iPage":		  oSettings._iDisplayLength === -1 ?
						0 : Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
					"iTotalPages":	oSettings._iDisplayLength === -1 ?
						0 : Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
				};
			};

			/* Bootstrap style pagination control */
			$.extend( $.fn.dataTableExt.oPagination, {
				"bootstrap": {
					"fnInit": function( oSettings, nPaging, fnDraw ) {
						var oLang = oSettings.oLanguage.oPaginate;
						var fnClickHandler = function ( e ) {
							e.preventDefault();
							if ( oSettings.oApi._fnPageChange(oSettings, e.data.action) ) {
								fnDraw( oSettings );
							}
						};

						$(nPaging).append(
							'<ul class="pagination">'+
								'<li class="prev disabled"><a href="#">&larr; '+oLang.sPrevious+'</a></li>'+
								'<li class="next disabled"><a href="#">'+oLang.sNext+' &rarr; </a></li>'+
							'</ul>'
						);
						var els = $('a', nPaging);
						$(els[0]).bind( 'click.DT', { action: "previous" }, fnClickHandler );
						$(els[1]).bind( 'click.DT', { action: "next" }, fnClickHandler );
					},

					"fnUpdate": function ( oSettings, fnDraw ) {
						var iListLength = 5;
						var oPaging = oSettings.oInstance.fnPagingInfo();
						var an = oSettings.aanFeatures.p;
						var i, ien, j, sClass, iStart, iEnd, iHalf=Math.floor(iListLength/2);

						if ( oPaging.iTotalPages < iListLength) {
							iStart = 1;
							iEnd = oPaging.iTotalPages;
						}
						else if ( oPaging.iPage <= iHalf ) {
							iStart = 1;
							iEnd = iListLength;
						} else if ( oPaging.iPage >= (oPaging.iTotalPages-iHalf) ) {
							iStart = oPaging.iTotalPages - iListLength + 1;
							iEnd = oPaging.iTotalPages;
						} else {
							iStart = oPaging.iPage - iHalf + 1;
							iEnd = iStart + iListLength - 1;
						}

						for ( i=0, ien=an.length ; i<ien ; i++ ) {
							// Remove the middle elements
							$('li:gt(0)', an[i]).filter(':not(:last)').remove();

							// Add the new list items and their event handlers
							for ( j=iStart ; j<=iEnd ; j++ ) {
								sClass = (j==oPaging.iPage+1) ? 'class="active"' : '';
								$('<li '+sClass+'><a href="#">'+j+'</a></li>')
									.insertBefore( $('li:last', an[i])[0] )
									.bind('click', function (e) {
										e.preventDefault();
										oSettings._iDisplayStart = (parseInt($('a', this).text(),10)-1) * oPaging.iLength;
										fnDraw( oSettings );
									} );
							}

							// Add / remove disabled classes from the static elements
							if ( oPaging.iPage === 0 ) {
								$('li:first', an[i]).addClass('disabled');
							} else {
								$('li:first', an[i]).removeClass('disabled');
							}

							if ( oPaging.iPage === oPaging.iTotalPages-1 || oPaging.iTotalPages === 0 ) {
								$('li:last', an[i]).addClass('disabled');
							} else {
								$('li:last', an[i]).removeClass('disabled');
							}
						}
					}
				}
			} );
		}

		/*
		 * TableTools Bootstrap compatibility
		 * Required TableTools 2.1+
		 */
		if ( $.fn.DataTable.TableTools ) {
			// Set the classes that TableTools uses to something suitable for Bootstrap
			$.extend( true, $.fn.DataTable.TableTools.classes, {
				"container": "DTTT btn-group",
				"buttons": {
					"normal": "btn btn-default",
					"disabled": "disabled"
				},
				"collection": {
					"container": "DTTT_dropdown dropdown-menu",
					"buttons": {
						"normal": "",
						"disabled": "disabled"
					}
				},
				"print": {
					"info": "DTTT_print_info modal"
				},
				"select": {
					"row": "active"
				}
			} );

			// Have the collection use a bootstrap compatible dropdown
			$.extend( true, $.fn.DataTable.TableTools.DEFAULTS.oTags, {
				"collection": {
					"container": "ul",
					"button": "li",
					"liner": "a"
				}
			} );
		}
	},

	initDatatable: function(url, selector) {

		//set selector if not set already
		selector = (typeof selector !== 'undefined' && selector != '')? selector : 'table.datatable';

		//try to find table
		var $tableElement = $(selector + ':not(.custom-table)');

		//use id if available
		if (typeof $tableElement.attr('id') !== 'undefined')
		{
			$tableElement = $('#' + $tableElement.attr('id') + ':not(.custom-table)');
		}

		//get url
		url = (typeof url !== 'undefined' && url !== '')? url : $tableElement.attr('data-url');

		//make sure value is defined
		if (url != '' && typeof url !== 'undefined' && $tableElement.length > 0)
		{
			//initate delete confirmation modals
			Cms.System.deleteModals();

			//init bootstrap styling of tables
			Cms.Datatables.initBootstrap();

			//define our breakpoints
			var responsiveHelper = undefined;
			var breakpointDefinition = {
				tablet: 1024,
				phone : 480
			};

			//build columns
			var columns = new Array();
			$tableElement.find('th').each(function(){
				var column = null;
				if ($(this).hasClass('no-search')) {
					column = {'bSearchable' : false};
				}
				columns.push(column);
			});

			//init datatables
			var table = $tableElement.dataTable({
				"bDestroy": true,
				"bStateSave": $tableElement.hasClass('save-state'),
				"bProcessing": true,
				"bServerSide": true,
				"sServerMethod": "POST",
				"sAjaxSource": url,
				"sPaginationType": 'bootstrap',
				"autoWidth": false,
				"aoColumns": columns,
				fnPreDrawCallback: function () {
					// Initialize the responsive datatables helper once.
					if (!responsiveHelper) {
						responsiveHelper = new ResponsiveDatatablesHelper($tableElement, breakpointDefinition);
					}
				},
				fnRowCallback  : function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
					responsiveHelper.createExpandIcon(nRow);
				},
				fnDrawCallback : function (oSettings) {
					responsiveHelper.respond();
				}
			});

			return table;
		}
	}
};

Cms.Roles = {
	formSelector: '#role-form',
	init: function() {
		Cms.Roles.sectionButtons();
		Cms.Roles.sectionCheckboxes();
		Cms.Roles.sectionField();
	},

	sectionField: function() {

		//define the form
		var $form = $(Cms.Roles.formSelector);

		//on change
		$form.find('select[name="sections[]"]').on('change', function(e){

			//get value(s) from post
			var sections = $(this).val();

			//loop through options
			var $options = $(this).find('option');
			$.each($options, function(index, item){

				//get value
				var value = $(this).attr('value');

				//define section
				var $sectionContainer = $form.find('#section-' + value);

				//if in selected options, show if not already shown
				if ($.inArray(value, sections) > -1)
				{
					//show section if not already shown
					if ($sectionContainer.hasClass('hide'))
					{
						$sectionContainer.removeClass('hide');

						//assume they want to allow all by default
						var $allowAll = $sectionContainer.find('input[name^="permType"][value!=""]');
						$allowAll.trigger('click');
					}
				} else {
					//hide container and set to unchecked
					$sectionContainer.addClass('hide');
					$sectionContainer.find('input[type="checkbox"]').prop('checked', false).icheck('unchecked');
				}
			});

			//init icheck
			Cms.System.initIcheck();
		});
	},

	sectionButtons: function() {

		//define the form
		var $form = $(Cms.Roles.formSelector);

		//on permission type change
		$form.find('input[name^="permType"]').on('change', function(e){

			//get the value
			var value = $(this).val();

			//grab parent container
			var $sectionContainer = $(this).parents('div.section-block');

			//grab descriptions
			var $descriptionContainer = $sectionContainer.find('div.section-description');

			//if we are granting permission to the whole section...
			if (value == "")
			{
				//hide and show appropriate descriptions & sections
				$descriptionContainer.find('div.manage').removeClass('hide');
				$descriptionContainer.find('div.allow').addClass('hide');
				$sectionContainer.find('div.manage-section-permissions').removeClass('hide');

			} else {

				//hide and show appropriate descriptions & sections
				$descriptionContainer.find('div.manage').addClass('hide');
				$descriptionContainer.find('div.allow').removeClass('hide');
				$sectionContainer.find('div.manage-section-permissions').addClass('hide');

				//uncheck anything checked
				$sectionContainer.find('input[type="checkbox"]').prop('checked', false).icheck('unchecked');
			}
		});
	},

	sectionCheckboxes: function() {

		//define the form
		var $form = $(Cms.Roles.formSelector);

		//determine if we are checking or unchecking
		$form.find('a[data-checkboxes]').on('click', function(e){

			//detmine if we are checking or unchecking
			var checking = $(this).attr('data-checkboxes');

			//grab parent container
			var $sectionContainer = $(this).parents('div.section-block');

			//uncheck or check things accordingly
			if (checking == 'uncheck')
			{
				$sectionContainer.find('input[type="checkbox"]').prop('checked', false).icheck('unchecked');
			} else if (checking == 'check') {
				$sectionContainer.find('input[type="checkbox"]').prop('checked', true).icheck('checked');
			}
		});

		//check the hidden checkbox
		$form.find('input[name^="resources"]').on('change', function(e){

			//get checkbox we want to adjust
			var $matchingCheckbox = $(this).parents('label').find('input[name^="actions"]');

			//check it if appropriate
			if ($(this).is(':checked'))
			{
				$matchingCheckbox.prop('checked', true);
			} else {
				$matchingCheckbox.prop('checked', false);
			}
		});
	}
};

Cms.Totals = {
	init: function() {
		Cms.Totals.unreadMessages();

		//set to reload every 5 minutes
		setInterval( function () {
			Cms.Totals.unreadMessages();
		}, (60000 * 5));
	},

	unreadMessages: function() {

		//make an ajax call
		$.ajax({
			url: '/admin/forms/json/get-unread-count',
			type: 'POST',
			dataType: 'json',
			success: function(response) {

				//get sidebar container
				var $sidebarContainer = $('#unread-message-notification');

				//get forms container
				var $inboxContainer = $('#mailbox-form-types').find('a[data-filter-category=""]');

				//populate field if applicable
				if (response.count > 0)
				{
					//populate sidebar
					$sidebarContainer.html(response.count);

					//populate forms count
					if ($inboxContainer)
					{
						$inboxContainer.find('span.unread-count').html('(' + response.count + ')');
					}

				} else {

					//set sidebar to empty
					$sidebarContainer.html('');

					//populate forms coun
					if ($inboxContainer)
					{
						$inboxContainer.find('span.unread-count').html('(' + response.count + ')');
					}
				}
			}
		});
	}
};

Cms.Pages = {
	formSelector: '#page-form',
	init: function() {
		Cms.Pages.urlField();
		Cms.Pages.parentField();
		Cms.Pages.saveDraftButton();
		Cms.Pages.initSort();
	},

	urlField: function() {
		//store form
		var $form = $(Cms.Pages.formSelector);

		//define the fields
		var $urlField = $form.find('input[name="url"]');
		var $pathField = $form.find('input[name="urlPath"]');
		var $parentField = $form.find('select[name="parentID"]');

		//on text change
		$urlField.on('keyup change', function(e){

			//get parent id
			var parentID = $parentField.val();

			//change text accordingly
			if (parentID != '' && parentID > 0) {
				$parentField.trigger('change');
			} else {
				$pathField.attr('value', $urlField.val());
			}
		});
	},

	tabValidation: function() {

		//init form validation per tab
		/*
		$('[data-toggle="tab"]').on('click', function(e){
			var $fields = $('input, textarea').filter(':visible');
			var valid = $fields.validation().isValid();
			if (valid == false)
			{
				$fields.validation().validate();
			}
			return valid;
		});
		*/
	},

	parentField: function() {
		//store form
		var $form = $(Cms.Pages.formSelector);

		//whenever the parent field is changed, alter the full url path as well
		$form.find('select[name="parentID"]').on('change', function(e){

			//get field value
			var parentID = $(this).val();

			//if something is selected
			if (parentID != '' && parentID > 0)
			{
				//define the fields
				var $urlField = $form.find('input[name="url"]');
				var $pathField = $form.find('input[name="urlPath"]');

				//make an ajax call to get parent page information
				$.ajax({
					url: '/admin/pages/json/generate-url-path/' + parentID,
					type: 'POST',
					dataType: 'json',
					success: function(response) {
						if (response.status == 'ok')
						{
							var newUrl = response.urlPath + '/' + $urlField.val();
							$pathField.attr('value', newUrl);
						} else {
							$pathField.attr('value', '');
							alert(response.message);
						}
					},
					error: function() {
						$pathField.attr('value', '');
					}
				});
			}
		});
	},

	saveDraftButton: function() {
		//store form
		var $form = $(Cms.Pages.formSelector);

		//the save draft button should put content in the draft column instead
		$form.find('.save-draft').click(function(e) {
			e.preventDefault();

			var action = $('form').attr('action');
			var actionUri = action.split('/');
			var pageID = actionUri[actionUri.length - 1];

			$form.attr('action', '/admin/pages/save/' + pageID).submit();
		});
	},

	initSort: function() {

		//initiate nested sortable on the menu
		$('ul.menu-sortable, ol.menu-sortable').nestedSortable({
			maxLevel: 0,
			listType: 'ul',
			handle: 'div.sortable-container:not(.protected)',
			items: 'li',
			toleranceElement: '> div',
			forcePlaceholderSize: true,
			placeholder: 'placeholder',
			update: function() {
				//var reorderedItems = JSON.stringify($(menuSelector).nestedSortable('toHierarchy'));
				var individualItems = $(this).nestedSortable('serialize'); //$item['pageID'] = parent ID, depth
				$.ajax({
					type: 'post',
					url: '/admin/pages/json/reorder',
					data: individualItems,
					success: function(feedback) {
					},
					error: function() {
						alert('An error occurred. Please try again.');
					}
				});
			}
		});
	}
};

//check to see if the page has loaded, if so...
$(function() {
	Cms.Controller.init();
});