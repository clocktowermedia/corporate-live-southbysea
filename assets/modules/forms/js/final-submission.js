var FinalSub = {
	Loader: null,
	Fields: null,
	Selectors: null,
	Upload: null,
	Validation: null
};



function myDeleteFunction() {
    document.getElementById("myTable").deleteRow(0);
}

/**
 * Admin.Loader
 *
 * Run every page through here and feed it only the JS it needs.
 */
FinalSub.Loader = {
	init: function() {

		// Fire off JS based on URI segment
		var uri = document.location.pathname.split('/');

		//hijack form submission
		FinalSub.Validation.hijackSubmit();
		FinalSub.Fields.init();
		FinalSub.Upload.init();
	}
};

FinalSub.Fields = {
	init: function() {
		FinalSub.Fields.orderIDField();
		FinalSub.Fields.dueDateField();
		FinalSub.Fields.toggleField('input[name="bagAndTag"]', 'input[name="bagAndTagSpreadsheet"]');
		FinalSub.Fields.toggleField('input[name="customNames"]', 'input[name="customNamesSpreadsheet"]');
		FinalSub.Fields.sizeBreakdown();
	},

	orderIDField: function() {
		//find button
		var $form = $(FinalSub.Selectors.mainForm);
		var $orderIDContainer = $form.find('input[name="orderID"]').parents('div.form-group');
		var $orderIDButton = $orderIDContainer.find('span.input-group-btn').find('button');

		//add ajax modal to it
		$orderIDButton.attr('data-toggle', 'ajax-modal').attr('title', 'What is my Order ID?').attr('data-link', '/faq/order-id?modal=true');
	},

	toggleField: function(watchField, toggleField) {
		var $form = $(FinalSub.Selectors.mainForm);
		var $fieldToWatch = $form.find(watchField);
		var $fieldToToggle = $form.find(toggleField);

		$fieldToWatch.on('change', function(e){
			var isChecked = ($(this).val() == 'yes')? true : false;
			if (isChecked == true)
			{
				$fieldToToggle.removeAttr('disabled').attr('required', 'required');
				$fieldToToggle.parents('div.form-group').show();
			} else {
				$fieldToToggle.val('').removeAttr('required').attr('disabled', 'disabled');
				$fieldToToggle.parents('div.form-group').hide();
			}
		});
		$fieldToWatch.trigger('change');
	},

	dueDateField: function() {
		var $form = $(FinalSub.Selectors.mainForm);
		var $field = $form.find('input[name="dueDate"]');

		//add ajax modal
		var $dueDateContainer = $field.parents('div.form-group');
		var $dueDateButton = $dueDateContainer.find('span.input-group-btn').find('button');
		$dueDateButton.attr('data-toggle', 'ajax-modal').attr('title', 'Delivery Date Options').attr('data-link', '/faq/delivery-dates?modal=true');

		var picker = $field.pickadate('picker');
		var message = '<span class="help-block has-error filled" id="date-error">Standard production time is 10 days. As such, a rush fee may be applied to this order.</span>';

		head.js('//cdn.jsdelivr.net/momentjs/2.9.0/moment.min.js', function(){
			var maxDate = new Date();
			maxDate.setTime(maxDate.getTime() + 5 * 86400000);

			//update max date
			picker.set({
				min: maxDate
			});

			picker.on('set', function(event) {

				//calculate number of days
				var today = moment();
				var endDate = moment($field.val() + ' 12:00:00');
				var days = endDate.diff(today, 'days');

				//show popup if less than 10 days
				if (event.select) {

					if (days < 10)
					{
						if ($('#date-error').length <= 0)
						{
							$field.after(message);
						}
					} else {
						$('#date-error').remove();
					}

				} else if ('clear' in event) {
					//remove message
					$('#date-error').remove();
				}
			});
		});
	},

	sizeBreakdown: function() {
		var $checkbox = $('input[name="noSizing"]');
		var $sizeContainer = $('#size-breakdown-container');
		var $inputs = $sizeContainer.find('input');
		var $extraSizes = $sizeContainer.find('div.extra-size-inputs');

		//toggle hiding/showing of size fields
		$checkbox.on('change', function(){
			if ($(this).is(':checked'))
			{
				$sizeContainer.hide();
				$inputs.val('').removeAttr('required').prop('disabled', true);
				$extraSizes.find('input:not(.hide)').addClass('hide');
				$extraSizes.find('input').prop('disabled', true);
			} else {
				$sizeContainer.show();
				$inputs.prop('disabled', false);
				$extraSizes.find('input').prop('disabled', false)
				$sizeContainer.find('input.required').attr('required', 'required');
			}
		});

		FinalSub.Fields.addStyleArea();
		FinalSub.Fields.initFields();
	},

	addStyleArea: function() {
		var $btn = $('#add-style-btn');
		var $row = $('div.size-group-container:first');
		var innerHtml = $row.wrap('<div/>').parent().html();
		$row.unwrap();

		$btn.on('click', function(e){
			e.preventDefault();
			var $append = $('div.size-group-container:last');
			$append.after(innerHtml);
			FinalSub.Fields.initFields();
		});
	},

	toggleExtraSizeMsg: function() {
		var $msg = $('#extra-sizes-msg');
		var extraSizes = (FinalSub.Fields.countExtraSizes() > 0)? false : true;
		$msg.toggleClass('hide', extraSizes);
	},

	countExtraSizes: function() {
		var $sizeContainer = $('#size-breakdown-container');
		return $sizeContainer.find('div.extra-sizes:not(.hide)').length;
	},

	initFields: function() {

		//init multi select
		$('select[name="extraSizes"]').multiselect({
			buttonWidth: '100%',
			nonSelectedText: 'Check all that are needed',
			buttonContainer: '<div class="btn-group filter-group" />',
			buttonClass: 'btn btn-default filter-btn',
			numberDisplayed: 15,
			onChange: function(option, checked, select) {
				var $parent = $(option).parents('div.size-group-container');
				var sizeID = $(option).val();
				$container = $parent.find('div.size-container[data-size-id="' + sizeID + '"]');
				$container.toggleClass('hide');
				if (checked == false)
				{
					$container.find('input').val('').prop('disabled', true);
				} else {
					$container.find('input').prop('disabled', false);
				}
			}
		});

		//toggle hiding/showing of extra sizes
		$('a.toggle-extra-sizes').on('click', function(e){
			e.preventDefault();
			var $parent = $(this).parents('div.size-group-container');
			$parent.find('div.extra-sizes').toggleClass('hide');
			$parent.find('a.toggle-extra-sizes').parent('span').toggleClass('hide');
			FinalSub.Fields.toggleExtraSizeMsg();
		});
	}
};

FinalSub.Validation = {
	hijackSubmit: function() {
		var $form = $(FinalSub.Selectors.mainForm);

		$form.submit(function(e){

			var numImages = FinalSub.Upload.getFileCount();
			var isValid = $form.validation().isValid();

			if (numImages < 1)
			{
				e.preventDefault();
				alert('Please upload at least one final design image.');
				return false;
			} else if (isValid == false) {
				$(form).validation().validate();
				return false;
			}
		});
	}
};

FinalSub.Selectors = {
	mainForm: '#final-submission-form',
	uploadedFiles: '#uploaded-images',
	uploadContainer: '#upload-container',
	uploder: null
};

FinalSub.Upload = {
	init: function() {
		FinalSub.Upload.initHelpers();
		FinalSub.Upload.addListeners();
		FinalSub.Upload.hijackForm();
	},

	settings: {
		maxFiles: 3
	},

	addListeners: function() {
		$('body').on('click touch', '#upload-btn', function(){
			$('#upload-trigger').click();
		});
	},

	hijackForm: function() {
		//define the form
		var $form = $(FinalSub.Selectors.mainForm);

		//hijack form to allow file uploading
		FinalSub.Upload.uploader = $(FinalSub.Selectors.uploadContainer).dmUploader({
			method: 'POST',
			maxFileSize: 25000000,
			maxFiles: FinalSub.Upload.settings.maxFiles,
			currentNumFiles: 0,
			extFilter: 'jpg;png;gif;ai;pdf;eps',
			url: '/forms/json/upload-image',
			dataType: 'json',
			extraData: {
				formType: $form.find('input[name="formType"]').val()
			},
			fileName: 'userfile',
			onNewFile: function(id, file){
			},
			onComplete: function(){
			},
			onUploadProgress: function(id, percent){
				var percentStr = percent + '%';
			},
			onUploadSuccess: function(id, data){

				//show error message if it didn't work
				if (data.status != 'ok')
				{
					alert('An error occurred: ' + data.message);
				} else {

					//add image queue id & file count
					data.image.queueID = id;

					//refresh and show new images
					FinalSub.Upload.showImages(data);
				}
			},
			onUploadError: function(id, message){
				//update the number of files
				var data = FinalSub.Upload.uploader.data('dmUploader');
				data.fileNum = FinalSub.Upload.getFileCount();
				data.queue = data.queue.slice(0,id).concat(data.queue.slice(id + 1));
				data.queuePos = data.queuePos - 1;
				alert('An error occurred: ' + message);
			},
			onFileExtError: function(file){
				var fileTypeMsg = FinalSub.Upload.uploader.data('dmUploader').settings.extFilter.replace(/\;/g, ', ').toUpperCase();
				alert('File \'' + file.name + '\' is not an accepted file type. ' + fileTypeMsg + ' files allowed.');
			},
			onFileSizeError: function(file){
				alert('File \'' + file.name + '\' exceeds the file size limit.');
			},
			onFallbackMode: function(message){
				alert(message + '.');
			}
		});

		//hijack image deletions, needs to be here otherwise the uploader is not defined
		$('body').on('click', 'a.delete-img', function(){
			//get ids
			var imageID = $(this).attr('data-id');
			var queueID = $(this).attr('data-queue');

			//remove image from markup
			$('#attached-image-' + imageID).remove();

			//get number of images
			var qty = FinalSub.Upload.getFileCount();

			//update the number of files
			var data = FinalSub.Upload.uploader.data('dmUploader');
			data.fileNum = qty;
			data.queue = data.queue.slice(0,queueID).concat(data.queue.slice(queueID + 1));
			data.queuePos = data.queuePos - 1;

			//hide or show the form accordingly
			FinalSub.Upload.hideShowUpload();
		});
	},

	hideShowUpload: function() {
		//define the container we want to hide/show
		var $toggle = $(FinalSub.Selectors.uploadContainer);

		//hide by default
		$toggle.hide();

		//get quantity and hide/show accordingly
		var qty = FinalSub.Upload.getFileCount();
		if (qty < FinalSub.Upload.settings.maxFiles)
		{
			$toggle.show();
		}
	},

	showImages: function(data) {
		//define the form
		var $form = $(FinalSub.Selectors.mainForm);

		//define our templates/placeholders
		var $template = $("#form-images-listing");
		var $populate = $(FinalSub.Selectors.uploadedFiles);

		// compile and apply the main template
		var template = Handlebars.compile($template.html());
		var html = template(data);
		$populate.append(html);

		//wrap with new wrapper
		if (Skeleton.System.isBreakpoint('xs') === true)
		{
			$populate.find("div.attached-image").wrapMatch(2,'row upload-wrapper');
		} else {
			$populate.find("div.attached-image").wrapMatch(3,'row upload-wrapper');
		}

		//hide/show upload form
		FinalSub.Upload.hideShowUpload();
	},

	getFileCount: function() {
		return $(FinalSub.Selectors.uploadedFiles + ' div.thumbnail-img').length;
	},

	initHelpers: function() {
		Handlebars.registerHelper('compare', function (lvalue, operator, rvalue, options) {
			var operators, result;
			if (arguments.length < 3) {
				throw new Error("Handlerbars Helper 'compare' needs 2 parameters");
			}
			if (options === undefined) {
				options = rvalue;
				rvalue = operator;
				operator = "===";
			}
			operators = {
				'==': function (l, r) { return l == r; },
				'===': function (l, r) { return l === r; },
				'!=': function (l, r) { return l != r; },
				'!==': function (l, r) { return l !== r; },
				'<': function (l, r) { return l < r; },
				'>': function (l, r) { return l > r; },
				'<=': function (l, r) { return l <= r; },
				'>=': function (l, r) { return l >= r; },
				'typeof': function (l, r) { return typeof l == r; },
				'~=': function(l, r) { return l.indexOf(r) != -1; },
				'!~=': function(l, r) { return l.indexOf(r) == -1; }
			};
			if (!operators[operator]) {
				throw new Error("Handlerbars Helper 'compare' doesn't know the operator " + operator);
			}
			result = operators[operator](lvalue, rvalue);

			if (result) {
				return options.fn(this);
			} else {
				return options.inverse(this);
			}
		});
	}
};
FinalSub.Loader.init();
