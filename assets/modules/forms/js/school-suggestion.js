var ProofRequest = {
	Loader: null,
	Selectors: null,
	Fields: null,
	Upload: null
};

/**
 * Admin.Loader
 *
 * Run every page through here and feed it only the JS it needs.
 */
ProofRequest.Loader = {
	init: function() {

		// Fire off JS based on URI segment
		var uri = document.location.pathname.split('/');

		//hijack form submission
		SouthBySea.Search.initHelpers();
		ProofRequest.Upload.init();
		ProofRequest.Fields.init();
	}
};

ProofRequest.Selectors = {
	mainForm: '#school-suggestion-form',
	uploadedFiles: '#uploaded-images',
	uploadContainer: '#upload-container',
	uploder: null
};

ProofRequest.Fields = {
	init: function() {
		ProofRequest.Fields.dueDateField();
	},

	dueDateField: function() {
		//find button
		var $form = $(ProofRequest.Selectors.mainForm);
		var $dueDateContainer = $form.find('input[name="dueDate"]').parents('div.form-group');
		var $dueDateButton = $dueDateContainer.find('span.input-group-btn').find('button');

		//add ajax modal to it
		$dueDateButton.attr('data-toggle', 'ajax-modal').attr('title', 'Delivery Date Options').attr('data-link', '/faq/delivery-dates?modal=true');
	},
};

ProofRequest.Upload = {
	init: function() {
		ProofRequest.Upload.initHelpers();
		ProofRequest.Upload.addListeners();
		ProofRequest.Upload.hijackForm();
	},

	settings: {
		maxFiles: 15
	},

	addListeners: function() {
		$('body').on('click touch', '#upload-btn', function(){
			$('#upload-trigger').click();
		});
	},

	hijackForm: function() {
		//define the form
		var $form = $(ProofRequest.Selectors.mainForm);

		//hijack form to allow file uploading
		ProofRequest.Upload.uploader = $(ProofRequest.Selectors.uploadContainer).dmUploader({
			method: 'POST',
			maxFileSize: 25000000,
			maxFiles: ProofRequest.Upload.settings.maxFiles,
			currentNumFiles: 0,
			extFilter: 'jpg;png;gif;ai;pdf;eps',
			url: '/forms/json/upload-image',
			dataType: 'json',
			extraData: {
				formType: $form.find('input[name="formType"]').val()
			},
			fileName: 'userfile',
			onNewFile: function(id, file){
			},
			onComplete: function(){
			},
			onUploadProgress: function(id, percent){
				var percentStr = percent + '%';
			},
			onUploadSuccess: function(id, data){

				//show error message if it didn't work
				if (data.status != 'ok')
				{
					alert('An error occurred: ' + data.message);
				} else {

					//add image queue id & file count
					data.image.queueID = id;

					//refresh and show new images
					ProofRequest.Upload.showImages(data);
				}
			},
			onUploadError: function(id, message){
				//update the number of files
				var data = ProofRequest.Upload.uploader.data('dmUploader');
				data.fileNum = ProofRequest.Upload.getFileCount();
				data.queue = data.queue.slice(0,id).concat(data.queue.slice(id + 1));
				data.queuePos = data.queuePos - 1;
				alert('An error occurred: ' + message);
			},
			onFileExtError: function(file){
				var fileTypeMsg = ProofRequest.Upload.uploader.data('dmUploader').settings.extFilter.replace(/\;/g, ', ').toUpperCase();
				alert('File \'' + file.name + '\' is not an accepted file type. ' + fileTypeMsg + ' files allowed.');
			},
			onFileSizeError: function(file){
				alert('File \'' + file.name + '\' exceeds the file size limit.');
			},
			onFallbackMode: function(message){
				alert(message + '.');
			}
		});

		//hijack image deletions, needs to be here otherwise the uploader is not defined
		$('body').on('click', 'a.delete-img', function(){
			//get ids
			var imageID = $(this).attr('data-id');
			var queueID = $(this).attr('data-queue');

			//remove image from markup
			$('#attached-image-' + imageID).remove();

			//get number of images
			var qty = ProofRequest.Upload.getFileCount();

			//update the number of files
			var data = ProofRequest.Upload.uploader.data('dmUploader');
			data.fileNum = qty;
			data.queue = data.queue.slice(0,queueID).concat(data.queue.slice(queueID + 1));
			data.queuePos = data.queuePos - 1;

			//hide or show the form accordingly
			ProofRequest.Upload.hideShowUpload();
		});
	},

	hideShowUpload: function() {
		//define the container we want to hide/show
		var $toggle = $(ProofRequest.Selectors.uploadContainer);

		//hide by default
		$toggle.hide();

		//get quantity and hide/show accordingly
		var qty = ProofRequest.Upload.getFileCount();
		if (qty < ProofRequest.Upload.settings.maxFiles)
		{
			$toggle.show();
		}
	},

	showImages: function(data) {
		//define the form
		var $form = $(ProofRequest.Selectors.mainForm);

		//define our templates/placeholders
		var $template = $("#form-images-listing");
		var $populate = $(ProofRequest.Selectors.uploadedFiles);

		// compile and apply the main template
		var template = Handlebars.compile($template.html());
		var html = template(data);
		$populate.append(html);

		//delete all old wrappers
		$populate.find('div.upload-wrapper').each(function(){
			$(this).replaceWith($(this).html());
		});

		//wrap with new wrapper
		if (Skeleton.System.isBreakpoint('xs') === true)
		{
			$populate.find("div.attached-image").wrapMatch(2,'row upload-wrapper');
		} else {
			$populate.find("div.attached-image").wrapMatch(3,'row upload-wrapper');
		}

		//hide/show upload form
		ProofRequest.Upload.hideShowUpload();
	},

	getFileCount: function() {
		return $(ProofRequest.Selectors.uploadedFiles + ' div.thumbnail-img').length;
	},

	initHelpers: function() {
		Handlebars.registerHelper('compare', function (lvalue, operator, rvalue, options) {
			var operators, result;
			if (arguments.length < 3) {
				throw new Error("Handlerbars Helper 'compare' needs 2 parameters");
			}
			if (options === undefined) {
				options = rvalue;
				rvalue = operator;
				operator = "===";
			}
			operators = {
				'==': function (l, r) { return l == r; },
				'===': function (l, r) { return l === r; },
				'!=': function (l, r) { return l != r; },
				'!==': function (l, r) { return l !== r; },
				'<': function (l, r) { return l < r; },
				'>': function (l, r) { return l > r; },
				'<=': function (l, r) { return l <= r; },
				'>=': function (l, r) { return l >= r; },
				'typeof': function (l, r) { return typeof l == r; },
				'~=': function(l, r) { return l.indexOf(r) != -1; },
				'!~=': function(l, r) { return l.indexOf(r) == -1; }
			};
			if (!operators[operator]) {
				throw new Error("Handlerbars Helper 'compare' doesn't know the operator " + operator);
			}
			result = operators[operator](lvalue, rvalue);

			if (result) {
				return options.fn(this);
			} else {
				return options.inverse(this);
			}
		});
	}
};
ProofRequest.Loader.init();
