var Designs = {
	Loader: null,
	AddEdit: null,
	Reorder: null
};

/**
 * Designs.Loader
 *
 * Run every page through here and feed it only the JS it needs.
 */
Designs.Loader = {
	init: function() {

		// Fire off JS based on URI segment
		var uri = document.location.pathname.split('/');

		//init stuff on design categories
		if (uri[3] == 'categories')
		{
			if (uri[4] == 'reorder-designs')
			{
				Designs.Reorder.initDesigns(uri);
			}

			if (uri[4] == 'reorder') 
			{
				Designs.Reorder.categories();
			}
		}
	}
};

Designs.Reorder = {
	initDesigns: function(uri) {
		Designs.Reorder.featuredDesigns(uri);
		Designs.Reorder.designs(uri);
	},

	featuredDesigns: function(uri) {
		$('#featured-design-images').find('div.row').sortable({
			opacity: '0.5',
			items: '> div.thumbnail-img',
			update: function(e, ui) {
				serial = $(this).sortable('serialize');
				$.ajax({
					url: '/admin/designs/json/reorder-designs/' + uri[5] + '/featured',
					type: 'POST',
					data: serial,
					dataType: 'json',
					success: function(response) {
						if (response.status != 'ok')
						{
							alert(response.message);
						}
					},
					error: function() {
						alert('An error occurred. Please try again.');
						return false;
					}
				});
			}
		});
	},

	designs: function(uri) {
		$('#design-images').find('div.row').sortable({
			opacity: '0.5',
			items: '> div.thumbnail-img',
			update: function(e, ui) {
				serial = $(this).sortable('serialize');
				$.ajax({
					url: '/admin/designs/json/reorder-designs/' + uri[5],
					type: 'POST',
					data: serial,
					dataType: 'json',
					success: function(response) {
						if (response.status != 'ok')
						{
							alert(response.message);
						}
					},
					error: function() {
						alert('An error occurred. Please try again.');
						return false;
					}
				});
			}
		});
	},

	categories: function() {
		//initiate nested sortable on the menu
		$('ol.sortable').nestedSortable({
			maxLevel: 1,
			listType: 'ol',
			//handle: 'li',
			items: 'li',
			toleranceElement: '> div',
			forcePlaceholderSize: true,
			placeholder: 'placeholder',
			protectRoot: true,
			start: function(e, ui) {
				// creates a temporary attribute on the element with the old parent id
				var parentID = ui.item.parent('ol').parent('li').attr('id');
				ui.item.attr('data-prevparent', parentID);
			},
			stop: function(e, ui) {
				//do not allow items to have their parent changed
				var newParentID = ui.item.parent('ol').parent('li').attr('id');
				if (newParentID != ui.item.attr('data-prevparent'))
				{
					$(this).nestedSortable('cancel');
				}
			},
			update: function() {
				//var reorderedItems = JSON.stringify($(menuSelector).nestedSortable('toHierarchy'));
				var individualItems = $(this).nestedSortable('serialize'); //$item['pageID'] = parent ID, depth
				$.ajax({
					type: 'post',
					url: '/admin/designs/categories/json/reorder',
					data: individualItems,
					success: function(feedback) {
					},
					error: function() {
						alert('An error occurred. Please try again.');
					}
				});
			}
		});
	}
};

Designs.Loader.init();