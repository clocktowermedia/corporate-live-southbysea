var Cart = {
	Loader: null,

	Functions: null,
	Forms: null,
	Fields: null,
	Elements: null,

	Page: null,
	Data: null,
	Checkout: null
};

/**
 * Cart.Loader
 *
 * Run every page through here and feed it only the JS it needs.
 */
Cart.Loader = {
	init: function() {

		// Fire off JS based on URI segment
		var uri = document.location.pathname.split('/');
		Cart.Data.store = uri[2];

		//init stuffs
		Cart.Fields.initListeners();
		Cart.Fields.initHiders();
		Cart.Forms.initHijack();
		Cart.Forms.subscribeForm();
		Cart.Elements.summaryDropdown();

		//init cart page
		if (uri[3] == 'cart')
		{
			Cart.Page.init();
		}

		//init checkout page
		if (uri[3] == 'checkout')
		{
			Cart.Checkout.init();
		}

		if (uri[3] == 'thank-you')
		{
			$('#download-pdf')[0].click();
		}
	}
};

Cart.Fields = {
	initListeners: function() {
		//watch the associated fields and update linked values where appropriate
		$('.attached-to').each(function(){
			var $toChange = $(this);
			var $fieldToWatch = $($toChange.attr('data-watch'));

			if ($fieldToWatch.length > 0)
			{
				$fieldToWatch.on('change', function(){
					//determine new value
					var newValue;
					if (typeof $toChange.attr('data-watch-attr') !== 'undefined')
					{
						newValue = $(this).find('option:selected').attr($toChange.attr('data-watch-attr'));
					} else {
						newValue = $(this).val();
					}

					//update value
					if ($toChange.attr('data-type') == 'val')
					{
						$toChange.val(newValue);
					} else {
						$toChange.html(newValue);
					}
				});
			}
		});
	},

	initHiders: function() {
		$('.hider-shower').each(function(){
			var $toWatch = $(this);

			$toWatch.on('change', function(){
				//hide all attached fields
				var $others = $('[data-parent="' + $(this).attr('id') + '"]');
				$others.removeClass('hide').addClass('hide');
				$others.find('input').prop('disabled', true);

				//get field to toggle
				var $toToggle = $('#' + $(this).val());
				if ($toToggle.length > 0)
				{
					$toToggle.toggleClass('hide');

					//if shown, re-add required attribute
					var hidden = $(this).hasClass('hide');
					if (hidden == false)
					{
						$toToggle.find('input').prop('disabled', false);
					}
				}
			});
		});
	}
};

Cart.Elements = {
	summaryDropdown: function() {

		//populate cart dropdown
		$.ajax({
			type: 'GET',
			url: '/preorder/' + Cart.Data.store + '/cart/json/get-cart-info',
			dataType: 'json',
			success: function(response) {
				if (response.status == 'ok')
				{
					//get all containers & update markup
					var $populate = $('#cart-summary-container');
					var $template = $('#' + $populate.attr('id').replace('container', '') + 'markup');
					var template = Handlebars.compile($template.html());
					$populate.html(template(response.order));

				} else {
					alert(response.message);
				}
			},
			error: function(response) {
				//alert(JSON.parse(response.responseText));
			}
		});
	}
};

Cart.Forms = {
	initHijack: function() {
		var $forms = $('form.hijack-submit');
		$forms.on('submit', function(e){
			e.preventDefault();
			var $form = $(this);

			$.ajax({
				type: 'POST',
				url: $form.attr('action'),
				dataType: 'json',
				data: $form.serialize(),
				success: function(response) {
					if (response.status == 'ok')
					{
						//redirect if applicable
						if (response.hasOwnProperty('redirect'))
						{
							window.location = response.redirect;
						} else {
							//show success message
							alert(response.message);
						}
					} else {
						//redirect if applicable
						if (response.hasOwnProperty('redirect'))
						{
							window.location = response.redirect;
						} else {
							alert(response.message);
						}
					}
				},
				error: function(response) {
					alert(JSON.parse(response.responseText));
				}
			});
		});
	},

	subscribeForm: function() {
		var $modal = $('#modal-layout');

		//on form submit
		$('body').on('submit', '#subscribe-form', function(e){
			e.preventDefault();

			$.ajax({
				type: 'POST',
				url: $(this).attr('action'),
				dataType: 'json',
				data: $(this).serialize(),
				success: function(response) {
					if (response.status == 'ok')
					{
						//remove content & show message
						$modal.find('span, form').remove();
						SouthBySea.Loader.showAlert('success', response.message, '#subscribe-form-messages');

					} else {
						SouthBySea.Loader.showAlert('danger', response.message, '#subscribe-form-messages');
					}
				},
				error: function(response) {
					SouthBySea.Loader.showAlert('danger', response.responseText, '#subscribe-form-messages');
				},
				complete: function() {
					window.setTimeout(function () {
						$('div.alert').fadeTo(400, 0).slideUp(400, function () {
							$(this).remove();
						});
					}, 2500);
				}
			});
		});
	}
};

Cart.Page = {
	init: function() {
		Cart.Page.hijackUpdateButton();
		Cart.Page.initCart();
	},

	initCart: function() {
		Cart.Page.initRemove();
		Cart.Page.initQty();
	},

	hijackUpdateButton: function() {
		$('#update-cart-btn').on('click touch', function(e){
			e.preventDefault();
			$('input[name="qty"]').trigger('change');
		});
	},

	initRemove: function() {
		$('a.remove-item').on('click touch', function(e){
			$.ajax({
				type: 'POST',
				url: '/preorder/' + Cart.Data.store + '/cart/json/remove-item',
				dataType: 'json',
				data: {
					'itemID': $(this).attr('data-id')
				},
				success: function(response) {
					if (response.status == 'ok')
					{
						if (response.hasOwnProperty('redirect'))
						{
							location.reload();
						} else {
							//reload cart markup
							Cart.Page.reloadCart();
						}

					} else {
						alert(response.message);
					}
				},
				error: function(response) {
					alert(JSON.parse(response.responseText));
				}
			});
		});
	},

	initQty: function() {
		var $qty = $('input[name="qty"]');
		$qty.on('change keyup', function(){
			var validQty = $qty.validation().isValid();
			if (validQty)
			{
				$.ajax({
					type: 'POST',
					url: '/preorder/' + Cart.Data.store + '/cart/json/update-quantity',
					dataType: 'json',
					data: {
						'itemID': $(this).attr('data-id'),
						'qty': $(this).val()
					},
					success: function(response) {
						if (response.status == 'ok')
						{
							Cart.Page.reloadPricing();
							Cart.Elements.summaryDropdown();
						} else {
							alert(response.message);
						}
					},
					error: function(response) {
						alert(JSON.parse(response.responseText));
					}
				});
			} else {
				$qty.validation().validate();
			}
		});
	},

	reloadPricing: function() {
		$.ajax({
			type: 'GET',
			url: '/preorder/' + Cart.Data.store + '/cart/json/get-cart-info',
			dataType: 'json',
			success: function(response) {
				if (response.status == 'ok')
				{
					//get all containers & update markup
					var $populate = $('#pricing-container');
					var $template = $('#' + $populate.attr('id').replace('container', '') + 'markup');
					var template = Handlebars.compile($template.html());
					$populate.html(template(response.order));

				} else {
					alert(response.message);
				}
			},
			error: function(response) {
				alert(JSON.parse(response.responseText));
			}
		});
	},

	reloadCart: function() {
		$.ajax({
			type: 'GET',
			url: '/preorder/' + Cart.Data.store + '/cart/json/get-cart-info',
			dataType: 'json',
			success: function(response) {
				if (response.status == 'ok')
				{
					//get all containers & update markup
					$('div.hb-container').each(function(){
						var $populate = $(this);
						var $template = $('#' + $populate.attr('id').replace('container', '') + 'markup');
						var template = Handlebars.compile($template.html());
						$populate.html(template(response.order));
						Cart.Page.initCart();
						Cart.Elements.summaryDropdown();
					});

				} else {
					alert(response.message);
				}
			},
			error: function(response) {
				alert(JSON.parse(response.responseText));
			}
		});
	}
};

Cart.Data = {
	selector: '#cart',
	store: null,
	getStore: function() {
		return $(Cart.Data.selector + '-container').attr('data-store');
	}
};

Cart.Checkout = {
	formSelector: '#checkout-form',

	init: function() {
		var $form = $(Cart.Checkout.formSelector);
		var $payment_method = $form.find('select[name="payment_method"]');

		//different payment methods
		if ($payment_method.length > 0)
		{
			$payment_method.on('change', function(){
				if ($(this).val() == 'credit')
				{
					Cart.Checkout.stripe();
				} else {
					var $form = $(Cart.Checkout.formSelector);
					$form.off('submit');
				}
			});
		} else {
			Cart.Checkout.stripe(); //assume credit card payment only
		}
	},

	stripe: function() {

		//load js
		head.js('https://js.stripe.com/v2/', function(){

			$.getJSON('/preorder/' + Cart.Data.store + '/cart/json/stripe-key', function(response){

				//set stripe key
				Stripe.setPublishableKey(response.stripe);

				//define checkout form
				var $form = $(Cart.Checkout.formSelector);
				var $errors = $('#payment-errors');
				$form.on('submit', function(e) {
					e.preventDefault();

					// Disable the submit button to prevent repeated clicks:
					var $submitBtn = $form.find('button[type="submit"]');
					$submitBtn.prop('disabled', true);

					//check to see if we've passed required checks first
					var $cardNum = $form.find('[data-stripe="number"]');
					var $expMonth = $form.find('[data-stripe="exp_month"]');
					var $expYear = $form.find('[data-stripe="exp_year"]');
					var $cvc = $form.find('[data-stripe="cvc"]');
					var validCard = Stripe.card.validateCardNumber($cardNum.val());
					var validExpiry = Stripe.card.validateExpiry($expMonth.val(), $expYear.val());
					var validCVC = Stripe.card.validateCVC($cvc.val());

					if (validCard === false)
					{
						$cardNum.parent('div').parent('div.form-group').addClass('has-error');
					}
					if (validExpiry === false)
					{
						$expMonth.parent('div').parent('div.form-group').addClass('has-error');
						$expYear.parent('div').parent('div.form-group').addClass('has-error');
					}
					if (validCVC === false)
					{
						$cvc.parent('div').parent('div.form-group').addClass('has-error');
					}
					if (validCVC === false || validCard === false || validExpiry === false)
					{
						$errors.html('<div class="alert alert-danger" role="alert">Your payment information is incorrect. Please review.</div>');
						$submitBtn.prop('disabled', false);

					} else {

						// Request a token from Stripe:
						Stripe.card.createToken($form, function(status, stripeResponse){
							if (stripeResponse.error) {

								// Show the errors on the form:
								$errors.html('<div class="alert alert-danger" role="alert">' + stripeResponse.error.message + '</div>');
								$form.find('.has-error').removeClass('has-error');

								//depending on error..
								var $errorField = $form.find('[data-stripe="' + stripeResponse.error.param + '"]');
								if ($errorField.length > 0)
								{
									$errorField.parent('div').parent('div.form-group').addClass('has-error');
								}

								$submitBtn.prop('disabled', false); // Re-enable submission

							} else { // Token was created!

								// Insert the token ID into the form so it gets submitted to the server:
								$form.append($('<input type="hidden" name="stripeToken">').val(stripeResponse.id));

								// Submit the form:
								$form.get(0).submit();
							}
						});
					}

					// Prevent the form from being submitted
					return false;
				});

			});
		});
	}
};

//check to see if the page has loaded, if so...
$(function() {
	Cart.Loader.init();
});