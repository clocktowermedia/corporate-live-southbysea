var Ecommerce = {
	Controller: null,
	Pages: null,
	Sizes: null,
	Elements: null,
	Datatables: null
};

/**
 * Ecommerce.Controller
 *
 * Run every page through here and feed it only the JS it needs.
 */
Ecommerce.Controller = {
	init: function() {

		// Fire off JS based on URI segment
		var uri = document.location.pathname.split('/');

		//initate delete confirmation modals
		Cms.System.deleteModals();

		//init calls for specific pages
		if (uri[3] == '' || uri[3] == null)
		{
			Ecommerce.Pages.listingPage();
		}
		if (uri[3] == 'create' || uri[3] == 'edit')
		{
			Ecommerce.Pages.createEditPage();
		}
		if (uri[3] == 'closing')
		{
			Ecommerce.Pages.closingPage();
		}
		if (uri[3] == 'products' && uri[4] == 'reorder')
		{
			Ecommerce.Pages.reorderProductsPage();
		}
		if (uri[3] == 'products' && uri[4] == 'sizes')
		{
			Ecommerce.Sizes.init();
		}
	}
};

/**
* Applies bootstrap styling to datatables
*/
Ecommerce.Datatables = {

	initDatatable: function(url, selector) {

		//set selector if not set already
		selector = (typeof selector !== 'undefined' && selector != '')? selector : 'table.datatable';

		//try to find table
		var $tableElement = $(selector);

		//use id if available
		if (typeof $tableElement.attr('id') !== 'undefined')
		{
			$tableElement = $('#' + $tableElement.attr('id'));
		}

		//get url
		url = (typeof url !== 'undefined' && url !== '')? url : $tableElement.attr('data-url');

		//make sure value is defined
		if (url != '' && typeof url !== 'undefined')
		{
			//init bootstrap styling of tables
			Cms.Datatables.initBootstrap();

			//define our breakpoints
			var responsiveHelper = undefined;
			var breakpointDefinition = {
				tablet: 1024,
				phone : 480
			};

			//get defaults, add toolbar
			var defaultMarkup = $.fn.dataTable.defaults.sDom;
			var newMarkup = defaultMarkup.replace('l>', "l<'toolbar'>>");

			//build columns
			var columns = new Array();
			$tableElement.find('th').each(function(){
				var column = null;
				if ($(this).hasClass('no-search')) {
					column = {'bSearchable' : false};
				}
				columns.push(column);
			});

			//init datatables
			var table = $tableElement.dataTable({
				"sDom": newMarkup,
				"bDestroy": true,
				"bProcessing": true,
				"bServerSide": true,
				"sServerMethod": "POST",
				"sAjaxSource": url,
				"sPaginationType": 'bootstrap',
				"autoWidth": false,
				"aoColumns": columns,
				fnPreDrawCallback: function () {
					// Initialize the responsive datatables helper once.
					if (!responsiveHelper) {
						responsiveHelper = new ResponsiveDatatablesHelper($tableElement, breakpointDefinition);
					}
				},
				fnRowCallback  : function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
					responsiveHelper.createExpandIcon(nRow);
				},
				fnDrawCallback : function (oSettings) {
					responsiveHelper.respond();
				}
			});

			//append store manager dropdown
			var $toolbarContainer = $tableElement.prev('div.row').find('div.toolbar');
			var acctMgrSelect = '<div class="acct_mgr_select">' +
				'<label>Account Manager: ' +
					Ecommerce.Elements.managerDropdownFromPage() +
				'</label>' +
			'</div>';
			$toolbarContainer.html(acctMgrSelect);

			//grab newly appended dropdown value
			var $dropdown = $toolbarContainer.find('div.acct_mgr_select').find('select');
			if (typeof Cookies.get('selectedAccMgr') !== 'undefined')
			{
				$dropdown.val(Cookies.get('selectedAccMgr'));
			}
			$dropdown.on('change', function(){
				//determine column number
				var className = $dropdown.attr('name').replace('_', '-');
				var $column = $tableElement.find('th.' + className);
				var columnNum = $column.prevAll('th').length;

				//filter table
				if ($(this).val() != '')
				{
					table.fnFilter($(this).val(), columnNum);
					Cookies.set('selectedAccMgr', $(this).val());
				} else {
					table.fnFilter('', columnNum);
					table.fnFilter('');
					Cookies.remove('selectedAccMgr');
				}
			});
			$dropdown.trigger('change'); //trigger on load

			//return the table
			return table;
		}
	}
};

Ecommerce.Elements = {
	managerDropdownFromPage: function() {
		return $('#acct_mgr_dropdown').html();
	},

	passwordToggle: function() {
		$('a.toggle-password').on('click', function(e){
			e.preventDefault();
			var $field = $(this).parents('div.form-group').find('input');
			if ($field.attr('type') == 'password')
			{
				$field.attr('type', 'text');
				$(this).text('Hide Password');
			} else {
				$field.attr('type', 'password');
				$(this).text('Show Password');
			}
		});
	}
};

Ecommerce.Pages = {
	reorderProductsPage: function() {
		var $container = $('#product-images');
		$container.find('div.row').sortable({
			opacity: '0.5',
			items: '> div.thumbnail-img',
			update: function(e, ui) {
				serial = $(this).sortable('serialize');
				$.ajax({
					url: '/admin/ecommerce/json/reorder-products/' + $container.attr('data-id'),
					type: 'POST',
					data: serial,
					dataType: 'json',
					success: function(response) {
						if (response.status != 'ok')
						{
							alert(response.message);
						} else {
							Cms.System.showAlert('success', response.message);
						}
					},
					error: function() {
						alert('An error occurred. Please try again.');
						return false;
					}
				});
			}
		});
	},

	closingPage: function() {
		//append account manager id to form submit
		var $form = $('#store-closing-form');
		var $select = $form.find('select');
		$select.on('change', function(){
			if ($(this).val() != '')
			{
				var value = $select.find('option[value="' + $(this).val() + '"]').text();
				Cookies.set('selectedAccMgr', value);
			} else {
				Cookies.remove('selectedAccMgr');
			}
			window.location = $form.attr('action') + '/' + $(this).val();
		});

		if (typeof Cookies.get('selectedAccMgr') !== 'undefined')
		{
			var $option = $select.find('option:contains("' + Cookies.get('selectedAccMgr') + '")');
			var $selected = $select.find('option:selected');
			if ($option.text() != $selected.text())
			{
				$option.prop('selected', true);
				$select.trigger('change');
			}
		}
	},

	listingPage: function() {

		//custom datatables
		if ($('table.datatable').length > 0)
		{
			$('table.datatable').each(function(){
				var selector = (typeof $(this).attr('id') !== 'undefined')? '#' + $(this).attr('id') : '';
				var table = Ecommerce.Datatables.initDatatable('', selector);
			});
		}
	},

	createEditPage: function() {
		Ecommerce.Elements.passwordToggle();

		var $password = $('input[name="storeP"]');
		var $checkbox = $('input[name="passwordEnabled"]');
		$checkbox.on('change', function(e){
			if ($(this).is(':checked'))
			{
				$password.prop('disabled', false);
				$password.parents('div.form-group').show();
			} else {
				$password.prop('disabled', true);
				$password.parents('div.form-group').hide();
			}
		});
		$checkbox.trigger('change');
	}
};

Ecommerce.Sizes = {
	selectors: {
		'form': '#add-sizes-form',
		'form_template': '#add-size-template',
		'form_add_btn': 'a.add-product-size',
		'form_add_row': 'div.product-size-row',
		'sort_container': '#product-sizes-container'
	},

	init: function() {
		Ecommerce.Sizes.refreshSortingMarkup();
		Ecommerce.Sizes.hijackSubmit();

		//init add buttons
		Ecommerce.Sizes.addAddRow();
		$('body').on('click', Ecommerce.Sizes.selectors.form_add_btn, function(e){
			Ecommerce.Sizes.addAddRow();
		});
	},

	sorting: function() {
		var $container = $(Ecommerce.Sizes.selectors.sort_container);
		$container.find('div.row').sortable({
			opacity: '0.5',
			items: '> div.thumbnail-img',
			update: function(e, ui) {
				serial = $(this).sortable('serialize');
				$.ajax({
					url: '/admin/ecommerce/json/reorder-sizes/' + $container.attr('data-id'),
					type: 'POST',
					data: serial,
					dataType: 'json',
					success: function(response) {
						if (response.status != 'ok')
						{
							alert(response.message);
						} else {
							Cms.System.showAlert('success', response.message);
						}
					},
					error: function() {
						alert('An error occurred. Please try again.');
						return false;
					}
				});
			}
		});
	},

	refreshSortingMarkup: function() {
		//define our containers and such
		var $populate = $(Ecommerce.Sizes.selectors.sort_container);
		var $template = $(Ecommerce.Sizes.selectors.sort_container.replace('container', 'listing'));

		//make an ajax call to submit the form
		$.ajax({
			url: '/admin/ecommerce/json/sizes/' + $populate.attr('data-id'),
			type: 'GET',
			dataType: 'json',
			success: function(response) {
				if (response.status == 'ok')
				{
					//refresh markup & init sorting
					var template = Handlebars.compile($template.html());
					$populate.html(template(response));
					Ecommerce.Sizes.sorting();
				}
			},
			error: function(response) {
				alert(response.responseText);
			}
		});
	},

	addAddRow: function() {

		//add additional size form to the page, start by defining our templates/placeholders
		var $template = $(Ecommerce.Sizes.selectors.form_template);
		var $populate = $(Ecommerce.Sizes.selectors.form_template.replace('template', 'container'));
		var $form = $(Ecommerce.Sizes.selectors.form);

		//get number of sizes already on page
		response = {
			number: Ecommerce.Sizes.countAddRows() + 1,
			price: $form.attr('data-default-price')
		};

		// compile and apply the main template
		var template = Handlebars.compile($template.html());
		$populate.append(template(response));

		//remove all but the last add button
		$(Ecommerce.Sizes.selectors.form_add_btn + ':not(:last)').remove();
		$form.find('input[name="qty"]').val(Ecommerce.Sizes.countAddRows());
	},

	countAddRows: function() {
		return $(Ecommerce.Sizes.selectors.form_add_row).length;
	},

	hijackSubmit: function() {
		var $form = $(Ecommerce.Sizes.selectors.form);

		$form.on('submit', function(e){
			e.preventDefault();

			//make an ajax call to submit the form
			$.ajax({
				url: '/admin/ecommerce/json/add-sizes',
				type: 'POST',
				data: $form.serialize(),
				dataType: 'json',
				success: function(response) {
					if (response.status == 'ok')
					{
						//show message
						Cms.System.showAlert('success', response.message);
					} else {
						alert(response.message);
					}
				},
				error: function(response) {
					alert(response.responseText);
				},
				complete: function() {
					//refresh markup
					Ecommerce.Sizes.refreshSortingMarkup();

					//clear out existing values
					$form.find('.product-size-row').remove();
					Ecommerce.Sizes.addAddRow();
				}
			});
		});
	}
};

//check to see if the page has loaded, if so...
$(function() {
	Ecommerce.Controller.init();
});