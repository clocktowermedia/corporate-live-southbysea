var Potentials = {
	Loader: null,
	Pages: null,
	Reporting: null
};

/**
 * Forms.Loader
 *
 * Run every page through here and feed it only the JS it needs.
 */
Potentials.Loader = {
	init: function() {

		// Fire off JS based on URI segment
		var uri = document.location.pathname.split('/');

		//init stuffs
		if (uri[3] == 'reporting' && (uri[4] == 'create' || uri[4] == 'edit'))
		{
			Potentials.Reporting.init();
		}

		if (uri[3] == 'create' || uri[3] == 'edit')
		{
			head.js('/assets/modules/potentials/js/form.js');
		}
	}
};

Potentials.Reporting = {
	selectors: {
		'sortContainer': '#field-container'
	},

	init: function() {
		Potentials.Reporting.reportFields();
		Potentials.Reporting.hijackReportCreation();
	},

	reportFields: function() {
		var $list = $('#field-list');
		var $form = $('form');

		//init field selection tree
		$.getJSON('/admin/potentials/json/report-fields/' + $form.attr('data-id'), function(response){
			var $tree = $list.jstree({
				'core': {
					'data': response,
					'themes': {
						'variant': 'large'
					}
				},
				'plugins' : ['wholerow', 'checkbox']
			});
		});

		//init dynamic loading of sortables
		$list.on('select_node.jstree', function(e, data){
			var fields = [data.node.id];
			$.getJSON('/admin/potentials/json/report-fields-by-id', {ids: fields}, function(response){
				Potentials.Reporting.reportFieldSorting(response, true);
			});
		});
		$list.on('deselect_node.jstree', function(e, data){
			$populate = $(Potentials.Reporting.selectors.sortContainer);
			$populate.find('#fields_' + data.node.id).remove();
			$populate.sortable('refresh');
		});

		//if we have a report id, load fields
		if ($form.attr('data-id') > 0)
		{
			$.getJSON('/admin/potentials/json/report-fields/' + $form.attr('data-id') + '/sortable', function(response){
				Potentials.Reporting.reportFieldSorting(response, false);
			});
		}
	},

	reportFieldSorting: function(data, append) {
		append = (typeof append === 'undefined')? false : append;

		//populate container
		var $populate = $(Potentials.Reporting.selectors.sortContainer);
		var $template = $('#' + $populate.attr('id').replace('container', '') + 'listing');
		var template = Handlebars.compile($template.html());

		if (append == true)
		{
			$populate.append(template(data));
		} else {
			$populate.html(template(data));
		}

		//init sorting
		$populate.sortable({
			opacity: '0.5',
			items: '> li',
		});
	},

	hijackReportCreation: function() {
		var $form = $('form');

		//on form submit, use ajax instead
		$form.on('submit', function(e){
			e.preventDefault();
			var $sortable = $(Potentials.Reporting.selectors.sortContainer);

			$.ajax({
				type: 'POST',
				url: $form.attr('action'),
				data: $sortable.sortable('serialize') + '&title=' + $form.find('input[name="title"]').val(),
				dataType: 'json',
				success: function(response) {
					if (response.status == 'ok')
					{
						//redirect if applicable
						if (response.hasOwnProperty('redirect'))
						{
							window.location = response.redirect;
						} else {
							//show success message
							Cms.System.showAlert('success', response.message);
						}
					} else {
						Cms.System.showAlert('danger', response.message);
					}
				},

				error: function(response) {
					//show error message
					Cms.System.showAlert('danger', JSON.parse(response.responseText));
				}
			});
		});
	}
};

//check to see if the page has loaded, if so...
$(function() {
	Potentials.Loader.init();
});