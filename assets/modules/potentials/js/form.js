var PotentialForm = {
	Selector: '#order-form-container',
	Loader: null,
	Fields: null,
	Datatables: null,
	Modals: null,
	Monetary: null,
	Attachments: null,
	Listings: null,
	Functions: null
};

/**
 * PotentialForm.Loader
 *
 * Run every page through here and feed it only the JS it needs.
 */
PotentialForm.Loader = {
	init: function() {

		// Fire off JS based on URI segment
		var uri = document.location.pathname.split('/');

		//init stuffs
		PotentialForm.Fields.init();
		PotentialForm.Monetary.init();
		PotentialForm.Attachments.init();
		PotentialForm.Modals.init();

		head.load('//cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js', function(){
			PotentialForm.Listings.init();
		});

		//init datatables
		var $tables = $('div.potential-items');
		$tables.each(function(){
			PotentialForm.Datatables.init('#' + $(this).attr('id'));
		});
	}
};

PotentialForm.Fields = {
	init: function() {
		PotentialForm.Fields.initWysiwyg();
		PotentialForm.Fields.initDatepickers();
		PotentialForm.Fields.panels();
		PotentialForm.Fields.selects();
		PotentialForm.Fields.autosuggest();
		PotentialForm.Fields.autosave();
		PotentialForm.Fields.autopopulate();
	},

	selects: function() {
		//disable first option on all selects
		$('option[value=""]').attr('disabled', 'disabled');
	},

	initWysiwyg: function() {
		$('textarea.plain').trumbowyg({
			btns: []
		});
	},

	initDatepickers: function() {
		$('input.date').pickadate({
			//format: 'mm/dd/yyyy',
			format: 'yyyy-mm-dd',
			selectYears: true,
			selectMonths: true,
			onOpen: function() {
				var selected = this.get();
				if (selected == '0000-00-00')
				{
					var date = new Date();
					this.set('select', [date.getFullYear(), date.getMonth() + 1, date.getDate()]);
				}
			}
		});
	},

	panels: function() {
		//toggle plus/minus icons for panels
        $('[data-toggle="collapse"]').on('click', function() {
            var $this = $(this), $parent = typeof $this.data('parent')!== 'undefined' ? $($this.data('parent')) : undefined;

            //if no parent, toggle on itself
            if($parent === undefined) {
                $this.find('.glyphicon').toggleClass('glyphicon-plus glyphicon-minus');
                return true;
            }

            /* Open element will be close if parent !== undefined */
            var currentIcon = $this.find('.glyphicon');
            currentIcon.toggleClass('glyphicon-plus glyphicon-minus');
            $parent.find('.glyphicon').not(currentIcon).removeClass('glyphicon-minus').addClass('glyphicon-plus');
        });
	},

	autosuggest: function() {
		$('select.select2').select2();

		$('input.autosuggest').each(function(){
			var $field = $(this);

			$field.select2({
				placeholder: $field.attr('placeholder'),
				minimumInputLength: 3,
				createSearchChoice: function (term) {
					return {
						id: $.trim(term),
						text: $.trim(term)
					};
				},
				ajax: {
					url: $field.attr('data-url'),
					dataType: 'json',
					data: function(term, page) {
						return {
							query: term
						};
					},
					results: function(data, page) {
						return {
							results: data.results
						};
					}
				}
			});

			$field.on('select2-selecting', function(e) {
				var selectionData = e.object;
				var $linked = $('input[data-autosuggest-link="' + $field.attr('name') + '"]');
				if ($linked.length > 0)
				{
					$linked.each(function(){
						var $field = $(this);

						//populate value
						var attr = $field.attr('data-autosuggest-val');
						if (typeof attr !== 'undefined' && selectionData.hasOwnProperty(attr))
						{
							$field.val(selectionData[attr]);
						}

						//disable field if applicable
						var disable = $field.attr('data-autosuggest-disable');
						if (typeof disable !== 'undefined' && disable == true)
						{
							$field.prop('disabled', true);
							$field.attr('readonly');
						}
					});
				}
			});
		});
	},

	autopopulate: function() {
		//define the form
		var $form = $(PotentialForm.Selector);

		$form.find('select.populated-options').on('change', function(){
			var value = $(this).val();
			if (typeof $(this).attr('data-category-id') !== 'undefined')
			{
				$.ajax({
					method: 'GET',
					url: '/forms/json/get-linked-fields',
					dataType: 'json',
					data: {
						categoryID: $(this).attr('data-category-id'),
						value: value
					},
					success: function(response) {
						if (response.status == 'ok' && response.linkedFields !== null && Object.keys(response.linkedFields).length > 0)
						{
							$.each(response.linkedFields, function(key, value){
								$form.find('input[name="' + key + '"], textarea[name="' + key + '"]').val(value).trigger('change');
								$form.find('select[name="' + key + '"]').val(value).change();
							});
						}
					},
					error: function(response) {
						console.log(JSON.parse(response.responseText));
					}
				});
			}
		});
	},

	autosave: function() {
		//define the form
		var $form = $(PotentialForm.Selector);

		//on input change, autosave
		$('input:not(.select2-input), textarea, select').on('change keyup blur', function(e){
			var value = '';

			if ($(this).attr('type') == 'checkbox')
			{
				value = ($(this).is(':checked'))? $(this).val() : $('input[type="hidden"][name="' + $(this).attr('name') + '"]').val();
			} else {
				value = $(this).val();
			}

			$.ajax({
				method: 'POST',
				url: '/admin/potentials/json/edit-field',
				dataType: 'json',
				data: {
					'id': $form.attr('data-id'),
					'name': $(this).attr('name'),
					'value': value
				},
				success: function(response) {
				},
				error: function(response) {
					console.log(JSON.parse(response.responseText));
				}
			});
		});
	}
};

PotentialForm.Attachments = {
	init: function() {
		$('body').on('click touch', 'button.upload-btn, a.upload-btn', function(){
			var $button = $(this);
			var $input = $button.parents('div.upload-container').find('input.upload-trigger');
			$input.click();
		});

		PotentialForm.Attachments.hijackForm();
	},

	hijackForm: function() {
		$('div.upload-container').each(function(){
			//hijack form to allow file uploading
			var $container = $(this);
			var sendData = {};
			sendData[$container.attr('data-parent')] = $container.attr('data-id');
			$container.dmUploader({
				method: 'POST',
				url: $container.attr('data-url'),
				dataType: 'json',
				extraData: sendData,
				fileName: 'userfile',
				onNewFile: function(id, file){
				},
				onComplete: function(){
				},
				onUploadProgress: function(id, percent){
					var percentStr = percent + '%';
				},
				onUploadSuccess: function(id, data){

					//show error message if it didn't work
					if (data.status != 'ok')
					{
						alert('An error occurred: ' + data.message);
					} else {

						if (typeof $container.attr('data-table') !== 'undefined')
						{
							$('#potentials-' + $container.attr('data-table')).find('.datatable').dataTable().fnStandingRedraw();
						}

						if (typeof $container.attr('data-section') !== 'undefined')
						{
							PotentialForm.Listings.populate($container.attr('data-section') + '-container');
						}
					}
				},
				onUploadError: function(id, message){
					alert('An error occurred: ' + message);
				},
				onFileTypeError: function(file){
					alert('File \'' + file.name + '\' is not an accepted file type.');
				},
				onFileSizeError: function(file){
					alert('File \'' + file.name + '\' exceeds the file size limit.');
				},
				onFallbackMode: function(message){
					alert(message + '.');
				}
			});
		});
	}
};

PotentialForm.Listings = {
	init: function() {
		PotentialForm.Listings.initHelpers();
		PotentialForm.Listings.sortButton();
		$('div.handlebars-listing').each(function(){
			PotentialForm.Listings.populate($(this).attr('id'));
		});
	},

	populate: function(containerId) {
		var $populate = $('#' + containerId);
		var $template = $('#' + $populate.attr('id').replace('container', '') + 'listing');
		var url = $populate.attr('data-url');

		$.ajax({
			method: 'GET',
			url: url,
			dataType: 'json',
			success: function(response) {

				// compile and apply the main template
				var template = Handlebars.compile($template.html());
				$populate.html(template(response));

				//init some stuff we need to do again
				PotentialForm.Attachments.hijackForm();
			},
			error: function(response) {
				$populate.html(JSON.parse(response.responseText));
			}
		});
	},

	sortButton: function() {
		$('a.sort-listing').on('click', function(){
			//define container
			var $populate = $('#' + $(this).attr('data-section') + '-container');

			//update sort order attributes/icons/message
			var newSortOrder = ($(this).attr('data-order') == 'desc')? 'asc' : 'desc';
			$(this).attr('data-order', newSortOrder);
			$(this).find('i.fa').toggleClass('fa-caret-up').toggleClass('fa-caret-down');
			$(this).find('span').text($(this).attr('data-' + newSortOrder + '-msg'))

			//update url with new sort order
			var url = $populate.attr('data-url');
			var newUrl = url.replace(new RegExp('('+ 'sort' +'=)[^\&]+') , '$1' + newSortOrder);
			if (typeof $(this).attr('data-order-by') !== 'undefined')
			{
				newUrl.replace(new RegExp('('+ 'sort-by' +'=)[^\&]+') , '$1' + $(this).attr('data-order-by'));
			}
			$populate.attr('data-url', newUrl);

			//refresh section
			PotentialForm.Listings.populate($populate.attr('id'));
		});
	},

	initHelpers: function() {
		Handlebars.registerHelper('dateFormat', function(context, block) {
			if (window.moment) {
				var f = block.hash.format || "MMM Do, YYYY";
				return moment(new Date(context)).format(f);
			} else {
				return context;   //  moment plugin not available. return data as is.
			}
		});

		Handlebars.registerHelper('compare', function (lvalue, operator, rvalue, options) {
			var operators, result;
			if (arguments.length < 3) {
				throw new Error("Handlerbars Helper 'compare' needs 2 parameters");
			}
			if (options === undefined) {
				options = rvalue;
				rvalue = operator;
				operator = "===";
			}
			operators = {
				'==': function (l, r) { return l == r; },
				'===': function (l, r) { return l === r; },
				'!=': function (l, r) { return l != r; },
				'!==': function (l, r) { return l !== r; },
				'<': function (l, r) { return l < r; },
				'>': function (l, r) { return l > r; },
				'<=': function (l, r) { return l <= r; },
				'>=': function (l, r) { return l >= r; },
				'typeof': function (l, r) { return typeof l == r; },
				'~=': function(l, r) { return l.indexOf(r) != -1; },
				'!~=': function(l, r) { return l.indexOf(r) == -1; }
			};
			if (!operators[operator]) {
				throw new Error("Handlerbars Helper 'compare' doesn't know the operator " + operator);
			}
			result = operators[operator](lvalue, rvalue);

			if (result) {
				return options.fn(this);
			} else {
				return options.inverse(this);
			}
		});
	}
};

PotentialForm.Datatables = {
	selectors: {
		'checkboxes': '.row-checkbox',
		'searchForm': 'form.datatables-search',
		'showDropdown': 'select[name="show"]',
		'actions': 'ul.datatables-actions',
		'container': '#inbox-container'
	},

	initFields: function(selector) {

		//init buttons and such
		PotentialForm.Datatables.checkAll(selector);
		PotentialForm.Datatables.rowCheckboxes(selector);
	},

	init: function(selector) {
		//get parent container
		var $parent = $(selector);

		if ($parent.length > 0)
		{
			//define our breakpoints
			responsiveHelper = undefined;
			var breakpointDefinition = {
				tablet: 1024,
				phone : 480
			};

			//define some elements
			var $form = $(PotentialForm.Selector);
			var $tableElement = $parent.find('table.datatable');
			var $searchForm = $parent.find(PotentialForm.Datatables.selectors.searchForm);
			var $showDropdown = $parent.find(PotentialForm.Datatables.selectors.showDropdown);

			var urlParts = $tableElement.attr('data-url').split('/');

			//kill old datatables instance and init new one
			var myDatatable = $tableElement.dataTable({
				"iDisplayLength": $showDropdown.val(),
				"bDestroy": true,
				"aaSorting": [],
				"bAutoWidth": false,
				"bProcessing": true,
				"bServerSide": true,
				"sServerMethod": "POST",
				"sAjaxSource": $tableElement.attr('data-url'),
				"sDom": "<'row'<'col-xs-12'>r>" + "t" + "<'row'<'col-xs-6'i><'col-xs-6'p>>",
				fnPreDrawCallback: function () {
					// Initialize the responsive datatables helper once.
					if (!responsiveHelper) {
						responsiveHelper = new ResponsiveDatatablesHelper($tableElement, breakpointDefinition);
					}
				},
				fnDrawCallback : function (oSettings) {
					responsiveHelper.respond(PotentialForm.Datatables.container);
					PotentialForm.Datatables.initFields(selector);
					Cms.System.initIcheck();
				},
				fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
					//responsiveHelper.createExpandIcon(nRow);

					//save the row
					var $currentRow = $(nRow);

					//define all the td's except the first one, since we want to modify this
					if ($tableElement.hasClass('editable'))
					{
						var $clickable = $currentRow.find('td').not(':first-child');
						$clickable.on('click', function() {
							//assume last element is id
							Cms.System.showModal('/admin/potentials/modals/edit/' + urlParts[4] + '/' + aData[aData.length - 1], 'Edit ' + PotentialForm.Functions.ucwords(urlParts[4]));
						});
						$clickable.css('cursor', 'pointer');
					}

					//return the row
					return nRow;
				}
			});

			//on winow resize, make the table responsive, delay half a second since that's the transition time for the sidebar menu
			/*
			$(window).resize(function() {
				setTimeout(function(){
					PotentialForm.Datatables.responsiveHelper.respond(PotentialForm.Datatables.container);
				}, 500);
			});
			*/

			//on hide/show sidebar, make the table responsive, delay half a second since that's the transition time for the sidebar menu
			$(window).on('show_sidenav', function(e, params){
				setTimeout(function(){
					//PotentialForm.Datatables.responsiveHelper.respond(PotentialForm.Datatables.container);
				}, 500);
			});

			/*
			//set to reload every 5 minutes
			var refresh = 60000 * 5;
			setInterval( function () {
				//redirect if we get a weird response
				$.ajax({
					url: '/admin/forms/datatables/data',
					dataType: 'json',
					success: function (response) {
						if (response.hasOwnProperty('sColumns') == false) {
							window.location.href = '/admin/dashboard';
						}
					}
				});

				//refresh forms table
				myDatatable.fnStandingRedraw();

			}, (refresh));
			*/

			//search form
			$searchForm.on('submit', function(e){
				//prevent default behavior
				e.preventDefault();

				//filter by search string
				var query = $(this).find('input[name="query"]').val();
				myDatatable.fnFilter(query);
			});
			$searchForm.find('input[name="query"]').on('keyup', function(e){
				//filter by search string
				myDatatable.fnFilter($(this).val());
			});

			//display length dropdown
			$showDropdown.on('change', function(e){
				var oSettings = myDatatable.fnSettings();
				oSettings._iDisplayLength = $(this).val();
				myDatatable.fnDraw();
			});

			//dropdown actions
			$parent.find(PotentialForm.Datatables.selectors.actions).find('a[data-hijack]').on('click', function(e){

				//make sure checkboxes are actually checked
				if (PotentialForm.Datatables.getCheckedCount(selector) > 0)
				{
					//grab checked checkboxes as data
					var formData = $parent.find('input[name="items[]"]').serialize();

					//get status & type
					var potentialID = $form.attr('data-id');
					var status = $(this).attr('data-hijack');
					var type = $(this).attr('data-type');
					var secondaryData = {'status': status, 'type': type};

					//format submitData
					var submitData = formData + '&' + $.param(secondaryData);

					//make an ajax call
					$.ajax({
						url: '/admin/potentials/json/update-status/' + potentialID,
						type: 'POST',
						data: submitData,
						dataType: 'json',
						success: function(response) {

							if (response.status == 'ok')
							{
								//reload the table
								myDatatable.fnStandingRedraw();

							} else {

								//alert user that an error occurred
								alert(response.message);
							}
						},
						error: function(response) {

							//show error message
							alert('An error occurred, please refresh the page and try again.');
						},
						complete: function() {

							//clear checkboxes
							PotentialForm.Datatables.clearCheckboxes(selector);
						}
					});

				} else {
					alert('No items are selected.');
				}
			});
		}
	},

	disableUI: function(selector) {
		//get parent container
		var $parent = $(selector);

		if ($parent.length > 0)
		{
			//get pagination and search
			var $pagination = $parent.find('ul.pagination');
			var $searchForm = $parent.find(PotentialForm.Datatables.selectors.searchForm);

			//if we have anything checked, disable search and pagination links
			if (PotentialForm.Datatables.getCheckedCount(selector) > 0)
			{
				//disable pagination
				$pagination.find('li, li a').not('.disabled').each(function(){
					$(this).addClass('disabled').addClass('disable-init');
				});

				//disable search
				$searchForm.find('input, button').attr('disabled', 'disabled');

				//disable clicking
				$pagination.find('a').on('click', function(e){
					if ($(this).hasClass('disabled')) {
						e.preventDefault();
						return false;
					}
				});

			} else {

				//enable pagination
				$pagination.find('.disable-init').each(function(){
					$(this).removeClass('disabled').removeClass('disable-init');
				});

				//enable search
				$searchForm.find('input, button').removeAttr('disabled');
			}
		}
	},

	checkAll: function(selector) {
		//get parent container
		var $parent = $(selector);

		if ($parent.length > 0)
		{
			//check all messages on page
			$parent.find('input.check-all').on('change', function(e){

				//determine if main checkbox is checked
				var isChecked = $(this).is(':checked');
				var $checkboxes = $parent.find('table.datatable').find(PotentialForm.Datatables.selectors.checkboxes);

				if (isChecked) {
					$checkboxes.prop('checked', true).icheck('checked');
				} else {
					$checkboxes.prop('checked', false).icheck('unchecked');
				}

				//add listeners for checkboxes
				PotentialForm.Datatables.disableUI(selector);
			});
		}
	},

	rowCheckboxes: function(selector) {
		//get parent container
		var $parent = $(selector);

		if ($parent.length > 0)
		{
			//disable pagination and search if checkboxes are checked
			$parent.find('table.datatable').find(PotentialForm.Datatables.selectors.checkboxes).on('change', function(){
				PotentialForm.Datatables.disableUI(selector);
			});
		}
	},

	getCheckedCount: function(selector) {
		//get parent container
		var $parent = $(selector);

		//return number of checked items
		return $parent.find('table.datatable').find(PotentialForm.Datatables.selectors.checkboxes + ':checked').length;
	},

	clearCheckboxes: function(selector) {
		//get parent container
		var $parent = $(selector);

		if ($parent.length > 0)
		{
			//uncheck all boxes and re-enable UI
			$parent.find('input[type="checkbox"]').prop('checked', false).icheck('unchecked');
		}

		PotentialForm.Datatables.disableUI(selector);
	},
};

PotentialForm.Modals = {
	init: function() {
		$('#modal-layout').on('shown.bs.modal', function(){
			Cms.System.forms('#modal-layout form');
		});

		$('body').on('submit', 'form.ajax-submit', function(e){
			e.preventDefault();

			//get form & modal
			var $form = $(this);
			var $modal = $('#modal-layout');

			$.ajax({
				method: $form.attr('method'),
				url: $form.attr('action'),
				data: $form.serialize(),
				dataType: 'json',
				success: function(response) {
					if (response.status == 'ok')
					{
						//close modal and reload datatable or section
						$modal.modal('hide');

						if (typeof $form.attr('data-table') !== 'undefined')
						{
							$('#potentials-' + $form.attr('data-table')).find('.datatable').dataTable().fnStandingRedraw();

							//if expenses, recalc
							if ($form.attr('data-table') == 'expenses')
							{
								PotentialForm.Monetary.updateNormalTotals();
								PotentialForm.Monetary.updateReprintTotals();
							}
						}

						if (typeof $form.attr('data-section') !== 'undefined')
						{
							PotentialForm.Listings.populate($form.attr('data-section') + '-container');
						}
					} else {
						alert(response.message);
					}
				},
				error: function(response) {
					alert('An error occurred, please try again.');
				}
			});
		});

		PotentialForm.Modals.initDelete();
	},

	initDelete: function() {
		var $modal = $('#modal-layout');

		$('body').on('click', 'a[data-confirm]', function (e) {
			e.preventDefault();

			//get the href & title of the clicked delete link
			var $link = $(this);
			var text = '<h4>' + $link.attr('data-confirm') + '</h4>';
			var section = (typeof $link.attr('data-section') === 'undefined')? '' : 'data-section="' + $link.attr('data-section') + '"';
			var table = (typeof $link.attr('data-table') === 'undefined')? '' : 'data-table="' + $link.attr('data-table') + '"';

			//append the modal to the page and show it
			$modal.html(
				'<div class="modal-dialog">' +
					'<div class="modal-content">' +
						'<div class="modal-header">' +
							'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
							'<h3 class="modal-title">Please Confirm</h3>' +
						'</div>' +
						'<div class="modal-body">' +
							text +
						'</div>' +
						'<div class="modal-footer">' +
							'<a class="btn btn-info delete-btn" data-url="' + $link.attr('href') + '" ' + section + ' ' + table + '">Confirm</button>' +
							'<a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a>' +
						'</div>' +
					'</div>' +
				'</div>'
			).modal('show');
		});

		$('body').on('click', '.delete-btn', function(e){
			e.preventDefault();
			var $link = $(this);

			$.ajax({
				method: 'GET',
				url: $(this).attr('data-url'),
				dataType: 'json',
				success: function(response) {
					if (response.status == 'ok')
					{
						//close modal and reload datatable or section
						$modal.modal('hide');

						if (typeof $link.attr('data-table') !== 'undefined')
						{
							$('#potentials-' + $link.attr('data-table')).find('.datatable').dataTable().fnStandingRedraw();

							//if expenses, recalc
							if ($form.attr('data-table') == 'expenses')
							{
								PotentialForm.Monetary.updateNormalTotals();
								PotentialForm.Monetary.updateReprintTotals();
							}
						}

						if (typeof $link.attr('data-section') !== 'undefined')
						{
							PotentialForm.Listings.populate($link.attr('data-section') + '-container');
						}
					} else {
						alert(response.message);
					}
				},
				error: function(response) {
					alert('An error occurred, please try again.');
				}
			});
		});
	}
};

PotentialForm.Monetary = {
	init: function() {

		//init on load
		PotentialForm.Monetary.updateNormalTotals();
		PotentialForm.Monetary.updateReprintTotals();

		$(PotentialForm.Selector).find('input.decimal').on('keyup blur', function(){
			PotentialForm.Monetary.updateNormalTotals();
			PotentialForm.Monetary.updateReprintTotals();
		});
	},

	updateNormalTotals: function() {
		//define fields
		var $form = $(PotentialForm.Selector);
		var $revenue = $form.find('input[name="preTax"]');
		var $profit = $form.find('input[name="profit"]');
		var $margin = $form.find('input[name="margin"]');

		//calculate expenses/margins and update
		if ($revenue.val() != '')
		{
			var expenses = PotentialForm.Monetary.calculateExpenses(':not(.reprint)');
			expenses += PotentialForm.Monetary.getTotalExpenseFromList();
			$profit.val(($revenue.val() - expenses).toFixed(2));
			$margin.val(($revenue.val() / ($revenue.val() - $profit.val())).toFixed(2));
		}
	},

	updateReprintTotals: function() {
		//define fields
		var $form = $(PotentialForm.Selector)
		var $revenue = $form.find('input[name="preTax"]');
		var $profit = $form.find('input[name="profit"]');
		var $reprintProfit = $form.find('input[name="reprintProfit"]');
		var $reprintMargin = $form.find('input[name="reprintMargin"]');

		//calculate expenses/margins and update
		if ($revenue.val() != '')
		{
			var expenses = PotentialForm.Monetary.calculateExpenses('.reprint');
			$reprintProfit.val(($profit.val() - expenses).toFixed(2));
			$reprintMargin.val(($revenue.val() / ($revenue.val() - $reprintProfit.val())).toFixed(2));
		}
	},

	calculateExpenses: function(additionalSelector) {
		//define additional selector
		additionalSelector = (typeof additionalSelector === 'undefined')? '' : additionalSelector;

		//define starting total
		var total = 0;

		//get other totals
		$(PotentialForm.Selector).find('input.decimal.expense' + additionalSelector).each(function(){
			total += $(this).val();
		});

		return total;
	},

	getTotalExpenseFromList: function() {
		var potentialID = $(PotentialForm.Selector).attr('data-id');
		$.post('/admin/potentials/json/get-expense-total/' + potentialID, function(response) {
			return response.total;
		});
	}
};

PotentialForm.Functions = {
	ucwords: function(string) {
		return string.toLowerCase().replace(/\b[a-z]/g, function(letter) {
			return letter.toUpperCase();
    	});
	}
};

//check to see if the page has loaded, if so...
$(function() {
	PotentialForm.Loader.init();
});