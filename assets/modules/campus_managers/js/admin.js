var CampusManagers = {
	Loader: null,
	Elements: null
};

/**
 * Admin.Loader
 *
 * Run every page through here and feed it only the JS it needs.
 */
CampusManagers.Loader = {
	init: function() {

		// Fire off JS based on URI segment
		var uri = document.location.pathname.split('/');

		//cropping stuffs
		$('a[href="#image"]').on('shown.bs.tab', function (e) {
			CampusManagers.Elements.cropper();
		});
	}
};

CampusManagers.Elements = {
	cropperInstance: null,
	selectors: {
		form: '#cropperForm',
		buttons: '#profile-image-cropper-btns',
		slider: '#zoom-slider'
	},
	cropper: function() {
		$('#profile-image-cropper > img').cropper({
			strict: true,
			responsive: true,
			checkImageOrigin: true,
			modal: true,
			guides: false,
			center: false,
			highlight: false,
			background: false,
			autoCrop: true,
			autoCropArea: 0.65,
			dragCrop: false,
			movable: true,
			rotatable: false,
			scalable: false,
			zoomable: true,
			aspectRatio: 1 / 1,
			cropBoxMovable: false,
			cropBoxResizable: false,
			//minContainerHeight: 300,
			//minContainerWidth: '100%',
			minCropBoxWidth: 500,
			minCropBoxHeight: 500,
			built: function(){
				CampusManagers.Elements.cropperInstance = $(this).cropper();
				CampusManagers.Elements.cropperControls();
				CampusManagers.Elements.hijackSubmit();
				//$(this).cropper('zoom', 1.5);

				/*
				if ($(this).attr('data-left') != '' && $(this).attr('data-top') != '')
				{
					var left = parseFloat($(this).attr('data-left'));
					var top = parseFloat($(this).attr('data-top'));
					$(this).cropper('setCanvasData', {
						left: left,
						top: top
					});
				}
				*/
			}
		});
	},

	cropperControls: function() {
		var $btns = $(CampusManagers.Elements.selectors.buttons);
		var $form = $(CampusManagers.Elements.selectors.form);

		$btns.find('.zoom_in').on('click', function() {
			CampusManagers.Elements.cropperInstance.cropper('zoom', 0.1);
		});

		$btns.find('.zoom_out').on('click', function() {
			CampusManagers.Elements.cropperInstance.cropper('zoom', -0.1);
		});

		$btns.find('.done').on('click', function() {
			var cropOptions = {
				width: 500,
				height: 500,
				fillColor: '#ff9944'
			};
			var croppedImage = CampusManagers.Elements.cropperInstance.cropper('getCroppedCanvas', cropOptions).toDataURL();
			var canvasData = CampusManagers.Elements.cropperInstance.cropper('getCanvasData');
			$form.find('input[name="image"]').val(croppedImage);
			$form.find('input[name="image_left"]').val(canvasData.left);
			$form.find('input[name="image_top"]').val(canvasData.top);
			$form.submit();
		});
	},

	hijackSubmit: function() {
		var $form = $(CampusManagers.Elements.selectors.form);
		$form.on('submit', function(e){
			e.preventDefault();

			//submit the form via ajax
			$.ajax({
				url: '/admin/campus-managers/json/upload',
				type: 'POST',
				dataType: 'json',
				data: $form.serialize(),
				success: function(response) {
					if (response.status == 'ok')
					{
						//replace thumbnail with cropped version
						var src = response.image + '?t=' + (new Date()).getTime();
						var $container = $('div.cropped-img-container');
						var $img = $container.find('img');
						if ($img.length > 0)
						{
							$img.attr('src', src);
						} else {
							$container.html('');
							$container.html('<img class="image-full img-full profile-image-cropped" src="' + src + '" />');
						}
					}
				},
				error: function(response) {
					//reset cropper?
				}
			});
		});
	}
};

CampusManagers.Loader.init();