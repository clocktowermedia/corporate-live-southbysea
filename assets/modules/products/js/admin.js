var Products = {
	Loader: null,
	AddEdit: null,
	Reorder: null
};

/**
 * Products.Loader
 *
 * Run every page through here and feed it only the JS it needs.
 */
Products.Loader = {
	init: function() {

		// Fire off JS based on URI segment
		var uri = document.location.pathname.split('/');

		//load the site js that's used on every page
		if (uri[3] == 'create' || uri[3] == 'edit')
		{
			Products.AddEdit.form();

			if (uri[3] == 'edit')
			{
				Products.AddEdit.gallery();
				Products.AddEdit.colors();
			}
		}

		//init stuff on product categories
		if (uri[3] == 'categories')
		{
			if (uri[4] == 'reorder-products')
			{
				Products.Reorder.initProducts(uri);
			}

			if (uri[4] == 'reorder')
			{
				Products.Reorder.categories();
			}
		}
	}
};

Products.AddEdit = {
	form: function() {
		//define the main form
		var $form = $('#main-product-form');
		var $formButton = $('#submit-product-form-btn');

		//define the "other" tabs
		var $otherTabs = $('#session-form-tabs a:not([href="#product"])');

		//when other tabs are clicked, submit the main product form
		$otherTabs.on('click', function(e){
			e.preventDefault();
			var $tab = $(this);

			//make sure form is valid
			var isValid = $form.validation().isValid();

			if (isValid == true)
			{
				//submit the product form
				$.ajax({
					url: $form.attr('data-action'),
					method: 'POST',
					data: $form.serialize(),
					dataType: 'json',
					success: function(response) {
						if (response.status == 'ok')
						{
							//add product id to form
							$form.attr('data-id', response.productID);

							//re-enable the tabs visually
							//$otherTabs.attr('data-toggle', 'tab');
							$otherTabs.parents('li').removeClass('disabled');

							//if we are on creation form...change action to edit
							if ($form.attr('data-action-type') == 'create')
							{
								$formButton.html('Update Product');
								$form.attr('data-action-type', 'edit');
								$form.attr('action', '/admin/products/edit/' + response.productID);
								$form.attr('data-action', '/admin/products/json/edit/' + response.productID);
								Products.AddEdit.gallery();
								Products.AddEdit.colors();
							}

							//show the tab
							$tab.tab('show');
						} else {

							//disable tabs visually
							$otherTabs.parents('li').removeClass('disabled').addClass('disabled');

							//show alert
							if (response.hasOwnProperty('showMessage'))
							{
								Cms.System.showAlert('danger', response.message);
							} else {
								alert(response.message);
							}
						}
					},
					error: function() {
						alert('An error occurred.');
					}
				});

			} else {
				$form.validation().validate();
				return false;
			}
		});

		//submit form when they click the button
		$formButton.on('click', function(e){
			$form.submit();
		});
	},

	gallery: function() {
		head.js('/assets/modules/products/js/admin-images.js');
	},

	colors: function() {
		head.js('/assets/modules/products/js/admin-colors.js');
	}
};

Products.Reorder = {
	initProducts: function(uri) {
		Products.Reorder.featuredProducts(uri);
		Products.Reorder.products(uri);
	},

	featuredProducts: function(uri) {
		$('#featured-product-images').find('div.row').sortable({
			opacity: '0.5',
			items: '> div.thumbnail-img',
			update: function(e, ui) {
				serial = $(this).sortable('serialize');
				$.ajax({
					url: '/admin/products/json/reorder-products/' + uri[5] + '/featured',
					type: 'POST',
					data: serial,
					dataType: 'json',
					success: function(response) {
						if (response.status != 'ok')
						{
							alert(response.message);
						}
					},
					error: function() {
						alert('An error occurred. Please try again.');
						return false;
					}
				});
			}
		});
	},

	products: function(uri) {
		$('#product-images').find('div.row').sortable({
			opacity: '0.5',
			items: '> div.thumbnail-img',
			update: function(e, ui) {
				serial = $(this).sortable('serialize');
				$.ajax({
					url: '/admin/products/json/reorder-products/' + uri[5],
					type: 'POST',
					data: serial,
					dataType: 'json',
					success: function(response) {
						if (response.status != 'ok')
						{
							alert(response.message);
						}
					},
					error: function() {
						alert('An error occurred. Please try again.');
						return false;
					}
				});
			}
		});
	},

	categories: function() {
		//initiate nested sortable on the menu
		$('ol.sortable').nestedSortable({
			maxLevel: 1,
			listType: 'ol',
			//handle: 'li',
			items: 'li',
			toleranceElement: '> div',
			forcePlaceholderSize: true,
			placeholder: 'placeholder',
			protectRoot: true,
			start: function(e, ui) {
				// creates a temporary attribute on the element with the old parent id
				var parentID = ui.item.parent('ol').parent('li').attr('id');
				ui.item.attr('data-prevparent', parentID);
			},
			stop: function(e, ui) {
				//do not allow items to have their parent changed
				var newParentID = ui.item.parent('ol').parent('li').attr('id');
				if (newParentID != ui.item.attr('data-prevparent'))
				{
					$(this).nestedSortable('cancel');
				}
			},
			update: function() {
				//var reorderedItems = JSON.stringify($(menuSelector).nestedSortable('toHierarchy'));
				var individualItems = $(this).nestedSortable('serialize'); //$item['pageID'] = parent ID, depth
				$.ajax({
					type: 'post',
					url: '/admin/products/categories/json/reorder',
					data: individualItems,
					success: function(feedback) {
					},
					error: function() {
						alert('An error occurred. Please try again.');
					}
				});
			}
		});
	}
};

Products.Loader.init();