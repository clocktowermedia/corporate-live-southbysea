var ProductImages = {
	Loader: null,
	Selectors: null,
	Upload: null,
	Markup: null,
	Actions: null
};

/**
 * Admin.Loader
 *
 * Run every page through here and feed it only the JS it needs.
 */
ProductImages.Loader = {
	init: function() {

		// Fire off JS based on URI segment
		var uri = document.location.pathname.split('/');

		//load images
		ProductImages.Markup.init();
		ProductImages.Actions.init();

		//hijack form submission
		ProductImages.Upload.init();
	}
};

ProductImages.Selectors = {
	mainForm: '#main-product-form',
	productImages: '#product-images',
	datatable: $('#product-image-table').dataTable(),
	uploader: null
};

ProductImages.Upload = {
	init: function() {
		ProductImages.Upload.addListeners();
		ProductImages.Upload.hijackForm();
	},

	addListeners: function() {
		$('#upload-btn').on('click touch', function(){
			$('#upload-trigger').click();
		});
	},

	hijackForm: function() {
		//define the form
		var $form = $(ProductImages.Selectors.mainForm);

		//hijack form to allow file uploading
		ProductImages.Selectors.uploader = $('#upload-photos').dmUploader({
			method: 'POST',
			maxFileSize: 0,
			allowedTypes: 'image/*',
			extFilter: 'jpg;png;gif',
			url: '/admin/products/json/upload-image',
			dataType: 'json',
			extraData: {
				productID: $form.attr('data-id')
			},
			fileName: 'userfile',
			onNewFile: function(id, file){
			},
			onComplete: function(){
				//refresh and show new images
				ProductImages.Markup.refresh();
				ProductImages.Selectors.datatable.fnStandingRedraw();
			},
			onBeforeUpload: function(id) {
				return false;
			},
			onUploadProgress: function(id, percent){
				var percentStr = percent + '%';
			},
			onUploadSuccess: function(id, data){
				//console.log('Server Response for file #' + id + ': ' + JSON.stringify(data));
			},
			onUploadError: function(id, message){
				alert('An error occurred: ' + message);
			},
			onFileTypeError: function(file){
				alert('File \'' + file.name + '\' is not an accepted file type.');
			},
			onFileSizeError: function(file){
				alert('File \'' + file.name + '\' exceeds the file size limit.');
			},
			onFallbackMode: function(message){
				alert(message);
			}
		});
	}
};

ProductImages.Markup = {
	init: function() {
		ProductImages.Markup.registerHelpers();
		ProductImages.Markup.refresh();
	},

	refresh: function() {
		//define the form
		var $form = $(ProductImages.Selectors.mainForm);

		//define our templates/placeholders
		var $template = $('#product-images-listing');
		var $populate = $(ProductImages.Selectors.productImages);

		$.ajax({
			type: 'GET',
			url: '/admin/products/json/images?id=' + $form.attr('data-id'),
			dataType: 'json',
			success: function(response) {

				// compile and apply the main template
				var template = Handlebars.compile($template.html());
				$populate.html(template(response));

				ProductImages.Markup.popovers();
			},
			error: function(response) {
				alert('Error');
				//$populate.html(JSON.parse(response.responseText));
			}
		});
	},

	popovers: function(action) {
		var $popovers = $(ProductImages.Selectors.productImages).find('[data-toggle="popover"]');

		//perform popover action
		if (typeof action !== "undefined" && action !== '')
		{
			$popovers.popover(action);
		}

		$popovers.popover({
			html: true,
			placement: 'auto',
			trigger: 'hover focus',
			content: function() {
				//get id
				var id = $(this).attr('data-id');

				//get content
				var html = $('#popover-content-' + id).html();
				return html;
			},
			delay: {show: 50, hide: 300}
		});

		$popovers.on('show.bs.popover', function() {
			//hide other popovers
			$popovers.not(this).popover('hide');
		});
	},

	registerHelpers: function() {
		Handlebars.registerHelper('equal', function(lvalue, rvalue, options) {
			if (arguments.length < 3)
				throw new Error("Handlebars Helper equal needs 2 parameters");
			if( lvalue!=rvalue ) {
				return options.inverse(this);
			} else {
				return options.fn(this);
			}
		});
	}
};

ProductImages.Actions = {
	init: function() {
		ProductImages.Actions.hijackDelete();
		ProductImages.Actions.hijackEdit();
		ProductImages.Actions.hijackDefault();
		ProductImages.Actions.hijackModel();
		ProductImages.Actions.deleteFromDatatables();
	},

	hijackDelete: function() {
		$('body').on('click', 'a.delete-image', function(){
			//get id
			var imageID = $(this).attr('data-id');

			//try to delete the image
			$.ajax({
				type: 'GET',
				url: '/admin/products/json/delete-image/' + imageID,
				dataType: 'json',
				success: function(response) {

					if (response.status == 'ok')
					{
						Cms.System.showAlert('success', response.message);
						ProductImages.Markup.refresh();
					} else {
						Cms.System.showAlert('danger', response.message);
					}
				},
				error: function(response) {
					alert('An error occurred. Please try again.');
				},
				complete: function() {
					ProductImages.Markup.popovers('hide');
					ProductImages.Selectors.datatable.fnStandingRedraw();
				}
			});
		});
	},

	hijackEdit: function() {

		//define modal
		var $modal = $('#modal-layout');

		//hijack modal shown
		$modal.on('shown.bs.modal', function(e) {
			//init select2 if applicable
			if ($modal.find('#image-modal-form').length > 0)
			{
				$modal.find('select[name="productColorID"]').select2({
					formatResult: ProductImages.Actions.formatEditSelect,
					formatSelection: ProductImages.Actions.formatEditSelect,
					escapeMarkup: function(m) { return m; }
				});
			}
		});

		//hijack edit submission
		$('body').on('submit', '#image-modal-form', function(e){
			e.preventDefault();

			//find the form
			$form = $('#image-modal-form');

			//get image id
			var imageID = $form.attr('data-id');
			
			//submit form
			$.ajax({
				type: 'POST',
				url: '/admin/products/json/update-image/' + imageID,
				dataType: 'json',
				data: $form.serialize(),
				success: function(response) {

					if (response.status == 'ok')
					{
						Cms.System.showAlert('success', response.message);
					} else {
						Cms.System.showAlert('danger', response.message);
					}
				},
				error: function(response) {
					alert('An error occurred. Please try again.');
				},
				complete: function() {
					ProductColors.Markup.popovers('hide');
					$modal.modal('hide');
					ProductImages.Selectors.datatable.fnStandingRedraw();
				}
			});
		});
	},

	hijackDefault: function() {
		$('body').on('click', 'a.set-as-default', function(){
			//get id
			var imageID = $(this).attr('data-id');

			//try to delete the image
			$.ajax({
				type: 'GET',
				url: '/admin/products/json/set-default-image/' + imageID,
				dataType: 'json',
				success: function(response) {

					if (response.status == 'ok')
					{
						Cms.System.showAlert('success', response.message);
						ProductImages.Markup.refresh();
					} else {
						Cms.System.showAlert('danger', response.message);
					}
				},
				error: function(response) {
					alert('An error occurred. Please try again.');
				},
				complete: function() {
					ProductImages.Markup.popovers('hide');
				}
			});
		});
	},

	hijackModel: function() {
		$('body').on('click', 'a.set-as-model', function(){
			//get id
			var imageID = $(this).attr('data-id');

			//try to delete the image
			$.ajax({
				type: 'GET',
				url: '/admin/products/json/set-model-image/' + imageID,
				dataType: 'json',
				success: function(response) {

					if (response.status == 'ok')
					{
						Cms.System.showAlert('success', response.message);
						ProductImages.Markup.refresh();
					} else {
						Cms.System.showAlert('danger', response.message);
					}
				},
				error: function(response) {
					alert('An error occurred. Please try again.');
				},
				complete: function() {
					ProductImages.Markup.popovers('hide');
				}
			});
		});
	},

	formatEditSelect: function(entry) {
		//define the option
		var option = entry.element;

		//define empty html
		var html = '';

		//format accordingly
		if ($(option).data('hex') != '')
		{
			html += '<span style="background:' + $(option).data('hex') + ';" class="color-swatch-dropdown"></span>' + entry.text;
		} else if ($(option).data('image') != '') {
			html += '<img src="' + $(option).data('image') + '" class="color-image-dropdown">' + entry.text;
		} else {
			html += entry.text;
		}

		html += '<div class="clearfix"></div>';
		return html;
	},

	deleteFromDatatables: function() {
		$('a.hijack-image-table').on('click', function(){
			var answer = confirm("Are you sure you want to delete these images?");
    		if (answer == true)
    		{
				//get form data
				var formData = ($(this).attr('data-images') != 'all')? $('input[name="images[]"]').serialize() : {images: 'all'};
				formData.productID = $(this).attr('data-parent-id');

				//make an ajax call
				$.ajax({
					url: '/admin/products/json/delete-images/' + status,
					type: 'POST',
					data: formData,
					dataType: 'json',
					success: function(response) {
					},
					error: function(response) {
						//show error message
						alert('An error occurred, please refresh the page and try again.');
					},
					complete: function() {
						//refresh table & markup
						ProductImages.Markup.refresh();
						ProductImages.Selectors.datatable.fnStandingRedraw();
					}
				});
			}
		});
	}
};

ProductImages.Loader.init();