var ProductColors = {
	Loader: null,
	Selectors: null,
	Upload: null,
	Markup: null,
	Actions: null,
	Datatables: null,
	Editable: null
};

/**
 * Admin.Loader
 *
 * Run every page through here and feed it only the JS it needs.
 */
ProductColors.Loader = {
	init: function() {

		// Fire off JS based on URI segment
		var uri = document.location.pathname.split('/');

		//load colors & init forms
		ProductColors.Markup.refresh();
		ProductColors.Actions.init();
		ProductColors.Colorpickers.init();
		ProductColors.Datatables.init();

		//hijack form submission
		ProductColors.Upload.init();
	}
};

ProductColors.Selectors = {
	mainForm: '#main-product-form',
	productColors: '#product-colors',
	datatable: $('#product-color-table').dataTable(),
};

ProductColors.AddForm = {
	init: function() {
		var response = '';

		//define our templates/placeholders
		var $template = $('#product-colors-listing');
		var $populate = $('#add-colors-form-container');

		// compile and apply the main template
		var template = Handlebars.compile($template.html());
		$populate.html(template(response));
	}
};

ProductColors.Upload = {
	init: function() {
		ProductColors.Upload.addListeners();
		ProductColors.Upload.hijackForm();
	},

	addListeners: function() {
		$('#upload-btn-colors').on('click', function(){
			$('#upload-trigger-colors').click();
		});
	},

	hijackForm: function() {
		//define the form
		var $form = $(ProductColors.Selectors.mainForm);

		//hijack form to allow file uploading
		$('#upload-colors').dmUploader({
			method: 'POST',
			maxFileSize: 0,
			allowedTypes: 'image/*',
			extFilter: 'jpg;png;gif',
			url: '/admin/products/json/upload-color',
			dataType: 'json',
			extraData: {
				productID: $form.attr('data-id')
			},
			fileName: 'userfile',
			onNewFile: function(id, file){
			},
			onComplete: function(){
				//refresh and show new images
				ProductColors.Markup.refresh();
			},
			onUploadProgress: function(id, percent){
				var percentStr = percent + '%';
			},
			onUploadSuccess: function(id, data){
				//console.log('Server Response for file #' + id + ': ' + JSON.stringify(data));
			},
			onUploadError: function(id, message){
				alert('An error occurred: ' + message);
			},
			onFileTypeError: function(file){
				alert('File \'' + file.name + '\' is not an accepted file type.');
			},
			onFileSizeError: function(file){
				alert('File \'' + file.name + '\' exceeds the file size limit.');
			},
			onFallbackMode: function(message){
				alert(message);
			}
		});
	}
};

ProductColors.Markup = {
	refresh: function() {
		//define the form
		var $form = $(ProductColors.Selectors.mainForm);

		//define our templates/placeholders
		var $template = $('#product-colors-listing');
		var $populate = $(ProductColors.Selectors.productColors);

		$.ajax({
			type: 'GET',
			url: '/admin/products/json/colors?id=' + $form.attr('data-id'),
			dataType: 'json',
			success: function(response) {

				// compile and apply the main template
				var template = Handlebars.compile($template.html());
				$populate.html(template(response));

				ProductColors.Markup.popovers();

				//refresh datatables
				ProductColors.Selectors.datatable.fnStandingRedraw();
			},
			error: function(response) {
				alert('Error');
				//$populate.html(JSON.parse(response.responseText));
			}
		});
	},

	popovers: function(action) {
		var $popovers = $(ProductColors.Selectors.productColors).find('[data-toggle="popover"]');

		//perform popover action
		if (typeof action !== "undefined" && action !== '')
		{
			$popovers.popover(action);
		}

		$popovers.popover({
			html: true,
			placement: 'auto',
			trigger: 'hover click focus',
			content: function() {
				//get id
				var id = $(this).attr('data-id');

				//get content
				var html = $('#popover-color-content-' + id).html();
				return html;
			},
			delay: {show: 50, hide: 300}
		});

		$popovers.on('show.bs.popover', function() {
			//hide other popovers
			$popovers.not(this).popover('hide');
		});
	}
};

ProductColors.Colorpickers = {
	init: function() {
		ProductColors.Colorpickers.registerHelpers();
		ProductColors.Colorpickers.refreshForm();
	},

	hijackAdd: function() {
		var $mainForm = $(ProductColors.Selectors.mainForm);
		var $form = $('#add-color-quantity');
		var $addButton = $form.find('#add-color-qty-btn');
		var $qty = $form.find('#colorQuantity');

		$addButton.on('click', function(e){
			if ($qty.val() > 0)
			{
				var response = {
					'qty': $qty.val(),
					'productID': $mainForm.attr('data-id')
				};

				ProductColors.Colorpickers.refreshForm(response);
			}
		});
	},

	hijackSubmit: function() {
		var $form = $('#colorpicker-form');
		var $submitButton = $form.find('#submit-color-qty');

		$form.find('input.colorpicker').minicolors({
			theme: 'bootstrap'
		});

		$submitButton.on('click', function(e){

			//make an ajax call to submit the form
			$.ajax({
				url: '/admin/products/json/add-color',
				type: 'POST',
				data: $form.serialize(),
				dataType: 'json',
				success: function(response) {
					if (response.status == 'ok')
					{
						ProductColors.Colorpickers.refreshForm();
						ProductColors.Markup.refresh();
					} else {
						alert(response.message);
					}
				},
				error: function(response) {

				}
			});
		});
	},

	refreshForm: function(response) {
		//define our templates/placeholders
		var $template = $('#add-colors-form');
		var $populate = $('#add-colors-form-container');

		// compile and apply the main template
		var template = Handlebars.compile($template.html());
		$populate.html(template(response));

		//hijack buttons again
		ProductColors.Colorpickers.hijackAdd();
		ProductColors.Colorpickers.hijackSubmit();

		//reinit datatable
		ProductColors.Selectors.datatable.fnStandingRedraw();
	},

	registerHelpers: function() {
		Handlebars.registerHelper('times', function(n, block) {
			var accum = '';
			for(var i = 1; i <= n; ++i)
				accum += block.fn(i);
			return accum;
		});
		Handlebars.registerHelper('for', function(from, to, incr, block) {
			var accum = '';
			for(var i = from; i < to; i += incr)
				accum += block.fn(i);
			return accum;
		});
	}
};

ProductColors.Actions = {
	init: function() {
		ProductColors.Actions.hijackDelete();
		ProductColors.Actions.hijackEdit();
	},

	hijackDelete: function() {
		$('body').on('click', 'a.delete-color', function(){
			//get id
			var colorID = $(this).attr('data-id');

			//try to delete the image
			$.ajax({
				type: 'GET',
				url: '/admin/products/json/delete-color/' + colorID,
				dataType: 'json',
				success: function(response) {

					if (response.status == 'ok')
					{
						Cms.System.showAlert('success', response.message);
						ProductColors.Markup.refresh();
					} else {
						Cms.System.showAlert('danger', response.message);
					}
				},
				error: function(response) {
					alert('An error occurred. Please try again.');
				},
				complete: function() {
					ProductColors.Markup.popovers('hide');
				}
			});
		});
	},

	hijackEdit: function() {

		//define modal
		var $modal = $('#modal-layout');

		//hijack edit submission
		$('body').on('submit', '#color-modal-form', function(e){
			e.preventDefault();

			//find the form
			$form = $('#color-modal-form');

			//get color id
			var colorID = $form.attr('data-id');
			
			//submit form
			$.ajax({
				type: 'POST',
				url: '/admin/products/json/update-color/' + colorID,
				dataType: 'json',
				data: $form.serialize(),
				success: function(response) {

					if (response.status == 'ok')
					{
						Cms.System.showAlert('success', response.message);
					} else {
						Cms.System.showAlert('danger', response.message);
					}
				},
				error: function(response) {
					alert('An error occurred. Please try again.');
				},
				complete: function() {
					ProductColors.Markup.popovers('hide');
					$modal.modal('hide');
					ProductColors.Selectors.datatable.fnStandingRedraw();
				}
			});
		});
	}
};

ProductColors.Datatables = {
	tables: {},
	init: function() {

		//init datatables
		$('table.datatable').each(function(){

			//init bootstrap styling of tables
			Cms.Datatables.initBootstrap();

			//define our breakpoints
			var responsiveHelper = undefined;
			var breakpointDefinition = {
				tablet: 1024,
				phone : 480
			};
			var url = $(this).attr('data-url');
			var id = $(this).attr('id');

			//init datatables
			var $tableElement = $(this);
			var table = $tableElement.dataTable({
				"bDestroy": true,
				"aaSorting": [],
				"bProcessing": true,
				"bServerSide": true,
				"sServerMethod": "POST",
				"sAjaxSource": url,
				"sPaginationType": 'bootstrap',
				"autoWidth": false,
				fnPreDrawCallback: function () {
					// Initialize the responsive datatables helper once.
					if (!responsiveHelper) {
						responsiveHelper = new ResponsiveDatatablesHelper($tableElement, breakpointDefinition);
					}
				},
				fnRowCallback  : function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
					responsiveHelper.createExpandIcon(nRow);
				},
				fnDrawCallback : function (oSettings) {
					responsiveHelper.respond();

					//init checkboxes
					Cms.System.initIcheck();

					//init editable
					ProductColors.Editable.init();
				}
			});

			ProductColors.Datatables.tables[id] = table;
		});
	}
};

ProductColors.Editable = {
	init: function()
	{
		head.js('//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js', function() {
			ProductColors.Editable.textFields();
			ProductColors.Editable.colorSelect();
		});
	},

	textFields: function() {

		//text fields
		$('.editable.text').editable({
			type: 'text',
			autotext: 'always',
			success: function(response) {
			},
			error: function(response) {
			}
		});
	},

	colorSelect: function() {
		$('.editable.select').each(function() {
			var id = $(this).attr('data-parent-id');
			var tableID = $(this).parents('table.datatable').attr('id');

			$(this).editable({
				type: 'select2',
				select2: {
					placeholder: 'Select A Color',
					dropdownAutoWidth : true,
					ajax: {
						url: '/admin/products/json/colors',
						dataType: 'json',
						data: function (term, page) {
							return {q: term, id: id};
						},
						results: function (data, page) {
							return {results: data.colors};
						}
					},
					id: function (item) {
						return item.productColorID;
					},
					formatResult: function (item) {
						return ProductColors.Editable.formatColorSelect(item);
					},
					formatSelection: function (item) {
						return ProductColors.Editable.formatColorSelect(item);
					},
					escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
				},
				success: function(response, newValue) {
					ProductColors.Datatables.tables[tableID].fnStandingRedraw();
				}
			});
		});
	},

	formatColorSelect: function(entry) {

		//define empty html
		var html = '';

		//format accordingly
		if (entry.hexCode != '')
		{
			html += '<span style="background:' + entry.hexCode + ';" class="color-swatch-dropdown"></span>' + entry.title;
		} else if (entry.thumbFullpath != '') {
			html += '<img src="' + entry.thumbFullpath + '" class="color-image-dropdown">' + entry.title;
		} else {
			html += entry.title;
		}

		html += '<div class="clearfix"></div>';
		return html;
	}
};

ProductColors.Loader.init();